package com.wex.bi.common;

import java.util.Properties;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.CrystalEnterprise;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.framework.ISessionMgr;
import com.wex.bi.businessobjects.BusObjFireEventInstance;

/**
 * @author Eric Maxham
 * @version 1.0
 */
public class BOSession {
	ISessionMgr sm = null;
	private IEnterpriseSession enterpriseSession = null;

	public BOSession() {
		try {
			
			sm = CrystalEnterprise.getSessionMgr();
			
			
			
		}
		catch(SDKException e) {
			throw new Error("An error has occured: " + e.getMessage());
		}
		catch(Exception e) {
			throw new Error("An error has occurred: " + e.getMessage());
		}
	}
	
	public IEnterpriseSession StartSession( ) {
		try {
			Properties p = ReadEnvironment.getEnvVars();
			//Values for BO_AUTHENTICATION = secLDAP secEnterprise
			String authentication = p.getProperty("BO_AUTHENTICATION");
			String cms = p.getProperty("BO_SERVER");
			String username = p.getProperty("BO_USER");
			String password = p.getProperty("BO_PASS");

			enterpriseSession = sm.logon(username, password, cms, authentication);
		//	enterpriseSession = sm.logon("borptrnr", "B0rptrn3r", "pwm-wex-1205.wrightexpress.com:6400","secEnterprise");
			
			System.out.println(enterpriseSession.getCMSName());
		}
		catch(SDKException e) {
			throw new Error("An error has occured: " + e.getMessage());
		}
		catch(Exception e) {
			throw new Error("An error has occurred: " + e.getMessage());
		} catch (Throwable e) {
		    e.printStackTrace();
		    System.exit(1);
		}
		
		return enterpriseSession;
	}
	
	public void EndSession()
	{
		try {
			if (enterpriseSession != null) {
				enterpriseSession.logoff();
			}
		}
		catch(Exception e) {
			throw new Error("An error has occurred: " + e.getMessage());
		}
	}
	

}
