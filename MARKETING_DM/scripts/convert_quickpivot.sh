#!/usr/bin/ksh
#############################################################################################################
#
#  File Name           : convert_quickpivot.sh
#  Parameters          : $1=ODATE, $2=File Name to process(w/o the zip extension)
#  Purpose             : Unzip and convert QuickPivot files from UTF-16 to WINDOWS-1252//TRANSLIT and rename 
#                        them to HUB/EDW standard names.
#  Created By          : Peter Burnell for BI-EDW
#                      : 03/03/2016
#  Log                 : Initial Script
#
###############################################################################################################

###############################################################################################################
#  This function will complete the ending procedure of the script in the case of an error
#  This function will always exit with a 16.
###############################################################################################################
exit_script_16() {
  echo "###### ABORT ##### "
  echo $1
  logger -t convert_quickpivot.sh -p local1.error $1
  exit 16
}
###############################################################################################################
# This function will convert a file from utf-16 to WINDOWS-1252 and remove the source file
###############################################################################################################
convert_utf_16() {
if [ -f $2 ]
then
  if [ $BegFileNbr = 0 ]
  then
    BegFileNbr=$1
  fi    
  EndFileNbr=$1
  iconv -f UTF-16 -t WINDOWS-1252//TRANSLIT   < $2    > $3
  if [ $? -ne 0 ] 
  then
      exit_script_16 "$1 has failed"
  fi
  echo "File: $1,Created:$3" >> $EDW_LOG/$tdate.convert_quickpivot.log
  echo "File: $1,Removed:$2" >> $EDW_LOG/$tdate.convert_quickpivot.log
  rm $2
fi
}
###############################################################################################################

logger -t convert_quickpivot.sh -p local1.notice "convert_quickpivot.sh has started"
echo  "convert_quickpivot.sh has started"  
#----------------------------------SET DATE------------------------------------- 
set -x 
tdate=`date +"%Y%m%d"`
date 

#echo "Check SOURCE environment "
#---------------------------DETERMINE SOURCE ENV----------------------------------
export MNAME=`uname -n`;
case $MNAME in
     pwm-wex-547.wrightexpress.com)
        export ORACLE_SID=pdedw
        export ENV=dev
        break;;
     apa-wex-550.wrightexpress.com)
        export ORACLE_SID=qtedw
        export ENV=stage
        break;;
     apa-wex-551.wrightexpress.com)
        export ORACLE_SID=qtedw
        export ENV=stage
        break;;
     *)
        export ORACLE_SID=predw
        export ENV=prod 
esac;

#------------------------------------------------------------------------------
export  EDW_SCRIPT=/r41/$ENV/hub/script
export  EDW_CTL=/r41/$ENV/hub/ctl 
export  EDW_SQL=/r41/$ENV/hub/sql
export  EDW_LOG=/i1/$ENV/hub/mktg/logs   
export  EDW_DATA=/i1/$ENV/hub/mktg/data

#------------------------------------------------------------------------------ 
echo "Server     : $MNAME"  >  $EDW_LOG/$tdate.convert_quickpivot.log
echo "Environment: $ENV"   >>  $EDW_LOG/$tdate.convert_quickpivot.log
echo "Date       : $tdate" >>  $EDW_LOG/$tdate.convert_quickpivot.log

# ensure that an odate and filename were passed
if [ -z "$1" ]; then
  exit_script_16 "convert_quickpivot.sh has failed : Missing Input Date Parameter<mmddyyyy>" 
fi
if [ -z "$2" ]; then
  exit_script_16 "convert_quickpivot.sh has failed : Missing Input 2nd parameter - File Name" 
fi

# Set up global variables
odate=$1
fname=$2
BegFileNbr=0
EndFileNbr=0

# Create parm file for Informatica run of the proc, add in the file range later
echo "[GLOBAL]" >  $EDW_DATA/file_range.dat

# get the zipped delta file to process, if more than 1, get the earliest, and check that it exists
dfiles=`ls $EDW_DATA/$fname*.zip`
if [ $? -eq 0 ] 
then
  arr=($dfiles)
  dfile=${arr[0]}

  echo "----------------------------------------------------------------------------------" >> $EDW_LOG/$tdate.convert_quickpivot.log
  echo "Unzipping file: $dfile"           >> $EDW_LOG/$tdate.convert_quickpivot.log
  echo "$1:$dfile"                        >  $EDW_DATA/file_nm.dat
 
  # rm any remnants of an old unzip that may have failed
  rm $EDW_DATA/*.txt
  
  # 7-zip extract(e) to load directory(-o)
  7z e -o$EDW_DATA $dfile >> $EDW_LOG/$tdate.convert_quickpivot.log 
  if [ $? -ne 0 ] 
  then
    exit_script_16 "7zip extraction Failed: $EDW_DATA $dfile"
  fi

  # Convert all the extracted files to lowercase
  for x in `ls $EDW_DATA/*.txt`
   do
    lc=`echo $x  | tr '[A-Z]' '[a-z]'`
    mv -f $x $lc
  done

  #change encoding and rename the files to the EDW standard naming convention to mount to Oracle as external tables, track files sent
  echo "convert to a proper encoding to load to Oracle" >> $EDW_LOG/$tdate.convert_quickpivot.log
  convert_utf_16   1  $EDW_DATA/fact_customerancillary_activity_daily.txt      $EDW_DATA/src_qp_cust_ancly_actvty_dly.dat 
  convert_utf_16   2  $EDW_DATA/fact_customerpurchase_activity_monthly.txt     $EDW_DATA/src_qp_cust_purch_actvty_mthly.dat  
  convert_utf_16   3  $EDW_DATA/fact_emailresponse_activity.txt                $EDW_DATA/src_qp_email_response_activity.dat 
  convert_utf_16   4  $EDW_DATA/fact_hubaccountcardissue_activity.txt          $EDW_DATA/src_qp_hub_acct_crd_issue_acty.dat  
  convert_utf_16   5  $EDW_DATA/fact_hubaccountcardstatus_monthly.txt          $EDW_DATA/src_qp_hub_acct_crd_sts_mthly.dat  
  convert_utf_16   6  $EDW_DATA/fact_marketingpromotion_activity.txt           $EDW_DATA/src_qp_marketing_promo_actvty.dat 
  convert_utf_16   7  $EDW_DATA/fact_nonrmidetailedresponse_activity.txt       $EDW_DATA/src_qp_non_rmi_dtl_resp_actvty.dat  
  convert_utf_16   8  $EDW_DATA/fact_pagetag_activity.txt                      $EDW_DATA/src_qp_page_tag_activity.dat  
  convert_utf_16   9  $EDW_DATA/fact_rmidetailedresponse_activity.txt          $EDW_DATA/src_qp_rmi_dtl_resp_activity.dat   
  convert_utf_16   10 $EDW_DATA/dim_carroll.txt                                $EDW_DATA/src_qp_carroll.dat    
  convert_utf_16   11 $EDW_DATA/dim_contactemail.txt                           $EDW_DATA/src_qp_contact_email.dat 
  convert_utf_16   12 $EDW_DATA/dim_dnb.txt                                    $EDW_DATA/src_qp_dnb.dat         
  convert_utf_16   13 $EDW_DATA/dim_fleetseek.txt                              $EDW_DATA/src_qp_fleet_seek.dat
  convert_utf_16   14 $EDW_DATA/dim_geography.txt                              $EDW_DATA/src_qp_geography.dat    
  convert_utf_16   15 $EDW_DATA/dim_havill.txt                                 $EDW_DATA/src_qp_havill.dat    
  convert_utf_16   16 $EDW_DATA/dim_hubaccount.txt                             $EDW_DATA/src_qp_hub_account.dat 
  convert_utf_16   17 $EDW_DATA/dim_hubapplication.txt                         $EDW_DATA/src_qp_hub_application.dat 
  convert_utf_16   18 $EDW_DATA/dim_hubopportunity.txt                         $EDW_DATA/src_qp_hub_opportunity.dat   
  convert_utf_16   19 $EDW_DATA/dim_infousa.txt                                $EDW_DATA/src_qp_infousa.dat   
  convert_utf_16   20 $EDW_DATA/dim_marketingcamp.txt                          $EDW_DATA/src_qp_marketing_camp.dat   
  convert_utf_16   21 $EDW_DATA/dim_marketingcompanysite.txt                   $EDW_DATA/src_qp_marketing_company_site.dat 
  convert_utf_16   22 $EDW_DATA/dim_pgmprod.txt                                $EDW_DATA/src_qp_pgm_prod.dat  
  convert_utf_16   23 $EDW_DATA/dim_polk.txt                                   $EDW_DATA/src_qp_polk.dat  
  convert_utf_16   24 $EDW_DATA/dim_purchasesite.txt                           $EDW_DATA/src_qp_purchase_site.dat  
  convert_utf_16   25 $EDW_DATA/dim_saleslead.txt                              $EDW_DATA/src_qp_sales_lead.dat  
  convert_utf_16   26 $EDW_DATA/dim_salesopportunity.txt                       $EDW_DATA/src_qp_sales_opportunity.dat 
  convert_utf_16   27 $EDW_DATA/assn_contactchannelproductpreference.txt       $EDW_DATA/src_qp_contact_chnl_prod_pref.dat 
  convert_utf_16   28 $EDW_DATA/assn_hubaccount_hubaccountcontact.txt          $EDW_DATA/src_qp_hub_acct_hub_acct_cntct.dat 
  convert_utf_16   29 $EDW_DATA/assn_hubacct_marketingcompanysite_marketingcontact_marketingcontactemail.txt  $EDW_DATA/src_qp_hacct_mktcosite_mktcnte.dat
  convert_utf_16   30 $EDW_DATA/assn_marketingcompany_salesterritory.txt       $EDW_DATA/src_qp_mktg_co_sales_terr.dat   
  convert_utf_16   31 $EDW_DATA/assn_marketingcompanysite_contact_email.txt    $EDW_DATA/src_qp_mktgcosite_cntct_email.dat    
  convert_utf_16   32 $EDW_DATA/assn_marketingcompanysite_hubaccount.txt       $EDW_DATA/src_qp_mktgcosite_hub_acct.dat  
  convert_utf_16   33 $EDW_DATA/assn_marketingcompanysite_hubapplication.txt   $EDW_DATA/src_qp_mktgcosite_hub_applic.dat  
  convert_utf_16   34 $EDW_DATA/assn_marketingcompanysite_hubopportunity.txt   $EDW_DATA/src_qp_mktgcosite_hub_opty.dat    
  convert_utf_16   35 $EDW_DATA/assn_marketingcompanysite_marketingcontact.txt $EDW_DATA/src_qp_mktgcosite_mktg_contact.dat    
  convert_utf_16   36 $EDW_DATA/assn_marketingcompanysiteaccountprod.txt       $EDW_DATA/src_qp_mktgcosite_acct_prod.dat  
  convert_utf_16   37 $EDW_DATA/assn_purchasesite_marketingcompanysite.txt     $EDW_DATA/src_qp_purch_site_mktgcosite.dat   
  convert_utf_16   38 $EDW_DATA/assn_sfdcopty_contact_email.txt                $EDW_DATA/src_qp_sfdc_opty_contact_email.dat  

  # save the table id range to load and rename the file
  echo "\$\$TABLEIDS=$BegFileNbr-$EndFileNbr" >> $EDW_DATA/file_range.dat
  echo "rename zip file"
  mv $dfile $dfile.PROCESSED
  
  # clean up processed files over x days
  days=10
  find $EDW_DATA/ -name "*.PROCESSED" -mtime +$days -exec rm -rf {} \;
else  
  echo "No files to process "                 >> $EDW_LOG/$tdate.convert_quickpivot.log
  echo "$1:NONE"                               > $EDW_DATA/file_nm.dat
  echo "\$\$TABLEIDS=$BegFileNbr-$EndFileNbr" >> $EDW_DATA/file_range.dat
fi #zip file found

echo  "convert_quickpivot.sh has ended successfully" 
logger -t convert_quickpivot.sh -p local1.notice "convert_quickpivot.sh has ended successfully "