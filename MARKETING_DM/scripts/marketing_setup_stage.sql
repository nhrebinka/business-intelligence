SET ECHO ON 
SET SERVEROUTPUT ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
SET LINESIZE 2000

/*
   NAME         :-    marketing_setup_stage.sql
   AUTHOR       :-    Time Howe
   DATE         :-    03/16
   DECRIPTION   :-    SQL  To create Roles for Marketing Schema 
   LOG          : -   VERSION 1.0 - Notes Changes made by Peter Burnell noted inline
*/

SPOOL /i1/&env/hub/logs/dba_run_role_script.log ;

/*===============================================================================================================================*
	New Tablespace: 
*===============================================================================================================================*/
CREATE BIGFILE TABLESPACE "MARKETING_DEFAULT" DATAFILE '+QTEDW_DATA' SIZE 10000M REUSE AUTOEXTEND ON NEXT 1024M MAXSIZE 60000M LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO ;

/*===============================================================================================================================*
	New Roles: 
*===============================================================================================================================*/
create role MARKETING_OWNER_SELECT;
create role MARKETING_STAGE_OWNER_SELECT;

/*===============================================================================================================================*
	New Directories: 
*===============================================================================================================================*/

-- You will have to create the local directories first...
CREATE OR REPLACE DIRECTORY MKTG_DATA_DIR AS '/i1/stage/hub/mktg/data';
CREATE OR REPLACE DIRECTORY MKTG_LOG_DIR AS '/i1/stage/hub/mktg/logs';

/*===============================================================================================================================*
	New Users:
*===============================================================================================================================*/

/*
 Peter Burnell commented out the following statements
	MARKETING_STAGE_OWNER
	MARKETING_OWNER
	MARKETING_BORPTRNR
*/

CREATE USER MARKETING_OWNER
  IDENTIFIED BY XXXXXXXXXXXXXXXXXXXX
  DEFAULT TABLESPACE MARKETING_DEFAULT
  TEMPORARY TABLESPACE TEMP
  PROFILE APPLICATION
  ACCOUNT UNLOCK
/

  GRANT CONNECT TO MARKETING_OWNER											;
  GRANT EXPLAIN_PLAN_ROLE TO MARKETING_OWNER                                ;
-- peter burnell commented out  GRANT IFCS_OWNER_SELECT TO MARKETING_OWNER                                ;
-- peter burnell commented out  GRANT IFCS_STAGE_OWNER_SELECT TO MARKETING_OWNER                          ;
  GRANT RESOURCE TO MARKETING_OWNER                                         ;
  GRANT SELECT_CATALOG_ROLE TO MARKETING_OWNER                              ;
  GRANT SQLT_USER_ROLE TO MARKETING_OWNER                                   ;
  GRANT TOAD_SESSION_BROWSER TO MARKETING_OWNER                             ;
  ALTER USER MARKETING_OWNER DEFAULT ROLE ALL                               ;

  GRANT ANALYZE ANY TO MARKETING_OWNER                                      ;
  GRANT CREATE DATABASE LINK TO MARKETING_OWNER                             ;
  GRANT CREATE JOB TO MARKETING_OWNER                                       ;
  GRANT CREATE PROCEDURE TO MARKETING_OWNER                                 ;
  GRANT CREATE PUBLIC DATABASE LINK TO MARKETING_OWNER                      ;
  GRANT CREATE PUBLIC SYNONYM TO MARKETING_OWNER                            ;
  GRANT CREATE SEQUENCE TO MARKETING_OWNER                                  ;
  GRANT CREATE SESSION TO MARKETING_OWNER                                   ;
  GRANT CREATE SYNONYM TO MARKETING_OWNER                                   ;
  GRANT CREATE TABLE TO MARKETING_OWNER                                     ;
  GRANT CREATE TABLESPACE TO MARKETING_OWNER                                ;
  GRANT CREATE VIEW TO MARKETING_OWNER                                      ;
  GRANT SELECT ANY DICTIONARY TO MARKETING_OWNER                            ;
  GRANT UNLIMITED TABLESPACE TO MARKETING_OWNER                             ;
                                                                            
  ALTER USER MARKETING_OWNER QUOTA UNLIMITED ON MARKETING_DEFAULT           ;
                                                                            
  GRANT EXECUTE ON EDW_OWNER.LOG4ME TO MARKETING_OWNER                      ;
-- peter burnell commented out  GRANT SELECT ON LINK_OWNER.T_SALES_OPPORTUNITY TO MARKETING_OWNER         ;
                                                                            
  GRANT EXECUTE ON SYS.DBMS_EXPORT_EXTENSION TO MARKETING_OWNER             ;
  GRANT EXECUTE ON SYS.DBMS_JOB TO MARKETING_OWNER                          ;
  GRANT EXECUTE ON SYS.DBMS_LOB TO MARKETING_OWNER                          ;
  GRANT EXECUTE ON SYS.DBMS_STATS TO MARKETING_OWNER                        ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_DATA_DIR TO MARKETING_OWNER   ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_LOG_DIR TO MARKETING_OWNER    ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_DATA_DIR TO MARKETING_OWNER  ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_LOG_DIR TO MARKETING_OWNER   ;
  GRANT EXECUTE ON SYS.UTL_FILE TO MARKETING_OWNER                          ;
  GRANT EXECUTE ON SYS.UTL_SMTP TO MARKETING_OWNER                          ;
  GRANT EXECUTE ON SYS.UTL_TCP TO MARKETING_OWNER                           ;
  GRANT EXECUTE ON WEX_UTIL.KILL_MY_SESSION TO MARKETING_OWNER              ;


CREATE USER MARKETING_STAGE_OWNER
  IDENTIFIED BY xxxxxxxxxxxxxxxxxxxxxxxxxx
  DEFAULT TABLESPACE MARKETING_DEFAULT
  TEMPORARY TABLESPACE TEMP
  PROFILE APPLICATION
  ACCOUNT UNLOCK
/
  -- 7 Roles for MARKETING_STAGE_OWNER 
  GRANT CONNECT TO MARKETING_STAGE_OWNER										;
-- peter burnell commented out    GRANT IFCS_OWNER_SELECT TO MARKETING_STAGE_OWNER                              ;
-- peter burnell commented out    GRANT IFCS_STAGE_OWNER_SELECT TO MARKETING_STAGE_OWNER                        ;
  GRANT RESOURCE TO MARKETING_STAGE_OWNER                                       ;
  GRANT SELECT_CATALOG_ROLE TO MARKETING_STAGE_OWNER                            ;
  GRANT SQLT_USER_ROLE TO MARKETING_STAGE_OWNER                                 ;
  GRANT TOAD_SESSION_BROWSER TO MARKETING_STAGE_OWNER                           ;
  ALTER USER MARKETING_STAGE_OWNER DEFAULT ROLE ALL                             ;
 
  GRANT ANALYZE ANY TO MARKETING_STAGE_OWNER                                    ;
  GRANT CREATE DATABASE LINK TO MARKETING_STAGE_OWNER                           ;
  GRANT CREATE JOB TO MARKETING_STAGE_OWNER                                     ;
  GRANT CREATE PROCEDURE TO MARKETING_STAGE_OWNER                               ;
  GRANT CREATE PUBLIC DATABASE LINK TO MARKETING_STAGE_OWNER                    ;
  GRANT CREATE PUBLIC SYNONYM TO MARKETING_STAGE_OWNER                          ;
  GRANT CREATE SEQUENCE TO MARKETING_STAGE_OWNER                                ;
  GRANT CREATE SESSION TO MARKETING_STAGE_OWNER                                 ;
  GRANT CREATE SYNONYM TO MARKETING_STAGE_OWNER                                 ;
  GRANT CREATE TABLE TO MARKETING_STAGE_OWNER                                   ;
  GRANT CREATE TABLESPACE TO MARKETING_STAGE_OWNER                              ;
  GRANT CREATE VIEW TO MARKETING_STAGE_OWNER                                    ;
  GRANT SELECT ANY DICTIONARY TO MARKETING_STAGE_OWNER                          ;
  GRANT UNLIMITED TABLESPACE TO MARKETING_STAGE_OWNER                           ;
                    
  ALTER USER MARKETING_STAGE_OWNER QUOTA UNLIMITED ON MARKETING_DEFAULT         ;
             
-- peter burnell commented out    GRANT SELECT ON LINK_OWNER.T_SALES_OPPORTUNITY TO MARKETING_STAGE_OWNER       ;
  GRANT EXECUTE ON SYS.DBMS_EXPORT_EXTENSION TO MARKETING_STAGE_OWNER           ;
  GRANT EXECUTE ON SYS.DBMS_JOB TO MARKETING_STAGE_OWNER                        ;
  GRANT EXECUTE ON SYS.DBMS_LOB TO MARKETING_STAGE_OWNER                        ;
  GRANT EXECUTE ON SYS.DBMS_STATS TO MARKETING_STAGE_OWNER                      ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_DATA_DIR TO MARKETING_STAGE_OWNER ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_LOG_DIR TO MARKETING_STAGE_OWNER  ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_DATA_DIR TO MARKETING_STAGE_OWNER;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_LOG_DIR TO MARKETING_STAGE_OWNER ;
  GRANT EXECUTE ON SYS.UTL_FILE TO MARKETING_STAGE_OWNER                        ;
  GRANT EXECUTE ON SYS.UTL_SMTP TO MARKETING_STAGE_OWNER                        ;
  GRANT EXECUTE ON SYS.UTL_TCP TO MARKETING_STAGE_OWNER                         ;
  GRANT EXECUTE ON WEX_UTIL.KILL_MY_SESSION TO MARKETING_STAGE_OWNER            ;


CREATE USER MARKETING_BORPTRNR
  IDENTIFIED BY xxxxxxxxxxxxxxxxxxx
  DEFAULT TABLESPACE MARKETING_DEFAULT
  TEMPORARY TABLESPACE TEMP
  PROFILE APPLICATION
  ACCOUNT UNLOCK
/

  GRANT CONNECT TO MARKETING_BORPTRNR											;
-- peter burnell commented out    GRANT IFCS_OWNER_SELECT TO MARKETING_BORPTRNR                                ;
-- peter burnell commented out    GRANT IFCS_STAGE_OWNER_SELECT TO MARKETING_BORPTRNR                          ;
  GRANT RESOURCE TO MARKETING_BORPTRNR                                         ;
  GRANT SELECT_CATALOG_ROLE TO MARKETING_BORPTRNR                              ;
  GRANT SQLT_USER_ROLE TO MARKETING_BORPTRNR                                   ;
  GRANT TOAD_SESSION_BROWSER TO MARKETING_BORPTRNR                             ;
  ALTER USER MARKETING_BORPTRNR DEFAULT ROLE ALL                               ;
  -- 14 System Privileges for MARKETING_BORPTRNR                               ;
  GRANT ANALYZE ANY TO MARKETING_BORPTRNR                                      ;
  GRANT CREATE DATABASE LINK TO MARKETING_BORPTRNR                             ;
  GRANT CREATE JOB TO MARKETING_BORPTRNR                                       ;
  GRANT CREATE PROCEDURE TO MARKETING_BORPTRNR                                 ;
  GRANT CREATE PUBLIC DATABASE LINK TO MARKETING_BORPTRNR                      ;
  GRANT CREATE PUBLIC SYNONYM TO MARKETING_BORPTRNR                            ;
  GRANT CREATE SEQUENCE TO MARKETING_BORPTRNR                                  ;
  GRANT CREATE SESSION TO MARKETING_BORPTRNR                                   ;
  GRANT CREATE SYNONYM TO MARKETING_BORPTRNR                                   ;
  GRANT CREATE TABLE TO MARKETING_BORPTRNR                                     ;
  GRANT CREATE TABLESPACE TO MARKETING_BORPTRNR                                ;
  GRANT CREATE VIEW TO MARKETING_BORPTRNR                                      ;
  GRANT SELECT ANY DICTIONARY TO MARKETING_BORPTRNR                            ;
  GRANT UNLIMITED TABLESPACE TO MARKETING_BORPTRNR                             ;
                                
  ALTER USER MARKETING_BORPTRNR QUOTA UNLIMITED ON MARKETING_DEFAULT           ;
    
  GRANT EXECUTE ON SYS.DBMS_EXPORT_EXTENSION TO MARKETING_BORPTRNR             ;
  GRANT EXECUTE ON SYS.DBMS_JOB TO MARKETING_BORPTRNR                          ;
  GRANT EXECUTE ON SYS.DBMS_LOB TO MARKETING_BORPTRNR                          ;
  GRANT EXECUTE ON SYS.DBMS_STATS TO MARKETING_BORPTRNR                        ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_DATA_DIR TO MARKETING_BORPTRNR   ;
  GRANT EXECUTE, READ, WRITE ON DIRECTORY EDM_LOG_DIR TO MARKETING_BORPTRNR    ;
  GRANT EXECUTE ON SYS.UTL_FILE TO MARKETING_BORPTRNR                          ;
  GRANT EXECUTE ON SYS.UTL_SMTP TO MARKETING_BORPTRNR                          ;
  GRANT EXECUTE ON SYS.UTL_TCP TO MARKETING_BORPTRNR                           ;
  GRANT EXECUTE ON WEX_UTIL.KILL_MY_SESSION TO MARKETING_BORPTRNR              ;

/* 
     Peter Burnell Added  04/20/16
*/
GRANT MARKETING_OWNER_SELECT TO DWLOADER;
GRANT MARKETING_OWNER_SELECT TO MARKETING_STAGE_OWNER;
GRANT MARKETING_OWNER_SELECT TO HUBLOADER;
--GRANT MARKETING_OWNER_SELECT TO USER_LDAP ;
GRANT MARKETING_OWNER_SELECT TO MARKETING_BORPTRNR  ;

GRANT MARKETING_STAGE_OWNER_SELECT TO DWLOADER;
GRANT MARKETING_STAGE_OWNER_SELECT TO MARKETING_STAGE_OWNER;
GRANT MARKETING_STAGE_OWNER_SELECT TO HUBLOADER;
--GRANT MARKETING_STAGE_OWNER_SELECT TO USER_LDAP ;
GRANT MARKETING_STAGE_OWNER_SELECT TO MARKETING_BORPTRNR;

-- Added on 7/20/2016 Per DBA's
GRANT MARKETING_OWNER_SELECT       TO ALL_SELECT_ROLES;
GRANT MARKETING_STAGE_OWNER_SELECT TO ALL_SELECT_ROLES;

/*
    Peter Burnell Added 05/20/16
*/
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_LOG_DIR TO DWLOADER;
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_LOG_DIR TO MARKETING_OWNER_SELECT;
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_LOG_DIR TO MARKETING_STAGE_OWNER_SELECT;
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_DATA_DIR TO DWLOADER;
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_DATA_DIR TO MARKETING_OWNER_SELECT;
GRANT EXECUTE, READ, WRITE ON DIRECTORY MKTG_DATA_DIR TO MARKETING_STAGE_OWNER_SELECT;