SET ECHO      ON
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET TIMING ON

SPOOL /i1/&env/hub/logs/MARKETING_LOAD.proc;
 
CREATE OR REPLACE PROCEDURE MARKETING_LOAD (tableIds IN VARCHAR2)
AS
/*******************************************************************************
  Name       :  Peter Burnell
  Description:  Procedure to load Marketing Schema tables.
  Dependency :  log4me package    
  Revision   :  v1.0 - 03/2016
*******************************************************************************/
--Global  Constants
  SESSION_TYPE CONSTANT VARCHAR2(40) := 'MARKETING_LOAD Process:';
  SESSION_FMT  CONSTANT VARCHAR2(40) := 'YYYYMMDD.HH24MISS';
  FMT_DT       CONSTANT VARCHAR2(20) := 'YYYY-MM-DD';
  SESSION_ID   CONSTANT VARCHAR2(40) := to_char(SYSTIMESTAMP,SESSION_FMT) ||'.1'
  ;  
  
--GLOBAL Variables  
  event_msg    VARCHAR2(200)
  ;  

--Variables
  Odate        VARCHAR2(10);
  FileNm       VARCHAR2(200);
  StartTableId INTEGER;
  EndTableId   INTEGER;
  SourceTbl    VARCHAR2(100);
  TargetTbl    VARCHAR2(100);
  shrinksql    varchar2(200)
  ;
  
-- Cursor:  Find the names of the Marketing Tables to load  
  CURSOR marketing_tables (StartTableId_in INTEGER, EndTableId_in INTEGER)
      IS
  SELECT table_id,
         source_table_name,
         target_table_name,
         primary_key,
         upsertdttm_flg,
         shrink_flg
    FROM marketing_stage_owner.marketing_tables
   WHERE table_id between StartTableId_in and EndTableId_in
  ;

-- Cursor:  Find the tables to shrink
  CURSOR shrink_tables 
      IS
  SELECT target_table_name
    FROM marketing_stage_owner.marketing_tables
   WHERE shrink_flg='Y'
  ;  
-- Local Procedure 
  PROCEDURE PROCESS_TABLE (TABLEID_IN    INTEGER,
                           SOURCETBL_IN  VARCHAR2,
                           TARGETTBL_IN  VARCHAR2,
                           PKEY_IN       VARCHAR2,
                           UPSERT_FLG_IN VARCHAR2,
                           ODATE_IN      VARCHAR2,
                           FILENM_IN     VARCHAR2,
                           SHRINK_FLG_IN VARCHAR2)
  AS      
    StartDttm    DATE; 
    SqlStmt      VARCHAR2(2000);
    Cnt          NUMBER;
    Dupcnt       NUMBER;
    OverallCnt   NUMBER;
    DeleteCnt    NUMBER;
    RemoveCnt    NUMBER;
    CreateCnt    NUMBER;
    IgnoreCnt    NUMBER;
    FileId       INTEGER;
    Str          VARCHAR2(2000);
    part_1       VARCHAR2(100);            
 
  BEGIN 
     event_msg:= 'Populating Table: '||TABLEID_IN||'-'||TARGETTBL_IN;
     edw_owner.log4me.info(EVENT_MSG, SESSION_ID);
     
     -- log the table to be loaded to the stats table
     SELECT SYSDATE INTO StartDttm FROM DUAL
     ;
     INSERT INTO marketing_stage_owner.marketing_table_load_stats
       (TABLE_ID,
        odate,
        file_nm,
        load_date,
        load_start_dttm
       )
      VALUES
       (TABLEID_IN,
        ODATE_IN,
        FILENM_IN,
        StartDttm,
        SYSDATE
       )
     ;
     COMMIT;
   
     -- find the primary key column
     SELECT  REGEXP_SUBSTR (pkey_in , '[^:]+', 1, 1) 
       INTO  part_1
       FROM  DUAL
     ; 
     STR:='target.'||part_1||'= source.'||part_1;
   
     -- if the table has upserts append a date/time filter
     IF UPSERT_FLG_IN='Y' THEN
        str:=str||' AND target.upsertdttm <= source.upsertdttm';
     END IF
     ;
   
     /*------------------------------------------------------------------------
      1 - Drop the temporary table if it exists, ignore an exception if it doesn't.
      2 - Create the temp table in the form of the source table, index, stats and default createdttm
      3 - Remove rows from target that have newer data in source
      4 - Remove rows in temp that are older than target
      5 - update the createdttm for all rows to be inserted from temp
      6 - insert new rows 
      7 - drop temp table
      8 - Log activity
     ------------------------------------------------------------------------*/
     DeleteCnt:=0;
     IgnoreCnt:=0;
     Cnt:=0;
   
     -- 1
     BEGIN
       EXECUTE IMMEDIATE 'DROP TABLE temp_load' ;
     EXCEPTION 
       WHEN OTHERS THEN
         null; --preventive code for restart, table should not be present normally.
     END
     ;
   
     -- 2 Create Temporary table with default dttm, index for joins, and gather stats
     SqlStmt:=
     'CREATE TABLE temp_load NOLOGGING PARALLEL 32 '||
     'AS SELECT /*+ PARALLEL(source 16)*/ * FROM '||SOURCETBL_IN||' source'  ;  
     EXECUTE IMMEDIATE SqlStmt;
     OVERALLCNT :=SQL%ROWCOUNT;
     COMMIT;   
     
-- Check if any records were sent for processing     
     IF OVERALLCNT <> 0 THEN
   
       sqlStmt:= 'CREATE INDEX temp_load_idx ON temp_load ('||part_1||') NOLOGGING' ;
       EXECUTE IMMEDIATE sqlStmt;
     
       DBMS_STATS.GATHER_TABLE_STATS (ownname => 'MARKETING_OWNER' , 
                                          tabname  => 'temp_load',
                                          cascade  => true, 
                                          estimate_percent => 10,
                                          method_opt       =>'for all indexed columns size 1',
                                          granularity      => 'ALL',
                                          degree           => 16);
                 
       -- check for any duplicate PK's
       sqlStmt:='SELECT COUNT(1) FROM '||TARGETTBL_IN||' target,  temp_load source '||
                  'WHERE '||'target.'||part_1||'= source.'||part_1; 
       dbms_output.put_line('dupcheck: '||SqlStmt);
       EXECUTE IMMEDIATE SqlStmt
       INTO DUPCNT;
   
       -- 3 
       IF DUPCNT <> 0 THEN
         SqlStmt:=
          'DELETE /*+ PARALLEL(target 16)*/ FROM '||TARGETTBL_IN||' target '||
          'WHERE EXISTS( SELECT /*+ PARALLEL(source 16)*/ 1 FROM temp_load source '||
                       'WHERE '||Str||')' ;
         EXECUTE IMMEDIATE SqlStmt;
         DELETECNT :=SQL%ROWCOUNT;
   
         -- 4
         IF UPSERT_FLG_IN='Y' THEN
           SqlStmt:=
           'DELETE FROM temp_load  target  
             WHERE EXISTS ( SELECT /*+ PARALLEL(source 16)*/ 1 FROM '||TARGETTBL_IN||' source '||
                            'WHERE '||Str||')' ; 
           EXECUTE IMMEDIATE sqlStmt;
           ignorecnt :=SQL%ROWCOUNT;
         END IF; --upsertdttm_flg
       END IF --dupcnt
       ;
       -- 5
       SqlStmt:=
       'UPDATE /*+ PARALLEL(temp_load 32)*/ temp_load
           SET ROW_CREATE_DTTM = SYSTIMESTAMP';
       EXECUTE IMMEDIATE SqlStmt;     
   
       -- 6
       SqlStmt:=
       'INSERT /*+ append */  INTO '||TARGETTBL_IN||' target '||
       'SELECT /*+ parallel (source,32) */ *
          FROM temp_load source'         ;
       EXECUTE IMMEDIATE SqlStmt;
       cnt :=SQL%ROWCOUNT;
   
       -- 7 
       EXECUTE IMMEDIATE 'DROP TABLE temp_load';
       
     END IF; --overallcnt <> 0
   
     -- 8
     CreateCnt:=Cnt-DeleteCnt;
     UPDATE marketing_stage_owner.marketing_table_load_stats
        SET tot_processed   = OverallCnt,
            tot_inserted    = CreateCnt,
            tot_updated     = DeleteCnt,
            tot_ignored     = IgnoreCnt,
            load_end_dttm   = SYSDATE
     WHERE table_id  = TABLEID_IN
       AND load_date = startdttm
     ;
     COMMIT;  
  END 
  ;
/*******************************************************************************
*******************************************************************************/  
BEGIN
   edw_owner.log4me.info('INIT - ' || SESSION_TYPE, SESSION_ID);
   
   SELECT odate, file_nm
     INTO Odate, FileNm       
     FROM marketing_stage_owner.file_nm;

   -- Find tables to load, range is denoted by -
   SELECT  NVL(to_number(REGEXP_SUBSTR (tableIds, '[^-]+', 1, 1)),0),  
           NVL(to_number(REGEXP_SUBSTR (tableIds, '[^-]+', 1, 2)),0)   
     INTO StartTableId,EndTableId
     FROM DUAL
   ;
   IF endTableId = 0 THEN -- no range was given
      endTableID:= startTableId;
   END IF;
   
   -- Check if zip file was sent(='NONE') and if files were in it to be loaded(=0)
   IF FileNm = 'NONE' OR StartTableID = 0 THEN
     edw_owner.log4me.info('No Data - '||Odate||' '||SESSION_TYPE, SESSION_ID);
   ELSE
     FOR mkt_tbl IN marketing_tables (StartTableId,EndTableId)
     LOOP  
     sourceTbl:='MARKETING_STAGE_OWNER.'||mkt_tbl.source_table_name;
     targetTbl:='MARKETING_OWNER.'      ||mkt_tbl.target_table_name;
     
     PROCESS_TABLE(mkt_tbl.table_id,
                   sourceTbl,
                   targetTbl,
                   mkt_tbl.primary_key,
                   mkt_tbl.upsertdttm_flg,
                   Odate,
                   FileNm,
                   mkt_tbl.shrink_flg)
     ;
     END LOOP
   ;
   END IF; -- File Name found
   
  --  Shrink tables as needed based upon activity
   FOR shrink_rec in shrink_tables
   LOOP
         shrinksql:=
         'ALTER TABLE '||shrink_rec.target_table_name||' SHRINK SPACE';
         EXECUTE IMMEDIATE shrinksql;
   END LOOP
   ;
   
   edw_owner.log4me.info('Completed - ' || SESSION_TYPE, SESSION_ID);
EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    edw_owner.Log4me.err('FAIL ' || SQLCODE || ' ' || SESSION_TYPE, SESSION_ID);
    RAISE;
END MARKETING_LOAD;
/
GRANT EXECUTE ON MARKETING_OWNER.MARKETING_LOAD TO DWLOADER;
CREATE OR REPLACE PUBLIC SYNONYM  MARKETING_LOAD  FOR MARKETING_OWNER.MARKETING_LOAD;

SPOOL OFF;