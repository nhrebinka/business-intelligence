/*
REM   Filename : cr_priv_synonym.sql		
REM 
REM
REM  Description: Generates script which creates private synonyms for
REM    	          all tables in  EDM_OWNER,  STAGE_OWNER 
REM 
REM  Log: Created:  06/16/2005   
REM  log        :  
REM             modified for HUB on 09/24/2010 by Venu
REM  
REM**********************************************************************/  



set heading off ;
set feedback off ; 
set linesize 180 ; 
set time off
set timing off
set echo off 

spool /i1/&&env/hub/logs/priv_synonym.sql ;

select  'create or replace synonym ' || uo.object_name || 
 ' ' || '  for ' ||uo.owner||'.'||uo.object_name || ';' 
from all_objects  uo , user_users uu 
 where uo.object_type in ('TABLE','VIEW', 'PROCEDURE' , 'FUNCTION' , 'SEQUENCE'  )
and uo.owner =  '&grant_select_account' ;
/

spool off ; 

set linesize 80 ;
set feedback on ; 
set heading on  ; 
set echo on ;

spool /i1/&&env/hub/logs/priv_synonym.log;

start /i1/&&env/hub/logs/priv_synonym.sql;

spool off
