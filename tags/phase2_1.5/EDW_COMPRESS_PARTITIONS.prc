CREATE OR REPLACE PROCEDURE EDW_OWNER.EDW_compress_partitions 
AS
   /*
    NAME        :-    WB_COMPRESS_PARTITIONS
    AUTHOUR     :-    Venugopal KAncharla
    DATE        :-    03/04/2009
    DESCRIPTION :-    Compress  paritions in a database it handles the existing types of range paritions
    LOG         : -   VERSION 1.0
                   :- Added In EDW for Performance in 07/21/2011

   */
   v_high_value                            VARCHAR2 (32767);
   v_high_date                              DATE;
   v_date                                      DATE;
   v_sql                                        VARCHAR2 (4000);
   v_sql1                                       VARCHAR2 (4000);
   v_sql_bit_map_indexes               VARCHAR2 (4000);
   v_sql_bit_map_part_indexes        VARCHAR2 (4000);
   c_process_code                          VARCHAR2 (100) := 'EDW_COMPRESS_PARTITIONS';
   p_month                                    number := 1 ; 

   CURSOR cpartitions
   IS
      (SELECT table_owner,
              table_name,
              partition_name,
              high_value,
              tablespace_name,
              b.object_type,
              b.max_part_cols
         FROM all_tab_partitions atp,
              (  SELECT owner,
                        NAME,
                        object_type,
                        MAX (column_position) max_part_cols
                   FROM all_part_key_columns
                  WHERE object_type = 'TABLE'
               GROUP BY owner, NAME, object_type) b
        WHERE     atp.table_owner = b.owner
              AND atp.table_name = b.NAME
              AND compression = 'DISABLED'
              AND partition_name <> 'MAX_VALUE'
              AND PARTITION_NAME LIKE 'YM%');


   CURSOR cbitmap_indexes
   IS
      (SELECT DISTINCT INDEX_NAME
         FROM (SELECT AI.OWNER AS OWNER,
                      AI.INDEX_NAME AS INDEX_NAME,
                      ATP.PARTITION_NAME AS PARTITION_NAME
                 FROM ALL_INDEXES AI, ALL_TAB_PARTITIONS ATP
                WHERE     AI.TABLE_NAME = ATP.TABLE_NAME
                      AND AI.OWNER = 'EDW_OWNER'
                      AND AI.INDEX_TYPE = 'BITMAP'));

   CURSOR cbitmap_part_indexes
   IS
      (SELECT AI.OWNER AS OWNER,
              AI.INDEX_NAME AS INDEX_NAME,
              ATP.PARTITION_NAME AS PARTITION_NAME
         FROM ALL_INDEXES AI, ALL_TAB_PARTITIONS ATP
        WHERE     AI.TABLE_NAME = ATP.TABLE_NAME
              AND AI.OWNER = 'EDW_OWNER'
              AND AI.INDEX_TYPE = 'BITMAP');
BEGIN
   --wb_log.process (c_process_code, 'START');

   --COMMIT;

   DBMS_APPLICATION_INFO.set_module (
      module_name   => c_process_code,
      action_name   => 'SET BIT MAP INDEXES UNUSABLE');
   DBMS_APPLICATION_INFO.set_action (action_name => '');
   DBMS_APPLICATION_INFO.set_action (
      action_name => 'STARTING SET BIT MAP INDEXES UNUSABLE');


   -- Setting the BIT-MAP INDEXES   UNusable ...

   FOR rbitmap_indexes IN cbitmap_indexes
   LOOP
      v_sql_bit_map_indexes :=
         ' ALTER   INDEX ' || rbitmap_indexes.index_name || ' UNUSABLE ';

      DBMS_OUTPUT.put_line (v_sql_bit_map_indexes);

      EXECUTE IMMEDIATE v_sql_bit_map_indexes;
   END LOOP;

   --- PROC  FOR COMPRESSING PARTITIONS

   DBMS_APPLICATION_INFO.set_module (
      module_name   => c_process_code,
      action_name   => 'COMPRESSING PARTITIONS');
   DBMS_APPLICATION_INFO.set_action (action_name => '');
   DBMS_APPLICATION_INFO.set_action (
      action_name => 'STARTING PARTITION COMRESSION FOR DATE RANGE PARTITIONS');


   FOR rpartitions IN cpartitions
   LOOP
      --  v_high_value := rpartitions.high_value;

      DBMS_OUTPUT.put_line ('HIGH_VALUE ' || rpartitions.high_value);

      CASE
         WHEN rpartitions.max_part_cols = 1
              AND rpartitions.high_value IS NOT NULL -- options to process different types of range partitions
         THEN
            v_sql1 :=
                  'SELECT  TRUNC('
               || rpartitions.high_value
               || ')'
               || ' FROM DUAL ';
         WHEN rpartitions.max_part_cols = 2
              AND rpartitions.high_value IS NOT NULL -- to process YYYY , MM  range partition ....
         THEN
            v_sql1 :=
                  'SELECT  TO_DATE('''
               || rpartitions.high_value
               || ''',''YYYY,MM'')'
               || ' FROM DUAL ';
         WHEN rpartitions.high_value IS NULL
         THEN
            v_sql1 :=
                  'SELECT TO_DATE(SUBSTR('
               || ''''
               || rpartitions.partition_name
               || ''''
               || ',4 )'
               || ' ,'
               || '''YYYY_MM'''
               || ' ) '
               || ' FROM DUAL ';
      END CASE;

      DBMS_OUTPUT.put_line (v_sql1);

      EXECUTE IMMEDIATE v_sql1 INTO v_high_date;

      DBMS_OUTPUT.put_line (v_high_date);


      IF ADD_MONTHS (v_high_date, p_month) <= TRUNC (SYSDATE)
      THEN
         BEGIN
            v_sql :=
                  ' ALTER TABLE "'
               || rpartitions.table_owner
               || '"."'
               || rpartitions.table_name
               || '" MOVE PARTITION "'
               || rpartitions.partition_name
               || '" COMPRESS UPDATE INDEXES PARALLEL 12  ';

            DBMS_OUTPUT.put_line (v_sql);

            EXECUTE IMMEDIATE v_sql;
         END;
      END IF;
   END LOOP;

   DBMS_APPLICATION_INFO.set_action (action_name => '');
   DBMS_APPLICATION_INFO.set_action (
      action_name => 'END OF PARTITION COMPRESS');
   DBMS_APPLICATION_INFO.set_action (action_name => '');
   DBMS_APPLICATION_INFO.set_module ('', '');

   --wb_log.process (c_process_code, 'END');


   -- REBUILIDING BIT MAP INDEXES BY PARTITION

   FOR rbitmap_part_indexes IN cbitmap_part_indexes
   LOOP
      v_sql_bit_map_part_indexes :=
            ' ALTER   INDEX '
         || rbitmap_part_indexes.index_name
         || ' REBUILD  PARTITION  '
         || rbitmap_part_indexes.partition_name
         || ' PARALLEL 12 ' ;

      DBMS_OUTPUT.put_line (v_sql_bit_map_part_indexes);

      EXECUTE IMMEDIATE v_sql_bit_map_part_indexes;
   END LOOP;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_APPLICATION_INFO.set_action (action_name => '');
      DBMS_APPLICATION_INFO.set_module ('', '');
      DBMS_OUTPUT.put_line (SQLCODE || SQLERRM);
      --email.send_mail ('WB_COMPRESS_PARTITIONS');
      RAISE;
END Edw_compress_partitions;
/