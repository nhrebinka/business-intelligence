SET ECHO ON
SET SERVEROUTPUT  ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/logs/dba_run_m_purch_device.log ;


/* Formatted on 5/19/2011 2:30:30 PM (QP5 v5.163.1008.3004) */
MERGE INTO M_PURCH_DEVICE A 
     USING (SELECT MPD.EMBOSS_PD_NBR
              FROM M_PURCH_DEVICE MPD, M_PURCH_DEVICE_XREF MPDX
             WHERE     MPDX.EDM_PURCH_DEVICE_KEY = MPD.EDM_PURCH_DEVICE_KEY
                   AND MPDX.DW_SOURCE_SYS = 'TANDEM'
                   AND MPD.EMBOSS_PD_NBR LIKE '0464%'
                   AND MPD.PD_STS = 'Active'
            MINUS
            (SELECT MPD.EMBOSS_PD_NBR
               FROM M_PURCH_DEVICE MPD, M_PURCH_DEVICE_XREF MPDX
              WHERE     MPDX.EDM_PURCH_DEVICE_KEY = MPD.EDM_PURCH_DEVICE_KEY
                    AND MPDX.DW_SOURCE_SYS <> 'TANDEM'
                    AND MPD.EMBOSS_PD_NBR LIKE '0464%' ) ) B 
 ON 
    (   A.EMBOSS_PD_NBR    = B.EMBOSS_PD_NBR ) 
 WHEN MATCHED 
  THEN UPDATE   SET 
                             A.PD_SETUP_DT   = '01-JAN-2007'  ,  
                             A.PD_STS            = 'Terminated'    , 
                             A.EDM_LAST_UPDT_SESSION_NM   = 'Sinclair Data Fix  To Terminate Tandem Only Cards'  ; 
    
                      
 commit ; 

spool off;





             
             
      
     
     
 



 