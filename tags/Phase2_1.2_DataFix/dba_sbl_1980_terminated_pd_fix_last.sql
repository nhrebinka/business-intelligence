SET ECHO ON
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET LINESIZE 2000

spool /i1/&&env/hub/logs/dba_sbl_1980_terminated_pd_fix_last.log  ;

-- this is done to terminate the  cards which are the Same Day update and Deletes ... 
-- This should be run only after the ETL has been run from M to D 


UPDATE D_PURCHASE_DEVICE DPD
SET 
     DPD.ROW_LAST_MOD_DTTM      = sysdate ,
     DPD.ROW_LAST_MOD_PROC_NM   = 'Data fix For SBL 1980 Dates',
     DPD.TERMINATED_DATE   = DPD.ISSUED_DATE  , 
     DPD.CARD_STATUS_DATE  = DPD.ISSUED_DATE 
WHERE
         DPD.CARD_STATUS_DESC = 'Terminated' 
   and   DPD.TERMINATED_DATE  =  '01-JAN-1980'
   and   DPD.EMBOSSED_CARD_NUMBER_ID NOT LIKE '63%' ; 

COMMIT ; 

spool OFF ; 
    
    
   
    
     