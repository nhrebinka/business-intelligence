CREATE OR REPLACE PROCEDURE  F_MONTHLY_ACCOUNT_AGGREGATES
AS
   /*
   NAME                    :-     F_Monmthly_ACCOUNT_AGGREGATES
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    06/2010
   DECRIPTION :-   PROCEURE TO  load daily Account Aggregates  into F_monthly_ACCOUNT_AGGREGATES
   LOG            : -   VERSION 1.0
                     :- Version 2.0 Modified By Venugopal Kancharla
                       DAte :- 10/11/2010
                     Added the F_SEED_DATES procedure to autoamte the history  laoding
                     and added the  indexes Drop and Recreate ...
                     and   added the dynamic Merge statement   to Tune Izzy's Sql :-)
                     
                     :- Version 2.1  for  Changes for Daily run -- Venu Kancharla 12/12/2010 
                     
                     :- version 2.2 for Hash Join daily runs - -Venu Kancharla 01/17/2010
                     
                     :- Version 2.3 Backed out the HASH JOIN hints ...Venu Kancharla 01/20/2010
                     
                     :- version 2.4 Added the D_SEED_DATES PROC .. for Daily Processing
                     and the PURCHSE DEVICE EVENT tabkles in the Merge statement  
                     
                      by Venu Kancharla on 03/31/2011 

/* Note :- the Start Date and END DATE input paramters always needs to be the Calendar Start date and Calendar END Date ...
even if we are trying to  calculate the Aggrgates for a Partial Month .
Note Added by Venu  Kancharla 11/03/2010

*/

   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;

   V_SQL                 VARCHAR2 (4000);

   V_SQL1                VARCHAR2 (4000);

   V_PART                VARCHAR2 (10);
BEGIN
   -- 11/09/2010   Following block added by Venu to make this procedure data driven ...
   -- and to improve performance

/*   V_SQL := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI1';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI2';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI3';

   EXECUTE IMMEDIATE V_SQL;
   
   */

   D_SEED_DATES (v_calendar_dt_start, v_calendar_dt_end);

   v_calendar_dt_end := LAST_DAY (v_calendar_dt_end); -- refer to the note above



   DBMS_OUTPUT.PUT_LINE (V_CALENDAR_DT_END);



   -- ADDED 11/2/2010 by Izzy to allow proc to be run acroos multi months for historic loads
   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT - D.DAY_IN_MONTH_NUMBER + 1                 AS FIRST_OF_MONTH,
                 D.CALENDAR_DATE_DT                                                                        AS LAST_OF_MONTH,
                 D.DATE_KEY                                                                                        AS DATE_KEY
            FROM 
                       D_DATE D
           WHERE 
                      D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start   AND v_calendar_dt_end
               AND D.LAST_DAY_IN_MONTH_FLG = 'Yes')
   LOOP
   
    -- Here there should be a Delete statement to Delete the Record for that Month ( Added By Venu this Compatible for Daily Run  



       MERGE    INTO F_MONTHLY_ACCOUNT_SNAPSHOT MAS 
       USING (SELECT   
                               --MON_ACCOUNT_SNAPSHOT_KEY_SEQ.NEXTVAL          MON_ACCOUNT_SNAPSHOT_KEY       ,
                               SNAPS.ACCOUNT_KEY                                                ACCOUNT_KEY                                         ,
                                SNAPS.PROGRAM_KEY                                              PROGRAM_KEY                                          ,
                                 LDOM.DATE_KEY                                                      DATE_KEY                                                ,
                                SNAPS.OUTSTANDING_CARDS_COUNT                        OUTSTANDING_CARDS_COUNT                 ,
                                SNAPS.TERMINAL_CARD_COUNT                                 TERMINAL_CARD_COUNT                          ,
                                SNAPS.TOTAL_CARDS_COUNT                                    TOTAL_CARDS_COUNT                              ,
                                SNAPS.CARDS_IN_TRANSITION_COUNT                       CARDS_IN_TRANSITION_COUNT                 ,
                                SNAPS.VIRTUAL_OUTSTANDING_CARD_COUNT             VIRTUAL_OUTSTANDING_CARD_COUNT      ,
                                SNAPS.VIRTUAL_TERMINAL_CARD_COUNT                   VIRTUAL_TERMINAL_CARD_COUNT             ,
                                SNAPS.VIRTUAL_TOTAL_CARD_COUNT                        VIRTUAL_TOTAL_CARD_COUNT                  ,
                                SNAPS.VIRTUAL_CARDS_IN_TRANS_COUNT                 VIRTUAL_CARDS_IN_TRANS_COUNT           ,
                                SNAPS.MAG_OUTSTANDING_CARD_COUNT                   MAG_OUTSTANDING_CARD_COUNT            ,
                                SNAPS.MAG_TERMINAL_CARD_COUNT                         MAG_TERMINAL_CARD_COUNT                   ,
                                SNAPS.MAG_TOTAL_CARD_COUNT                              MAG_TOTAL_CARD_COUNT                        ,
                                SNAPS.MAG_CARDS_IN_TRANS_COUNT                       MAG_CARDS_IN_TRANS_COUNT                 ,
                                 SUMS.ISSUED_CARDS_COUNT                                   ISSUED_CARDS_COUNT                             ,
                                 SUMS.TERMINATED_CARDS_COUNT                          TERMINATED_CARDS_COUNT                      ,
                                 SUMS.TRANS_CARDS_COUNT                                    TRANS_CARDS_COUNT                              ,                            
                                 SUMS.VIRTUAL_ISSUED_CARDS_COUNT                     VIRTUAL_ISSUED_CARDS_COUNT               ,
                                 SUMS.VIRTUAL_TERMINATED_CARDS_COUNT             VIRTUAL_TERMINATED_CARDS_COUNT       ,
                                 SUMS.VIRTUAL_TRANS_CARDS_COUNT                      VIRTUAL_TRANS_CARDS_COUNT                ,
                                 SUMS.MAG_ISSUED_CARDS_COUNT                            MAG_ISSUED_CARDS_COUNT                     ,
                                 SUMS.MAG_TERMINATED_CARDS_COUNT                    MAG_TERMINATED_CARDS_COUNT             ,
                                 SUMS.MAG_TRANS_CARD_COUNT                               MAG_TRANS_CARD_COUNT                        ,
                                  0                                                                             ACTIVE_CARD_COUNT                               ,
                           SYSDATE                                                                       ROW_CREATE_DTTM                               ,
                           SYSDATE                                                                      ROW_LAST_MOD_DTTM                         ,
                           'F_MONTHLY_ACCOUNT_AGGREGATES'                            ROW_LAST_MOD_PROC_NM                       ,
                                1                                                                             ROW_LAST_MOD_PROC_SEQ_NBR   
            FROM (  SELECT                          
                           DAS.ACCOUNT_KEY                                                     AS ACCOUNT_KEY,
                           DAS.PROGRAM_KEY                                                     AS PROGRAM_KEY,
                           MAX (D.DATE_KEY)                                                      AS DATE_KEY,
                           D.MONTH_YEAR_NAME                                                AS MONTH_YEAR_NAME,
                           SUM (DAS.ISSUED_CARDS_COUNT)                             AS ISSUED_CARDS_COUNT,
                           SUM (DAS.TERMINATED_CARDS_COUNT)                     AS TERMINATED_CARDS_COUNT,
                           SUM (DAS.TRANS_CARDS_COUNT)                              AS TRANS_CARDS_COUNT,
                           SUM (DAS.VIRTUAL_ISSUED_CARDS_COUNT)               AS VIRTUAL_ISSUED_CARDS_COUNT,
                           SUM (DAS.VIRTUAL_TERMINATED_CARDS_COUNT)       AS VIRTUAL_TERMINATED_CARDS_COUNT,
                           SUM (DAS.VIRTUAL_TRANS_CARDS_COUNT)                AS VIRTUAL_TRANS_CARDS_COUNT,
                           SUM (DAS.MAG_ISSUED_CARDS_COUNT)                     AS MAG_ISSUED_CARDS_COUNT,
                           SUM (DAS.MAG_TERMINATED_CARDS_COUNT)             AS MAG_TERMINATED_CARDS_COUNT,
                           SUM (DAS.MAG_TRANS_CARD_COUNT)                        AS MAG_TRANS_CARD_COUNT
                      FROM 
                                F_DAILY_ACCOUNT_SNAPSHOT DAS,
                                D_DATE D
                     WHERE 
                                  D.DATE_KEY = DAS.DATE_KEY
                           AND D.CALENDAR_DATE_DT BETWEEN REC.FIRST_OF_MONTH    AND REC.LAST_OF_MONTH
                  GROUP BY 
                           DAS.ACCOUNT_KEY,
                           DAS.PROGRAM_KEY,
                           D.MONTH_YEAR_NAME) SUMS
                 JOIN                   /* build snapshot portion of record */
                     (SELECT  
                             DAS.ACCOUNT_KEY                                                   AS ACCOUNT_KEY,
                             DAS.PROGRAM_KEY                                                        AS PROGRAM_KEY,
                             DAS.DATE_KEY                                                               AS DATE_KEY,
                             --LDOM.MONTH_YEAR_NAME AS MONTH_YEAR_NAME,
                             DAS.OUTSTANDING_CARDS_COUNT                                   AS OUTSTANDING_CARDS_COUNT,
                             DAS.TERMINAL_CARD_COUNT                                           AS TERMINAL_CARD_COUNT,
                             DAS.TOTAL_CARDS_COUNT                                              AS TOTAL_CARDS_COUNT,
                             DAS.CARDS_IN_TRANSITION_COUNT                                 AS CARDS_IN_TRANSITION_COUNT,
                             DAS.VIRTUAL_OUTSTANDING_CARD_COUNT                      AS VIRTUAL_OUTSTANDING_CARD_COUNT,
                             DAS.VIRTUAL_TERMINAL_CARD_COUNT                             AS VIRTUAL_TERMINAL_CARD_COUNT,
                             DAS.VIRTUAL_TOTAL_CARD_COUNT                                  AS VIRTUAL_TOTAL_CARD_COUNT,
                             DAS.VIRTUAL_CARDS_IN_TRANS_COUNT                           AS VIRTUAL_CARDS_IN_TRANS_COUNT,
                             DAS.MAG_OUTSTANDING_CARD_COUNT                             AS MAG_OUTSTANDING_CARD_COUNT,
                             DAS.MAG_TERMINAL_CARD_COUNT                                   AS MAG_TERMINAL_CARD_COUNT,
                             DAS.MAG_TOTAL_CARD_COUNT                                        AS MAG_TOTAL_CARD_COUNT,
                             DAS.MAG_CARDS_IN_TRANS_COUNT                                 AS MAG_CARDS_IN_TRANS_COUNT
                        FROM    
                                   F_DAILY_ACCOUNT_SNAPSHOT DAS
                           JOIN   (  SELECT DAS.ACCOUNT_KEY     AS ACCOUNT_KEY,
                                              MAX (DAS.DATE_KEY)        AS DATE_KEY
                                         FROM 
                                                    F_DAILY_ACCOUNT_SNAPSHOT DAS,  D_DATE D
                                        WHERE
                                                      D.CALENDAR_DATE_DT BETWEEN REC.FIRST_OF_MONTH AND REC.LAST_OF_MONTH
                                              AND DAS.DATE_KEY  = D.Date_Key
                                     GROUP BY 
                                                  DAS.ACCOUNT_KEY
                                     ) A
                           ON    
                                (         A.DATE_KEY         = DAS.DATE_KEY
                                   AND A.ACCOUNT_KEY   = DAS.ACCOUNT_KEY )
                                ) SNAPS
                    ON 
                          (  SNAPS.ACCOUNT_KEY    = SUMS.ACCOUNT_KEY
                       AND SNAPS.PROGRAM_KEY    = SUMS.PROGRAM_KEY) 
                 JOIN (SELECT D.MONTH_YEAR_NAME, 
                                     D.DATE_KEY AS DATE_KEY
                         FROM 
                                 D_DATE D) D1
                    ON 
                           D1.DATE_KEY = SNAPS.DATE_KEY
                 JOIN
                   (SELECT D.DATE_KEY                AS DATE_KEY,
                                D.MONTH_YEAR_NAME AS MONTH_YEAR_NAME
                         FROM 
                                  D_DATE D
                        WHERE 
                                  D.LAST_DAY_IN_MONTH_FLG = 'Yes') LDOM
                    ON 
                           D1.MONTH_YEAR_NAME = LDOM.MONTH_YEAR_NAME)  AB 
           ON (  MAS.ACCOUNT_KEY = AB.ACCOUNT_KEY 
                  AND MAS.DATE_KEY   = AB.DATE_KEY    ) 
    WHEN MATCHED THEN 
    UPDATE 
     SET        --  MAS.ACCOUNT_KEY                                       = AB.ACCOUNT_KEY                                        ,
                     MAS.PROGRAM_KEY                                       = AB.PROGRAM_KEY                                        ,
                  --   MAS.DATE_KEY                                             = AB.DATE_KEY                                                ,
                     MAS.OUTSTANDING_CARDS_COUNT               = AB.OUTSTANDING_CARDS_COUNT                 ,
                     MAS.TERMINAL_CARD_COUNT                        = AB.TERMINAL_CARD_COUNT                         ,
                     MAS.TOTAL_CARDS_COUNT                           = AB.TOTAL_CARDS_COUNT                            ,
                     MAS.CARDS_IN_TRANSITION_COUNT              = AB.CARDS_IN_TRANSITION_COUNT              ,
                     MAS.VIRTUAL_OUTSTANDING_CARD_COUNT    = AB.VIRTUAL_OUTSTANDING_CARD_COUNT    ,
                     MAS.VIRTUAL_TERMINAL_CARD_COUNT           = AB.VIRTUAL_TERMINAL_CARD_COUNT           ,
                     MAS.VIRTUAL_TOTAL_CARD_COUNT                = AB.VIRTUAL_TOTAL_CARD_COUNT               ,
                     MAS.VIRTUAL_CARDS_IN_TRANS_COUNT         = AB.VIRTUAL_CARDS_IN_TRANS_COUNT         ,
                     MAS.MAG_OUTSTANDING_CARD_COUNT           = AB.MAG_OUTSTANDING_CARD_COUNT           ,
                     MAS.MAG_TERMINAL_CARD_COUNT                  = AB.MAG_TERMINAL_CARD_COUNT                ,
                     MAS.MAG_TOTAL_CARD_COUNT                       = AB.MAG_TOTAL_CARD_COUNT                     ,
                     MAS.MAG_CARDS_IN_TRANS_COUNT                = AB.MAG_CARDS_IN_TRANS_COUNT              ,
                     MAS.ISSUED_CARDS_COUNT                            = AB.ISSUED_CARDS_COUNT                          ,
                     MAS.TERMINATED_CARDS_COUNT                    = AB.TERMINATED_CARDS_COUNT                 ,
                     MAS.TRANS_CARDS_COUNT                             = AB.TRANS_CARDS_COUNT                          ,
                     MAS.VIRTUAL_ISSUED_CARDS_COUNT              = AB.VIRTUAL_ISSUED_CARDS_COUNT            ,
                     MAS.VIRTUAL_TERMINATED_CARDS_COUNT      = AB.VIRTUAL_TERMINATED_CARDS_COUNT    ,
                     MAS.VIRTUAL_TRANS_CARDS_COUNT               = AB.VIRTUAL_TRANS_CARDS_COUNT              ,
                     MAS.MAG_ISSUED_CARDS_COUNT                    = AB.MAG_ISSUED_CARDS_COUNT                   ,
                     MAS.MAG_TERMINATED_CARDS_COUNT            = AB.MAG_TERMINATED_CARDS_COUNT          ,
                     MAS.MAG_TRANS_CARD_COUNT                       = AB.MAG_TRANS_CARD_COUNT                   ,
                     MAS.ACTIVE_CARD_COUNT                               = AB.ACTIVE_CARD_COUNT                          ,
                     MAS.ROW_CREATE_DTTM                                 = AB.ROW_CREATE_DTTM                            ,
                     MAS.ROW_LAST_MOD_DTTM                            = AB.ROW_LAST_MOD_DTTM                         ,
                     MAS.ROW_LAST_MOD_PROC_NM                       = AB.ROW_LAST_MOD_PROC_NM                   ,
                     MAS.ROW_LAST_MOD_PROC_SEQ_NBR              = AB.ROW_LAST_MOD_PROC_SEQ_NBR 
   WHEN NOT  MATCHED 
   THEN 
      INSERT  
                 (   MAS.MONTHLY_ACCOUNT_SNAPSHOT_KEY       ,
                     MAS.ACCOUNT_KEY                                        ,
                     MAS.PROGRAM_KEY                                        ,
                     MAS.DATE_KEY                                               ,
                     /* daily cards snapshot */
                     MAS.OUTSTANDING_CARDS_COUNT                ,
                     MAS.TERMINAL_CARD_COUNT                        ,
                     MAS.TOTAL_CARDS_COUNT                           ,
                     MAS.CARDS_IN_TRANSITION_COUNT              ,
                     MAS.VIRTUAL_OUTSTANDING_CARD_COUNT    ,
                     MAS.VIRTUAL_TERMINAL_CARD_COUNT           ,
                     MAS.VIRTUAL_TOTAL_CARD_COUNT                ,
                     MAS.VIRTUAL_CARDS_IN_TRANS_COUNT         ,
                     MAS.MAG_OUTSTANDING_CARD_COUNT          ,
                     MAS.MAG_TERMINAL_CARD_COUNT                ,
                     MAS.MAG_TOTAL_CARD_COUNT                     ,
                     MAS.MAG_CARDS_IN_TRANS_COUNT              ,
                     /* daily card  transactions */
                     MAS.ISSUED_CARDS_COUNT                         ,
                     MAS.TERMINATED_CARDS_COUNT                 ,
                     MAS.TRANS_CARDS_COUNT                          ,
                     MAS.VIRTUAL_ISSUED_CARDS_COUNT           ,
                     MAS.VIRTUAL_TERMINATED_CARDS_COUNT  ,
                     MAS.VIRTUAL_TRANS_CARDS_COUNT           ,
                     MAS.MAG_ISSUED_CARDS_COUNT                ,
                     MAS.MAG_TERMINATED_CARDS_COUNT        ,
                     MAS.MAG_TRANS_CARD_COUNT                   ,
                     MAS.ACTIVE_CARD_COUNT                          ,
                     MAS.ROW_CREATE_DTTM                            ,
                     MAS.ROW_LAST_MOD_DTTM                       ,
                     MAS.ROW_LAST_MOD_PROC_NM                ,
                     MAS.ROW_LAST_MOD_PROC_SEQ_NBR)
      VALUES    
                    (                   MON_ACCOUNT_SNAPSHOT_KEY_SEQ.NEXTVAL  ,        
                                        AB.ACCOUNT_KEY                      ,
                                        AB.PROGRAM_KEY                      ,
                                        AB.DATE_KEY                         ,
                                        AB.OUTSTANDING_CARDS_COUNT          ,
                                        AB.TERMINAL_CARD_COUNT              ,
                                        AB.TOTAL_CARDS_COUNT                ,
                                        AB.CARDS_IN_TRANSITION_COUNT        ,
                                        AB.VIRTUAL_OUTSTANDING_CARD_COUNT   ,
                                        AB.VIRTUAL_TERMINAL_CARD_COUNT      ,
                                        AB.VIRTUAL_TOTAL_CARD_COUNT         ,
                                        AB.VIRTUAL_CARDS_IN_TRANS_COUNT     ,
                                        AB.MAG_OUTSTANDING_CARD_COUNT       ,
                                        AB.MAG_TERMINAL_CARD_COUNT          ,
                                        AB.MAG_TOTAL_CARD_COUNT             ,
                                        AB.MAG_CARDS_IN_TRANS_COUNT         ,
                                        AB.ISSUED_CARDS_COUNT               ,
                                        AB.TERMINATED_CARDS_COUNT           ,
                                        AB.TRANS_CARDS_COUNT                ,
                                        AB.VIRTUAL_ISSUED_CARDS_COUNT       ,
                                        AB.VIRTUAL_TERMINATED_CARDS_COUNT   ,
                                        AB.VIRTUAL_TRANS_CARDS_COUNT        ,
                                        AB.MAG_ISSUED_CARDS_COUNT           ,
                                        AB.MAG_TERMINATED_CARDS_COUNT       ,
                                        AB.MAG_TRANS_CARD_COUNT             ,
                                        AB.ACTIVE_CARD_COUNT                ,
                                        AB.ROW_CREATE_DTTM                  ,
                                        AB.ROW_LAST_MOD_DTTM                ,
                                        AB.ROW_LAST_MOD_PROC_NM             ,
                                        AB.ROW_LAST_MOD_PROC_SEQ_NBR ) ;         

                
         
       
      COMMIT;

      V_PART := 'YM_' || TO_CHAR (REC.LAST_OF_MONTH, 'YYYY_MM');

      DBMS_OUTPUT.PUT_LINE (V_PART);

      -- ADDED BY VENU on  11/02/2010
            

      V_SQL1 :=
            ' MERGE INTO F_MONTHLY_ACCOUNT_SNAPSHOT MAS
               USING
                          (  SELECT a.acct_key           ak, 
                                        COUNT (a.pd_key) PD_cnt
                                FROM 
                                             (SELECT 
                                                            TL.PURCHASE_ACCOUNT_KEY    Acct_key,
                                                            TL.PURCHASE_DEVICE_KEY        PD_key,
                                                            MAX (tl.revenue_date_key)        R_DATE,
                                                            COUNT (*)                               CNT
                                              FROM F_TRANSACTION_LINE_ITEM  partition ('|| V_PART
                                                                                                                     || ') TL ,
                                                          F_PURCHASE_DEVICE_EVENT FDE
                                              WHERE 
                                                          TL.PURCHASE_ACCOUNT_KEY  = FDE.ACCOUNT_KEY
                                                   AND TL.PURCHASE_DEVICE_KEY      = FDE.PURCHASE_DEVICE_KEY
                                                   AND TL.GROSS_SPEND_AMOUNT    <> 0
                                                   AND TL.PURCHASE_DEVICE_KEY     <> 0
                                             GROUP BY 
                                                         TL.PURCHASE_ACCOUNT_KEY,
                                                         TL.PURCHASE_DEVICE_KEY
                                            ) A
                                GROUP BY
                                             a.acct_key
                          ) b
             ON 
                   (MAS.ACCOUNT_KEY = b.ak)
             WHEN MATCHED
            THEN
                     UPDATE SET MAS .ACTIVE_CARD_COUNT = b.pd_cnt
                     WHERE MAS.DATE_KEY = '  || REC.DATE_KEY;

      EXECUTE IMMEDIATE V_SQL1;

      COMMIT;
   END LOOP;


 /*  V_SQL :=
      'CREATE BITMAP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI1 ON F_MONTHLY_ACCOUNT_SNAPSHOT
(ACCOUNT_KEY)
  TABLESPACE I_BI
  NOLOGGING
  COMPUTE STATISTICS';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL :=
      'CREATE BITMAP INDEX  F_MONTHLY_ACCOUNT_SNAPSHOT_UI2 ON F_MONTHLY_ACCOUNT_SNAPSHOT
(PROGRAM_KEY)
  TABLESPACE I_BI
  NOLOGGING
  COMPUTE STATISTICS';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL :=
      'CREATE BITMAP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI3 ON F_MONTHLY_ACCOUNT_SNAPSHOT
(DATE_KEY)
  TABLESPACE I_BI
  NOLOGGING
  COMPUTE STATISTICS ';

   EXECUTE IMMEDIATE V_SQL;
   */
   
END F_MONTHLY_ACCOUNT_AGGREGATES;
/