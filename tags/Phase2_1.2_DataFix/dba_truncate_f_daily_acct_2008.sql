SET ECHO    ON 
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON 
SET HEADING      ON 
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/dba_truncate_f_daily_acct_2008.log ; 
   
EXEC F_DAILY_ACCOUNT_AGGREGATES_H ( '01-JAN-2008' , '30-JUN-2008' ) ; 
EXEC F_DAILY_ACCOUNT_AGGREGATES_H ( '01-JUL-2008' , '31-DEC-2008' ) ; 

EXEC F_MONTHLY_ACCOUNT_AGGREGATES_H ( '01-JAN-2008' , '30-JUN-2008' ) ; 
EXEC F_MONTHLY_ACCOUNT_AGGREGATES_H ( '01-JUL-2008' , '31-DEC-2008' ) ; 
        
spool off ;  