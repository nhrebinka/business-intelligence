CREATE OR REPLACE PROCEDURE F_DAILY_ACCOUNT_AGGREGATES
AS
   /*
   NAME                    :-     F_DAILY_ACCOUNT_AGGREGATES
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    06/2010
   DECRIPTION :-   PROCEURE TO  load daily Account Aggregates  into F_DAILY_ACCOUNT_AGGREGATES
   LOG            : -   VERSION 1.0
   
                     :- Version 2.0 Modified By Venugopal Kancharla
                       DAte :- 10/11/2010
                       Added the F_SEED_DATES procedure to autoamte the history  laoding
                       and added the  indexes Drop and Recreate ...
                       
                  :-  Version 2.1 Modified By Venu KAncharla 
                     Date :- 12/12/2010
                     Adding the Filter in the where Clause 
                       
                       
                 :- version 2,2 
                  commented  DW_Current_FLG = '1'  01/17/2011 by VK since this is not needed            
                  
                  
               :- Version 2.3 
                Added the new Seed Dates to the PROD D_SEED_DATES    By VK on  03/31/2011   
  */


   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;

   V_SQL                 VARCHAR2 (4000);
BEGIN
   -- Seed Procedure and Indexes management  Added By venu on 11/09/2010

   D_SEED_DATES (v_calendar_dt_start, v_calendar_dt_end);

   DBMS_OUTPUT.PUT_LINE (v_calendar_dt_start);
   DBMS_OUTPUT.PUT_LINE (v_calendar_dt_end);

 /*  V_SQL := 'DROP INDEX F_DAILY_ACCOUNT_SNAPSHOT_UI1';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL := 'DROP INDEX F_DAILY_ACCOUNT_SNAPSHOT_UI2';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL := 'DROP INDEX F_DAILY_ACCOUNT_SNAPSHOT_UI3';

   EXECUTE IMMEDIATE V_SQL;

   V_SQL := 'DROP INDEX F_DAILY_ACCOUNT_SNAPSHOT_UI4';

   EXECUTE IMMEDIATE V_SQL;
   */

   --for rec in (select D.CALENDAR_DATE_DT, D.DATE_KEY  from D_DATE D where D.CALENDAR_DATE_DT between '01-Jan-2008' and sysdate - 1) loop
   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT, D.DATE_KEY
            FROM D_DATE D
           WHERE D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start
                                        AND v_calendar_dt_end)
   LOOP
      INSERT INTO F_DAILY_ACCOUNT_SNAPSHOT DAS (
                     DAS.DAILY_ACCOUNT_SNAPSHOT_KEY,
                     DAS.ACCOUNT_KEY,
                     DAS.PROGRAM_KEY,
                     DAS.DATE_KEY,
                     DAS.REGION_KEY,
                     /* daily cards snapshot */
                     DAS.OUTSTANDING_CARDS_COUNT,
                     DAS.TERMINAL_CARD_COUNT,
                     DAS.TOTAL_CARDS_COUNT,
                     DAS.CARDS_IN_TRANSITION_COUNT,
                     DAS.VIRTUAL_OUTSTANDING_CARD_COUNT,
                     DAS.VIRTUAL_TERMINAL_CARD_COUNT,
                     DAS.VIRTUAL_TOTAL_CARD_COUNT,
                     DAS.VIRTUAL_CARDS_IN_TRANS_COUNT,
                     DAS.MAG_OUTSTANDING_CARD_COUNT,
                     DAS.MAG_TERMINAL_CARD_COUNT,
                     DAS.MAG_TOTAL_CARD_COUNT,
                     DAS.MAG_CARDS_IN_TRANS_COUNT,
                     /* daily card  transactions */
                     DAS.ISSUED_CARDS_COUNT,
                     DAS.TERMINATED_CARDS_COUNT,
                     DAS.TRANS_CARDS_COUNT,
                     DAS.VIRTUAL_ISSUED_CARDS_COUNT,
                     DAS.VIRTUAL_TERMINATED_CARDS_COUNT,
                     DAS.VIRTUAL_TRANS_CARDS_COUNT,
                     DAS.MAG_ISSUED_CARDS_COUNT,
                     DAS.MAG_TERMINATED_CARDS_COUNT,
                     DAS.MAG_TRANS_CARD_COUNT,
                     DAS.ROW_CREATE_DTTM,
                     DAS.ROW_LAST_MOD_DTTM,
                     DAS.ROW_LAST_MOD_PROC_NM,
                     DAS.ROW_LAST_MOD_PROC_SEQ_NBR)
         (SELECT DAILY_ACCOUNT_SNAPSHOT_KEY_SEQ.NEXTVAL
                    AS DAILY_ACCOUNT_SNAPSHOT_KEY,
                 FRA.ACCOUNT_KEY,
                 FRA.PROGRAM_KEY,
                 FRA.DATE_KEY,
                 FRA.REGION_KEY,
                 FRA.OUTSTANDING_CARDS_COUNT,
                 FRA.TERMINAL_CARD_COUNT,
                 FRA.TOTAL_CARDS_COUNT,
                 FRA.CARDS_IN_TRANSITION_COUNT,
                 FRA.VIRTUAL_OUTSTANDING_CARD_COUNT,
                 FRA.VIRTUAL_TERMINAL_CARD_COUNT,
                 FRA.VIRTUAL_TOTAL_CARD_COUNT,
                 FRA.VIRTUAL_CARDS_IN_TRANS_COUNT,
                 FRA.MAG_OUTSTANDING_CARD_COUNT,
                 FRA.MAG_TERMINAL_CARD_COUNT,
                 FRA.MAGSTRIPE_DEVICE_COUNT,
                 FRA.MAG_CARDS_IN_TRANS_COUNT,
                 FRA.ISSUED_CARDS_COUNT,
                 FRA.TERMINATED_CARDS_COUNT,
                 FRA.TRANS_CARDS_COUNT,
                 FRA.VIRTUAL_ISSUED_CARDS_COUNT,
                 FRA.VIRTUAL_TERMINATED_CARDS_COUNT,
                 FRA.VIRTUAL_TRANS_CARDS_COUNT,
                 FRA.MAG_ISSUED_CARDS_COUNT,
                 FRA.MAG_TERMINATED_CARDS_COUNT,
                 FRA.MAG_TRANS_CARD_COUNT,
                 FRA.ROW_CREATE_DTTM,
                 FRA.ROW_LAST_MOD_DTTM,
                 FRA.ROW_LAST_MOD_PROC_NM,
                 FRA.ROW_LAST_MOD_PROC_SEQ_NBR
            FROM (  SELECT A.ACCOUNT_KEY AS ACCOUNT_KEY,
                           PDE.PROGRAM_KEY AS PROGRAM_KEY,
                           rec.DATE_KEY AS DATE_KEY,
                           0 AS REGION_KEY,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.TERMINATED_COUNT1
                                   ELSE
                                      0
                                END)
                              AS OUTSTANDING_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.TERMINATED_COUNT1
                                 ELSE
                                    0
                              END)
                              AS TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS TOTAL_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.IN_TRANSITION_COUNT1
                                 ELSE
                                    0
                              END)
                              AS CARDS_IN_TRANSITION_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.VIRTUAL_DEVICE_COUNT1
                                   ELSE
                                      0
                                END)
                              AS VIRTUAL_OUTSTANDING_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.TERMINATED_COUNT1
                                 ELSE
                                    0
                              END),
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TOTAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_CARDS_IN_TRANS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.MAGSTRIPE_DEVICE_COUNT1
                                   ELSE
                                      0
                                END)
                              AS MAG_OUTSTANDING_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END),
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAGSTRIPE_DEVICE_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_CARDS_IN_TRANS_COUNT,
                           SUM (
                              DECODE (PD.ISSUED_DATE,
                                      REC.CALENDAR_DATE_DT, 1,
                                      0))
                              AS ISSUED_CARDS_COUNT,
                           SUM (
                              DECODE (PD.TERMINATED_DATE,
                                      REC.CALENDAR_DATE_DT, 1,
                                      0))
                              AS TERMINATED_CARDS_COUNT,
                           SUM (
                              DECODE (PD.IN_TRANSITION_DATE,
                                      REC.CALENDAR_DATE_DT, 1,
                                      0))
                              AS TRANS_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.ISSUED_DATE,
                                 REC.CALENDAR_DATE_DT, PD.VIRTUAL_DEVICE_COUNT1,
                                 0))
                              AS VIRTUAL_ISSUED_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.TERMINATED_DATE,
                                 REC.CALENDAR_DATE_DT, PD.VIRTUAL_DEVICE_COUNT1,
                                 0))
                              AS VIRTUAL_TERMINATED_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.IN_TRANSITION_DATE,
                                 REC.CALENDAR_DATE_DT, PD.VIRTUAL_DEVICE_COUNT1,
                                 0))
                              AS VIRTUAL_TRANS_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.ISSUED_DATE,
                                 REC.CALENDAR_DATE_DT, MAGSTRIPE_DEVICE_COUNT1,
                                 0))
                              AS MAG_ISSUED_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.TERMINATED_DATE,
                                 REC.CALENDAR_DATE_DT, MAGSTRIPE_DEVICE_COUNT1,
                                 0))
                              AS MAG_TERMINATED_CARDS_COUNT,
                           SUM (
                              DECODE (
                                 PD.IN_TRANSITION_DATE,
                                 REC.CALENDAR_DATE_DT, MAGSTRIPE_DEVICE_COUNT1,
                                 0))
                              AS MAG_TRANS_CARD_COUNT,
                           SYSDATE AS ROW_CREATE_DTTM,
                           SYSDATE AS ROW_LAST_MOD_DTTM,
                           'F_DAILY_ACCOUNT_AGGREGATES' AS ROW_LAST_MOD_PROC_NM,
                           1 AS ROW_LAST_MOD_PROC_SEQ_NBR
                      FROM D_Account A
                           JOIN F_Purchase_Device_Event PDE
                              ON A.ACCOUNT_KEY = PDE.ACCOUNT_KEY
                           JOIN D_Purchase_Device PD
                              ON PD.PURCHASE_DEVICE_KEY =
                                    PDE.PURCHASE_DEVICE_KEY
                     WHERE
                                A.ACCOUNT_CLOSED_DATE >= REC.CALENDAR_DATE_DT
                           AND A.ACCOUNT_OPEN_DATE <= REC.CALENDAR_DATE_DT -- filter for active accounts
                        --    AND A.CURRENT_RECORD_FLG = '1'    commented out on 01/17/2011 by VK since this is not needed ...  
                  GROUP BY A.ACCOUNT_KEY, PDE.PROGRAM_KEY) FRA);

      COMMIT;
   END LOOP;


 /*  v_sql :=
      'CREATE BITMAP INDEX F_DAILY_ACCOUNT_SNAPSHOT_UI1 ON  F_DAILY_ACCOUNT_SNAPSHOT
(ACCOUNT_KEY)
NOLOGGING
TABLESPACE I_BI
LOCAL
(  PARTITION YM_2008_01 ,
    PARTITION YM_2008_02 ,
   PARTITION YM_2008_03  ,
  PARTITION YM_2008_04  ,
  PARTITION YM_2008_05 ,
  PARTITION YM_2008_06 ,
  PARTITION YM_2008_07 ,
  PARTITION YM_2008_08 ,
  PARTITION YM_2008_09 ,
  PARTITION YM_2008_10 ,
  PARTITION YM_2008_11 ,
  PARTITION YM_2008_12 ,
  PARTITION YM_2009_01 ,
  PARTITION YM_2009_02 ,
  PARTITION YM_2009_03 ,
  PARTITION YM_2009_04 ,
  PARTITION YM_2009_05 ,
  PARTITION YM_2009_06 ,
  PARTITION YM_2009_07 ,
  PARTITION YM_2009_08 ,
  PARTITION YM_2009_09 ,
  PARTITION YM_2009_10 ,
  PARTITION YM_2009_11 ,
  PARTITION YM_2009_12 ,
  PARTITION YM_2010_01 ,
  PARTITION YM_2010_02 ,
  PARTITION YM_2010_03 ,
  PARTITION YM_2010_04 ,
  PARTITION YM_2010_05 ,
  PARTITION YM_2010_06 ,
  PARTITION YM_2010_07 ,
  PARTITION YM_2010_08 ,
  PARTITION YM_2010_09 ,
  PARTITION YM_2010_10 ,
  PARTITION YM_2010_11 ,
  PARTITION YM_2010_12 ,
  PARTITION MAX_VALUE
)  compute statistics  ';

   EXECUTE IMMEDIATE V_SQL;

   v_sql :=
      'CREATE BITMAP INDEX  F_DAILY_ACCOUNT_SNAPSHOT_UI2 ON  F_DAILY_ACCOUNT_SNAPSHOT
(PROGRAM_KEY)
NOLOGGING
TABLESPACE I_F_TRANS_LINE_ITEM
LOCAL
(  PARTITION YM_2008_01 ,
    PARTITION YM_2008_02 ,
   PARTITION YM_2008_03  ,
  PARTITION YM_2008_04  ,
  PARTITION YM_2008_05 ,
  PARTITION YM_2008_06 ,
  PARTITION YM_2008_07 ,
  PARTITION YM_2008_08 ,
  PARTITION YM_2008_09 ,
  PARTITION YM_2008_10 ,
  PARTITION YM_2008_11 ,
  PARTITION YM_2008_12 ,
  PARTITION YM_2009_01 ,
  PARTITION YM_2009_02 ,
  PARTITION YM_2009_03 ,
  PARTITION YM_2009_04 ,
  PARTITION YM_2009_05 ,
  PARTITION YM_2009_06 ,
  PARTITION YM_2009_07 ,
  PARTITION YM_2009_08 ,
  PARTITION YM_2009_09 ,
  PARTITION YM_2009_10 ,
  PARTITION YM_2009_11 ,
  PARTITION YM_2009_12 ,
  PARTITION YM_2010_01 ,
  PARTITION YM_2010_02 ,
  PARTITION YM_2010_03 ,
  PARTITION YM_2010_04 ,
  PARTITION YM_2010_05 ,
  PARTITION YM_2010_06 ,
  PARTITION YM_2010_07 ,
  PARTITION YM_2010_08 ,
  PARTITION YM_2010_09 ,
  PARTITION YM_2010_10 ,
  PARTITION YM_2010_11 ,
  PARTITION YM_2010_12 ,
  PARTITION MAX_VALUE
)  compute statistics  ';

   EXECUTE IMMEDIATE V_SQL;

   v_sql :=
      ' CREATE BITMAP INDEX  F_DAILY_ACCOUNT_SNAPSHOT_UI3 ON  F_DAILY_ACCOUNT_SNAPSHOT
(DATE_KEY)
NOLOGGING
TABLESPACE I_BI
LOCAL
(  PARTITION YM_2008_01 ,
    PARTITION YM_2008_02 ,
   PARTITION YM_2008_03  ,
  PARTITION YM_2008_04  ,
  PARTITION YM_2008_05 ,
  PARTITION YM_2008_06 ,
  PARTITION YM_2008_07 ,
  PARTITION YM_2008_08 ,
  PARTITION YM_2008_09 ,
  PARTITION YM_2008_10 ,
  PARTITION YM_2008_11 ,
  PARTITION YM_2008_12 ,
  PARTITION YM_2009_01 ,
  PARTITION YM_2009_02 ,
  PARTITION YM_2009_03 ,
  PARTITION YM_2009_04 ,
  PARTITION YM_2009_05 ,
  PARTITION YM_2009_06 ,
  PARTITION YM_2009_07 ,
  PARTITION YM_2009_08 ,
  PARTITION YM_2009_09 ,
  PARTITION YM_2009_10 ,
  PARTITION YM_2009_11 ,
  PARTITION YM_2009_12 ,
  PARTITION YM_2010_01 ,
  PARTITION YM_2010_02 ,
  PARTITION YM_2010_03 ,
  PARTITION YM_2010_04 ,
  PARTITION YM_2010_05 ,
  PARTITION YM_2010_06 ,
  PARTITION YM_2010_07 ,
  PARTITION YM_2010_08 ,
  PARTITION YM_2010_09 ,
  PARTITION YM_2010_10 ,
  PARTITION YM_2010_11 ,
  PARTITION YM_2010_12 ,
  PARTITION MAX_VALUE
)  COMPUTE STATISTICS';

   EXECUTE IMMEDIATE V_SQL;

   v_sql :=
      ' CREATE BITMAP INDEX  F_DAILY_ACCOUNT_SNAPSHOT_UI4 ON  F_DAILY_ACCOUNT_SNAPSHOT
(REGION_KEY)
NOLOGGING
TABLESPACE I_BI
LOCAL
( PARTITION YM_2008_01 ,
  PARTITION YM_2008_02 ,
  PARTITION YM_2008_03 ,
  PARTITION YM_2008_04 ,
  PARTITION YM_2008_05 ,
  PARTITION YM_2008_06 ,
  PARTITION YM_2008_07 ,
  PARTITION YM_2008_08 ,
  PARTITION YM_2008_09 ,
  PARTITION YM_2008_10 ,
  PARTITION YM_2008_11 ,
  PARTITION YM_2008_12 ,
  PARTITION YM_2009_01 ,
  PARTITION YM_2009_02 ,
  PARTITION YM_2009_03 ,
  PARTITION YM_2009_04 ,
  PARTITION YM_2009_05 ,
  PARTITION YM_2009_06 ,
  PARTITION YM_2009_07 ,
  PARTITION YM_2009_08 ,
  PARTITION YM_2009_09 ,
  PARTITION YM_2009_10 ,
  PARTITION YM_2009_11 ,
  PARTITION YM_2009_12 ,
  PARTITION YM_2010_01 ,
  PARTITION YM_2010_02 ,
  PARTITION YM_2010_03 ,
  PARTITION YM_2010_04 ,
  PARTITION YM_2010_05 ,
  PARTITION YM_2010_06 ,
  PARTITION YM_2010_07 ,
  PARTITION YM_2010_08 ,
  PARTITION YM_2010_09 ,
  PARTITION YM_2010_10 ,
  PARTITION YM_2010_11 ,
  PARTITION YM_2010_12 ,
  PARTITION MAX_VALUE
)  compute statistics ';

   EXECUTE IMMEDIATE V_SQL;
   */
   
END F_DAILY_ACCOUNT_AGGREGATES;
/