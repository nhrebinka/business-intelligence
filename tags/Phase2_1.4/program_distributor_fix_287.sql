
SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/program_distributor_fix_287.log ; 

--Delete the duplicate rows from M_SPNR_PGM----

DELETE M_SPNR_PGM_XREF WHERE SPNR_PGM_PK IN ('1-43KVY3','1-43KVY0');

COMMIT;

--Populate the EDM_PAR_PGM_KEY for all rows---
                                         
 MERGE INTO M_SPNR_PGM MSP
     USING (SELECT E1.EDM_SPNR_PGM_KEY AS EDM_SPNR_PGM_KEY,
                   E2.EDM_SPNR_PGM_KEY AS EDM_PAR_PGM_KEY
              FROM STAGE_OWNER.SRC_SBL_SPNR_PGM S,
                   EDM_OWNER.M_SPNR_PGM_XREF e1,
                   EDM_OWNER.M_SPNR_PGM_XREF e2
             WHERE     S.PGM_ROW_ID = E1.SPNR_PGM_PK
                   AND S.PAR_PGM_ROW_ID = E2.SPNR_PGM_PK
                   AND E1.DW_SOURCE_SYS = 'SIEBEL'
                   AND E2.DW_SOURCE_SYS = 'SIEBEL') A
        ON (MSP.EDM_SPNR_PGM_KEY = A.EDM_SPNR_PGM_KEY)
WHEN MATCHED
THEN
   UPDATE SET MSP.EDM_PAR_PGM_KEY = A.EDM_PAR_PGM_KEY;

COMMIT;

--Populate the correct PROGRAM_SPONSOR_NAME-----

UPDATE EDM_OWNER.M_SPNR_PGM B
   SET B.EDM_LAST_UPDT_SESSION_NM = 'One Shot BI#287 Data Fix',
       PROGRAM_SPONSOR_NAME =
          (SELECT PAR.PGM_NM
             FROM EDM_OWNER.M_SPNR_PGM CHLD,
                  (SELECT EDM_SPNR_PGM_KEY, PGM_NM
                     FROM EDM_OWNER.M_SPNR_PGM
                    WHERE EDM_SPNR_PGM_KEY IN
                             (SELECT EDM_PAR_PGM_KEY
                                FROM EDM_OWNER.M_SPNR_PGM A
                               WHERE A.PROGRAM_NAME IN
                                        ('D4', 'Basic', 'Enhanced')
                                     AND A.PROGRAM_SPONSOR_NAME NOT IN
                                            ('Parent Jiffy Lube Program',
                                             'Pacific Pride Services Inc',
                                             'Sinclair Fleet Track Parent',
                                             'Wex Distributor Parent Program',
                                             'Parent Valvoline Universal Prg',
                                             'Parent Tesoro Universal',
                                             'Parent ConocoPhillips Program'))) PAR
            WHERE     CHLD.EDM_PAR_PGM_KEY = PAR.EDM_SPNR_PGM_KEY
                  AND CHLD.EDM_SPNR_PGM_KEY = B.EDM_SPNR_PGM_KEY
                  AND CHLD.PROGRAM_NAME IN ('D4', 'Basic', 'Enhanced')
                  AND CHLD.PROGRAM_SPONSOR_NAME NOT IN
                         ('Parent Jiffy Lube Program',
                          'Pacific Pride Services Inc',
                          'Sinclair Fleet Track Parent',
                          'Wex Distributor Parent Program',
                          'Parent Valvoline Universal Prg',
                          'Parent Tesoro Universal',
                          'Parent ConocoPhillips Program'))
 WHERE EDM_SPNR_PGM_KEY IN
          (SELECT EDM_SPNR_PGM_KEY
             FROM EDM_OWNER.M_SPNR_PGM A
            WHERE A.PROGRAM_NAME IN ('D4', 'Basic', 'Enhanced')
                  AND A.PROGRAM_SPONSOR_NAME NOT IN
                         ('Parent Jiffy Lube Program',
                          'Pacific Pride Services Inc',
                          'Sinclair Fleet Track Parent',
                          'Wex Distributor Parent Program',
                          'Parent Valvoline Universal Prg',
                          'Parent Tesoro Universal',
                          'Parent ConocoPhillips Program'));

COMMIT;                                                                                                                                                                                                                                                                                                                                  
                        
                        
spool off ;                                              
