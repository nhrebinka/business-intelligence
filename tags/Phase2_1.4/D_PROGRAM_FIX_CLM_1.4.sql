SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/d_program_update_clm_1.4.log ; 



--Query for updating D_PROGRAM@EDW--


UPDATE D_PROGRAM D
set       D.PROGRAM_NAME = 'Universal',
           D.SERVICE_OPTION_CODE = '1',
          D.PROGRAM_SPONSOR_NAME = 'WEX',
          D.PRODUCT_NAME = 'Proprietary-Direct',
          D.PROGRAM_PACKAGE_NAME= 'Sunoco Universal Universal',
          D.PROGRAM_MANAGER_NAME = 'Sunoco'
          WHERE D.PROGRAM_KEY = 315;
       
COMMIT;




  
  
UPDATE D_PROGRAM D
set      D.PROGRAM_NAME = 'Universal',
          D.SERVICE_OPTION_CODE = '1',
          D.PROGRAM_SPONSOR_NAME = 'WEX',
           D.PRODUCT_NAME = 'Proprietary-Direct',
          D.PROGRAM_PACKAGE_NAME= 'ExxonMobil Universal Universal',
          D.PROGRAM_MANAGER_NAME = 'ExxonMobil'
          WHERE D.PROGRAM_KEY = 571;
          
          COMMIT;
          
          
spool off ;        