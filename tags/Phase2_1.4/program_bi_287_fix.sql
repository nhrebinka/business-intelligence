SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/program_bi_287_fix.log ; 

update edm_owner.m_spnr_pgm pgm
set edm_par_pgm_key= '76313'
where edm_spnr_pgm_key in ('76314', '76313');

commit;

spool off;