CREATE OR REPLACE PACKAGE BODY DW_OWNER.AuthCapture
IS
   /******************************************************************************
      NAME:    AuthCapture
      PURPOSE:
      REVISIONS:
      Ver        Date                   Author                     Description
      ---------  ----------            ---------------  ------------------------------------
      1.0        12/14/2011        Venu  KAncharla       1. Created  just a Pacakge Header minus Body for dev integration for Loading auth data

      1.1       03/05/2012         Venu KAncharla        Removed trunc for LAST_UPDT_DT and LAST_UPDT_TMS

      1.2      03/08/2012          Venu Kancharla        DEFECT # 27110

      1.3      04/04/2012          Venu Kancharla        Max for PD ,  tunning for ROOT ACCT DRIVER LOOKUP's

     1.4       04/09/2012          Venu Kancharla        Remove the look for the  ROOT_ACCT_DRIVER_KEY this is not needed
                                                         for RED and eventually the column needs to be dropped from DB

     1.5       04/25/2012          Venu Kancharla       Branch out the Code to do the look up for Drive KEy based onthe Source SYS where the PD Is..
                                                         This is to reduce the I/O

     1.6       08/20/2012          Sagar Patel             Changed the IOL_CCN_NUM to INTERNAL_PD_NBR                                               
     1.7       09/10/2012         Venu Kancharla        Added P_ORACLE_CODE out ...           

   ******************************************************************************/
   PROCEDURE LOOKUP_TIME_KEY (
      P_LOCTRANDTTM       IN     FW_AUTH_DTL.LOCAL_TRANS_DT%TYPE,
      P_DW_TIME_DAY_KEY      OUT FW_AUTH_DTL.DW_TIME_DAY_KEY%TYPE)
   IS
      /*
        PROCEDUE TO LOOK UP THE ACCOUNT_KEY and PGM_KEY for a Given WEx_account_Number
     */
      V_DW_TIME_DAY_KEY   FW_AUTH_DTL.DW_PRODUCT_KEY%TYPE;
   BEGIN
      SELECT DTD.DW_TIME_DAY_KEY
        INTO V_DW_TIME_DAY_KEY
        FROM DW_TIME_DAY DTD
       WHERE     DTD.DAILY_DATE_DT = TRUNC (P_LOCTRANDTTM)
             AND DTD.LANGUAGE_CD = 'ENG';

      P_DW_TIME_DAY_KEY := V_DW_TIME_DAY_KEY;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --      DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_TIME_DAY_KEY := 99;
   END LOOKUP_TIME_KEY;

   PROCEDURE LOOKUP_PRODUCT_KEY (
      P_NACSCD           IN     FW_AUTH_DTL.NACSCD%TYPE,
      P_DW_PRODUCT_KEY      OUT FW_AUTH_DTL.DW_PRODUCT_KEY%TYPE)
   IS
      /*
        PROCEDUE TO LOOK UP THE ACCOUNT_KEY and PGM_KEY for a Given WEx_account_Number
     */
      V_DW_PRODUCT_KEY   FW_AUTH_DTL.DW_PRODUCT_KEY%TYPE;
   BEGIN
      SELECT MAX (DW_PRODUCT_KEY)
        INTO V_DW_PRODUCT_KEY
        FROM DW_PRODUCT DP
       WHERE DP.IND_STD_PROD_CD = P_NACSCD AND DP.DW_CURRENT_FLG = 'Y';

      P_DW_PRODUCT_KEY := V_DW_PRODUCT_KEY;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- --         DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_PRODUCT_KEY := 99;
   END LOOKUP_PRODUCT_KEY;

   PROCEDURE LOOKUP_ACCT_PGM_KEY (
      P_ACCTID              IN     FW_AUTH_DTL.WEX_ACCT_NBR%TYPE,
      P_DW_ACCOUNT_ROW_ID   IN     DW_PURCH_DEVICE.ACCT_ROW_ID%TYPE,
      P_DW_ACCT_KEY            OUT FW_AUTH_DTL.DW_ACCT_KEY%TYPE,
      P_DW_SPNR_PGM_KEY        OUT FW_AUTH_DTL.DW_SPNR_PGM_KEY%TYPE,
      P_DW_ACCT_ROW_ID         OUT DW_ACCT.ACCT_ROW_ID%TYPE)
   IS
      /*
        PROCEDUE TO LOOK UP THE ACCOUNT_KEY and PGM_KEY for a Given WEx_account_Number IF the ACCTID from AUTH IS NOT NULL
        IF the ACCTID from AUTH is NULL than the following logic kick which is
       for bulk of classic Accounts there will no ACCT_ID from AUTH  Account  ROW ID is looked up based on the  PDID.

     */
      V_DW_ACCT_KEY         FW_AUTH_DTL.DW_ACCT_KEY%TYPE;
      V_DW_SPNR_PGM_KEY     FW_AUTH_DTL.DW_SPNR_PGM_KEY%TYPE;
      V_DW_ACCOUNT_ROW_ID   DW_PURCH_DEVICE.ACCT_ROW_ID%TYPE; -- Note this Account Row is sourced from the PD table and this is used only if the ACCTID FROM AUTH IS NULL
      V_DW_ACCT_ROW_ID      DW_ACCT.ACCT_ROW_ID%TYPE; -- This is to Source the account is as we get from Auth
   BEGIN
      CASE
         WHEN TRIM (P_ACCTID) IS NOT NULL --  Account ID from auth is processed and this data is available for only Mp accounts and some Cp accounts only
         THEN
            SELECT /*+ parallel (da, 8 )  */
                   MAX (DA.DW_ACCT_KEY),
                   MAX (DSP.DW_SPNR_PGM_KEY),
                   MAX (DA.ACCT_ROW_ID)
              INTO V_DW_ACCT_KEY, V_DW_SPNR_PGM_KEY, V_DW_ACCT_ROW_ID
              FROM DW_ACCT DA, DW_SPNR_PGM DSP
             WHERE     DA.DW_CURRENT_FLG = 'Y'
                   AND DSP.DW_CURRENT_FLG = 'Y'
                   AND DA.PGM_ROW_ID = DSP.PGM_ROW_ID
                   AND DA.WEX_ACCT_NBR = P_ACCTID;


            P_DW_ACCT_KEY := V_DW_ACCT_KEY;
            P_DW_SPNR_PGM_KEY := V_DW_SPNR_PGM_KEY;

            P_DW_ACCT_ROW_ID := V_DW_ACCT_ROW_ID; --- The Acct ID from Auth takes Precedence if we get one ...
         ELSE
            SELECT /*+ parallel (da, 8 )  */
                  MAX (DA.DW_ACCT_KEY),
                   MAX (DSP.DW_SPNR_PGM_KEY),
                   MAX (DA.ACCT_ROW_ID)
              INTO V_DW_ACCT_KEY, V_DW_SPNR_PGM_KEY, V_DW_ACCOUNT_ROW_ID
              FROM DW_ACCT DA, DW_SPNR_PGM DSP
             WHERE     DA.DW_CURRENT_FLG = 'Y'
                   AND DSP.DW_CURRENT_FLG = 'Y'
                   AND DA.PGM_ROW_ID = DSP.PGM_ROW_ID
                   AND DA.ACCT_ROW_ID = P_DW_ACCOUNT_ROW_ID;

            P_DW_ACCT_KEY := V_DW_ACCT_KEY;
            P_DW_SPNR_PGM_KEY := V_DW_SPNR_PGM_KEY;

            P_DW_ACCT_ROW_ID := V_DW_ACCOUNT_ROW_ID; --- Note the Account id from PD is Source if we do not get a Valid (NON-NULL)  ACCTID
      END CASE;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- --         DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_ACCT_KEY := 99;
         P_DW_SPNR_PGM_KEY := 99;
   --P_DW_ACCT_ROW_ID  := '99' ;

   END LOOKUP_ACCT_PGM_KEY;

   PROCEDURE LOOKUP_POS_SITE_STKHLDR_KEY (
      P_TERMPRFX      IN     FW_AUTH_DTL.TERMINAL_PRFX%TYPE,
      P_TERMID        IN     FW_AUTH_DTL.TERMINAL_ID%TYPE,
      P_DW_POS_KEY       OUT FW_AUTH_DTL.DW_POS_KEY%TYPE,
      P_DW_SITE_KEY      OUT FW_AUTH_DTL.DW_SITE_KEY%TYPE,
      P_DW_MRCH_KEY      OUT FW_AUTH_DTL.DW_MRCH_KEY%TYPE)
   IS
      /*
             PROCEDUE TO LOOK UP THE  POS_KEY  , SITE_KEY and STKHLDR_KEY for a  given TERM_PREFIX and TERM_ID
     */

      V_DW_POS_KEY    FW_AUTH_DTL.DW_POS_KEY%TYPE;
      V_DW_SITE_KEY   FW_AUTH_DTL.DW_SITE_KEY%TYPE;
      V_DW_MRCH_KEY   FW_AUTH_DTL.DW_MRCH_KEY%TYPE;
   BEGIN
      SELECT MAX (DW_POS_KEY), MAX (DW_SITE_KEY), MAX (DSK.DW_STKHLDR_KEY)
        INTO V_DW_POS_KEY, V_DW_SITE_KEY, V_DW_MRCH_KEY
        FROM DW_POS DP, DW_SITE DS, DW_STKHLDR DSK
       WHERE     DP.SITE_ROW_ID = DS.SITE_ROW_ID
             AND DSK.STKHLDR_ROW_ID = DS.STKHLDR_ROW_ID
             AND DS.DW_CURRENT_FLG = 'Y'
             AND DP.DW_CURRENT_FLG = 'Y'
             AND DSK.DW_CURRENT_FLG = 'Y'
             AND DP.GRP_MRCH_ID = P_TERMPRFX
             AND DP.POS_ID = P_TERMID;

      P_DW_POS_KEY := V_DW_POS_KEY;
      P_DW_SITE_KEY := V_DW_SITE_KEY;
      P_DW_MRCH_KEY := V_DW_MRCH_KEY;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- --         DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_POS_KEY := 99;
         P_DW_SITE_KEY := 99;
         P_DW_MRCH_KEY := 99;
   END LOOKUP_POS_SITE_STKHLDR_KEY;


   PROCEDURE LOOKUP_PD_KEY (
      P_PDID                  IN     FW_AUTH_DTL.PD_ID%TYPE,
      P_DW_PURCH_DEVICE_KEY      OUT FW_AUTH_DTL.DW_PURCH_DEVICE_KEY%TYPE,
      P_DW_ASSET_ROW_ID          OUT DW_PURCH_DEVICE.ASSET_ROW_ID%TYPE,
      P_DW_ACCOUNT_ROW_ID        OUT DW_PURCH_DEVICE.ACCT_ROW_ID%TYPE,
      P_PD_DW_SOURCE_SYS         OUT DW_PURCH_DEVICE.DW_SOURCE_SYS%TYPE)
   IS
      /*
        PROCEDUE TO LOOK UP THE ACCOUNT_KEY and PGM_KEY for a Given WEx_account_Number
     */
      V_DW_PURCH_DEVICE_KEY   FW_AUTH_DTL.DW_PURCH_DEVICE_KEY%TYPE;
      V_PDID                  FW_AUTH_DTL.PD_ID%TYPE;
      V_DW_ASSET_ROW_ID       DW_PURCH_DEVICE.ASSET_ROW_ID%TYPE;
      V_DW_ACCOUNT_ROW_ID     DW_PURCH_DEVICE.ACCT_ROW_ID%TYPE; -- this is the ROW ID SOURCED FROM PURCh DEVICE WHICH WILL BE USED IF AND ONLY IF
      --  IT IS MISSING FROM AUTH ACCTID
      V_PD_DW_SOURCE_SYS         DW_PURCH_DEVICE.DW_SOURCE_SYS%TYPE; --- This is used to find the Source of the PD..Which is latter used to Branch out the logic
   BEGIN
      V_PDID := TRIM (P_PDID); -- this is added just if we need to    do some more extensive cleansing ..


      IF V_PDID IS NOT NULL
      THEN
         CASE
            WHEN     SUBSTR (V_PDID, 1, 6) <> '690046'
                 AND SUBSTR (V_PDID, 1, 6) = '732800' -- Exxon Card lookup Logic
            THEN
               SELECT /*+ parallel (DPD, 12)*/
                     MAX (DPD.DW_PURCH_DEVICE_KEY),
                      MAX (DPD.ASSET_ROW_ID),
                      MAX (DPD.ACCT_ROW_ID),
                      MAX (DPD.DW_SOURCE_SYS)
                 INTO
                      V_DW_PURCH_DEVICE_KEY,
                      V_DW_ASSET_ROW_ID,
                      V_DW_ACCOUNT_ROW_ID,
                      V_PD_DW_SOURCE_SYS
                 FROM
                      DW_PURCH_DEVICE DPD
                WHERE
                          DPD.DW_CURRENT_FLG = 'Y'
                      AND DPD.ARCHIVE_FLG = 'N'
                      AND DPD.ENCODED_PD_NBR = V_PDID;
            WHEN SUBSTR (V_PDID, 1, 6) = '690046'        -- 04 Universal Logic
            THEN
               SELECT /*+ parallel (DPD, 12)*/
                     MAX (DPD.DW_PURCH_DEVICE_KEY),
                      MAX (DPD.ASSET_ROW_ID),
                      MAX (DPD.ACCT_ROW_ID),
                      MAX (DPD.DW_SOURCE_SYS)
                 INTO
                      V_DW_PURCH_DEVICE_KEY,
                      V_DW_ASSET_ROW_ID,
                      V_DW_ACCOUNT_ROW_ID,
                      V_PD_DW_SOURCE_SYS
                 FROM
                       DW_PURCH_DEVICE DPD
                WHERE
                         DPD.DW_CURRENT_FLG = 'Y'
                      AND DPD.ARCHIVE_FLG = 'N'
                      AND DPD.EMBOSS_PD_NBR = SUBSTR (V_PDID, 7);
            WHEN SUBSTR (V_PDID, 1, 6) NOT IN ('690046', '732800') --  CCN Logic   For  IOL FLEET ISO
            THEN
               SELECT /*+ parallel (DPD, 12)*/
                     MAX (DPD.DW_PURCH_DEVICE_KEY),
                      MAX (DPD.ASSET_ROW_ID),
                      MAX (DPD.ACCT_ROW_ID),
                      MAX (DPD.DW_SOURCE_SYS)
                 INTO
                      V_DW_PURCH_DEVICE_KEY,
                      V_DW_ASSET_ROW_ID,
                      V_DW_ACCOUNT_ROW_ID,
                      V_PD_DW_SOURCE_SYS
                 FROM
                      DW_PURCH_DEVICE DPD
                WHERE
                          DPD.DW_CURRENT_FLG = 'Y'
                      AND DPD.ARCHIVE_FLG = 'N'
                      AND DPD.INTERNAL_PD_NBR = V_PDID;
            ELSE
               V_DW_PURCH_DEVICE_KEY := 99;
               V_DW_ASSET_ROW_ID := '99';
         END CASE;
      END IF;

      P_DW_PURCH_DEVICE_KEY := V_DW_PURCH_DEVICE_KEY;
      P_DW_ASSET_ROW_ID     := V_DW_ASSET_ROW_ID;
      P_DW_ACCOUNT_ROW_ID   := V_DW_ACCOUNT_ROW_ID;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --      DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_PURCH_DEVICE_KEY := 99;
         P_DW_ASSET_ROW_ID := '99';
   END LOOKUP_PD_KEY;

   PROCEDURE LOOKUP_DRIVER_ASSET_KEY (
      --  P_ACCTID                       IN     FW_AUTH_DTL.WEX_ACCT_NBR%TYPE         ,
      P_DW_ACCOUNT_ROW_ID   IN     DW_ACCT.ACCT_ROW_ID%TYPE,
      P_DW_ASSET_ROW_ID     IN     DW_PURCH_DEVICE.ASSET_ROW_ID%TYPE,
      --  P_PD_ID                          IN      FW_AUTH_DTL.PD_ID%TYPE                        ,
      P_POSPRMPTCODE1       IN     FW_AUTH_DTL.POS_PROMPT_CD1%TYPE,
      P_POSPRMPTCODE2       IN     FW_AUTH_DTL.POS_PROMPT_CD2%TYPE,
      P_POSPRMPTCODE3       IN     FW_AUTH_DTL.POS_PROMPT_CD3%TYPE,
      P_POSPRMPTDATA1       IN     FW_AUTH_DTL.POS_PROMPT_DATA1%TYPE,
      P_POSPRMPTDATA2       IN     FW_AUTH_DTL.POS_PROMPT_DATA2%TYPE,
      P_POSPRMPTDATA3       IN     FW_AUTH_DTL.POS_PROMPT_DATA3%TYPE,
      P_DW_DRIVER_KEY          OUT FW_AUTH_DTL.DW_DRIVER_KEY%TYPE,
      P_DW_ASSET_KEY           OUT FW_AUTH_DTL.DW_ASSET_KEY%TYPE,
      P_WEX_DRIVER_ID          OUT DW_DRIVER.WEX_DRIVER_ID%TYPE)
   IS
      /*
        PROCEDUE TO LOOK UP  DRIVER KEY AND ASSET KEY USING THE ACCOUNT ROW ID and ASSET ROW ID respectively
        and using the promptcodes and prompt data to find the appropriate driver id / PIN and Vehicle ID
        Herever Appropriate .
     */
      V_DW_ACCT_KEY       FW_AUTH_DTL.DW_ACCT_KEY%TYPE;
      V_DW_SPNR_PGM_KEY   FW_AUTH_DTL.DW_SPNR_PGM_KEY%TYPE;
      V_ACCT_ROW_ID       DW_ACCT.ACCT_ROW_ID%TYPE;
      V_WEX_DRIVER_ID     DW_DRIVER.WEX_DRIVER_ID%TYPE;
      V_DW_DRIVER_KEY     DW_DRIVER.DW_DRIVER_KEY%TYPE;
      V_WEX_ASSET_ID      DW_ASSET.WEX_ASSET_ID%TYPE;
      V_DW_ASSET_KEY      DW_ASSET.DW_ASSET_KEY%TYPE;
   BEGIN
      -- Logic to lookup the DRIVER ID

      V_WEX_DRIVER_ID :=
         CASE
            WHEN P_POSPRMPTCODE1 = '3' THEN P_POSPRMPTDATA1
            WHEN P_POSPRMPTCODE2 = '3' THEN P_POSPRMPTDATA2
            WHEN P_POSPRMPTCODE3 = '3' THEN P_POSPRMPTDATA3
            ELSE '99'
         END;


      P_WEX_DRIVER_ID := V_WEX_DRIVER_ID;

      -- Logic to lookup the  ASSET ID / VEHICLE ID

      V_WEX_ASSET_ID :=
         CASE
            WHEN P_POSPRMPTCODE1 = '1' THEN P_POSPRMPTDATA1
            WHEN P_POSPRMPTCODE2 = '1' THEN P_POSPRMPTDATA2
            WHEN P_POSPRMPTCODE3 = '1' THEN P_POSPRMPTDATA3
            ELSE '99'
         END;


      CASE
         WHEN     V_WEX_ASSET_ID = '99'
              AND NVL (P_DW_ACCOUNT_ROW_ID, '99') <> '99'
         /*  This should be  Vehicle Card since there are no prompts for Vehicle ID ...
               This means  the asset row id on the PD is the Asset Key to which this needs to be
               tied out .
         */
         THEN
            SELECT /*+ parallel (DAS, 12 ) */
                  MAX (DAS.DW_ASSET_KEY)
              INTO V_DW_ASSET_KEY
              FROM DW_ASSET DAS
             WHERE     DAS.DW_CURRENT_FLG = 'Y'
                   AND DAS.ARCHIVE_FLG = 'N'
                   AND DAS.ROOT_ACCT_ROW_ID = P_DW_ACCOUNT_ROW_ID
                   AND DAS.ASSET_ROW_ID = P_DW_ASSET_ROW_ID;
         WHEN     V_WEX_ASSET_ID <> '99'
              AND NVL (P_DW_ACCOUNT_ROW_ID, '99') <> '99'
         /*  This should be  a DRIVER CARD since we are prompting for a Vehicle ID
         */
         THEN
            SELECT /*+ parallel (DAS, 12 ) */
                  MAX (DAS.DW_ASSET_KEY)
              INTO V_DW_ASSET_KEY
              FROM DW_ASSET DAS
             WHERE     DAS.DW_CURRENT_FLG = 'Y'
                   AND DAS.ARCHIVE_FLG = 'N'
                   AND DAS.ROOT_ACCT_ROW_ID = P_DW_ACCOUNT_ROW_ID
                   AND DAS.WEX_ASSET_ID = V_WEX_ASSET_ID;
         ELSE
            V_DW_ASSET_KEY := 99;
      END CASE;

      P_DW_ASSET_KEY := V_DW_ASSET_KEY;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- --         DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .
         P_DW_ASSET_KEY := 99;
         P_DW_DRIVER_KEY := 99;
   END LOOKUP_DRIVER_ASSET_KEY;



   PROCEDURE LOOKUP_ROOT_ACCT_DRIVER_KEY (
      P_DW_ACCT_ROW_ID       IN     DW_ACCT.ACCT_ROW_ID%TYPE,
      P_WEX_DRIVER_ID        IN     DW_DRIVER.WEX_DRIVER_ID%TYPE,
      P_PD_DW_SOURCE_SYS     IN     DW_DRIVER.DW_SOURCE_SYS%TYPE ,
      P_DRIVER_ROOTED_ACCTID IN     DW_ACCT.WEX_ACCT_NBR%TYPE ,
      P_DRIVER_CORECTED_PIN  IN     DW_DRIVER.WEX_DRIVER_ID%TYPE,
      P_DW_ACCT_DRIVER_KEY      OUT FW_AUTH_DTL.DW_ROOT_ACCT_DRIVER_KEY%TYPE,
      P_DW_DRIVER_KEY           OUT DW_DRIVER.DW_DRIVER_KEY%TYPE)
   IS
      /*
        PROCEDURE TO LOOK UP  DRIVER KEY AND ASSET KEY USING THE ACCOUNT ROW ID and ASSET ROW ID respectively
        and using the promptcodes and prompt data to find the appropriate driver id / PIN and Vehicle ID
        Wherever Appropriate .


        -- Versioin 1.2   Drop the look ups for  Root Acct Driver Key ...this is not needed ...changing  the code not to  lookup the root account Driver Key .


     */
      --   V_DW_ACCT_DRIVER_KEY            DW_ACCT_DRIVER.DW_ACCT_DRIVER_KEY%TYPE  ;
      V_DW_DRIVER_KEY   DW_DRIVER.DW_DRIVER_KEY%TYPE;
   BEGIN
       IF  P_PD_DW_SOURCE_SYS = 'SIEBEL'      -- This has been added to branch out the code for Root account look up for TANDEM and SIEBEL to REDUCE THE I/O on the BOX

       THEN

             select /*+ parallel (dd, 4) */
                    MAX(DW_DRIVER_KEY)
                    INTO V_DW_DRIVER_KEY
             from
                  DW_DRIVER DD
             WHERE
                             DD.DW_CURRENT_FLG  = 'Y'
                         and DD.ARCHIVE_FLG     = 'N'
                         and DD.DW_SOURCE_SYS   = 'SIEBEL'
                         and DD.WEX_DRIVER_ID   = P_DRIVER_CORECTED_PIN
                         and DD.ROOT_ACCT_ROW_ID
                                            IN
                                            ( select /*+ parallel (Da, 4 ) */
                                              MAX(DA.ACCT_ROW_ID)  from DW_ACCT DA
                                             WHERE DA.DW_CURRENT_FLG = 'Y'
                                             and DA.ARCHIVE_FLG = 'N'
                                             and DA.DW_SOURCE_SYS = 'SIEBEL'
                                             and DA.WEX_ACCT_NBR = P_DRIVER_ROOTED_ACCTID ) ;

          /*     SELECT /*+ parallel (DD, 8) */
                      /*                  MAX (DD.DW_DRIVER_KEY)
                                    INTO V_DW_DRIVER_KEY
                                    FROM DW_DRIVER DD
                                   WHERE     DD.DW_CURRENT_FLG = 'Y'
                                         AND DD.ARCHIVE_FLG = 'N'
                                         AND DD.WEX_DRIVER_ID = P_WEX_DRIVER_ID ---  parameter for wex Driver ID
                                         AND DD.ROOT_ACCT_ROW_ID IN
                                                (SELECT /*+ parallel (da , 4 ) */
                             /*                          da.acct_row_id root_acct_row_id
                                                   FROM dw_acct da, dw_acct da2
                                                  WHERE     da.fla_par_row_id = da2.fla_par_row_id
                                                        AND da.dw_current_flg = DA2.DW_CURRENT_FLG
                                                        AND DA.ARCHIVE_FLG = DA2.ARCHIVE_FLG
                                                        AND DA2.ARCHIVE_FLG = 'N'
                                                        AND DA.DW_CURRENT_FLG = 'Y'
                                                        AND DA2.ACCT_ROW_ID = P_DW_ACCT_ROW_ID -- parameter  for the Account
                                                        AND da.acct_row_id =
                                                               CASE
                                                                  WHEN da.derived_hier_level_nbr = 1
                                                                  THEN
                                                                     da2.l1_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 2
                                                                  THEN
                                                                     da2.l2_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 3
                                                                  THEN
                                                                     da2.l3_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 4
                                                                  THEN
                                                                     da2.l4_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 5
                                                                  THEN
                                                                     da2.l5_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 6
                                                                  THEN
                                                                     da2.l6_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 7
                                                                  THEN
                                                                     da2.l7_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 8
                                                                  THEN
                                                                     da2.l8_acct_row_id
                                                                  WHEN da.derived_hier_level_nbr = 9
                                                                  THEN
                                                                     da2.l9_acct_row_id
                                                                  ELSE
                                                                     NULL
                                                               END);

                                  /*+
                                   SELECT MAX (DD.DW_DRIVER_KEY),
                                         --   MAX(DD.DRIVER_ROW_ID) ,
                                         --   MAX(DD.ROOT_ACCT_ROW_ID)  ,
                                         --    MAX(A.DW_ACCT_KEY) ,
                                           MAX (A.DW_ACCT_DRIVER_KEY)                  --,
                                   --    MAX(A.ROOT_ACCt_ROW_ID)
                                      INTO
                                       V_DW_DRIVER_KEY            ,
                                       V_DW_ACCT_DRIVER_KEY
                                   FROM
                                           (SELECT /*+ parallel (dad, 8 ) */
                                  /*           acct.root_dw_acct_key,
                                               acct.root_acct_row_id,
                                                --    acct.root_acct_dw_current_flg,
                                                acct.dw_acct_key,
                                                acct.acct_row_id,
                                                --    acct.dw_current_flg acct_dw_current_flg,
                                                dad.dw_acct_driver_key,
                                                dad.driver_row_id,
                                                dad.intersection_row_id                                     --,
                                           --   dad.is_root_acct_flg
                                           FROM (SELECT CASE
                                                           WHEN da.acct_row_id != da2.acct_row_id
                                                           THEN
                                                              'SHARED'
                                                           ELSE
                                                              'ROOTED'
                                                        END
                                                           driver_shared_ind,
                                                        da.dw_acct_key root_dw_acct_key,
                                                        da.acct_row_id root_acct_row_id,
                                                        da.dw_current_flg root_acct_dw_current_flg,
                                                        da2.dw_acct_key dw_acct_key,
                                                        da2.acct_row_id acct_row_id,
                                                        da2.dw_current_flg dw_current_flg,
                                                        da2.fla_par_row_id fla_par_row_id
                                                   FROM dw_acct da, dw_acct da2
                                                  WHERE     da.fla_par_row_id = da2.fla_par_row_id
                                                        AND da.dw_current_flg = DA2.DW_CURRENT_FLG
                                                        AND DA.ARCHIVE_FLG = DA2.ARCHIVE_FLG
                                                        AND DA2.ARCHIVE_FLG = 'N'
                                                        AND DA.DW_CURRENT_FLG = 'Y'
                                                        AND da.acct_row_id =
                                                                                       CASE
                                                                                          WHEN da.derived_hier_level_nbr = 1
                                                                                          THEN
                                                                                             da2.l1_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 2
                                                                                          THEN
                                                                                             da2.l2_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 3
                                                                                          THEN
                                                                                             da2.l3_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 4
                                                                                          THEN
                                                                                             da2.l4_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 5
                                                                                          THEN
                                                                                             da2.l5_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 6
                                                                                          THEN
                                                                                             da2.l6_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 7
                                                                                          THEN
                                                                                             da2.l7_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 8
                                                                                          THEN
                                                                                             da2.l8_acct_row_id
                                                                                          WHEN da.derived_hier_level_nbr = 9
                                                                                          THEN
                                                                                             da2.l9_acct_row_id
                                                                                          ELSE
                                                                                             NULL
                                                                                       END) acct,
                                                                                               dw_acct_driver dad
                                          WHERE
                                                       acct.root_acct_row_id            = dad.acct_row_id
                                                AND acct.root_acct_dw_current_flg = 'Y'
                                                AND acct.dw_current_flg                = 'Y'
                                                and Dad.DW_CURRENT_FLG           = 'Y'
                                                and DAD.ARCHIVE_FLG                  = 'N'
                                                AND CASE
                                                       WHEN dad.shared_mode = 'SHAREALL' THEN 1 --If driver not 'shareall' then only include for the acount it's assigned to.
                                                       WHEN acct.root_acct_row_id = acct.acct_row_id THEN 1
                                                       ELSE 0
                                                    END = 1) A,
                                                DW_DRIVER dd
                                  WHERE     a.root_acct_row_id   = DD.ROOT_ACCT_ROW_ID
                                        AND A.DRIVER_ROW_ID     = DD.DRIVER_ROW_ID
                                        and a.acct_row_id              =   P_DW_ACCT_ROW_ID               -- '1-7AF5-2'
                                        and DD.WEX_DRIVER_ID     =    P_WEX_DRIVER_ID
                                        AND DD.DW_CURRENT_FLG = 'Y'
                                        AND DD.ARCHIVE_FLG         = 'N';
                                     */
          ELSE
                  SELECT /*+ parallel (DD, 8) */
                           MAX (DD.DW_DRIVER_KEY)
                      INTO  V_DW_DRIVER_KEY
                  FROM   DW_DRIVER DD
                 WHERE
                              DD.DW_CURRENT_FLG = 'Y'
                          AND DD.ARCHIVE_FLG = 'N'
                          AND DD.WEX_DRIVER_ID = P_WEX_DRIVER_ID ---  parameter for wex Driver ID
                          AND DD.ROOT_ACCT_ROW_ID = P_DW_ACCT_ROW_ID ;

          END IF ;


      P_DW_ACCT_DRIVER_KEY := 99;
      P_DW_DRIVER_KEY := V_DW_DRIVER_KEY;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- --         DEFAULTS KEYS WHEN YOU CANNOT FIND ANY DATA for a given account number .

         P_DW_ACCT_DRIVER_KEY := 99;
   END LOOKUP_ROOT_ACCT_DRIVER_KEY;



   PROCEDURE LOADAUTHDATA (
      P_ACCTID                 IN FW_AUTH_DTL.WEX_ACCT_NBR%TYPE,
      P_ACQINSTID              IN FW_AUTH_DTL.ACQUIRER_INSTITUTION_ID%TYPE,
      P_ACTNCODE               IN FW_AUTH_DTL.AUTH_ACTION_CD%TYPE,
      P_ALERTCODE              IN FW_AUTH_DTL.ALERT_CD%TYPE,
      P_AUDITNBR               IN FW_AUTH_DTL.AUDIT_NBR%TYPE,
      P_AUTHAPPRVCODE          IN FW_AUTH_DTL.AUTH_APPRV_CODE%TYPE,
      P_AUTHHDRID              IN FW_AUTH_DTL.AUTHHDR_ID%TYPE,
      P_AUTHMSGID              IN NUMBER, -- Not needed  This needs to  be mapped
      P_BLADENM                IN FW_AUTH_DTL.BLADENM%TYPE,
      P_CAVENC                 IN FW_AUTH_DTL.VISIBLE_CARD_AUTH_VALUE%TYPE,
      P_CAVVISB                IN FW_AUTH_DTL.MAG_STRIPE_CARD_AUTH_VALUE%TYPE,
      P_CORRPRMPTCODE          IN FW_AUTH_DTL.CORR_PROMPT_CD%TYPE,
      P_CORRPRMPTNBR           IN FW_AUTH_DTL.CORR_PROMPT_NBR%TYPE,
      P_CRCYTYPE_ACCTBASIS     IN FW_AUTH_DTL.ACCT_CURRENCY_TYPE%TYPE,
      P_CRCYTYPE_MRCHBASIS     IN FW_AUTH_DTL.SITE_CURRENCY_TYPE%TYPE,
      P_CRCYTYPE_WEXBASIS      IN FW_AUTH_DTL.SITE_CURRENCY_TYPE%TYPE, -- This needs to be updated
      P_DAYOFYR                IN FW_AUTH_DTL.DAY_OF_YR%TYPE,
      P_DISPLPDID              IN VARCHAR2,        --  Not needed  FW_AUTH_DTL
      P_ERRMSG                 IN FW_AUTH_DTL.ERRMSG%TYPE,
      P_EXPDTMOYR              IN FW_AUTH_DTL.AUTH_PD_EXP_DT%TYPE,
      P_FUNCCODE               IN FW_AUTH_DTL.FUNC_CD%TYPE,
      P_GEOCODE                IN FW_AUTH_DTL.GEOGRAPHY_CD%TYPE,
      --  GROSSAMT_ACCTBASIS     IN FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT%TYPE,
      --  GROSSAMT_MRCHBASIS    IN FW_AUTH_DTL.SITE_TRANS_GROSS_AMT%TYPE,
      --  GROSSAMT_WEXBASIS     IN FW_AUTH_DTL.US_TRANS_GROSS_AMT%TYPE,
      P_LASTUPDTTMS            IN FW_AUTH_DTL.LAST_UPDT_TMS%TYPE,
      P_LOCALRSPTM             IN FW_AUTH_DTL.LOCALRSPTM%TYPE,
      P_LOCTRANDTTM            IN DATE, -- This needs to Poulated into  LOCAL_TRANS_Dt and LOCAL_TRANS_TM
      P_LOGQTM                 IN VARCHAR2, -- NOt needed for Auth DTL and AUT
      P_MSGRSNCODE             IN FW_AUTH_DTL.MSG_REASON_CD%TYPE,
      P_MSGRSPTYPE             IN FW_AUTH_DTL.MSGRSPTYPE%TYPE,
      P_MSGTYPE                IN FW_AUTH_DTL.MSG_TYPE%TYPE,
      P_OPTID                  IN FW_AUTH_DTL.OPT_ID%TYPE,
      P_PARENTPGMID            IN INTEGER,               -- this is not needed
      P_PDID                   IN FW_AUTH_DTL.PD_ID%TYPE,
      P_PDSEQNUM               IN FW_AUTH_DTL.PD_SEQ_NBR%TYPE,
      P_PGMID                  IN INTEGER,               -- THis is not needed
      P_POSAPPCONFIG           IN FW_AUTH_DTL.POS_APP_CONFIG%TYPE,
      P_POSDATACODE            IN FW_AUTH_DTL.POS_DATA_CD%TYPE,
      P_POSPRMPTCODE1          IN FW_AUTH_DTL.POS_PROMPT_CD1%TYPE,
      P_POSPRMPTCODE2          IN FW_AUTH_DTL.POS_PROMPT_CD2%TYPE,
      P_POSPRMPTCODE3          IN FW_AUTH_DTL.POS_PROMPT_CD3%TYPE,
      P_POSPRMPTDATA1          IN FW_AUTH_DTL.POS_PROMPT_DATA1%TYPE,
      P_POSPRMPTDATA2          IN FW_AUTH_DTL.POS_PROMPT_DATA2%TYPE,
      P_POSPRMPTDATA3          IN FW_AUTH_DTL.POS_PROMPT_DATA3%TYPE,
      P_POSTQTM                IN NUMBER,                        -- Not needed
      P_POSTTM                 IN NUMBER,                        -- Not needed
      P_PROCCODE               IN FW_AUTH_DTL.PROC_CD%TYPE,
      P_PROCQTM                IN NUMBER,                        -- Not needed
      P_PRODCODESETID          IN FW_AUTH_DTL.PROP_PROD_CD_SET_ID%TYPE,
      P_PRVTSITEUSAGE          IN FW_AUTH_DTL.PRIVATE_SITE_USAGE%TYPE,
      P_READTM                 IN NUMBER,                        -- Not needed
      P_REMOTEIP               IN FW_AUTH_DTL.REMOTE_IP%TYPE,
      P_REMOTEPORT             IN FW_AUTH_DTL.REMOTE_PORT%TYPE,
      P_RETVREFNUM             IN FW_AUTH_DTL.TICKET_NBR%TYPE,
      P_RFID                   IN VARCHAR2,              -- this is not needed
      P_SERVLEVL               IN CHAR,                  -- this is not needed
      P_SITEID                 IN FW_AUTH_DTL.SITE_ID%TYPE,
      P_TANDEMQTM              IN NUMBER,                -- This is not needed
      P_TANDEMRSPTM            IN FW_AUTH_DTL.TANDEMRSPTM%TYPE,
      P_TERMID                 IN FW_AUTH_DTL.TERMINAL_ID%TYPE,
      P_TERMPRFX               IN FW_AUTH_DTL.TERMINAL_PRFX%TYPE,
      P_TRACKTWO               IN FW_AUTH_DTL.TRACKTWO%TYPE,
      P_VERNBR                 IN FW_AUTH_DTL.MSG_VERSION_NBR%TYPE,
      P_WRITEQTM               IN NUMBER,                -- this is not needed
      P_WRITETM                IN NUMBER,                -- this is not needed
      P_AUTHDTLSEQNUM          IN FW_AUTH_DTL.AUTH_DTL_SEQ_NUM%TYPE,
      --  AUTHHDRID                     IN FW_AUTH_DTL.AUTHHDR_ID%TYPE,
      P_EXTPRODCODE            IN FW_AUTH_DTL.PROP_PROD_CD%TYPE,
      P_GROSSAMT_ACCTBASIS     IN FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT%TYPE,
      P_GROSSAMT_MRCHBASIS     IN FW_AUTH_DTL.SITE_ITEM_GROSS_AMT%TYPE,
      P_GROSSAMT_WEXBASIS      IN FW_AUTH_DTL.US_ITEM_GROSS_AMT%TYPE,
      P_INTRNPRODID            IN FW_AUTH_DTL.IND_STD_PROD_CD%TYPE,
      --  LASTUPDTTMS                IN TIMESTAMP ,             -- this is not needed
      --  MSGTYPE                         IN VARCHAR2 ,
      P_NACSCD                 IN FW_AUTH_DTL.NACSCD%TYPE,
      P_PRODCLSID              IN FW_AUTH_DTL.PROD_CLASS_ID%TYPE,
      P_QTY_ACCTBASIS          IN FW_AUTH_DTL.ACCT_ITEM_QTY%TYPE,
      P_QTY_MRCHBASIS          IN FW_AUTH_DTL.SITE_ITEM_QTY%TYPE,
      P_QTY_WEXBASIS           IN FW_AUTH_DTL.US_ITEM_QTY%TYPE,
      P_SVCLVL                 IN FW_AUTH_DTL.ITEM_SERVICE_LEVEL%TYPE,
      P_UNITPRC_ACCTBASIS      IN FW_AUTH_DTL.ACCT_ITEM_UNT_PRC%TYPE,
      P_UNITPRC_MRCHBASIS      IN FW_AUTH_DTL.SITE_ITEM_UNT_PRC%TYPE,
      P_UNITPRC_WEXBASIS       IN FW_AUTH_DTL.US_ITEM_UNT_PRC%TYPE,
      P_UOM_ACCTBASIS          IN FW_AUTH_DTL.ACCT_ITEM_UOM%TYPE,
      P_UOM_MRCHBASIS          IN FW_AUTH_DTL.SITE_ITEM_UOM%TYPE,
      P_UOM_WEXBASIS           IN FW_AUTH_DTL.US_ITEM_UOM%TYPE,
      P_SITE_TRANS_GROSS_AMT   IN FW_AUTH_DTL.SITE_TRANS_GROSS_AMT%TYPE,
      P_ACCT_TRANS_GROSS_AMT   IN FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT%TYPE,
      P_US_TRANS_GROSS_AMT     IN FW_AUTH_DTL.US_TRANS_GROSS_AMT%TYPE ,
      P_DRIVER_ROOTED_ACCTID                    IN VARCHAR2 ,
      P_DRIVER_CORECTED_PIN                     IN VARCHAR2 ,
      P_VEHICLE_ROOTED_ACCTID                   IN VARCHAR2 ,
      P_VEHICLE_CORRECTED_PIN                   In VARCHAR2 , 
      P_ORACLE_CODE                             OUT VARCHAR2   )
   IS
      C_PROCESS_CODE              VARCHAR2 (30) := 'LOADAUTHDATA';

      V_DW_CREATE_DT              DATE := SYSDATE;
      V_DW_LAST_UPDT_DT           DATE := SYSDATE;
      V_DW_LAST_UPDT_SESSION_NM   VARCHAR2 (100)
                                     := 'AUTHCAPTURE.LOADAUTHDATAPROC';
      V_DW_SOURCE_SYS             VARCHAR2 (30) := 'ODS';

      V_FW_AUTH_DTL_SEQ           NUMBER (15);

      V_DW_TIME_DAY_KEY           NUMBER (15);

      V_DW_POS_KEY                NUMBER (15);
      V_DW_SITE_KEY               NUMBER (15);
      V_DW_MRCH_KEY               NUMBER (15);
      V_DW_PRODUCT_KEY            NUMBER (15);

      V_DW_ACCT_KEY               NUMBER (15);
      V_DW_SPNR_PGM_KEY           NUMBER (15);

      V_DW_ACCT_ROW_ID            DW_ACCT.ACCT_ROW_ID%TYPE; -- OLD  ( this to Soure Account ROW _ID FROm AUTH )

      V_DW_ACCOUNT_ROW_ID         DW_ACCT.ACCT_ROW_ID%TYPE; -- NEW  ( This is to source from PD if we do not get it from AUTH )

      V_DW_RPT_ACCT_KEY           NUMBER (15) := 99;

      V_DW_PURCH_DEVICE_KEY       NUMBER (15);
      V_DW_ASSET_ROW_ID           DW_PURCH_DEVICE.ASSET_ROW_ID%TYPE;

      V_DW_DRIVER_KEY             FW_AUTH_DTL.DW_DRIVER_KEY%TYPE;
      V_DW_ASSET_KEY              FW_AUTH_DTL.DW_ASSET_KEY%TYPE;

      V_DW_ROOT_ACCT_DRIVER_KEY   NUMBER (15);

      V_DW_NETWORK_KEY            NUMBER (15) := 99;

      V_DW_DRIVER_ROW_ID          DW_DRIVER.DRIVER_ROW_ID%TYPE;

      V_WEX_DRIVER_ID             DW_DRIVER.WEX_DRIVER_ID%TYPE;

      V_PD_DW_SOURCE_SYS          DW_PURCH_DEVICE.DW_SOURCE_SYS%TYPE ; -- added for branching the logic

   BEGIN
      DBMS_APPLICATION_INFO.set_module (
         module_name   => c_process_code,
         action_name   => 'INSERT to FW_AUTH_DTL');


      /*  Lookup the Surrogate keys for the dimesions ...  */

      -- Sequence Look UP

      SELECT FW_AUTH_DTL_SEQ.NEXTVAL INTO V_FW_AUTH_DTL_SEQ FROM DUAL;

      -- Lookup product KEy
      LOOKUP_PRODUCT_KEY (P_NACSCD, V_DW_PRODUCT_KEY);

      -- Lookup Time Day Key
      LOOKUP_TIME_KEY (P_LOCTRANDTTM, V_DW_TIME_DAY_KEY);

      -- lookup  The pos_key , Site_Key and STKHLDR_KEY
      LOOKUP_POS_SITE_STKHLDR_KEY (P_TERMPRFX,
                                   P_TERMID,
                                   V_DW_POS_KEY,
                                   V_DW_SITE_KEY,
                                   V_DW_MRCH_KEY);


      -- lookup  DW_PURCh_DEVICE_KEY lookup

      LOOKUP_PD_KEY (P_PDID,
                     V_DW_PURCH_DEVICE_KEY,
                     V_DW_ASSET_ROW_ID,
                     V_DW_ACCOUNT_ROW_ID,
                     V_PD_DW_SOURCE_SYS);

      -- lookup  The Account Keys and Program Key

      LOOKUP_ACCT_PGM_KEY (P_ACCTID,
                           V_DW_ACCOUNT_ROW_ID,
                           V_DW_ACCT_KEY,
                           V_DW_SPNR_PGM_KEY,
                           V_DW_ACCT_ROW_ID);

      -- This is to lookup Driver Key and  ASSET KEY ...

      LOOKUP_DRIVER_ASSET_KEY (          --  P_ACCTID                        ,
                               V_DW_ACCT_ROW_ID,
                               V_DW_ASSET_ROW_ID,
                               --P_PD_ID                          ,
                               P_POSPRMPTCODE1,
                               P_POSPRMPTCODE2,
                               P_POSPRMPTCODE3,
                               P_POSPRMPTDATA1,
                               P_POSPRMPTDATA2,
                               P_POSPRMPTDATA3,
                               V_DW_DRIVER_KEY,
                               V_DW_ASSET_KEY,
                               V_WEX_DRIVER_ID);


      LOOKUP_ROOT_ACCT_DRIVER_KEY (V_DW_ACCT_ROW_ID,
                                   V_WEX_DRIVER_ID,
                                   V_PD_DW_SOURCE_SYS,
                                   P_DRIVER_ROOTED_ACCTID ,
                                   P_DRIVER_CORECTED_PIN ,
                                   V_DW_ROOT_ACCT_DRIVER_KEY,
                                   V_DW_DRIVER_KEY);


      INSERT INTO /*+append*/
                 FW_AUTH_DTL (FW_AUTH_DTL_KEY,
                              DW_ACCT_KEY,
                              DW_ASSET_KEY,
                              DW_CREATE_DT,                                 --
                              DW_DRIVER_KEY,
                              DW_LAST_UPDT_DT,                              --
                              DW_LAST_UPDT_SESSION_NM,                      --
                              DW_MRCH_KEY,
                              DW_NETWORK_KEY,
                              DW_POS_KEY,
                              DW_PRODUCT_KEY,
                              DW_PURCH_DEVICE_KEY,
                              DW_RPT_ACCT_KEY,
                              DW_SITE_KEY,
                              DW_SOURCE_SYS,                               ---
                              DW_SPNR_PGM_KEY,
                              DW_TIME_DAY_KEY,
                              DW_ROOT_ACCT_DRIVER_KEY,
                              FW_AUTH_DTL.WEX_ACCT_NBR,
                              FW_AUTH_DTL.ACQUIRER_INSTITUTION_ID,
                              FW_AUTH_DTL.AUTH_ACTION_CD,
                              FW_AUTH_DTL.ALERT_CD,
                              FW_AUTH_DTL.AUDIT_NBR,
                              FW_AUTH_DTL.AUTH_APPRV_CODE,
                              FW_AUTH_DTL.AUTHHDR_ID,                       --
                              FW_AUTH_DTL.BLADENM,
                              FW_AUTH_DTL.VISIBLE_CARD_AUTH_VALUE,
                              FW_AUTH_DTL.MAG_STRIPE_CARD_AUTH_VALUE,
                              FW_AUTH_DTL.CORR_PROMPT_CD,
                              FW_AUTH_DTL.CORR_PROMPT_NBR,
                              FW_AUTH_DTL.ACCT_CURRENCY_TYPE,
                              FW_AUTH_DTL.SITE_CURRENCY_TYPE,
                              FW_AUTH_DTL.DAY_OF_YR,                        --
                              FW_AUTH_DTL.ERRMSG,
                              FW_AUTH_DTL.AUTH_PD_EXP_DT,
                              FW_AUTH_DTL.FUNC_CD,
                              FW_AUTH_DTL.GEOGRAPHY_CD,
                              FW_AUTH_DTL.LAST_UPDT_TMS,
                              FW_AUTH_DTL.LOCALRSPTM,
                              FW_AUTH_DTL.LOCAL_TRANS_DT,
                              FW_AUTH_DTL.LOCAL_TRANS_TM,
                              FW_AUTH_DTL.MSG_REASON_CD,
                              FW_AUTH_DTL.MSGRSPTYPE,                       --
                              FW_AUTH_DTL.MSG_TYPE,
                              FW_AUTH_DTL.OPT_ID,
                              FW_AUTH_DTL.PD_ID,
                              FW_AUTH_DTL.PD_SEQ_NBR,
                              FW_AUTH_DTL.POS_APP_CONFIG,
                              FW_AUTH_DTL.POS_DATA_CD,
                              FW_AUTH_DTL.POS_PROMPT_CD1,
                              FW_AUTH_DTL.POS_PROMPT_CD2,
                              FW_AUTH_DTL.POS_PROMPT_CD3,
                              FW_AUTH_DTL.POS_PROMPT_DATA1,
                              FW_AUTH_DTL.POS_PROMPT_DATA2,
                              FW_AUTH_DTL.POS_PROMPT_DATA3,                 --
                              FW_AUTH_DTL.PROC_CD,
                              FW_AUTH_DTL.PROP_PROD_CD_SET_ID,
                              FW_AUTH_DTL.PRIVATE_SITE_USAGE,
                              FW_AUTH_DTL.REMOTE_IP,
                              FW_AUTH_DTL.REMOTE_PORT,
                              FW_AUTH_DTL.TICKET_NBR,
                              FW_AUTH_DTL.SITE_ID,
                              FW_AUTH_DTL.TANDEMRSPTM,
                              FW_AUTH_DTL.TERMINAL_ID,
                              FW_AUTH_DTL.TERMINAL_PRFX,
                              FW_AUTH_DTL.TRACKTWO,
                              FW_AUTH_DTL.MSG_VERSION_NBR,
                              FW_AUTH_DTL.AUTH_DTL_SEQ_NUM,
                              FW_AUTH_DTL.PROP_PROD_CD,
                              FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                              FW_AUTH_DTL.SITE_ITEM_GROSS_AMT,
                              FW_AUTH_DTL.US_ITEM_GROSS_AMT,
                              FW_AUTH_DTL.IND_STD_PROD_CD,
                              FW_AUTH_DTL.NACSCD,
                              FW_AUTH_DTL.PROD_CLASS_ID,
                              FW_AUTH_DTL.ACCT_ITEM_QTY,
                              FW_AUTH_DTL.SITE_ITEM_QTY,
                              FW_AUTH_DTL.US_ITEM_QTY,
                              FW_AUTH_DTL.ITEM_SERVICE_LEVEL,
                              FW_AUTH_DTL.ACCT_ITEM_UNT_PRC,
                              FW_AUTH_DTL.SITE_ITEM_UNT_PRC,
                              FW_AUTH_DTL.US_ITEM_UNT_PRC,
                              FW_AUTH_DTL.ACCT_ITEM_UOM,
                              FW_AUTH_DTL.SITE_ITEM_UOM,
                              FW_AUTH_DTL.US_ITEM_UOM,
                              FW_AUTH_DTL.AUTH_ACTION_DESC,
                              FW_AUTH_DTL.POINT_SRVC_CD,
                              FW_AUTH_DTL.MAG_STRIPE_POS_PROMPT_CD,
                              FW_AUTH_DTL.MAG_STRIPE_AUTH_CNTRL_NM,
                              FW_AUTH_DTL.SITE_TRANS_GROSS_AMT,
                              FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                              FW_AUTH_DTL.US_TRANS_GROSS_AMT)
           VALUES (V_FW_AUTH_DTL_SEQ,                       -- Create sequence
                   NVL (V_DW_ACCT_KEY, 99),
                   NVL (V_DW_ASSET_KEY, 99),
                   V_DW_CREATE_DT,
                   NVL (V_DW_DRIVER_KEY, 99),
                   V_DW_LAST_UPDT_DT,
                   V_DW_LAST_UPDT_SESSION_NM,
                   NVL (V_DW_MRCH_KEY, 99),
                   NVL (V_DW_NETWORK_KEY, 99),
                   NVL (V_DW_POS_KEY, 99),
                   NVL (V_DW_PRODUCT_KEY, 99),
                   NVL (V_DW_PURCH_DEVICE_KEY, 99),
                   NVL (V_DW_RPT_ACCT_KEY, 99),
                   NVL (V_DW_SITE_KEY, 99),
                   V_DW_SOURCE_SYS,
                   NVL (V_DW_SPNR_PGM_KEY, 99),
                   V_DW_TIME_DAY_KEY,
                   NVL (V_DW_ROOT_ACCT_DRIVER_KEY, 99),
                   NVL (P_ACCTID, ' '),
                   NVL (P_ACQINSTID, ' '),
                   NVL (P_ACTNCODE, ' '),
                   NVL (P_ALERTCODE, ' '),
                   NVL (P_AUDITNBR, ' '),
                   NVL (P_AUTHAPPRVCODE, ' '),
                   NVL (P_AUTHHDRID, 0),
                   NVL (P_BLADENM, ' '),
                   NVL (P_CAVENC, ' '),
                   NVL (P_CAVVISB, ' '),
                   NVL (P_CORRPRMPTCODE, ' '),
                   NVL (P_CORRPRMPTNBR, ' '),
                   NVL (P_CRCYTYPE_ACCTBASIS, ' '),
                   NVL (P_CRCYTYPE_MRCHBASIS, ' '),
                   NVL (P_DAYOFYR, 0),                                      --
                   NVL (P_ERRMSG, ' '),
                   NVL (P_EXPDTMOYR, ' '),
                   NVL (P_FUNCCODE, ' '),
                   NVL (P_GEOCODE, ' '),
                   NVL (P_LASTUPDTTMS, SYSDATE),
                   NVL (P_LOCALRSPTM, 0),
                   NVL (P_LOCTRANDTTM, SYSDATE),
                   TO_CHAR (P_LOCTRANDTTM, 'HH:mi:ss'),
                   NVL (P_MSGRSNCODE, ' '),                                 --
                   NVL (P_MSGRSPTYPE, ' '),
                   NVL (P_MSGTYPE, ' '),
                   NVL (P_OPTID, 0),
                   NVL (P_PDID, ' '),
                   NVL (P_PDSEQNUM, ' '),
                   NVL (P_POSAPPCONFIG, ' '),
                   NVL (P_POSDATACODE, ' '),
                   NVL (P_POSPRMPTCODE1, ' '),
                   NVL (P_POSPRMPTCODE2, ' '),
                   NVL (P_POSPRMPTCODE3, ' '),
                   NVL (P_POSPRMPTDATA1, ' '),
                   NVL (P_POSPRMPTDATA2, ' '),
                   NVL (P_POSPRMPTDATA3, ' '),                              --
                   NVL (P_PROCCODE, ' '),
                   NVL (P_PRODCODESETID, ' '),
                   NVL (P_PRVTSITEUSAGE, ' '),
                   NVL (P_REMOTEIP, ' '),
                   NVL (P_REMOTEPORT, ' '),
                   NVL (P_RETVREFNUM, ' '),
                   NVL (P_SITEID, ' '),
                   NVL (P_TANDEMRSPTM, 0),
                   NVL (P_TERMID, ' '),
                   NVL (P_TERMPRFX, ' '),
                   NVL (P_TRACKTWO, ' '),
                   NVL (P_VERNBR, ' '),
                   NVL (P_AUTHDTLSEQNUM, 0),
                   NVL (P_EXTPRODCODE, ' '),
                   NVL (P_GROSSAMT_ACCTBASIS, 0),
                   NVL (P_GROSSAMT_MRCHBASIS, 0),
                   NVL (P_GROSSAMT_WEXBASIS, 0),
                   NVL (P_INTRNPRODID, 0),
                   NVL (P_NACSCD, ' '),
                   NVL (P_PRODCLSID, 0),
                   NVL (P_QTY_ACCTBASIS, 0),
                   NVL (P_QTY_MRCHBASIS, 0),
                   NVL (P_QTY_WEXBASIS, 0),
                   NVL (P_SVCLVL, ' '),
                   NVL (P_UNITPRC_ACCTBASIS, 0),
                   NVL (P_UNITPRC_MRCHBASIS, 0),
                   NVL (P_UNITPRC_WEXBASIS, 0),
                   NVL (P_UOM_ACCTBASIS, ' '),
                   NVL (P_UOM_MRCHBASIS, ' '),
                   NVL (P_UOM_WEXBASIS, ' '),
                   '  ',                                   -- No data from ODS
                   '  ',                                   -- No data from ODS
                   '  ',                                   -- No data from ODS
                   '  ',                                   -- No data from ODS
                   NVL (P_SITE_TRANS_GROSS_AMT, 0),
                   NVL (P_ACCT_TRANS_GROSS_AMT, 0),
                   NVL (P_US_TRANS_GROSS_AMT, 0));

      -- COMMIT ; commented to check the issue with   ORA-02089 ..

      DBMS_APPLICATION_INFO.set_module (
         module_name   => c_process_code,
         action_name   => 'INSERT  COMMITED to FW_AUTH_DTL');
         
         P_ORACLE_CODE := '0'  ;
         
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE ('NO LOOKup KEYS FOUND');
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            SUBSTR ('ERROR: ' || SQLCODE || SQLERRM, 1, 250));
       --  RAISE;
          
         P_ORACLE_CODE := SUBSTR ('ERROR: ' || SQLCODE || SQLERRM, 1, 250);  
         DBMS_APPLICATION_INFO.set_module (
         module_name   => c_process_code,
         action_name   => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,
                                      1,
                                      250)));
   END LOADAUTHDATA;
END AUTHCAPTURE;
/