CREATE OR REPLACE PACKAGE BODY DW_OWNER.TRANSACTIONSEARCH
IS
/******************************************************************************
   NAME:    TRANSACTION  SEARCH 
   PURPOSE:  Using Account Number  and  ActionCode  and Time  interval  .

   REVISIONS:
   Ver        Date                     Author                                   Description
   ---------  ----------              ---------------                   ------------------------------------
   1.0       01/15/2010             Venu Kancharla                 TRANSACTION SEARCH  PROCEDURE 

  Supported Searches avaiable in v1.0 
  
  Account Number                       - Exact match on WEX_ACCT_NBR
  Sponsor Account Number          - Exact Match on SPNR_ACCT_NBR
  National ID                               - A Classic Platform Grouping ID stored in wex_acct_nbr
  
    -1.2     02/08/2012               Venu Kancharla                Added  New Index Hint and BIN for Account and PD
  
  -- 1.3 03/02/2012                 Venu Kancharla         Defect #26999 -  FIX      
  
  --1.4  03/06/2012                  Venu KAncharla      Defect # 27068   -  FIX
  
  --1.5  03/12/2012                  Venu KAncharla      Defect # 27010-  FIX
  
  -- 1.6  03/12/2012                 Venu  Kancharla   Defect # 27220 - FIX 
  
  --  1.7 03/28/2012                 Venu KAncharla   Defect#27498 -Fix 
  
******************************************************************************/

/******************************************************************************
 Transaction  Search By Account Number 
******************************************************************************/

PROCEDURE SearchByAccount(
                                            START_DT_in         IN     VARCHAR2,
                                             START_TM_in        IN     VARCHAR2,    
                                             END_DT_in             IN     VARCHAR2,
                                             END_TM_in             IN     VARCHAR2,
                                             Date_Type_in          IN     VARCHAR2,
                                             Timezone_in           IN      VARCHAR2,
                                             Acct_Nbr_in             IN     VARCHAR2,
                                             ACCT_TYPE_In        IN      VARCHAR2 , 
                                             Product_Type_in      IN      VARCHAR2,
                                             Unbilled_Only_in      IN       VARCHAR2,
                                             Start_Page_in          IN       INTEGER,
                                             Rows_Per_Page_in   IN       INTEGER,
                                             TransDetail_out        OUT    TransDtlListtyp
                                            )
IS

C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearchByaccount' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_Unbilled_Only_in                VARCHAR2(2) ;  
 V_DATE_TYPE_IN                  VARCHAR2(50) ; 
 V_PRODUCT_TYPE_IN            VARCHAR2(50) ; 
 V_BILL_CD                            VARCHAR2(50 ) ; 
 V_DATE                                DATE ; 
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByaccount '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                        ;
  V_Unbilled_Only_in              := NVL(UPPER(TRIM(Unbilled_Only_in)), 'N')        ;
  V_PRODUCT_TYPE_IN          := NVl(UPPER(PRODUCT_TYPE_IN), 'ALL')            ;
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  
  
    
   V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
  

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
     CASE WHEN   V_Unbilled_Only_in    = 'Y'  and V_DATE_TYPE_In = 'POSTING_DATE' 
          THEN 
                   SELECT   MAX(POST_FROM_DT)  ,  
                                             MAX(POST_TO_DT) 
                                 INTO 
                                            V_START_DTTM_IN , 
                                            V_END_DTTM_IN     
                                FROM  
                                            DW_ACCT_BILL_CYCLE,   
                                            DW_CYCLE_CALENDAR 
                               WHERE 
                                             DW_CYCLE_CALENDAR.CYCLE_CD             = DW_ACCT_BILL_CYCLE.CYCLE_CD
                                      and  DW_CYCLE_CALENDAR.SCHEDULED_RUN_DT > SYSDATE 
                                      and SYSDATE BETWEEN DW_CYCLE_CALENDAR.POST_FROM_DT and  DW_CYCLE_CALENDAR.POST_TO_DT    
                                      and DW_ACCT_BILL_CYCLE.ACCT_ROW_ID 
                                                                                                    IN  
                                                                                      (SELECT /*+ parallel (DW_ACCT, 12) */
                                                                                                                   DISTINCT(DW_ACCT.ACCT_ROW_ID)  
                                                                                                             FROM DW_ACCT
                                                                                                   WHERE   
                                                                                                            (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR   =   Acct_Nbr_in ) 
                                                                                                           OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR =   Acct_Nbr_in )
                                                                                                           OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                           OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                             ) 
                                                                                                            AND DW_ACCT.ARCHIVE_FLG = 'N'      
                                                                                    ) ;
                 WHEN   V_Unbilled_Only_in    = 'Y'  and V_DATE_TYPE_In = 'TRANS_DATE'
                 THEN                                                                                      
                         SELECT        MAX(BEGIN_AR_DT)  ,  
                                             MAX(END_AR_DT) 
                                 INTO 
                                            V_START_DTTM_IN , 
                                            V_END_DTTM_IN      
                                FROM  
                                            DW_ACCT_BILL_CYCLE,   
                                            DW_CYCLE_CALENDAR 
                               WHERE 
                                             DW_CYCLE_CALENDAR.CYCLE_CD             = DW_ACCT_BILL_CYCLE.CYCLE_CD
                                      and  DW_CYCLE_CALENDAR.SCHEDULED_RUN_DT > SYSDATE 
                                      and SYSDATE BETWEEN DW_CYCLE_CALENDAR.POST_FROM_DT and  DW_CYCLE_CALENDAR.POST_TO_DT    
                                      and DW_ACCT_BILL_CYCLE.ACCT_ROW_ID 
                                                                                                    IN  
                                                                                    (SELECT /*+ parallel (DW_ACCT, 12) */
                                                                                                                   DISTINCT(DW_ACCT.ACCT_ROW_ID)  
                                                                                                             FROM DW_ACCT
                                                                                                   WHERE   
                                                                                                            (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR   =   Acct_Nbr_in ) 
                                                                                                           OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR =   Acct_Nbr_in )
                                                                                                           OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                           OR   (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                             ) 
                                                                                                            AND DW_ACCT.ARCHIVE_FLG = 'N'      
                                                                                    ) ;
                   ELSE 
                             SELECT SYSDATE INTO V_DATE FROm DUAL ;                                                                     
                   END CASE ;                                                        
         
       
   CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN                                                                   
                  OPEN TransDetail_out FOR
                           SELECT                                 
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DATE ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                      CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD, 
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                       AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES            
                                          FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                    ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                               ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT  
                                                               FROM                                                                  
                                               (SELECT     /*+     parallel (AM_PURCH_TRANS_ITEM , 12 )  INDEX (AM_PURCH_TRANS_ITEM AM_PURCH_TRANS_ITEM_IE33) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          , 
                                                                      TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)     LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                           AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                              AND     V_End_Dttm_In 
                                                                        AND  AM_PURCH_TRANS_ITEM.DW_ACCT_KEY 
                                                                                                                                        IN         --  These are types of accounts which could be fed in ... 
                                                                                                                                           (SELECT  
                                                                                                                                                             DW_ACCT_KEY
                                                                                                                                                  FROM   DW_ACCT
                                                                                                                                                 WHERE   
                                                                                                                                                             ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR   =   Acct_Nbr_in ) 
                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR =   Acct_Nbr_in )
                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                        OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                              ) 
                                                                                                                                                          AND DW_ACCT.ARCHIVE_FLG = 'N'            
                                                                                                                                                )                                                                                                                                                  
                                                                         AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY
                                                                                                                                   IN  
                                                                                                                                    (  SELECT    -- Use this technique to avoid too much redundant coding ... 
                                                                                                                                                   DW_PRODUCT_KEY 
                                                                                                                                        FROM  DW_PRODUCT
                                                                                                                                        WHERE    
                                                                                                                                                    (V_PRODUCT_TYPE_In = 'FUEL'             AND   NVL(TRIM(BILL_CD), 'FUL') IN ( 'FUL') ) 
                                                                                                                                             OR   (V_PRODUCT_TYPE_In = 'NON-FUEL'     AND   NVL(TRIM(BILL_CD), 'FUL')  NOT IN ( 'FUL'))
                                                                                                                                             OR   (V_PRODUCT_TYPE_In = 'ALL'               AND   NVL(TRIM(BILL_CD), 'FUL')   IN ( 'FUL','OTH', 'SER')    )                
                                                                                                                                        )  
                                                                         
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
                                                                                                                             
WHEN V_DATE_TYPE_IN = 'TRANS_DATE'                                                                                                                              
THEN                                                                                                                                                                                                                                           
         OPEN TransDetail_out FOR
                           SELECT                                                    
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                       AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                               CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES     
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                    ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                              ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT  
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 14 )   INDEX (AM_PURCH_TRANS_ITEM AM_PURCH_TRANS_ITEM_IE33) */ 
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        , 
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,  
                                                                      TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                               AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                              AND     V_End_Dttm_In            
                                                                          AND  AM_PURCH_TRANS_ITEM.DW_ACCT_KEY 
                                                                                                                                        IN         --  These are types of accounts which could be fed in ... 
                                                                                                                                           (SELECT 
                                                                                                                                                             DW_ACCT_KEY
                                                                                                                                                  FROM   DW_ACCT
                                                                                                                                                 WHERE   
                                                                                                                                                       (      (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR   =   Acct_Nbr_in ) 
                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR =   Acct_Nbr_in )
                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                        OR   (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                        )
                                                                                                                                                      AND DW_ACCT.ARCHIVE_FLG = 'N'
                                                                                                                                                )                                                                                                                                                                                                                                                              
                                                                         AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY
                                                                                                                                   IN  
                                                                                                                                    (  SELECT   -- Use this technique to avoid too much redundant coding ... 
                                                                                                                                                   DW_PRODUCT_KEY 
                                                                                                                                        FROM  DW_PRODUCT
                                                                                                                                        WHERE    
                                                                                                                                                    (V_PRODUCT_TYPE_In = 'FUEL'             AND   NVL(TRIM(BILL_CD), 'FUL') IN ( 'FUL') ) 
                                                                                                                                             OR   (V_PRODUCT_TYPE_In = 'NON-FUEL'     AND   NVL(TRIM(BILL_CD), 'FUL')  NOT IN ( 'FUL'))
                                                                                                                                             OR   (V_PRODUCT_TYPE_In = 'ALL'               AND   NVL(TRIM(BILL_CD), 'FUL')   IN ( 'FUL','OTH', 'SER')    )                
                                                                                                                                        )  
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;



END CASE ; 


EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 
END SearchByAccount ;

PROCEDURE SearchByPurchaseDevice(START_DT_in              IN     VARCHAR2,
                                                       START_TM_in              IN     VARCHAR2,    
                                                       END_DT_in                   IN     VARCHAR2,
                                                       END_TM_in                   IN     VARCHAR2,
                                                       Date_Type_in                IN     VARCHAR2,
                                                       Timezone_in                 IN     VARCHAR2,
                                                       Acct_Nbr_in                  IN     VARCHAR2,
                                                       ACCT_TYPE_In              IN     VARCHAR2 , 
                                                       Purch_Device_Nbr_in     IN     VARCHAR2,
                                                       Start_Page_in               IN     INTEGER,
                                                       Rows_Per_Page_in        IN     INTEGER,
                                                       TransDetail_out            OUT   TransDtlListtyp
                                                      )
                                                       
IS

C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByPurchaseDevice' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
  
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByPurchaseDevice '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  
  
  
   V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
  

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                               ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT  
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,  
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT        , 
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'POSTING_DATE' and  AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                  AND     V_End_Dttm_In 
                                                                        AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY 
                                                                                                                                                            IN 
                                                                                                                                          (SELECT /*+ parallel (DPD, 12) */
                                                                                                                                                       DW_PURCH_DEVICE_KEY 
                                                                                                                                           FROM     DW_PURCH_DEVICE DPD  
                                                                                                                                              WHERE  DPD.EMBOSS_PD_NBR    =  Purch_Device_Nbr_in 
                                                                                                                                               AND  DPD.ACCT_ROW_ID     IN  
                                                                                                                                                                                  (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                                                               DA.ACCT_ROW_ID
                                                                                                                                                                                     FROM DW_ACCT DA 
                                                                                                                                                                                    WHERE  
                                                                                                                                                                                             ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                                                        ) 
                                                                                                                                                                                         AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                                                                   )   
                                                                                                                                            )                                                                                                                                                                                           
                                                                                                                                                
                                                                                                                                                
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT , 
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                       AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                               CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                          ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                               ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT  
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'TRANS_DATE' and  AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                        AND     V_End_Dttm_In 
                                                                        AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY 
                                                                                                                                                            IN 
                                                                                                                                          (SELECT /*+ parallel (DPD, 12) */
                                                                                                                                                       DW_PURCH_DEVICE_KEY 
                                                                                                                                           FROM     DW_PURCH_DEVICE DPD  
                                                                                                                                              WHERE  DPD.EMBOSS_PD_NBR    =  Purch_Device_Nbr_in 
                                                                                                                                               AND  DPD.ACCT_ROW_ID     IN  
                                                                                                                                                                                  (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                                                               DA.ACCT_ROW_ID
                                                                                                                                                                                     FROM DW_ACCT DA 
                                                                                                                                                                                    WHERE  
                                                                                                                                                                                        (      (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                                                         ) 
                                                                                                                                                                                       AND DA.ARCHIVE_FLG = 'N'
                                                                                                                                                                                   )   
                                                                                                                                            )                                                                                                                                                                                           
                                                                                                                                                
                                                                                                                                                
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
 
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 
END SearchByPurchaseDevice ;
PROCEDURE SearchByAsset (            
                                         START_DT_in             IN     VARCHAR2,
                                         START_TM_in            IN      VARCHAR2,    
                                         END_DT_in                 IN      VARCHAR2,
                                         END_TM_in                 IN      VARCHAR2,
                                         Date_Type_in              IN      VARCHAR2,
                                         Timezone_in               IN      VARCHAR2,
                                         Acct_Nbr_in                IN      VARCHAR2,
                                         ACCT_TYPE_In            IN      VARCHAR2 , 
                                         Vin_in                         IN      VARCHAR2,
                                         Custom_Asset_Id_in     IN      VARCHAR2,
                                         Vehicle_Prompt_Id_in   IN      VARCHAR2,
                                         Start_Page_in              IN      INTEGER,
                                         Rows_Per_Page_in       IN      INTEGER,
                                         TransDetail_out           OUT   TransDtlListtyp
                                                      )
                                                       
IS


C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByAsset' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
 V_VIN_IN                                             DW_ASSET.VIN%TYPE ; 
 V_CUSTOM_ASSET_ID_IN                     DW_ASSET.CUST_ASSET_NBR%TYPE ;  
 V_VEHICLE_PROMPT_ID_IN                   DW_ASSET.WEX_ASSET_ID%TYPE    ;                                             

 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByAsset '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  

 V_VIN_IN                                     := TRIM (VIN_IN) ;  
 V_CUSTOM_ASSET_ID_IN             := TRIM (CUSTOM_ASSET_ID_in ) ; 
 V_VEHICLE_PROMPT_ID_IN           := TRIM(VEHICLE_PROMPT_ID_in) ; 
  
 IF V_CUSTOM_ASSET_ID_IN  IS NOT NULL 
 THEN 
      V_CUSTOM_ASSET_ID_IN := V_CUSTOM_ASSET_ID_IN||'%' ;
END IF ;          
   
   V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
  

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT                                
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                         POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                   ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                               ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT  
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        , 
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,  
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'POSTING_DATE' and  AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                  AND     V_End_Dttm_In 
                                                                        AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY 
                                                                                                                                        IN 
                                                                                                                                   (   SELECT /*+ parallel (DA, 12) */  DA.DW_ASSET_KEY 
                                                                                                                                        FROM   DW_ASSET DA    
                                                                                                                                        WHERE
                                                                                                                                                 DA.ARCHIVE_FLG = 'N'
                                                                                                                                         AND          
                                                                                                                                                (V_VIN_IN IS NOT NULL AND  DA.VIN =  V_VIN_IN ) 
                                                                                                                                          OR ( V_VEHICLE_PROMPT_ID_IN IS NOT NULL AND DA.WEX_ASSET_ID =   V_VEHICLE_PROMPT_ID_IN )
                                                                                                                                          OR  (V_CUSTOM_ASSET_ID_IN IS NOT NULL AND DA.CUST_ASSET_NBR like V_CUSTOM_ASSET_ID_IN  )     
                                                                                                                                    )          
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                           (       (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                            AND   DA.ARCHIVE_FLG = 'N' 
                                                                                                                                       )   
                                                                         )   A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT                                  
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                         POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                   ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                      AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                     TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                     TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'TRANS_DATE' and  AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                        AND     V_End_Dttm_In 
                                                                     AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY 
                                                                                                                                        IN 
                                                                                                                                   (   SELECT /*+ parallel (DA, 12) */  DA.DW_ASSET_KEY 
                                                                                                                                        FROM   DW_ASSET DA    
                                                                                                                                        WHERE
                                                                                                                                                  DA.ARCHIVE_FLG = 'N' 
                                                                                                                                        AND 
                                                                                                                                                (V_VIN_IN IS NOT NULL AND  DA.VIN =  V_VIN_IN ) 
                                                                                                                                          OR ( V_VEHICLE_PROMPT_ID_IN IS NOT NULL AND DA.WEX_ASSET_ID =   V_VEHICLE_PROMPT_ID_IN )
                                                                                                                                          OR  (V_CUSTOM_ASSET_ID_IN IS NOT NULL AND DA.CUST_ASSET_NBR like V_CUSTOM_ASSET_ID_IN  )     
                                                                                                                                    )          
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                                 ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                       )    
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
 
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 
END SearchByAsset ;

PROCEDURE SearchByDriver(START_DT_in                     IN     VARCHAR2,
                                          START_TM_in                    IN     VARCHAR2,    
                                          END_DT_in                         IN     VARCHAR2,
                                          END_TM_in                         IN     VARCHAR2,
                                          Date_Type_in                      IN     VARCHAR2,
                                          Timezone_in                       IN      VARCHAR2,
                                          Acct_Nbr_in                        IN     VARCHAR2,
                                          ACCT_TYPE_In                   IN      VARCHAR2 , 
                                          Driver_Prompt_Id_in           IN     VARCHAR2,
                                          Driver_First_Name_in          IN     VARCHAR2,
                                          Driver_Last_Name_in           IN     VARCHAR2,
                                          Driver_Middle_Name_in        IN     VARCHAR2,
                                          Company_Driver_Id_in         IN     VARCHAR2,
                                          Start_Page_in                     IN     INTEGER,
                                          Rows_Per_Page_in              IN     INTEGER,
                                          TransDetail_out                  OUT    TransDtlListtyp)
IS 


C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByDriver' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 
 V_DRIVER_ID_IN                     DW_DRIVER.WEX_DRIVER_ID%TYPE ; 
 V_DRIVER_FIRST_NAME_IN     DW_DRIVER.DRIVER_FIRST_NM%TYPE ; 
 V_DRIVER_LAST_NAME_IN      DW_DRIVER.DRIVER_LAST_NM%TYPE ;
 V_DRIVER_MIDDLE_NAME_IN   DW_DRIVER.DRIVER_MIDDLE_NM%TYPE ; 

                                          

 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByDriver '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  

 V_DRIVER_ID_IN                    := TRIM(DRIVER_PROMPT_ID_IN) ;  
 V_DRIVER_FIRST_NAME_IN    := UPPER(TRIM(DRIVER_FIRST_NAME_IN)) ; 
 V_DRIVER_LAST_NAME_IN     := UPPER(TRIM(DRIVER_LAST_NAME_IN)) ;  
 V_DRIVER_MIDDLE_NAME_IN  := UPPER(TRIM(DRIVER_MIDDLE_NAME_IN)) ; 
 
 IF V_DRIVER_FIRST_NAME_IN IS NOT NULL 
 THEN 
       V_DRIVER_FIRST_NAME_IN  := V_DRIVER_FIRST_NAME_IN ||'%' ; 
 END IF ; 
 
 IF V_DRIVER_LAST_NAME_IN  IS NOT NULL 
 THEN 
        V_DRIVER_LAST_NAME_IN := V_DRIVER_LAST_NAME_IN || '%' ; 
 END IF ;        
        
 IF V_DRIVER_MIDDLE_NAME_IN  IS NOT NULL 
 THEN 
        V_DRIVER_MIDDLE_NAME_IN := V_DRIVER_MIDDLE_NAME_IN || '%' ; 
 END IF ;        
 
 
 
 
    
    V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                         POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                               CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                   ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                     AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR    ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                     TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'POSTING_DATE' and  AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                  AND     V_End_Dttm_In 
                                                                        AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY 
                                                                                                                                        IN 
                                                                                                                                ( SELECT /*+  parallel (DD, 12) FULL (DD) */  DD.DW_DRIVER_KEY
                                                                                                                                    FROM   DW_DRIVER DD 
                                                                                                                                  WHERE
                                                                                                                                                DD.ARCHIVE_FLG   = 'N' 
                                                                                                                                     AND            
                                                                                                                                         (     (   DD.WEX_DRIVER_ID  =  V_DRIVER_ID_IN  and V_DRIVER_ID_IN is not null  )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                            AND V_DRIVER_FIRST_NAME_IN           IS  NULL    
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL
                                                                                                                                                                                               
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )  
                                                                                                                                            OR 
                                                                                                                                              ( V_DRIVER_ID_IN  IS NULL      and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS   NULL    
                                                                                                                                               )     
                                                                                                                                            OR  ( V_DRIVER_ID_IN  IS NULL and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                               
                                                                                                                                               )    
                                                                                                                                         )                                       
                                                                                                                                 )                        
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12)  */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                            (     (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                       )   
                                                                         )   A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT                                 
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                      AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                      TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'TRANS_DATE' and  AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                         AND     V_End_Dttm_In 
                                                                     AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY 
                                                                                                                                        IN 
                                                                                                                                ( SELECT /*+ parallell (DD, 12 )   FULL (DD) */  
                                                                                                                                               DD.DW_DRIVER_KEY
                                                                                                                                    FROM   DW_DRIVER DD 
                                                                                                                                  WHERE 
                                                                                                                                             DD.ARCHIVE_FLG   = 'N' 
                                                                                                                                     AND        
                                                                                                                                          (     (   DD.WEX_DRIVER_ID  =  V_DRIVER_ID_IN  and V_DRIVER_ID_IN is not null  )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS   NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )  
                                                                                                                                            OR 
                                                                                                                                              ( V_DRIVER_ID_IN  IS NULL      and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS   NULL    
                                                                                                                                               )     
                                                                                                                                            OR  ( V_DRIVER_ID_IN  IS NULL and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL    
                                                                                                                                               )    
                                                                                                                                         )                                       
                                                                                                                                 )                   
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12)   */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                            (      (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )    
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
 
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 END SearchByDriver ; 
PROCEDURE SearchByCardDepartment(START_DT_in         IN     VARCHAR2,
                                                         START_TM_in        IN     VARCHAR2,    
                                                         END_DT_in             IN     VARCHAR2,
                                                         END_TM_in             IN     VARCHAR2,
                                                         Date_Type_in          IN     VARCHAR2,
                                                         Timezone_in           IN      VARCHAR2,
                                                         Acct_Nbr_in             IN     VARCHAR2,
                                                         ACCT_TYPE_In        IN      VARCHAR2 , 
                                                        Card_Dept_in           IN     VARCHAR2,
                                                        Start_Page_in           IN     INTEGER,
                                                        Rows_Per_Page_in     IN     INTEGER,
                                                        TransDetail_out      OUT    TransDtlListtyp
                                                       )
    IS 
C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByCardDepartment' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 
 V_PURCH_DEVICE_DEPT_IN    DW_DEPT.DEPT_NM%TYPE ;  
                                          
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByDriver '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  
  V_PURCH_DEVICE_DEPT_IN := UPPER(TRIM(CARD_DEPT_IN ))  ;  
 

 
 IF V_PURCH_DEVICE_DEPT_IN IS NOT NULL 
 THEN 
      V_PURCH_DEVICE_DEPT_IN  := V_PURCH_DEVICE_DEPT_IN ||'%' ; 
 END IF ; 
  
    
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
   V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT        /*+  parallel (DW_PURCH_DEVICE , 12 ) */                                
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                       DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                               CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                     ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                      AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                     TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'POSTING_DATE' and  AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                  AND     V_End_Dttm_In 
                                                                     AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY 
                                                                                                                       in
                                                                                                       (select /*+ parallel (dpd, 12) */ DPD.DW_PURCH_DEVICE_KEY  
                                                                                                                                     from DW_PURCH_DEVICE  dpd 
                                                                                                                                    where DPD.DEPT_ROW_ID IN  
                                                                                                                                                                  (SELECT /*+ parallel (dd, 12) */  DEPT_ROW_ID 
                                                                                                                                                                      FROM   DW_DEPT DD 
                                                                                                                                                                   WHERE   UPPER(DD.DEPT_NM)  like V_PURCH_DEVICE_DEPT_IN
                                                                                                                                                                  )      
                                                                                                          )                              
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                              (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )    
                                                                         )   A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                        DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                     ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                      AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'TRANS_DATE' and  AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                         AND     V_End_Dttm_In 
                                                                      AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY 
                                                                                                                       in
                                                                                                       (select /*+ parallel (dpd, 12) */ DPD.DW_PURCH_DEVICE_KEY  
                                                                                                                                     from DW_PURCH_DEVICE  dpd 
                                                                                                                                    where DPD.DEPT_ROW_ID IN  
                                                                                                                                                                  (SELECT /*+ parallel (dd, 12) */  DEPT_ROW_ID 
                                                                                                                                                                      FROM   DW_DEPT DD 
                                                                                                                                                                   WHERE   UPPER(DD.DEPT_NM)  like V_PURCH_DEVICE_DEPT_IN
                                                                                                                                                                  )      
                                                                                                          )                              
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                              (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )    
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 END SearchByCardDepartment ;                                                    
                                                        
PROCEDURE SearchByRegion(START_DT_in         IN     VARCHAR2,
                                           START_TM_in        IN     VARCHAR2,    
                                           END_DT_in             IN     VARCHAR2,
                                           END_TM_in             IN     VARCHAR2,
                                           Date_Type_in          IN     VARCHAR2,
                                           Timezone_in           IN      VARCHAR2,
                                           Acct_Nbr_in             IN     VARCHAR2,
                                           ACCT_TYPE_In        IN      VARCHAR2 , 
                                           Site_City_in             IN     VARCHAR2,
                                           Site_State_in           IN     VARCHAR2,
                                           Site_Zip_in              IN     VARCHAR2,
                                           Start_Page_in          IN     INTEGER,
                                           Rows_Per_Page_in   IN     INTEGER,
                                           TransDetail_out      OUT    TransDtlListtyp)
    IS 
C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByRegion' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 
 V_SITE_STATE_CD_IN            DW_ADDR.ADDR_STATE_PROV_CD%TYPE ; 
 V_SITE_CITY_IN                    DW_ADDR.ADDR_CITY%TYPE ; 
 V_SITE_ZIP_IN                       DW_ADDR.ADDR_ZIP_POSTAL_CD%TYPE ;    
                                          
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByRegion '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  
  
  
   V_SITE_STATE_CD_IN         := UPPER(TRIM(SITE_STATE_IN)) ; 
   V_SITE_CITY_IN                  := UPPER(TRIM(SITE_CITY_IN))  ; 
   V_SITE_ZIP_IN                    :=            TRIM(SITE_ZIP_IN) ;      

 
   IF V_SITE_CITY_IN  IS NOT NULL 
   THEN   
                 V_SITE_CITY_IN := V_SITE_CITY_IN ||'%'  ;
   ELSE 
                 V_SITE_CITY_IN := V_SITE_CITY_IN ;               
   END IF ; 
  
  IF  V_SITE_ZIP_IN IS NOT NULL 
  THEN                
            V_SITE_ZIP_IN   := V_SITE_ZIP_IN||'%' ;
   ELSE 
           V_SITE_ZIP_IN    :=  V_SITE_ZIP_IN        ;  
  END IF ;     


    
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
  

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT                                  
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                        NATIONAL_ID ,  
                                        DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                         POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                    ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                               ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT  /*+   parallel (AM_PURCH_TRANS_ITEM , 14 )    */  
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                           AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                           AND     V_End_Dttm_In
                                                                        AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT   /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                              (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )  AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )                                                                                                                              
                                                                        AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY 
                                                                                                                            IN
                                                                                                                             ( SELECT /*+ parallel (DS , 12)  */   
                                                                                                                                           DW_SITE_KEY 
                                                                                                                                  FROM DW_SITE DS 
                                                                                                                               WHERE 
                                                                                                                                         DS.ARCHIVE_FLG = 'N'  
                                                                                                                                AND   DS.PHYS_ADDR_ROW_ID 
                                                                                                                                                                        IN
                                                                                                                                                                     ( SELECT /*+ parallel (DA, 12 ) */  DA.ADDR_ROW_ID  
                                                                                                                                                                       FROm DW_ADDR DA  
                                                                                                                                                                       WHERE 
                                                                                                                                                                                   DA.DW_CURRENT_FLG = 'Y' 
                                                                                                                                                                          AND (     (  UPPER( DA.ADDR_STATE_PROV_CD)    IN  ( V_SITE_STATE_CD_IN )  and            V_SITE_CITY_IN   IS  NULL                     and V_SITE_ZIP_IN IS NULL )   
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN  ( V_SITE_STATE_CD_IN )  and UPPER( DA.ADDR_CITY)    like  V_SITE_CITY_IN  and V_SITE_ZIP_IN IS NULL )  
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  UPPER(DA.ADDR_CITY )   like  V_SITE_CITY_IN  and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN)
                                                                                                                                                                                 OR  ( UPPER(  DA.ADDR_CITY )                     like  V_SITE_CITY_IN              and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_ZIP_IN IS NULL ) 
                                                                                                                                                                                 OR  (  DA.ADDR_ZIP_POSTAL_CD                  like  V_SITE_ZIP_IN                and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                  OR ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN  and  V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                 )
                                                                                                                                                                        )
                                                                                                                                  )                     
                                                                         )   A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT                                 
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                        DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                         POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                       AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                     ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT  /*+     parallel (AM_PURCH_TRANS_ITEM , 14 )    */
                                                                  AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                          V_DATE_TYPE_IN = 'TRANS_DATE' and  AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                                                                        AND     V_End_Dttm_In 
                                                                      AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY 
                                                                                                                            IN
                                                                                                                             ( SELECT /*+ parallel (DS , 12)  */  DW_SITE_KEY 
                                                                                                                                  FROM DW_SITE DS 
                                                                                                                               WHERE DS.PHYS_ADDR_ROW_ID 
                                                                                                                                                                        IN
                                                                                                                                                                   ( SELECT /*+ parallel (DA, 12 ) */  DA.ADDR_ROW_ID  
                                                                                                                                                                       FROm DW_ADDR DA  
                                                                                                                                                                       WHERE 
                                                                                                                                                                                   DA.DW_CURRENT_FLG = 'Y' 
                                                                                                                                                                          AND (     (  UPPER( DA.ADDR_STATE_PROV_CD)    IN  ( V_SITE_STATE_CD_IN )  and            V_SITE_CITY_IN   IS  NULL                     and V_SITE_ZIP_IN IS NULL )   
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN  ( V_SITE_STATE_CD_IN )  and UPPER( DA.ADDR_CITY)    like  V_SITE_CITY_IN  and V_SITE_ZIP_IN IS NULL )  
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  UPPER(DA.ADDR_CITY )   like  V_SITE_CITY_IN  and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN)
                                                                                                                                                                                 OR  ( UPPER(  DA.ADDR_CITY )                     like  V_SITE_CITY_IN              and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_ZIP_IN IS NULL ) 
                                                                                                                                                                                 OR  (  DA.ADDR_ZIP_POSTAL_CD                  like  V_SITE_ZIP_IN                and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                  OR ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN  and  V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                 )
                                                                                                                                                                        )
                                                                                                                                  )                     
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                              (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            ) AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )    
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 END SearchByRegion  ; 

PROCEDURE SearchByAmount(START_DT_in         IN     VARCHAR2,
                                             START_TM_in        IN     VARCHAR2,    
                                             END_DT_in             IN     VARCHAR2,
                                             END_TM_in             IN     VARCHAR2,
                                             Date_Type_in          IN     VARCHAR2,
                                             Timezone_in           IN      VARCHAR2,
                                             Acct_Nbr_in             IN     VARCHAR2,
                                             ACCT_TYPE_In        IN      VARCHAR2 , 
                                            Gross_Amt_in           IN     NUMBER,
                                            Nbr_Trans_Units_in   IN     NUMBER,
                                            Price_Per_Unit_in      IN     NUMBER,
                                            Start_Page_in           IN     INTEGER,
                                            Rows_Per_Page_in   IN     INTEGER,
                                            TransDetail_out      OUT    TransDtlListtyp
                                           )
IS 
C_PROCESS_CODE                VARCHAR2 (100) := 'TransactionSearch.SearchByAmount' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 
 V_DATE_TYPE_IN                  VARCHAR2(50)  ;  

 V_DATE                                DATE ;
               
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 
V_GROSS_DOLLAR_AMT_IN      AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT%TYPE   ;   
V_ACCT_PPU_AMT                   AM_PURCH_TRANS_ITEM.ACCT_PPU_AMT%TYPE ; 
V_ACCT_NBR_TRANS_UNITS   AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS%TYPE ;  
                                          
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR TRANSACTIONSEARCH.SearchByAmount '
                                  );

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;
  V_DATE_TYPE_IN                :=  NVL(UPPER(DATE_TYPE_IN) , 'TRANS_DATE')  ;  
  
  
  V_GROSS_DOLLAR_AMT_IN  :=  NVL(GROSS_AMT_in , 0)  ; 
  V_ACCT_PPU_AMT               :=   NVl(Price_Per_Unit_in , 0) ; 
  V_ACCT_NBR_TRANS_UNITS :=   NVL(Nbr_Trans_Units_in, 0) ; 
 

    V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
  

    CASE WHEN  START_PAGE_IN  >= 1   
    THEN 
            V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
            V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
     ELSE 
         V_START_RECORD := 1 ; 
    END CASE ;                 
         
 select sysdate Into V_DATE FROm DUAL ;
    
    CASE WHEN V_DATE_TYPE_IN = 'POSTING_DATE' 
            THEN    
                    OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                        DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                        AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                                CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT                    ,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                       TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                            AM_PURCH_TRANS_ITEM.POSTING_DT  BETWEEN V_Start_Dttm_In  AND     V_End_Dttm_In 
                                                                      AND   (    ( AM_PURCH_TRANS_ITEM. ACCT_TRANS_GROSS_AMT   >= V_GROSS_DOLLAR_AMT_IN     and  V_ACCT_NBR_TRANS_UNITS =  0  and V_ACCT_PPU_AMT = 0  )
                                                                              OR ( AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS       >=  V_ACCT_NBR_TRANS_UNITS and   V_GROSS_DOLLAR_AMT_IN   = 0  and V_ACCT_PPU_AMT = 0  )  
                                                                              OR ( AM_PURCH_TRANS_ITEM.ACCT_PPU_AMT                    >=  V_ACCT_PPU_AMT                and  V_ACCT_NBR_TRANS_UNITS =  0   and V_GROSS_DOLLAR_AMT_IN = 0  ) 
                                                                             )           
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                            (    (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )  AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                       )   
                                                                         )   A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 
  WHEN V_DATE_TYPE_IN = 'TRANS_DATE'
   THEN 
          OPEN TransDetail_out FOR
                           SELECT     /*+  parallel (DW_PURCH_DEVICE , 12 ) */                             
                                       DW_ACCT.ACCT_NM                                                   ACCOUNT_NM,
                                       DW_ACCT.L1_WEX_ACCT_NBR                                         NATIONAL_ID ,  
                                        DW_ACCT.BANK_ID_NBR||DW_ACCT.SPNR_ACCT_NBR  SPNR_ACCT_NBR,         -- WEX Program Account NBR
                                       DW_ACCT.CUSTOM_ACCT_NBR                                   CUSTOM_ACCT_NBR,
                                       DW_PURCH_DEVICE.BANK_ID_NBR||DW_PURCH_DEVICE.EMBOSS_PD_NBR                          CARD_NUMBER,
                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                       AM_PURCH_TRANS_ITEM.DRIVER_PROMPT_ID              DRIVER_PROMPT_ID,
                                       DW_ASSET.CUST_ASSET_NBR                                    CUST_ASSET_NBR,
                                       DW_ASSET.VIN                                                          VIN,      
                                       AM_PURCH_TRANS_ITEM.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                       DW_SITE.MRCH_SITE_ID                                             SITE_ID,
                                       DW_POS.LEGACY_WEX_SITE_ID                                   LEGACY_WEX_SITE_ID,
                                       DW_STKHLDR.BRAND_NM                                            BRAND_NM,
                                       DW_SITE.SITE_NM                                                      SITE_NM,
                                       DW_ADDR.ADDR_LN_1                                                 ADDR_LN_1,
                                       DW_ADDR.ADDR_CITY                                                 ADDR_CITY,
                                       DW_ADDR.ADDR_STATE_PROV_CD                              ADDR_STATE_PROV_CD,
                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                                ADDR_ZIP_POSTAL_CD,
                                       DW_SITE.GEO_CD_LAT                                               GEO_CD_LAT,
                                       DW_SITE.GEO_CD_LONG                                             GEO_CD_LONG,
                                       CASE WHEN DW_SITE.SITE_TYPE= 'Private' 
                                                THEN 1                 
                                                ELSE 0  
                                       END                                                                           PRIVATE_SITE_FLG, 
                                       AM_PURCH_TRANS_ITEM.SRC_SYS_SITE_TKT_NBR      SRC_SYS_SITE_TKT_NBR, 
                                       AM_PURCH_TRANS_ITEM.POSTING_DT                        POSTING_DT ,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                       DW_PROD_CLASS.CLASS_NM                                     CLASS_NM,
                                       DW_PRODUCT.WEX_SETTLEMENT_CD                         WEX_SETTLEMENT_CD,
                                       DW_WEX_PROD_CD.SHORT_DESC                               SHORT_DESC,
                                       DW_PRODUCT.IND_STD_PROD_CD                               IND_STD_PROD_CD, 
                                       DW_PRODUCT.PROD_LONG_NM                                    PROD_LONG_NM,
                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                TRANSAC_DESC_CDS ,   
                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS      ACCT_NBR_TRANS_UNTS ,
                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS           ACCT_UNIT_OF_MEAS,
                                       AM_PURCH_TRANS_ITEM.TOTAL_FUEL_COST               TOTAL_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.SERVICE_COST                     SERVICE_COST,
                                       AM_PURCH_TRANS_ITEM.OTHER_COST                       OTHER_COST,
                                       AM_PURCH_TRANS_ITEM.TOTAL_NON_FUEL_COST       TOTAL_NON_FUEL_COST,
                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT   GROSS_COST,
                                      AM_PURCH_TRANS_ITEM.EXEMPT_TAX                        EXEMPT_TAX,
                                       AM_PURCH_TRANS_ITEM.DISCOUNT                            DISCOUNT, 
                                       AM_PURCH_TRANS_ITEM.NET_COST                            NET_COST,
                                       AM_PURCH_TRANS_ITEM.REPORTED_TAX                    REPORTED_TAX,
                                       AM_PURCH_TRANS_ITEM.ODOMETER                           ODOMETER , 
                                       AM_PURCH_TRANS_ITEM.CURRENCY_EXCHANGE_RATE CURRENCY_EXCHANGE_RATE , 
                                       AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                       CASE 
                                                WHEN AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                       THEN  
                                               CEIL (AM_PURCH_TRANS_ITEM.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                        ELSE 
                                                1 
                                       END                                                                           TOTAL_PAGES
                                                  FROM                                       
                                            (SELECT 
                                                               A.AM_PURCH_TRANS_ITEM_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_PURCH_DEVICE_KEY , 
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.SRC_SYS_SITE_TKT_NBR ,
                                                               A.POSTING_DT,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.TRANS_DESC_CDS ,  
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.ACCT_NBR_TRANS_UNTS   ,       
                                                               A.ACCT_UNIT_OF_MEAS ,
                                                               A.TOTAL_FUEL_COST,
                                                               A.SERVICE_COST , 
                                                               A.OTHER_COST , 
                                                               A.TOTAL_NON_FUEL_COST,
                                                               A.EXEMPT_TAX , 
                                                               A.DISCOUNT , 
                                                               A.NET_COST,
                                                               A.REPORTED_TAX ,
                                                               A.CURRENCY_EXCHANGE_RATE,
                                                                ROW_NUMBER () OVER (ORDER BY A.AM_PURCH_TRANS_ITEM_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                                     Total_RECORD_COUNT
                                                               FROM                                                                  
                                               (SELECT /*+     parallel (AM_PURCH_TRANS_ITEM , 12 ) */
                                                                       AM_PURCH_TRANS_ITEM.AM_PURCH_TRANS_ITEM_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ACCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_POS_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_SITE_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_ASSET_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY,
                                                                       AM_PURCH_TRANS_ITEM.DW_MRCH_KEY,
                                                                       CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Vehicle ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                         CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Driver ID'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                        CASE
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_1  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_1
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_2  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_2
                                                                          WHEN AM_PURCH_TRANS_ITEM.POS_PROMPT_NM_3  = 'Odometer'
                                                                          THEN
                                                                             AM_PURCH_TRANS_ITEM.POS_PROMPT_DATA_3
                                                                       END
                                                                          ODOMETER,
                                                                       AM_PURCH_TRANS_ITEM.SITE_TKT_NBR                     SRC_SYS_SITE_TKT_NBR        ,   
                                                                      TRUNC(AM_PURCH_TRANS_ITEM.POSTING_DT)             POSTING_DT          ,
                                                                       TRUNC( AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT)  LOCAL_TRANS_DT ,
                                                                       AM_PURCH_TRANS_ITEM.LOCAL_TRANS_TM                   ,
                                                                       AM_PURCH_TRANS_ITEM.TRANS_DESC_CDS                  ,  
                                                                       AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT     ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS        ,
                                                                       AM_PURCH_TRANS_ITEM.ACCT_UNIT_OF_MEAS            , 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_FUEL_AMT                     TOTAL_FUEL_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT                    SERVICE_COST, 
                                                                       AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT         OTHER_COST  ,       
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_SERV_AMT         + 
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_PROD_AMT )      TOTAL_NON_FUEL_COST  ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_TRANS_GROSS_AMT       GROSS_COST  , 
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_TAX_EXEMPT_AMT     +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_TAX_EXEMPT_AMT +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_TAX_EXEMPT_AMT ) EXEMPT_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_ITEM_DISC_AMT                   DISCOUNT,
                                                                        AM_PURCH_TRANS_ITEM.ACCT_CUST_ITEM_NET_AMT          NET_COST,
                                                                       (AM_PURCH_TRANS_ITEM.ACCT_FED_RPTD_TAX_AMT    +
                                                                        AM_PURCH_TRANS_ITEM.ACCT_STATE_RPTD_TAX_AMT+
                                                                        AM_PURCH_TRANS_ITEM.ACCT_OTHER_RPTD_TAX_AMT )   REPORTED_TAX ,
                                                                        AM_PURCH_TRANS_ITEM.FROM_ACCT_CRCY_RATE             CURRENCY_EXCHANGE_RATE     
                                                                  FROM   AM_PURCH_TRANS_ITEM  
                                                                 WHERE   
                                                                            AM_PURCH_TRANS_ITEM.LOCAL_TRANS_DT  BETWEEN V_Start_Dttm_In
                                                                                                                                                   AND     V_End_Dttm_In 
                                                                      AND 
                                                                              (    ( AM_PURCH_TRANS_ITEM. ACCT_TRANS_GROSS_AMT   >= V_GROSS_DOLLAR_AMT_IN     and  V_ACCT_NBR_TRANS_UNITS =  0  and V_ACCT_PPU_AMT = 0  )
                                                                              OR ( AM_PURCH_TRANS_ITEM.ACCT_NBR_TRANS_UNTS       >=  V_ACCT_NBR_TRANS_UNITS and   V_GROSS_DOLLAR_AMT_IN   = 0  and V_ACCT_PPU_AMT = 0  )  
                                                                              OR ( AM_PURCH_TRANS_ITEM.ACCT_PPU_AMT                    >=  V_ACCT_PPU_AMT                and  V_ACCT_NBR_TRANS_UNITS =  0   and V_GROSS_DOLLAR_AMT_IN = 0  ) 
                                                                         )           
                                                                      AND AM_PURCH_TRANS_ITEM.DW_ACCT_KEY
                                                                                                                                      IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                             (     (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                            )
                                                                                                                                             AND DA.ARCHIVE_FLG = 'N'  
                                                                                                                                       )    
                                                                                                                                                
                                                                               ) A)  AM_PURCH_TRANS_ITEM,
                                                       DW_ACCT,
                                                       DW_PURCH_DEVICE , 
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE   AM_PURCH_TRANS_ITEM.DW_ACCT_KEY             = DW_ACCT.DW_ACCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PURCH_DEVICE_KEY = DW_PURCH_DEVICE.DW_PURCH_DEVICE_KEY      
                                                       AND AM_PURCH_TRANS_ITEM.DW_DRIVER_KEY            = DW_DRIVER.DW_DRIVER_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_ASSET_KEY             = DW_ASSET.DW_ASSET_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_POS_KEY                 = DW_POS.DW_POS_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_SITE_KEY                = DW_SITE.DW_SITE_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_PRODUCT_KEY         =  DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND AM_PURCH_TRANS_ITEM.DW_MRCH_KEY               = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD                   = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD                               = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID      = DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND AM_PURCH_TRANS_ITEM.ROW_NUM BETWEEN V_START_RECORD 
                                                                                                                             AND V_END_RECORD;
 END CASE ;
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 END SearchByAmount  ;                                              


END TRANSACTIONSEARCH ;
/

GRANT EXECUTE ON TRANSACTIONSEARCH TO SOA_LOADER ; 
GRANT EXECUTE ON TRANSACTIONSEARCH TO USER_LDAP  ; 

CREATE OR REPLACE PUBLIC SYNONYM TRANSACTIONSEARCH  FOR DW_OWNER.TRANSACTIONSEARCH ; 