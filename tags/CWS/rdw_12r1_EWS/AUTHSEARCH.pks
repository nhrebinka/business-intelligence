CREATE OR REPLACE PACKAGE DW_OWNER.AuthSearch
AS
   /******************************************************************************
      NAME:    AauthSearch
      PURPOSE:

      REVISIONS:
      Ver        Date                   Author                     Description
      ---------  ----------            ---------------  ------------------------------------
      1.0        12/12/2011        Venu  KAncharla          1. Created  just a Pacakge Header minus Body for dev integration
   ******************************************************************************/

   -- Defines the Returned Result set as a Reference Cursor
   TYPE AuthDtlListtyp IS REF CURSOR;

   -- Interface for Account NBR Search
   PROCEDURE SearchByAccount ( START_DT_in         IN     VARCHAR2,
                                                  START_TM_in        IN     VARCHAR2,
                                                  END_DT_in             IN     VARCHAR2,
                                                  END_TM_in             IN     VARCHAR2,
                                                  TIMEZONE_in            IN      VARCHAR2,
                                                  ACTION_CD_in           IN     VARCHAR2,
                                                  ACCT_NBR_in            IN      VARCHAR2,
                                                  ACCT_TYPE_In           IN     VARCHAR2 ,
                                                  START_PAGE_in         IN     INTEGER,
                                                  ROWS_PER_PAGE_in   IN     INTEGER,
                                                  AUTHDETAIL_out        OUT AuthDtlListtyp
                                                 );

   -- Interface for PurchaseDevice Number
   PROCEDURE SearchByPurchDevice (
                                                        START_DT_in                       IN     VARCHAR2,
                                                        START_TM_in                       IN     VARCHAR2,
                                                        END_DT_in                            IN     VARCHAR2,
                                                        END_TM_in                           IN     VARCHAR2,
                                                        TIMEZONE_in                        IN     VARCHAR2,
                                                        ACTION_CD_in                      IN     VARCHAR2,
                                                        SPNR_PD_NBR_in                  IN     VARCHAR2,
                                                        ACCT_NBR_in                       IN     VARCHAR2,
                                                        ACCT_TYPE_in                      IN     VARCHAR2,
                                                        START_PAGE_in                   IN     INTEGER,
                                                        ROWS_PER_PAGE_in            IN     INTEGER,
                                                        AUTHDETAIL_out           OUT AuthDtlListtyp
                                                   );

   -- Interface for Assest  Search

   PROCEDURE SearchByAsset (
                                                  START_DT_in                 IN     VARCHAR2,
                                                   START_TM_in                IN     VARCHAR2,
                                                   END_DT_in                     IN     VARCHAR2,
                                                   END_TM_in                     IN     VARCHAR2,
                                                    TIMEZONE_in                 IN     VARCHAR2,
                                                    ACTION_CD_in               IN     VARCHAR2,
                                                    ACCT_NBR_in                 IN     VARCHAR2,
                                                    ACCT_Type_In               In      VARCHAR2 ,
                                                    VIN_in                           IN     VARCHAR2,
                                                    CUSTOM_ASSET_ID_in   IN     VARCHAR2,
                                                    VEHICLE_ID_in               IN     VARCHAR2,
                                                    START_PAGE_in             IN     INTEGER,
                                                    ROWS_PER_PAGE_in      IN     INTEGER,
                                                    AUTHDETAIL_out          OUT AuthDtlListtyp
                                            );

   -- Interface for  Driver Search  List
   PROCEDURE SearchByDriver (
                                                   START_DT_in                 IN     VARCHAR2,
                                                   START_TM_in                IN     VARCHAR2,
                                                   END_DT_in                     IN     VARCHAR2,
                                                   END_TM_in                     IN     VARCHAR2,
                                                   TIMEZONE_in                   IN     VARCHAR2,
                                                   ACTION_CD_in                  IN     VARCHAR2,
                                                   DRIVER_ID_in                     IN     VARCHAR2,
                                                   DRIVER_FIRST_NAME_in     IN     VARCHAR2,
                                                   DRIVER_LAST_NAME_in      IN     VARCHAR2,
                                                   DRIVER_MIDDLE_NAME_in   IN     VARCHAR2,
                                                   PAR_ACCT_NBR_in             IN     VARCHAR2,
                                                   ACCT_NBR_in                     IN     VARCHAR2,
                                                   ACCT_Type_In                    In      VARCHAR2 ,
                                                   START_PAGE_in                  IN     INTEGER,
                                                   ROWS_PER_PAGE_in            IN     INTEGER,
                                                   AUTHDETAIL_out             OUT AuthDtlListtyp
                                             );

   -- Interface for  Card Department  Search  List
   PROCEDURE SearchByCardDepartment (
                                                                  START_DT_in                 IN     VARCHAR2,
                                                                  START_TM_in                IN     VARCHAR2,
                                                                  END_DT_in                     IN     VARCHAR2,
                                                                  END_TM_in                     IN     VARCHAR2,
                                                                  TIMEZONE_in                   IN     VARCHAR2,
                                                                  ACTION_CD_in                   IN     VARCHAR2,
                                                                  PURCH_DEVICE_DEPT_in      IN     VARCHAR2,
                                                                  PAR_ACCT_NBR_in               IN     VARCHAR2,
                                                                  ACCT_NBR_in                      IN     VARCHAR2,
                                                                   ACCT_Type_In                    In      VARCHAR2 ,
                                                                  START_PAGE_in                  IN     INTEGER,
                                                                  ROWS_PER_PAGE_in           IN     INTEGER,
                                                                  AUTHDETAIL_out                 OUT AuthDtlListtyp
                                                            );


   -- Interface for  Region Search list

   PROCEDURE SearchByRegion (
                                                     START_DT_in                IN     VARCHAR2,
                                                     START_TM_in               IN     VARCHAR2,
                                                     END_DT_in                    IN     VARCHAR2,
                                                     END_TM_in                    IN     VARCHAR2,
                                                     TIMEZONE_in                 IN     VARCHAR2,
                                                     ACTION_CD_in               IN     VARCHAR2,
                                                     SITE_CITY_in                 IN     VARCHAR2,
                                                     SITE_STATE_CD_in        IN     VARCHAR2,
                                                     SITE_ZIP_in                   IN     VARCHAR2,
                                                     PAR_ACCT_NBR_in        IN     VARCHAR2,
                                                     ACCT_NBR_in                IN     VARCHAR2,
                                                     ACCT_Type_In               In      VARCHAR2 ,
                                                     START_PAGE_in             IN     INTEGER,
                                                     ROWS_PER_PAGE_in      IN     INTEGER,
                                                     AUTHDETAIL_out           OUT AuthDtlListtyp
                                             );

   -- Interface for  Amount serach list
   PROCEDURE SearchByAmount (
                                                 START_DT_in                  IN     VARCHAR2,
                                                 START_TM_in                  IN     VARCHAR2,
                                                 END_DT_in                       IN     VARCHAR2,
                                                 END_TM_in                       IN     VARCHAR2,
                                                 TIMEZONE_in                    IN     VARCHAR2,
                                                 ACTION_CD_in                  IN     VARCHAR2,
                                                 GROSS_DOLLAR_AMT_in    IN     NUMBER,
                                                 PAR_ACCT_NBR_in             IN     VARCHAR2,
                                                 ACCT_NBR_in                     IN     VARCHAR2,
                                                 ACCT_Type_In                   In      VARCHAR2 ,
                                                 START_PAGE_in                 IN     INTEGER,
                                                 ROWS_PER_PAGE_in          IN     INTEGER,
                                                 AUTHDETAIL_out              OUT AuthDtlListtyp
                                               );
END AuthSearch;
/