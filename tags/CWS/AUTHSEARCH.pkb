CREATE OR REPLACE PACKAGE BODY DW_OWNER.AUTHSEARCH
IS
/******************************************************************************
   NAME:    AUTH SEARCH 
   PURPOSE:  Using Account Number  and  ActionCode  and Time  interval  .

   REVISIONS:
   Ver        Date                     Author                                   Description
   ---------  ----------              ---------------                   ------------------------------------
   1.0       12/15/2010             Venu Kancharla                 AUTHSEARCH PROCEDURE
   
   1.1      03/05/2012              Venu KAncharla               DEFECT ID#  27025 , 27024 , 27023, 27017 , 27020, 27026, 27021
   
  --1.2  03/12/2012                  Venu KAncharla      Defect # 27010-  FIX
  
  1.3 03/12/2012                       Venu KAncharla    Defect # 2718 
  
  1.4 03/14/2012                        Venu KAncharla  Defect #  27130 -- Fix 
    
  1.5 03/27/2012                         Venu KAncharla Defect #  27498-FIX 
  
  1.6 03/28/2012                         Venu KAncharla Defect #  27498-FIX 
  
  1.7  03/28/20102                     Venu KAncharla  Defect 27641-- Driver Middle name issue
  
  1.8 03/29/2012                         Venu KAncharla Defect #  27574 -- Message Version Number  
  
  1.9  03/30/2012                          Venu KAncharla Defect # 27585 , 27586 

  Supported Searches avaiable in v1.0 
  
  Account Number                       - Exact match on WEX_ACCT_NBR
  Sponsor Account Number          - Exact Match on SPNR_ACCT_NBR
  National ID                               - A Classic Platform Grouping ID stored in wex_acct_nbr
  Account NameSearch                - wildcard with 
  IOL CCN number                       - Exact Match
  Embossed Card Number            - Exact Match
  Account Phone Number             - Wildcard match
******************************************************************************/

/******************************************************************************
 Authorization Search By Account Number 
******************************************************************************/

PROCEDURE SearchByAccount(
                                            START_DT_in         IN     VARCHAR2,
                                            START_TM_in        IN     VARCHAR2,    
                                            END_DT_in             IN     VARCHAR2,
                                            END_TM_in             IN     VARCHAR2,
                                            TimeZone_in              IN VARCHAR2 ,
                                            Action_Cd_In              IN VARCHAR2 ,
                                            Acct_Nbr_In                IN VARCHAR2 ,
                                            ACCT_Type_In            In VARCHAR2 ,
                                            Start_Page_In             IN INTEGER    , 
                                            Rows_Per_Page_in      IN INTEGER    ,                 
                                            Authdetail_out             OUT AuthDtlListtyp
                                            )
IS

C_PROCESS_CODE                VARCHAR2 (30) := 'AUTHSEARCHSearchByaccount' ;

 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN              DATE ; 
 V_END_DTTM_IN                  DATE ;
  
 V_SQL_PARALLEL                   VARCHAR2(100) ;  
 
 
BEGIN

   DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByaccount '
                                  );

  V_Acct_Type_In      := UPPER(TRIM(Acct_TYPE_In) ) ;
  
  
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000001')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 


  V_SQL_PARALLEL :=     'ALTER SESSION FORCE PARALLEL DDL PARALLEL 12'  ;
  

CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;


 -- DBMS_OUTPUT.PUT_LINE ('V_ACTION_CD_IN' || V_ACTION_CD_IN) ;  
 -- V_SQL :=    '  AND WEX_ACCT_NBR = ACCT_NBR_IN  ' ;

 EXECUTE IMMEDIATE V_SQL_PARALLEL ; 

 
                   OPEN Authdetail_out FOR
                                  SELECT          DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                    NATIONAL_ID ,        
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                                    DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                                   DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                                 DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD, 
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                          TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR            MSG_VERSION_NBR       
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num , 
                                                               COUNT(*)   OVER ()                                      Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                          WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_Start_Dttm_In
                                                                                                                                      AND      V_End_Dttm_In
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START and V_AUTH_CD_END 
                                                                              AND FW_AUTH_DTL.DW_ACCT_KEY IN
                                                                                                                                (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                                ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR         =   Acct_Nbr_in  ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR        =   Acct_Nbr_in  )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR   = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                ) 
                                                                                                                                         AND DA.ARCHIVE_FLG = 'N'           
                                                                                                                                 )   
                                                                 ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY         = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY         = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY          = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY              = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY             = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY      = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY           = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD             = 'ENG'
                                                       AND (  DW_PRODUCT.IND_STD_PROD_ROW_ID  =  DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG    = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE                  = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID             = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG                = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN     V_START_RECORD 
                                                                                                             AND     V_END_RECORD;

EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
 
END SearchByAccount ;  

PROCEDURE SearchByPurchDevice (
                                                      START_DT_in              IN      VARCHAR2,
                                                      START_TM_in              IN      VARCHAR2,
                                                      END_DT_in                   IN      VARCHAR2,
                                                      END_TM_in                   IN      VARCHAR2,
                                                      TIMEZONE_in                 IN     VARCHAR2,
                                                      ACTION_CD_in               IN     VARCHAR2,
                                                      SPNR_PD_NBR_in           IN     VARCHAR2,
                                                      ACCT_NBR_in                 IN     VARCHAR2,
                                                      ACCT_TYPE_in               IN     VARCHAR2,
                                                      START_PAGE_in             IN     INTEGER,
                                                      ROWS_PER_PAGE_in      IN     INTEGER,
                                                      AUTHDETAIL_out           OUT   AuthDtlListtyp
                                                      )
  IS     
      
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByPurchaseDevice' ;
 V_Acct_Type_In                    Varchar2(50)  ;   
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;
                                            
       
 BEGIN 
 
 
    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByPurchaseDevice '
                                  );

  
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 

  V_Acct_Type_In                  := UPPER(TRIM(Acct_TYPE_In) )                          ;

 
CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;

CASE WHEN LENGTH(SPNR_PD_NBR_IN) = 4     -- SPNR_PD_NBR       
THEN 
                   OPEN Authdetail_out FOR
                                          SELECT  DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                           TOTAL_PAGES  , 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                                COUNT(*)   OVER ()                           Total_RECORD_COUNT, 
                                                                A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                      TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                          WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END  
                                                                             AND FW_AUTH_DTL.DW_PURCH_DEVICE_KEY  IN 
                                                                                                                                          (SELECT /*+ parallel (DPD, 12) */
                                                                                                                                                       DW_PURCH_DEVICE_KEY 
                                                                                                                                           FROM     DW_PURCH_DEVICE DPD  
                                                                                                                                              WHERE  DPD.SPNR_PD_NBR = SPNR_PD_NBR_IN 
                                                                                                                                               AND  DPD.ACCT_ROW_ID     IN  
                                                                                                                                                                                 (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                                                               DA.ACCT_ROW_ID
                                                                                                                                                                                     FROM DW_ACCT DA 
                                                                                                                                                                                    WHERE  
                                                                                                                                                                                             ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                                                        ) 
                                                                                                                                                                                         AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                                                                   )   
                                                                                                                                            )                                        
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;  
  ELSE 
                                 OPEN Authdetail_out FOR
                                          SELECT 
                                                       DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT       TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                           TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR  
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                             Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR             
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END  
                                                                             AND FW_AUTH_DTL.DW_PURCH_DEVICE_KEY  IN 
                                                                                                                                              (SELECT /*+ parallel (DW_ACCT, 12) */
                                                                                                                                                            DW_PURCH_DEVICE_KEY 
                                                                                                                                               FROM     DW_PURCH_DEVICE DPD  
                                                                                                                                                  WHERE  DPD.IOL_CCN_NUM  = SPNR_PD_NBR_IN 
                                                                                                                                                   AND  DPD.ACCT_ROW_ID     IN  
                                                                                                                                                                                   (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                                                               DA.ACCT_ROW_ID
                                                                                                                                                                                     FROM DW_ACCT DA 
                                                                                                                                                                                    WHERE  
                                                                                                                                                                                             ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                                                                        OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                                                        ) 
                                                                                                                                                                                         AND DA.ARCHIVE_FLG = 'N' 
                                                                                                                                                                                   )   
                                                                                                                                                )                                        
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;  
 
  END CASE ;     
  

EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );

END SearchByPurchDevice ; 


PROCEDURE SearchByAsset (  START_DT_in                IN     VARCHAR2,
                                            START_TM_in                IN     VARCHAR2,    
                                            END_DT_in                     IN     VARCHAR2,
                                            END_TM_in                     IN     VARCHAR2,
                                            TIMEZONE_in                  IN     VARCHAR2,
                                            ACTION_CD_in                IN     VARCHAR2,
                                            ACCT_NBR_in                 IN     VARCHAR2,
                                            ACCT_Type_In                IN      VARCHAR2 ,
                                            VIN_in                            IN     VARCHAR2,
                                            CUSTOM_ASSET_ID_in    IN     VARCHAR2,
                                            VEHICLE_ID_in                IN     VARCHAR2,
                                            START_PAGE_in             IN     INTEGER,
                                            ROWS_PER_PAGE_in      IN     INTEGER,
                                            AUTHDETAIL_out           OUT AuthDtlListtyp)
IS 
      
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByAsset' ;
 
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;         
 
 V_Acct_Type_In                     VARCHAR2(50)   ;
 
 V_VIN_IN                                    DW_ASSET.VIN%TYPE ; 
 V_CUSTOM_ASSET_ID_IN            DW_ASSET.CUST_ASSET_NBR%TYPE ;  
 V_VEHICLE_PROMPT_ID_IN          DW_ASSET.WEX_ASSET_ID%TYPE    ;      
 
                                       

 BEGIN 
 
 
    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByAsset'
                                  );

  
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 

 
CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;
 
  select Sysdate INTO V_DATE  From Dual ;
  
 V_Acct_Type_In                    := UPPER(TRIM(Acct_TYPE_In) )                          ;
    
 V_VIN_IN                               := TRIM (VIN_IN) ;  
 V_CUSTOM_ASSET_ID_IN       := TRIM ( CUSTOM_ASSET_ID_in ) ; 
 V_VEHICLE_PROMPT_ID_IN     := TRIM(VEHICLE_ID_in) ;  
 

-- this is to ASSET ID like  

 IF V_CUSTOM_ASSET_ID_IN  IS NOT NULL  
 THEN 
      V_CUSTOM_ASSET_ID_IN := V_CUSTOM_ASSET_ID_IN||'%' ;
END IF ;          
 


            OPEN Authdetail_out FOR
                                           SELECT DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                    NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL  (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                            TOTAL_PAGES  ,
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR   
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                             Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR             
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR      
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END  
                                                                             AND FW_AUTH_DTL.DW_ACCT_KEY
                                                                                                                                 IN  
                                                                                                                                (SELECT     -- Hint for DW_ACCT REMOVED PER BILL's  suggestion ... 
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                                 ( (  V_Acct_Type_In =  'WEXACCT'               and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'              and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'              and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                   ) 
                                                                                                                                            AND DA.ARCHIVE_FLG = 'N'            
                                                                                                                                    )   
                                                                             AND  FW_AUTH_DTL.DW_ASSET_KEY  
                                                                                                                               IN 
                                                                                                                            (   SELECT -- Hint for DW_ACCT REMOVED PER BILL's  suggestion ...   
                                                                                                                                          DA.DW_ASSET_KEY 
                                                                                                                                   FROM   DW_ASSET DA    
                                                                                                                                 WHERE
                                                                                                                                              (  (V_VIN_IN IS NOT NULL AND  DA.VIN =  V_VIN_IN ) 
                                                                                                                                          OR ( V_VEHICLE_PROMPT_ID_IN IS NOT NULL AND DA.WEX_ASSET_ID =   V_VEHICLE_PROMPT_ID_IN )
                                                                                                                                          OR  (V_CUSTOM_ASSET_ID_IN IS NOT NULL AND DA.CUST_ASSET_NBR like V_CUSTOM_ASSET_ID_IN  )
                                                                                                                                               ) 
                                                                                                                                          AND DA.ARCHIVE_FLG = 'N'                    
                                                                                                                                    )                     
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;  
  
                                            
EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );
END  SearchByAsset;

PROCEDURE SearchByDriver (  START_DT_in                   IN     VARCHAR2,
                                             START_TM_in                 IN     VARCHAR2,    
                                             END_DT_in                     IN     VARCHAR2,
                                             END_TM_in                     IN     VARCHAR2,
                                             TIMEZONE_in                   IN     VARCHAR2,
                                             ACTION_CD_in                  IN     VARCHAR2,
                                             DRIVER_ID_in                    IN     VARCHAR2,
                                             DRIVER_FIRST_NAME_in     IN     VARCHAR2,
                                             DRIVER_LAST_NAME_in      IN     VARCHAR2,
                                             DRIVER_MIDDLE_NAME_in    IN     VARCHAR2,
                                             PAR_ACCT_NBR_in              IN     VARCHAR2,
                                             ACCT_NBR_in                     IN     VARCHAR2,
                                             ACCT_Type_In                    In      VARCHAR2 ,
                                             START_PAGE_in                  IN     INTEGER,
                                             ROWS_PER_PAGE_in            IN     INTEGER,
                                             AUTHDETAIL_out                OUT AuthDtlListtyp)
                                             
IS 
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByDriver' ;
 
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;             
 
 V_PAR_ACCT_NBR_IN              DW_ACCT.SPNR_ACCT_NBR%TYPE ;  
 V_ACCT_NBR_IN                      DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 
 V_DRIVER_ID_IN                        DW_DRIVER.WEX_DRIVER_ID%TYPE ; 
 V_DRIVER_FIRST_NAME_IN        DW_DRIVER.DRIVER_FIRST_NM%TYPE ; 
 V_DRIVER_LAST_NAME_IN         DW_DRIVER.DRIVER_LAST_NM%TYPE ;  
 V_DRIVER_MIDDLE_NAME_IN      DW_DRIVER.DRIVER_MIDDLE_NM%TYPE; 
 
 V_ACCT_TYPE_In                  VARCHAR2(50) ;    
                                

 BEGIN 
 
 
    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByDriver'
                                  );
  
  V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 

CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;
 
 V_PAR_ACCT_NBR_IN           := TRIM(PAR_ACCT_NBR_IN)   ; 
 V_ACCT_NBR_IN                  := TRIM(ACCT_NBR_IN)   ;
 V_ACCT_TYPE_IN                 :=  TRIM(ACCT_TYPE_IN) ; 
 
 V_DRIVER_ID_IN                      := TRIM(DRIVER_ID_IN) ;  
 V_DRIVER_FIRST_NAME_IN      := UPPER(TRIM(DRIVER_FIRST_NAME_IN))   ; 
 V_DRIVER_LAST_NAME_IN       := UPPER(TRIM(DRIVER_LAST_NAME_IN))    ;     
 V_DRIVER_MIDDLE_NAME_IN    := UPPER(TRIM(DRIVER_MIDDLE_NAME_IN)) ;
  
 
     IF V_DRIVER_FIRST_NAME_IN IS NOT NULL 
     THEN 
           V_DRIVER_FIRST_NAME_IN  := V_DRIVER_FIRST_NAME_IN ||'%' ; 
     END IF ; 
     
     IF V_DRIVER_LAST_NAME_IN  IS NOT NULL 
     THEN 
            V_DRIVER_LAST_NAME_IN := V_DRIVER_LAST_NAME_IN || '%' ; 
     END IF ;        
            
     IF V_DRIVER_MIDDLE_NAME_IN  IS NOT NULL 
     THEN 
            V_DRIVER_MIDDLE_NAME_IN := V_DRIVER_MIDDLE_NAME_IN || '%' ; 
     END IF ;        
    
 
 
  
 
    CASE  WHEN   V_PAR_ACCT_NBR_IN   IS NOT NULL   
    THEN 
          OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                    NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT       TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                           TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR    
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                              Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR               
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                      TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR         
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                              AND FW_AUTH_DTL.DW_DRIVER_KEY in
                                                                                                     ( SELECT /*+ parallell (DD, 12 ) */  DD.DW_DRIVER_KEY
                                                                                                            FROM   DW_DRIVER DD 
                                                                                                            WHERE 
                                                                                                                        (                     (   DD.WEX_DRIVER_ID  =  V_DRIVER_ID_IN  and V_DRIVER_ID_IN is not null )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                            AND V_DRIVER_FIRST_NAME_IN           IS  NULL    
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL
                                                                                                                                                                                               
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )  
                                                                                                                                            OR 
                                                                                                                                              ( V_DRIVER_ID_IN  IS NULL      and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS   NULL    
                                                                                                                                               )     
                                                                                                                                            OR  ( V_DRIVER_ID_IN  IS NULL and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                               )    
                                                                                                                        )    
                                                                                                      )                                                                    
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                      IN 
                                                                                                                          ( select /*+ parallel (da, 12) */  DA.DW_ACCT_KEY
                                                                                                                                    from dw_acct da,
                                                                                                                                    dw_acct da_par,
                                                                                                                                     ( select  da.l1_acct_row_id,
                                                                                                                                       da.spnr_acct_nbr,
                                                                                                                                       da.acct_row_id,
                                                                                                                                       da.derived_hier_level_nbr
                                                                                                                                       from dw_acct da
                                                                                                                                       where da.dw_current_flg = 'Y'
                                                                                                                                    ) da_lvl
                                                                                                                                    where
                                                                                                                                           da.l1_acct_row_id = da_lvl.l1_acct_row_id
                                                                                                                                    and da_lvl.acct_row_id = da_par.acct_row_id
                                                                                                                                    and da_par.acct_row_id = 
                                                                                                                                       case
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 1 then da.l1_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 2 then da.l2_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 3 then da.l3_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 4 then da.l4_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 5 then da.l5_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 6 then da.l6_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 7 then da.l7_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 8 then da.l8_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 9 then da.l9_acct_row_id
                                                                                                                                          else null
                                                                                                                                       end
                                                                                                                                 and da_lvl.spnr_acct_nbr = V_PAR_ACCT_NBR_IN
                                                                                                                                  AND DA.ARCHIVE_FLG = 'N'           
                                                                                                                                 )                              
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;      
  
    WHEN   (V_PAR_ACCT_NBR_IN  IS  NULL         AND  
                 V_ACCT_NBR_IN         IS NOT NULL  ) 
         THEN 
                        OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                                           TOTAL_PAGES  , 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR  
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                             Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR               
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                      TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                          WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR       
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                              AND FW_AUTH_DTL.DW_DRIVER_KEY in
                                                                                                       ( SELECT /*+ parallell (DD, 12 ) */  DD.DW_DRIVER_KEY
                                                                                                            FROM   DW_DRIVER DD 
                                                                                                            WHERE 
                                                                                                                        (     (   DD.WEX_DRIVER_ID  =  V_DRIVER_ID_IN  and V_DRIVER_ID_IN is not null  )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL    
                                                                                                                                               )  
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                            AND V_DRIVER_FIRST_NAME_IN           IS  NULL    
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)         like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  V_DRIVER_LAST_NAME_IN             IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )   
                                                                                                                                         OR  ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL
                                                                                                                                                                                               
                                                                                                                                               )  
                                                                                                                                         OR   ( V_DRIVER_ID_IN  IS NULL    and  UPPER(DD.DRIVER_LAST_NM)        like V_DRIVER_LAST_NAME_IN  
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NOT NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NULL    
                                                                                                                                               )  
                                                                                                                                            OR 
                                                                                                                                              ( V_DRIVER_ID_IN  IS NULL      and  UPPER(DD.DRIVER_FIRST_NM)       like V_DRIVER_FIRST_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS   NULL    
                                                                                                                                               )     
                                                                                                                                            OR  ( V_DRIVER_ID_IN  IS NULL and  UPPER(DD.DRIVER_MIDDLE_NM)     like V_DRIVER_MIDDLE_NAME_IN 
                                                                                                                                                                                           AND V_DRIVER_MIDDLE_NAME_IN          IS  NOT NULL
                                                                                                                                                                                           and V_DRIVER_LAST_NAME_IN              IS  NULL 
                                                                                                                                                                                           AND V_DRIVER_FIRST_NAME_IN            IS  NULL 
                                                                                                                                                  )    
                                                                                                                        )    
                                                                                                      )                                                                                 
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                                    IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                               ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                )
                                                                                                                                            AND DA.ARCHIVE_FLG = 'N'            
                                                                                                                                       )             
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;
 
  END CASE ;       
 
  

EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );                                             
END   SearchByDriver ;


PROCEDURE SearchByCardDepartment (
                                                              START_DT_in                 IN     VARCHAR2,
                                                              START_TM_in                IN     VARCHAR2,    
                                                              END_DT_in                     IN     VARCHAR2,
                                                              END_TM_in                     IN     VARCHAR2,
                                                              TIMEZONE_in                   IN     VARCHAR2,
                                                              ACTION_CD_in                   IN     VARCHAR2,
                                                              PURCH_DEVICE_DEPT_in   IN     VARCHAR2,
                                                              PAR_ACCT_NBR_in           IN     VARCHAR2,
                                                              ACCT_NBR_in                   IN     VARCHAR2,
                                                              ACCT_TYPE_in                   IN     VARCHAR2,
                                                              START_PAGE_in                 IN     INTEGER,
                                                              ROWS_PER_PAGE_in          IN     INTEGER,
                                                              AUTHDETAIL_out            OUT AuthDtlListtyp
                                                          )
IS 
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByCardDept'  ;
 
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;             
 
 V_PAR_ACCT_NBR_IN            DW_ACCT.SPNR_ACCT_NBR%TYPE ;  
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 V_ACCT_TYPE_In                  VARCHAR2(50) ;
 
 V_PURCH_DEVICE_DEPT_IN    DW_DEPT.DEPT_NM%TYPE ;      
                                                                                 

 BEGIN 
 
  select Sysdate INTO V_DATE  From Dual ; 
     
    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByCardDepartment'
                                  );
                                  
                                  
    V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 
  
  
CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;
  
  
   V_PAR_ACCT_NBR_IN         := TRIM(PAR_ACCT_NBR_in) ;
   V_PURCH_DEVICE_DEPT_IN := UPPER(TRIM(PURCH_DEVICE_DEPT_IN )) ;    
   V_ACCT_NBR_IN                 := TRIM(ACCT_NBR_IN )  ;   
   V_ACCT_TYPE_IN               := TRIM(ACCT_TYPE_IN ) ;  
   
     IF V_PURCH_DEVICE_DEPT_IN  IS NOT NULL 
     THEN 
           V_PURCH_DEVICE_DEPT_IN   := V_PURCH_DEVICE_DEPT_IN  ||'%' ; 
     END IF ; 
   
   
  
    CASE  WHEN   
                         ( V_PAR_ACCT_NBR_IN         IS NOT NULL       AND  
                           V_PURCH_DEVICE_DEPT_IN IS NOT NULL       AND  
                           V_ACCT_NBR_IN  IS NULL ) 
    THEN 
          OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT       TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL  (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                          TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR    
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                                      Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR             
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR            
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                              AND FW_AUTH_DTL.DW_PURCH_DEVICE_KEY in
                                                                                                       (select /*+ parallel (dpd, 12) */ DPD.DW_PURCH_DEVICE_KEY  
                                                                                                                                     from DW_PURCH_DEVICE  dpd 
                                                                                                                                    where DPD.DEPT_ROW_ID IN  
                                                                                                                                                                  (SELECT /*+ parallel (dd, 12) */  DEPT_ROW_ID 
                                                                                                                                                                      FROM   DW_DEPT DD 
                                                                                                                                                                   WHERE  UPPER( DD.DEPT_NM) like V_PURCH_DEVICE_DEPT_IN
                                                                                                                                                                  )      
                                                                                                          )                       
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                      IN 
                                                                                                                          ( select  -- Hint for DW_ACCT REMOVED PER BILL's  suggestion ... 
                                                                                                                           DA.DW_ACCT_KEY
                                                                                                                                    from dw_acct da,
                                                                                                                                    dw_acct da_par,
                                                                                                                                     ( select  da.l1_acct_row_id,
                                                                                                                                       da.spnr_acct_nbr,
                                                                                                                                       da.acct_row_id,
                                                                                                                                       da.derived_hier_level_nbr
                                                                                                                                       from dw_acct da
                                                                                                                                       where da.dw_current_flg = 'Y'
                                                                                                                                    ) da_lvl
                                                                                                                                    where
                                                                                                                                           da.l1_acct_row_id = da_lvl.l1_acct_row_id
                                                                                                                                    and da_lvl.acct_row_id = da_par.acct_row_id
                                                                                                                                    and da_par.acct_row_id = 
                                                                                                                                       case
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 1 then da.l1_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 2 then da.l2_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 3 then da.l3_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 4 then da.l4_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 5 then da.l5_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 6 then da.l6_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 7 then da.l7_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 8 then da.l8_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 9 then da.l9_acct_row_id
                                                                                                                                          else null
                                                                                                                                       end
                                                                                                                                 and da_lvl.spnr_acct_nbr = V_PAR_ACCT_NBR_IN
                                                                                                                                 AND DA.ARCHIVE_FLG = 'N'           
                                                                                                                                 )                              
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;
                                                                                                             
                                                                                                             
        WHEN         ( V_PAR_ACCT_NBR_IN     IS  NULL                    AND  
                                (V_PURCH_DEVICE_DEPT_IN IS NOT NULL     AND   
                                 V_ACCT_NBR_IN  IS  NOT NULL             
                                 )
                          )                                                                                                                 
                                                                          
           THEN 
                               OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT       TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                           TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR    
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                            Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR               
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                              AND FW_AUTH_DTL.DW_PURCH_DEVICE_KEY in
                                                                                                                             (select /*+ parallel (dpd, 12) */ DPD.DW_PURCH_DEVICE_KEY  
                                                                                                                                     from DW_PURCH_DEVICE  dpd 
                                                                                                                                    where DPD.DEPT_ROW_ID IN  
                                                                                                                                                                  (SELECT /*+ parallel (dd, 12) */  DEPT_ROW_ID 
                                                                                                                                                                      FROM   DW_DEPT DD 
                                                                                                                                                                   WHERE   UPPER(DD.DEPT_NM) like V_PURCH_DEVICE_DEPT_IN
                                                                                                                                                                  )      
                                                                                                          )                       
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                           IN  
                                                                                                                          (SELECT  -- Hint for DW_ACCT REMOVED PER BILL's  suggestion ...
                                                                                                                                          DA.DW_ACCT_KEY
                                                                                                                              FROM DW_ACCT DA 
                                                                                                                            WHERE  
                                                                                                                                      (  (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                  OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                  OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                  OR (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                       )
                                                                                                                                     AND DA.ARCHIVE_FLG = 'N'                
                                                                                                                                       )                          
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;      
                                                                   
      
  END CASE ; 
  
                                                          
   EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );                                                               
 END  SearchByCardDepartment  ;

 PROCEDURE SearchByRegion (
                                             START_DT_in              IN     VARCHAR2,
                                             START_TM_in              IN     VARCHAR2,    
                                             END_DT_in                   IN     VARCHAR2,
                                             END_TM_in                   IN     VARCHAR2,
                                             TIMEZONE_in                IN     VARCHAR2,
                                             ACTION_CD_in              IN     VARCHAR2,
                                             SITE_CITY_in                IN     VARCHAR2,
                                             SITE_STATE_CD_in       IN     VARCHAR2,
                                             SITE_ZIP_in                  IN     VARCHAR2,
                                             PAR_ACCT_NBR_In        IN     VARCHAR2,
                                             ACCT_NBR_in                IN     VARCHAR2,
                                             ACCT_Type_In                    In      VARCHAR2 ,
                                             START_PAGE_in            IN     INTEGER,
                                             ROWS_PER_PAGE_in      IN     INTEGER,
                                             AUTHDETAIL_out           OUT AuthDtlListtyp
                                          )
IS 
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByRegion'  ;
 
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;             
 
 V_PAR_ACCT_NBR_IN            DW_ACCT.SPNR_ACCT_NBR%TYPE ;  
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 V_ACCT_TYPE_In                  VARCHAR2(50) ;
 
 V_SITE_STATE_CD_IN            DW_ADDR.ADDR_STATE_PROV_CD%TYPE ; 
 V_SITE_CITY_IN                    DW_ADDR.ADDR_CITY%TYPE ; 
 V_SITE_ZIP_IN                       DW_ADDR.ADDR_ZIP_POSTAL_CD%TYPE ;            
 
 
 BEGIN 
 
  select Sysdate INTO V_DATE  From Dual ; 

    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByRegion'
                                  );
   V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
  V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ; 

CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In  ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;
  
  
   V_PAR_ACCT_NBR_IN         := TRIM(PAR_ACCT_NBR_in) ;
   V_ACCT_NBR_IN                 := TRIM(ACCT_NBR_IN )  ;   
   V_ACCT_TYPE_IN               := TRIM(ACCT_TYPE_IN ) ;
   
   V_SITE_STATE_CD_IN         := UPPER(TRIM(SITE_STATE_CD_IN))  ; 
   V_SITE_CITY_IN                  := UPPER(TRIM(SITE_CITY_IN)) ;
   V_SITE_ZIP_IN                    :=            TRIM(SITE_ZIP_IN) ;      
  
   IF V_SITE_CITY_IN IS NOT NULL 
   THEN   
                 V_SITE_CITY_IN := V_SITE_CITY_IN ||'%'  ;
   ELSE 
                 V_SITE_CITY_IN := V_SITE_CITY_IN  ;               
   END IF ; 
  
  IF  V_SITE_ZIP_IN IS nOT NULL 
  THEN                
            V_SITE_ZIP_IN   := V_SITE_ZIP_IN||'%' ;
   ELSE 
           V_SITE_ZIP_IN    :=  V_SITE_ZIP_IN         ;  
  END IF ;                  
     
  
  DBMS_OUTPUT.PUT_LINE('V_SITE_CITY_IN'||V_SITE_CITY_IN) ;  
  
  DBMS_OUTPUT.PUT_LINE('V_SITE_ZIP_IN '||V_SITE_ZIP_IN ) ;
  
  
  
  
    CASE  WHEN   
                         ( V_PAR_ACCT_NBR_IN         IS NOT NULL       AND  
                           V_ACCT_NBR_IN  IS NULL   ) 
    THEN 
          OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                      FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL  (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                          TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR            MSG_VERSION_NBR
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                                      Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                              AND FW_AUTH_DTL.DW_SITE_KEY 
                                                                                                                            IN
                                                                                                                             ( SELECT /*+ parallel (DS , 12)  */  DW_SITE_KEY 
                                                                                                                                  FROM DW_SITE DS 
                                                                                                                               WHERE DS.PHYS_ADDR_ROW_ID 
                                                                                                                                                                        IN
                                                                                                                                                                      ( SELECT /*+ parallel (DA, 12 ) */  DA.ADDR_ROW_ID  
                                                                                                                                                                       FROm DW_ADDR DA  
                                                                                                                                                                       WHERE 
                                                                                                                                                                                   DA.DW_CURRENT_FLG = 'Y' 
                                                                                                                                                                          AND (     (  UPPER( DA.ADDR_STATE_PROV_CD)    IN  ( V_SITE_STATE_CD_IN )  and            V_SITE_CITY_IN   IS  NULL                     and V_SITE_ZIP_IN IS NULL )   
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN  ( V_SITE_STATE_CD_IN )  and UPPER( DA.ADDR_CITY)    like  V_SITE_CITY_IN  and V_SITE_ZIP_IN IS NULL )  
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  UPPER(DA.ADDR_CITY )   like  V_SITE_CITY_IN  and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN)
                                                                                                                                                                                 OR  ( UPPER(  DA.ADDR_CITY )                     like  V_SITE_CITY_IN              and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_ZIP_IN IS NULL ) 
                                                                                                                                                                                 OR  (  DA.ADDR_ZIP_POSTAL_CD                  like  V_SITE_ZIP_IN                and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                  OR ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN  and  V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                 )
                                                                                                                                                                        )
                                                                                                                                  )               
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                      IN 
                                                                                                                          ( select /*+ parallel (da, 12) */  DA.DW_ACCT_KEY
                                                                                                                                    from dw_acct da,
                                                                                                                                    dw_acct da_par,
                                                                                                                                     ( select  da.l1_acct_row_id,
                                                                                                                                       da.spnr_acct_nbr,
                                                                                                                                       da.acct_row_id,
                                                                                                                                       da.derived_hier_level_nbr
                                                                                                                                       from dw_acct da
                                                                                                                                       where da.dw_current_flg = 'Y'
                                                                                                                                    ) da_lvl
                                                                                                                                    where
                                                                                                                                           da.l1_acct_row_id = da_lvl.l1_acct_row_id
                                                                                                                                    and da_lvl.acct_row_id = da_par.acct_row_id
                                                                                                                                    and da_par.acct_row_id = 
                                                                                                                                       case
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 1 then da.l1_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 2 then da.l2_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 3 then da.l3_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 4 then da.l4_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 5 then da.l5_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 6 then da.l6_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 7 then da.l7_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 8 then da.l8_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 9 then da.l9_acct_row_id
                                                                                                                                          else null
                                                                                                                                       end
                                                                                                                                 and da_lvl.spnr_acct_nbr = V_PAR_ACCT_NBR_IN
                                                                                                                                  AND DA.ARCHIVE_FLG = 'N'           
                                                                                                                                 )                              
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;
                                                                                                             
        WHEN           ( V_PAR_ACCT_NBR_IN     IS  NULL                    AND  
                                ( V_ACCT_NBR_IN  IS  NOT NULL 
                                 )
                          )                                                                                                                 
                                                                          
           THEN 
                               OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                       FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                          TOTAL_PAGES  , 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR            MSG_VERSION_NBR
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                                      Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                             AND FW_AUTH_DTL.DW_SITE_KEY 
                                                                                                                            IN
                                                                                                                             ( SELECT /*+ parallel (DS , 12)  */  DW_SITE_KEY 
                                                                                                                                  FROM DW_SITE DS 
                                                                                                                               WHERE DS.PHYS_ADDR_ROW_ID 
                                                                                                                                                                        IN
                                                                                                                                                                        ( SELECT /*+ parallel (DA, 12 ) */  DA.ADDR_ROW_ID  
                                                                                                                                                                       FROm DW_ADDR DA  
                                                                                                                                                                       WHERE 
                                                                                                                                                                                   DA.DW_CURRENT_FLG = 'Y' 
                                                                                                                                                                          AND (     (  UPPER( DA.ADDR_STATE_PROV_CD)    IN  ( V_SITE_STATE_CD_IN )  and            V_SITE_CITY_IN   IS  NULL                     and V_SITE_ZIP_IN IS NULL )   
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN  ( V_SITE_STATE_CD_IN )  and UPPER( DA.ADDR_CITY)    like  V_SITE_CITY_IN  and V_SITE_ZIP_IN IS NULL )  
                                                                                                                                                                                 OR  ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  UPPER(DA.ADDR_CITY )   like  V_SITE_CITY_IN  and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN)
                                                                                                                                                                                 OR  ( UPPER(  DA.ADDR_CITY )                     like  V_SITE_CITY_IN              and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_ZIP_IN IS NULL ) 
                                                                                                                                                                                 OR  (  DA.ADDR_ZIP_POSTAL_CD                  like  V_SITE_ZIP_IN                and  V_SITE_STATE_CD_IN IS NULL                         and   V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                  OR ( UPPER( DA.ADDR_STATE_PROV_CD)     IN ( V_SITE_STATE_CD_IN )   and  DA.ADDR_ZIP_POSTAL_CD  like V_SITE_ZIP_IN  and  V_SITE_CITY_IN IS NULL )
                                                                                                                                                                                 )
                                                                                                                                                                        )
                                                                                                                                  )                                                                                                                                                               
                                                                             AND        FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                               IN  
                                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                                        FROM DW_ACCT DA 
                                                                                                                                      WHERE  
                                                                                                                                                ( (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                                            OR (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                                )
                                                                                                                                            AND DA.ARCHIVE_FLG = 'N'                    
                                                                                                                                       )                               
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;      
                                                                                                               
   
                                                                                                             
END CASE ; 
                                                          
                                                          
   EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );                     
END   SearchByRegion ;

   PROCEDURE SearchByAmount (
                                                 START_DT_in                    IN     VARCHAR2,
                                                 START_TM_in                    IN     VARCHAR2,    
                                                 END_DT_in                          IN     VARCHAR2,
                                                 END_TM_in                          IN     VARCHAR2,
                                                 TIMEZONE_in                        IN     VARCHAR2,
                                                 ACTION_CD_in                      IN     VARCHAR2,
                                                 GROSS_DOLLAR_AMT_in        IN     NUMBER,
                                                 PAR_ACCT_NBR_in                IN     VARCHAR2,
                                                 ACCT_NBR_in                       IN     VARCHAR2,
                                                 ACCT_Type_In                     In      VARCHAR2 ,
                                                 START_PAGE_in                  IN     INTEGER,
                                                 ROWS_PER_PAGE_in           IN     INTEGER,
                                                 AUTHDETAIL_out              OUT AuthDtlListtyp
                                                 )
IS 
 C_PROCESS_CODE                VARCHAR2 (100) := 'AUTHSEARCH.SearchByAmount'  ;
 
 V_START_Record                  INTEGER  ;                  
 V_END_RECORD                    INTEGER ;  
 V_AUTH_CD_START              FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;   
 V_AUTH_CD_END                  FW_AUTH_DTL.AUTH_ACTION_CD%TYPE ;
 V_START_DTTM_IN               DATE ; 
 V_END_DTTM_IN                   DATE ;
 V_DATE                                DATE ;             
 
 V_PAR_ACCT_NBR_IN            DW_ACCT.SPNR_ACCT_NBR%TYPE ;  
 V_ACCT_NBR_IN                   DW_ACCT.SPNR_ACCT_NBR%TYPE ;
 V_ACCT_TYPE_In                  VARCHAR2(50) ;
 
 V_GROSS_DOLLAR_AMT_IN        FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT%TYPE ; 
                                        

 BEGIN 
 
    DBMS_APPLICATION_INFO.set_module
                                  (module_name      => c_process_code,
                                    action_name        => 'START SEARCH FOR AUTHSEARCH.SearchByAmount'
                                  );
    
    V_START_DTTM_IN := TO_DATE (  START_DT_IN || NVL(TRIM(START_TM_IN), '000000')  , ' YYYYMMDDHH24MISS' ) ;   
    V_END_DTTM_IN     := TO_DATE (  END_DT_IN || NVL(TRIM(END_TM_IN), '235959')  , ' YYYYMMDDHH24MISS' ) ;
     
CASE WHEN  START_PAGE_IN  >= 1   
THEN 
        V_START_RECORD  := ( (START_PAGE_IN - 1)   *  RowS_PER_Page_In ) + 1 ;
        V_END_RECORD      :=   (START_PAGE_IN)  *  RowS_PER_Page_In ; 
 ELSE 
     V_START_RECORD := 1 ; 
END CASE ;                 
 
/* Formatted on 1/9/2012 6:22:08 PM (QP5 v5.185.11230.41888) */
 CASE WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'All'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END      :=  '999';
  WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Approve'
  THEN
       V_AUTH_CD_START   := '000';
       V_AUTH_CD_END       := '099';
   WHEN  TRIM(Action_Cd_In) IS NULL OR TRIM(Action_Cd_In) =   'Decline'
  THEN
       V_AUTH_CD_START   := '100';
       V_AUTH_CD_END       := '999';

  END CASE;
  
  
   V_PAR_ACCT_NBR_IN         := TRIM(PAR_ACCT_NBR_in) ;
   V_ACCT_NBR_IN                 := TRIM(ACCT_NBR_IN )  ;   
   V_ACCT_TYPE_IN               := TRIM(ACCT_TYPE_IN ) ;
   
   
V_GROSS_DOLLAR_AMT_IN  :=  GROSS_DOLLAR_AMT_in ; 
   
    CASE  WHEN   ( V_PAR_ACCT_NBR_IN    IS NOT NULL  AND V_ACCT_NBR_IN  IS NULL   ) 
    THEN 
          OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                        FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                           TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR             MSG_VERSION_NBR  
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                                        Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR 
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                             AND    FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT    >= V_GROSS_DOLLAR_AMT_IN                                                                    
                                                                             AND    FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                      IN 
                                                                                                                          ( select /*+ parallel (da, 12) */  DA.DW_ACCT_KEY
                                                                                                                                    from dw_acct da,
                                                                                                                                    dw_acct da_par,
                                                                                                                                     ( select  da.l1_acct_row_id,
                                                                                                                                       da.spnr_acct_nbr,
                                                                                                                                       da.acct_row_id,
                                                                                                                                       da.derived_hier_level_nbr
                                                                                                                                       from dw_acct da
                                                                                                                                       where da.dw_current_flg = 'Y'
                                                                                                                                    ) da_lvl
                                                                                                                                    where
                                                                                                                                           da.l1_acct_row_id = da_lvl.l1_acct_row_id
                                                                                                                                    and da_lvl.acct_row_id = da_par.acct_row_id
                                                                                                                                    and da_par.acct_row_id = 
                                                                                                                                       case
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 1 then da.l1_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 2 then da.l2_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 3 then da.l3_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 4 then da.l4_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 5 then da.l5_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 6 then da.l6_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 7 then da.l7_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 8 then da.l8_acct_row_id
                                                                                                                                          when da_lvl.derived_hier_level_nbr = 9 then da.l9_acct_row_id
                                                                                                                                          else null
                                                                                                                                       end
                                                                                                                                 and da_lvl.spnr_acct_nbr = V_PAR_ACCT_NBR_IN
                                                                                                                                AND DA.ARCHIVE_FLG = 'N'           )                              
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;


   WHEN   ( V_PAR_ACCT_NBR_IN    IS  NULL  AND (  V_ACCT_NBR_IN  IS NOT NULL ) )  
    THEN 
          OPEN Authdetail_out FOR
                                         SELECT   DW_ACCT.ACCT_NM                                   ACCOUNT_NM,
                                                       DW_ACCT.L1_WEX_ACCT_NBR                         NATIONAL_ID ,
                                                       DW_ACCT.SPNR_ACCT_NBR                        SPNR_ACCT_NBR,         -- WEC Program Account NBR
                                                       DW_ACCT.CUSTOM_ACCT_NBR                   CUSTOM_ACCT_NBR,
                                                       FW_AUTH_DTL.PD_ID                                  PD_ID,
                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT                AUTH_PD_EXP_DT,
                                                       DW_ASSET.CUST_ASSET_NBR                     CUST_ASSET_NBR,
                                                       FW_AUTH_DTL.VEHICLE_PROMPT_ID            VEHICLE_PROMPT_ID,
                                                       DW_DRIVER.DRIVER_LAST_NM                     DRIVER_LAST_NM,
                                                       DW_DRIVER.DRIVER_FIRST_NM                    DRIVER_FIRST_NM, 
                                                       DW_DRIVER.DRIVER_MIDDLE_NM                  DRIVER_MIDDLE_NM,
                                                       FW_AUTH_DTL.DRIVER_PROMPT_ID             DRIVER_PROMPT_ID,
                                                       FW_AUTH_DTL.ODOMETER                          ODOMETER,
                                                       FW_AUTH_DTL.LOCAL_TRANS_DT                LOCAL_TRANS_DT,
                                                       FW_AUTH_DTL.LOCAL_TRANS_TM               LOCAL_TRANS_TM,
                                                       DW_SITE.TIME_ZONE                                  TIME_ZONE, -- Needs  to be  filled/updated in DW_SITE  by source
                                                       ' ' GMT_TRANSACTION_DATE,            -- Need to be added when the TIMEZONE attrrbute is updated  in DW SITE TABLE
                                                       ' ' GMT_TRANSACTION_TIME,            -- Need to be added
                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT  ACCT_TRANS_GROSS_AM,
                                                       FW_AUTH_DTL.MSG_TYPE                           MSG_TYPE,
                                                       FW_AUTH_DTL.SITE_ID                               SITE_ID,
                                                       DW_POS.LEGACY_WEX_SITE_ID                   LEGACY_WEX_SITE_ID,
                                                       DW_STKHLDR.BRAND_NM                            BRAND_NM,
                                                       DW_SITE.SITE_NM                                     SITE_NM,
                                                       DW_ADDR.ADDR_LN_1                                 ADDR_LN_1,
                                                       DW_ADDR.ADDR_CITY                                 ADDR_CITY,
                                                       DW_ADDR.ADDR_STATE_PROV_CD              ADDR_STATE_PROV_CD,
                                                       DW_ADDR.ADDR_ZIP_POSTAL_CD                ADDR_ZIP_POSTAL_CD,
                                                       DW_SITE.GEO_CD_LAT                                GEO_CD_LAT,
                                                       DW_SITE.GEO_CD_LONG                              GEO_CD_LONG,
                                                       FW_AUTH_DTL.Action                                  Action,
                                                       FW_AUTH_DTL.AUTHORIZARION_CODE         AUTHORIZARION_CODE,
                                                       FW_AUTH_DTL.AUTH_ACTION_DESC            AUTH_ACTION_DESC,
                                                       DW_PROD_CLASS.CLASS_NM                       CLASS_NM,
                                                       DW_PRODUCT.WEX_SETTLEMENT_CD          WEX_SETTLEMENT_CD,
                                                       DW_WEX_PROD_CD.SHORT_DESC                SHORT_DESC,
                                                       FW_AUTH_DTL.IND_STD_PROD_CD              IND_STD_PROD_CD,
                                                       DW_PRODUCT.PROD_LONG_NM                    PROD_LONG_NM,
                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT     ACCT_ITEM_GROSS_AMT,
                                                       FW_AUTH_DTL.ACCT_ITEM_QTY                  ACCT_ITEM_QTY,
                                                        FW_AUTH_DTL.ACCT_ITEM_UOM                 ACCT_ITEM_UOM, 
                                                       FW_AUTH_DTL.TOTAL_RECORD_COUNT        TOTAL_RECORD_COUNT,
                                                        CASE 
                                                        WHEN FW_AUTH_DTL.TOTAL_RECORD_COUNT  >  RowS_PER_Page_In
                                                        THEN  
                                                         CEIL (FW_AUTH_DTL.TOTAL_RECORD_COUNT / RowS_PER_Page_In)
                                                        ELSE 
                                                        1 
                                                        END                                                            TOTAL_PAGES, 
                                                        FW_AUTH_DTL.MSG_VERSION_NBR              MSG_VERSION_NBR
                                                  FROM (SELECT 
                                                               A.FW_AUTH_DTL_KEY,
                                                               A.DW_ACCT_KEY,
                                                               A.DW_DRIVER_KEY,
                                                               A.DW_POS_KEY,
                                                               A.DW_SITE_KEY,
                                                               A.DW_ASSET_KEY,
                                                               A.DW_PRODUCT_KEY,
                                                               A.DW_MRCH_KEY,
                                                               A.WEX_ACCT_NBR,
                                                               A.PD_ID,
                                                               A.AUTH_PD_EXP_DT,
                                                               A.VEHICLE_PROMPT_ID,
                                                               A.DRIVER_PROMPT_ID,
                                                               A.ODOMETER,
                                                               A.LOCAL_TRANS_DT,
                                                               A.LOCAL_TRANS_TM,
                                                               A.ACCT_TRANS_GROSS_AMT,
                                                               A.MSG_TYPE,
                                                               A.SITE_ID,
                                                               A.Action,
                                                               A.AUTHORIZARION_CODE,
                                                               A.AUTH_ACTION_DESC,
                                                               A.PROD_CLASS_ID,
                                                               A.IND_STD_PROD_CD,
                                                               A.ACCT_ITEM_GROSS_AMT,
                                                               A.ACCT_ITEM_QTY,
                                                               A.ACCT_ITEM_UOM,
                                                               ROW_NUMBER () OVER (ORDER BY A.FW_AUTH_DTL_KEY) row_num, 
                                                               COUNT(*)   OVER ()                                      Total_RECORD_COUNT, 
                                                               A.MSG_VERSION_NBR
                                                          FROM (SELECT /*+     parallel (FW_AUTH_DTL , 12 ) */
                                                                      FW_AUTH_DTL.FW_AUTH_DTL_KEY,
                                                                       FW_AUTH_DTL.DW_ACCT_KEY,
                                                                       FW_AUTH_DTL.DW_DRIVER_KEY,
                                                                       FW_AUTH_DTL.DW_POS_KEY,
                                                                       FW_AUTH_DTL.DW_SITE_KEY,
                                                                       FW_AUTH_DTL.DW_ASSET_KEY,
                                                                       FW_AUTH_DTL.DW_PRODUCT_KEY,
                                                                       FW_AUTH_DTL.DW_MRCH_KEY,
                                                                       FW_AUTH_DTL.WEX_ACCT_NBR,
                                                                       FW_AUTH_DTL.PD_ID,
                                                                       FW_AUTH_DTL.AUTH_PD_EXP_DT,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '1'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          VEHICLE_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_Cd3 = '3'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          DRIVER_PROMPT_ID,
                                                                       CASE
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD1 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA1
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD2 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA2
                                                                          WHEN FW_AUTH_DTL.POS_PROMPT_CD3 = '4'
                                                                          THEN
                                                                             FW_AUTH_DTL.POS_PROMPT_DATA3
                                                                       END
                                                                          ODOMETER,
                                                                       TRUNC(FW_AUTH_DTL.LOCAL_TRANS_DT)  LOCAL_TRANS_DT,
                                                                       FW_AUTH_DTL.LOCAL_TRANS_TM,
                                                                       FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT,
                                                                       FW_AUTH_DTL.MSG_TYPE,
                                                                       FW_AUTH_DTL.SITE_ID,
                                                                       CASE
                                                                           WHEN TRIM (FW_AUTH_DTL.AUTH_ACTION_CD) between  '000' and '099'
                                                                          THEN
                                                                             'Approve'
                                                                          ELSE
                                                                             'Decline'
                                                                       END
                                                                          Action,
                                                                       FW_AUTH_DTL.AUTH_APPRV_CODE AUTHORIZARION_CODE,
                                                                       FW_AUTH_DTL.AUTH_ACTION_DESC,
                                                                       FW_AUTH_DTL.PROD_CLASS_ID,
                                                                       FW_AUTH_DTL.IND_STD_PROD_CD,
                                                                       FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT,
                                                                       FW_AUTH_DTL.ACCT_ITEM_QTY,
                                                                       FW_AUTH_DTL.ACCT_ITEM_UOM, 
                                                                       FW_AUTH_DTL.MSG_VERSION_NBR
                                                                  FROM FW_AUTH_DTL
                                                                 WHERE     FW_AUTH_DTL.LOCAL_TRANS_DT BETWEEN V_START_DTTM_IN  
                                                                                                                                      AND      V_END_DTTM_IN  
                                                                             AND   FW_AUTH_DTL.AUTH_ACTION_CD BETWEEN V_AUTH_CD_START  
                                                                                                                                                and  V_AUTH_CD_END
                                                                             AND    FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT    >= V_GROSS_DOLLAR_AMT_IN                                                                    
                                                                             AND    FW_AUTH_DTL.DW_ACCT_KEY 
                                                                                                                     IN  
                                                                                                                     (SELECT /*+ parallel (DA, 12) */
                                                                                                                                  DA.DW_ACCT_KEY
                                                                                                                        FROM DW_ACCT DA 
                                                                                                                      WHERE  
                                                                                                                                (  (  V_Acct_Type_In =  'WEXACCT'   and  WEX_ACCT_NBR       =   Acct_Nbr_in ) 
                                                                                                                            OR  (  V_Acct_Type_In =  'SPNRACCT'  and  SPNR_ACCT_NBR      =   Acct_Nbr_in )
                                                                                                                            OR  (  V_Acct_Type_In =  'CUSTACCT'   and CUSTOM_ACCT_NBR = ACCT_NBR_IN )   
                                                                                                                            OR  (  V_Acct_Type_In =  'CREDITNATIONAL'  and L1_WEX_ACCT_NBR  = ACCT_NBR_IN )
                                                                                                                                 )
                                                                                                                            AND DA.ARCHIVE_FLG = 'N'               
                                                                                                                       )   
                                                                                        ) A) FW_AUTH_DTL,
                                                       DW_ACCT,
                                                       DW_DRIVER,
                                                       DW_ASSET,
                                                       DW_POS,
                                                       DW_SITE,
                                                       DW_STKHLDR,
                                                       DW_PRODUCT,
                                                       DW_WEX_PROD_CD,
                                                       DW_PROD_CLASS,
                                                       DW_ADDR
                                                 WHERE     FW_AUTH_DTL.DW_ACCT_KEY = DW_ACCT.DW_ACCT_KEY
                                                       AND FW_AUTH_DTL.DW_DRIVER_KEY = DW_DRIVER.DW_DRIVER_KEY
                                                       AND FW_AUTH_DTL.DW_ASSET_KEY = DW_ASSET.DW_ASSET_KEY
                                                       AND FW_AUTH_DTL.DW_POS_KEY = DW_POS.DW_POS_KEY
                                                       AND FW_AUTH_DTL.DW_SITE_KEY = DW_SITE.DW_SITE_KEY
                                                       AND FW_AUTH_DTL.DW_PRODUCT_KEY = DW_PRODUCT.DW_PRODUCT_KEY
                                                       AND FW_AUTH_DTL.DW_MRCH_KEY = DW_STKHLDR.DW_STKHLDR_KEY
                                                       AND DW_PRODUCT.WEX_SETTLEMENT_CD = DW_WEX_PROD_CD.PROD_CD
                                                       AND DW_PRODUCT.LANGUAGE_CD = 'ENG'
                                                       AND (    DW_PRODUCT.IND_STD_PROD_ROW_ID =
                                                                   DW_PROD_CLASS.IND_STD_PROD_ROW_ID
                                                            AND DW_PROD_CLASS.DW_CURRENT_FLG = 'Y')
                                                       AND DW_PROD_CLASS.CLASS_TYPE = 'NACS'
                                                       AND (    DW_SITE.PHYS_ADDR_ROW_ID = DW_ADDR.ADDR_ROW_ID
                                                            AND DW_ADDR.DW_CURRENT_FLG = 'Y')
                                                       AND FW_AUTH_DTL.ROW_NUM BETWEEN    V_START_RECORD  
                                                                                                             AND    V_END_RECORD    ;

  
END CASE ;                                                                                                               
                                                          
   EXCEPTION 
when NO_DATA_FOUND
    then 
       DBMS_OUTPUT.PUT_LINE('NO LOOKup KEYS FOUND') ;
 WHEN OTHERS 
  THEN
       DBMS_OUTPUT.PUT_LINE(SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) ; 
  Raise ;
  
   DBMS_APPLICATION_INFO.set_module
                                (module_name      => c_process_code,
                                   action_name      => (SUBSTR ('ERROR: ' || SQLCODE || SQLERRM,1,250)) 
                                  );                                                     
                                                 
                                                 
END SearchByAmount ;



END AUTHSEARCH ;
/

GRANT EXECUTE ON AUTHSEARCH TO SOA_LOADER ; 
GRANT EXECUTE ON AUTHSEARCH TO USER_LDAP  ; 

CREATE OR REPLACE PUBLIC SYNONYM AUTHSEARCH  FOR DW_OWNER.AUTHSEARCH ;