CREATE OR REPLACE PACKAGE DW_OWNER.AuthCapture
AS
   /******************************************************************************
      NAME:    AuthCapture
      PURPOSE:
      REVISIONS:
      Ver        Date                   Author                     Description
      ---------  ----------            ---------------  ------------------------------------
      1.0        12/14/2011        Venu  KAncharla          1. Created  just a Pacakge Header minus Body for dev integration for Loading auth data
   ******************************************************************************/
   PROCEDURE LOADAUTHDATA (
      P_ACCTID                                   IN FW_AUTH_DTL.WEX_ACCT_NBR%TYPE,
      P_ACQINSTID                              IN FW_AUTH_DTL.ACQUIRER_INSTITUTION_ID%TYPE,
      P_ACTNCODE                              IN FW_AUTH_DTL.AUTH_ACTION_CD%TYPE,
      P_ALERTCODE                             IN FW_AUTH_DTL.ALERT_CD%TYPE,
      P_AUDITNBR                                IN FW_AUTH_DTL.AUDIT_NBR%TYPE,
      P_AUTHAPPRVCODE                     IN FW_AUTH_DTL.AUTH_APPRV_CODE%TYPE,
      P_AUTHHDRID                             IN FW_AUTH_DTL.AUTHHDR_ID%TYPE,
      P_AUTHMSGID                            IN  NUMBER , -- Not needed  This needs to  be mapped
      P_BLADENM                               IN FW_AUTH_DTL.BLADENM%TYPE,
      P_CAVENC                                IN FW_AUTH_DTL.VISIBLE_CARD_AUTH_VALUE%TYPE,
      P_CAVVISB                               IN FW_AUTH_DTL.MAG_STRIPE_CARD_AUTH_VALUE%TYPE,
      P_CORRPRMPTCODE                 IN FW_AUTH_DTL.CORR_PROMPT_CD%TYPE,
      P_CORRPRMPTNBR                   IN FW_AUTH_DTL.CORR_PROMPT_NBR%TYPE,
      P_CRCYTYPE_ACCTBASIS         IN FW_AUTH_DTL.ACCT_CURRENCY_TYPE%TYPE,
      P_CRCYTYPE_MRCHBASIS        IN FW_AUTH_DTL.SITE_CURRENCY_TYPE%TYPE,
      P_CRCYTYPE_WEXBASIS         IN FW_AUTH_DTL.SITE_CURRENCY_TYPE%TYPE, -- This needs to be updated
      P_DAYOFYR                           IN FW_AUTH_DTL.DAY_OF_YR%TYPE,
      P_DISPLPDID                         IN VARCHAR2 ,      --  Not needed  FW_AUTH_DTL
      P_ERRMSG                            IN FW_AUTH_DTL.ERRMSG%TYPE,
      P_EXPDTMOYR                      IN FW_AUTH_DTL.AUTH_PD_EXP_DT%TYPE,
      P_FUNCCODE                        IN FW_AUTH_DTL.FUNC_CD%TYPE,
      P_GEOCODE                          IN FW_AUTH_DTL.GEOGRAPHY_CD%TYPE,
    --  GROSSAMT_ACCTBASIS     IN FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT%TYPE,
    --  GROSSAMT_MRCHBASIS    IN FW_AUTH_DTL.SITE_TRANS_GROSS_AMT%TYPE,
    --  GROSSAMT_WEXBASIS     IN FW_AUTH_DTL.US_TRANS_GROSS_AMT%TYPE,
      P_LASTUPDTTMS                  IN FW_AUTH_DTL.LAST_UPDT_TMS%TYPE,
      P_LOCALRSPTM                     IN FW_AUTH_DTL.LOCALRSPTM%TYPE,
      P_LOCTRANDTTM                  IN DATE, -- This needs to Poulated into  LOCAL_TRANS_Dt and LOCAL_TRANS_TM
      P_LOGQTM                            IN VARCHAR2 , -- NOt needed for Auth DTL and AUT
      P_MSGRSNCODE                  IN FW_AUTH_DTL.MSG_REASON_CD%TYPE,
      P_MSGRSPTYPE                    IN FW_AUTH_DTL.MSGRSPTYPE%TYPE,
      P_MSGTYPE                         IN FW_AUTH_DTL.MSG_TYPE%TYPE,
      P_OPTID                              IN FW_AUTH_DTL.OPT_ID%TYPE,
      P_PARENTPGMID                  IN INTEGER,                   -- this is not needed
      P_PDID                               IN FW_AUTH_DTL.PD_ID%TYPE,
      P_PDSEQNUM                     IN FW_AUTH_DTL.PD_SEQ_NBR%TYPE,
      P_PGMID                            IN INTEGER,                   -- THis is not needed
      P_POSAPPCONFIG              IN FW_AUTH_DTL.POS_APP_CONFIG%TYPE,
      P_POSDATACODE               IN FW_AUTH_DTL.POS_DATA_CD%TYPE,
      P_POSPRMPTCODE1           IN FW_AUTH_DTL.POS_PROMPT_CD1%TYPE,
      P_POSPRMPTCODE2          IN FW_AUTH_DTL.POS_PROMPT_CD2%TYPE,
      P_POSPRMPTCODE3          IN FW_AUTH_DTL.POS_PROMPT_CD3%TYPE,
      P_POSPRMPTDATA1         IN FW_AUTH_DTL.POS_PROMPT_DATA1%TYPE,
      P_POSPRMPTDATA2         IN FW_AUTH_DTL.POS_PROMPT_DATA2%TYPE,
      P_POSPRMPTDATA3         IN FW_AUTH_DTL.POS_PROMPT_DATA3%TYPE,
      P_POSTQTM                    IN NUMBER ,                       -- Not needed
      P_POSTTM                      IN NUMBER ,                       -- Not needed
      P_PROCCODE                  IN FW_AUTH_DTL.PROC_CD%TYPE,
      P_PROCQTM                   IN NUMBER ,                       -- Not needed
      P_PRODCODESETID        IN FW_AUTH_DTL.PROP_PROD_CD_SET_ID%TYPE,
      P_PRVTSITEUSAGE          IN FW_AUTH_DTL.PRIVATE_SITE_USAGE%TYPE,
      P_READTM                     IN NUMBER ,                       -- Not needed
      P_REMOTEIP                  IN FW_AUTH_DTL.REMOTE_IP%TYPE,
      P_REMOTEPORT            IN FW_AUTH_DTL.REMOTE_PORT%TYPE,
      P_RETVREFNUM             IN FW_AUTH_DTL.TICKET_NBR%TYPE,
      P_RFID                         IN VARCHAR2 ,             -- this is not needed
      P_SERVLEVL                 IN CHAR ,                  -- this is not needed
      P_SITEID                      IN FW_AUTH_DTL.SITE_ID%TYPE,
      P_TANDEMQTM             IN NUMBER ,               -- This is not needed
      P_TANDEMRSPTM          IN FW_AUTH_DTL.TANDEMRSPTM%TYPE,
      P_TERMID                     IN FW_AUTH_DTL.TERMINAL_ID%TYPE,
      P_TERMPRFX                 IN FW_AUTH_DTL.TERMINAL_PRFX%TYPE,
      P_TRACKTWO                IN FW_AUTH_DTL.TRACKTWO%TYPE,
      P_VERNBR                      IN FW_AUTH_DTL.MSG_VERSION_NBR%TYPE,
      P_WRITEQTM                     IN NUMBER ,               -- this is not needed
      P_WRITETM                        IN NUMBER ,               -- this is not needed
      P_AUTHDTLSEQNUM             IN FW_AUTH_DTL.AUTH_DTL_SEQ_NUM%TYPE,
    --  AUTHHDRID                     IN FW_AUTH_DTL.AUTHHDR_ID%TYPE,
      P_EXTPRODCODE                 IN FW_AUTH_DTL.PROP_PROD_CD%TYPE,
      P_GROSSAMT_ACCTBASIS   IN FW_AUTH_DTL.ACCT_ITEM_GROSS_AMT%TYPE,
      P_GROSSAMT_MRCHBASIS   IN FW_AUTH_DTL.SITE_ITEM_GROSS_AMT%TYPE,
      P_GROSSAMT_WEXBASIS    IN FW_AUTH_DTL.US_ITEM_GROSS_AMT%TYPE,
      P_INTRNPRODID                  IN FW_AUTH_DTL.IND_STD_PROD_CD%TYPE,
    --  LASTUPDTTMS                IN TIMESTAMP ,             -- this is not needed
    --  MSGTYPE                         IN VARCHAR2 ,
      P_NACSCD                          IN FW_AUTH_DTL.NACSCD%TYPE,
      P_PRODCLSID                     IN FW_AUTH_DTL.PROD_CLASS_ID%TYPE,
      P_QTY_ACCTBASIS             IN FW_AUTH_DTL.ACCT_ITEM_QTY%TYPE,
      P_QTY_MRCHBASIS            IN FW_AUTH_DTL.SITE_ITEM_QTY%TYPE,
      P_QTY_WEXBASIS              IN FW_AUTH_DTL.US_ITEM_QTY%TYPE,
      P_SVCLVL                         IN FW_AUTH_DTL.ITEM_SERVICE_LEVEL%TYPE,
      P_UNITPRC_ACCTBASIS     IN FW_AUTH_DTL.ACCT_ITEM_UNT_PRC%TYPE,
      P_UNITPRC_MRCHBASIS    IN FW_AUTH_DTL.SITE_ITEM_UNT_PRC%TYPE,
      P_UNITPRC_WEXBASIS             IN FW_AUTH_DTL.US_ITEM_UNT_PRC%TYPE,
      P_UOM_ACCTBASIS                  IN FW_AUTH_DTL.ACCT_ITEM_UOM%TYPE,
      P_UOM_MRCHBASIS                  IN FW_AUTH_DTL.SITE_ITEM_UOM%TYPE,
      P_UOM_WEXBASIS                    IN FW_AUTH_DTL.US_ITEM_UOM%TYPE ,
      P_SITE_TRANS_GROSS_AMT     IN FW_AUTH_DTL.SITE_TRANS_GROSS_AMT%TYPE ,
      P_ACCT_TRANS_GROSS_AMT    IN FW_AUTH_DTL.ACCT_TRANS_GROSS_AMT%TYPE,
      P_US_TRANS_GROSS_AMT        IN FW_AUTH_DTL.US_TRANS_GROSS_AMT%TYPE    );
END AuthCapture;
/