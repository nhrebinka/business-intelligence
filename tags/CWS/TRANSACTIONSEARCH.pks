CREATE OR REPLACE PACKAGE DW_OWNER.TRANSACTIONSEARCH AS

TYPE  TransDtlListtyp IS REF CURSOR;

-- Interface for Transaction Search by Account

PROCEDURE SearchByAccount(START_DT_in         IN     VARCHAR2,
                                             START_TM_in        IN     VARCHAR2,
                                             END_DT_in             IN     VARCHAR2,
                                             END_TM_in             IN     VARCHAR2,
                                             Date_Type_in          IN     VARCHAR2,
                                             Timezone_in           IN      VARCHAR2,
                                             Acct_Nbr_in             IN     VARCHAR2,
                                             ACCT_TYPE_In        IN      VARCHAR2 ,
                                             Product_Type_in      IN      VARCHAR2,
                                             Unbilled_Only_in      IN       VARCHAR2,
                                             Start_Page_in          IN       INTEGER,
                                             Rows_Per_Page_in   IN       INTEGER,
                                             TransDetail_out        OUT    TransDtlListtyp);


-- Interface for Transaction Search by Purchase Device

PROCEDURE SearchByPurchaseDevice(START_DT_in              IN     VARCHAR2,
                                                       START_TM_in              IN     VARCHAR2,
                                                       END_DT_in                   IN     VARCHAR2,
                                                       END_TM_in                   IN     VARCHAR2,
                                                       Date_Type_in                IN     VARCHAR2,
                                                       Timezone_in                 IN     VARCHAR2,
                                                       Acct_Nbr_in                  IN     VARCHAR2,
                                                       ACCT_TYPE_In              IN     VARCHAR2 ,
                                                       Purch_Device_Nbr_in     IN     VARCHAR2,
                                                       Start_Page_in               IN     INTEGER,
                                                       Rows_Per_Page_in        IN     INTEGER,
                                                       TransDetail_out            OUT   TransDtlListtyp);

-- Interface for Transaction Search by Asset

PROCEDURE SearchByAsset(START_DT_in             IN     VARCHAR2,
                                         START_TM_in            IN      VARCHAR2,
                                         END_DT_in                 IN      VARCHAR2,
                                         END_TM_in                 IN      VARCHAR2,
                                         Date_Type_in              IN      VARCHAR2,
                                         Timezone_in               IN      VARCHAR2,
                                         Acct_Nbr_in                IN      VARCHAR2,
                                         ACCT_TYPE_In            IN      VARCHAR2 ,
                                         Vin_in                         IN      VARCHAR2,
                                         Custom_Asset_Id_in     IN      VARCHAR2,
                                         Vehicle_Prompt_Id_in   IN      VARCHAR2,
                                         Start_Page_in              IN      INTEGER,
                                         Rows_Per_Page_in       IN      INTEGER,
                                         TransDetail_out           OUT   TransDtlListtyp);

-- Interface for Transaction Search by Driver

PROCEDURE SearchByDriver(START_DT_in                     IN     VARCHAR2,
                                          START_TM_in                    IN     VARCHAR2,
                                          END_DT_in                         IN     VARCHAR2,
                                          END_TM_in                         IN     VARCHAR2,
                                          Date_Type_in                      IN     VARCHAR2,
                                          Timezone_in                       IN      VARCHAR2,
                                          Acct_Nbr_in                        IN     VARCHAR2,
                                          ACCT_TYPE_In                   IN      VARCHAR2 ,
                                          Driver_Prompt_Id_in           IN     VARCHAR2,
                                          Driver_First_Name_in          IN     VARCHAR2,
                                          Driver_Last_Name_in           IN     VARCHAR2,
                                          Driver_Middle_Name_in        IN     VARCHAR2,
                                          Company_Driver_Id_in         IN     VARCHAR2,
                                          Start_Page_in                     IN     INTEGER,
                                          Rows_Per_Page_in              IN     INTEGER,
                                          TransDetail_out                  OUT    TransDtlListtyp);

-- Interface for Transaction Search by Card Department

PROCEDURE SearchByCardDepartment(START_DT_in         IN     VARCHAR2,
                                                         START_TM_in        IN     VARCHAR2,
                                                         END_DT_in             IN     VARCHAR2,
                                                         END_TM_in             IN     VARCHAR2,
                                                         Date_Type_in          IN     VARCHAR2,
                                                         Timezone_in           IN      VARCHAR2,
                                                         Acct_Nbr_in             IN     VARCHAR2,
                                                         ACCT_TYPE_In        IN      VARCHAR2 ,
                                                        Card_Dept_in           IN     VARCHAR2,
                                                        Start_Page_in           IN     INTEGER,
                                                        Rows_Per_Page_in     IN     INTEGER,
                                                        TransDetail_out      OUT    TransDtlListtyp);

-- Interface for Transaction Search by Region

PROCEDURE SearchByRegion(START_DT_in         IN     VARCHAR2,
                                           START_TM_in        IN     VARCHAR2,
                                           END_DT_in             IN     VARCHAR2,
                                           END_TM_in             IN     VARCHAR2,
                                           Date_Type_in          IN     VARCHAR2,
                                           Timezone_in           IN      VARCHAR2,
                                           Acct_Nbr_in             IN     VARCHAR2,
                                           ACCT_TYPE_In        IN      VARCHAR2 ,
                                           Site_City_in             IN     VARCHAR2,
                                           Site_State_in           IN     VARCHAR2,
                                           Site_Zip_in              IN     VARCHAR2,
                                           Start_Page_in          IN     INTEGER,
                                           Rows_Per_Page_in   IN     INTEGER,
                                           TransDetail_out      OUT    TransDtlListtyp);

-- Interface for Transaction Search by Amount

PROCEDURE SearchByAmount(START_DT_in         IN     VARCHAR2,
                                             START_TM_in        IN     VARCHAR2,
                                             END_DT_in             IN     VARCHAR2,
                                             END_TM_in             IN     VARCHAR2,
                                             Date_Type_in          IN     VARCHAR2,
                                             Timezone_in           IN      VARCHAR2,
                                             Acct_Nbr_in             IN     VARCHAR2,
                                             ACCT_TYPE_In        IN      VARCHAR2 ,
                                            Gross_Amt_in           IN     NUMBER,
                                            Nbr_Trans_Units_in   IN     NUMBER,
                                            Price_Per_Unit_in      IN     NUMBER,
                                            Start_Page_in           IN     INTEGER,
                                            Rows_Per_Page_in   IN     INTEGER,
                                            TransDetail_out      OUT    TransDtlListtyp);

END TRANSACTIONSEARCH;
/