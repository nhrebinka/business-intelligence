SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000


spool /i1/&&env/hub/data/m_spnr_pgm_edm;


DELETE FROM EDM_OWNER.M_SPNR_PGM_XREF MP
      WHERE MP.EDM_SPNR_PGM_KEY IN
               (SELECT MP.EDM_SPNR_PGM_KEY
                  FROM EDM_OWNER.M_SPNR_PGM MP
                 WHERE MP.PROGRAM_PACKAGE_NAME IN
                          ('Gate Station Card Private Label Unfunded',
                           'Lisa W Test Program D4'));

COMMIT;

DELETE FROM EDM_OWNER.M_SPNR_PGM MP
      WHERE MP.PROGRAM_PACKAGE_NAME IN
               ('Gate Station Card Private Label Unfunded',
                'Lisa W Test Program D4');

COMMIT;



UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '000598'
 WHERE MP.PGM_NM = 'Ward Oil';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '000072'
 WHERE MP.PGM_NM = 'Prima';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '400038'
 WHERE MP.PGM_NM = 'MasterCard';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '400071'
 WHERE MP.PGM_NM = 'Waring Oil (Affinity)';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '400054'
 WHERE MP.PGM_NM = 'Private Label';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '400062'
 WHERE MP.PGM_NM = 'Sinclair Oil Corporation';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '400018'
 WHERE MP.PGM_NM = 'Exxon Mobil WEX';

UPDATE M_SPNR_PGM MP
   SET MP.EDM_LAST_UPDT_SESSION_NM =
          'One Shot manual Update for RelatinShipCode',
       MP.EDM_LAST_UPDT_DT = SYSDATE,
       MP.RELATION_CD = '000055'
 WHERE MP.PGM_NM = '76/Circle K';


COMMIT;


MERGE INTO EDM_OWNER.M_SPNR_PGM PM
     USING (SELECT M_SPNR_PGM.EDM_SPNR_PGM_KEY, SRC_SBL_SPNR_PGM.RELATION_CD
              FROM STAGE_OWNER.SRC_SBL_SPNR_PGM,
                   M_SPNR_PGM_XREF,
                   M_SPNR_PGM
             WHERE M_SPNR_PGM.EDM_SPNR_PGM_KEY =
                      M_SPNR_PGM_XREF.EDM_SPNR_PGM_KEY
                   AND (SRC_SBL_SPNR_PGM.SRC_SYS_LAST_UPDT_DT,
                        SRC_SBL_SPNR_PGM.PGM_ROW_ID) IN
                          (  SELECT MAX (SRC_SBL_SPNR_PGM.SRC_SYS_LAST_UPDT_DT),
                                    SRC_SBL_SPNR_PGM.PGM_ROW_ID
                               FROM STAGE_OWNER.SRC_SBL_SPNR_PGM
                           GROUP BY SRC_SBL_SPNR_PGM.PGM_ROW_ID)
                   AND SRC_SBL_SPNR_PGM.PGM_ROW_ID =
                          M_SPNR_PGM_XREF.SPNR_PGM_PK) S
        ON (PM.EDM_SPNR_PGM_KEY = S.EDM_SPNR_PGM_KEY)
WHEN MATCHED
THEN
   UPDATE SET
      PM.RELATION_CD = S.RELATION_CD,
      PM.EDM_LAST_UPDT_SESSION_NM = 'One Shot Update for RelatinShipCode',
      EDM_LAST_UPDT_DT = SYSDATE;

COMMIT;



spool off;