SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
set LINESIZE 2000


spool /i1/&&env/hub/data/d_program_delete_edw ; 

DELETE FROM EDW_OWNER.D_PROGRAM DP
      WHERE DP.PROGRAM_PACKAGE_NAME IN
               ('Jackson Oil Solvents D2',
                'Foster Blue Water Oil LLC (542) Basic',
                'Foster Oil 0460 Non Issuing D4',
                'Sinclair Commercial Card D4',
                'OD Snider & Son Inc Enhanced',
                'Lisa W Test Program D4',
                'Gate Station Card Private Label Unfunded');


COMMIT;

spool off;