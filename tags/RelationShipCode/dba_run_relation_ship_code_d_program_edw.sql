SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING OFF
SET TRIMSPOOL ON
SET ESCAPE ON
SET TAB OFF
set LINESIZE 2000



spool /i1/&&env/hub/data/d_program; 

ALTER TABLE EDW_OWNER.D_PROGRAM ADD RELATIONSHIP_CODE  VARCHAR2(200)NOT NULL ;


MERGE INTO EDW_OWNER.D_PROGRAM DP
     USING LINK_OWNER.D_PROGRAM MP 
        ON (DP.PROGRAM_KEY  = MP.PROGRAM_KEY) 
WHEN MATCHED THEN
   UPDATE SET DP.RELATIONSHIP_CODE = MP.RELATIONSHIP_CODE;
   
COMMIT;

 ALTER TABLE EDW_OWNER. D_PROGRAM  DROP CONSTRAINT NK_PROGRAM_AK1;
 
 DROP  INDEX  NK_PROGRAM_AK1 ;



CREATE UNIQUE INDEX NK_PROGRAM_AK1 ON EDW_OWNER.D_PROGRAM
(RELATIONSHIP_CODE)
NOLOGGING
TABLESPACE I_BI
NOPARALLEL ;

ALTER TABLE EDW_OWNER.D_PROGRAM ADD (CONSTRAINT NK_PROGRAM_AK1
UNIQUE (RELATIONSHIP_CODE)
USING INDEX NK_PROGRAM_AK1   ENABLE VALIDATE);


DELETE FROM EDW_OWNER.D_PROGRAM DP
      WHERE DP.PROGRAM_PACKAGE_NAME IN
               ('Jackson Oil Solvents D2',
                'Foster Blue Water Oil LLC (542) Basic',
                'Foster Oil 0460 Non Issuing D4',
                'Sinclair Commercial Card D4',
                'OD Snider \& Son Inc Enhanced',
                'Lisa W Test Program D4',
              'Gate Station Card Private Label Unfunded');
COMMIT;




spool off;