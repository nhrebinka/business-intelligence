SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000



spool /i1/&&env/hub/data/src_ps_item_activity.dat ; 
              
select 
  BUSINESS_UNIT      ||'~|~'||
  CUST_ID            ||'~|~'||
  ITEM               ||'~|~'||
  ITEM_LINE          ||'~|~'||
  ITEM_SEQ_NUM       ||'~|~'||
  ENTRY_TYPE         ||'~|~'||
  ENTRY_REASON       ||'~|~'||
  ENTRY_AMT          ||'~|~'||
  ENTRY_EVENT        ||'~|~'||
 TO_CHAR ( ACCOUNTING_DT, 'MM/DD/YYYY HH24:mi:ss')    	||'~|~'||
 TO_CHAR ( POST_DT      , 'MM/DD/YYYY HH24:mi:ss')	||'~|~'||
 TO_CHAR  (ASOF_DT      , 'MM/DD/YYYY HH24:mi:ss')	||'~|~'||
  DOCUMENT           ||'~|~'||
  DOCUMENT_LINE      ||'~|~'||
  DEPOSIT_BU         ||'~|~'||
  DEPOSIT_ID         ||'~|~'||
  PAYMENT_SEQ_NUM    ||'~|~'||
  DRAFT_BU           ||'~|~'||
  DRAFT_ID           ||'~|~'||
  GROUP_BU           ||'~|~'||
  GROUP_ID           ||'~|~'||
  GROUP_SEQ_NUM      ||'~|~'||
  HIST_STATUS        ||'~|~'||
  SUBCUST_QUAL1      ||'~|~'||
  SUBCUST_QUAL2      ||'~|~'||
  SUBCUST_STATUS     ||'~|~'||
  ENTRY_CURRENCY     ||'~|~'||
  RT_TYPE            ||'~|~'||
  RATE_MULT          ||'~|~'||
  RATE_DIV           ||'~|~'||
  PAYMENT_AMT        ||'~|~'||
  PAYMENT_CURRENCY   ||'~|~'||
  PYMT_RT_TYPE       ||'~|~'||
  PYMT_RATE_MULT     ||'~|~'||
  PYMT_RATE_DIV      ||'~|~'||
  ENTRY_AMT_BASE     ||'~|~'||
  CURRENCY_CD        ||'~|~'||
  REAL_GAIN_LOSS     ||'~|~'||
  ENTRY_USE_ID       ||'~|~'||
  ORIGIN_ID          ||'~|~'||
  PAYMENT_ID         ||'~|~'||
  GROUP_TYPE         ||'~|~'||
  VOUCHER_ID         ||'~|~'||
  SENT_TO_AP         ||'~|~'||
  CONSOL_BUS_UNIT    ||'~|~'||
  CONSOL_INVOICE     ||'~|~'||
  CR_ANALYST         ||'~|~'||
  SALES_PERSON       ||'~|~'||
  COLLECTOR          ||'~|~'||
  PO_REF             ||'~|~'||
  PO_LINE            ||'~|~'||
  BILL_OF_LADING     ||'~|~'||
  ORDER_NO           ||'~|~'||
  CONTRACT_NUM       ||'~|~'||
  BUSINESS_UNIT_BI   ||'~|~'||
  BUSINESS_UNIT_OM   ||'~|~'||
  BANK_SETID         ||'~|~'||
  BANK_CD            ||'~|~'||
  BANK_ACCT_KEY      ||'~|~'||
  DD_BU              ||'~|~'||
  DD_ID              ||'~|~'||
  POSTED_PI          ||'~|~'||
  ARRE_STATUS        ||'~|~'||
  PROCESS_INSTANCE   ||'~|~'||
  USER_AMT1          ||'~|~'||
  USER_AMT2          ||'~|~'||
  USER_AMT3          ||'~|~'||
  USER_AMT4          ||'~|~'||
  USER_AMT5          ||'~|~'||
  USER_AMT6          ||'~|~'||
  USER_AMT7          ||'~|~'||
  USER_AMT8          ||'~|~'||
  TO_CHAR(USER_DT1 , 'MM/DD/YYYY HH24:mi:ss' ) ||'~|~'||
  TO_CHAR(USER_DT2 , 'MM/DD/YYYY HH24:mi:ss' ) ||'~|~'||
  TO_CHAR(USER_DT3 , 'MM/DD/YYYY HH24:mi:ss' ) ||'~|~'||
  TO_CHAR(USER_DT4 , 'MM/DD/YYYY HH24:mi:ss' ) ||'~|~'||
  USER1              ||'~|~'||
  USER2              ||'~|~'||
  USER3              ||'~|~'||
  USER4              ||'~|~'||
  USER5              ||'~|~'||
  USER6              ||'~|~'||
  USER7              ||'~|~'||
  USER8              ||'~|~'||
  USER9              ||'~|~'||
  USER10             ||'~|~'||
  ST_ID_NUM          ||'~|~'||
  VAT_ADVPAY_FLG     ||'~|~'||
  UNPOST_REASON      ||'~|~'||
  PC_DISTRIB_STATUS  ||'~|~'||
  WS_REASON          ||'~|~'||
  SUB_GROUP_ID       
 from PS_ITEM_ACTIVITY
where post_dt between  &post_dt_from and &post_dt_to  ;  

  
spool off ; 
    

  


