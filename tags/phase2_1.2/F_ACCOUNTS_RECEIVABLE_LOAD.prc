CREATE OR REPLACE PROCEDURE EDW_OWNER.F_ACCOUNTS_RECEIVABLE_LOAD
AS
   
/******************************************************************************
   NAME:       F_ACCOUNTS_RECEIVABLE_LOAD
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/16/2011   SSirigiri       1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     F_ACCOUNTS_RECEIVABLE_ENTRY
      Sysdate:         3/16/2011
      Date and Time:   3/16/2011, 4:06:10 PM, and 3/16/2011 4:06:10 PM
      Username:        SSirigiri (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN
   INSERT                   /*+ append */
         INTO F_ACCOUNTS_RECEIVABLE_ENTRY (ACCOUNTS_RECEIVABLE_ENTRY_KEY,
                                           ACCOUNT_RECEIVABLE_TYPE_KEY,
                                           DATE_KEY,
                                           ACCOUNT_KEY,
                                           PROGRAM_KEY,
                                           ENTRY_AMOUNT,
                                           ACCOUNT_RECEIVABLE_COUNT1,
                                           ROW_LAST_MOD_PROC_SEQ_NBR,
                                           ROW_CREATE_DTTM,
                                           ROW_LAST_MOD_DTTM,
                                           ROW_LAST_MOD_PROC_NM)
      (SELECT                                       /*+ parallel (FTLI, 4 ) */
              ACCOUNTS_RECEIVABLE_ENTRY_KEY,
              ACCOUNT_RECEIVABLE_TYPE_KEY,
              DATE_KEY,
              ACCOUNT_KEY,
              PROGRAM_KEY,
              ENTRY_AMOUNT,
              ACCOUNT_RECEIVABLE_COUNT1,
              ROW_LAST_MOD_PROC_SEQ_NBR,
              ROW_CREATE_DTTM,
              ROW_LAST_MOD_DTTM,
              ROW_LAST_MOD_PROC_NM
         FROM EDW_STAGE_OWNER.F_ACCOUNTS_RECEIVABLE_ENTRY FTLI );

COMMIT;
END F_ACCOUNTS_RECEIVABLE_LOAD;
/

CREATE OR REPLACE PUBLIC SYNONYM F_ACCOUNTS_RECEIVABLE_LOAD FOR EDW_OWNER.F_ACCOUNTS_RECEIVABLE_LOAD ; 

GRANT EXECUTE ON  F_ACCOUNTS_RECEIVABLE_LOAD TO DWLOADER ; 