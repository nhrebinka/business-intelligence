SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_bill_cd.dat;

SELECT    Dw_BILL_CD_KEY
       || '~|~'
       || BILL_CD
       || '~|~'
       || LANGUAGE_CD
       || '~|~'
       || BILL_CD_SHORT_DESC
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || BILL_CD_CATEGORY
       || '~|~'
       || INV_SORT_ORDER_NBR
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || ''
       || '~|~'
       || DEPT_FLG
       || '~|~'
       || BILL_CD_LANG_DESC
  FROM DW_BILL_CD
 WHERE DW_CURRENT_FLG = 'Y';

spool off;

