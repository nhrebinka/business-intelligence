/* Formatted on 8/23/2011 2:26:20 PM (QP5 v5.139.911.3011) */
SET ECHO    ON
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000


spool /i1/&&env/hub/logs/d_termination_purchase_device_upd.log;


MERGE INTO EDM_OWNER.D_PURCHASE_DEVICE DP
     USING EDM_OWNER.M_PURCH_DEVICE MP
        ON (DP.EMBOSSED_CARD_NUMBER_ID = MP.EMBOSS_PD_NBR)
WHEN MATCHED
THEN
   UPDATE SET
      DP.ROW_MD5_CHECKSUM_HEX = ' ',DP.ROW_LAST_MOD_PROC_NM='One Shot Termination Date update'
           WHERE DP.TERMINATED_DATE <> MP.PD_SETUP_DT
                 AND MP.PD_STS = 'Terminated';
COMMIT;

spool off;