SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/d_purchase_device_upd.log ; 
  

UPDATE EDM_OWNER.D_PURCHASE_DEVICE DP
   SET DP.ACTIVATED_DATE = DP.ISSUED_DATE, DP.ROW_LAST_MOD_PROC_NM='One Shot Activation Date update'
     WHERE DP.ACTIVATED_DATE != DP.ISSUED_DATE ; 
 
 COMMIT ;  

  
spool off ; 
    

  


