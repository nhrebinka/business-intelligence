/**
 * File:		ReadEnvironment.java
 * 
 * Description:	This class reads the OS environment variables
 *              and then sets each name/value pair into a Properties 
 *              object.  Return the Properties object that can be referenced
 *              for individual environment variable values.
 * 
 * Modification History :
 * 
 * Date         Name               Program Modification
 * --------     -----------------  -------------------------------------------
 * 04/28/2008   David Black        Created.
 */

package com.wex.bi.common;

import java.io.*;
import java.util.*;

public class ReadEnvironment {

	public static Properties getEnvVars() 
			throws Throwable {
		  
	Process p = null;
	Properties envVars = new Properties();
	Runtime r = Runtime.getRuntime();
	String OS = System.getProperty("os.name").toLowerCase();
	  
	//Get a list of environment variables
	if (OS.indexOf("windows 9") > -1) {
		p = r.exec( "command.com /c set" );
	} else if (    (OS.indexOf("nt") > -1)
			  	|| (OS.indexOf("windows 2000") > -1 )
			  	|| (OS.indexOf("windows xp") > -1) ) {
		p = r.exec( "cmd.exe /c set" );
	} else {
		p = r.exec( "env" );
	}

	BufferedReader br = new BufferedReader ( new InputStreamReader( p.getInputStream() ) );
		  
	//Read the environment variables and set the name/value pairs to the Properties object
	String line;
	while( (line = br.readLine()) != null ) {
		int idx = line.indexOf( '=' );
		String key = line.substring( 0, idx );
		String value = line.substring( idx+1 );
		envVars.setProperty( key, value );
		//System.out.println( key + " = " + value );
	}
		  
	return envVars;
		
	}

}
