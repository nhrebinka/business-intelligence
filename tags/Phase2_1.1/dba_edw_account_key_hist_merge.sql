SET ECHO    ON 
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON 
SET HEADING      ON 
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000
DEFINE &&env = &1

spool /i1/&&env/hub/logs/dba_edw_ACCOUNT_HIST_KEY_merge.log ; 


UPDATE D_ACCOUNT D  
SET 
    D.CURRENT_RECORD_FLG = '0'
WHERE 
       Account_key <= 0 ;  
       
COMMIT ;

MERGE INTO D_ACCOUNT D
     USING (  SELECT SOURCE_ACCOUNT_ID SOURCE_ACCOUNT_ID,
                     MIN (ACCOUNT_KEY) ACCOUNT_HIST_KEY
                FROM D_ACCOUNT
            GROUP BY SOURCE_ACCOUNT_ID) D1
        ON (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED
THEN
   UPDATE SET
                    D.ACCOUNT_HIST_KEY = D1.ACCOUNT_HIST_KEY,
                    D.ROW_LAST_MOD_DTTM = SYSDATE,
                    D.ROW_LAST_MOD_PROC_NM = 'ACCOUNT_HIST_KEY DATA FIX  UPDATE ';
    
commit ;
    
     
merge  into D_ACCOUNT D  using EDM_OWNER.D_ACCOUNT@PRHUB.PROD  M 
 on  ( D.ACCOUNT_KEY   = M.ACCOUNT_KEY  ) 
 WHEN MATCHED THEN 
   UPDATE SET 
                  D.CLM_CLUSTER_NAME          =   M.CLM_CLUSTER_NAME  		,
                  D.ROW_MD5_CHECKSUM_HEX_T2   =   M.ROW_MD5_CHECKSUM_HEX_T2 	, 
                  D.ROW_LAST_MOD_DTTM         =   sysdate  			,
                  D.ROW_LAST_MOD_PROC_NM      =  'Cluster Name Update Data Fix'   ; 
                  
commit ; 

 

spool OFF  ;