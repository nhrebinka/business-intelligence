#!/bin/ksh

##############################################Update Information:  (01/12/10)#######################################################
#  Name:  Ed Osmolski
#  Description:  Modified script for BI phase 1 project.  Requires the following new parms added to the pc_env_var file
#	 PCBIWFLUSER, PCBIWFLPASSWORD
#  OLD LINE:  Modified email group.
#  NEW OR MODIFIED LINE:  
################################################################################################################################
################################################################################################################################
#  Name:  Edward Osmolski       Company:  Wright Express          Date:  06/29/2004
#  Description:  This script will allow the user to run a single workflow(file, dimension, or fact).  It relies on an ini file
#  which must reside in the same directory as this script.  
################################################################################################################################
#####################################################################################################################################
#  Precondition:  This function will recieve no variables.  If it is called, when an error has occured.
#  Postcondition:  This function will complete the ending procedure of the script and notify the PC group of failure through an 
#  email.  This function will always exit with a 16.
#####################################################################################################################################
exit_script_16() {

  MAIL_LIST="ed_osmolski@wrightexpress.com"               	#the email mail list
  echo "\nEnd bi_runpcwfl.sh: `date`">>$errlog			#End log message
  mailx -s "bi_runpcwfl.sh error" $MAIL_LIST < "$errlog" #Send email to the powercenter group
  exit 16
  
}
################################################################################################################################
#  Precondition:  This function will only be called on a unix error.
#  Postcondition:  This function will then try to remove the lockfile if it exists before exiting the program.
################################################################################################################################
trap_exit() {

echo "\nbi_runpcwfl.sh failed due to a Unix error.">>$errlog
echo "Either due to a Hang Up, Control-c, Quit Key, or SIGTERM. ">>$errlog
echo "Check the PC workflow montior to see if the job has completed, is running, or never ran. ">>$errlog
echo "\t1) If the job completed, then force ok. ">>$errlog
echo "\t2) If the job is running, wait until it finishes and then force ok. ">>$errlog
echo "\t3) If the job never ran, restart from control-m.  If it fails again for the same ">>$errlog
echo "\treason, contact the unix admins and let them know that the job is failing due \n\tto a unix issue. ">>$errlog
exit_script_16

}
################################################################################################################################
#  Precondition:  This function will recieve a folder($1) and workflow name($2).  This function has been shut down for now cause
#  I am tired of getting an error message for passwords not being up to date.  10/13/2009
#  Postcondition:  This function will then try to find the latest error for this folder.
################################################################################################################################
Find_Err_Msg(){

SQL_ERR1="SELECT DENSE_RANK() OVER (ORDER BY SESSION_NAME) || ')' || CHR(13) || RPAD('Start Time: ' || "
SQL_ERR2="TO_CHAR(ACTUAL_START, 'MM/DD/YYYY HH:MI:SS AM'),77,' ') || RPAD('Sess Nm: ' || SESSION_NAME,80,' ') || "
SQL_ERR3="'**1st Err Msg: ' || CHR(13) || TRIM(CHR(13) FROM FIRST_ERROR_MSG) || '**' "
SQL_ERR4="FROM PCENTER.REP_SESS_LOG "
SQL_ERR5="WHERE UPPER(SUBJECT_AREA) = UPPER('$1') AND UPPER(WORKFLOW_NAME) = UPPER('$2') "
SQL_ERR6="AND LAST_ERROR IS NOT NULL AND (ACTUAL_START, SESSION_NAME) IN "
SQL_ERR7="(SELECT MAX(ACTUAL_START), SESSION_NAME FROM PCENTER.REP_SESS_LOG WHERE LAST_ERROR IS NOT NULL AND "
SQL_ERR8="TRUNC(ACTUAL_START) BETWEEN TRUNC(SYSDATE)-1 AND TRUNC(SYSDATE) GROUP BY SESSION_NAME) "
SQL_ERR9="ORDER BY ACTUAL_START DESC"
SQL_ERR=$SQL_ERR1$SQL_ERR2$SQL_ERR3$SQL_ERR4$SQL_ERR5$SQL_ERR6$SQL_ERR7$SQL_ERR8$SQL_ERR9

ERR_SET=`sqlplus -silent $PCINFSQLLOGIN << EOF
whenever sqlerror exit failure
set hea off
set feedback off
$SQL_ERR;
EOF`

if [ $? -ne 0 ] ; then
  echo "The sqlplus command failed.  Please check the query and try again.">>$errlog
  echo "\n$ERR_SET">>$errlog
else
  echo "PowerCenter Error(May contain day old errors.):\n$ERR_SET">>$errlog
fi

}
################################################################################################################################
#  Precondition:  This function will recieve a workflow folder name($1), a workflow name($2) and a logfile name($3).
#  Postcondition:  This function will then find out if we need to run the workflow for that file or not.  NOTE:  This function
#  to run files is slightly different than the functions to run facts or dimensions.  In facts and dimensions, if you try to run
#  an invalid worflow, informatica will catch it and send out the appropriate to this script to be later written to a log file.
#  Since the files require a lookup into the DO_SCHEDULE table, if the user(control - M) enters in an invalid workflow name to 
#  be run.  The SQL statement will not find it.  However, at this point it will be assumed that the file does not need to be
#  run and therefore the script will send a message to the log saying that the file was not scheduled to run.  An error message
#  will not be sent to the user(control - M).  This may be an issue that needs to be addressed.  If so, a way to determine what
#  is and what is not a workflow will have to be addressed and solved in order to remove this problem.  This problem will be 
#  ignored for now.  
################################################################################################################################
run_file() {

RESULT=`sqlplus -silent $PCDWSQLLOGIN << EOF
whenever sqlerror exit failure
set hea off
set feedback off
select count(1)
from do_schedule
where upper(rpt_executable_nm) = upper('$2') and upper(RPT_SCHEDULE_STS) = 'SCHEDULED';
EOF`

if [ $? -ne 0 ] ; then
  echo "The sqlplus command failed.  Please check the query and try again.">>$3
  exit_script_16
fi

if [ $RESULT -ne 0 ] ; then                     #The workflow was found in the schedule table.

  pmcmd startworkflow -sv $PCINTSERVNM -d $PCDOMAINNM -u $PCBIWFLUSER -p $PCBIWFLPASSWORD -f $1 -wait $2
  
  if [ $? -ne 0 ] ; then			#find out if pmcmd failed or not
    #Find_Err_Msg $1 $2				#See if we can find the err msg.  Removed for 9.2 cause of DB issues
    echo "\n$2 has failed. Please check the workflow monitor for more details.">>$errlog
    exit_script_16				#exit the script and mail the error message to the PC group
  fi

else
  echo "$2 was not found in the schedule table.  Therefore it does not need to run.">>$3
fi
}
              
#####################################################Start Main Code############################################################
#  Precondition:  This script will recieve a folder name($1), a workflow name($2) a workflow area($3) and an environment($4).
#  Postcondition:  This script will then take the information provided by the user and run the appropriate workflows. If needed.
#  Special Note:  Param 4 could be removed entirely if we want to change all of the control-m objects.  It's not used right now.
#  This script also needs the following variables defined in the infor(env) user for PC in order for this script to work.
#  PCSCRIPTLOGS, PCINFASERVPORT, PCBIWFLUSER, PCBIWFLPASSWORD, PCINFSQLLOGIN, PCDWSQLLOGIN.
################################################################################################################################
trap 'trap_exit;exit 16' 1 2 3 15    #try to trap Hang Up(1), Control-c(2), Quit Key(3), and SIGTERM(15), and exit gracefully

. pc_var					#export all of the variables
errlog="$PCSCRIPTLOGS$2.err"			#error log file to record information about this run

rm -f $errlog					#try to remove this log

echo "Start bi_runpcwfl.sh: `date` \nParam 1(FLDR): $1\nParam 2(WKFL): $2\nParam 3(AREA): $3">$errlog
#echo "Param 4(ENV): $4\n">>$errlog

#if [ $# -ne 4 ] ; then                          # Check to see whether the correct number of arguments was entered
#  echo "The number of arguments is incorrect.">>$errlog
#  echo "The proper syntax is: bi_runpcwfl.sh <Folder Name> <Workflow Name> <workflow area> <environment>">>$errlog
#  exit_script_16
#fi

case "$3" in                                            #Find out what type of workflow we will run
  [Ff][Ii][Ll][Ee])                                     #Run a file workflow.
    run_file $1 $2 $errlog		                #call the function to run a file workflow.
    ;;                                            
  *)                                         		#Try to run the workflow.
    pmcmd startworkflow -sv $PCINTSERVNM -d $PCDOMAINNM -u $PCBIWFLUSER -p $PCBIWFLPASSWORD -f $1 -wait $2
    
    if [ $? -ne 0 ] ; then				#find out if pmcmd failed or not
      #Find_Err_Msg $1 $2				#See if we can find the err msg.  Removed for 9.2 cause of DB issues
      echo "\n$2 has failed. Please check the workflow monitor for more details.">>$errlog
      exit_script_16					#exit the script and mail the error message to the PC group
    fi
    ;;                                            
esac

echo "\nEnd bi_runpcwfl.sh: `date`">>$errlog		#End log message
exit 0                                                  #Exit this program once control has returned to main program