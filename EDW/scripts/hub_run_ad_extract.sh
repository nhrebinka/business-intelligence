#!/usr/bin/ksh
#############################################################################################################
#
#  File Name           : hub_run_ad_extract.sh
#  Purpose	           : to execute the Active Directory extract
#
#  created  by         : Eric Maxham for BI-EDW
#                      : 12/10/2012
#  Log                 :
#
#
###############################################################################################################
logger -t hub_run_ad_extract.sh -p local1.notice "hub_run_ad_extract.sh has started"
echo  "hub_run_ad_extract.sh has started"

#----------------------------------GET PARAMETERS-------------------------------
if [ $# -eq 1 ]
then
   ENV=$1
else
   echo "USAGE:  hub_run_ad_extract.sh <environment>"
   logger -t hub_run_ad_extract.sh -p local1.error "*** hub_run_ad_extract.sh has FAILED. Missing Input parameter  ***"
   exit 8
fi

#----------------------------------SET DATE-------------------------------------
set -x
tdate=`date +"%Y%m%d"`
date

echo "Set environment "

. /r41/$ENV/hub/script/hub_set_env.sh

#----------------------------------SOURCE DONE----------------------------------
echo $MNAME                                    >  $EDW_LOG/$tdate.hub_run_ad_extract.log
echo $ENV                                      >> $EDW_LOG/$tdate.hub_run_ad_extract.log
echo $tdate 				                       >> $EDW_LOG/$tdate.hub_run_ad_extract.log

echo "Execute Active Directory Extract"        >> $EDW_LOG/$tdate.hub_run_ad_extract.log
cd $EDW_SCRIPT/..
java -Dlog4j.configuration="file:resources/log4j.properties" -jar bin/ActiveDirectoryExtract.jar
return_code=$?

echo "deleting log files older than 60  days"  >> $EDW_LOG/$tdate.hub_run_ad_extract.log
find  $EDW_LOG -mtime +60 -name "ActiveDirectoryExtract*" -exec rm -rf {} \;   >> $EDW_LOG/$tdate.hub_run_ad_extract.log

echo "Active Directory Extract done"           >> $EDW_LOG/$tdate.hub_run_ad_extract.log

if [ $return_code -ne 0 ]
then
    echo "###### ABORT ##### "
    echo  "hub_run_ad_extract.sh has failed"
    logger -t hub_run_ad_extract -p local1.error "hub_run_ad_extract.sh has failed"
else
    echo  "hub_run_ad_extract.sh has ended successfully"
    logger -t hub_run_ad_extract.sh -p local1.notice "hub_run_ad_extract.sh has ended successfully "
fi
echo "exit"
