---
#IFCS Base Model
---
[Full List](table_list.md)

###Application
  + APPLICATIONS
        13 APPLICATION_
         4 APP_
         4 UI_
         1 MENU_
         3 VALIDATION_
         1 ENCODING_
         1 TRANSLATIONS_
  + USERS
         3 AUTH_
         2 USER_
         2 ACCESS_
  + (REPORTS)
        11 REPORT_
        22 RPT_
         1 EXTERNAL_

###Front-end (auth/token with merchant banks)
  + ACCOUNTS / CARDS
        10 ACCOUNT_
        28 CARD_
         5 BULK_CARD_
         2 SUSPENSE_
         2 SUSPENDED_
         1 UNALLOCATED_
         1 PLASTIC_
  + DRIVERS / VEHICLES 
         1 DRIVER_
         3 VEHICLE_
         2 EMISSION_

###Back-end (deal with fund transfer to/from Federal Banks)
  + M_CLIENTS / M_CLIENT_GROUPS
         8 CLIENT_
  + MEMBERS 
  + ALLOCATIONS

###(Clearing)
  + (API)
         8 INTERNET_
         2 INET_
         6 INTERFACE_
         1 INTERF_
         1 ENCRYPTED_

###Merchant
  + M_CUSTOMERS / M_MERCHANTS
         4 MERCHANT_
        10 CUSTOMER_
         1 CUST_
         2 MERCH_
  + M_LOCATIONS / M_LOCATION_GROUPS
         5 LOCATION_
  + TERMINALS / POS(es)
         4 POS_ (?provided by processing company)
         2 PUMP_

###Transaction
  + PRODUCTS 
         8 PRODUCT_
  + TRANSACTIONS 
        11 TRANSACTION_
         8 TRANS_
         1 LINE_ (not here?)
         1 MASTER_
         3 DETAIL_
  + BATCH(es)
         1 BATCH_

###Settlement
  + BANKS / CONTACTS
         2 BANK_
         2 CONTACT_
  + AGING(s) / AGED
         1 AGED_
         2 DUNNING_
         1 COLLECTION_
         1 BAD_
  + BILLING(s) / PAYMENT(s)
         1 BILLING_
         4 PAYMENT_
         1 CAPTURE_
  + SETTLEMENTS / DISPUTE(s) / DISPUTED
         1 STATEMENT_
         1 REMITTANCE_
         1 HOLDING_
         1 DISPUTE_
         1 DISPUTED_
         2 CREDIT_ (charge_back?)
  + FEES / REBATES / INTEREST(s) / ADJUSTMENT(s) / TAX(s)
         4 FEE_
         8 REBATE_
         1 INTEREST_
         1 ADJUSTMENT_
         2 ADJ_
         2 TAX_

###Finance
  + FREQUENCIES / PERIOD(s)
  + EXCHANGE / RATES
  + LANGUAGES / CURRENCIES 
  + INDUSTRIES 
  + GL(s)
         9 GL_
  + AUDITS (might belongs to APP)
         7 AUDIT_

###Marketing
  + MARKETING(s)
         1 MARKETING_
  + PRICING(s) / REPRICE(s)
        10 PRICING_
         3 REPRICE_
         4 COST_
         4 VELOCITY_
  + DISTRIBUTOR(s)
         1 DISTRIBUTOR_
  + MAIL(s)
         1 MAIL_
         8 LETTER_
         1 IVR_

---
#Lookups
---

###Geo
  + TIMEZONES 
  + TERRITORIES / DISTRICTS 
  + COUNTRIES / STATES / ADDRESSES 
         2 GEOGRAPHIC_
         1 ADDRESS_

###Time
  + MONTHS / CALENDAR / TIME

###Other
  + CONSTANTS 
         2 CONSTANT_
         2 COMPVALS_
  + ?MCC Code

---
#Misc. Relationships
---
      8 WORK
      7 POINT
      5 COMPONENT
      4 SYS
      3 STORED
      3 SERVER
      3 NUMBERING
      3 FAL
      2 SUNDRY
      2 MANUAL
      2 MAN
      2 IFO
      1 SETTINGS 
      1 RFCO
      1 RESTART
      1 REQUEST
      1 RELATIONSHIPS 
      1 RELATIONSHIP
      1 PRODUCED
      1 PRIVATE
      1 HIERARCHIES 
      1 GENERATED
      1 EVENT
      1 DIARY
      1 DESCRIPTIONS 
      1 DESCRIPTION
      1 DATABASECHANGELOGLOCK 
      1 DATABASECHANGELOG 
      1 DATABASE
      1 APPL
