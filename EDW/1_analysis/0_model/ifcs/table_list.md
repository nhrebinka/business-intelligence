ACCESS_GROUP_CLIENTS 
ACCESS_GROUPS 
ACCOUNT_AMOUNT_STATS 
ACCOUNT_EVENT_STATS 
ACCOUNT_FEE_ITEMS 
ACCOUNT_FEE_LOGS 
ACCOUNT_FEE_WORKING_DATA 
ACCOUNT_FEES 
ACCOUNT_STATUS 
ACCOUNT_STATUS_LOGS 
ACCOUNT_SUB_STATUS 
ACCOUNT_TOTALS 
ACCOUNTS 
ADDRESS_CLEANSING 
ADDRESSES 
ADJ_TRANS_LINE_ITEMS 
ADJ_TRANSACTIONS 
ADJUSTMENT_TYPES 
AGED_AMOUNTS 
AGING_BUCKETS 
ALLOCATIONS 
APP_TYPE_TRANS_CARD_CTRLS 
APP_TYPE_TRANS_CARD_FEE 
APP_TYPE_TRANS_CARD_PRODS 
APP_TYPE_TRANS_PROFILES 
APPL_TYPE_TRANSFERS 
APPLICATION_ADDRESSES 
APPLICATION_BANK_ACCOUNTS 
APPLICATION_CARD_PRODUCTS 
APPLICATION_CARD_PROFILES 
APPLICATION_CARDS 
APPLICATION_CONTACTS 
APPLICATION_FUEL_DETAILS 
APPLICATION_FUEL_USAGE 
APPLICATION_SIGNATORIES 
APPLICATION_SOURCES 
APPLICATION_STATUS 
APPLICATION_TYPE_REPORTS 
APPLICATION_TYPES 
APPLICATIONS 
AUDIT_CATEGORY_COLUMNS 
AUDIT_CATEGORY_TABLES 
AUDIT_CONTROL 
AUDIT_DETAILS 
AUDIT_LOG 
AUDIT_TABLES 
AUDIT_TABLES_BAK 
AUTH_ACQUIRING_INST 
AUTH_TRANS_LINE_ITEMS 
AUTH_TRANSACTION_LOGS 
BAD_DEBT_ACCOUNTS 
BANK_ACCOUNTS 
BANK_BRANCHES 
BANKS 
BATCH_TYPES 
BILLING_PLANS 
BULK_CARD_COLUMNS 
BULK_CARD_DETAILS 
BULK_CARD_ERRORS 
BULK_CARD_HEADERS 
BULK_CARD_TEMPLATES 
CALENDAR 
CAPTURE_TYPES 
CARD_ALLOCATION_ORDERS 
CARD_BRANDS 
CARD_BULK_REISSUES 
CARD_CONTACTS 
CARD_CONTROL_PROFILES 
CARD_CONTROLS 
CARD_GRAPHICS 
CARD_GRAPHICS_PRODS 
CARD_HOTLIST_LOGS 
CARD_PREFIXES 
CARD_PRODUCT_TOTALS 
CARD_PRODUCTS 
CARD_PROGRAM_CARD_TYPES 
CARD_PROGRAM_TRANS_TOTS 
CARD_PROGRAMS 
CARD_REISS_PROF_CONTROLS 
CARD_REISSUE_PROFILES 
CARD_REQUESTS 
CARD_STATUS 
CARD_STATUS_CHANGES 
CARD_STATUS_CHG_LOGS 
CARD_STATUS_LOGS 
CARD_STATUS_LOGS_BPANZ_3450 
CARD_STATUS_OLS 
CARD_STATUS_OLS_MAPPING 
CARD_TRANSFER_CARDS 
CARD_TRANSFER_REQUESTS 
CARD_VELOCITY_TOTALS 
CARDS 
CARDS_BPANZ_3450 
CARDS_BPANZ_3619 
CARDS_CR280_AU_BAK 
CARDS_CR280_NZ_BAK 
CLIENT_BANKING_DETAILS 
CLIENT_COLLECTN_SETTINGS 
CLIENT_DATA_CONTROLS 
CLIENT_DATA_DEPENDENCIES 
CLIENT_RETENTION_PERIODS 
CLIENT_TRANS_DETAIL_GRPS 
CLIENT_TRANS_FEES 
CLIENT_TRANS_TYPES 
COLLECTION_EVENTS 
COMPONENT_TYPES 
COMPONENT_VALUES 
COMPONENT_VALUES_BPANZ_3674 
COMPONENT_VALUES_GSD_AU_BAK 
COMPONENT_VALUES_GSD_NZ_BAK 
COMPVALS_WBPT3606_BAK1 
COMPVALS_WBPT3606_RB1 
CONSTANT_DESCRIPTIONS 
CONSTANT_TYPES 
CONSTANTS 
CONTACT_RANKINGS 
CONTACT_TYPES 
CONTACTS 
COST_CENTRE_CARD_TOTALS 
COST_CENTRE_DRIVER_TOTS 
COST_CENTRE_VEHICLE_TOTS 
COST_CENTRES 
COUNTRIES 
CREDIT_CARDS 
CREDIT_PLANS 
CURRENCIES 
CUST_CARD_TEMPLATE_FIELDS 
CUSTOMER_CARD_PRODUCTS 
CUSTOMER_CARD_PROFILES 
CUSTOMER_CARD_TEMPLATES 
CUSTOMER_COST_CENTRES 
CUSTOMER_PRICING_LOGS 
CUSTOMER_PRODUCT_TOT_LOCS 
CUSTOMER_PRODUCT_TOTALS 
CUSTOMER_REPORT_LEVELS 
CUSTOMER_VALUE_TYPES 
CUSTOMER_VALUES 
DATABASE_INFO 
DATABASECHANGELOG 
DATABASECHANGELOGLOCK 
DESCRIPTION_TYPES 
DESCRIPTIONS 
DETAIL_GROUP_TRANS 
DETAIL_GROUP_TYPES 
DETAIL_GROUPS 
DIARY_NOTES 
DISPUTE_REASONS 
DISPUTED_TRANSACTIONS 
DISTRIBUTOR_REFERENCES 
DISTRICTS_ZONES 
DRIVER_PRODUCT_TOTALS 
DRIVERS 
DUNNING_CODES 
DUNNING_CONTROLS 
EMISSION_TYPES 
EMISSION_VALUES 
ENCODING_PROMPTS 
ENCRYPTED_PINS 
EVENT_REQUESTS 
EXCHANGE_RATES 
EXTERNAL_CLIENT_IDS 
FAL_DEBUG_LOGS 
FAL_TABLES 
FAL_TABLES_METADATA 
FEE_CONTROLS 
FEE_EFFECTIVE_DATES 
FEE_PROFILES 
FEE_VALUES 
FEES 
FREQUENCIES 
GENERATED_PAYMENTS 
GEOGRAPHIC_AREA_TYPES 
GEOGRAPHIC_AREAS 
GL_ACCOUNT_CODES 
GL_ACCOUNT_NUMBERS 
GL_EXTR_LEDGER_DETAILS 
GL_EXTR_LEDGER_WORKING_DATA 
GL_LOC_REC_WORKING_DATA 
GL_SUMMARIES 
GL_SUMMARY_WORKING_DATA 
GL_TOTALS 
GL_TRANS_WORKING_DATA 
HIERARCHIES 
HOLDING_TRANSACTIONS 
IFO_AUDIT_COLUMNS 
IFO_AUDIT_TABLES 
INDUSTRIES 
INET_HOST_CLIENT_MAP 
INET_URL_CLIENT_MAP 
INTEREST_RATES 
INTERF_FILE_INC_LOG_ROWS 
INTERFACE_FILE_INC_LOGS 
INTERFACE_FILE_LOG_PARAMS 
INTERFACE_FILE_LOGS 
INTERFACE_FILE_PARAMS 
INTERFACE_FILES 
INTERFACE_FILES_INCOMING 
INTERNET_ACCESS_GROUPS 
INTERNET_PERMISSIONS 
INTERNET_ROLES_PERMISSIONS 
INTERNET_ROLES_PERMISSIONS_ORG 
INTERNET_USER_ACCESS 
INTERNET_USER_ACCESS_ALERTS 
INTERNET_USER_PWD_LOGS 
INTERNET_USERS 
IVR_REQUEST_LOGS 
LANGUAGES 
LETTER_ASSIGN_VARIABLES 
LETTER_ASSIGNMENTS 
LETTER_DEFINITIONS 
LETTER_EXTRACTS 
LETTER_FONTS 
LETTER_TEMPLATES 
LETTER_TEXT_LINES 
LETTER_VARIABLES 
LINE_ITEM_COMPONENTS 
LOCATION_PROD_TRANSLATION 
LOCATION_PRODUCT_TOTALS 
LOCATION_RECONCILIATIONS 
LOCATION_RESTRICT_LOCS 
LOCATION_RESTRICTIONS 
M_CLIENT_GROUPS 
M_CLIENTS 
M_CUSTOMERS 
M_LOCATION_GROUPS 
M_LOCATIONS 
M_MERCHANTS 
MAIL_INDICATORS 
MAN_TRANS_BATCH_HEADERS 
MAN_TRANS_LINE_ITEMS 
MANUAL_TRANSACTIONS 
MANUAL_VOUCHERS 
MARKETING_MESSAGES 
MASTER_PIN_KEY 
MEMBERS 
MENU_ACCESS_RESTRICTIONS 
MERCH_AGRMNT_VALUE_TYPES 
MERCH_AGRMNT_VALUES 
MERCHANT_AGREEMENTS 
MERCHANT_PAYMENT_TYPES 
MERCHANT_PRICING_LOGS 
MERCHANT_TRANS_GROUPS 
MONTHS 
NUMBERING_METHODS 
NUMBERING_SEQUENCE_RESERVES 
NUMBERING_SEQUENCES 
PAYMENT_ARRANGEMENTS 
PAYMENT_CODES 
PAYMENT_GATEWAYS 
PAYMENT_REQUESTS 
PERIOD_ACCOUNT_TOTALS 
PERIOD_BALANCES 
PLASTIC_TYPES 
POINT_CATEGORIES 
POINT_OUT_OPTIONS 
POINT_PROFILES 
POINT_PROGRAMS 
POINT_RATINGS 
POINT_TOTALS 
POINT_VALUES 
POS_INTERFACE 
POS_LOCN_RECONCILIATIONS 
POS_TRANS_LINE_ITEMS 
POS_TRANSACTIONS 
PRICING_CATEGORIES 
PRICING_CONTROLS 
PRICING_OPTION_PRODUCTS 
PRICING_OPTION_RECIPES 
PRICING_OPTIONS 
PRICING_PROFILES 
PRICING_RECIPE_RULES 
PRICING_RECIPES 
PRICING_SCHEME_OPTIONS 
PRICING_SCHEMES 
PRIVATE_QUESTIONS 
PRODUCED_CARDS 
PRODUCT_EMISSIONS 
PRODUCT_GROUP_PRODUCTS 
PRODUCT_GROUPS 
PRODUCT_RESTRICT_PRODS 
PRODUCT_RESTRICTION_ENC 
PRODUCT_RESTRICTIONS 
PRODUCT_THRESHOLDS 
PRODUCT_TRANSLATIONS 
PRODUCTS 
PRODUCTS_SOLD_LOCATIONS 
PUMP_CONTROLS 
PUMP_POS_PROMPT_ENCODING 
RATE_CODE_VALUES 
RATE_CODES 
REBATE_CATEGORIES 
REBATE_COMPONENT_TYPES 
REBATE_CONTRIBUTIONS 
REBATE_OUT_OPTIONS 
REBATE_PARENTING_SCHEMES 
REBATE_PROFILES 
REBATE_RATINGS 
REBATE_VALUES 
REBATES 
REBATES_CR434 
RELATIONSHIP_ASSIGNMENTS 
RELATIONSHIPS 
REMITTANCE_NUMBERS 
REPORT_ASSIGNMENT_PARAMS 
REPORT_ASSIGNMENTS 
REPORT_CATEGORIES 
REPORT_COVER_PAGES 
REPORT_DELIVERY_TYPES 
REPORT_FREQUENCIES 
REPORT_SORT_OPTIONS 
REPORT_TEMPLATE_PARAMS 
REPORT_TEMPLATES 
REPORT_TRANSACTION_ERRORS 
REPORT_TYPES 
REPRICE_ADJ_LINE_ITEMS 
REPRICE_ADJUSTMENTS 
REPRICE_BATCH_HEADERS 
REQUEST_TRANS_TYPES 
RESTART_PROCESS 
RFCO_REQUESTS 
RPT_EXTR_CHILDREN 
RPT_EXTR_CLIENTS 
RPT_EXTR_CUSTOMERS 
RPT_EXTR_DAILY_RECON 
RPT_EXTR_DETAIL_GROUPS 
RPT_EXTR_FINANCIALS 
RPT_EXTR_LINE_ITEMS 
RPT_EXTR_MERC_CHILDREN 
RPT_EXTR_MERC_ITEMS 
RPT_EXTR_MERC_RECONS 
RPT_EXTR_MERC_TRANS 
RPT_EXTR_MERC_TRANS_NOTES 
RPT_EXTR_MERCHANTS 
RPT_EXTR_TRANS_FEES 
RPT_EXTR_TRANS_KEYS 
RPT_EXTR_TRANS_NOTES 
RPT_EXTR_TRANS_RPT_TYPES 
RPT_EXTR_TRANSACTIONS 
RPT_EXTR_YTD_CARDS 
RPT_EXTR_YTD_CUSTOMERS 
RPT_EXTR_YTD_DRIVERS 
RPT_EXTR_YTD_VEHICLES 
SERVER_CALL_REPLAY_STEPS 
SERVER_CALL_REPLAYS 
SERVER_CALL_STATISTICS 
SETTINGS 
STATEMENT_MESSAGES 
STATES 
STORED_ALERTS 
STORED_ATTACHMENTS 
STORED_REPORTS 
SUNDRY_HOLDINGS 
SUNDRY_HOLDINGS_LOGS 
SUSPENDED_LINE_ITEMS 
SUSPENDED_TRANSACTIONS 
SUSPENSE_ITEM_AUDITS 
SUSPENSE_TRANS_AUDITS 
SYS_TOT_CARD_PGM_TRANS 
SYS_TOT_PROCESS_CONTROLS 
SYS_TOT_PROCESS_ERRORS 
SYS_TOT_TRANS_TYPES 
TAX_NUMBERS 
TAX_RATES 
TERMINALS 
TERRITORIES 
TIME_LIMITS 
TIMEZONES 
TRANS_ADJUSTMENT_ITEMS 
TRANS_ADJUSTMENT_TYPES 
TRANS_DISPUTE_TYPES 
TRANS_REBATE_BREAKDOWNS 
TRANS_REFUND_TYPES 
TRANS_REPORT_CATEGORIES 
TRANS_STEPS_ERRORS 
TRANS_TRANSLATIONS 
TRANSACTION_BATCHES 
TRANSACTION_ERRORS 
TRANSACTION_EXCEPTIONS 
TRANSACTION_FEES 
TRANSACTION_LINE_ITEMS 
TRANSACTION_NOTES 
TRANSACTION_POINTS 
TRANSACTION_REBATES 
TRANSACTION_STEPS 
TRANSACTION_TYPE_STEPS 
TRANSACTION_TYPES 
TRANSACTIONS 
TRANSLATIONS 
UI_PROD_RESTRICT_ITEMS 
UI_PROD_RESTRICT_MAP_ITEMS 
UI_PROD_RESTRICT_MAPS 
UI_PROD_RESTRICT_TITLES 
UNALLOCATED_FUNDS 
USER_PASSWORD_LOGS 
USER_RELATIONSHIPS 
USERS 
VALIDATION_CONTEXT_FLDS 
VALIDATION_CONTROLS 
VALIDATION_FIELDS 
VEHICLE_ODO_READINGS 
VEHICLE_ODOMETER_SPANS 
VEHICLE_PRODUCT_TOTALS 
VEHICLES 
VELOCITY_ASSIGNMENTS 
VELOCITY_TYPE_PRODUCTS 
VELOCITY_TYPE_VALUES 
VELOCITY_TYPES 
WALLET 
WALLET_CARDS 
WORK_FILE_BATCH_CONTROLS 
WORK_FILES 
WORK_QUEUE_ACTION_ITEMS 
WORK_QUEUE_ACTIONS 
WORK_QUEUE_GROUP_USERS 
WORK_QUEUE_ITEMS 
WORK_QUEUE_USER_GROUPS 
WORK_QUEUES 
