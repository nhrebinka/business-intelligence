create table WORK_QUEUE_ACTIONS (
  WORK_QUEUE_ACTION_OID           varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLOSE_ITEM                      varchar2(200),
  RESCHEDULE_ITEM                 varchar2(200),
  IS_LETTER_REQUIRED              varchar2(200),
  IS_COMMENT_REQUIRED             varchar2(200),
  IS_PROMISE_TO_PAY               varchar2(200),
  PERIOD_DEFAULT                  varchar2(200),
  PERIOD_MAXIMUM                  varchar2(200),
  DESCRIPTION                     varchar2(200),
  WORK_QUEUE_PERIOD_CID           varchar2(200),
  constraint WORK_QUEUE_ACTIONS_PK primary key (WORK_QUEUE_ACTION_OID) using index);

create table WORK_QUEUES (
  WORK_QUEUE_OID                  varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_QUEUE_TYPE_CID             varchar2(200),
  WORK_QUEUE_USER_GROUP_TYPE_CID  varchar2(200),
  PRIORITY                        varchar2(200),
  IS_FOR_INFORMATION_ONLY         varchar2(200),
  DESCRIPTION                     varchar2(200),
  WORK_QUEUE_USER_GROUP_OID       varchar2(200),
  constraint WORK_QUEUES_PK primary key (WORK_QUEUE_OID) using index);

create table WORK_FILE_BATCH_CONTROLS (
  WORK_FILE_BATCH_CONTROL_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_FILE_OID                   varchar2(200),
  CONTROL_DATE                    date,
  CONTROL_COUNT                   varchar2(200),
  CONTROL_TOTAL                   varchar2(200),
  constraint WORK_FILE_BATCH_CONTROLS_PK primary key (WORK_FILE_BATCH_CONTROL_OID) using index);

create table WORK_FILES (
  WORK_FILE_OID                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CREATED_AT                      varchar2(200),
  PROCESS_ID                      varchar2(200),
  FILE_NAME                       varchar2(200),
  FILE_PATH                       varchar2(200),
  ORIGINAL_FILE_NAME              varchar2(200),
  ORIGINAL_FILE_PATH              varchar2(200),
  constraint WORK_FILES_PK primary key (WORK_FILE_OID) using index);

create table WALLET_CARDS (
  WALLET_CARD_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WALLET_OID                      varchar2(200),
  CARD_NO                         varchar2(200),
  CARD_TYPE                       varchar2(200),
  EFFECTIVE_AT                    date,
  EXPIRES_AT                      date,
  IFCS_CARD_NO                    varchar2(200),
  constraint WALLET_CARDS_PK primary key (WALLET_CARD_OID) using index);

create table WALLET (
  WALLET_OID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERNET_USER_OID               varchar2(200),
  DEVICE                          varchar2(200),
  EFFECTIVE_AT                    date,
  EXPIRES_AT                      date,
  constraint WALLET_PK primary key (WALLET) using index);

create table VELOCITY_TYPE_VALUES (
  VELOCITY_TYPE_VALUE_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VELOCITY_TYPE_OID               varchar2(200),
  VALUE                           varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  TYPE_COLUMN_NAME                varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  ENCODING_PROMPT                 varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  constraint VELOCITY_TYPE_VALUES_PK primary key (VELOCITY_TYPE_VALUE_OID) using index);

create table VELOCITY_TYPE_PRODUCTS (
  VELOCITY_TYPE_PRODUCT_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VELOCITY_TYPE_OID               varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint VELOCITY_TYPE_PRODUCTS_PK primary key (VELOCITY_TYPE_PRODUCT_OID) using index);

create table VELOCITY_TYPES (
  VELOCITY_TYPE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  VELOCITY_PERIOD_TYPE_CID        varchar2(200),
  VELOCITY_SUM_TYPE_CID           varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  VELOCITY_LEVEL_CID              varchar2(200),
  constraint VELOCITY_TYPES_PK primary key (VELOCITY_TYPE_OID) using index);

create table VELOCITY_ASSIGNMENTS (
  VELOCITY_ASSIGNMENT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_ERROR_TYPE_1_AUTH            varchar2(200),
  IS_ERROR_TYPE_1_COLLECT         varchar2(200),
  IS_ERROR_TYPE_2_AUTH            varchar2(200),
  IS_ERROR_TYPE_2_COLLECT         varchar2(200),
  IS_ERROR_TYPE_3_AUTH            varchar2(200),
  IS_ERROR_TYPE_3_COLLECT         varchar2(200),
  IS_ERROR_TYPE_4_AUTH            varchar2(200),
  IS_ERROR_TYPE_4_COLLECT         varchar2(200),
  IS_ERROR_TYPE_5_AUTH            varchar2(200),
  IS_ERROR_TYPE_5_COLLECT         varchar2(200),
  IS_ERROR_TYPE_6_AUTH            varchar2(200),
  IS_ERROR_TYPE_6_COLLECT         varchar2(200),
  IS_ERROR_TYPE_7_AUTH            varchar2(200),
  IS_ERROR_TYPE_7_COLLECT         varchar2(200),
  IS_ERROR_TYPE_8_AUTH            varchar2(200),
  IS_ERROR_TYPE_8_COLLECT         varchar2(200),
  VELOCITY_TYPE_VALUE_1_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_2_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_3_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_4_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_5_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_6_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_7_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_8_OID       varchar2(200),
  constraint VELOCITY_ASSIGNMENTS_PK primary key (VELOCITY_ASSIGNMENT_OID) using index);

create table VEHICLE_PRODUCT_TOTALS (
  VEHICLE_PRODUCT_TOTAL_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VEHICLE_OID                     varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  constraint VEHICLE_PRODUCT_TOTALS_PK primary key (VEHICLE_PRODUCT_TOTAL_OID) using index);

create table VEHICLE_ODO_READINGS (
  VEHICLE_ODO_READING_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VEHICLE_OID                     varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  PROCESSED_AT                    varchar2(200),
  IS_READING_DUBIOUS              varchar2(200),
  HAS_BEEN_ANALYSED               varchar2(200),
  ODOMETER                        varchar2(200),
  DISTANCE_DRIVEN                 varchar2(200),
  constraint VEHICLE_ODO_READINGS_PK primary key (VEHICLE_ODO_READING_OID) using index);

create table VEHICLE_ODOMETER_SPANS (
  VEHICLE_ODOMETER_SPAN_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VEHICLE_OID                     varchar2(200),
  MTD_DISTANCE_DRIVEN             varchar2(200),
  YTD_DISTANCE_DRIVEN             varchar2(200),
  LTD_DISTANCE_DRIVEN             varchar2(200),
  LMTH_DISTANCE_DRIVEN            varchar2(200),
  constraint VEHICLE_ODOMETER_SPANS_PK primary key (VEHICLE_ODOMETER_SPAN_OID) using index);

create table VEHICLES (
  VEHICLE_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  VEHICLE_STATUS_CID              varchar2(200),
  IS_PRINT_OPTION                 varchar2(200),
  VEHICLE_TYPE_DID                varchar2(200),
  VEHICLE_CLASS_DID               varchar2(200),
  VEHICLE_MAKE_DID                varchar2(200),
  CARD_OID                        varchar2(200),
  YEAR                            varchar2(200),
  VEHICLE_ID                      varchar2(200),
  LICENSE_PLATE                   varchar2(200),
  MODEL                           varchar2(200),
  CURRENT_ODO_READING             varchar2(200),
  CURRENT_ODO_EFFECTIVE_AT        varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint VEHICLES_PK primary key (VEHICLE_OID) using index);

create table VALIDATION_FIELDS (
  VALIDATION_FIELD_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SYSTEM_MODULE_CID               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  DESCRIPTION                     varchar2(200),
  COLUMN_NAME                     varchar2(200),
  TABLE_NAME                      varchar2(200),
  constraint VALIDATION_FIELDS_PK primary key (VALIDATION_FIELD_OID) using index);

create table VALIDATION_CONTROLS (
  VALIDATION_CONTROL_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SYSTEM_MODULE_CID               varchar2(200),
  VALIDATION_FIELD_OID            varchar2(200),
  COLUMN_CONTROL_CID              varchar2(200),
  MINIMUM_VALUE                   varchar2(200),
  MAXIMUM_VALUE                   varchar2(200),
  VALIDATION_CONTEXT_FLD_OID      varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  CONTEXT_VALUE_OID               varchar2(200),
  DEFAULT_VALUE                   varchar2(200),
  LABEL_OVERRIDE                  varchar2(200),
  IS_USED_BY_DESKTOP              varchar2(200),
  IS_USED_BY_ONLINE               varchar2(200),
  NOT_NEW_MODE_ONLY               varchar2(200),
  INTERNET_ACCESS_GROUP_OID       varchar2(200),
  MASK                            varchar2(200),
  MASK_ERROR_MESSAGE              varchar2(200),
  TOOLTIP_TEXT                    varchar2(200),
  HELP_TEXT                       varchar2(200),
  CASE_ACTION_CID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  APPLICABLE_MODE_CID             varchar2(200),
  constraint VALIDATION_CONTROLS_PK primary key (VALIDATION_CONTROL_OID) using index);

create table VALIDATION_CONTEXT_FLDS (
  VALIDATION_CONTEXT_FLD_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SYSTEM_MODULE_CID               varchar2(200),
  DESCRIPTION                     varchar2(200),
  COLUMN_NAME                     varchar2(200),
  TABLE_NAME                      varchar2(200),
  constraint VALIDATION_CONTEXT_FLDS_PK primary key (VALIDATION_CONTEXT_FLD_OID) using index);

create table USER_RELATIONSHIPS (
  USER_RELATIONSHIP_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  USER_OID                        varchar2(200),
  PARENT_USER_OID                 varchar2(200),
  constraint USER_RELATIONSHIPS_PK primary key (USER_RELATIONSHIP_OID) using index);

create table USER_PASSWORD_LOGS (
  USER_PASSWORD_LOG_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  USER_OID                        varchar2(200),
  PASSWORD                        varchar2(200),
  constraint USER_PASSWORD_LOGS_PK primary key (USER_PASSWORD_LOG_OID) using index);

create table USERS (
  USER_OID                        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOGON_STATUS_CID                varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  LOGON_ID                        varchar2(200),
  IS_A_SUPERVISOR                 varchar2(200),
  IS_A_WORK_QUEUE_USER            varchar2(200),
  HAS_SUBORDINATES                varchar2(200),
  HAS_INTERNET_ACCESS             varchar2(200),
  FAILED_LOGON_COUNT              varchar2(200),
  LAST_LOGGEDON_AT                varchar2(200),
  NAME                            varchar2(200),
  PASSWORD_CREATED_AT             varchar2(200),
  PASSWORD                        varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  IS_A_ONLINE_SUPERUSER           varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint USERS_PK primary key (USER_OID) using index);

create table UNALLOCATED_FUNDS (
  UNALLOCATED_FUND_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  AMOUNT                          varchar2(200),
  UNALLOCATED_AMOUNT              varchar2(200),
  SUPPLIED_REMITTANCE_ID          varchar2(200),
  constraint UNALLOCATED_FUNDS_PK primary key (UNALLOCATED_FUND_OID) using index);

create table UI_PROD_RESTRICT_TITLES (
  UI_PROD_RESTRICT_TITLE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  constraint UI_PROD_RESTRICT_TITLES_PK primary key (UI_PROD_RESTRICT_TITLE_OID) using index);

create table UI_PROD_RESTRICT_MAP_ITEMS (
  UI_PROD_RESTRICT_MAP_ITEM_OID   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UI_PROD_RESTRICT_MAP_OID        varchar2(200),
  UI_PROD_RESTRICT_ITEM_OID       varchar2(200),
  constraint UI_PROD_RESTRICT_MAP_ITEMS_PK primary key (UI_PROD_RESTRICT_MAP_ITEM_OID) using index);

create table UI_PROD_RESTRICT_MAPS (
  UI_PROD_RESTRICT_MAP_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint UI_PROD_RESTRICT_MAPS_PK primary key (UI_PROD_RESTRICT_MAP_OID) using index);

create table UI_PROD_RESTRICT_ITEMS (
  UI_PROD_RESTRICT_ITEM_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UI_PROD_RESTRICT_TITLE_OID      varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  FILE_NAME                       varchar2(200),
  constraint UI_PROD_RESTRICT_ITEMS_PK primary key (UI_PROD_RESTRICT_ITEM_OID) using index);

create table TRANS_TRANSLATIONS (
  TRANS_TRANSLATION_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  IS_REVERSAL                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  BANKING_ACCOUNT_MAP_OID         varchar2(200),
  constraint TRANS_TRANSLATIONS_PK primary key (TRANS_TRANSLATION_OID) using index);

create table TRANS_STEPS_ERRORS (
  TRANS_STEPS_ERROR_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_STEP_OID            varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  constraint TRANS_STEPS_ERRORS_PK primary key (TRANS_STEPS_ERROR_OID) using index);

create table TRANS_REPORT_CATEGORIES (
  TRANS_REPORT_CATEGORY_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_CATEGORY_OID             varchar2(200),
  CLIENT_TRANS_TYPE_OID           varchar2(200),
  constraint TRANS_REPORT_CATEGORIES_PK primary key (TRANS_REPORT_CATEGORIE_OID) using index);

create table TRANS_REFUND_TYPES (
  TRANS_REFUND_TYPE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  REFUND_TRANSACTION_TYPE_OID     varchar2(200),
  constraint TRANS_REFUND_TYPES_PK primary key (TRANS_REFUND_TYPE_OID) using index);

create table TRANS_REBATE_BREAKDOWNS (
  TRANS_REBATE_BREAKDOWN_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_REBATE_OID          varchar2(200),
  REBATE_AMOUNT                   varchar2(200),
  INCLUDE_ALL_TOTAL_AMOUNT        varchar2(200),
  EXCLUDE_PUMP_PRICE_AMOUNT       varchar2(200),
  PRODUCT_OID                     varchar2(200),
  LOCATION_MID                    varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  TAX_RATE                        varchar2(200),
  INCLUDE_ALL_TOTAL_VOLUME        varchar2(200),
  constraint TRANS_REBATE_BREAKDOWNS_PK primary key (TRANS_REBATE_BREAKDOWN_OID) using index);

create table TRANS_DISPUTE_TYPES (
  TRANS_DISPUTE_TYPE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  DISPUTE_TRANSACTION_TYPE_OID    varchar2(200),
  REDEBIT_TRANSACTION_TYPE_OID    varchar2(200),
  WRITEOFF_TRANSACTION_TYPE_OID   varchar2(200),
  CHARGEBCK_TRANSACTION_TYPE_OID  varchar2(200),
  constraint TRANS_DISPUTE_TYPES_PK primary key (TRANS_DISPUTE_TYPE_OID) using index);

create table WORK_QUEUE_USER_GROUPS (
  WORK_QUEUE_USER_GROUP_OID       varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_QUEUE_USER_GROUP_TYPE_CID  varchar2(200),
  DESCRIPTION                     varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  TERRITORY_OID                   varchar2(200),
  CREDIT_PLAN_OID                 varchar2(200),
  BILLING_PLAN_OID                varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  constraint WORK_QUEUE_USER_GROUPS_PK primary key (WORK_QUEUE_USER_GROUP_OID) using index);

create table WORK_QUEUE_ITEMS (
  WORK_QUEUE_ITEM_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  WORK_QUEUE_ITEM_STATUS_CID      varchar2(200),
  CREATED_AT                      varchar2(200),
  ASSIGNED_AT                     varchar2(200),
  BRING_UP_AT                     varchar2(200),
  ASSIGNED_USER_OID               varchar2(200),
  COLLECTION_EVENT_OID            varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  DISPUTED_TRANSACTION_OID        varchar2(200),
  APPLICATION_OID                 varchar2(200),
  DIARY_NOTE_OID                  varchar2(200),
  constraint WORK_QUEUE_ITEMS_PK primary key (WORK_QUEUE_ITEM_OID) using index);

create table WORK_QUEUE_GROUP_USERS (
  WORK_QUEUE_GROUP_USER_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_QUEUE_USER_GROUP_OID       varchar2(200),
  USER_OID                        varchar2(200),
  constraint WORK_QUEUE_GROUP_USERS_PK primary key (WORK_QUEUE_GROUP_USER_OID) using index);

create table WORK_QUEUE_ACTION_ITEMS (
  WORK_QUEUE_ACTION_ITEM_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WORK_QUEUE_ITEM_OID             varchar2(200),
  WORK_QUEUE_ITEM_STATUS_CID      varchar2(200),
  WORK_QUEUE_ACTION_OID           varchar2(200),
  WORKED_USER_OID                 varchar2(200),
  WORKED_AT                       varchar2(200),
  constraint WORK_QUEUE_ACTION_ITEMS_PK primary key (WORK_QUEUE_ACTION_ITEM_OID) using index);

create table TRANS_ADJUSTMENT_TYPES (
  TRANS_ADJUSTMENT_TYPE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  REVORIG_TRANSACTION_TYPE_OID    varchar2(200),
  POSTADJ_TRANSACTION_TYPE_OID    varchar2(200),
  constraint TRANS_ADJUSTMENT_TYPES_PK primary key (TRANS_ADJUSTMENT_TYPE_OID) using index);

create table TRANS_ADJUSTMENT_ITEMS (
  TRANS_ADJUSTMENT_ITEM_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  ADJUSTMENT_TYPE_OID             varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  PRODUCT_OID                     varchar2(200),
  CHEQUE_NO                       varchar2(200),
  APPROVED_BY                     varchar2(200),
  SUPPLIED_REMITTANCE_ID          varchar2(200),
  constraint TRANS_ADJUSTMENT_ITEMS_PK primary key (TRANS_ADJUSTMENT_ITEM_OID) using index);

create table TRANSLATIONS (
  TRANSLATION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  TABLE_OID                       varchar2(200),
  TABLE_NAME                      varchar2(200),
  COLUMN_NAME                     varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint TRANSLATIONS_PK primary key (TRANSLATION_OID) using index);

create table TRANSACTION_TYPE_STEPS (
  TRANSACTION_TYPE_STEP_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_STEP_OID            varchar2(200),
  TRANSACTION_STEP_ACTION_CID     varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  constraint TRANSACTION_TYPE_STEPS_PK primary key (TRANSACTION_TYPE_STEP_OID) using index);

create table TRANSACTION_TYPES (
  TRANSACTION_TYPE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_CATEGORY_CID        varchar2(200),
  TRANSACTION_CLASS_CID           varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  TRANSACTION_TYPE                varchar2(200),
  IS_DEBIT                        varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  ACCOUNT_TOTAL_TYPE_CID          varchar2(200),
  IS_DISPLAYED                    varchar2(200),
  constraint TRANSACTION_TYPES_PK primary key (TRANSACTION_TYPE_OID) using index);

create table TRANSACTION_STEPS (
  TRANSACTION_STEP_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_STEP_CATEGORY_CID   varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  CLASS_REFERENCE                 varchar2(200),
  BUSINESS_DELEGATE               varchar2(200),
  MUST_SUSPEND                    varchar2(200),
  constraint TRANSACTION_STEPS_PK primary key (TRANSACTION_STEP_OID) using index);

create table TRANSACTION_REBATES (
  TRANSACTION_REBATE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  REBATE_OID                      varchar2(200),
  REBATE_AMOUNT                   varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  IS_EXCLUDE_PUMP_PRICE_TRANS     varchar2(200),
  IS_CALCULATED_ON_VOLUME         varchar2(200),
  INCLUDE_ALL_TOTAL_AMOUNT        varchar2(200),
  EXCLUDE_PUMP_PRICE_AMOUNT       varchar2(200),
  ORIG_CUST_MID                   varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  constraint TRANSACTION_REBATES_PK primary key (TRANSACTION_REBATE_OID) using index);

create table TRANSACTION_POINTS (
  TRANSACTION_POINT_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  POINT_PROGRAM_OID               varchar2(200),
  AMOUNT                          varchar2(200),
  POINTS_EXPIRE_ON                date,
  constraint TRANSACTION_POINTS_PK primary key (TRANSACTION_POINT_OID) using index);

create table TRANSACTION_NOTES (
  TRANSACTION_NOTE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  RECORDED_AT                     varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  SHORT_NOTE                      varchar2(200),
  constraint TRANSACTION_NOTES_PK primary key (TRANSACTION_NOTE_OID) using index);

create table TRANSACTION_LINE_ITEMS (
  TRANSACTION_OID                 varchar2(200),
  LINE_NUMBER                     varchar2(200),
  PRODUCT_OID                     varchar2(200),
  QUANTITY                        varchar2(200),
  ORIGINAL_UNIT_PRICE             varchar2(200),
  CUSTOMER_UNIT_PRICE             varchar2(200),
  MERCHANT_UNIT_PRICE             varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  CUSTOMER_VALUE                  varchar2(200),
  MERCHANT_VALUE                  varchar2(200),
  TAX_RATE                        varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  CUSTOMER_REBATE_TOTAL           varchar2(200),
  MERCHANT_FEE_TOTAL              varchar2(200),
  MERCHANT_FEE_TOTAL_TAX          varchar2(200),
  REBATE_CONTRIBUTION_TOTAL       varchar2(200),
  REBATE_CONTRIBUTION_TOTAL_TAX   varchar2(200),
  CUSTOMER_PRICING_OPTION_OID     varchar2(200),
  CUSTOMER_PRICING_RECIPE_OID     varchar2(200),
  MERCHANT_PRICING_OPTION_OID     varchar2(200),
  MERCHANT_PRICING_RECIPE_OID     varchar2(200),
  MERCHANT_PRICE_TYPE_CID         varchar2(200),
  MERCHANT_HANDLING_FEE_TOTAL     varchar2(200),
  MERCHANT_HANDLING_FEE_TOT_TAX   varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL      varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL_TAX  varchar2(200),
  MERCHANT_REIMBUSE_ADJUST        varchar2(200),
  MERCHANT_SERVICE_FEE_ADJUST     varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  MERCHANT_REIMBUSE_ADJUST_TAX    varchar2(200),
  MERCHANT_SERVICE_FEE_ADJ_TAX    varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  PRODUCT_TRANSLATION_OID         varchar2(200),
  DISCOUNT_AMOUNT                 varchar2(200),
  DISCOUNT_PERCENTAGE             varchar2(200),
  TRANS_CURR_CUST_UNIT_PRICE      varchar2(200),
  TRANS_CURR_CUST_VALUE           varchar2(200),
  PAYMENT_CURRENCY_OID            varchar2(200),
  PAYMENT_CURRENCY_VALUE          varchar2(200),
  constraint TRANSACTION_LINE_ITEMS_PK primary key (TRANSACTION_LINE_ITEM_OID) using index);

create table TRANSACTION_FEES (
  TRANSACTION_FEE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  FEE_OID                         varchar2(200),
  FEE_AMOUNT                      varchar2(200),
  FEE_TAX                         varchar2(200),
  constraint TRANSACTION_FEES_PK primary key (TRANSACTION_FEE_OID) using index);

create table TRANSACTION_EXCEPTIONS (
  TRANSACTION_EXCEPTION_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  PROCESSED_AT                    varchar2(200),
  IS_ERROR_TYPE_COLLECT           varchar2(200),
  LOCATION_CODE                   varchar2(200),
  CARD_NO                         varchar2(200),
  REFERENCE                       varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  constraint TRANSACTION_EXCEPTIONS_PK primary key (TRANSACTION_EXCEPTION_OID) using index);

create table TRANSACTION_ERRORS (
  TRANSACTION_ERROR_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANS_ERROR_CODE                varchar2(200),
  TRANS_ERROR_CATEGORY_CID        varchar2(200),
  IS_TO_BE_SUSPENDED              varchar2(200),
  IS_TO_BE_RECORDED               varchar2(200),
  IS_TO_BE_ALERTED                varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXCEPTION_CODE                  varchar2(200),
  RESPONSE_CODE                   varchar2(200),
  IS_FORCIBLE                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  IS_TRANS_AUTH_DECLINE           varchar2(200),
  IS_TRANS_AUTH_EXCEPTION         varchar2(200),
  constraint TRANSACTION_ERRORS_PK primary key (TRANSACTION_ERROR_OID) using index);

create table TRANSACTION_BATCHES (
  TRANSACTION_BATCH_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONTROL_COUNT                   varchar2(200),
  CONTROL_TOTAL                   varchar2(200),
  PROCESSED_ON                    date,
  DATE_TIME_FILE_CREATED          varchar2(200),
  SOURCE_TYPE                     varchar2(200),
  COMPANY                         varchar2(200),
  constraint TRANSACTION_BATCHES_PK primary key (TRANSACTION_BATCHE_OID) using index);

create table TRANSACTIONS (
  TRANSACTION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_CATEGORY_CID        varchar2(200),
  TRANSACTION_PROCESS_MODE_CID    varchar2(200),
  ORIGINAL_CURRENCY_OID           varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  PROCESSED_AT                    varchar2(200),
  POSTED_AT                       varchar2(200),
  POSTED_BY                       varchar2(200),
  EXCHANGE_RATE                   varchar2(200),
  CUSTOMER_AMOUNT                 varchar2(200),
  MERCHANT_AMOUNT                 varchar2(200),
  FEE_TOTAL_TAX                   varchar2(200),
  FEE_TOTAL_AMOUNT                varchar2(200),
  CUSTOMER_REBATE_TOTAL           varchar2(200),
  MERCHANT_REBATE_TOTAL           varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  CUSTOMER_TOTAL_POINTS           varchar2(200),
  CUSTOMER_POINT_AMOUNT           varchar2(200),
  REFERENCE                       varchar2(200),
  POINTS_EXPIRE_ON                date,
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  LOCATION_MID                    varchar2(200),
  CAPTURE_TYPE_OID                varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  LOCATION_RECONCILIATION_OID     varchar2(200),
  CARD_OID                        varchar2(200),
  DRIVER_OID                      varchar2(200),
  VEHICLE_OID                     varchar2(200),
  MERCHANT_MID                    varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  PRODUCT_OID                     varchar2(200),
  CARD_NO                         varchar2(200),
  GL_EXTRACT_STATUS_CID           varchar2(200),
  PRODUCT_TRANSLATION_OID         varchar2(200),
  AUTH_ACCOUNT_OID                varchar2(200),
  PAYMENT_CURRENCY_OID            varchar2(200),
  PAYMENT_CURRENCY_VALUE          varchar2(200),
  constraint TRANSACTIONS_PK primary key (TRANSACTION_OID) using index);

create table TIME_LIMITS (
  TIME_LIMIT_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  TIME_LIMIT_CODE                 varchar2(200),
  MONDAY_START_AT                 varchar2(200),
  MONDAY_END_AT                   varchar2(200),
  TUESDAY_START_AT                varchar2(200),
  TUESDAY_END_AT                  varchar2(200),
  WEDNESDAY_START_AT              varchar2(200),
  WEDNESDAY_END_AT                varchar2(200),
  THURSDAY_START_AT               varchar2(200),
  THURSDAY_END_AT                 varchar2(200),
  FRIDAY_START_AT                 varchar2(200),
  FRIDAY_END_AT                   varchar2(200),
  SATURDAY_START_AT               varchar2(200),
  SATURDAY_END_AT                 varchar2(200),
  SUNDAY_START_AT                 varchar2(200),
  SUNDAY_END_AT                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint TIME_LIMITS_PK primary key (TIME_LIMIT_OID) using index);

create table TIMEZONES (
  TIMEZONE_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint TIMEZONES_PK primary key (TIMEZONE_OID) using index);

create table TERRITORIES (
  TERRITORY_OID                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TERRITORY_TYPE_CID              varchar2(200),
  DESCRIPTION                     varchar2(200),
  DEPARTMENT                      varchar2(200),
  GL_REGION_DID                   varchar2(200),
  USER_OID                        varchar2(200),
  CONTACT_DETAILS                 varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  constraint TERRITORIES_PK primary key (TERRITORIE_OID) using index);

create table TERMINALS (
  TERMINAL_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  TERMINAL_ID                     varchar2(200),
  TERMINAL_TYPE_CID               varchar2(200),
  TERMINAL_STATUS_CID             varchar2(200),
  DESCRIPTION                     varchar2(200),
  SERIAL_NO                       varchar2(200),
  constraint TERMINALS_PK primary key (TERMINAL_OID) using index);

create table TAX_RATES (
  TAX_RATE_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_OID                     varchar2(200),
  COUNTRY_OID                     varchar2(200),
  RATE_CODE_OID                   varchar2(200),
  constraint TAX_RATES_PK primary key (TAX_RATE_OID) using index);

create table TAX_NUMBERS (
  TAX_NUMBER_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  COUNTRY_OID                     varchar2(200),
  TAX_NO                          varchar2(200),
  TAX_TYPE_CID                    varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  TAX_NO_REG_TYPE_CID             varchar2(200),
  constraint TAX_NUMBERS_PK primary key (TAX_NUMBER_OID) using index);

create table SYS_TOT_TRANS_TYPES (
  SYS_TOT_TRANS_TYPE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROCESSED_ON                    date,
  CLIENT_MID                      varchar2(200),
  TOTAL_TYPE_CID                  varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  DEBIT_TOTAL                     varchar2(200),
  CREDIT_TOTAL                    varchar2(200),
  constraint SYS_TOT_TRANS_TYPES_PK primary key (SYS_TOT_TRANS_TYPE_OID) using index);

create table SYS_TOT_PROCESS_ERRORS (
  SYS_TOT_PROCESS_ERROR_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SYS_TOT_PROCESS_CONTROL_OID     varchar2(200),
  ERROR_CODE                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  MERCHANT_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  constraint SYS_TOT_PROCESS_ERRORS_PK primary key (SYS_TOT_PROCESS_ERROR_OID) using index);

create table SYS_TOT_PROCESS_CONTROLS (
  SYS_TOT_PROCESS_CONTROL_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROCESSED_AT                    varchar2(200),
  STARTED_AT                      varchar2(200),
  ENDED_AT                        varchar2(200),
  PROCESS_ID                      varchar2(200),
  STREAM                          varchar2(200),
  ROWS_READ                       varchar2(200),
  ROWS_ADDED                      varchar2(200),
  ROWS_UPDATED                    varchar2(200),
  ROWS_DELETED                    varchar2(200),
  TRANSACTIONS_POSTED             varchar2(200),
  CUSTOMER_DR_AMOUNT              varchar2(200),
  CUSTOMER_CR_AMOUNT              varchar2(200),
  MERCHANT_DR_AMOUNT              varchar2(200),
  MERCHANT_CR_AMOUNT              varchar2(200),
  CLIENT_MID                      varchar2(200),
  ROWS_IN_ERROR                   varchar2(200),
  constraint SYS_TOT_PROCESS_CONTROLS_PK primary key (SYS_TOT_PROCESS_CONTROL_OID) using index);

create table SYS_TOT_CARD_PGM_TRANS (
  SYS_TOT_CARD_PGM_TRAN_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROCESSED_ON                    date,
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  DEBIT_AMOUNT                    varchar2(200),
  CREDIT_AMOUNT                   varchar2(200),
  constraint SYS_TOT_CARD_PGM_TRANS_PK primary key (SYS_TOT_CARD_PGM_TRAN_OID) using index);

create table SUSPENSE_TRANS_AUDITS (
  SUSPENSE_TRANS_AUDIT_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  SUSPENSE_STATUS_CID             varchar2(200),
  UPDATED_AT                      varchar2(200),
  UPDATED_BY                      varchar2(200),
  CARD_NO                         varchar2(200),
  DATE_TIME_EFFECTIVE             varchar2(200),
  PROCESSED_AT                    varchar2(200),
  REFERENCE                       varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  ODOMETER                        varchar2(200),
  TRANSACTION_CODE                varchar2(200),
  LOCATION_CODE                   varchar2(200),
  TERMINAL_ID                     varchar2(200),
  BATCH_NO                        varchar2(200),
  BATCH_SOURCE                    varchar2(200),
  VEHICLE_ID                      varchar2(200),
  DRIVER_ID                       varchar2(200),
  AUTHORISATION_NO                varchar2(200),
  ATTENTION_KEY                   varchar2(200),
  POS_ENTRY_1                     varchar2(200),
  POS_ENTRY_2                     varchar2(200),
  POS_ENTRY_3                     varchar2(200),
  POS_ENTRY_4                     varchar2(200),
  POS_ENTRY_5                     varchar2(200),
  constraint SUSPENSE_TRANS_AUDITS_PK primary key (SUSPENSE_TRANS_AUDIT_OID) using index);

create table SUSPENSE_ITEM_AUDITS (
  SUSPENSE_ITEM_AUDIT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SUSPENSE_TRANS_AUDIT_OID        varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  LINE_NUMBER                     varchar2(200),
  WAS_CORRECTED                   varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  QUANTITY                        varchar2(200),
  UNIT_PRICE                      varchar2(200),
  VALUE                           varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  DISCOUNT_AMOUNT                 varchar2(200),
  DISCOUNT_PERCENTAGE             varchar2(200),
  constraint SUSPENSE_ITEM_AUDITS_PK primary key (SUSPENSE_ITEM_AUDIT_OID) using index);

create table SUSPENDED_TRANSACTIONS (
  POS_TRANSACTION_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INITIAL_TRANS_ERROR_OID         varchar2(200),
  SUSPENSE_STATUS_CID             varchar2(200),
  DATE_TIME_EFFECTIVE             varchar2(200),
  PROCESSED_AT                    varchar2(200),
  WAS_CORRECTED                   varchar2(200),
  LOCATION_MID                    varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  RFCO_REQUESTED_COUNT            varchar2(200),
  RFCO_REQUESTED_AT               varchar2(200),
  NOTES                           varchar2(200),
  constraint SUSPENDED_TRANSACTIONS_PK primary key (SUSPENDED_TRANSACTION_OID) using index);

create table SUSPENDED_LINE_ITEMS (
  POS_TRANSACTION_OID             varchar2(200),
  LINE_NUMBER                     varchar2(200),
  WAS_CORRECTED                   varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  constraint SUSPENDED_LINE_ITEMS_PK primary key (SUSPENDED_LINE_ITEM_OID) using index);

create table SUNDRY_HOLDINGS_LOGS (
  SUNDRY_HOLDINGS_LOG_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SUNDRY_STATUS_CID               varchar2(200),
  EFFECTIVE_AT                    date,
  SUNDRY_HOLDING_OID              varchar2(200),
  constraint SUNDRY_HOLDINGS_LOGS_PK primary key (SUNDRY_HOLDINGS_LOG_OID) using index);

create table SUNDRY_HOLDINGS (
  SUNDRY_HOLDING_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SUNDRY_TYPE_CID                 varchar2(200),
  SUNDRY_STATUS_CID               varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  AMOUNT                          varchar2(200),
  TAX_TOTAL                       varchar2(200),
  PROCESSED_BY                    varchar2(200),
  REFERENCE                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  PROCESSED_ON                    date,
  EFFECTIVE_ON                    date,
  MERCHANT_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_OID                        varchar2(200),
  PRODUCT_OID                     varchar2(200),
  APPROVED_BY                     varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  SHORT_NOTE                      varchar2(200),
  ADJUSTMENT_TYPE_OID             varchar2(200),
  REQUEST_TRANS_TYPE_OID          varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  TAX_RATE                        varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  PAYMENT_ID                      varchar2(200),
  CLIENT_MID                      varchar2(200),
  CREDIT_NOTE_INVOICED_ON         date,
  CREDIT_NOTE_PAYMENT_REQU_OID    varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  REMITTANCE_ADVICE               varchar2(200),
  constraint SUNDRY_HOLDINGS_PK primary key (SUNDRY_HOLDING_OID) using index);

create table SUBSCRIPTIONS (
  SUBSCRIPTION_OID                varchar2(200),
  APPLICATION_OID                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  FEE_OID                         varchar2(200),
  DESCRIPTION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint SUBSCRIPTIONS_PK primary key (SUBSCRIPTION_OID) using index);

create table STORED_REPORTS (
  STORED_REPORT_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  FILE_FORMAT_CID                 varchar2(200),
  CREATED_ON                      date,
  FILE_NAME                       varchar2(200),
  RPT_EXTR_CUSTOMER_OID           varchar2(200),
  RPT_EXTR_MERCHANT_OID           varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  ORIENTATION_CID                 varchar2(200),
  IS_DUPLEX                       varchar2(200),
  DELIVERY_TYPE_CID               varchar2(200),
  REPORT_STATUS_CID               varchar2(200),
  NUM_OF_PAGES                    varchar2(200),
  SEND_TO_ADDRESS_OID             varchar2(200),
  SEND_TO_CONTACT_NAME            varchar2(200),
  META_DATA                       varchar2(200),
  IS_ATTACHMENT_COMPRESSED        varchar2(200),
  SEND_TO_EMAIL_ADDRESS           varchar2(200),
  REPORT_ASSIGNMENT_OID           varchar2(200),
  COUNTRY_OID                     varchar2(200),
  UUID                            varchar2(200),
  constraint STORED_REPORTS_PK primary key (STORED_REPORT_OID) using index);

create table STORED_ATTACHMENTS (
  STORED_ATTACHMENT_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ATTACHMENT_TYPE_DID             varchar2(200),
  CREATED_ON                      date,
  FILE_NAME                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  constraint STORED_ATTACHMENTS_PK primary key (STORED_ATTACHMENT_OID) using index);

create table STORED_ALERTS (
  STORED_ALERT_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  CREATE_AT                       varchar2(200),
  DELIVERY_TYPE_CID               varchar2(200),
  DELIVERY_ADDRESS                varchar2(200),
  MESSAGE                         varchar2(200),
  TRANSACTION_EXCEPTION_OID       varchar2(200),
  DELIVERY_ADDRESS_DISPLAY_NAME   varchar2(200),
  constraint STORED_ALERTS_PK primary key (STORED_ALERT_OID) using index);

create table STATES (
  STATE_OID                       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COUNTRY_OID                     varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint STATES_PK primary key (STATE_OID) using index);

create table STATEMENT_MESSAGES (
  STATEMENT_MESSAGE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  MESSAGE                         varchar2(200),
  PHONE_NUMBER                    varchar2(200),
  constraint STATEMENT_MESSAGES_PK primary key (STATEMENT_MESSAGE_OID) using index);

create table SETTINGS (
  SETTING_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SETTING_CATEGORY_CID            varchar2(200),
  SETTING_ID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  SETTING_VALUE                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  DATA_TYPE_CID                   varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint SETTINGS_PK primary key (SETTING_OID) using index);

create table SERVER_CALL_STATISTICS (
  SERVER_CALL_STATISTICS_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPLAY_ID                       varchar2(200),
  SESSION_NAME                    varchar2(200),
  STEP_NAME                       varchar2(200),
  STEP_SEQUENCE                   varchar2(200),
  ERROR_INDICATOR                 varchar2(200),
  ELAPSED_NANOSECONDS             varchar2(200),
  SERVER_CALL_REPLAYS_OID         varchar2(200),
  THREAD_NUMBER                   varchar2(200),
  ERROR_MESSAGE                   varchar2(200),
  METHOD_NAME                     varchar2(200),
  constraint SERVER_CALL_STATISTICS_PK primary key (SERVER_CALL_STATISTIC_OID) using index);

create table SERVER_CALL_REPLAY_STEPS (
  SERVER_CALL_REPLAY_STEPS_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SERVER_CALL_REPLAYS_OID         varchar2(200),
  SESSION_NAME                    varchar2(200),
  NUMBER_OF_THREADS               varchar2(200),
  constraint SERVER_CALL_REPLAY_STEPS_PK primary key (SERVER_CALL_REPLAY_STEP_OID) using index);

create table SERVER_CALL_REPLAYS (
  SERVER_CALL_REPLAYS_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPLAY_NAME                     varchar2(200),
  IS_MULTITHREADED                varchar2(200),
  constraint SERVER_CALL_REPLAYS_PK primary key (SERVER_CALL_REPLAY_OID) using index);

create table RPT_EXTR_YTD_VEHICLES (
  RPT_EXTR_YTD_VEHICLE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  VEHICLE_OID                     varchar2(200),
  QUANTITY                        varchar2(200),
  VALUE                           varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  TAX_VALUE                       varchar2(200),
  REBATE_VALUE                    varchar2(200),
  DISTANCE_DRIVEN                 varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  TOTAL_CATEGORY_DESCRIPTION      varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint RPT_EXTR_YTD_VEHICLES_PK primary key (RPT_EXTR_YTD_VEHICLE_OID) using index);

create table RPT_EXTR_YTD_DRIVERS (
  RPT_EXTR_YTD_DRIVER_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  DRIVER_OID                      varchar2(200),
  QUANTITY                        varchar2(200),
  VALUE                           varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  TAX_VALUE                       varchar2(200),
  REBATE_VALUE                    varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  TOTAL_CATEGORY_DESCRIPTION      varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint RPT_EXTR_YTD_DRIVERS_PK primary key (RPT_EXTR_YTD_DRIVER_OID) using index);

create table RPT_EXTR_YTD_CUSTOMERS (
  RPT_EXTR_YTD_CUSTOMER_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  QUANTITY                        varchar2(200),
  VALUE                           varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  TAX_VALUE                       varchar2(200),
  REBATE_VALUE                    varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  TOTAL_CATEGORY_DESCRIPTION      varchar2(200),
  CLIENT_MID                      varchar2(200),
  FEE_AMOUNT                      varchar2(200),
  FEE_TAX                         varchar2(200),
  constraint RPT_EXTR_YTD_CUSTOMERS_PK primary key (RPT_EXTR_YTD_CUSTOMER_OID) using index);

create table RPT_EXTR_YTD_CARDS (
  RPT_EXTR_YTD_CARD_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  QUANTITY                        varchar2(200),
  VALUE                           varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  TAX_VALUE                       varchar2(200),
  REBATE_VALUE                    varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  TOTAL_CATEGORY_DESCRIPTION      varchar2(200),
  CLIENT_MID                      varchar2(200),
  FEE_AMOUNT                      varchar2(200),
  FEE_TAX                         varchar2(200),
  constraint RPT_EXTR_YTD_CARDS_PK primary key (RPT_EXTR_YTD_CARD_OID) using index);

create table RPT_EXTR_TRANS_RPT_TYPES (
  RPT_EXTR_TRANS_RPT_TYPE_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  constraint RPT_EXTR_TRANS_RPT_TYPES_PK primary key (RPT_EXTR_TRANS_RPT_TYPE_OID) using index);

create table RPT_EXTR_TRANS_NOTES (
  RPT_EXTR_TRANS_NOTE_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_TRANSACTION_OID        varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  SHORT_NOTE                      varchar2(200),
  constraint RPT_EXTR_TRANS_NOTES_PK primary key (RPT_EXTR_TRANS_NOTE_OID) using index);

create table RPT_EXTR_TRANS_KEYS (
  RPT_EXTR_TRANS_KEY_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_TRANSACTION_OID        varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  CARD_OID                        varchar2(200),
  DRIVER_OID                      varchar2(200),
  VEHICLE_OID                     varchar2(200),
  COST_CENTRE_OID                 varchar2(200),
  constraint RPT_EXTR_TRANS_KEYS_PK primary key (RPT_EXTR_TRANS_KEY_OID) using index);

create table RPT_EXTR_TRANS_FEES (
  RPT_EXTR_TRANS_FEE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_TRANSACTION_OID        varchar2(200),
  FEE_AMOUNT                      varchar2(200),
  FEE_TAX                         varchar2(200),
  FEE_DESCRIPTION                 varchar2(200),
  constraint RPT_EXTR_TRANS_FEES_PK primary key (RPT_EXTR_TRANS_FEE_OID) using index);

create table RPT_EXTR_TRANSACTIONS (
  RPT_EXTR_TRANSACTION_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  TRANSACTION_CATEGORY_CID        varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  PROCESSED_AT                    varchar2(200),
  IS_UNDER_DISPUTE                varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  EXCHANGE_RATE                   varchar2(200),
  DISTANCE_DRIVEN                 varchar2(200),
  DISTANCE_PER_QUANTITY           varchar2(200),
  AMOUNT_PER_DISTANCE             varchar2(200),
  CUSTOMER_AMOUNT                 varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  FEE_TOTAL_TAX                   varchar2(200),
  FEE_TOTAL_AMOUNT                varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  REFERENCE                       varchar2(200),
  DETAIL_GROUP_OID                varchar2(200),
  ODOMETER                        varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  REMITTANCE_ID                   varchar2(200),
  CARD_NO                         varchar2(200),
  CARD_CUSTOMER_COST_CENTRE_CODE  varchar2(200),
  CARD_COST_CENTRE_DESCRIPTION    varchar2(200),
  CARD_COST_CENTRE_SHORT_DESCR    varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  DRIVER_ID                       varchar2(200),
  DRIVER_NAME                     varchar2(200),
  DRIVER_SHORT_NAME               varchar2(200),
  VEHICLE_ID                      varchar2(200),
  LICENSE_PLATE                   varchar2(200),
  LOCATION_NAME                   varchar2(200),
  LOCATION_ADDRESS_LINE           varchar2(200),
  LOCATION_STATE_SHORT            varchar2(200),
  LOCATION_COUNTRY_DESCRIPTION    varchar2(200),
  POS_ENTRY_1                     varchar2(200),
  POS_ENTRY_2                     varchar2(200),
  POS_ENTRY_3                     varchar2(200),
  POS_ENTRY_4                     varchar2(200),
  POS_ENTRY_5                     varchar2(200),
  LOCATION_SUBURB                 varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  LOCATION_NO                     varchar2(200),
  VEHICLE_MAKE                    varchar2(200),
  TRANSACTION_TYPE_DESC           varchar2(200),
  ADMIN_USER_NAME                 varchar2(200),
  ADMIN_CONTACT_DETAILS           varchar2(200),
  CARD_EXTERNAL_REF               varchar2(200),
  BATCH_NO                        varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  ATTENTION_KEY                   varchar2(200),
  BATCH_SOURCE                    varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  VEHICLE_DESCRIPTION             varchar2(200),
  POS_VEHICLE_ID                  varchar2(200),
  POS_DRIVER_ID                   varchar2(200),
  CUSTOMER_COST_CENTRE_OID        varchar2(200),
  AUTH_ACCOUNT_OID                varchar2(200),
  constraint RPT_EXTR_TRANSACTIONS_PK primary key (RPT_EXTR_TRANSACTION_OID) using index);

create table RPT_EXTR_MERC_TRANS_NOTES (
  RPT_EXTR_MERC_TRANS_NOTE_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_MERC_TRAN_OID          varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  SHORT_NOTE                      varchar2(200),
  constraint RPT_EXTR_MERC_TRANS_NOTES_PK primary key (RPT_EXTR_MERC_TRANS_NOTE_OID) using index);

create table RPT_EXTR_MERC_TRANS (
  RPT_EXTR_MERC_TRAN_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_MERC_RECON_OID         varchar2(200),
  TRANSACTION_CATEGORY_CID        varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  PROCESSED_AT                    varchar2(200),
  EXCHANGE_RATE                   varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  MERCHANT_AMOUNT                 varchar2(200),
  MERCHANT_REBATE_TOTAL           varchar2(200),
  REFERENCE                       varchar2(200),
  ODOMETER                        varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  CARD_NO                         varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  LOCATION_STATE_SHORT            varchar2(200),
  LOCATION_NAME                   varchar2(200),
  LOCATION_COUNTRY_DESCRIPTION    varchar2(200),
  LOCATION_ADDRESS_LINE           varchar2(200),
  LOCATION_SUBURB                 varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  BATCH_NO                        varchar2(200),
  constraint RPT_EXTR_MERC_TRANS_PK primary key (RPT_EXTR_MERC_TRAN_OID) using index);

create table RPT_EXTR_MERC_RECONS (
  RPT_EXTR_MERC_RECON_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MERCHANT_MID                    varchar2(200),
  LOCATION_RECONCILIATION_OID     varchar2(200),
  PROCESSED_ON                    date,
  TRANSACTION_COUNT_SETTLED       varchar2(200),
  TRANSACTION_COUNT_RECEIVED      varchar2(200),
  QUANTITY_SETTLED                varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  LOCATION_NAME                   varchar2(200),
  TRANSACTION_GROUP_DESCRIPTION   varchar2(200),
  CARD_ISO_PREFIX                 varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CAPTURE_TYPE_DESCRIPTION        varchar2(200),
  LOCATION_EXTERNAL_CODE          varchar2(200),
  TERMINAL_ID                     varchar2(200),
  constraint RPT_EXTR_MERC_RECONS_PK primary key (RPT_EXTR_MERC_RECON_OID) using index);

create table RPT_EXTR_MERC_ITEMS (
  RPT_EXTR_MERC_ITEM_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_MERC_TRAN_OID          varchar2(200),
  QUANTITY                        varchar2(200),
  ORIGINAL_UNIT_PRICE             varchar2(200),
  MERCHANT_UNIT_PRICE             varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  MERCHANT_VALUE                  varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  MERCHANT_FEE_TOTAL              varchar2(200),
  REBATE_CONTRIBUTION_TOTAL       varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  PRICING_RECIPE_SHORT_DESCR      varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRICING_RECIPE_DESCRIPTION      varchar2(200),
  MERCHANT_HANDLING_FEE_TOTAL     varchar2(200),
  MERCHANT_HANDLING_FEE_TOT_TAX   varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL      varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL_TAX  varchar2(200),
  MERCHANT_REIMBUSE_ADJUST        varchar2(200),
  MERCHANT_SERVICE_FEE_ADJUST     varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  MERCHANT_REIMBUSE_ADJUST_TAX    varchar2(200),
  MERCHANT_SERVICE_FEE_ADJ_TAX    varchar2(200),
  LINE_NUMBER                     varchar2(200),
  IS_FUEL                         varchar2(200),
  constraint RPT_EXTR_MERC_ITEMS_PK primary key (RPT_EXTR_MERC_ITEM_OID) using index);

create table RPT_EXTR_MERC_CHILDREN (
  RPT_EXTR_MERC_CHILD_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_MERCHANT_OID           varchar2(200),
  MERCHANT_MID                    varchar2(200),
  MERCHANT_NO                     varchar2(200),
  NAME                            varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  LANGUAGE                        varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  POSTAL_CODE                     varchar2(200),
  TRADING_NAME                    varchar2(200),
  TAX_NO                          varchar2(200),
  SUBURB                          varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  constraint RPT_EXTR_MERC_CHILDREN_PK primary key (RPT_EXTR_MERC_CHILDREN) using index);

create table RPT_EXTR_MERCHANTS (
  RPT_EXTR_MERCHANT_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  MERCHANT_MID                    varchar2(200),
  RELATIONSHIP_OID                varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  RUN_ID                          varchar2(200),
  MERCHANT_NO                     varchar2(200),
  NAME                            varchar2(200),
  IS_PARENTED                     varchar2(200),
  CREATED_ON                      date,
  REPORT_RANK                     varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  LANGUAGE                        varchar2(200),
  DELIVERY_TYPE                   varchar2(200),
  COUNTRY_DESCRIPTION             varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  LAST_REPORTED_ON                date,
  PREVIOUS_REPORTED_ON            date,
  STATE_SHORT_DESCRIPTION         varchar2(200),
  POSTAL_CODE                     varchar2(200),
  TAX_NO                          varchar2(200),
  CUST_SERVICE_PULL_INITS         varchar2(200),
  PULL_CODE                       varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  CONTACT_NAME                    varchar2(200),
  TRADING_NAME                    varchar2(200),
  STATE_DESCRIPTION               varchar2(200),
  SUBURB                          varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  ADMIN_USER_NAME                 varchar2(200),
  ADMIN_CONTACT_DETAILS           varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  REPORT_ASSIGNMENT_OID           varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PROCESSING_STATUS_CID           varchar2(200),
  POSTING_PROCESS_ID              varchar2(200),
  constraint RPT_EXTR_MERCHANTS_PK primary key (RPT_EXTR_MERCHANT_OID) using index);

create table RPT_EXTR_LINE_ITEMS (
  RPT_EXTR_LINE_ITEM_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_TRANSACTION_OID        varchar2(200),
  QUANTITY                        varchar2(200),
  ORIGINAL_UNIT_PRICE             varchar2(200),
  CUSTOMER_UNIT_PRICE             varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  CUSTOMER_VALUE                  varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  CUSTOMER_REBATE_TOTAL           varchar2(200),
  PRODUCT_DESCRIPTION             varchar2(200),
  PRODUCT_SHORT_DESCRIPTION       varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  MERCHANT_REIMBUSE_ADJUST_TAX    varchar2(200),
  MERCHANT_SERVICE_FEE_ADJ_TAX    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  IS_TAX_CREDITED                 varchar2(200),
  PRODUCT_EXTERNAL_CODE           varchar2(200),
  CUSTOMER_PRICING_RECIPE_DESC    varchar2(200),
  CUSTOMER_PRICED_AT_PUMP         varchar2(200),
  LINE_NUMBER                     varchar2(200),
  IS_FUEL                         varchar2(200),
  constraint RPT_EXTR_LINE_ITEMS_PK primary key (RPT_EXTR_LINE_ITEM_OID) using index);

create table RPT_EXTR_FINANCIALS (
  RPT_EXTR_FINANCIAL_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_CUSTOMER_OID           varchar2(200),
  OPENING_BALANCE                 varchar2(200),
  OVERDUE_PERIOD_1                varchar2(200),
  OVERDUE_PERIOD_2                varchar2(200),
  OVERDUE_PERIOD_3                varchar2(200),
  OVERDUE_PERIOD_4                varchar2(200),
  OVERDUE_PERIOD_5                varchar2(200),
  OVERDUE_PERIOD_6                varchar2(200),
  OVERDUE_AMOUNT                  varchar2(200),
  MINIMUM_DUE                     varchar2(200),
  CREDIT_LIMIT                    varchar2(200),
  CLOSING_BALANCE                 varchar2(200),
  LAST_REPORTED_ON                date,
  DUE_ON                          date,
  LAST_BILLED_ON                  date,
  CREDIT_TERMS                    varchar2(200),
  constraint RPT_EXTR_FINANCIALS_PK primary key (RPT_EXTR_FINANCIAL_OID) using index);

create table RPT_EXTR_DETAIL_GROUPS (
  RPT_EXTR_DETAIL_GROUP_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  DETAIL_GROUP_OID                varchar2(200),
  PERIOD_BALANCE_TYPE_CID         varchar2(200),
  COUNTRY_OID                     varchar2(200),
  OVERDUE_DAYS                    varchar2(200),
  MINIMUM_DUE                     varchar2(200),
  DUE_ON                          date,
  LAST_REPORTED_ON                date,
  AGE_DESCRIPTION                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  REMITTANCE_ID                   varchar2(200),
  HOST_DEBIT_VOLUME               varchar2(200),
  HOST_CREDIT_VOLUME              varchar2(200),
  BILLED_ON                       date,
  constraint RPT_EXTR_DETAIL_GROUPS_PK primary key (RPT_EXTR_DETAIL_GROUP_OID) using index);

create table RPT_EXTR_DAILY_RECON (
  RPT_EXTR_DAILY_RECON_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PROCESSING_DATE                 date,
  A_CUSTOMER_TOTAL                varchar2(200),
  B_CARD_LEDGER_TOTAL             varchar2(200),
  E_RECEIVABLES                   varchar2(200),
  F1_MERCHANT_DEBIT               varchar2(200),
  F2_MERCHANT_CREDIT              varchar2(200),
  G_MERCH_TOTAL                   varchar2(200),
  P1_MTD_GL_TOT_DEBIT             varchar2(200),
  P2_MTD_GL_TOT_CREDIT            varchar2(200),
  P3_MTD_TRANS_FEES               varchar2(200),
  T1_DEBIT_BALANCE                varchar2(200),
  T2_CREDIT_BALANCE               varchar2(200),
  Z1_MTD_MERCH_TOT_DEBIT          varchar2(200),
  Z2_MTD_MERCH_TOT_CREDIT         varchar2(200),
  T3_CARD_BALANCE                 varchar2(200),
  constraint RPT_EXTR_DAILY_RECON_PK primary key (RPT_EXTR_DAILY_RECON) using index);

create table RPT_EXTR_CUSTOMERS (
  RPT_EXTR_CUSTOMER_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  RUN_ID                          varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  NAME                            varchar2(200),
  IS_PARENTED                     varchar2(200),
  DO_REBATES_EXIST                varchar2(200),
  DO_NONFINANCIALS_EXIST          varchar2(200),
  IS_A_VAT_INVOICE                varchar2(200),
  CREATED_ON                      date,
  REPORT_RANK                     varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  LANGUAGE                        varchar2(200),
  DELIVERY_TYPE                   varchar2(200),
  COUNTRY_DESCRIPTION             varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  CONTACT_NAME                    varchar2(200),
  LAST_REPORTED_ON                date,
  PREVIOUS_REPORTED_ON            date,
  POSTAL_CODE                     varchar2(200),
  TRADING_NAME                    varchar2(200),
  PULL_CODE                       varchar2(200),
  STATE_SHORT_DESCRIPTION         varchar2(200),
  STATE_DESCRIPTION               varchar2(200),
  CUST_SERVICE_PULL_INITS         varchar2(200),
  TAX_NO                          varchar2(200),
  SUBURB                          varchar2(200),
  ADMIN_USER_NAME                 varchar2(200),
  ADMIN_CONTACT_DETAILS           varchar2(200),
  EXT_ACCOUNT_REF                 varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  REPORT_ASSIGNMENT_OID           varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PROCESSING_STATUS_CID           varchar2(200),
  POSTING_PROCESS_ID              varchar2(200),
  NUM_ACTIVE_CARDS                varchar2(200),
  constraint RPT_EXTR_CUSTOMERS_PK primary key (RPT_EXTR_CUSTOMER_OID) using index);

create table RPT_EXTR_CLIENTS (
  RPT_EXTR_CLIENT_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  NAME                            varchar2(200),
  SHORT_NAME                      varchar2(200),
  REMITTANCE_ADDRESS_LINE         varchar2(200),
  STREET_ADDRESS_LINE             varchar2(200),
  POSTAL_CODE                     varchar2(200),
  STATE_SHORT_DESCRIPTION         varchar2(200),
  STATE_DESCRIPTION               varchar2(200),
  TAX_NO                          varchar2(200),
  IMAGE_FILE_NAME                 varchar2(200),
  REMITTANCE_ADDR_SUBURB          varchar2(200),
  STREET_ADDR_SUBURB              varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  TAX_RATE                        varchar2(200),
  constraint RPT_EXTR_CLIENTS_PK primary key (RPT_EXTR_CLIENT_OID) using index);

create table RPT_EXTR_CHILDREN (
  RPT_EXTR_CHILD_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RPT_EXTR_CUSTOMER_OID           varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  NAME                            varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  LANGUAGE                        varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  CONTACT_NAME                    varchar2(200),
  POSTAL_CODE                     varchar2(200),
  TRADING_NAME                    varchar2(200),
  TAX_NO                          varchar2(200),
  SUBURB                          varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  EXT_ACCOUNT_REF                 varchar2(200),
  constraint RPT_EXTR_CHILDREN_PK primary key (RPT_EXTR_CHILDREN) using index);

create table RFCO_REQUESTS (
  RFCO_REQUEST_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  CREATED_ON                      date,
  CREATED_BY                      varchar2(200),
  DISPUTED_TRANSACTION_OID        varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  constraint RFCO_REQUESTS_PK primary key (RFCO_REQUEST_OID) using index);

create table RESTART_PROCESS (
  RESTART_PROCESS_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  STREAM                          varchar2(200),
  PHASE                           varchar2(200),
  IS_IN_RESTART_MODE              varchar2(200),
  LAST_PROCESSED_ON               date,
  PROCESS_ID                      varchar2(200),
  LAST_OID_PROCESSED              varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint RESTART_PROCESS_PK primary key (RESTART_PROCES_OID) using index);

create table REQUEST_TRANS_TYPES (
  REQUEST_TRANS_TYPE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  TRANS_TYPE_ASSIGNMENT_CID       varchar2(200),
  DEBIT_TRANSACTION_TYPE_OID      varchar2(200),
  CREDIT_TRANSACTION_TYPE_OID     varchar2(200),
  DEBIT_GL_ACCOUNT_CODE_OID       varchar2(200),
  CREDIT_GL_ACCOUNT_CODE_OID      varchar2(200),
  POST_TO_ACCOUNT                 varchar2(200),
  PREVIOUS_RUNDATE                date,
  LAST_RUNDATE                    date,
  NO_OF_MONTHS                    varchar2(200),
  constraint REQUEST_TRANS_TYPES_PK primary key (REQUEST_TRANS_TYPE_OID) using index);

create table REPRICE_BATCH_HEADERS (
  REPRICE_BATCH_HEADER_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUST_CLIENT_MID                 varchar2(200),
  LOCN_CLIENT_MID                 varchar2(200),
  CREATED_AT                      varchar2(200),
  REPRICING_STATUS_CID            varchar2(200),
  EFFECTIVE_FROM                  date,
  EFFECTIVE_TO                    date,
  CUSTOMER_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  CUSTOMER_NOTES                  varchar2(200),
  MERCHANT_NOTES                  varchar2(200),
  CUSTOMER_PRICING_PROFILE_OID    varchar2(200),
  CUSTOMER_PRICING_OPTION_OID     varchar2(200),
  CUSTOMER_PRICING_RECIPE_OID     varchar2(200),
  CUSTOMER_COMPONENT_TYPE_OID     varchar2(200),
  CUSTOMER_REBATE_PROFILE_OID     varchar2(200),
  MERCHANT_PRICING_PROFILE_OID    varchar2(200),
  MERCHANT_PRICING_OPTION_OID     varchar2(200),
  MERCHANT_PRICING_RECIPE_OID     varchar2(200),
  MERCHANT_COMPONENT_TYPE_OID     varchar2(200),
  CUSTOMER_VALUE_1_OID            varchar2(200),
  CUSTOMER_VALUE_2_OID            varchar2(200),
  CUSTOMER_VALUE_3_OID            varchar2(200),
  MERCHANT_AGRMNT_VALUE_1_OID     varchar2(200),
  MERCHANT_AGRMNT_VALUE_2_OID     varchar2(200),
  MERCHANT_AGRMNT_VALUE_3_OID     varchar2(200),
  GEOGRAPHIC_AREA_1_OID           varchar2(200),
  GEOGRAPHIC_AREA_2_OID           varchar2(200),
  GEOGRAPHIC_AREA_3_OID           varchar2(200),
  GEOGRAPHIC_AREA_4_OID           varchar2(200),
  GEOGRAPHIC_AREA_5_OID           varchar2(200),
  constraint REPRICE_BATCH_HEADERS_PK primary key (REPRICE_BATCH_HEADER_OID) using index);

create table REPRICE_ADJ_LINE_ITEMS (
  REPRICE_ADJUSTMENT_OID          varchar2(200),
  LINE_NUMBER                     varchar2(200),
  CUSTOMER_VALUE                  varchar2(200),
  MERCHANT_VALUE                  varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  CUSTOMER_REBATE_TOTAL           varchar2(200),
  MERCHANT_FEE_TOTAL              varchar2(200),
  MERCHANT_FEE_TOTAL_TAX          varchar2(200),
  REBATE_CONTRIBUTION_TOTAL       varchar2(200),
  REBATE_CONTRIBUTION_TOTAL_TAX   varchar2(200),
  CUSTOMER_PRICING_OPTION_OID     varchar2(200),
  CUSTOMER_PRICING_RECIPE_OID     varchar2(200),
  MERCHANT_PRICING_OPTION_OID     varchar2(200),
  MERCHANT_PRICING_RECIPE_OID     varchar2(200),
  MERCHANT_HANDLING_FEE_TOTAL     varchar2(200),
  MERCHANT_HANDLING_FEE_TOT_TAX   varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL      varchar2(200),
  MERCHANT_SERVICE_FEE_TOTAL_TAX  varchar2(200),
  MERCHANT_REIMBUSE_ADJUST        varchar2(200),
  MERCHANT_SERVICE_FEE_ADJUST     varchar2(200),
  constraint REPRICE_ADJ_LINE_ITEMS_PK primary key (REPRICE_ADJ_LINE_ITEM_OID) using index);

create table REPRICE_ADJUSTMENTS (
  REPRICE_ADJUSTMENT_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPRICE_BATCH_HEADER_OID        varchar2(200),
  REPRICING_STATUS_CID            varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  ORIG_TRANSACTION_OID            varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  IS_ADJUSTMENT_APPLIED           varchar2(200),
  CUSTOMER_AMOUNT                 varchar2(200),
  MERCHANT_AMOUNT                 varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  CUSTOMER_TOTAL_POINTS           varchar2(200),
  CUSTOMER_POINT_AMOUNT           varchar2(200),
  CUSTOMER_TERRITORY_OID          varchar2(200),
  MERCHANT_TERRITORY_OID          varchar2(200),
  constraint REPRICE_ADJUSTMENTS_PK primary key (REPRICE_ADJUSTMENT_OID) using index);

create table REPORT_TYPES (
  REPORT_TYPE_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  RANK                            varchar2(200),
  IS_PULL_CODE_USED               varchar2(200),
  IS_REMITTANCE_CALCULATED        varchar2(200),
  DESCRIPTION                     varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  DELIVERY_TYPE_CID               varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  CONTACT_HIERARCHY_CID           varchar2(200),
  DETAIL_GROUP_TYPE_OID           varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  REPORT_CATEGORY_OID             varchar2(200),
  SECONDARY_DELIVERY_TYPE_CID     varchar2(200),
  REPORT_FORMAT                   varchar2(200),
  IS_REPORT_LEVELS_REPORT         varchar2(200),
  IS_LEVEL1_REPORT                varchar2(200),
  IS_LEVEL2_REPORT                varchar2(200),
  IS_LEVEL3_REPORT                varchar2(200),
  ORIENTATION_CID                 varchar2(200),
  IS_DUPLEX                       varchar2(200),
  OUTPUT_FORMAT_CID               varchar2(200),
  IS_SORT_OPTION_REQUIRED         varchar2(200),
  REPORT_SORT_OPTION_OID          varchar2(200),
  IS_CARD_DETAILS_REQUIRED        varchar2(200),
  CARD_DETAILS_1_CID              varchar2(200),
  CARD_DETAILS_2_CID              varchar2(200),
  SUBREPORT_TYPE_OID              varchar2(200),
  DETAIL                          varchar2(200),
  REPORT_GROUP_DID                varchar2(200),
  constraint REPORT_TYPES_PK primary key (REPORT_TYPE_OID) using index);

create table REPORT_TRANSACTION_ERRORS (
  REPORT_TRANSACTION_ERROR_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  constraint REPORT_TRANSACTION_ERRORS_PK primary key (REPORT_TRANSACTION_ERROR_OID) using index);

create table REPORT_TEMPLATE_PARAMS (
  REPORT_TEMPLATE_PARAM_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  DATA_TYPE_CID                   varchar2(200),
  LABEL                           varchar2(200),
  DEFAULT_VALUE                   varchar2(200),
  TABLE_NAME                      varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  FILTER_CLAUSE                   varchar2(200),
  PARAM_CONFIG                    varchar2(200),
  constraint REPORT_TEMPLATE_PARAMS_PK primary key (REPORT_TEMPLATE_PARAM_OID) using index);

create table REPORT_TEMPLATES (
  REPORT_TEMPLATE_OID             varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_MEMBER_TYPE_CID          varchar2(200),
  IS_REPORT_EXTRACTED             varchar2(200),
  IS_ADHOC                        varchar2(200),
  REPORT_TEMPLATE_CODE            varchar2(200),
  DESCRIPTION                     varchar2(200),
  FILE_NAME                       varchar2(200),
  IS_ONLINE_SCHEDULED             varchar2(200),
  IS_COVER_PAGE_REPORT            varchar2(200),
  IS_SUB_REPORT                   varchar2(200),
  IS_EXTERNALLY_CREATED           varchar2(200),
  IS_WATERMARKED                  varchar2(200),
  constraint REPORT_TEMPLATES_PK primary key (REPORT_TEMPLATE_OID) using index);

create table REPORT_SORT_OPTIONS (
  REPORT_SORT_OPTION_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  SORT_OPTION_CID                 varchar2(200),
  constraint REPORT_SORT_OPTIONS_PK primary key (REPORT_SORT_OPTION_OID) using index);

create table REPORT_FREQUENCIES (
  REPORT_FREQUENCY_OID            varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  constraint REPORT_FREQUENCIES_PK primary key (REPORT_FREQUENCIE_OID) using index);

create table REPORT_DELIVERY_TYPES (
  REPORT_DELIVERY_TYPE_OID        varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  DELIVERY_TYPE_CID               varchar2(200),
  COVER_REPORT_TEMPLATE_OID       varchar2(200),
  FEE_OID                         varchar2(200),
  constraint REPORT_DELIVERY_TYPES_PK primary key (REPORT_DELIVERY_TYPE_OID) using index);

create table REPORT_COVER_PAGES (
  REPORT_COVER_PAGE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COVER_REPORT_TEMPLATE_OID       varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  constraint REPORT_COVER_PAGES_PK primary key (REPORT_COVER_PAGE_OID) using index);

create table REPORT_CATEGORIES (
  REPORT_CATEGORY_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  IS_A_VAT_INVOICE                varchar2(200),
  IS_A_FINANCIAL_DOCUMENT         varchar2(200),
  IS_A_CUSTOMER_STATEMENT         varchar2(200),
  ACCOUNTING_TYPE_CID             varchar2(200),
  IS_A_MERCHANT_STATEMENT         varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint REPORT_CATEGORIES_PK primary key (REPORT_CATEGORIE_OID) using index);

create table REPORT_ASSIGNMENT_PARAMS (
  REPORT_ASSIGNMENT_PARAM_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REPORT_ASSIGNMENT_OID           varchar2(200),
  VALUE                           varchar2(200),
  LABEL                           varchar2(200),
  REPORT_TEMPLATE_PARAM_OID       varchar2(200),
  constraint REPORT_ASSIGNMENT_PARAMS_PK primary key (REPORT_ASSIGNMENT_PARAM_OID) using index);

create table REPORT_ASSIGNMENTS (
  REPORT_ASSIGNMENT_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  DELIVERY_TYPE_CID               varchar2(200),
  CREATED_ON                      date,
  IS_PULL_CODE_USED               varchar2(200),
  LAST_REPORTED_ON                date,
  PREVIOUS_REPORTED_ON            date,
  CONTACT_HIERARCHY_CID           varchar2(200),
  RELATIONSHIP_OID                varchar2(200),
  IS_ADHOC_REPORT                 varchar2(200),
  IS_ENABLED                      varchar2(200),
  IS_INTERNET_USER                varchar2(200),
  DESCRIPTION                     varchar2(200),
  USER_OID                        varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  REPORT_SORT_OPTION_OID          varchar2(200),
  CARD_DETAILS_1_CID              varchar2(200),
  CARD_DETAILS_2_CID              varchar2(200),
  IS_CARD_DETAILS_REQUIRED        varchar2(200),
  IS_ATTACHMENT_COMPRESSED        varchar2(200),
  SUBDELIVERY_TYPE_CID            varchar2(200),
  IS_SUB_ATTACHMENT_COMPRESSED    varchar2(200),
  ADHOC_REP_BATCH_REQ_STATUS_CID  varchar2(200),
  FEE_OID                         varchar2(200),
  constraint REPORT_ASSIGNMENTS_PK primary key (REPORT_ASSIGNMENT_OID) using index);

create table REMITTANCE_NUMBERS (
  REMITTANCE_NUMBER_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DETAIL_GROUP_TYPE_OID           varchar2(200),
  LAST_NUMBER_USED                varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint REMITTANCE_NUMBERS_PK primary key (REMITTANCE_NUMBER_OID) using index);

create table RELATIONSHIP_ASSIGNMENTS (
  RELATIONSHIP_ASSIGNMENT_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RELATIONSHIP_OID                varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  IS_BALANCE_ENTITY               varchar2(200),
  IS_BILLING_ENTITY               varchar2(200),
  IS_AUTHORISATION_ENTITY         varchar2(200),
  constraint RELATIONSHIP_ASSIGNMENTS_PK primary key (RELATIONSHIP_ASSIGNMENT_OID) using index);

create table RELATIONSHIPS (
  RELATIONSHIP_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  HIERARCHY_OID                   varchar2(200),
  MEMBER_OID                      varchar2(200),
  PARENT_RELATIONSHIP_OID         varchar2(200),
  constraint RELATIONSHIPS_PK primary key (RELATIONSHIP_OID) using index);

create table REBATE_VALUES (
  REBATE_VALUE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_OID                      varchar2(200),
  VALUE_RANGE_FROM                varchar2(200),
  VALUE_RANGE_TO                  varchar2(200),
  RATE                            varchar2(200),
  constraint REBATE_VALUES_PK primary key (REBATE_VALUE_OID) using index);

create table REBATE_RATINGS (
  REBATE_RATING_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RATING                          varchar2(200),
  PRIORITY                        varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint REBATE_RATINGS_PK primary key (REBATE_RATING_OID) using index);

create table REBATE_PROFILES (
  REBATE_PROFILE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  CLIENT_MID                      varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  constraint REBATE_PROFILES_PK primary key (REBATE_PROFILE_OID) using index);

create table REBATE_OUT_OPTIONS (
  REBATE_OUT_OPTION_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_PROFILE_OID              varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  constraint REBATE_OUT_OPTIONS_PK primary key (REBATE_OUT_OPTION_OID) using index);

create table REBATE_CONTRIBUTIONS (
  REBATE_CONTRIBUTION_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  CONTRIBUTION_PERCENTAGE         varchar2(200),
  MEMBER_OID                      varchar2(200),
  constraint REBATE_CONTRIBUTIONS_PK primary key (REBATE_CONTRIBUTION_OID) using index);

create table REBATE_COMPONENT_TYPES (
  REBATE_COMPONENT_TYPE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_CATEGORY_OID             varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  constraint REBATE_COMPONENT_TYPES_PK primary key (REBATE_COMPONENT_TYPE_OID) using index);

create table REBATE_CATEGORIES (
  REBATE_CATEGORY_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_TYPE_CID                 varchar2(200),
  REBATE_CALCULATION_TYPE_CID     varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  IS_CREDITED_TO_A_3RD_PARTY      varchar2(200),
  IS_CALCULATED_ON_VOLUME         varchar2(200),
  IS_BASED_ON_ANNUAL              varchar2(200),
  DESCRIPTION                     varchar2(200),
  CREDIT_TRANS_TYPE_OID           varchar2(200),
  MINIMUM_CHARGE                  varchar2(200),
  MAXIMUM_CHARGE                  varchar2(200),
  IS_EXCLUDE_PUMP_PRICE_TRANS     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  DEBIT_TRANS_TYPE_OID            varchar2(200),
  REBATE_PARENTING_SCHEME_CID     varchar2(200),
  PRODUCT_OID                     varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  REBATE_PERIOD_RANGE_CID         varchar2(200),
  VALIDATE_REBATES_IN_HIERARCHY   varchar2(200),
  constraint REBATE_CATEGORIES_PK primary key (REBATE_CATEGORIE_OID) using index);

create table REBATES (
  REBATE_OID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REBATE_CATEGORY_OID             varchar2(200),
  REBATE_PROFILE_OID              varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  PRODUCT_OID                     varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  MERCH_AGRMNT_VALUE_OID          varchar2(200),
  constraint REBATES_PK primary key (REBATE_OID) using index);

create table RATE_CODE_VALUES (
  RATE_CODE_VALUE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RATE_CODE_OID                   varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  RATE                            varchar2(200),
  FIXED_RATE                      varchar2(200),
  constraint RATE_CODE_VALUES_PK primary key (RATE_CODE_VALUE_OID) using index);

create table RATE_CODES (
  RATE_CODE_OID                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RATE_TYPE_CID                   varchar2(200),
  RATE_CODE                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  constraint RATE_CODES_PK primary key (RATE_CODE_OID) using index);

create table PUMP_POS_PROMPT_ENCODING (
  PUMP_POS_PROMPT_ENCODING_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  ENCODING_CODE                   varchar2(200),
  PUMP_CONTROL_OID                varchar2(200),
  IS_VEHICLE_ID_REQ               varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_DRIVER_ID_REQ                varchar2(200),
  IS_ORDER_NUMBER_REQ             varchar2(200),
  IS_FLEET_ID_REQ                 varchar2(200),
  IS_CUSTOMER_SELECTION_REQ       varchar2(200),
  IS_ODOMETER_REQ                 varchar2(200),
  constraint PUMP_POS_PROMPT_ENCODING_PK primary key (PUMP_PO_OID) using index);

create table PUMP_CONTROLS (
  PUMP_CONTROL_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  ENCODING_CODE                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  PIN_REQUIRED_ACTION_CID         varchar2(200),
  DISALLOWED_TERMINAL_TYPES       varchar2(200),
  constraint PUMP_CONTROLS_PK primary key (PUMP_CONTROL_OID) using index);

create table PRODUCT_TRANSLATIONS (
  PRODUCT_TRANSLATION_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_OID                     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  EXTERNAL_CLIENT_ID_OID          varchar2(200),
  DESCRIPTION                     varchar2(200),
  RESPONSE_CODE                   varchar2(200),
  constraint PRODUCT_TRANSLATIONS_PK primary key (PRODUCT_TRANSLATION_OID) using index);

create table PRODUCT_THRESHOLDS (
  PRODUCT_THRESHOLD_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_OID                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  CONVERSION_RATE                 varchar2(200),
  MINIMUM_UNIT_PRICE              varchar2(200),
  MAXIMUM_UNIT_PRICE              varchar2(200),
  MINIMUM_VOLUME                  varchar2(200),
  MAXIMUM_VOLUME                  varchar2(200),
  IS_VOLUME_CALCULATED            varchar2(200),
  IS_CUSTOMER_PRICING_REQUIRED    varchar2(200),
  IS_MERCHANT_PRICING_REQUIRED    varchar2(200),
  IS_MERCHANT_SAMEDAY_SETTLED     varchar2(200),
  DEFAULT_UNIT_PRICE              varchar2(200),
  constraint PRODUCT_THRESHOLDS_PK primary key (PRODUCT_THRESHOLD_OID) using index);

create table PRODUCT_RESTRICT_PRODS (
  PRODUCT_RESTRICT_PROD_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint PRODUCT_RESTRICT_PRODS_PK primary key (PRODUCT_RESTRICT_PROD_OID) using index);

create table PRODUCT_RESTRICTION_ENC (
  PRODUCT_RESTRICTION_ENC_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  AUTOSERVE_PLIM_CODE             varchar2(200),
  P30_PLIM_CODE                   varchar2(200),
  constraint PRODUCT_RESTRICTION_ENC_PK primary key (PRODUCT_RE_OID) using index);

create table PRODUCT_RESTRICTIONS (
  PRODUCT_RESTRICTION_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  PRODUCT_RESTRICTION_CODE        varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  MAILER_LINE_1                   varchar2(200),
  MAILER_LINE_2                   varchar2(200),
  MAILER_LINE_3                   varchar2(200),
  MAILER_LINE_4                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  constraint PRODUCT_RESTRICTIONS_PK primary key (PRODUCT_RESTRICTION_OID) using index);

create table PRODUCT_GROUP_PRODUCTS (
  PRODUCT_GROUP_PRODUCT_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint PRODUCT_GROUP_PRODUCTS_PK primary key (PRODUCT_GROUP_PRODUCT_OID) using index);

create table PRODUCT_GROUPS (
  PRODUCT_GROUP_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_GROUP_TYPE_CID          varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  EXTERNAL_CLIENT_CODE            varchar2(200),
  IS_GL_VOLUME_REQUIRED           varchar2(200),
  constraint PRODUCT_GROUPS_PK primary key (PRODUCT_GROUP_OID) using index);

create table PRODUCT_EMISSIONS (
  PRODUCT_EMISSION_OID            varchar2(200),
  EMISSION_TYPE_OID               varchar2(200),
  PRODUCT_OID                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint PRODUCT_EMISSIONS_PK primary key (PRODUCT_EMISSION_OID) using index);

create table PRODUCTS_SOLD_LOCATIONS (
  PRODUCTS_SOLD_LOCATION_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  PUMP_PRICE                      varchar2(200),
  TRANS_EFFECTIVE_AT              varchar2(200),
  constraint PRODUCTS_SOLD_LOCATIONS_PK primary key (PRODUCTS_SOLD_LOCATION_OID) using index);

create table PRODUCTS (
  PRODUCT_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRODUCT_UNIT_TYPE_CID           varchar2(200),
  IS_FUEL                         varchar2(200),
  IS_CREDIT_VOUCHER               varchar2(200),
  IS_TAX_APPLIED                  varchar2(200),
  IS_USED_BY_CARD_REMOTE          varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  IS_TAX_CREDITED                 varchar2(200),
  constraint PRODUCTS_PK primary key (PRODUCT_OID) using index);

create table PRODUCED_CARDS (
  PRODUCED_CARD_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  EMBOSSING_STATUS_CID            varchar2(200),
  ADDRESS_TYPE_CID                varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  COUNTRY_OID                     varchar2(200),
  PRODUCED_AT                     varchar2(200),
  EXPIRES_ON                      date,
  CUSTOMER_NO                     varchar2(200),
  CARD_NO                         varchar2(200),
  PLASTIC_CODE                    varchar2(200),
  IS_HIGH_PRIORITY                varchar2(200),
  IS_MANUALLY_PRODUCED            varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  TIME_LIMIT_OID                  varchar2(200),
  FEE_OID                         varchar2(200),
  STATE_OID                       varchar2(200),
  POSTAL_CODE                     varchar2(200),
  FILE_ID                         varchar2(200),
  FILE_CREATED_AT                 varchar2(200),
  PIN_PRINTED_AT                  varchar2(200),
  PIN_OFFSET                      varchar2(200),
  EMBOSS_LINE_1                   varchar2(200),
  EMBOSS_LINE_2                   varchar2(200),
  EMBOSS_LINE_3                   varchar2(200),
  EMBOSS_LINE_4                   varchar2(200),
  EMBOSS_LINE_5                   varchar2(200),
  CONTACT_NAME                    varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  REQUESTED_BY                    varchar2(200),
  CVV_SECURITY_CODE               varchar2(200),
  SUBURB                          varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  IS_VEHICLE_ID_REQ               varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_DRIVER_ID_REQ                varchar2(200),
  IS_ORDER_NUMBER_REQ             varchar2(200),
  IS_FLEET_ID_REQ                 varchar2(200),
  IS_CUSTOMER_SELECTION_REQ       varchar2(200),
  IS_ODOMETER_REQ                 varchar2(200),
  ODOMETER_TOLERANCE              varchar2(200),
  ODOMETER_OVERRIDE               varchar2(200),
  IS_RUC_PURCHASE_CARD            varchar2(200),
  IS_RUC_REPRINT_ONLY             varchar2(200),
  CARD_REQUEST_OID                varchar2(200),
  CREATED_AT                      varchar2(200),
  RECIPROCITY_OPTION_CID          varchar2(200),
  GEO_COVER_OPTION_CID            varchar2(200),
  ADDITIONAL_EMBOSS_DATA          varchar2(200),
  constraint PRODUCED_CARDS_PK primary key (PRODUCED_CARD_OID) using index);

create table PRIVATE_QUESTIONS (
  PRIVATE_QUESTION_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  constraint PRIVATE_QUESTIONS_PK primary key (PRIVATE_QUESTION_OID) using index);

create table PRICING_SCHEME_OPTIONS (
  PRICING_SCHEME_OPTION_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRICING_SCHEME_OID              varchar2(200),
  PRICING_OPTION_OID              varchar2(200),
  PRICING_TRANS_CATEGORY_CID      varchar2(200),
  IS_CLIENT_SPECIFIC              varchar2(200),
  MERCHANT_CLIENT_MID             varchar2(200),
  CUSTOMER_CLIENT_MID             varchar2(200),
  constraint PRICING_SCHEME_OPTIONS_PK primary key (PRICING_SCHEME_OPTION_OID) using index);

create table PRICING_SCHEMES (
  PRICING_SCHEME_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRICING_SCHEME_TYPE_CID         varchar2(200),
  CLIENT_MID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint PRICING_SCHEMES_PK primary key (PRICING_SCHEME_OID) using index);

create table PRICING_RECIPE_RULES (
  PRICING_RECIPE_RULE_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PRICING_RECIPE_OID              varchar2(200),
  PRICING_CATEGORY_OID            varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  IS_BETTER_OF_COMPARISON         varchar2(200),
  IS_PERCENT_CALCULATION          varchar2(200),
  IS_TO_BE_ADDED                  varchar2(200),
  constraint PRICING_RECIPE_RULES_PK primary key (PRICING_RECIPE_RULE_OID) using index);

create table PRICING_RECIPES (
  PRICING_RECIPE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  MERCHANT_PRICE_TYPE_CID         varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint PRICING_RECIPES_PK primary key (PRICING_RECIPE_OID) using index);

create table PRICING_PROFILES (
  PRICING_PROFILE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CLIENT_MID                      varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  APPLICATION_OID                 varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  constraint PRICING_PROFILES_PK primary key (PRICING_PROFILE_OID) using index);

create table PRICING_OPTION_RECIPES (
  PRICING_OPTION_RECIPE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRICING_OPTION_OID              varchar2(200),
  PRICING_RECIPE_OID              varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  EXPIRES_AT                      varchar2(200),
  constraint PRICING_OPTION_RECIPES_PK primary key (PRICING_OPTION_RECIPE_OID) using index);

create table PRICING_OPTION_PRODUCTS (
  PRICING_OPTION_PRODUCT_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRICING_OPTION_OID              varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint PRICING_OPTION_PRODUCTS_PK primary key (PRICING_OPTION_PRODUCT_OID) using index);

create table PRICING_OPTIONS (
  PRICING_OPTION_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint PRICING_OPTIONS_PK primary key (PRICING_OPTION_OID) using index);

create table PRICING_CONTROLS (
  PRICING_CONTROL_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PRICING_PROFILE_OID             varchar2(200),
  PRODUCT_OID                     varchar2(200),
  PRICING_SCHEME_OID              varchar2(200),
  constraint PRICING_CONTROLS_PK primary key (PRICING_CONTROL_OID) using index);

create table PRICING_CATEGORIES (
  PRICING_CATEGORY_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  IS_BLANK_DISPLAYED              varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint PRICING_CATEGORIES_PK primary key (PRICING_CATEGORIE_OID) using index);

create table POS_TRANS_LINE_ITEMS (
  POS_TRANSACTION_OID             varchar2(200),
  LINE_NUMBER                     varchar2(200),
  WAS_CORRECTED                   varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  QUANTITY                        varchar2(200),
  UNIT_PRICE                      varchar2(200),
  VALUE                           varchar2(200),
  DISCOUNT_AMOUNT                 varchar2(200),
  DISCOUNT_PERCENTAGE             varchar2(200),
  UNIT_TAX_RATE                   varchar2(200),
  NET_COST_PRICE                  varchar2(200),
  constraint POS_TRANS_LINE_ITEMS_PK primary key (POS_TRANS_LINE_ITEM_OID) using index);

create table POS_TRANSACTIONS (
  POS_TRANSACTION_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_STATUS_CID          varchar2(200),
  MERCHANT_STATUS_CID             varchar2(200),
  RECONCILIATION_TYPE_CID         varchar2(200),
  PROCESSED_AT                    varchar2(200),
  CARD_NO                         varchar2(200),
  DATE_TIME_EFFECTIVE             varchar2(200),
  REFERENCE                       varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  TRANSACTION_CODE                varchar2(200),
  LOCATION_CODE                   varchar2(200),
  TERMINAL_ID                     varchar2(200),
  POS_LOCN_RECONCILIATION_OID     varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  ODOMETER                        varchar2(200),
  ATTENTION_KEY                   varchar2(200),
  AUTHORISATION_NO                varchar2(200),
  BATCH_NO                        varchar2(200),
  BATCH_SOURCE                    varchar2(200),
  DRIVER_ID                       varchar2(200),
  VEHICLE_ID                      varchar2(200),
  POS_ENTRY_1                     varchar2(200),
  POS_ENTRY_2                     varchar2(200),
  POS_ENTRY_3                     varchar2(200),
  POS_ENTRY_4                     varchar2(200),
  POS_ENTRY_5                     varchar2(200),
  POSTING_PROCESS_ID              varchar2(200),
  LOCATION_NAME                   varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  TRINPUT_DATE_TIME_EFF           varchar2(200),
  NETWORK_SETTLED_ON              date,
  INTERFACE_FILE_INC_LOG_OID      varchar2(200),
  SOURCE_CURRENCY                 varchar2(200),
  SOURCE_AMOUNT                   varchar2(200),
  EXCHANGE_RATE                   varchar2(200),
  AUTH_TRANSACTION_LOG_OID        varchar2(200),
  constraint POS_TRANSACTIONS_PK primary key (POS_TRANSACTION_OID) using index);

create table POS_LOCN_RECONCILIATIONS (
  POS_LOCN_RECONCILIATION_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RECONCILIATION_TYPE_CID         varchar2(200),
  BATCH_STATUS_CID                varchar2(200),
  PROCESSED_ON                    date,
  TRANSACTION_COUNT               varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  TRANSACTION_CODE                varchar2(200),
  LOCATION_NO                     varchar2(200),
  CARD_ISO_PREFIX                 varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  TERMINAL_ID                     varchar2(200),
  CLOSED_ON                       date,
  BATCH_NO                        varchar2(200),
  POS_LOCN_BATCH_SOURCE           varchar2(200),
  constraint POS_LOCN_RECONCILIATIONS_PK primary key (POS_LOCN_RECONCILIATION_OID) using index);

create table POS_INTERFACE (
  POS_INTERFACE_OID               varchar2(200),
  LAST_UPDATED_AT                 date,
  SETTLEMENT_DATE                 date,
  LOCATION_NO                     varchar2(200),
  TERMINAL_ID                     varchar2(200),
  POS_TOT_DR_COUNT                varchar2(200),
  POS_TOT_DR_AMOUNT               varchar2(200),
  POS_TOT_CR_COUNT                varchar2(200),
  POS_TOT_CR_AMOUNT               varchar2(200),
  POS_TOT_CSH_COUNT               varchar2(200),
  POS_TOT_CSH_AMOUNT              varchar2(200),
  POS_BATCH_SOURCE                varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_INC_LOG_OID      varchar2(200),
  constraint POS_INTERFACE_PK primary key (PO_OID) using index);

create table POINT_VALUES (
  POINT_VALUE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  POINT_PROGRAM_OID               varchar2(200),
  VALUE_RANGE_FROM                varchar2(200),
  VALUE_RANGE_TO                  varchar2(200),
  RATE                            varchar2(200),
  constraint POINT_VALUES_PK primary key (POINT_VALUE_OID) using index);

create table POINT_TOTALS (
  POINT_TOTAL_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CREATED_ON                      date,
  BALANCE                         varchar2(200),
  MTD_ACCRUED_AMOUNT              varchar2(200),
  MTD_REDEEMED_AMOUNT             varchar2(200),
  MTD_DELETED_AMOUNT              varchar2(200),
  MTD_EXPIRED_AMOUNT              varchar2(200),
  YTD_ACCRUED_AMOUNT              varchar2(200),
  YTD_REDEEMED_AMOUNT             varchar2(200),
  YTD_DELETED_AMOUNT              varchar2(200),
  YTD_EXPIRED_AMOUNT              varchar2(200),
  LTD_ACCRUED_AMOUNT              varchar2(200),
  LTD_REDEEMED_AMOUNT             varchar2(200),
  LTD_DELETED_AMOUNT              varchar2(200),
  LTD_EXPIRED_AMOUNT              varchar2(200),
  LMTD_ACCRUED_AMOUNT             varchar2(200),
  LMTD_REDEEMED_AMOUNT            varchar2(200),
  LMTD_DELETED_AMOUNT             varchar2(200),
  LMTD_EXPIRED_AMOUNT             varchar2(200),
  constraint POINT_TOTALS_PK primary key (POINT_TOTAL_OID) using index);

create table POINT_RATINGS (
  POINT_RATING_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RATING                          varchar2(200),
  PRIORITY                        varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint POINT_RATINGS_PK primary key (POINT_RATING_OID) using index);

create table POINT_PROGRAMS (
  POINT_PROGRAM_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  POINT_CATEGORY_OID              varchar2(200),
  POINT_PROFILE_OID               varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  MEMBER_TYPE_CID                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint POINT_PROGRAMS_PK primary key (POINT_PROGRAM_OID) using index);

create table POINT_PROFILES (
  POINT_PROFILE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  CLIENT_MID                      varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  constraint POINT_PROFILES_PK primary key (POINT_PROFILE_OID) using index);

create table POINT_OUT_OPTIONS (
  POINT_OUT_OPTION_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  POINT_PROFILE_OID               varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  APPLICATION_OID                 varchar2(200),
  constraint POINT_OUT_OPTIONS_PK primary key (POINT_OUT_OPTION_OID) using index);

create table POINT_CATEGORIES (
  POINT_CATEGORY_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  POINT_TYPE_CID                  varchar2(200),
  POINT_CALCULATION_TYPE_CID      varchar2(200),
  POINT_POSTING_METHOD_CID        varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  IS_CALCULATED_ON_VOLUME         varchar2(200),
  DURATION                        varchar2(200),
  DESCRIPTION                     varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  MINIMUM_CHARGE                  varchar2(200),
  MAXIMUM_CHARGE                  varchar2(200),
  constraint POINT_CATEGORIES_PK primary key (POINT_CATEGORIE_OID) using index);

create table PERIOD_BALANCES (
  PERIOD_BALANCE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  PERIOD_BALANCE_TYPE_CID         varchar2(200),
  CREATED_ON                      date,
  OPENING_BALANCE                 varchar2(200),
  CLOSING_BALANCE                 varchar2(200),
  OVERDUE_AMOUNT                  varchar2(200),
  DISPUTES_TOTAL                  varchar2(200),
  DUNNING_CODE                    varchar2(200),
  DUNNING_CONTROL_1_DESCR         varchar2(200),
  DUNNING_CONTROL_2_DESCR         varchar2(200),
  DUNNING_CONTROL_3_DESCR         varchar2(200),
  DUNNING_CONTROL_4_DESCR         varchar2(200),
  DESCRIPTION                     varchar2(200),
  TOTAL_VOLUME                    varchar2(200),
  TOTAL_BALANCE                   varchar2(200),
  constraint PERIOD_BALANCES_PK primary key (PERIOD_BALANCE_OID) using index);

create table PERIOD_ACCOUNT_TOTALS (
  PERIOD_ACCOUNT_TOTAL_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PERIOD_BALANCE_OID              varchar2(200),
  ACCOUNT_TOTAL_TYPE_CID          varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  AMOUNT                          varchar2(200),
  constraint PERIOD_ACCOUNT_TOTALS_PK primary key (PERIOD_ACCOUNT_TOTAL_OID) using index);

create table PAYMENT_REQUESTS (
  PAYMENT_REQUEST_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  MINIMUM_DUE                     varchar2(200),
  REQUEST_TOTAL                   varchar2(200),
  IS_PAID_IN_FULL                 varchar2(200),
  BILLED_ON                       date,
  ORIGINAL_DUE_ON                 date,
  DUE_ON                          date,
  OVERDUE_DAYS                    varchar2(200),
  CREDIT_NOTE_AMOUNT              varchar2(200),
  LATE_FEE_CHARGED                varchar2(200),
  constraint PAYMENT_REQUESTS_PK primary key (PAYMENT_REQUEST_OID) using index);

create table PAYMENT_GATEWAY_LOGS (
  PAYMENT_GATEWAY_LOG_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SIGNED_DATE_TIME                varchar2(200),
  PROCESSING_STATUS_CID           varchar2(200),
  PAYMENT_GATEWAY_OID             varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  REFERENCE_NUMBER                varchar2(200),
  TRANSACTION_UUID                varchar2(200),
  PROCESSED_BY                    varchar2(200),
  REASON_CODE                     varchar2(200),
  constraint PAYMENT_GATEWAY_LOGS_PK primary key (PAYMENT_GATEWAY_LOG_OID) using index);

create table PAYMENT_GATEWAYS (
  PAYMENT_GATEWAY_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  GATEWAY_PAYMENT_METHOD_CID      varchar2(200),
  PROFILE_ID                      varchar2(200),
  ACCESS_KEY                      varchar2(200),
  SECRET_KEY                      varchar2(200),
  GATEWAY_TRANSACTION_TYPE        varchar2(200),
  VISA_TRANSACTION_TYPE_OID       varchar2(200),
  MASTER_TRANSACTION_TYPE_OID     varchar2(200),
  AMEX_TRANSACTION_TYPE_OID       varchar2(200),
  GATEWAY_URL                     varchar2(200),
  OVERRIDE_CUSTOM_RECEIPT_PAGE    varchar2(200),
  constraint PAYMENT_GATEWAYS_PK primary key (PAYMENT_GATEWAY_OID) using index);

create table PAYMENT_CODES (
  PAYMENT_CODE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  WALLET_OID                      varchar2(200),
  WALLET_CARD_OID                 varchar2(200),
  PAYMENT_CODE                    varchar2(200),
  CREATED_AT                      date,
  EXPIRES_AT                      date,
  constraint PAYMENT_CODES_PK primary key (PAYMENT_CODE_OID) using index);

create table PAYMENT_ARRANGEMENTS (
  PAYMENT_ARRANGEMENT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  WAS_LAST_PAYMENT_MISSED         varchar2(200),
  CREATED_BY                      varchar2(200),
  CREATED_ON                      date,
  NEXT_PAYMENT_DUE_ON             date,
  REVIEW_ON                       date,
  PAYMENT_PERIODS                 varchar2(200),
  PERIODS_COMPLETE                varchar2(200),
  PERIODS_MISSED                  varchar2(200),
  OUTSTANDING_BALANCE             varchar2(200),
  INITIAL_AMOUNT                  varchar2(200),
  INSTALMENT_AMOUNT               varchar2(200),
  AMOUNT_PAID                     varchar2(200),
  TOTAL_PAID                      varchar2(200),
  LAST_PAYMENT_MADE_ON            date,
  CHEQUE_NO                       varchar2(200),
  constraint PAYMENT_ARRANGEMENTS_PK primary key (PAYMENT_ARRANGEMENT_OID) using index);

create table OPERATION_LIMIT (
  OPERATION_LIMIT_OID             varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  USER_OID                        varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  MAXIMUM_REBATE_AMOUNT           varchar2(200),
  MAXIMUM_REBATE_PERCENT          varchar2(200),
  MAXIMUM_CREDIT_LIMIT            varchar2(200),
  MAXIMUM_CREDIT_PERCENT          varchar2(200),
  MAX_SUNDRY_ADJ_AUTH_REQ_AMNT    varchar2(200),
  MAX_SUNDRY_ADJ_APPR_AMNT        varchar2(200),
  CAN_AUTH_OWN_SUNDRY_ADJ         varchar2(200),
  MAX_SUNDRY_PYMNT_ENTRY_AMNT     varchar2(200),
  MAX_SUNDRY_PYMNT_AUTH_REQ_AMNT  varchar2(200),
  MAX_SUNDRY_PYMNT_APPR_AMNT      varchar2(200),
  CAN_AUTH_OWN_SUNDRY_PYMNT       varchar2(200),
  MAX_MANUAL_PAYMENT_AMOUNT       varchar2(200),
  MAX_SUNDRY_ADJ_ENTRY_AMNT       varchar2(200),
  MAX_CREDIT_NOTE_ENTRY_AMNT      varchar2(200),
  MAX_CREDIT_NOTE_AUTH_REQ_AMNT   varchar2(200),
  MAX_CREDIT_NOTE_APPR_AMNT       varchar2(200),
  CAN_AUTH_OWN_CREDIT_NOTE        varchar2(200),
  constraint OPERATION_LIMIT_PK primary key (OPERATION_LIMIT) using index);

create table OLS_EXTERNAL_CONTENT_LINKS (
  OLS_EXTERNAL_CONTENT_LINK_OID   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CONTENT_TYPE_CID                varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  URL                             varchar2(200),
  constraint OLS_EXTERNAL_CONTENT_LINKS_PK primary key (OLS_EXTERNAL_CONTENT_LINK_OID) using index);

create table OLS_CONFIG (
  OLS_CONFIG_OID                  varchar2(200),
  CLIENT_MID                      varchar2(200),
  IS_THIRD_PARTY_AUTH             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint OLS_CONFIG_PK primary key (OL_OID) using index);

create table NUMBERING_SEQUENCE_RESERVES (
  NUMBERING_SEQ_RESERVE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  NUMBERING_SEQUENCE_OID          varchar2(200),
  RANGE_FROM                      varchar2(200),
  RANGE_TO                        varchar2(200),
  constraint NUMBERING_SEQUENCE_RESERVES_PK primary key (NUMBERING_SEQUENCE_RESERVE_OID) using index);

create table NUMBERING_SEQUENCES (
  NUMBERING_SEQUENCE_OID          varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  NUMBERING_METHOD_OID            varchar2(200),
  STARTING_NUMBER                 varchar2(200),
  INCREMENT_VALUE                 varchar2(200),
  LAST_NUMBER_USED                varchar2(200),
  HIGHEST_NUMBER_ALLOWED          varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint NUMBERING_SEQUENCES_PK primary key (NUMBERING_SEQUENCE_OID) using index);

create table NUMBERING_METHODS (
  NUMBERING_METHOD_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  NUMBERING_TYPE_CID              varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLASS_REFERENCE                 varchar2(200),
  constraint NUMBERING_METHODS_PK primary key (NUMBERING_METHOD_OID) using index);

create table M_MERCHANTS (
  MERCHANT_MID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  MERCHANT_TYPE_DID               varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  CURRENCY_OID                    varchar2(200),
  MERCHANT_NO                     varchar2(200),
  NAME                            varchar2(200),
  IS_WEB_ACCESS_REQUIRED          varchar2(200),
  CREATED_ON                      date,
  GL_CHANNEL_DID                  varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  ADMIN_TERRITORY_OID             varchar2(200),
  MARKETING_TERRITORY_OID         varchar2(200),
  PULL_CODE_DID                   varchar2(200),
  BUSINESS_COMMENCED_ON           date,
  CUST_SERVICE_EXPIRES_ON         date,
  ACCOUNT_NO                      varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  TAX_NO                          varchar2(200),
  GL_COST_CENTRE_CODE             varchar2(200),
  CUST_SERVICE_PULL_INITS         varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  TRADING_NAME                    varchar2(200),
  CHANNEL_OF_TRADE_DID            varchar2(200),
  CONTACT_NAME                    varchar2(200),
  IS_USING_ELECTRONIC_REPORTING   varchar2(200),
  IS_USING_ELECTRONIC_MARKETING   varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  constraint M_MERCHANTS_PK primary key (MERCHANT_MID) using index);

create table M_LOCATION_GROUPS (
  LOCATION_GROUP_MID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint M_LOCATION_GROUPS_PK primary key (LOCATION_GROUP_MID) using index);

create table M_LOCATIONS (
  LOCATION_MID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  LOCATION_TYPE_DID               varchar2(200),
  LOCATION_STATUS_CID             varchar2(200),
  MARKETING_TERRITORY_OID         varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  LOCATION_NO                     varchar2(200),
  NAME                            varchar2(200),
  IS_EFTPOS_AVAILABLE             varchar2(200),
  IS_FUEL_REBATE_ALLOWED          varchar2(200),
  IS_WEB_ACCESS_REQUIRED          varchar2(200),
  CREATED_ON                      date,
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  ADMIN_TERRITORY_OID             varchar2(200),
  FIN_YEAR_END_MONTH_OID          varchar2(200),
  BUSINESS_HOURS_DID              varchar2(200),
  GEOGRAPHIC_AREA_1_OID           varchar2(200),
  GEOGRAPHIC_AREA_2_OID           varchar2(200),
  GEOGRAPHIC_AREA_3_OID           varchar2(200),
  GEOGRAPHIC_AREA_4_OID           varchar2(200),
  GEOGRAPHIC_AREA_5_OID           varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  OPENED_ON                       date,
  CLOSED_ON                       date,
  EXTERNAL_CODE                   varchar2(200),
  TAX_NO                          varchar2(200),
  TRADING_NAME                    varchar2(200),
  ISP_ACCOUNT_NUMBER              varchar2(200),
  CONTACT_NAME                    varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EXTERNAL_NAME                   varchar2(200),
  LONGITUDE                       varchar2(200),
  LATITUDE                        varchar2(200),
  constraint M_LOCATIONS_PK primary key (LOCATION_MID) using index);

create table M_CUSTOMERS (
  CUSTOMER_MID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_TYPE_DID               varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CURRENCY_OID                    varchar2(200),
  BUSINESS_TYPE_DID               varchar2(200),
  INDUSTRY_OID                    varchar2(200),
  BUYER_CLASS_DID                 varchar2(200),
  FIN_YEAR_END_MONTH_OID          varchar2(200),
  ADMIN_TERRITORY_OID             varchar2(200),
  MARKETING_TERRITORY_OID         varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  NAME                            varchar2(200),
  IS_WEB_ACCESS_REQUIRED          varchar2(200),
  CREATED_ON                      date,
  LAST_CARD_SEQUENCE_USED         varchar2(200),
  APPLICATION_OID                 varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  EXTERNAL_CUSTOMER_CODE          varchar2(200),
  GL_COST_CENTRE_CODE             varchar2(200),
  EMBOSSING_NAME                  varchar2(200),
  TRADING_NAME                    varchar2(200),
  MAX_CARD_EXPIRY_DATE            date,
  PUMP_CONTROL_OID                varchar2(200),
  PIN_OFFSET                      varchar2(200),
  CUSTOMER_VALUE_1_OID            varchar2(200),
  CUSTOMER_VALUE_2_OID            varchar2(200),
  CUSTOMER_VALUE_3_OID            varchar2(200),
  CARD_GRAPHIC_OID                varchar2(200),
  MAIL_INDICATOR_OID              varchar2(200),
  TRUSTEE_FOR                     varchar2(200),
  PARENT_COMPANY                  varchar2(200),
  CONTACT_NAME                    varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  PROMOTIONAL_MATERIAL            varchar2(200),
  COMPANY_NO                      varchar2(200),
  MAIN_BUSINESS                   varchar2(200),
  CARBON_OFFSET                   varchar2(200),
  CARBON_OFFSET_LEVEL_DID         varchar2(200),
  NETWORK_ID                      varchar2(200),
  CURRENT_SUPPLIER_DID            varchar2(200),
  LAST_TRANSFER_SEQ_USED          varchar2(200),
  EXT_DELIVERY_REF                varchar2(200),
  EXT_ACCOUNT_REF                 varchar2(200),
  ENCRYPTED_PIN                   varchar2(200),
  IS_USING_ELECTRONIC_REPORTING   varchar2(200),
  IS_USING_ELECTRONIC_MARKETING   varchar2(200),
  IS_REPORT_LEVEL_USED            varchar2(200),
  REPORT_LEVELS                   varchar2(200),
  IS_SEND_TO_LEVEL1               varchar2(200),
  EXT_DELIVERY_NAME               varchar2(200),
  EXT_ACCOUNT_ID                  varchar2(200),
  DISTRIBUTOR_ID                  varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  IS_SUPPRESS_PIN_MAILER          varchar2(200),
  CARD_REISSUE_PROFILE_OID        varchar2(200),
  AUTHENTICATION_ANSWER           varchar2(200),
  OCCUPATION                      varchar2(200),
  CONTACTLESS_PAYMENT             varchar2(200),
  CONTACTLESS_PMT_MODIFIED_ON     date,
  TRADE_EXPOSURE_LIMIT            varchar2(200),
  EMBASSY_CODE                    varchar2(200),
  RESIDENCY_COUNTRY_OID           varchar2(200),
  INTERACTION_TYPE_DID            varchar2(200),
  IS_TAX_EXEMPT                   varchar2(200),
  constraint M_CUSTOMERS_PK primary key (CUSTOMER_MID) using index);

create table M_CLIENT_GROUPS (
  CLIENT_GROUP_MID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  NAME                            varchar2(200),
  constraint M_CLIENT_GROUPS_PK primary key (CLIENT_GROUP_MID) using index);

create table M_CLIENTS (
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  CURRENCY_OID                    varchar2(200),
  REMITTANCE_NO_ASSIGNMENT_CID    varchar2(200),
  GL_COMPANY_DID                  varchar2(200),
  DEFAULT_POINT_DURATION          varchar2(200),
  SMALL_BALANCE                   varchar2(200),
  LINE_ITEM_TOLERANCE             varchar2(200),
  NAME                            varchar2(200),
  RECONCILIATION_TYPE_CID         varchar2(200),
  ACCOUNT_SUB_STATUS_OID          varchar2(200),
  CLIENT_ENTITY_TYPE_CID          varchar2(200),
  ENTITY_MEMBER_OID               varchar2(200),
  COUNTRY_OID                     varchar2(200),
  MERCHANT_NO_SEQUENCE_OID        varchar2(200),
  LOCATION_NO_SEQUENCE_OID        varchar2(200),
  DEFAULT_CUSTOMER_NO_SEQ_OID     varchar2(200),
  REMITTANCE_ADDRESS_OID          varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  POINT_EXP_TRANSACTION_TYPE_OID  varchar2(200),
  POINT_RED_TRANSACTION_TYPE_OID  varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  SHORT_NAME                      varchar2(200),
  TAX_NO                          varchar2(200),
  IMAGE_FILE_NAME                 varchar2(200),
  APPLICATION_NO_SEQUENCE_OID     varchar2(200),
  IS_REMITTANCE_NO_BY_CLIENT      varchar2(200),
  IS_POSTING_RESTRICTED           varchar2(200),
  IS_GL_USED                      varchar2(200),
  FINANCIAL_YEAR_ENDS_ON          date,
  PROCESSING_DATE                 date,
  CURRENT_MONTH                   varchar2(200),
  CURRENT_YEAR                    varchar2(200),
  GL_REGION_DID                   varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  GL_TOTAL_CATEGORY_DID           varchar2(200),
  PRICING_RECEIVED_AT             varchar2(200),
  IS_PRICING_DATE_AUTOMATIC       varchar2(200),
  TIMEZONE_OID                    varchar2(200),
  IS_PRODUCTS_SOLD_AT_LOC         varchar2(200),
  IS_POSTAL_ADDRESS_OVERRIDE      varchar2(200),
  SMALL_BAL_ADJUSTMENT_TYPE_OID   varchar2(200),
  IS_GL_BY_MEMBER                 varchar2(200),
  RATE_CODE_OID                   varchar2(200),
  ODOMETER_TOLERANCE              varchar2(200),
  ODOMETER_OVERRIDE               varchar2(200),
  MONTH_END_1_ON                  date,
  MONTH_END_2_ON                  date,
  MONTH_END_3_ON                  date,
  IS_TRANS_PROCESSING_ALLOWED     varchar2(200),
  EXCESSIVE_TRANS_THRESHOLD       varchar2(200),
  MERCH_ENTERED_BATCH_SEQ_OID     varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  PAYMENT_ID_SEQ_OID              varchar2(200),
  BANK_ACCOUNT_OID                varchar2(200),
  OLS_MANDATORY_PAGE_CID          varchar2(200),
  OLS_MANDATORY_URL               varchar2(200),
  OLS_MANDATORY_VALID_FROM        date,
  IS_GL_PRODUCT_ACCUMULATION      varchar2(200),
  GL_PRODUCT_TRANSLATION          varchar2(200),
  CONTROL_ACCOUNT_OID             varchar2(200),
  MANUAL_AUTH_NO_SEQ_OID          varchar2(200),
  IS_GL_EXTRACT_CUSTOMER_TRANS    varchar2(200),
  IS_GL_EXTRACT_MERCHANT_TRANS    varchar2(200),
  IS_GL_DOUBLE_ENTRIES            varchar2(200),
  IE_CLIENT_ID                    varchar2(200),
  IS_GL_EXTRACT_BANKING_TRANS     varchar2(200),
  GP_REFERENCE_NO_SEQUENCE_OID    varchar2(200),
  MERCHANT_STMT_NO_SEQUENCE_OID   varchar2(200),
  COMPANY_REG_NO                  varchar2(200),
  DD_PREPROCESS_DAYS              varchar2(200),
  REPORTING_CLIENT_NAME           varchar2(200),
  IS_GL_BY_BATCH                  varchar2(200),
  GL_CONTROL_ACCOUNT_OID          varchar2(200),
  constraint M_CLIENTS_PK primary key (CLIENT_MID) using index);

create table MONTH_CUST_PRD_LOC_TOTS (
  MONTH_CUST_PRD_LOC_TOTS_OID     varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  QUANTITY                        varchar2(200),
  VALUE                           varchar2(200),
  TAX_VALUE                       varchar2(200),
  REBATE_VALUE                    varchar2(200),
  PERIOD_MONTH                    varchar2(200),
  PERIOD_YEAR                     varchar2(200),
  constraint MONTH_CUST_PRD_LOC_TOTS_PK primary key (MONTH_CUST_PRD_LOC_TOT_OID) using index);

create table MONTHS (
  MONTH_OID                       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  DAYS                            varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  constraint MONTHS_PK primary key (MONTH_OID) using index);

create table MERCH_AGRMNT_VALUE_TYPES (
  MERCH_AGRMNT_VALUE_TYPE_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  IS_MANDATORY                    varchar2(200),
  MERCH_AGRMNT_VALUE_NO           varchar2(200),
  DESCRIPTION                     varchar2(200),
  SCREEN_LITERAL                  varchar2(200),
  constraint MERCH_AGRMNT_VALUE_TYPES_PK primary key (MERCH_AGRMNT_VALUE_TYPE_OID) using index);

create table MERCH_AGRMNT_VALUES (
  MERCH_AGRMNT_VALUE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MERCH_AGRMNT_VALUE_TYPE_OID     varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint MERCH_AGRMNT_VALUES_PK primary key (MERCH_AGRMNT_VALUE_OID) using index);

create table MERCHANT_TRANS_GROUPS (
  MERCHANT_TRANS_GROUP_OID        varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_GROUP_CID           varchar2(200),
  CAPTURE_TYPE_OID                varchar2(200),
  constraint MERCHANT_TRANS_GROUPS_PK primary key (MERCHANT_TRANS_GROUP_OID) using index);

create table MERCHANT_PRICING_LOGS (
  MERCHANT_PRICING_LOG_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  PRICING_PROFILE_OID             varchar2(200),
  IS_REPRICING_REQUIRED           varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  constraint MERCHANT_PRICING_LOGS_PK primary key (MERCHANT_PRICING_LOG_OID) using index);

create table MERCHANT_PAYMENT_TYPES (
  MERCHANT_PAYMENT_TYPE_OID       varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PAYMENT_METHOD_CID              varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  DEBIT_TRANSACTION_TYPE_OID      varchar2(200),
  CREDIT_TRANSACTION_TYPE_OID     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint MERCHANT_PAYMENT_TYPES_PK primary key (MERCHANT_PAYMENT_TYPE_OID) using index);

create table MERCHANT_AGREEMENTS (
  MERCHANT_AGREEMENT_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RELATIONSHIP_ASSIGNMENT_OID     varchar2(200),
  MERCHANT_PAYMENT_TYPE_OID       varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  IS_DEBIT_AUTHORISED             varchar2(200),
  IS_SAME_DAY_SETTLE              varchar2(200),
  TAX_EXEMPTION_OID               varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  BANK_ACCOUNT_OID                varchar2(200),
  CONTRA_ACCOUNT_CODE             varchar2(200),
  AP_ACCOUNT_CODE                 varchar2(200),
  TAX_NO                          varchar2(200),
  MERCH_AGRMNT_VALUE_1_OID        varchar2(200),
  MERCH_AGRMNT_VALUE_2_OID        varchar2(200),
  MERCH_AGRMNT_VALUE_3_OID        varchar2(200),
  IS_PRIVATELY_OWNED              varchar2(200),
  BANK_TYPE                       varchar2(200),
  constraint MERCHANT_AGREEMENTS_PK primary key (MERCHANT_AGREEMENT_OID) using index);

create table MENU_ACCESS_RESTRICTIONS (
  MENU_ACCESS_RESTRICTION_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  SCREEN_ATTRIBUTE_CID            varchar2(200),
  ACTION_ID                       varchar2(200),
  MENU_ID                         varchar2(200),
  PARENT_MENU_ID                  varchar2(200),
  constraint MENU_ACCESS_RESTRICTIONS_PK primary key (MENU_ACCESS_RESTRICTION_OID) using index);

create table MEMBERS (
  MEMBER_OID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  constraint MEMBERS_PK primary key (MEMBER_OID) using index);

create table MASTER_PIN_KEY (
  MASTER_PIN_KEY_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PART_1                          varchar2(200),
  PART_2                          varchar2(200),
  PART_3                          varchar2(200),
  CLIENT_MID                      varchar2(200),
  DECIMALIZATION                  varchar2(200),
  constraint MASTER_PIN_KEY_PK primary key (MA_OID) using index);

create table MARKETING_MESSAGES (
  MARKETING_MESSAGE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CHANNEL_OF_TRADE_DID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXPIRES_ON                      date,
  EFFECTIVE_ON                    date,
  REPORT_TYPE_OID                 varchar2(200),
  IS_ONLINE                       varchar2(200),
  CHARACTER_COUNT                 varchar2(200),
  MOBILE_MESSAGE                  varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  MESSAGE                         varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  constraint MARKETING_MESSAGES_PK primary key (MARKETING_MESSAGE_OID) using index);

create table MAN_TRANS_LINE_ITEMS (
  MANUAL_TRANSACTION_OID          varchar2(200),
  LINE_NUMBER                     varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  QUANTITY                        varchar2(200),
  UNIT_PRICE                      varchar2(200),
  VALUE                           varchar2(200),
  DISCOUNT_AMOUNT                 varchar2(200),
  DISCOUNT_PERCENTAGE             varchar2(200),
  UNIT_TAX_RATE                   varchar2(200),
  NET_COST_PRICE                  varchar2(200),
  constraint MAN_TRANS_LINE_ITEMS_PK primary key (MAN_TRANS_LINE_ITEM_OID) using index);

create table MAN_TRANS_BATCH_HEADERS (
  MAN_TRANS_BATCH_HEADER_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  REFERENCE                       varchar2(200),
  BATCH_TYPE_OID                  varchar2(200),
  BATCH_STATUS_CID                varchar2(200),
  CONTROL_COUNT                   varchar2(200),
  CONTROL_TOTAL                   varchar2(200),
  ACTUAL_COUNT                    varchar2(200),
  ACTUAL_TOTAL                    varchar2(200),
  CREATED_ON                      date,
  LAST_ACTION_AT                  varchar2(200),
  LAST_ACTION_BY                  varchar2(200),
  constraint MAN_TRANS_BATCH_HEADERS_PK primary key (MAN_TRANS_BATCH_HEADER_OID) using index);

create table MANUAL_VOUCHERS (
  MANUAL_VOUCHER_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  STATE_OID                       varchar2(200),
  TRANS_TRANSLATION_OID           varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  REFERENCE                       varchar2(200),
  CARD_NO                         varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  FILE_NAME                       varchar2(200),
  CARD_OID                        varchar2(200),
  LOCATION_NO                     varchar2(200),
  constraint MANUAL_VOUCHERS_PK primary key (MANUAL_VOUCHER_OID) using index);

create table MANUAL_TRANSACTIONS (
  MANUAL_TRANSACTION_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MAN_TRANS_BATCH_HEADER_OID      varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  PROCESSED_AT                    varchar2(200),
  REFERENCE                       varchar2(200),
  CARD_NO                         varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  IS_A_CREDIT_TRANSACTION         varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  ODOMETER                        varchar2(200),
  LOCATION_CODE                   varchar2(200),
  BATCH_NO                        varchar2(200),
  TERMINAL_ID                     varchar2(200),
  DRIVER_ID                       varchar2(200),
  VEHICLE_ID                      varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  POS_ENTRY_1                     varchar2(200),
  POS_ENTRY_2                     varchar2(200),
  POS_ENTRY_3                     varchar2(200),
  POS_ENTRY_4                     varchar2(200),
  POS_ENTRY_5                     varchar2(200),
  AUTHORISATION_NO                varchar2(200),
  VOUCHER_FILE_NAME               varchar2(200),
  LOCATION_MID                    varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  MANUAL_VOUCHER_OID              varchar2(200),
  constraint MANUAL_TRANSACTIONS_PK primary key (MANUAL_TRANSACTION_OID) using index);

create table MANUAL_AUTH_REQUEST (
  MANUAL_AUTH_REQUEST_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SITE_LOCATION_NUMBER            varchar2(200),
  MERCHANT_OPERATOR_NAME          varchar2(200),
  MERCHANT_OPERATOR_TEL           varchar2(200),
  TRANSACTION_DATE                date,
  TRANSACTION_VALUE               varchar2(200),
  CARD_NUMBER                     varchar2(200),
  CARD_EXP_DATE                   varchar2(200),
  PRODUCT_RESTRICTIONS            varchar2(200),
  PRODUCTS_PURCHASED              varchar2(200),
  AUTH_CODE                       varchar2(200),
  AUTH_CODE_REQ_DATE              date,
  AUTH_STATUS_CID                 varchar2(200),
  AUTH_CODE_USED_DATE             date,
  AUTH_CODE_REASON                varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint MANUAL_AUTH_REQUEST_PK primary key (MANUAL_AUTH_REQUE_OID) using index);

create table MAIL_INDICATORS (
  MAIL_INDICATOR_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  EMBOSS_CODE                     varchar2(200),
  constraint MAIL_INDICATORS_PK primary key (MAIL_INDICATOR_OID) using index);

create table LOCATION_RESTRICT_LOCS (
  LOCATION_RESTRICT_LOCS_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_RESTRICTION_OID        varchar2(200),
  RESTRICTED_BY                   varchar2(200),
  constraint LOCATION_RESTRICT_LOCS_PK primary key (LOCATION_RESTRICT_LOC_OID) using index);

create table LOCATION_RESTRICTIONS (
  LOCATION_RESTRICTION_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  LOCATION_RESTRICTION_TYPE_CID   varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint LOCATION_RESTRICTIONS_PK primary key (LOCATION_RESTRICTION_OID) using index);

create table LOCATION_RECON_STMT_NUMBERS (
  CLIENT_MID                      varchar2(200),
  STATEMENT_NUMBER                varchar2(200),
  MERCHANT_REFERENCE              varchar2(200),
  PROCESSED_ON                    date,
  constraint LOCATION_RECON_STMT_NUMBERS_PK primary key (LOCATION_RECON_STMT_NUMBER_OID) using index);

create table LOCATION_RECONCILIATIONS (
  LOCATION_RECONCILIATION_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  MERCHANT_MID                    varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  TRANSACTION_GROUP_CID           varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  QUANTITY                        varchar2(200),
  TRANSACTION_AMOUNT              varchar2(200),
  NET_AMOUNT                      varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  REBATE_CONTRIBUTION_TOTAL       varchar2(200),
  UNIT_PRICE_VARIANCE             varchar2(200),
  PROCESSED_ON                    date,
  POS_LOCN_RECONCILIATION_OID     varchar2(200),
  SETTLEMENT_DESCRIPTION          varchar2(200),
  PAID_ON                         date,
  GL_EXTRACT_STATUS_CID           varchar2(200),
  STATEMENT_NUMBER                varchar2(200),
  constraint LOCATION_RECONCILIATIONS_PK primary key (LOCATION_RECONCILIATION_OID) using index);

create table LOCATION_PROD_TRANSLATION (
  LOCATION_PROD_TRANSLATION_OID   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  CONVERT_FROM_PRODUCT_OID        varchar2(200),
  CONVERT_TO_PRODUCT_OID          varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  constraint LOCATION_PROD_TRANSLATION_PK primary key (LOCATION_PROD_TRAN_OID) using index);

create table LOCATION_PRODUCT_TOTALS (
  LOCATION_PRODUCT_TOTAL_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  constraint LOCATION_PRODUCT_TOTALS_PK primary key (LOCATION_PRODUCT_TOTAL_OID) using index);

create table LINE_ITEM_COMPONENTS (
  LINE_ITEM_COMPONENT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  LINE_NUMBER                     varchar2(200),
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  UNIT_COST                       varchar2(200),
  RATE                            varchar2(200),
  UNIT_TO_QTY_VALUE               varchar2(200),
  COMPONENT_CATEGORY_CID          varchar2(200),
  REBATE_PROFILE_OID              varchar2(200),
  REBATE_OID                      varchar2(200),
  PROFILE_CRITERIA_CID            varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  constraint LINE_ITEM_COMPONENTS_PK primary key (LINE_ITEM_COMPONENT_OID) using index);

create table LETTER_VARIABLES (
  LETTER_VARIABLE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_PROMPT_REQUIRED              varchar2(200),
  VARIABLE_ID                     varchar2(200),
  DESCRIPTION                     varchar2(200),
  LETTER_FIELD_TYPE_CID           varchar2(200),
  FIELD_DISPLACEMENT              varchar2(200),
  constraint LETTER_VARIABLES_PK primary key (LETTER_VARIABLE_OID) using index);

create table LETTER_TEXT_LINES (
  LETTER_TEXT_LINE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LETTER_DEFINITION_OID           varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  TEXT_LINE                       varchar2(200),
  constraint LETTER_TEXT_LINES_PK primary key (LETTER_TEXT_LINE_OID) using index);

create table LETTER_TEMPLATES (
  LETTER_TEMPLATE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  PAPER_SIZES_CID                 varchar2(200),
  MARGIN_TOP                      varchar2(200),
  MARGIN_BOTTOM                   varchar2(200),
  MARGIN_LEFT                     varchar2(200),
  MARGIN_RIGHT                    varchar2(200),
  LETTER_FONT_OID                 varchar2(200),
  FONT_SIZE                       varchar2(200),
  PDF_TEMPLATE_PAGE               varchar2(200),
  constraint LETTER_TEMPLATES_PK primary key (LETTER_TEMPLATE_OID) using index);

create table LETTER_FONTS (
  LETTER_FONT_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  PATH_TO_FONT                    varchar2(200),
  IS_FONT_PDF_DEFAULT             varchar2(200),
  constraint LETTER_FONTS_PK primary key (LETTER_FONT_OID) using index);

create table LETTER_EXTRACTS (
  LETTER_EXTRACT_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_NO                  varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  NAME                            varchar2(200),
  CONTACT_NAME                    varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  SUBURB                          varchar2(200),
  POSTAL_CODE                     varchar2(200),
  STATE_SHORT_DESCRIPTION         varchar2(200),
  STATE_DESCRIPTION               varchar2(200),
  COUNTRY_DESCRIPTION             varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  constraint LETTER_EXTRACTS_PK primary key (LETTER_EXTRACT_OID) using index);

create table LETTER_DEFINITIONS (
  LETTER_DEFINITION_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  LETTER_DEFINITION_TYPE_CID      varchar2(200),
  DESCRIPTION                     varchar2(200),
  HEADER_DEFINITION_OID           varchar2(200),
  FOOTER_DEFINITION_OID           varchar2(200),
  CONTACT_HIERARCHY_CID           varchar2(200),
  LETTER_CODE                     varchar2(200),
  LETTER_TEMPLATE_OID             varchar2(200),
  constraint LETTER_DEFINITIONS_PK primary key (LETTER_DEFINITION_OID) using index);

create table LETTER_ASSIGN_VARIABLES (
  LETTER_ASSIGN_VARIABLE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LETTER_ASSIGNMENT_OID           varchar2(200),
  LETTER_VARIABLE_OID             varchar2(200),
  VARIABLE_VALUE                  varchar2(200),
  constraint LETTER_ASSIGN_VARIABLES_PK primary key (LETTER_ASSIGN_VARIABLE_OID) using index);

create table LETTER_ASSIGNMENTS (
  LETTER_ASSIGNMENT_OID           varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LETTER_DEFINITION_OID           varchar2(200),
  LETTER_PRINT_STATUS_CID         varchar2(200),
  SPOOLER_QUEUE_DID               varchar2(200),
  IS_HIGH_PRIORITY                varchar2(200),
  STATUS_AT                       varchar2(200),
  CREATED_AT                      varchar2(200),
  CREATED_BY                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  APPLICATION_OID                 varchar2(200),
  PRINTED_AT                      varchar2(200),
  FILE_NAME                       varchar2(200),
  IS_DUPLEX                       varchar2(200),
  NUM_OF_PAGES                    varchar2(200),
  LETTER_EXTRACT_OID              varchar2(200),
  constraint LETTER_ASSIGNMENTS_PK primary key (LETTER_ASSIGNMENT_OID) using index);

create table LANGUAGES (
  LANGUAGE_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LANGUAGE_CODE                   varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  CHAR_ENCODING                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  ISO_LANGUAGE_CODE               varchar2(200),
  constraint LANGUAGES_PK primary key (LANGUAGE_OID) using index);

create table IVR_REQUEST_LOGS (
  IVR_REQUEST_LOG_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VALIDATE_RECEIVED_AT            varchar2(200),
  CONFIRM_RECEIVED_AT             varchar2(200),
  REFERENCE                       varchar2(200),
  LOCATION_CODE                   varchar2(200),
  CARD_NO                         varchar2(200),
  EXPIRY_DATE                     varchar2(200),
  DATE_TIME_EFFECTIVE             varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  IVR_REQUEST_STATUS_CID          varchar2(200),
  PRODUCT_RESTRICTION_CODE        varchar2(200),
  RESPONSE_TEXT                   varchar2(200),
  VALID_CARD_FLAG                 varchar2(200),
  CONFIRMATION_FLAG               varchar2(200),
  VALIDATE_TIME                   varchar2(200),
  CONFIRM_TIME                    varchar2(200),
  constraint IVR_REQUEST_LOGS_PK primary key (IVR_REQUEST_LOG_OID) using index);

create table INTERNET_USER_PWD_LOGS (
  INTERNET_USER_PWD_LOG_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERNET_USER_OID               varchar2(200),
  PASSWORD                        varchar2(200),
  constraint INTERNET_USER_PWD_LOGS_PK primary key (INTERNET_USER_PWD_LOG_OID) using index);

create table INTERNET_USER_ACCESS_ALERTS (
  INTERNET_USER_ACCESS_ALERT_OID  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERNET_USER_ACCESS_OID        varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  IS_OPTED_IN                     varchar2(200),
  constraint INTERNET_USER_ACCESS_ALERTS_PK primary key (INTERNET_USER_ACCESS_ALERT_OID) using index);

create table INTERNET_USER_ACCESS (
  INTERNET_USER_ACCESS_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERNET_USER_OID               varchar2(200),
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  constraint INTERNET_USER_ACCESS_PK primary key (INTERNET_USER_ACCES_OID) using index);

create table INTERNET_USERS (
  INTERNET_USER_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  LOGON_STATUS_CID                varchar2(200),
  LAST_LOGGEDON_AT                varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  INTERNET_USER_TYPE_CID          varchar2(200),
  LOGON_ID                        varchar2(200),
  FAILED_LOGON_COUNT              varchar2(200),
  NAME                            varchar2(200),
  PASSWORD_CREATED_AT             varchar2(200),
  PASSWORD                        varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  IS_CHANGE_PASSW_ON_NXT_LOGON    varchar2(200),
  PRIVATE_QUESTION_OID            varchar2(200),
  PRIVATE_ANSWER                  varchar2(200),
  FAILED_ANSWER                   varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  PARENT_INTERNET_USER_OID        varchar2(200),
  IS_USER_AGREE_MANDATORY_PAGE    varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  OLS_LAST_AGREED_ON              date,
  constraint INTERNET_USERS_PK primary key (INTERNET_USER_OID) using index);

create table INTERNET_ROLES_PERMISSIONS_ORG (
  INTERNET_ROLE_PERMISSION_OID    varchar2(200),
  INTERNET_ROLE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERNET_PERMISSION_OID         varchar2(200),
  constraint INTERNET_ROLES_PERMISSIONS_ORG_PK primary key (INTERNET_ROLES_PERMISSION_OID) using index);

create table INTERNET_ROLES_PERMISSIONS (
  INTERNET_ROLE_PERMISSION_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  INTERNET_PERMISSION_OID         varchar2(200),
  ACCESS_LEVEL_CID                varchar2(200),
  constraint INTERNET_ROLES_PERMISSIONS_PK primary key (INTERNET_ROLES_PERMISSION_OID) using index);

create table INTERNET_PERMISSIONS (
  INTERNET_PERMISSION_OID         varchar2(200),
  PERMISSION_NM                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CATEGORY                        varchar2(200),
  LABEL                           varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint INTERNET_PERMISSIONS_PK primary key (INTERNET_PERMISSION_OID) using index);

create table INTERNET_ACCESS_GROUPS (
  INTERNET_ACCESS_GROUP_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCESS_MEMBER_TYPE_CID          varchar2(200),
  IS_PASSWORD_EXPIRY_FORCED       varchar2(200),
  PASSWORD_EXPIRY_DAYS            varchar2(200),
  DESCRIPTION                     varchar2(200),
  LOCKOUT_INACTIVITY_DAYS         varchar2(200),
  MAX_PASSWORD_ATTEMPTS           varchar2(200),
  UNIQUE_PASSWORD_RETAINED        varchar2(200),
  LOCK_USER_ON_ACCT_CLOSE         varchar2(200),
  IS_LOCKED_OUT                   varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  EXTERNAL_NAME                   varchar2(200),
  constraint INTERNET_ACCESS_GROUPS_PK primary key (INTERNET_ACCESS_GROUP_OID) using index);

create table INTERF_FILE_INC_LOG_ROWS (
  INTERF_FILE_INC_LOG_ROWS_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_INC_LOG_OID      varchar2(200),
  ROW_IN_ERROR                    varchar2(200),
  ERROR_MESSAGE                   varchar2(200),
  constraint INTERF_FILE_INC_LOG_ROWS_PK primary key (INTERF_FILE_INC_LOG_ROW_OID) using index);

create table INTERFACE_FILE_PARAMS (
  INTERFACE_FILE_PARAM_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_OID              varchar2(200),
  PARAM_NAME                      varchar2(200),
  PARAM_VALUE                     varchar2(200),
  constraint INTERFACE_FILE_PARAMS_PK primary key (INTERFACE_FILE_PARAM_OID) using index);

create table INTERFACE_FILE_LOG_PARAMS (
  INTERFACE_FILE_LOG_PARAM_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_LOG_OID          varchar2(200),
  PARAM_NAME                      varchar2(200),
  PARAM_VALUE                     varchar2(200),
  constraint INTERFACE_FILE_LOG_PARAMS_PK primary key (INTERFACE_FILE_LOG_PARAM_OID) using index);

create table INTERFACE_FILE_LOGS (
  INTERFACE_FILE_LOG_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_OID              varchar2(200),
  OUTPUT_FILE_PATH                varchar2(200),
  SEQUENCE_USED                   varchar2(200),
  OUTPUT_FILE_TYPE_CID            varchar2(200),
  DEFINITION_FILE_NAME            varchar2(200),
  DELIMITER                       varchar2(200),
  OUTPUT_FILE_COMPRESS_TYPE_CID   varchar2(200),
  IS_COMPRESSED                   varchar2(200),
  STARTED_AT                      varchar2(200),
  ENDED_AT                        varchar2(200),
  ROWS_ADDED                      varchar2(200),
  IS_IN_ERROR                     varchar2(200),
  ERROR_MESSAGE                   varchar2(200),
  LAST_ROW_SEQUENCE_USED          varchar2(200),
  FILE_SIZE                       varchar2(200),
  COMPRESSED_FILE_SIZE            varchar2(200),
  PROCESSING_DATE                 date,
  constraint INTERFACE_FILE_LOGS_PK primary key (INTERFACE_FILE_LOG_OID) using index);

create table INTERFACE_FILE_INC_LOGS (
  INTERFACE_FILE_INC_LOG_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_INCOMING_OID     varchar2(200),
  STARTED_AT                      varchar2(200),
  ENDED_AT                        varchar2(200),
  ROWS_RECEIVED                   varchar2(200),
  IS_IN_ERROR                     varchar2(200),
  ERROR_MESSAGE                   varchar2(200),
  SEQUENCE_NUMBER                 varchar2(200),
  FILE_CREATED_AT                 varchar2(200),
  INCOMING_FILE_NAME              varchar2(200),
  PROCESSING_DATE                 date,
  EXTERNAL_FILE_NAME              varchar2(200),
  constraint INTERFACE_FILE_INC_LOGS_PK primary key (INTERFACE_FILE_INC_LOG_OID) using index);

create table INTERFACE_FILES_INCOMING (
  INTERFACE_FILE_INCOMING_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_INCOMING_ID      varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  LAST_SEQUENCE_NUMBER            varchar2(200),
  LAST_RUN_AT                     varchar2(200),
  INPUT_FILE_NAME_PREFIX          varchar2(200),
  MAXIMUM_RECORDS                 varchar2(200),
  LAST_PROCESS_DATE               date,
  BATCH_SIZE                      varchar2(200),
  INPUT_FILE_FOLDER_NAME          varchar2(200),
  SEQUENCE_NUMBER_TEMPLATE        varchar2(200),
  DATA_MANAGER_NAME               varchar2(200),
  DATA_TRANSFER_OBJECT_NAME       varchar2(200),
  constraint INTERFACE_FILES_INCOMING_PK primary key (INTERFACE_FILE_OID) using index);

create table INTERFACE_FILES (
  INTERFACE_FILE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INTERFACE_FILE_ID               varchar2(200),
  CLIENT_MID                      varchar2(200),
  LAST_SEQUENCE_USED              varchar2(200),
  DEFINITION_FILE_NAME            varchar2(200),
  OUTPUT_FILE_NAME_TEMPLATE       varchar2(200),
  DELIMITER                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  OUTPUT_FILE_TYPE_CID            varchar2(200),
  OUTPUT_FILE_COMPRESS_TYPE_CID   varchar2(200),
  IS_COMPRESSED                   varchar2(200),
  LAST_ROW_SEQUENCE_USED          varchar2(200),
  IS_ROW_SEQUENCE_USED            varchar2(200),
  LAST_RUN_AT                     varchar2(200),
  LAST_PROCESS_DATE               date,
  IS_GENERATE_NULL_FILE           varchar2(200),
  MAXIMUM_RECORDS                 varchar2(200),
  DATA_MANAGER_NAME               varchar2(200),
  DATA_TRANSFER_OBJECT_NAME       varchar2(200),
  constraint INTERFACE_FILES_PK primary key (INTERFACE_FILE_OID) using index);

create table INTEREST_RATES (
  INTEREST_RATE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  RATE_CODE_OID                   varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  constraint INTEREST_RATES_PK primary key (INTEREST_RATE_OID) using index);

create table INET_HOST_CLIENT_MAP (
  INET_HOST_CLIENT_MAP_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint INET_HOST_CLIENT_MAP_PK primary key (INET_HO_OID) using index);

create table INDUSTRIES (
  INDUSTRY_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INDUSTRY_CODE                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint INDUSTRIES_PK primary key (INDUSTRIE_OID) using index);

create table IFO_AUDIT_TABLES (
  IFO_AUDIT_TABLE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUDIT_CATEGORY_TABLE_OID        varchar2(200),
  INTERFACE_FILE_OID              varchar2(200),
  constraint IFO_AUDIT_TABLES_PK primary key (IFO_AUDIT_TABLE_OID) using index);

create table IFO_AUDIT_COL_EXCLUSIONS (
  IFO_AUDIT_COLUMN_EXCL_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  USER_ID                         varchar2(200),
  IFO_AUDIT_COLUMN_OID            varchar2(200),
  constraint IFO_AUDIT_COL_EXCLUSIONS_PK primary key (IFO_AUDIT_COL_EXCLUSION_OID) using index);

create table IFO_AUDIT_COLUMNS (
  IFO_AUDIT_COLUMN_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUDIT_CATEGORY_COLUMN_OID       varchar2(200),
  IFO_AUDIT_TABLE_OID             varchar2(200),
  constraint IFO_AUDIT_COLUMNS_PK primary key (IFO_AUDIT_COLUMN_OID) using index);

create table IE_SERVICE_LOCATIONS (
  IE_SERVICE_LOCATION_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CODE                            varchar2(200),
  URL                             varchar2(200),
  constraint IE_SERVICE_LOCATIONS_PK primary key (IE_SERVICE_LOCATION_OID) using index);

create table HOLDING_TRANSACTIONS (
  HOLDING_TRANSACTION_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  SUNDRY_TYPE_CID                 varchar2(200),
  SUNDRY_STATUS_CID               varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  AMOUNT                          varchar2(200),
  TAX_TOTAL                       varchar2(200),
  PROCESSED_BY                    varchar2(200),
  REFERENCE                       varchar2(200),
  PROCESSED_ON                    date,
  EFFECTIVE_ON                    date,
  MERCHANT_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_OID                        varchar2(200),
  PRODUCT_OID                     varchar2(200),
  APPROVED_BY                     varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  SHORT_NOTE                      varchar2(200),
  ADJUSTMENT_TYPE_OID             varchar2(200),
  REQUEST_TRANS_TYPE_OID          varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  TAX_RATE                        varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  ORIG_TRANSACTION_OID            varchar2(200),
  constraint HOLDING_TRANSACTIONS_PK primary key (HOLDING_TRANSACTION_OID) using index);

create table HIERARCHIES (
  HIERARCHY_OID                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  OWNING_MEMBER_OID               varchar2(200),
  DESCRIPTION                     varchar2(200),
  IS_MERCH_AGREEEMNT_AND_REPORTS  varchar2(200),
  IS_LOCATION                     varchar2(200),
  IS_CUSTOMER_REPORTS             varchar2(200),
  IS_CUSTOMER_FINANCIAL           varchar2(200),
  IS_PERIOD_REBATE                varchar2(200),
  constraint HIERARCHIES_PK primary key (HIERARCHIE_OID) using index);

create table GL_TRANS_WORKING_DATA (
  TRANSACTION_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint GL_TRANS_WORKING_DATA_PK primary key (GL_TRAN_OID) using index);

create table GL_TOTALS (
  GL_TOTAL_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROCESSED_ON                    date,
  GL_COMPANY_DID                  varchar2(200),
  GL_TOTAL_CATEGORY_DID           varchar2(200),
  AMOUNT                          varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint GL_TOTALS_PK primary key (GL_TOTAL_OID) using index);

create table GL_SUMMARY_WORKING_DATA (
  GL_SUMMARY_WORKING_DATA_OID     varchar2(200),
  IS_PROCESSED                    varchar2(200),
  GL_EXTRACT_LEVEL                varchar2(200),
  IS_CUSTOMER                     varchar2(200),
  IS_GL_BY_MEMBER                 varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  PROCESSED_AT                    varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TRANSACTION_CATEGORY_CID        varchar2(200),
  CLIENT_MID                      varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  GL_COST_CENTRE_CODE             varchar2(200),
  FEE_TOTAL_TAX                   varchar2(200),
  FEE_TOTAL_AMOUNT                varchar2(200),
  AMOUNT                          varchar2(200),
  REBATE_TOTAL                    varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  ORIGINAL_VALUE                  varchar2(200),
  CUSTOMER_VALUE                  varchar2(200),
  MERCHANT_VALUE                  varchar2(200),
  CUSTOMER_TAX_AMOUNT             varchar2(200),
  MERCHANT_TAX_AMOUNT             varchar2(200),
  UNIT_TO_QTY_VALUE               varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  AGREEMENT_ACCOUNT_OID           varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  GL_REGION_DID                   varchar2(200),
  PRODUCT_OID                     varchar2(200),
  UNIT_QUANTITY                   varchar2(200),
  BATCH_NO                        varchar2(200),
  constraint GL_SUMMARY_WORKING_DATA_PK primary key (GL__OID) using index);

create table GL_SUMMARIES (
  GL_SUMMARY_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  GL_VALUE_SOURCE_CID             varchar2(200),
  AMOUNT                          varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  PROCESSED_ON                    date,
  GL_CHANNEL_DID                  varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  TRANSACTION_GROUP_CID           varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  GL_MERC_ITEM_TYPE_CID           varchar2(200),
  GL_TRANS_ITEM_TYPE_CID          varchar2(200),
  GL_COST_CENTRE_CODE             varchar2(200),
  DESCRIPTION                     varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  GL_REGION_DID                   varchar2(200),
  PROCESSED_AT                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  UNIT_QUANTITY                   varchar2(200),
  BATCH_NO                        varchar2(200),
  constraint GL_SUMMARIES_PK primary key (GL_SUMMARIE_OID) using index);

create table GL_PROFITCOST_CENTRE_MAPS (
  GL_PROFITCOST_CENTRE_MAP_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  GL_ACCOUNT_NUMBER               varchar2(200),
  CLIENT_MID                      varchar2(200),
  GL_ACCOUNT_DESCRIPTION          varchar2(200),
  PROFITCOST_MAPPING_TYPE_CID     varchar2(200),
  constraint GL_PROFITCOST_CENTRE_MAPS_PK primary key (GL_PROFITCOST_CENTRE_MAP_OID) using index);

create table GL_LOC_REC_WORKING_DATA (
  LOCATION_RECONCILIATION_OID     varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint GL_LOC_REC_WORKING_DATA_PK primary key (GL_LOC_REC_WORKING_DATA) using index);

create table GL_INVOICE_NUMBER_RANGES (
  GL_INVOICE_NUMBER_RANGE_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  GL_DESCRIPTION                  varchar2(200),
  SEQUENCE_NUMBER_START           varchar2(200),
  SEQUENCE_NUMBER_END             varchar2(200),
  SEQUENCE_NUMBER_CURRENT         varchar2(200),
  constraint GL_INVOICE_NUMBER_RANGES_PK primary key (GL_INVOICE_NUMBER_RANGE_OID) using index);

create table GL_INVOICE_NUMBER_ASSIGNMENTS (
  GL_INVOICE_NUMBER_OID           varchar2(200),
  GL_EXTR_LEDGER_DETAIL_OID       varchar2(200),
  constraint GL_INVOICE_NUMBER_ASSIGNMENTS_PK primary key (GL_INVOICE_NUMBER_ASSIGNMENT_OID) using index);

create table GL_INVOICE_NUMBERS (
  GL_INVOICE_NUMBER_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  GL_INVOICE_NUMBER_RANGE_OID     varchar2(200),
  GL_INVOICE_NUMBER               varchar2(200),
  REFERENCE_DESCRIPTION           varchar2(200),
  GL_EXTRACT_DATE                 date,
  HAS_BEEN_POSTED                 varchar2(200),
  constraint GL_INVOICE_NUMBERS_PK primary key (GL_INVOICE_NUMBER_OID) using index);

create table GL_EXTR_LEDGER_WORKING_DATA (
  CLIENT_MID                      varchar2(200),
  GL_ACCOUNT_NUMBER_OID           varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  GL_SUMMARY_OID                  varchar2(200),
  GL_COMPANY_DID                  varchar2(200),
  GL_REGION_DID                   varchar2(200),
  GL_REGION_CODE                  varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  GL_CHANNEL_CODE                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  GL_TOTAL_CATEGORY_DID           varchar2(200),
  AMOUNT                          varchar2(200),
  TAX_AMOUNT                      varchar2(200),
  DEBIT_COMPANY_CODE              varchar2(200),
  DEBIT_ACCOUNT_NUMBER            varchar2(200),
  DEBIT_COST_CENTRE_CODE          varchar2(200),
  DEBIT_DESCRIPTION               varchar2(200),
  CREDIT_COMPANY_CODE             varchar2(200),
  CREDIT_ACCOUNT_NUMBER           varchar2(200),
  CREDIT_COST_CENTRE_CODE         varchar2(200),
  CREDIT_DESCRIPTION              varchar2(200),
  PROCESSED_ON                    date,
  DESCRIPTION                     varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  PRODUCT_OID                     varchar2(200),
  BATCH_NO                        varchar2(200),
  constraint GL_EXTR_LEDGER_WORKING_DATA_PK primary key (GL_EXTR_LEDGER_WORKING_DATA) using index);

create table GL_EXTR_LEDGER_DETAILS (
  GL_EXTR_LEDGER_DETAIL_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  GL_SUMMARY_OID                  varchar2(200),
  AMOUNT                          varchar2(200),
  GL_REGION_CODE                  varchar2(200),
  GL_CHANNEL_CODE                 varchar2(200),
  DEBIT_COMPANY_CODE              varchar2(200),
  DEBIT_ACCOUNT_NUMBER            varchar2(200),
  DEBIT_COST_CENTRE_CODE          varchar2(200),
  DEBIT_DESCRIPTION               varchar2(200),
  CREDIT_COMPANY_CODE             varchar2(200),
  CREDIT_ACCOUNT_NUMBER           varchar2(200),
  CREDIT_COST_CENTRE_CODE         varchar2(200),
  CREDIT_DESCRIPTION              varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  GL_ACCOUNT_NUMBER_OID           varchar2(200),
  CLIENT_MID                      varchar2(200),
  PROCESSED_ON                    date,
  PRODUCT_OID                     varchar2(200),
  BATCH_NO                        varchar2(200),
  constraint GL_EXTR_LEDGER_DETAILS_PK primary key (GL_EXTR_LEDGER_DETAIL_OID) using index);

create table GL_ACCOUNT_NUMBERS (
  GL_ACCOUNT_NUMBER_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  GL_VALUE_SOURCE_CID             varchar2(200),
  GL_COMPANY_DID                  varchar2(200),
  GL_REGION_DID                   varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  GL_TOTAL_CATEGORY_DID           varchar2(200),
  DESCRIPTION                     varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  TRANSACTION_GROUP_CID           varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  GL_MERC_ITEM_TYPE_CID           varchar2(200),
  GL_TRANS_ITEM_TYPE_CID          varchar2(200),
  DEBIT_COMPANY_CODE              varchar2(200),
  DEBIT_COST_CENTRE_CODE          varchar2(200),
  DEBIT_DESCRIPTION               varchar2(200),
  CREDIT_COMPANY_CODE             varchar2(200),
  CREDIT_COST_CENTRE_CODE         varchar2(200),
  CREDIT_DESCRIPTION              varchar2(200),
  CLIENT_MID                      varchar2(200),
  DEBIT_ACCOUNT_NUMBER_OID        varchar2(200),
  CREDIT_ACCOUNT_NUMBER_OID       varchar2(200),
  TIMING_OF_EXTRACT_CID           varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  IS_CREDIT_EXTRACT_PRODUCT       varchar2(200),
  IS_DEBIT_EXTRACT_PRODUCT        varchar2(200),
  BANKING_ACCOUNT_MAP_OID         varchar2(200),
  IS_CREDIT_POS_TRNX_BATCHES      varchar2(200),
  IS_DEBIT_POS_TRNX_BATCHES       varchar2(200),
  constraint GL_ACCOUNT_NUMBERS_PK primary key (GL_ACCOUNT_NUMBER_OID) using index);

create table GL_ACCOUNT_CODES (
  GL_ACCOUNT_CODE_OID             varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_CLASS_CID           varchar2(200),
  ACCOUNT_CODE                    varchar2(200),
  DESCRIPTION                     varchar2(200),
  PRODUCT_OID                     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint GL_ACCOUNT_CODES_PK primary key (GL_ACCOUNT_CODE_OID) using index);

create table GEOGRAPHIC_AREA_TYPES (
  GEOGRAPHIC_AREA_TYPE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  IS_MANDATORY                    varchar2(200),
  GEOGRAPHIC_AREA_NO              varchar2(200),
  DESCRIPTION                     varchar2(200),
  SCREEN_LITERAL                  varchar2(200),
  constraint GEOGRAPHIC_AREA_TYPES_PK primary key (GEOGRAPHIC_AREA_TYPE_OID) using index);

create table GEOGRAPHIC_AREAS (
  GEOGRAPHIC_AREA_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  GEOGRAPHIC_AREA_TYPE_OID        varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint GEOGRAPHIC_AREAS_PK primary key (GEOGRAPHIC_AREA_OID) using index);

create table GENERATED_PAYMENTS (
  GENERATED_PAYMENT_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PAYMENT_METHOD_CID              varchar2(200),
  CREATED_ON                      date,
  ENTERED_ON                      date,
  AMOUNT                          varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  MERCHANT_AGREEMENT_OID          varchar2(200),
  REFERENCE                       varchar2(200),
  STATUS_TYPE_CID                 varchar2(200),
  PREVIOUS_GENERATED_PAYMENT_OID  varchar2(200),
  RESCHEDULE_REQUEST_ON           date,
  STATUS_AT                       date,
  DUE_ON                          date,
  constraint GENERATED_PAYMENTS_PK primary key (GENERATED_PAYMENT_OID) using index);

create table FREQUENCIES (
  FREQUENCY_OID                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  FREQUENCY_TYPE_CID              varchar2(200),
  REPEAT_EVERY_DAYS               varchar2(200),
  REPEAT_EVERY_WEEKS              varchar2(200),
  REPEAT_EVERY_MONTHS             varchar2(200),
  TIMES_APPLIED                   varchar2(200),
  USE_ACCOUNT_BILLING_FREQUENCY   varchar2(200),
  USE_IFCS_END_OF_MONTH           varchar2(200),
  USE_IFCS_END_OF_DAY             varchar2(200),
  USE_IFCS_BANKING_DAY            varchar2(200),
  USE_ON_PHYSICAL_END_OF_MONTH    varchar2(200),
  IS_IMMEDIATE                    varchar2(200),
  IS_IFCS_END_OF_MONTH            varchar2(200),
  IS_IFCS_END_OF_DAY              varchar2(200),
  IS_IFCS_BANKING_DAY             varchar2(200),
  USE_ON_MONDAY                   varchar2(200),
  USE_ON_TUESDAY                  varchar2(200),
  USE_ON_WEDNESDAY                varchar2(200),
  USE_ON_THURSDAY                 varchar2(200),
  USE_ON_FRIDAY                   varchar2(200),
  USE_ON_SATURDAY                 varchar2(200),
  USE_ON_SUNDAY                   varchar2(200),
  USE_ON_DAY_01                   varchar2(200),
  USE_ON_DAY_02                   varchar2(200),
  USE_ON_DAY_03                   varchar2(200),
  USE_ON_DAY_04                   varchar2(200),
  USE_ON_DAY_05                   varchar2(200),
  USE_ON_DAY_06                   varchar2(200),
  USE_ON_DAY_07                   varchar2(200),
  USE_ON_DAY_08                   varchar2(200),
  USE_ON_DAY_09                   varchar2(200),
  USE_ON_DAY_10                   varchar2(200),
  USE_ON_DAY_11                   varchar2(200),
  USE_ON_DAY_12                   varchar2(200),
  USE_ON_DAY_13                   varchar2(200),
  USE_ON_DAY_14                   varchar2(200),
  USE_ON_DAY_15                   varchar2(200),
  USE_ON_DAY_16                   varchar2(200),
  USE_ON_DAY_17                   varchar2(200),
  USE_ON_DAY_18                   varchar2(200),
  USE_ON_DAY_19                   varchar2(200),
  USE_ON_DAY_20                   varchar2(200),
  USE_ON_DAY_21                   varchar2(200),
  USE_ON_DAY_22                   varchar2(200),
  USE_ON_DAY_23                   varchar2(200),
  USE_ON_DAY_24                   varchar2(200),
  USE_ON_DAY_25                   varchar2(200),
  USE_ON_DAY_26                   varchar2(200),
  USE_ON_DAY_27                   varchar2(200),
  USE_ON_DAY_28                   varchar2(200),
  USE_ON_DAY_29                   varchar2(200),
  USE_ON_DAY_30                   varchar2(200),
  USE_ON_DAY_31                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  USE_CUST_FIN_YEAR_END           varchar2(200),
  USE_ON_MONTH_01                 varchar2(200),
  USE_ON_MONTH_02                 varchar2(200),
  USE_ON_MONTH_03                 varchar2(200),
  USE_ON_MONTH_04                 varchar2(200),
  USE_ON_MONTH_05                 varchar2(200),
  USE_ON_MONTH_06                 varchar2(200),
  USE_ON_MONTH_07                 varchar2(200),
  USE_ON_MONTH_08                 varchar2(200),
  USE_ON_MONTH_09                 varchar2(200),
  USE_ON_MONTH_10                 varchar2(200),
  USE_ON_MONTH_11                 varchar2(200),
  USE_ON_MONTH_12                 varchar2(200),
  constraint FREQUENCIES_PK primary key (FREQUENCIE_OID) using index);

create table FEE_VALUES (
  FEE_VALUE_OID                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  FEE_EFFECTIVE_DATE_OID          varchar2(200),
  DESCRIPTION                     varchar2(200),
  VALUE_RANGE_FROM                varchar2(200),
  VALUE_RANGE_TO                  varchar2(200),
  RATE                            varchar2(200),
  COUNTRY_OID                     varchar2(200),
  STATE_OID                       varchar2(200),
  constraint FEE_VALUES_PK primary key (FEE_VALUE_OID) using index);

create table FEE_PROFILES (
  FEE_PROFILE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  APPLICATION_OID                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_OID                        varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  MERCHANT_MID                    varchar2(200),
  constraint FEE_PROFILES_PK primary key (FEE_PROFILE_OID) using index);

create table FEE_EFFECTIVE_DATES (
  FEE_EFFECTIVE_DATE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  FEE_OID                         varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  MINIMUM_CHARGE                  varchar2(200),
  MAXIMUM_CHARGE                  varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint FEE_EFFECTIVE_DATES_PK primary key (FEE_EFFECTIVE_DATE_OID) using index);

create table FEE_CONTROLS (
  FEE_CONTROL_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  FEE_TYPE_CID                    varchar2(200),
  FEE_OID                         varchar2(200),
  INITIAL_DELAY_MONTHS            varchar2(200),
  IS_FEE_CHARGED                  varchar2(200),
  MSF_DID                         varchar2(200),
  APPLY_MONTHS                    varchar2(200),
  constraint FEE_CONTROLS_PK primary key (FEE_CONTROL_OID) using index);

create table FEES (
  FEE_OID                         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PRODUCT_OID                     varchar2(200),
  FEE_TYPE_CID                    varchar2(200),
  FEE_CALCULATION_TYPE_CID        varchar2(200),
  FEE_POSTING_METHOD_CID          varchar2(200),
  FREQUENCY_OID                   varchar2(200),
  INITIAL_DELAY_MONTHS            varchar2(200),
  DESCRIPTION                     varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  FEE_PERIOD_MONTHS               varchar2(200),
  IS_RESERVE_FEE                  varchar2(200),
  APPLY_MONTHS                    varchar2(200),
  constraint FEES_PK primary key (FEE_OID) using index);

create table FAL_TABLES_METADATA (
  FAL_TABLES_METADATA_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  COLUMN_NAME                     varchar2(200),
  DATA_LENGTH                     varchar2(200),
  IS_MANDATORY                    varchar2(200),
  IS_PRIMARY_KEY                  varchar2(200),
  DEFAULT_VALUE                   varchar2(200),
  UNIQUE_INDEX_NAME               varchar2(200),
  FOREIGN_TABLE_NAME              varchar2(200),
  FOREIGN_COLUMN_NAME             varchar2(200),
  IS_CASE_SENSITIVE               varchar2(200),
  DATA_TYPE                       varchar2(200),
  constraint FAL_TABLES_METADATA_PK primary key (FAL_TABLE_OID) using index);

create table FAL_TABLES (
  FAL_TABLE_OID                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  IS_CACHED                       varchar2(200),
  IS_AUDITABLE                    varchar2(200),
  AUDIT_CATEGORY_CID              varchar2(200),
  SIZING_CATEGORY                 varchar2(200),
  SIZING_RATE                     varchar2(200),
  AVG_ROW_LEN                     varchar2(200),
  TABLE_TYPE_CODE                 varchar2(200),
  IS_HOST_AUTH_TABLE              varchar2(200),
  constraint FAL_TABLES_PK primary key (FAL_TABLE_OID) using index);

create table FAL_DEBUG_LOGS (
  FAL_DEBUG_LOG_OID               varchar2(200),
  LOGGED_AT                       varchar2(200),
  LOGGED_BY                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  FIELD_NUMBER                    varchar2(200),
  FIELD_DATE                      date,
  FIELD_STRING                    varchar2(200),
  constraint FAL_DEBUG_LOGS_PK primary key (FAL_DEBUG_LOG_OID) using index);

create table EXT_VELOCITY_CTRL_PROF (
  EXT_VELOCITY_CTRL_PROF_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  VELOCITY_TYPE_VALUE_1_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_2_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_3_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_4_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_5_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_6_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_7_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_8_OID       varchar2(200),
  CLIENT_MID                      varchar2(200),
  PROFILE_NUMBER                  varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint EXT_VELOCITY_CTRL_PROF_PK primary key (EXT_VELOCITY_CTRL_PROF) using index);

create table EXTERNAL_CLIENT_IDS (
  EXTERNAL_CLIENT_ID_OID          varchar2(200),
  CLIENT_MID                      varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint EXTERNAL_CLIENT_IDS_PK primary key (EXTERNAL_CLIENT_ID_OID) using index);

create table EXCHANGE_RATES (
  EXCHANGE_RATE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CURRENCY_FROM_OID               varchar2(200),
  CURRENCY_TO_OID                 varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  EXCHANGE_RATE                   varchar2(200),
  constraint EXCHANGE_RATES_PK primary key (EXCHANGE_RATE_OID) using index);

create table EVENT_REQUESTS (
  EVENT_REQUEST_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  EVENT_TYPE_CID                  varchar2(200),
  PROCESS_STATUS_CID              varchar2(200),
  ACTION_ON                       date,
  RECORD_OID                      varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint EVENT_REQUESTS_PK primary key (EVENT_REQUEST_OID) using index);

create table ENCRYPTED_PINS (
  ENCRYPTED_PIN_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ENCRYPTED_PIN                   varchar2(200),
  CARD_OID                        varchar2(200),
  DRIVER_OID                      varchar2(200),
  constraint ENCRYPTED_PINS_PK primary key (ENCRYPTED_PIN_OID) using index);

create table ENCODING_PROMPTS (
  ENCODING_PROMPT_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  ENCODING_TYPE_CID               varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint ENCODING_PROMPTS_PK primary key (ENCODING_PROMPT_OID) using index);

create table EMISSION_VALUES (
  EMISSION_VALUE_OID              varchar2(200),
  EMISSION_TYPE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ECI                             varchar2(200),
  CO2                             varchar2(200),
  CH4                             varchar2(200),
  N2O                             varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  constraint EMISSION_VALUES_PK primary key (EMISSION_VALUE_OID) using index);

create table EMISSION_TYPES (
  EMISSION_TYPE_OID               varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint EMISSION_TYPES_PK primary key (EMISSION_TYPE_OID) using index);

create table DUNNING_CONTROLS (
  DUNNING_CONTROL_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  DUNNING_CODE_POSITION_CID       varchar2(200),
  THRESHOLD_VALUE_FROM            varchar2(200),
  THRESHOLD_VALUE_TO              varchar2(200),
  DUNNING_CODE_DIGIT              varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  constraint DUNNING_CONTROLS_PK primary key (DUNNING_CONTROL_OID) using index);

create table DUNNING_CODES (
  DUNNING_CODE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  DUNNING_CODE                    varchar2(200),
  THRESHOLD_VALUE_1               varchar2(200),
  THRESHOLD_VALUE_2               varchar2(200),
  THRESHOLD_VALUE_3               varchar2(200),
  THRESHOLD_VALUE_4               varchar2(200),
  DUNNING_CONTROL_1_DESCR         varchar2(200),
  DUNNING_CONTROL_2_DESCR         varchar2(200),
  DUNNING_CONTROL_3_DESCR         varchar2(200),
  DUNNING_CONTROL_4_DESCR         varchar2(200),
  DESCRIPTION                     varchar2(200),
  STATEMENT_MESSAGE_OID           varchar2(200),
  constraint DUNNING_CODES_PK primary key (DUNNING_CODE_OID) using index);

create table DRIVER_PRODUCT_TOTALS (
  DRIVER_PRODUCT_TOTAL_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DRIVER_OID                      varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  constraint DRIVER_PRODUCT_TOTALS_PK primary key (DRIVER_PRODUCT_TOTAL_OID) using index);

create table DRIVERS (
  DRIVER_OID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  DRIVER_STATUS_CID               varchar2(200),
  DRIVER_NAME                     varchar2(200),
  IS_PRINT_OPTION                 varchar2(200),
  PIN_OFFSET                      varchar2(200),
  CARD_OID                        varchar2(200),
  DRIVER_ID                       varchar2(200),
  SHORT_NAME                      varchar2(200),
  constraint DRIVERS_PK primary key (DRIVER_OID) using index);

create table DISTRICTS_ZONES (
  DISTRICT_ZONE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DISTRICT_ID                     varchar2(200),
  ZONE_TYPE_ID                    varchar2(200),
  ZONE_TYPE_MNEMONIC              varchar2(200),
  ZONE_ID                         varchar2(200),
  ZONE_MNEMONIC                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  EXPIRES_ON                      date,
  constraint DISTRICTS_ZONES_PK primary key (DISTRICTS_ZONE_OID) using index);

create table DISTRIBUTOR_REFERENCES (
  DISTRIBUTOR_REFERENCE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DISTRIBUTOR_ID                  varchar2(200),
  REFERENCE_NUMBER                varchar2(200),
  REFERENCE_NAME                  varchar2(200),
  DISTR_REF_STATUS_CID            varchar2(200),
  constraint DISTRIBUTOR_REFERENCES_PK primary key (DISTRIBUTOR_REFERENCE_OID) using index);

create table DISPUTE_REASONS (
  DISPUTE_REASON_OID              varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  constraint DISPUTE_REASONS_PK primary key (DISPUTE_REASON_OID) using index);

create table DISPUTED_TRANSACTIONS (
  DISPUTED_TRANSACTION_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  DISPUTE_STATUS_CID              varchar2(200),
  DISPUTE_REASON_OID              varchar2(200),
  CREATED_ON                      date,
  CREATED_BY                      varchar2(200),
  BRING_UP_ON                     date,
  RFCO_REQUESTED_COUNT            varchar2(200),
  RFCO_REQUESTED_AT               varchar2(200),
  CLOSED_ON                       date,
  CLOSED_BY                       varchar2(200),
  constraint DISPUTED_TRANSACTIONS_PK primary key (DISPUTED_TRANSACTION_OID) using index);

create table DIARY_NOTES (
  DIARY_NOTE_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DIARY_NOTE_OWNER_CID            varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  CREATED_AT                      varchar2(200),
  BRING_UP_AT                     varchar2(200),
  SHORT_NOTE                      varchar2(200),
  DETAILED_NOTE                   varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_OID                        varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  MERCHANT_MID                    varchar2(200),
  WORK_QUEUE_ACTION_ITEM_OID      varchar2(200),
  USER_OID                        varchar2(200),
  DISPUTED_TRANSACTION_OID        varchar2(200),
  IS_HIGH_PRIORITY                varchar2(200),
  constraint DIARY_NOTES_PK primary key (DIARY_NOTE_OID) using index);

create table DETAIL_GROUP_TYPES (
  DETAIL_GROUP_TYPE_OID           varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_A_DEBIT_GROUP                varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  DETAIL_GROUP_CATEGORY_CID       varchar2(200),
  constraint DETAIL_GROUP_TYPES_PK primary key (DETAIL_GROUP_TYPE_OID) using index);

create table DETAIL_GROUP_TRANS (
  TRANSACTION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DETAIL_GROUP_OID                varchar2(200),
  constraint DETAIL_GROUP_TRANS_PK primary key (DETAIL_GROUP_TRAN_OID) using index);

create table DETAIL_GROUPS (
  DETAIL_GROUP_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CURRENCY_OID                    varchar2(200),
  COUNTRY_OID                     varchar2(200),
  DETAIL_GROUP_TYPE_OID           varchar2(200),
  DETAIL_GROUP_STATUS_CID         varchar2(200),
  AGING_BUCKET_OID                varchar2(200),
  ORIGINAL_DEBIT_AMOUNT           varchar2(200),
  ORIGINAL_CREDIT_AMOUNT          varchar2(200),
  HOST_DEBIT_AMOUNT               varchar2(200),
  HOST_CREDIT_AMOUNT              varchar2(200),
  PAYMENT_REQUEST_OID             varchar2(200),
  PAID_IN_FULL_ON                 date,
  REMITTANCE_ID                   varchar2(200),
  HOST_DEBIT_VOLUME               varchar2(200),
  HOST_CREDIT_VOLUME              varchar2(200),
  BILLED_ON                       date,
  constraint DETAIL_GROUPS_PK primary key (DETAIL_GROUP_OID) using index);

create table DESCRIPTION_TYPES (
  DESCRIPTION_TYPE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  constraint DESCRIPTION_TYPES_PK primary key (DESCRIPTION_TYPE_OID) using index);

create table DESCRIPTIONS (
  DESCRIPTION_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION_TYPE_OID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint DESCRIPTIONS_PK primary key (DESCRIPTION_OID) using index);

create table DEPRC_CARD_PROGRAM_CARD_TYPES (
  CARD_PROGRAM_CARD_TYPE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  INITIAL_CARD_STATUS_OID         varchar2(200),
  MANUAL_CARD_STATUS_OID          varchar2(200),
  NUMBERING_SEQUENCE_OID          varchar2(200),
  EMBOSS_ENCODING_RULE_CID        varchar2(200),
  EMBOSS_NAME_ACTION_CID          varchar2(200),
  CARD_NO_ACTION_CID              varchar2(200),
  EXPIRES_ON_ACTION_CID           varchar2(200),
  CARD_PRODUCT_ACTION_CID         varchar2(200),
  PRODUCT_RESTRICT_ACTION_CID     varchar2(200),
  TIME_LIMITS_ACTION_CID          varchar2(200),
  EMBOSS_TEXT_ACTION_CID          varchar2(200),
  PIN_OFFSET_ACTION_CID           varchar2(200),
  DRIVER_NAME_ACTION_CID          varchar2(200),
  DRIVER_ID_ACTION_CID            varchar2(200),
  VEHICLE_DESCRIPTION_ACTION_CID  varchar2(200),
  VEHICLE_ID_ACTION_CID           varchar2(200),
  LICENSE_PLATE_ACTION_CID        varchar2(200),
  VELOCITY_TYPE_1_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_2_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_3_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_4_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_5_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_6_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_7_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_8_ACTION_CID      varchar2(200),
  EXPIRY_MONTHS                   varchar2(200),
  CARD_ISO_PREFIX                 varchar2(200),
  CARD_PIN_ISO_PREFIX             varchar2(200),
  EXTERNAL_REF_CID                varchar2(200),
  MAXIMUM_EXPIRY_MONTHS           varchar2(200),
  UPDATE_EXPIRY                   varchar2(200),
  REQ_SIGN_ACTION_CID             varchar2(200),
  REQ_PIN_ACTION_CID              varchar2(200),
  REQ_FLEET_ID_ACTION_CID         varchar2(200),
  REQ_ODOMETER_ACTION_CID         varchar2(200),
  REQ_ORDER_NUM_ACTION_CID        varchar2(200),
  IS_INCL_BULK_CARD_SHEET         varchar2(200),
  CARD_INITIAL_STATUS_OVERRIDE    varchar2(200),
  LOCATION_RESTRICT_ACTION_CID    varchar2(200),
  AUTHORIZATION_LEVEL_CID         varchar2(200),
  CLIENT_PVK_OID                  varchar2(200),
  PVKI                            varchar2(200),
  IS_DEFAULT_PVKI_USED            varchar2(200),
  PUMP_CONTROL_ACTION_CID         varchar2(200),
  SPEEDPASS_INDICATOR             varchar2(200),
  constraint DEPRC_CARD_PROGRAM_CARD_TYPES_PK primary key (DEPRC_CARD_PROGRAM_CARD_TYPE_OID) using index);

create table DEFAULT_PRICING_PROFILE (
  DEFAULT_PRICING_PROFILE_OID     varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_PRODUCT_GROUP_OID          varchar2(200),
  MERCH_AGRMNT_VALUE_OID          varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  PRICING_PROFILE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COUPON_CODE                     varchar2(200),
  APPLICATION_OID                 varchar2(200),
  constraint DEFAULT_PRICING_PROFILE_PK primary key (DEFAULT_PRICING_PROFILE) using index);

create table DATABASE_INFO (
  PROJECT_ID                      varchar2(200),
  VERSION                         varchar2(200),
  LAST_UPGRADED_AT                varchar2(200),
  constraint DATABASE_INFO_PK primary key (DATABA_OID) using index);

create table DATABASECHANGELOGLOCK (
  ID                              varchar2(200),
  LOCKED                          varchar2(200),
  LOCKGRANTED                     varchar2(200),
  LOCKEDBY                        varchar2(200),
  constraint DATABASECHANGELOGLOCK_PK primary key (DATABA_OID) using index);

create table DATABASECHANGELOG (
  ID                              varchar2(200),
  AUTHOR                          varchar2(200),
  FILENAME                        varchar2(200),
  DATEEXECUTED                    varchar2(200),
  MD5SUM                          varchar2(200),
  DESCRIPTION                     varchar2(200),
  COMMENTS                        varchar2(200),
  TAG                             varchar2(200),
  LIQUIBASE                       varchar2(200),
  constraint DATABASECHANGELOG_PK primary key (DATABA_OID) using index);

create table CUST_CARD_TEMPLATE_FIELDS (
  CUST_CARD_TEMPLATE_FIELD_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_CARD_TEMPLATE_OID      varchar2(200),
  BULK_CARD_COLUMN_OID            varchar2(200),
  VALUE                           varchar2(200),
  constraint CUST_CARD_TEMPLATE_FIELDS_PK primary key (CUST_CARD_TEMPLATE_FIELD_OID) using index);

create table CUSTOMER_VAS_OFFERINGS (
  CUSTOMER_VAS_OFFERING_OID       varchar2(200),
  CLIENT_MID                      varchar2(200),
  EFFECTIVE_DATE                  date,
  EXPIRY_ON                       date,
  PRODUCT_OID                     varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint CUSTOMER_VAS_OFFERINGS_PK primary key (CUSTOMER_VAS_OFFERING_OID) using index);

create table CUSTOMER_VALUE_TYPES (
  CUSTOMER_VALUE_TYPE_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  IS_MANDATORY                    varchar2(200),
  CUSTOMER_VALUE_NO               varchar2(200),
  DESCRIPTION                     varchar2(200),
  SCREEN_LITERAL                  varchar2(200),
  constraint CUSTOMER_VALUE_TYPES_PK primary key (CUSTOMER_VALUE_TYPE_OID) using index);

create table CUSTOMER_VALUES (
  CUSTOMER_VALUE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_VALUE_TYPE_OID         varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint CUSTOMER_VALUES_PK primary key (CUSTOMER_VALUE_OID) using index);

create table CUSTOMER_REPORT_LEVELS (
  REPORT_LEVEL_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONTACT_NAME                    varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  LEVEL_NUMBER                    varchar2(200),
  LEVEL_CODE                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  ADDRESS_OID                     varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  constraint CUSTOMER_REPORT_LEVELS_PK primary key (CUSTOMER_REPORT_LEVEL_OID) using index);

create table CUSTOMER_PRODUCT_TOT_LOCS (
  CUSTOMER_PROD_TOT_LOC_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  MTD_FEE_AMOUNT                  varchar2(200),
  MTD_FEE_TAX                     varchar2(200),
  YTD_FEE_AMOUNT                  varchar2(200),
  YTD_FEE_TAX                     varchar2(200),
  LTD_FEE_AMOUNT                  varchar2(200),
  LTD_FEE_TAX                     varchar2(200),
  LMTH_FEE_AMOUNT                 varchar2(200),
  LMTH_FEE_TAX                    varchar2(200),
  TRANS_REBATE_TYPE_CID           varchar2(200),
  constraint CUSTOMER_PRODUCT_TOT_LOCS_PK primary key (CUSTOMER_PRODUCT_TOT_LOC_OID) using index);

create table CUSTOMER_PRODUCT_TOTALS (
  CUSTOMER_PRODUCT_TOTAL_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  TRANS_REBATE_TYPE_CID           varchar2(200),
  MTD_FEE_AMOUNT                  varchar2(200),
  MTD_FEE_TAX                     varchar2(200),
  YTD_FEE_AMOUNT                  varchar2(200),
  YTD_FEE_TAX                     varchar2(200),
  LTD_FEE_AMOUNT                  varchar2(200),
  LTD_FEE_TAX                     varchar2(200),
  LMTH_FEE_AMOUNT                 varchar2(200),
  LMTH_FEE_TAX                    varchar2(200),
  constraint CUSTOMER_PRODUCT_TOTALS_PK primary key (CUSTOMER_PRODUCT_TOTAL_OID) using index);

create table CUSTOMER_PRICING_LOGS (
  CUSTOMER_PRICING_LOG_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  PRICING_PROFILE_OID             varchar2(200),
  IS_REPRICING_REQUIRED           varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  MERCH_AGRMNT_VALUE_OID          varchar2(200),
  PRODUCT_GROUP_OID               varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_PRODUCT_GROUP_OID          varchar2(200),
  constraint CUSTOMER_PRICING_LOGS_PK primary key (CUSTOMER_PRICING_LOG_OID) using index);

create table CUSTOMER_COST_CENTRES (
  CUSTOMER_COST_CENTRE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CUSTOMER_COST_CENTRE_CODE       varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint CUSTOMER_COST_CENTRES_PK primary key (CUSTOMER_COST_CENTRE_OID) using index);

create table CUSTOMER_CARD_TEMPLATES (
  CUSTOMER_CARD_TEMPLATE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  BULK_CARD_TEMPLATE_OID          varchar2(200),
  NAME                            varchar2(200),
  DESCRIPTION                     varchar2(200),
  APPROVER_COMMENTS               varchar2(200),
  APPROVER                        varchar2(200),
  constraint CUSTOMER_CARD_TEMPLATES_PK primary key (CUSTOMER_CARD_TEMPLATE_OID) using index);

create table CUSTOMER_CARD_PROFILES (
  CUSTOMER_CARD_PROFILE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_CONTROL_PROFILE_OID        varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  constraint CUSTOMER_CARD_PROFILES_PK primary key (CUSTOMER_CARD_PROFILE_OID) using index);

create table CUSTOMER_CARD_PRODUCTS (
  CUSTOMER_CARD_PRODUCT_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  CARD_INITIAL_STATUS_OVERRIDE    varchar2(200),
  INITIAL_CARD_STATUS_OID         varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  EXT_EMBOSSER_CUSTOMER_REF       varchar2(200),
  IS_CARD_REQ_VALIDATION_RULE     varchar2(200),
  constraint CUSTOMER_CARD_PRODUCTS_PK primary key (CUSTOMER_CARD_PRODUCT_OID) using index);

create table CUSTOMER_ACCNT_PROFILES (
  CUSTOMER_ACCNT_PROFILE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_CONTROL_PROFILE_OID     varchar2(200),
  constraint CUSTOMER_ACCNT_PROFILES_PK primary key (CUSTOMER_ACCNT_PROFILE_OID) using index);

create table CURRENCIES (
  CURRENCY_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  CURRENCY_SYMBOL                 varchar2(200),
  CURRENCY_PRECISION              varchar2(200),
  CURRENCY_ALIGNMENT_CID          varchar2(200),
  ISO_CURRENCY_NO                 varchar2(200),
  constraint CURRENCIES_PK primary key (CURRENCIE_OID) using index);

create table CREDIT_PLANS (
  CREDIT_PLAN_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  ACCOUNTING_TYPE_CID             varchar2(200),
  DESCRIPTION                     varchar2(200),
  IS_AUTO_LOCKABLE                varchar2(200),
  GRACE_DAYS                      varchar2(200),
  INTEREST_CHARGING_METHOD_CID    varchar2(200),
  INTEREST_RATE_OID               varchar2(200),
  LIMIT_THRESHOLD_PERCENTAGE      varchar2(200),
  LIMIT_THRESHOLD_AMOUNT          varchar2(200),
  IS_SEND_CLIENT_THRESHOLD_ALERT  varchar2(200),
  CALCULATION_TYPE_CID            varchar2(200),
  MINIMUM_FEE_THRESHOLD           varchar2(200),
  constraint CREDIT_PLANS_PK primary key (CREDIT_PLAN_OID) using index);

create table CREDIT_CARDS (
  CREDIT_CARD_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_NAME                       varchar2(200),
  CARD_TOKEN_NUMBER               varchar2(200),
  LAST_FOUR_DIGITS                varchar2(200),
  CREDIT_CARD_TYPE_CID            varchar2(200),
  CARD_EXPIRES_ON                 date,
  REFERENCE_NUMBER                varchar2(200),
  APPLICATION_OID                 varchar2(200),
  constraint CREDIT_CARDS_PK primary key (CREDIT_CARD_OID) using index);

create table COUNTRIES (
  COUNTRY_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CURRENCY_OID                    varchar2(200),
  DESCRIPTION                     varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  constraint COUNTRIES_PK primary key (COUNTRIE_OID) using index);

create table COST_CENTRE_VEHICLE_TOTS (
  COST_CENTRE_VEHICLE_TOT_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COST_CENTRE_OID                 varchar2(200),
  VEHICLE_OID                     varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  constraint COST_CENTRE_VEHICLE_TOTS_PK primary key (COST_CENTRE_VEHICLE_TOT_OID) using index);

create table COST_CENTRE_DRIVER_TOTS (
  COST_CENTRE_DRIVER_TOT_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COST_CENTRE_OID                 varchar2(200),
  DRIVER_OID                      varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  constraint COST_CENTRE_DRIVER_TOTS_PK primary key (COST_CENTRE_DRIVER_TOT_OID) using index);

create table COST_CENTRE_CARD_TOTALS (
  COST_CENTRE_CARD_TOTAL_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COST_CENTRE_OID                 varchar2(200),
  CARD_OID                        varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  MTD_FEE_AMOUNT                  varchar2(200),
  MTD_FEE_TAX                     varchar2(200),
  YTD_FEE_AMOUNT                  varchar2(200),
  YTD_FEE_TAX                     varchar2(200),
  LTD_FEE_AMOUNT                  varchar2(200),
  LTD_FEE_TAX                     varchar2(200),
  LMTH_FEE_AMOUNT                 varchar2(200),
  LMTH_FEE_TAX                    varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  constraint COST_CENTRE_CARD_TOTALS_PK primary key (COST_CENTRE_CARD_TOTAL_OID) using index);

create table COST_CENTRES (
  COST_CENTRE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_COST_CENTRE_OID        varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CARD_OID                        varchar2(200),
  DRIVER_OID                      varchar2(200),
  VEHICLE_OID                     varchar2(200),
  constraint COST_CENTRES_PK primary key (COST_CENTRE_OID) using index);

create table CONTACT_TYPES (
  CONTACT_TYPE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  IS_MULTIPLE_CONTACTS_ALLOWED    varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint CONTACT_TYPES_PK primary key (CONTACT_TYPE_OID) using index);

create table CONTACT_RANKINGS (
  CONTACT_RANKING_OID             varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONTACT_HIERARCHY_CID           varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  CONTACT_TYPE_OID                varchar2(200),
  constraint CONTACT_RANKINGS_PK primary key (CONTACT_RANKING_OID) using index);

create table CONTACTS (
  CONTACT_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  CONTACT_NAME                    varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  CONTACT_TYPE_OID                varchar2(200),
  IS_DEFAULT                      varchar2(200),
  constraint CONTACTS_PK primary key (CONTACT_OID) using index);

create table CONSTANT_TYPES (
  CONSTANT_TYPE_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  constraint CONSTANT_TYPES_PK primary key (CONSTANT_TYPE_OID) using index);

create table CONSTANT_DESCRIPTIONS (
  CONSTANT_DESCRIPTION_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONSTANT_OID                    varchar2(200),
  DESCRIPTION_OID                 varchar2(200),
  constraint CONSTANT_DESCRIPTIONS_PK primary key (CONSTANT_DESCRIPTION_OID) using index);

create table CONSTANTS (
  CONSTANT_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONSTANT_TYPE_OID               varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  constraint CONSTANTS_PK primary key (CONSTANT_OID) using index);

create table COMPONENT_VALUES (
  COMPONENT_VALUE_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  COMPONENT_TYPE_OID              varchar2(200),
  PRODUCT_OID                     varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  EXPIRES_AT                      varchar2(200),
  VALUE                           varchar2(200),
  MERCHANT_CLIENT_MID             varchar2(200),
  COUNTRY_OID                     varchar2(200),
  STATE_OID                       varchar2(200),
  LOCATION_MID                    varchar2(200),
  GEOGRAPHIC_AREA_1_OID           varchar2(200),
  GEOGRAPHIC_AREA_2_OID           varchar2(200),
  GEOGRAPHIC_AREA_3_OID           varchar2(200),
  GEOGRAPHIC_AREA_4_OID           varchar2(200),
  GEOGRAPHIC_AREA_5_OID           varchar2(200),
  MERCH_AGRMNT_VALUE_1_OID        varchar2(200),
  MERCH_AGRMNT_VALUE_2_OID        varchar2(200),
  MERCH_AGRMNT_VALUE_3_OID        varchar2(200),
  CUSTOMER_VALUE_1_OID            varchar2(200),
  CUSTOMER_VALUE_2_OID            varchar2(200),
  CUSTOMER_VALUE_3_OID            varchar2(200),
  constraint COMPONENT_VALUES_PK primary key (COMPONENT_VALUE_OID) using index);

create table COMPONENT_TYPES (
  COMPONENT_TYPE_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  COMPONENT_SOURCE_CID            varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  IS_BY_COUNTRY                   varchar2(200),
  IS_BY_STATE                     varchar2(200),
  IS_BY_LOCATION                  varchar2(200),
  IS_BY_GEOGRAPHIC_AREA_1         varchar2(200),
  IS_BY_GEOGRAPHIC_AREA_2         varchar2(200),
  IS_BY_GEOGRAPHIC_AREA_3         varchar2(200),
  IS_BY_GEOGRAPHIC_AREA_4         varchar2(200),
  IS_BY_GEOGRAPHIC_AREA_5         varchar2(200),
  IS_MAINTAINED_INTERNALLY        varchar2(200),
  IS_ZERO_VALID                   varchar2(200),
  IS_OPTIONAL                     varchar2(200),
  VALUE_LOW_LIMIT                 varchar2(200),
  VALUE_HIGH_LIMIT                varchar2(200),
  COMPONENT_CATEGORY_CID          varchar2(200),
  IS_BY_MERCH_AGRMNT_VALUE_1      varchar2(200),
  IS_BY_MERCH_AGRMNT_VALUE_2      varchar2(200),
  IS_BY_MERCH_AGRMNT_VALUE_3      varchar2(200),
  IS_BY_CUSTOMER_VALUE_1          varchar2(200),
  IS_BY_CUSTOMER_VALUE_2          varchar2(200),
  IS_BY_CUSTOMER_VALUE_3          varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  IS_USE_PRODUCT_GROUP            varchar2(200),
  IS_USE_CLIENT_TAX_RATE          varchar2(200),
  constraint COMPONENT_TYPES_PK primary key (COMPONENT_TYPE_OID) using index);

create table COLLECTION_EVENTS (
  COLLECTION_EVENT_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COLLECTION_STATUS_CID           varchar2(200),
  COLLECTION_EVENT_ACTION_CID     varchar2(200),
  CREATED_AT                      varchar2(200),
  STATUS_SET_AT                   varchar2(200),
  HAS_BEEN_PROCESSED              varchar2(200),
  DESCRIPTION                     varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  DISPUTED_TRANSACTION_OID        varchar2(200),
  constraint COLLECTION_EVENTS_PK primary key (COLLECTION_EVENT_OID) using index);

create table CLIENT_TRANS_TYPES (
  CLIENT_TRANS_TYPE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXTERNAL_TRANS_GROUPING_DID     varchar2(200),
  constraint CLIENT_TRANS_TYPES_PK primary key (CLIENT_TRANS_TYPE_OID) using index);

create table CLIENT_TRANS_FEES (
  CLIENT_TRANS_FEE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  FEE_OID                         varchar2(200),
  MSF_DID                         varchar2(200),
  constraint CLIENT_TRANS_FEES_PK primary key (CLIENT_TRANS_FEE_OID) using index);

create table CLIENT_TRANS_DETAIL_GRPS (
  CLIENT_TRANS_DETAIL_GRP_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  DETAIL_GROUP_TYPE_OID           varchar2(200),
  constraint CLIENT_TRANS_DETAIL_GRPS_PK primary key (CLIENT_TRANS_DETAIL_GRP_OID) using index);

create table CLIENT_RETENTION_PERIODS (
  RETENTION_PERIODS_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  RETENTION_PERIODS_TYPE_CID      varchar2(200),
  RETENTION_PERIODS_VALUE         varchar2(200),
  constraint CLIENT_RETENTION_PERIODS_PK primary key (CLIENT_RETENTION_PERIOD_OID) using index);

create table CLIENT_PVK (
  CLIENT_PVK_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PVK                             varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint CLIENT_PVK_PK primary key (CLIENT_PVK) using index);

create table CLIENT_GROUP_LANGUAGES (
  CLIENT_GROUP_LANGUAGE_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  constraint CLIENT_GROUP_LANGUAGES_PK primary key (CLIENT_GROUP_LANGUAGE_OID) using index);

create table CLIENT_DATA_DEPENDENCIES (
  CLIENT_DATA_DEPENDENCY_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_DATA_CONTROL_OID         varchar2(200),
  PARENT_CLIENT_DATA_CONTROL_OID  varchar2(200),
  constraint CLIENT_DATA_DEPENDENCIES_PK primary key (CLIENT_DATA_DEPENDENCIE_OID) using index);

create table CLIENT_DATA_CONTROLS (
  CLIENT_DATA_CONTROL_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONTENT_TYPE                    varchar2(200),
  TABLE_NAME                      varchar2(200),
  IS_USED_BY_INTERNET             varchar2(200),
  VERSION                         varchar2(200),
  FILTERED_BY                     varchar2(200),
  IS_CACHED_EAGERLY               varchar2(200),
  constraint CLIENT_DATA_CONTROLS_PK primary key (CLIENT_DATA_CONTROL_OID) using index);

create table CLIENT_COUNTRIES (
  CLIENT_COUNTRY_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  COUNTRY_PURPOSE_CID             varchar2(200),
  COUNTRY_OID                     varchar2(200),
  constraint CLIENT_COUNTRIES_PK primary key (CLIENT_COUNTRIE_OID) using index);

create table CLIENT_COLLECTN_SETTINGS (
  CLIENT_COLLECTN_SETTING_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CREDIT_LIMIT_THRESHOLD          varchar2(200),
  ACCOUNT_BALANCE_THRESHOLD       varchar2(200),
  INACTIVE_DAYS_THRESHOLD         varchar2(200),
  TRACK_DUNNING_POSITION_1        varchar2(200),
  TRACK_DUNNING_POSITION_2        varchar2(200),
  TRACK_DUNNING_POSITION_3        varchar2(200),
  TRACK_DUNNING_POSITION_4        varchar2(200),
  TRACK_DISPUTE_TRANSACTIONS      varchar2(200),
  OVERDUE_ACCOUNT_Q_OID           varchar2(200),
  OVERDUE_CLEARED_Q_OID           varchar2(200),
  CREDIT_WITHIN_THRESHOLD_Q_OID   varchar2(200),
  CREDIT_OVER_LIMIT_Q_OID         varchar2(200),
  CREDIT_LIMIT_EXC_CLEARED_Q_OID  varchar2(200),
  CREDIT_LIMIT_EXT_APPLIED_Q_OID  varchar2(200),
  CREDIT_LIMIT_EXT_EXPIRED_Q_OID  varchar2(200),
  CREDIT_LIMIT_UPDATED_Q_OID      varchar2(200),
  CREDIT_PERCENT_OVER_Q_OID       varchar2(200),
  ACCOUNT_AUTO_LOCKED_Q_OID       varchar2(200),
  ACCOUNT_AUTO_REINSTATED_Q_OID   varchar2(200),
  ACCOUNT_CREDIT_BALANCE_Q_OID    varchar2(200),
  ACCOUNT_STATUS_UPDATED_Q_OID    varchar2(200),
  ACCOUNT_BALANCE_THRESHLD_Q_OID  varchar2(200),
  ACCOUNT_INACTIVE_DAYS_Q_OID     varchar2(200),
  constraint CLIENT_COLLECTN_SETTINGS_PK primary key (CLIENT_COLLECTN_SETTING_OID) using index);

create table CLIENT_BANKING_DETAILS (
  CLIENT_BANKING_DETAIL_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  TOTAL_TYPE_CID                  varchar2(200),
  BANK_OID                        varchar2(200),
  IS_DEBIT                        varchar2(200),
  OFFSET_BANK_NO                  varchar2(200),
  OFFSET_BRANCH_NO                varchar2(200),
  OFFSET_ACCOUNT_NO               varchar2(200),
  constraint CLIENT_BANKING_DETAILS_PK primary key (CLIENT_BANKING_DETAIL_OID) using index);

create table CARD_VELOCITY_TOTALS (
  CARD_VELOCITY_TOTAL_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  VELOCITY_TYPE_OID               varchar2(200),
  TRANSACTION_MONTH               varchar2(200),
  COUNT_HARD_DECLINE              varchar2(200),
  COUNT_SOFT_DECLINE              varchar2(200),
  TOTAL_HOST_AUTH                 varchar2(200),
  TOTAL_COLLECT                   varchar2(200),
  TOTAL_OTHER                     varchar2(200),
  TRANSACTION_DATE                date,
  TRANSACTION_WEEK                varchar2(200),
  TRANSACTION_YEAR                varchar2(200),
  LAST_ACCUMULATED_ON             date,
  constraint CARD_VELOCITY_TOTALS_PK primary key (CARD_VELOCITY_TOTAL_OID) using index);

create table CARD_TRANSFER_REQUESTS (
  CARD_TRANSFER_REQUEST_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_TRANSFER_REQ_STATUS_CID    varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  REFERENCE                       varchar2(200),
  NEW_CUSTOMER_MID                varchar2(200),
  EFFECTIVE_ON                    date,
  NEW_COST_CENTRE_OID             varchar2(200),
  NEW_CARD_CONTROL_PROFILE_OID    varchar2(200),
  NEW_CARD_FEE_PROFILE_OID        varchar2(200),
  IS_CREATE_PRIVATE_PROFILE       varchar2(200),
  REQUESTED_PHONE                 varchar2(200),
  REQUESTED_BY                    varchar2(200),
  constraint CARD_TRANSFER_REQUESTS_PK primary key (CARD_TRANSFER_REQUEST_OID) using index);

create table CARD_TRANSFER_CARDS (
  CARD_TRANSFER_CARD_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_TRANSFER_REQUEST_OID       varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_TRANSFER_CARD_STATUS_CID   varchar2(200),
  constraint CARD_TRANSFER_CARDS_PK primary key (CARD_TRANSFER_CARD_OID) using index);

create table CARD_STATUS_OLS_MAPPING (
  CARD_STATUS_OLS_CS_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  CARD_STATUS_OLS_OID             varchar2(200),
  IS_STATUS_CHANGE                varchar2(200),
  constraint CARD_STATUS_OLS_MAPPING_PK primary key (CARD_STATUS_OL_OID) using index);

create table CARD_STATUS_OLS (
  CARD_STATUS_OLS_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint CARD_STATUS_OLS_PK primary key (CARD_STATUS_OL_OID) using index);

create table CARD_STATUS_LOGS (
  CARD_STATUS_LOG_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  EXPIRES_AT                      varchar2(200),
  constraint CARD_STATUS_LOGS_PK primary key (CARD_STATUS_LOG_OID) using index);

create table CARD_STATUS_CHG_LOGS (
  CARD_STATUS_CHG_LOG_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_STATUS_CHG_LOG_STATUS_CID  varchar2(200),
  OLD_CARD_STATUS_OID             varchar2(200),
  NEW_CARD_STATUS_OID             varchar2(200),
  STATUS_CHANGED_AT               varchar2(200),
  CARD_HOT_LIST_STATUS_CID        varchar2(200),
  constraint CARD_STATUS_CHG_LOGS_PK primary key (CARD_STATUS_CHG_LOG_OID) using index);

create table CARD_STATUS_CHANGES (
  CARD_STATUS_CHANGE_OID          varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  FROM_CARD_STATUS_OID            varchar2(200),
  TO_CARD_STATUS_OID              varchar2(200),
  CARD_STATUS_AUTO_CHANGE_CID     varchar2(200),
  IS_USED_BY_CARD_REMOTE          varchar2(200),
  ZEROISE_PIN_RETRIES             varchar2(200),
  NEGATIVE_FILE_ACTION            varchar2(200),
  POSITIVE_FILE_ACTION            varchar2(200),
  POS_ACTION                      varchar2(200),
  IS_ROLL_UP_BALANCE              varchar2(200),
  constraint CARD_STATUS_CHANGES_PK primary key (CARD_STATUS_CHANGE_OID) using index);

create table CARD_STATUS (
  CARD_STATUS_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_REISSUE_ACTION_CID         varchar2(200),
  IS_ACTIVE                       varchar2(200),
  IS_VALID                        varchar2(200),
  IS_HOST_AUTH_OK                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  POSITIVE_FILE_REASON            varchar2(200),
  IS_VALID_FOR_BULK_REISSUE       varchar2(200),
  RESPONSE_CODE                   varchar2(200),
  ROLL_UP_BALANCE                 varchar2(200),
  constraint CARD_STATUS_PK primary key (CARD_STATU_OID) using index);

create table CARD_REQUESTS (
  CARD_REQUEST_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_REQUEST_STATUS_CID         varchar2(200),
  REQUESTED_AT                    varchar2(200),
  IS_HIGH_PRIORITY                varchar2(200),
  IS_PIN_MAILER_ONLY_REQD         varchar2(200),
  IS_ISSUE_FEE_CHARGED            varchar2(200),
  ISSUED_BY                       varchar2(200),
  constraint CARD_REQUESTS_PK primary key (CARD_REQUEST_OID) using index);

create table CARD_REISS_PROF_CONTROLS (
  CARD_REISS_PROF_CONTROL_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_REISSUE_PROFILE_OID        varchar2(200),
  IS_AUTO_REISSUE                 varchar2(200),
  REISSUE_CONFIRM_MONTHS          varchar2(200),
  REISSUE_ON_DAYS                 varchar2(200),
  REISSUE_ACTIVE_DAYS             varchar2(200),
  OVERRIDE_EXCLUSION_AT_REISSUE   varchar2(200),
  constraint CARD_REISS_PROF_CONTROLS_PK primary key (CARD_REISS_PROF_CONTROL_OID) using index);

create table CARD_REISSUE_PROFILES (
  CARD_REISSUE_PROFILE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  APPLICATION_OID                 varchar2(200),
  constraint CARD_REISSUE_PROFILES_PK primary key (CARD_REISSUE_PROFILE_OID) using index);

create table CARD_PROGRAM_TRANS_TOTS (
  CARD_PROGRAM_TRANS_TOT_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_DEBIT_TOTAL                 varchar2(200),
  MTD_CREDIT_TOTAL                varchar2(200),
  YTD_DEBIT_TOTAL                 varchar2(200),
  YTD_CREDIT_TOTAL                varchar2(200),
  LTD_DEBIT_TOTAL                 varchar2(200),
  LTD_CREDIT_TOTAL                varchar2(200),
  LMTH_DEBIT_TOTAL                varchar2(200),
  LMTH_CREDIT_TOTAL               varchar2(200),
  constraint CARD_PROGRAM_TRANS_TOTS_PK primary key (CARD_PROGRAM_TRANS_TOT_OID) using index);

create table CARD_PROGRAM_OFFERS (
  CARD_PROGRAM_OFFER_OID          varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint CARD_PROGRAM_OFFERS_PK primary key (CARD_PROGRAM_OFFER_OID) using index);

create table CARD_PROGRAMS (
  CARD_PROGRAM_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_NO_SEQUENCE_OID        varchar2(200),
  CARD_PROGRAM_ID                 varchar2(200),
  DESCRIPTION                     varchar2(200),
  PIN_OFFSET_MAX_LENGTH           varchar2(200),
  PIN_OFFSET_RESET_VALUE          varchar2(200),
  FUNDS_MGMT_LEVEL_CID            varchar2(200),
  IS_ALLOW_RESERVE_FEE            varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  BILLING_HIERARCHY_DEPTH_CID     varchar2(200),
  ACCOUNTING_TYPE_CID             varchar2(200),
  IS_REFUND_PROCESSING            varchar2(200),
  REFUND_THRESHOLD                varchar2(200),
  IS_AUTH_BILLING_DIFF_LVLS       varchar2(200),
  CUSTOMER_PIN_CONTROL_CID        varchar2(200),
  X_CARD_PIN_CONTROL_CID          varchar2(200),
  X_DRIVER_PIN_CONTROL_CID        varchar2(200),
  X_IS_PIN_MAILER_ON_REISSUE      varchar2(200),
  X_IS_PIN_MLR_WHEN_NO_PIN_CHNGE  varchar2(200),
  FUNDS_TRANSFER_METHOD_CID       varchar2(200),
  HIERARCHY_CONTROL_CID           varchar2(200),
  constraint CARD_PROGRAMS_PK primary key (CARD_PROGRAM_OID) using index);

create table CARD_PRODUCT_TOTALS (
  CARD_PRODUCT_TOTAL_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  PRODUCT_OID                     varchar2(200),
  TOTAL_CATEGORY_CID              varchar2(200),
  MTD_QUANTITY                    varchar2(200),
  MTD_VALUE                       varchar2(200),
  MTD_ORIGINAL_VALUE              varchar2(200),
  MTD_TAX_VALUE                   varchar2(200),
  MTD_REBATE_VALUE                varchar2(200),
  YTD_QUANTITY                    varchar2(200),
  YTD_VALUE                       varchar2(200),
  YTD_ORIGINAL_VALUE              varchar2(200),
  YTD_TAX_VALUE                   varchar2(200),
  YTD_REBATE_VALUE                varchar2(200),
  LTD_QUANTITY                    varchar2(200),
  LTD_VALUE                       varchar2(200),
  LTD_ORIGINAL_VALUE              varchar2(200),
  LTD_TAX_VALUE                   varchar2(200),
  LTD_REBATE_VALUE                varchar2(200),
  LMTH_QUANTITY                   varchar2(200),
  LMTH_VALUE                      varchar2(200),
  LMTH_ORIGINAL_VALUE             varchar2(200),
  LMTH_TAX_VALUE                  varchar2(200),
  LMTH_REBATE_VALUE               varchar2(200),
  MTD_FEE_AMOUNT                  varchar2(200),
  MTD_FEE_TAX                     varchar2(200),
  YTD_FEE_AMOUNT                  varchar2(200),
  YTD_FEE_TAX                     varchar2(200),
  LTD_FEE_AMOUNT                  varchar2(200),
  LTD_FEE_TAX                     varchar2(200),
  LMTH_FEE_AMOUNT                 varchar2(200),
  LMTH_FEE_TAX                    varchar2(200),
  constraint CARD_PRODUCT_TOTALS_PK primary key (CARD_PRODUCT_TOTAL_OID) using index);

create table CARD_PRODUCT_GRP_PRODUCTS (
  CARD_PRODUCT_GRP_PRODUCT_OID    varchar2(200),
  CARD_PRODUCT_GROUP_OID          varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint CARD_PRODUCT_GRP_PRODUCTS_PK primary key (CARD_PRODUCT_GRP_PRODUCT_OID) using index);

create table CARD_PRODUCT_GROUPS (
  CARD_PRODUCT_GROUP_OID          varchar2(200),
  DESCRIPTION                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint CARD_PRODUCT_GROUPS_PK primary key (CARD_PRODUCT_GROUP_OID) using index);

create table CARD_PRODUCTS (
  CARD_PRODUCT_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  INITIAL_LOAD_VALUE              varchar2(200),
  PLASTIC_CODE                    varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  CARD_GRAPHIC_OID                varchar2(200),
  IS_WRITEOFF_EXPIRED             varchar2(200),
  WRITEOFF_TRANSACTION_TYPE_OID   varchar2(200),
  IS_RELOADABLE                   varchar2(200),
  RELOAD_TRANSACTION_TYPE_OID     varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  CONTACTLESS_PAYMENT             varchar2(200),
  CARD_BALANCE_THRESHOLD          varchar2(200),
  SIGN_FUEL_ONLY                  varchar2(200),
  NO_SIGN_FUEL_ONLY               varchar2(200),
  SIGN_FUEL_AND_NON_FUEL          varchar2(200),
  NO_SIGN_FUEL_AND_NON_FUEL       varchar2(200),
  AUTHORIZATION_LEVEL_CID         varchar2(200),
  IS_INCL_BULK_CARD_SHEET         varchar2(200),
  CARD_BRAND_OID                  varchar2(200),
  EMBOSS_ENCODING_RULE_CID        varchar2(200),
  CARD_ISO_PREFIX                 varchar2(200),
  NUMBERING_SEQUENCE_OID          varchar2(200),
  EXPIRY_MONTHS                   varchar2(200),
  MAXIMUM_EXPIRY_MONTHS           varchar2(200),
  UPDATE_EXPIRY                   varchar2(200),
  INITIAL_CARD_STATUS_OID         varchar2(200),
  MANUAL_CARD_STATUS_OID          varchar2(200),
  CARD_INITIAL_STATUS_OVERRIDE    varchar2(200),
  CARD_PIN_ISO_PREFIX             varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PIN_CONTROL_CID            varchar2(200),
  DRIVER_PIN_CONTROL_CID          varchar2(200),
  PIN_OFFSET_MAX_LENGTH           varchar2(200),
  PIN_OFFSET_RESET_VALUE          varchar2(200),
  IS_PIN_MLR_WHEN_NO_PIN_CHNGE    varchar2(200),
  IS_PIN_MAILER_ON_REISSUE        varchar2(200),
  CLIENT_PVK_OID                  varchar2(200),
  PVKI                            varchar2(200),
  IS_DEFAULT_PVKI_USED            varchar2(200),
  EMBOSS_NAME_ACTION_CID          varchar2(200),
  CARD_NO_ACTION_CID              varchar2(200),
  EXPIRES_ON_ACTION_CID           varchar2(200),
  CARD_PRODUCT_ACTION_CID         varchar2(200),
  EMBOSS_TEXT_ACTION_CID          varchar2(200),
  PIN_OFFSET_ACTION_CID           varchar2(200),
  DRIVER_NAME_ACTION_CID          varchar2(200),
  DRIVER_ID_ACTION_CID            varchar2(200),
  VEHICLE_DESCRIPTION_ACTION_CID  varchar2(200),
  VEHICLE_ID_ACTION_CID           varchar2(200),
  LICENSE_PLATE_ACTION_CID        varchar2(200),
  EXTERNAL_REF_ACTION_CID         varchar2(200),
  PUMP_CONTROL_ACTION_CID         varchar2(200),
  PRODUCT_RESTRICT_ACTION_CID     varchar2(200),
  TIME_LIMITS_ACTION_CID          varchar2(200),
  VELOCITY_TYPE_1_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_2_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_3_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_4_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_5_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_6_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_7_ACTION_CID      varchar2(200),
  VELOCITY_TYPE_8_ACTION_CID      varchar2(200),
  REQ_SIGN_ACTION_CID             varchar2(200),
  REQ_PIN_ACTION_CID              varchar2(200),
  REQ_FLEET_ID_ACTION_CID         varchar2(200),
  REQ_ODOMETER_ACTION_CID         varchar2(200),
  REQ_ORDER_NUM_ACTION_CID        varchar2(200),
  LOCATION_RESTRICT_ACTION_CID    varchar2(200),
  VEHICLE_OPTION_CID              varchar2(200),
  GEO_COVER_OPTION_CID            varchar2(200),
  RECIPROCITY_OPTION_CID          varchar2(200),
  MAX_VEHICLES_ALLOWED            varchar2(200),
  OLS_ORDERING_ALLOWED            varchar2(200),
  constraint CARD_PRODUCTS_PK primary key (CARD_PRODUCT_OID) using index);

create table CARD_PREFIXES (
  CARD_PREFIX_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PREFIX                     varchar2(200),
  DESCRIPTION                     varchar2(200),
  DISPLAY_MASK                    varchar2(200),
  IS_SPACES_IN                    varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint CARD_PREFIXES_PK primary key (CARD_PREFIXE_OID) using index);

create table CARD_OFFER_RULES (
  CARD_OFFER_RULE_OID             varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  PRESENTATION_SEQUENCE           varchar2(200),
  CONDITION_TYPE_CID              varchar2(200),
  CALCULATION_TYPE_CID            varchar2(200),
  CALCULATION_TYPE_VALUE          varchar2(200),
  PERIOD_TYPE_CID                 varchar2(200),
  PERIOD_TYPE_VALUE               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint CARD_OFFER_RULES_PK primary key (CARD_OFFER_RULE_OID) using index);

create table CARD_OFFERS (
  CARD_OFFER_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  CONDITION_TYPE_CID              varchar2(200),
  CARD_BRAND_CID                  varchar2(200),
  constraint CARD_OFFERS_PK primary key (CARD_OFFER_OID) using index);

create table CARD_NUMBER_SERIES (
  CARD_NUMBER_SERIES_OID          varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  SERIES_NUMBER                   varchar2(200),
  SEQUENCE_NUMBER                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint CARD_NUMBER_SERIES_PK primary key (CARD_NUMBER_SERIE_OID) using index);

create table CARD_HOTLIST_LOGS (
  CARD_HOTLIST_LOG_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_OID                        varchar2(200),
  NEGFILE_ACTION_CODE             varchar2(200),
  NEGFILE_CREATED_AT              varchar2(200),
  NEGFILE_REFRESH_CODE            varchar2(200),
  NEGFILE_NAME                    varchar2(200),
  constraint CARD_HOTLIST_LOGS_PK primary key (CARD_HOTLIST_LOG_OID) using index);

create table CARD_GRAPHICS_PRODS (
  CARD_GRAPHICS_PROD_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_GRAPHIC_OID                varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint CARD_GRAPHICS_PRODS_PK primary key (CARD_GRAPHICS_PROD_OID) using index);

create table CARD_GRAPHICS (
  CARD_GRAPHIC_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_GRAPHIC_CODE               varchar2(200),
  DESCRIPTION                     varchar2(200),
  PRIORITY                        varchar2(200),
  GRAPHIC_CATEGORY_CID            varchar2(200),
  THRESHOLD_VALUE                 varchar2(200),
  constraint CARD_GRAPHICS_PK primary key (CARD_GRAPHIC_OID) using index);

create table CARD_CONTROL_PROFILES (
  CARD_CONTROL_PROFILE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  constraint CARD_CONTROL_PROFILES_PK primary key (CARD_CONTROL_PROFILE_OID) using index);

create table CARD_CONTROLS (
  CARD_CONTROL_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_CONTROL_PROFILE_OID        varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  IS_ERROR_PROD_REST_COLL         varchar2(200),
  IS_ERROR_PROD_REST_AUTH         varchar2(200),
  IS_ERROR_TIME_LIMT_COLL         varchar2(200),
  IS_ERROR_TIME_LIMT_AUTH         varchar2(200),
  VELOCITY_ASSIGNMENT_OID         varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  TIME_LIMIT_OID                  varchar2(200),
  LOCATION_RESTRICTION_OID        varchar2(200),
  IS_ERROR_LOC_RESTRIC_COLL       varchar2(200),
  IS_ERROR_LOC_RESTRIC_AUTH       varchar2(200),
  IS_VEHICLE_ID_REQ               varchar2(200),
  IS_DRIVER_ID_REQ                varchar2(200),
  IS_ORDER_NUMBER_REQ             varchar2(200),
  IS_FLEET_ID_REQ                 varchar2(200),
  IS_CUSTOMER_SELECTION_REQ       varchar2(200),
  IS_ODOMETER_REQ                 varchar2(200),
  IS_ODOMETER_VALIDATION          varchar2(200),
  EXT_VELOCITY_CTRL_PROF_OID      varchar2(200),
  LOYALTY_CARD_PROMPT             varchar2(200),
  constraint CARD_CONTROLS_PK primary key (CARD_CONTROL_OID) using index);

create table CARD_CONTACTS (
  CARD_CONTACT_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CONTACT_NAME                    varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  constraint CARD_CONTACTS_PK primary key (CARD_CONTACT_OID) using index);

create table CARD_BULK_REISSUES (
  CARD_BULK_REISSUE_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BULK_REISSUE_STATUS_CID         varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  FROM_EXPIRES_ON                 date,
  TO_EXPIRES_ON                   date,
  NEW_EXPIRES_ON                  date,
  REISSUE_ON                      date,
  CONFIRM_ON                      date,
  IS_HIGH_PRIORITY                varchar2(200),
  REQUESTED_BY                    varchar2(200),
  REQUESTED_PHONE                 varchar2(200),
  OVERRIDE_EXCLUSION_AT_REISSUE   varchar2(200),
  CREATED_ON                      date,
  IS_REPORT_SCHEDULED             varchar2(200),
  constraint CARD_BULK_REISSUES_PK primary key (CARD_BULK_REISSUE_OID) using index);

create table CARD_BRANDS (
  CARD_BRAND_OID                  varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  EMBOSSER_FILE_ID                varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint CARD_BRANDS_PK primary key (CARD_BRAND_OID) using index);

create table CARD_BALANCES (
  CARD_BALANCE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BALANCE                         varchar2(200),
  AUTHORISED_BALANCE              varchar2(200),
  constraint CARD_BALANCES_PK primary key (CARD_BALANCE_OID) using index);

create table CARD_ALLOCATION_ORDERS (
  CARD_ALLOCATION_ORDER_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  EXPIRY_DATE                     date,
  NUMBER_OF_CARDS                 varchar2(200),
  CARD_FEE_CHARGE                 varchar2(200),
  HIGH_PRIORITY                   varchar2(200),
  ORDER_DATE                      date,
  REQEST_STATUS_CID               varchar2(200),
  REQUESTED_BY                    varchar2(200),
  REQUESTED_BY_PHONE              varchar2(200),
  MANUAL_PRODUCTION               varchar2(200),
  FIRST_CARD_NO                   varchar2(200),
  LAST_CARD_NO                    varchar2(200),
  constraint CARD_ALLOCATION_ORDERS_PK primary key (CARD_ALLOCATION_ORDER_OID) using index);

create table CARDS_HSM (
  CARD_HSM_OID                    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_OID                        varchar2(200),
  HSM_SERVICE                     varchar2(200),
  HSM_SINGLE_REQUEST              varchar2(200),
  BATCH_ID                        varchar2(200),
  HSM_REQUEST_SENT                varchar2(200),
  HSM_RESPONSE_RECD               varchar2(200),
  HSM_RESPONSE                    varchar2(200),
  HSM_ERROR_DESC                  varchar2(200),
  ERROR_RECTIFIED                 varchar2(200),
  constraint CARDS_HSM_PK primary key (CARDS_H_OID) using index);

create table CARDS (
  CARD_OID                        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  CARD_CONTROL_PROFILE_OID        varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  CARD_NO                         varchar2(200),
  ISSUED_ON                       date,
  EXPIRES_ON                      date,
  CARD_STATUS_AT                  varchar2(200),
  PIN_RETRIES                     varchar2(200),
  IS_MANUALLY_PRODUCED            varchar2(200),
  EXCLUDE_FROM_BULK_REISSUE       varchar2(200),
  INITIAL_LOAD_VALUE              varchar2(200),
  TOTAL_SPEND                     varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  DRIVER_OID                      varchar2(200),
  VEHICLE_OID                     varchar2(200),
  PREVIOUS_CARD_OID               varchar2(200),
  REPLACE_CARD_OID                varchar2(200),
  CARD_CONTACT_OID                varchar2(200),
  FEE_PROFILE_ASSIGNED_ON         date,
  LAST_TRANSACTION_ON             date,
  PIN_OFFSET                      varchar2(200),
  EMBOSS_LINE_1                   varchar2(200),
  EMBOSS_LINE_2                   varchar2(200),
  EMBOSS_LINE_3                   varchar2(200),
  EMBOSS_LINE_4                   varchar2(200),
  EMBOSS_LINE_5                   varchar2(200),
  REQUESTED_BY                    varchar2(200),
  REQUESTED_PHONE                 varchar2(200),
  CVV_SECURITY_CODE               varchar2(200),
  ORDER_REFERENCE                 varchar2(200),
  ORDER_METHOD_DID                varchar2(200),
  EXTERNAL_REF                    varchar2(200),
  PERM_CARD_CONTACT_OID           varchar2(200),
  PUMP_CONTROL_OID                varchar2(200),
  LAST_TRIED                      date,
  TRIED_TIMES                     varchar2(200),
  REPORT_LEVEL1_OID               varchar2(200),
  REPORT_LEVEL2_OID               varchar2(200),
  REPORT_LEVEL3_OID               varchar2(200),
  REPLACED_AT                     varchar2(200),
  IS_RUC_PURCHASE_CARD            varchar2(200),
  IS_RUC_REPRINT_ONLY             varchar2(200),
  PIN_OFFSET_CHANGED_AT           varchar2(200),
  TAX_CREDIT                      varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  EMBOSSING_NAME                  varchar2(200),
  REPORT_EMAIL_ADDRESS            varchar2(200),
  NEW_EXPIRY_DATE_OVERRIDE        date,
  CVKI                            varchar2(200),
  PVV                             varchar2(200),
  PVKI                            varchar2(200),
  CARD_BALANCE_OID                varchar2(200),
  PVV_CHANGED_AT                  varchar2(200),
  SCHEDULED_RESET_PIN_TRIES       varchar2(200),
  IS_BALANCE_ALLOWED              varchar2(200),
  LAST_EXPORTED_IN_CAF_AT         varchar2(200),
  CONTACTLESS_PAYMENT             varchar2(200),
  CONTACTLESS_PMT_MODIFIED_ON     date,
  ADDITIONAL_EMBOSS_DATA          varchar2(200),
  constraint CARDS_PK primary key (CARD_OID) using index);

create table CAPTURE_TYPES (
  CAPTURE_TYPE_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_SAMEDAY_SETTLED              varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  MANUAL_AUTH_CHECK               varchar2(200),
  constraint CAPTURE_TYPES_PK primary key (CAPTURE_TYPE_OID) using index);

create table CALENDAR (
  CALENDAR_OID                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROCESS_ON                      date,
  FREQUENCY_OID                   varchar2(200),
  constraint CALENDAR_PK primary key (CALENDAR) using index);

create table BULK_CARD_TEMPLATES (
  BULK_CARD_TEMPLATE_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  TEMPLATE_TYPE_CID               varchar2(200),
  PRIMARY_SHEET_COLOUR_CID        varchar2(200),
  SECONDARY_SHEET_COLOUR_CID      varchar2(200),
  constraint BULK_CARD_TEMPLATES_PK primary key (BULK_CARD_TEMPLATE_OID) using index);

create table BULK_CARD_HEADERS (
  BULK_CARD_HEADER_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  INTERNET_USER_OID               varchar2(200),
  ACTION_CID                      varchar2(200),
  ORDER_REFERENCE                 varchar2(200),
  STATUS_CID                      varchar2(200),
  RECEIVED_AT                     varchar2(200),
  PROCESSED_AT                    varchar2(200),
  RECEIVED_COUNT                  varchar2(200),
  SUCCESSFUL_ROWS                 varchar2(200),
  REJECTED_COUNT                  varchar2(200),
  FAIL_REASON                     varchar2(200),
  BULK_CARD_TEMPLATE_OID          varchar2(200),
  ISSUED_BY                       varchar2(200),
  constraint BULK_CARD_HEADERS_PK primary key (BULK_CARD_HEADER_OID) using index);

create table BULK_CARD_ERRORS (
  BULK_CARD_ERROR_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BULK_CARD_HEADER_OID            varchar2(200),
  STATUS_CID                      varchar2(200),
  BULK_CARD_DETAIL_OID            varchar2(200),
  FIELD_IN_ERROR                  varchar2(200),
  ERROR_MESSAGE                   varchar2(200),
  PARAM1                          varchar2(200),
  PARAM2                          varchar2(200),
  PARAM3                          varchar2(200),
  constraint BULK_CARD_ERRORS_PK primary key (BULK_CARD_ERROR_OID) using index);

create table BULK_CARD_DETAILS (
  BULK_CARD_DETAIL_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BULK_CARD_HEADER_OID            varchar2(200),
  STATUS_CID                      varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  FAILED_REASON                   varchar2(200),
  CARD_NO                         varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  NEW_CARD_STATUS_OID             varchar2(200),
  NEW_CARD_STATUS_EFFECTIVE_ON    date,
  EMBOSS_NAME                     varchar2(200),
  EXTERNAL_REF                    varchar2(200),
  PIN_OFFSET                      varchar2(200),
  VEHICLE_DESCRIPTION             varchar2(200),
  VEHICLE_ID                      varchar2(200),
  LICENSE_PLATE                   varchar2(200),
  DRIVER_NAME                     varchar2(200),
  DRIVER_ID                       varchar2(200),
  PUMP_CONTROL_OID                varchar2(200),
  REPORT_LEVEL1_CODE              varchar2(200),
  REPORT_LEVEL2_CODE              varchar2(200),
  REPORT_LEVEL3_CODE              varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  TIME_LIMIT_OID                  varchar2(200),
  LOCATION_RESTRICTION_OID        varchar2(200),
  VELOCITY_TYPE_VALUE_1_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_2_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_3_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_4_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_5_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_6_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_7_OID       varchar2(200),
  VELOCITY_TYPE_VALUE_8_OID       varchar2(200),
  IS_ODOMETER_REQ                 varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  IS_VEHICLE_ID_REQ               varchar2(200),
  IS_DRIVER_ID_REQ                varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_FLEET_ID_REQ                 varchar2(200),
  IS_ORDER_NUMBER_REQ             varchar2(200),
  IS_CUSTOMER_SELECTION_REQ       varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  POSTAL_CONTACT_NAME             varchar2(200),
  POSTAL_CONTACT_TITLE            varchar2(200),
  POSTAL_ADDRESS_LINE             varchar2(200),
  POSTAL_POSTAL_CODE              varchar2(200),
  POSTAL_COUNTRY_OID              varchar2(200),
  POSTAL_PHONE_BUSINESS           varchar2(200),
  POSTAL_SUBURB                   varchar2(200),
  STREET_CONTACT_NAME             varchar2(200),
  STREET_CONTACT_TITLE            varchar2(200),
  STREET_ADDRESS_LINE             varchar2(200),
  STREET_POSTAL_CODE              varchar2(200),
  STREET_COUNTRY_OID              varchar2(200),
  STREET_PHONE_BUSINESS           varchar2(200),
  STREET_SUBURB                   varchar2(200),
  REQUESTED_BY                    varchar2(200),
  REQUESTED_PHONE                 varchar2(200),
  COST_CENTRE_EFFECTIVE_ON        date,
  STREET_STATE_OID                varchar2(200),
  POSTAL_STATE_OID                varchar2(200),
  COST_CENTRE_CODE                varchar2(200),
  TAX_CREDIT                      varchar2(200),
  REISSUE_CID                     varchar2(200),
  REISSUE_NEW_CARD_NO             varchar2(200),
  REPORT_EMAIL_ADDRESS            varchar2(200),
  EXPIRES_ON                      date,
  INITIAL_CARD_STATUS_OID         varchar2(200),
  IS_BALANCE_ALLOWED              varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  constraint BULK_CARD_DETAILS_PK primary key (BULK_CARD_DETAIL_OID) using index);

create table BULK_CARD_COLUMN_DETAILS (
  BULK_CARD_COLUMN_DETAIL_OID     varchar2(200),
  BULK_CARD_COLUMN_OID            varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  COMMENTS                        varchar2(200),
  COLUMN_DESCRIPTION              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint BULK_CARD_COLUMN_DETAILS_PK primary key (BULK_CARD_COLUMN_DETAIL_OID) using index);

create table BULK_CARD_COLUMNS (
  BULK_CARD_COLUMN_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BULK_CARD_TEMPLATE_OID          varchar2(200),
  COLUMN_NUMBER                   varchar2(200),
  DESCRIPTION                     varchar2(200),
  IFCS3_COLUMN                    varchar2(200),
  EXCEL_FORMAT_CID                varchar2(200),
  IS_EXCLUDE_FROM_UPDATE          varchar2(200),
  constraint BULK_CARD_COLUMNS_PK primary key (BULK_CARD_COLUMN_OID) using index);

create table BILLING_PLANS (
  BILLING_PLAN_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  PAYMENT_METHOD_CID              varchar2(200),
  PERIOD_BALANCE_TYPE_CID         varchar2(200),
  PAYMENT_DAYS                    varchar2(200),
  DESCRIPTION                     varchar2(200),
  STATEMENT_MESSAGE_OID           varchar2(200),
  DEBIT_TRANSACTION_TYPE_OID      varchar2(200),
  CREDIT_TRANSACTION_TYPE_OID     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  EXTERNAL_PAYMENT_METHOD_DID     varchar2(200),
  DUE_ON_CALCULATION_CID          varchar2(200),
  IS_DUE_ON_BANKING_DAY           varchar2(200),
  constraint BILLING_PLANS_PK primary key (BILLING_PLAN_OID) using index);

create table BATCH_TYPES (
  BATCH_TYPE_OID                  varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CAPTURE_TYPE_OID                varchar2(200),
  BATCH_SOURCE_DID                varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  DEBIT_TRANS_TRANSLATION_OID     varchar2(200),
  CREDIT_TRANS_TRANSLATION_OID    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  EXTERNAL_CLIENT_ID_OID          varchar2(200),
  IS_MANUAL_ENTRY_ALLOWED         varchar2(200),
  IS_MERCHANT_ENTRY_ALLOWED       varchar2(200),
  constraint BATCH_TYPES_PK primary key (BATCH_TYPE_OID) using index);

create table BANK_BRANCHES (
  BANK_BRANCH_OID                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BANK_OID                        varchar2(200),
  BRANCH_NO                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint BANK_BRANCHES_PK primary key (BANK_BRANCHE_OID) using index);

create table BANK_ACCOUNTS (
  BANK_ACCOUNT_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BANK_OID                        varchar2(200),
  BANK_BRANCH_OID                 varchar2(200),
  CREATED_ON                      date,
  ACCOUNT_NO                      varchar2(200),
  BANK_NO                         varchar2(200),
  BRANCH_NO                       varchar2(200),
  ACCOUNT_NAME                    varchar2(200),
  LETTER_ON                       date,
  IBAN                            varchar2(200),
  BIC                             varchar2(200),
  SWIFT_CODE                      varchar2(200),
  constraint BANK_ACCOUNTS_PK primary key (BANK_ACCOUNT_OID) using index);

create table BANKS (
  BANK_OID                        varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CHECK_DIGIT_ROUTINE_CID         varchar2(200),
  BANK_NO                         varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  LOW_BRANCH                      varchar2(200),
  HIGH_BRANCH                     varchar2(200),
  SWIFT_CODE                      varchar2(200),
  constraint BANKS_PK primary key (BANK_OID) using index);

create table BANKING_TRANS_GL_SUMMARIES (
  BANKING_TRANS_GL_SUMMARY_OID    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  GL_ACCOUNT_CODE_OID             varchar2(200),
  GL_ACCOUNT_NUMBER_OID           varchar2(200),
  COST_PROFIT_CENTRE              varchar2(200),
  GL_SUMMARY_AT                   varchar2(200),
  GL_EXTRACT_AT                   varchar2(200),
  MONETARY_AMOUNT_TOTAL           varchar2(200),
  HAS_BEEN_POSTED                 varchar2(200),
  DEBIT_CREDIT                    varchar2(200),
  constraint BANKING_TRANS_GL_SUMMARIES_PK primary key (BANKING_TRANS_GL_SUMMARIE_OID) using index);

create table BANKING_TRANS_CATEGORIES (
  BANKING_TRANS_CATEGORY_OID      varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  EXTERNAL_TRANSACTION_TYPE       varchar2(200),
  BANKING_TRANS_TYPE_CAT_CID      varchar2(200),
  CLIENT_MID                      varchar2(200),
  BANKING_ACCOUNT_MAP_OID         varchar2(200),
  CLEARING_WINDOW                 varchar2(200),
  AUTO_POST                       varchar2(200),
  GEN_PAYMENT_DISHONOR            varchar2(200),
  constraint BANKING_TRANS_CATEGORIES_PK primary key (BANKING_TRANS_CATEGORIE_OID) using index);

create table BANKING_TRANSACTIONS (
  BANKING_TRANSACTION_OID         varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  BANKING_HEADER_OID              varchar2(200),
  BANKING_STATEMENT_OID           varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  BANKING_TRANS_CATEGORY_OID      varchar2(200),
  MONETARY_AMOUNT                 varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  VALUE_ON                        date,
  EFFECTIVE_ON                    date,
  REFERENCE_NO_QUALIFIER          varchar2(200),
  REFERENCE_NO                    varchar2(200),
  SEQUENCE_NO                     varchar2(200),
  TRANSACTION_DETAIL              varchar2(200),
  TRANSACTION_STATUS_CID          varchar2(200),
  TRANSACTION_STATUS_AT           varchar2(200),
  NOTES                           varchar2(200),
  WAS_INITIALLY_UNALLOCATED       varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  GL_ACCOUNT_NUMBER_OID           varchar2(200),
  GL_SUMMARIZED                   varchar2(200),
  CLIENT_MID                      varchar2(200),
  MATCHING_ID                     varchar2(200),
  XMATCHED                         varchar2(200),
  TRANSACTION_DATE                date,
  ORIGINAL_ACCOUNT_NO             varchar2(200),
  REMITTANCE_ADVICE               varchar2(200),
  constraint BANKING_TRANSACTIONS_PK primary key (BANKING_TRANSACTION_OID) using index);

create table BANKING_STATEMENTS (
  BANKING_STATEMENT_OID           varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  BANKING_STATEMENT_TYPE          varchar2(200),
  BANKING_STATEMENT_NO            varchar2(200),
  BANKING_HEADER_OID              varchar2(200),
  CURRENCY_OID                    varchar2(200),
  BRANCH_NO                       varchar2(200),
  SWIFT_CODE                      varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  BANK_NO                         varchar2(200),
  BANK_ACCOUNT_NUMBER             varchar2(200),
  CLOSING_BALANCE_REFERENCE_DATE  date,
  OPENING_BALANCE_REFERENCE_DATE  date,
  OPENING_BALANCE_AMOUNT          varchar2(200),
  OPENING_BALANCE_CURRENCY_OID    varchar2(200),
  CLOSING_BALANCE_AMOUNT          varchar2(200),
  CLOSING_BALANCE_CURRENCY_OID    varchar2(200),
  STATEMENT_BALANCED              varchar2(200),
  constraint BANKING_STATEMENTS_PK primary key (BANKING_STATEMENT_OID) using index);

create table BANKING_HEADERS (
  BANKING_HEADER_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BANK_ID                         varchar2(200),
  DOCUMENT_TYPE                   varchar2(200),
  DOCUMENT_NO                     varchar2(200),
  DOCUMENT_DATED_AT               varchar2(200),
  constraint BANKING_HEADERS_PK primary key (BANKING_HEADER_OID) using index);

create table BANKING_ACCOUNT_MAP (
  BANKING_ACCOUNT_MAP_OID         varchar2(200),
  CLIENT_MID                      varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  BANK_NUMBER                     varchar2(200),
  BANK_NAME                       varchar2(200),
  BANK_ACCOUNT_NUMBER             varchar2(200),
  BANK_ACCOUNT_NAME               varchar2(200),
  DESCRIPTION                     varchar2(200),
  CHECK_STM_NUMBER                varchar2(200),
  constraint BANKING_ACCOUNT_MAP_PK primary key (BANKING_ACCOUNT_MAP) using index);

create table BAD_DEBT_ACCOUNTS (
  BAD_DEBT_ACCOUNT_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  CHARGE_OFF_AMOUNT               varchar2(200),
  AGENCY_RECOVERY                 varchar2(200),
  NON_AGENCY_RECOVERY             varchar2(200),
  WRITE_OFF_AMOUNT                varchar2(200),
  INITIAL_CHARGE_OFF_ON           date,
  constraint BAD_DEBT_ACCOUNTS_PK primary key (BAD_DEBT_ACCOUNT_OID) using index);

create table AUTH_TRANS_LINE_ITEMS (
  AUTH_TRANS_LINE_ITEM_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUTH_TRANSACTION_LOG_OID        varchar2(200),
  LINE_NUMBER                     varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  UNIT_OF_MEASURE                 varchar2(200),
  QUANTITY                        varchar2(200),
  UNIT_PRICE                      varchar2(200),
  VALUE                           varchar2(200),
  TAX_CODE                        varchar2(200),
  ADDITIONAL_PRODUCT_CODE         varchar2(200),
  IS_FUEL                         varchar2(200),
  PRODUCT_OID                     varchar2(200),
  constraint AUTH_TRANS_LINE_ITEMS_PK primary key (AUTH_TRANS_LINE_ITEM_OID) using index);

create table AUTH_TRANS_EXCEPTIONS (
  AUTH_TRANS_EXCEPTION_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUTH_TRANSACTION_LOG_OID        varchar2(200),
  TRANSACTION_STEP_OID            varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  constraint AUTH_TRANS_EXCEPTIONS_PK primary key (AUTH_TRANS_EXCEPTION_OID) using index);

create table AUTH_TRANSACTION_LOGS (
  AUTH_TRANSACTION_LOG_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  RECEIVED_AT                     varchar2(200),
  LOCATION_CODE                   varchar2(200),
  CARD_NO                         varchar2(200),
  DATE_TIME_EFFECTIVE             varchar2(200),
  REFERENCE                       varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  TRANSACTION_CODE                varchar2(200),
  TERMINAL_ID                     varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  ODOMETER                        varchar2(200),
  ATTENTION_KEY                   varchar2(200),
  AUTHORISATION_NO                varchar2(200),
  BATCH_NO                        varchar2(200),
  BATCH_SOURCE                    varchar2(200),
  DRIVER_ID                       varchar2(200),
  VEHICLE_ID                      varchar2(200),
  POS_ENTRY_1                     varchar2(200),
  POS_ENTRY_2                     varchar2(200),
  POS_ENTRY_3                     varchar2(200),
  POS_ENTRY_4                     varchar2(200),
  POS_ENTRY_5                     varchar2(200),
  EXPIRY_DATE                     varchar2(200),
  PIN_BLOCK                       varchar2(200),
  CVV_SECURITY_CODE               varchar2(200),
  NEW_PIN                         varchar2(200),
  SESSION_PIN_BLOCK               varchar2(200),
  TRANSACTION_ERROR_OID           varchar2(200),
  RESPONSE_FEE                    varchar2(200),
  RESPONSE_AVAILABLE_BALANCE      varchar2(200),
  RESPONSE_CODE                   varchar2(200),
  LOCATION_NAME                   varchar2(200),
  HOST_AUTH_PROCESS_STATUS_CID    varchar2(200),
  STAN                            varchar2(200),
  PROC_CODE                       varchar2(200),
  LOCAL_TRANSACTION_TIME          varchar2(200),
  LOCAL_TRANSACTION_DATE          varchar2(200),
  SETTLEMENT_DATE                 varchar2(200),
  POS_ENTRY_MODE                  varchar2(200),
  TERMINAL_TYPE                   varchar2(200),
  ACQUIR_INSTITUTION_ID           varchar2(200),
  TRACK2                          varchar2(200),
  CAID                            varchar2(200),
  SEC_RELATED_CONTROL_INFO        varchar2(200),
  MAC                             varchar2(200),
  NETWORK_MAN_INFO                varchar2(200),
  KEY_EXCHANGE_DATA               varchar2(200),
  IS_TRANS_APPROVED               varchar2(200),
  IS_DUPLICATE                    varchar2(200),
  IS_TRANS_REVERSED               varchar2(200),
  IS_AUTH_MATCHED                 varchar2(200),
  IS_AUTH_DROPPED                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ORIG_TRANSACTION_CODE           varchar2(200),
  ORIG_REFERENCE                  varchar2(200),
  ORIG_TRANS_DATE_TIME            varchar2(200),
  ORIG_ACQUIR_INST_ID             varchar2(200),
  ORIG_FWD_ACQUIR_INST_ID         varchar2(200),
  PROCESSING_TIME_MS              varchar2(200),
  CLIENT_MID                      varchar2(200),
  FAILED_TRAN_STEP_SHORT_DESC     varchar2(200),
  TRANSMISSION_DATE_TIME          varchar2(200),
  PARTIALLY_APPROVED_AMOUNT       varchar2(200),
  IS_TIMEOUT                      varchar2(200),
  VEH_FLEET_IDENT_MODE            varchar2(200),
  PUMP_LINKED_INDICATOR           varchar2(200),
  DELIVERY_NOTE_NUMBER            varchar2(200),
  ORIG_BATCH_NO                   varchar2(200),
  ORIG_STAN                       varchar2(200),
  FUNCTION_CODE                   varchar2(200),
  ORIGINAL_DATE_LOCAL             varchar2(200),
  ORIGINAL_TIME_LOCAL             varchar2(200),
  POS_TRANSACTION_OID             varchar2(200),
  ORIG_AUTH_TRANSACTION_LOG_OID   varchar2(200),
  MTI_CID                         varchar2(200),
  IS_TRANS_STRICTLY_MATCHED       varchar2(200),
  MESSAGE_REASON_CODE             varchar2(200),
  CUSTOMER_REFERENCE_NUMBER       varchar2(200),
  PIN_ENCRYPTION_METHODOLOGY      varchar2(200),
  ORIGINAL_CURRENCY_CODE          varchar2(200),
  SERVICE_LEVEL                   varchar2(200),
  SYNC_UPDATE_COUNT               varchar2(200),
  PROCESSED_AT                    varchar2(200),
  BILLING_ACCOUNT_OID             varchar2(200),
  AUTH_ACCOUNT_OID                varchar2(200),
  POS_AMOUNT                      varchar2(200),
  POS_CURRENCY_CODE               varchar2(200),
  EXCHANGE_RATE                   varchar2(200),
  IS_BALANCE_ALLOWED              varchar2(200),
  constraint AUTH_TRANSACTION_LOGS_PK primary key (AUTH_TRANSACTION_LOG_OID) using index);

create table AUTH_ACQUIRING_INST (
  AUTH_ACQUIRING_INST_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACQUIRING_INST_ID               varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXTERNAL_CLIENT_ID_OID          varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  ACQ_INST_ID_CURRENCY_CODE       varchar2(200),
  constraint AUTH_ACQUIRING_INST_PK primary key (AUTH_ACQUIRING_IN_OID) using index);

create table AUDIT_TABLES_BAK (
  AUDIT_TABLE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  IS_AUDITING_ENABLED             varchar2(200),
  constraint AUDIT_TABLES_BAK_PK primary key (AUDIT_TABLE_OID) using index);

create table AUDIT_TABLES (
  AUDIT_TABLE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  TABLE_NAME                      varchar2(200),
  IS_AUDITING_ENABLED             varchar2(200),
  constraint AUDIT_TABLES_PK primary key (AUDIT_TABLE_OID) using index);

create table AUDIT_LOG (
  TABLE_NAME                      varchar2(200),
  KEY_OID                         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  UPDATED_BY                      varchar2(200),
  UPDATED_AT                      varchar2(200),
  AUDIT_ACTION                    varchar2(200),
  constraint AUDIT_LOG_PK primary key (AUDIT_LOG) using index);

create table AUDIT_DETAILS (
  AUDIT_DETAIL_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUDIT_CATEGORY_TABLE_OID        varchar2(200),
  KEY_OID                         varchar2(200),
  UPDATED_AT                      varchar2(200),
  UPDATED_BY                      varchar2(200),
  TABLE_NAME                      varchar2(200),
  COLUMN_NAME                     varchar2(200),
  AUDIT_ACTION                    varchar2(200),
  CLIENT_MID                      varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  APPLICATION_OID                 varchar2(200),
  LOCATION_MID                    varchar2(200),
  MERCHANT_MID                    varchar2(200),
  CARD_OID                        varchar2(200),
  BEFORE_IMAGE                    varchar2(200),
  AFTER_IMAGE                     varchar2(200),
  TABLE_DESCRIPTION               varchar2(200),
  COLUMN_DESCRIPTION              varchar2(200),
  APPLICATION_CONTACT_OID         varchar2(200),
  CONTACT_OID                     varchar2(200),
  CARD_CONTACT_OID                varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  constraint AUDIT_DETAILS_PK primary key (AUDIT_DETAIL_OID) using index);

create table AUDIT_CONTROL (
  AUDIT_CONTROL_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_AUDITING_ENABLED             varchar2(200),
  AUDIT_TO_SCHEMA                 varchar2(200),
  constraint AUDIT_CONTROL_PK primary key (AUDIT_CONTROL) using index);

create table AUDIT_CATEGORY_TABLES (
  AUDIT_CATEGORY_TABLE_OID        varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUDIT_TABLE_OID                 varchar2(200),
  AUDIT_CATEGORY_CID              varchar2(200),
  TABLE_KEY_COLUMN                varchar2(200),
  IS_AUDITING_ENABLED             varchar2(200),
  DESCRIPTION                     varchar2(200),
  PARENT_TABLE_COLUMN             varchar2(200),
  TABLE_FK_COLUMN                 varchar2(200),
  CONTEXT_JOIN_SQL                varchar2(200),
  EXTRA_IDENTIFIER_INSERT         varchar2(200),
  EXTRA_IDENTIFIER_JOIN_SQL       varchar2(200),
  TABLE_ID_COLUMN                 varchar2(200),
  constraint AUDIT_CATEGORY_TABLES_PK primary key (AUDIT_CATEGORY_TABLE_OID) using index);

create table AUDIT_CATEGORY_COLUMNS (
  AUDIT_CATEGORY_COLUMN_OID       varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  AUDIT_CATEGORY_TABLE_OID        varchar2(200),
  COLUMN_NAME                     varchar2(200),
  IS_AUDITING_ENABLED             varchar2(200),
  DESCRIPTION                     varchar2(200),
  FOREIGN_TABLE_NAME              varchar2(200),
  FOREIGN_COLUMN_NAME             varchar2(200),
  FOREIGN_DESC_COLUMN_NAME        varchar2(200),
  FOREIGN_EXTRA_SQL               varchar2(200),
  IS_EXTRACTED_ENABLED            varchar2(200),
  constraint AUDIT_CATEGORY_COLUMNS_PK primary key (AUDIT_CATEGORY_COLUMN_OID) using index);

create table APP_TYPE_TRANS_PROFILES (
  APP_TYPE_TRANS_PROFILE_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPL_TYPE_TRANSFER_OID          varchar2(200),
  FROM_PRICING_PROFILE_OID        varchar2(200),
  TO_PRICING_PROFILE_OID          varchar2(200),
  PRICING_PROF_TRANSF_OPTION_CID  varchar2(200),
  FROM_FEE_PROFILE_OID            varchar2(200),
  TO_FEE_PROFILE_OID              varchar2(200),
  FEE_PROF_TRANSF_OPTION_CID      varchar2(200),
  FROM_CARD_REISSUE_PROFILE_OID   varchar2(200),
  TO_CARD_REISSUE_PROFILE_OID     varchar2(200),
  REISSUE_PROF_TRANSF_OPTION_CID  varchar2(200),
  constraint APP_TYPE_TRANS_PROFILES_PK primary key (APP_TYPE_TRANS_PROFILE_OID) using index);

create table APP_TYPE_TRANS_CARD_PRODS (
  APP_TYPE_TRANS_CARD_PROD_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPL_TYPE_TRANSFER_OID          varchar2(200),
  FROM_CARD_PRODUCT_OID           varchar2(200),
  TO_CARD_PRODUCT_OID             varchar2(200),
  IS_DEFAULT_CARD_PRODUCT         varchar2(200),
  constraint APP_TYPE_TRANS_CARD_PRODS_PK primary key (APP_TYPE_TRANS_CARD_PROD_OID) using index);

create table APP_TYPE_TRANS_CARD_FEE (
  APP_TYPE_TRANS_CARD_FEE_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPL_TYPE_TRANSFER_OID          varchar2(200),
  FROM_FEE_PROFILE_OID            varchar2(200),
  TO_FEE_PROFILE_OID              varchar2(200),
  TRANSFER_OPTION_CID             varchar2(200),
  FROM_CARD_PRODUCT_OID           varchar2(200),
  constraint APP_TYPE_TRANS_CARD_FEE_PK primary key (APP_TYPE_TRAN_OID) using index);

create table APP_TYPE_TRANS_CARD_CTRLS (
  APP_TYPE_TRANS_CARD_CTRL_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPL_TYPE_TRANSFER_OID          varchar2(200),
  FROM_CARD_CONTROL_PROFILE_OID   varchar2(200),
  TO_CARD_CONTROL_PROFILE_OID     varchar2(200),
  TRANSFER_OPTION_CID             varchar2(200),
  FROM_CARD_PRODUCT_OID           varchar2(200),
  constraint APP_TYPE_TRANS_CARD_CTRLS_PK primary key (APP_TYPE_TRANS_CARD_CTRL_OID) using index);

create table APPL_TYPE_TRANSFERS (
  APPL_TYPE_TRANSFER_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  EFFECTIVE_ON                    date,
  FROM_APPLICATION_TYPE_OID       varchar2(200),
  TO_APPLICATION_TYPE_OID         varchar2(200),
  APPL_TYPE_TRANSFER_STATUS_CID   varchar2(200),
  REQUESTED_BY                    varchar2(200),
  CREATED_BY                      varchar2(200),
  CREATED_AT                      date,
  PROCESSED_AT                    date,
  ERROR                           varchar2(200),
  constraint APPL_TYPE_TRANSFERS_PK primary key (APPL_TYPE_TRANSFER_OID) using index);

create table APPL_ACCNT_PROFILES (
  APPLICATION_ACCNT_PROFILE_OID   varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_OID                 varchar2(200),
  ACCOUNT_CONTROL_PROFILE_OID     varchar2(200),
  constraint APPL_ACCNT_PROFILES_PK primary key (APPL_ACCNT_PROFILE_OID) using index);

create table APPLICATION_VAS_OFFERINGS (
  APPLICATION_VAS_OFFERING_OID    varchar2(200),
  CLIENT_MID                      varchar2(200),
  EFFECTIVE_DATE                  date,
  EXPIRY_ON                       date,
  PRODUCT_OID                     varchar2(200),
  APPLICATION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint APPLICATION_VAS_OFFERINGS_PK primary key (APPLICATION_VAS_OFFERING_OID) using index);

create table APPLICATION_TYPE_REPORTS (
  APPLICATION_TYPE_REPORT_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  REPORT_TYPE_OID                 varchar2(200),
  constraint APPLICATION_TYPE_REPORTS_PK primary key (APPLICATION_TYPE_REPORT_OID) using index);

create table APPLICATION_TYPES (
  APPLICATION_TYPE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  ACCOUNT_STATUS_OID              varchar2(200),
  ACCOUNT_SUB_STATUS_OID          varchar2(200),
  DESCRIPTION                     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  SETUP_FEE_OID                   varchar2(200),
  IS_APPROVABLE                   varchar2(200),
  IS_FOR_DISTRIBUTOR              varchar2(200),
  constraint APPLICATION_TYPES_PK primary key (APPLICATION_TYPE_OID) using index);

create table APPLICATION_STATUS (
  APPLICATION_STATUS_OID          varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_MODE_CID            varchar2(200),
  IS_AUTO_ASSIGNABLE              varchar2(200),
  DESCRIPTION                     varchar2(200),
  WORK_QUEUE_OID                  varchar2(200),
  LETTER_DEFINITION_OID           varchar2(200),
  REPORT_TEMPLATE_OID             varchar2(200),
  constraint APPLICATION_STATUS_PK primary key (APPLICATION_STATU_OID) using index);

create table APPLICATION_SOURCES (
  APPLICATION_SOURCE_OID          varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  IS_LOCATION_NO_REQUIRED         varchar2(200),
  DESCRIPTION                     varchar2(200),
  SHORT_DESCRIPTION               varchar2(200),
  constraint APPLICATION_SOURCES_PK primary key (APPLICATION_SOURCE_OID) using index);

create table APPLICATION_SIGNATORIES (
  APPLICATION_SIGNATORIES_OID     varchar2(200),
  APPLICATION_OID                 varchar2(200),
  SIGNATORY_NAME                  varchar2(200),
  JOB_TITLE                       varchar2(200),
  DATE_OF_BIRTH                   date,
  DRIVERS_LICENCE                 varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint APPLICATION_SIGNATORIES_PK primary key (APPLICATION_SIGNATORIE_OID) using index);

create table APPLICATION_FUEL_USAGE (
  APPLICATION_FUEL_USAGE_OID      varchar2(200),
  APPLICATION_OID                 varchar2(200),
  APPLICATION_FUEL_DETAILS_OID    varchar2(200),
  FUEL_USAGE_UNITS_CID            varchar2(200),
  VOLUME                          varchar2(200),
  CHARGE                          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint APPLICATION_FUEL_USAGE_PK primary key (APPLICATION_FUEL_U_OID) using index);

create table APPLICATION_FUEL_DETAILS (
  APPLICATION_FUEL_DETAILS_OID    varchar2(200),
  CLIENT_MID                      varchar2(200),
  FUEL_TYPE                       varchar2(200),
  DESCRIPTION                     varchar2(200),
  COST                            varchar2(200),
  DISPLAY                         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  constraint APPLICATION_FUEL_DETAILS_PK primary key (APPLICATION_FUEL_DETAIL_OID) using index);

create table APPLICATION_CONTACTS (
  APPLICATION_CONTACT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CONTACT_NAME                    varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  CONTACT_TYPE_OID                varchar2(200),
  IS_DEFAULT                      varchar2(200),
  constraint APPLICATION_CONTACTS_PK primary key (APPLICATION_CONTACT_OID) using index);

create table APPLICATION_CARD_PROFILES (
  APPLICATION_CARD_PROFILE_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CARD_CONTROL_PROFILE_OID        varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  constraint APPLICATION_CARD_PROFILES_PK primary key (APPLICATION_CARD_PROFILE_OID) using index);

create table APPLICATION_CARD_PRODUCTS (
  APPLICATION_CARD_PRODUCT_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  CARD_INITIAL_STATUS_OVERRIDE    varchar2(200),
  INITIAL_CARD_STATUS_OID         varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  EXT_EMBOSSER_CUSTOMER_REF       varchar2(200),
  IS_CARD_REQ_VALIDATION_RULE     varchar2(200),
  constraint APPLICATION_CARD_PRODUCTS_PK primary key (APPLICATION_CARD_PRODUCT_OID) using index);

create table APPLICATION_BANK_ACCOUNTS (
  APPLICATION_BANK_ACCOUNT_OID    varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  BANK_OID                        varchar2(200),
  BANK_BRANCH_OID                 varchar2(200),
  CREATED_ON                      date,
  ACCOUNT_NO                      varchar2(200),
  BANK_NO                         varchar2(200),
  BRANCH_NO                       varchar2(200),
  ACCOUNT_NAME                    varchar2(200),
  LETTER_ON                       date,
  IBAN                            varchar2(200),
  BIC                             varchar2(200),
  SWIFT_CODE                      varchar2(200),
  constraint APPLICATION_BANK_ACCOUNTS_PK primary key (APPLICATION_BANK_ACCOUNT_OID) using index);

create table APPLICATION_ADDRESSES (
  APPLICATION_ADDRESS_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COUNTRY_OID                     varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  STATE_OID                       varchar2(200),
  POSTAL_CODE                     varchar2(200),
  SUBURB                          varchar2(200),
  constraint APPLICATION_ADDRESSES_PK primary key (APPLICATION_ADDRESSE_OID) using index);

create table APPLICATIONS (
  APPLICATION_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CLIENT_MID                      varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  APPLICATION_STATUS_OID          varchar2(200),
  APPLICATION_NO                  varchar2(200),
  NAME                            varchar2(200),
  IS_EXISTING_NO_USED             varchar2(200),
  IS_WEB_ACCESS_REQUIRED          varchar2(200),
  IS_EXTERNAL_CARD_SOURCE         varchar2(200),
  IS_BANK_OPINION                 varchar2(200),
  IS_TRADE_REFERENCE              varchar2(200),
  IS_FINANCIAL_REFERENCE          varchar2(200),
  IS_DIRECTORS_CHEQUE             varchar2(200),
  IS_TRUST_DEED_RECEIVED          varchar2(200),
  IS_INCORPORATION_CERT           varchar2(200),
  IS_DUNN_BRADSTREET_CHECK        varchar2(200),
  IS_RISK_ASSESSMENT              varchar2(200),
  IS_SECURITY_HELD                varchar2(200),
  IS_DIRECTORS_GUARANTEE          varchar2(200),
  IS_CHECKS_COMPLETED             varchar2(200),
  IS_TRUSTEE                      varchar2(200),
  APPLICATION_STATUS_AT           varchar2(200),
  RECEIVED_ON                     date,
  CREDIT_LIMIT                    varchar2(200),
  LANGUAGE_OID                    varchar2(200),
  CURRENCY_OID                    varchar2(200),
  BUSINESS_TYPE_DID               varchar2(200),
  INDUSTRY_OID                    varchar2(200),
  BUYER_CLASS_DID                 varchar2(200),
  FIN_YEAR_END_MONTH_OID          varchar2(200),
  PRICING_PROFILE_OID             varchar2(200),
  BILLING_PLAN_OID                varchar2(200),
  CREDIT_PLAN_OID                 varchar2(200),
  CUSTOMER_TYPE_DID               varchar2(200),
  SIGNUP_MEMBER_OID               varchar2(200),
  BILLING_FREQUENCY_OID           varchar2(200),
  CYCLE_FREQUENCY_OID             varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  ADMIN_TERRITORY_OID             varchar2(200),
  MARKETING_TERRITORY_OID         varchar2(200),
  POSTAL_ADDRESS_OID              varchar2(200),
  STREET_ADDRESS_OID              varchar2(200),
  APPLICATION_BANK_ACCOUNT_OID    varchar2(200),
  DENIAL_REASON_DID               varchar2(200),
  APPLICATION_SOURCE_OID          varchar2(200),
  GL_CHANNEL_DID                  varchar2(200),
  GL_COST_CENTRE_CODE             varchar2(200),
  CUSTOMER_NO                     varchar2(200),
  EMAIL_ADDRESS                   varchar2(200),
  PHONE_BUSINESS                  varchar2(200),
  PHONE_FAX                       varchar2(200),
  PHONE_MOBILE_1                  varchar2(200),
  PHONE_MOBILE_2                  varchar2(200),
  NEEDED_ON                       date,
  PERCENT_BUSINESS_HELD           varchar2(200),
  BUSINESS_COMMENCED_ON           date,
  EXTERNAL_CUSTOMER_CODE          varchar2(200),
  CONTRA_ACCOUNT_CODE             varchar2(200),
  AP_ACCOUNT_CODE                 varchar2(200),
  TAX_NO                          varchar2(200),
  EMBOSSING_NAME                  varchar2(200),
  TRADING_NAME                    varchar2(200),
  MAX_CARD_EXPIRY_DATE            date,
  PUMP_CONTROL_OID                varchar2(200),
  CARD_GRAPHIC_OID                varchar2(200),
  MAIL_INDICATOR_OID              varchar2(200),
  TRUSTEE_FOR                     varchar2(200),
  PARENT_COMPANY                  varchar2(200),
  CONTACT_NAME                    varchar2(200),
  CONTACT_TITLE                   varchar2(200),
  EXISTING_ACCOUNT                varchar2(200),
  EXISTING_ACCOUNT_NO             varchar2(200),
  PROMOTIONAL_MATERIAL            varchar2(200),
  COMPANY_NO                      varchar2(200),
  MAIN_BUSINESS                   varchar2(200),
  CURRENT_SUPPLIER_DID            varchar2(200),
  CARDS_REQUIRED                  varchar2(200),
  VEHICLES                        varchar2(200),
  CARBON_OFFSET                   varchar2(200),
  CARBON_OFFSET_LEVEL_DID         varchar2(200),
  IS_USING_ELECTRONIC_REPORTING   varchar2(200),
  IS_USING_ELECTRONIC_MARKETING   varchar2(200),
  EXT_DELIVERY_REF                varchar2(200),
  EXT_DELIVERY_NAME               varchar2(200),
  EXT_ACCOUNT_REF                 varchar2(200),
  EXT_ACCOUNT_ID                  varchar2(200),
  IS_PIN_REQ                      varchar2(200),
  IS_SIGNATURE_REQ                varchar2(200),
  IS_CARD_SYSTEM_ONLY             varchar2(200),
  COUPON_CODE                     varchar2(200),
  CARD_REISSUE_PROFILE_OID        varchar2(200),
  ALERT_THRESHOLD_PERCENTAGE      varchar2(200),
  AUTHENTICATION_ANSWER           varchar2(200),
  BANK_GUARANTEE_AMOUNT           varchar2(200),
  IS_BANK_GUARANTEE_PROVIDED      varchar2(200),
  BEHAVIOURAL_SCORE               varchar2(200),
  IS_STATUS_CHANGE_REPORT_REQ     varchar2(200),
  CONTACTLESS_PAYMENT             varchar2(200),
  CONTACTLESS_PMT_MODIFIED_ON     date,
  EXT_APPLICATION_REF             varchar2(200),
  SECURITY_INDICATOR              varchar2(200),
  TOTAL_SECURITY_VALUE            varchar2(200),
  TRADE_EXPOSURE_LIMIT            varchar2(200),
  PARENT_COMPANY_NO               varchar2(200),
  RISK_GRADE_DID                  varchar2(200),
  EMBASSY_CODE                    varchar2(200),
  RESIDENCY_COUNTRY_OID           varchar2(200),
  INTERACTION_TYPE_DID            varchar2(200),
  IS_TAX_EXEMPT                   varchar2(200),
  constraint APPLICATIONS_PK primary key (APPLICATION_OID) using index);

create table ALLOCATIONS (
  ALLOCATION_OID                  varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DETAIL_GROUP_OID                varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  AMOUNT                          varchar2(200),
  constraint ALLOCATIONS_PK primary key (ALLOCATION_OID) using index);

create table AGING_BUCKETS (
  AGING_BUCKET_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PERIOD_BALANCE_TYPE_CID         varchar2(200),
  DESCRIPTION                     varchar2(200),
  constraint AGING_BUCKETS_PK primary key (AGING_BUCKET_OID) using index);

create table AGED_AMOUNTS (
  AGED_AMOUNT_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PERIOD_BALANCE_OID              varchar2(200),
  AGING_BUCKET_OID                varchar2(200),
  AMOUNT                          varchar2(200),
  constraint AGED_AMOUNTS_PK primary key (AGED_AMOUNT_OID) using index);

create table ADJ_TRANS_LINE_ITEMS (
  ADJ_TRANSACTION_OID             varchar2(200),
  LINE_NUMBER                     varchar2(200),
  WAS_CORRECTED                   varchar2(200),
  PRODUCT_OID                     varchar2(200),
  QUANTITY                        varchar2(200),
  UNIT_PRICE                      varchar2(200),
  VALUE                           varchar2(200),
  PRODUCT_CODE                    varchar2(200),
  constraint ADJ_TRANS_LINE_ITEMS_PK primary key (ADJ_TRANS_LINE_ITEM_OID) using index);

create table ADJ_TRANSACTIONS (
  ADJ_TRANSACTION_OID             varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  LOCATION_MID                    varchar2(200),
  TRANSACTION_STATUS_AT           varchar2(200),
  PROCESSED_AT                    varchar2(200),
  CARD_NO                         varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  REFERENCE                       varchar2(200),
  CAPTURE_TYPE                    varchar2(200),
  CURRENCY_CODE                   varchar2(200),
  TRANSACTION_CODE                varchar2(200),
  LOCATION_CODE                   varchar2(200),
  TERMINAL_ID                     varchar2(200),
  POS_LOCN_RECONCILIATION_OID     varchar2(200),
  ORIGINAL_AMOUNT                 varchar2(200),
  ODOMETER                        varchar2(200),
  ATTENTION_KEY                   varchar2(200),
  AUTHORISATION_NO                varchar2(200),
  BATCH_NO                        varchar2(200),
  BATCH_SOURCE                    varchar2(200),
  DRIVER_ID                       varchar2(200),
  VEHICLE_ID                      varchar2(200),
  ADJ_ENTRY_1                     varchar2(200),
  ADJ_ENTRY_2                     varchar2(200),
  ADJ_ENTRY_3                     varchar2(200),
  ADJ_ENTRY_4                     varchar2(200),
  ADJ_ENTRY_5                     varchar2(200),
  POSTING_PROCESS_ID              varchar2(200),
  LOCATION_NAME                   varchar2(200),
  PROBLEM_TYPE_DID                varchar2(200),
  MERCHANT_COMMENT                varchar2(200),
  CSR_COMMENT                     varchar2(200),
  MERCHANT_MID                    varchar2(200),
  MERCHANT_STATUS_CID             varchar2(200),
  RECONCILIATION_TYPE_CID         varchar2(200),
  TRANSACTION_STATUS_CID          varchar2(200),
  EXTERNAL_CODE_CLIENT            varchar2(200),
  constraint ADJ_TRANSACTIONS_PK primary key (ADJ_TRANSACTION_OID) using index);

create table ADJUSTMENT_TYPES (
  ADJUSTMENT_TYPE_OID             varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  DEBIT_TRANSACTION_TYPE_OID      varchar2(200),
  CREDIT_TRANSACTION_TYPE_OID     varchar2(200),
  TRANSACTION_CLASS_CID           varchar2(200),
  DESCRIPTION                     varchar2(200),
  DEBIT_CARD_TRANS_TYPE_OID       varchar2(200),
  CREDIT_CARD_TRANS_TYPE_OID      varchar2(200),
  IS_CREDIT_NOTE                  varchar2(200),
  DEFAULT_GL_ACCOUNT_CODE_OID     varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint ADJUSTMENT_TYPES_PK primary key (ADJUSTMENT_TYPE_OID) using index);

create table ADDRESS_CLEANSING (
  ADDRESS_CLEANSING_OID           varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  CREATED_ON                      date,
  MEMBER_OID                      varchar2(200),
  MEMBER_TYPE_CID                 varchar2(200),
  ADDRESS_TYPE_CID                varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  SUBURB                          varchar2(200),
  COUNTRY_CODE                    varchar2(200),
  STATE_CODE                      varchar2(200),
  POSTAL_CODE                     varchar2(200),
  BAR_CODE                        varchar2(200),
  DPID                            varchar2(200),
  ADDRESS_OID                     varchar2(200),
  STATE_OID                       varchar2(200),
  COUNTRY_OID                     varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  CLIENT_MID                      varchar2(200),
  CONTACT_TYPE_OID                varchar2(200),
  constraint ADDRESS_CLEANSING_PK primary key (ADDRESS_CLEAN_OID) using index);

create table ADDRESSES (
  ADDRESS_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  COUNTRY_OID                     varchar2(200),
  ADDRESS_LINE                    varchar2(200),
  STATE_OID                       varchar2(200),
  POSTAL_CODE                     varchar2(200),
  SUBURB                          varchar2(200),
  DPID                            varchar2(200),
  BAR_CODE                        varchar2(200),
  CONFIDENCE_MATCH                varchar2(200),
  constraint ADDRESSES_PK primary key (ADDRESSE_OID) using index);

create table ACCOUNT_VELOCITY_TOTALS (
  ACCOUNT_VELOCITY_TOTAL_OID      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  VELOCITY_TYPE_OID               varchar2(200),
  COUNT_HARD_DECLINE              varchar2(200),
  COUNT_SOFT_DECLINE              varchar2(200),
  TOTAL_HOST_AUTH                 varchar2(200),
  TOTAL_COLLECT                   varchar2(200),
  TOTAL_OTHER                     varchar2(200),
  TRANSACTION_DATE                date,
  TRANSACTION_WEEK                varchar2(200),
  TRANSACTION_MONTH               varchar2(200),
  TRANSACTION_YEAR                varchar2(200),
  LAST_ACCUMULATED_ON             date,
  constraint ACCOUNT_VELOCITY_TOTALS_PK primary key (ACCOUNT_VELOCITY_TOTAL_OID) using index);

create table ACCOUNT_TOTALS (
  ACCOUNT_TOTAL_OID               varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  ACCOUNT_TOTAL_TYPE_CID          varchar2(200),
  PERIOD_BALANCE_TYPE_CID         varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  AMOUNT                          varchar2(200),
  constraint ACCOUNT_TOTALS_PK primary key (ACCOUNT_TOTAL_OID) using index);

create table ACCOUNT_SUB_STATUS_CHANGES (
  ACCOUNT_SUB_STATUS_CHANGES_OID  varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  PROCESS                         varchar2(200),
  EVENT                           varchar2(200),
  FROM_ACCOUNT_SUB_STATUS_OID     varchar2(200),
  TO_ACCOUNT_SUB_STATUS_OID       varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint ACCOUNT_SUB_STATUS_CHANGES_PK primary key (ACCOUNT_SUB_STATUS_CHANGE_OID) using index);

create table ACCOUNT_SUB_STATUS (
  ACCOUNT_SUB_STATUS_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_STATUS_OID              varchar2(200),
  CLIENT_MID                      varchar2(200),
  DESCRIPTION                     varchar2(200),
  PULL_CODE_DID                   varchar2(200),
  CARD_STATUS_OID                 varchar2(200),
  DISABLE_CARD_ORDER              varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  constraint ACCOUNT_SUB_STATUS_PK primary key (ACCOUNT_SUB_STATU_OID) using index);

create table ACCOUNT_STATUS_LOGS (
  ACCOUNT_STATUS_LOG_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  ACCOUNT_STATUS_OID              varchar2(200),
  ACCOUNT_SUB_STATUS_OID          varchar2(200),
  EFFECTIVE_AT                    varchar2(200),
  EXPIRES_AT                      varchar2(200),
  constraint ACCOUNT_STATUS_LOGS_PK primary key (ACCOUNT_STATUS_LOG_OID) using index);

create table ACCOUNT_STATUS (
  ACCOUNT_STATUS_OID              varchar2(200),
  CLIENT_MID                      varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_STATUS_TYPE_CID         varchar2(200),
  DESCRIPTION                     varchar2(200),
  DISABLE_CARD_ORDER              varchar2(200),
  EXTERNAL_CODE                   varchar2(200),
  IS_SHOW_IN_OLS                  varchar2(200),
  DEFAULT_ACCOUNT_SUB_STATUS_OID  varchar2(200),
  constraint ACCOUNT_STATUS_PK primary key (ACCOUNT_STATU_OID) using index);

create table ACCOUNT_FEE_WORKING_DATA (
  ACCOUNT_FEE_WORKING_DATA_OID    varchar2(200),
  CLIENT_MID                      varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  CARD_OID                        varchar2(200),
  CARD_NO                         varchar2(200),
  AMOUNT_TO_USE_FOR_CALC          varchar2(200),
  FEE_DESCRIPTION                 varchar2(200),
  FEE_OID                         varchar2(200),
  FEE_CALCULATION_TYPE_CID        varchar2(200),
  TRANSACTION_TYPE_OID            varchar2(200),
  FEE_POSTING_METHOD_CID          varchar2(200),
  MINIMUM_CHARGE                  varchar2(200),
  MAXIMUM_CHARGE                  varchar2(200),
  FEE_VALUE_OID                   varchar2(200),
  RATE                            varchar2(200),
  DESCRIPTION                     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  VALUE_RANGE_FROM                varchar2(200),
  VALUE_RANGE_TO                  varchar2(200),
  TAX_RATE                        varchar2(200),
  PRODUCT_OID                     varchar2(200),
  RATE_CODE_NO                    varchar2(200),
  IS_RATE_INCLUDED                varchar2(200),
  TAX_TYPE_CID                    varchar2(200),
  STATUS_CID                      varchar2(200),
  PROCESS_ID                      varchar2(200),
  FEE_PERIOD_MONTHS               varchar2(200),
  constraint ACCOUNT_FEE_WORKING_DATA_PK primary key (ACCOUNT_FEE_WORKING_DATA) using index);

create table ACCOUNT_FEE_LOGS (
  ACCOUNT_FEE_LOG_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  FEE_OID                         varchar2(200),
  LAST_CHARGED_ON                 date,
  CARD_OID                        varchar2(200),
  constraint ACCOUNT_FEE_LOGS_PK primary key (ACCOUNT_FEE_LOG_OID) using index);

create table ACCOUNT_FEE_ITEMS (
  ACCOUNT_FEE_ITEM_OID            varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_FEE_OID                 varchar2(200),
  CARD_OID                        varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  TRANSACTION_FEE_OID             varchar2(200),
  constraint ACCOUNT_FEE_ITEMS_PK primary key (ACCOUNT_FEE_ITEM_OID) using index);

create table ACCOUNT_FEES (
  ACCOUNT_FEE_OID                 varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_FEE_STATUS_CID          varchar2(200),
  FEE_CALCULATION_SOURCE_CID      varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  FEE_OID                         varchar2(200),
  FEE_VALUE_OID                   varchar2(200),
  APPLIED_COUNT                   varchar2(200),
  FEE_TOTAL_AMOUNT                varchar2(200),
  FEE_TOTAL_TAX                   varchar2(200),
  RATE                            varchar2(200),
  TRANSACTION_OID                 varchar2(200),
  POSTED_AT                       varchar2(200),
  CARD_OID                        varchar2(200),
  constraint ACCOUNT_FEES_PK primary key (ACCOUNT_FEE_OID) using index);

create table ACCOUNT_EVENT_STATS (
  ACCOUNT_EVENT_STAT_OID          varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  EVENT_STATISTIC_CID             varchar2(200),
  OCCURRED_ON                     date,
  AMOUNT                          varchar2(200),
  constraint ACCOUNT_EVENT_STATS_PK primary key (ACCOUNT_EVENT_STAT_OID) using index);

create table ACCOUNT_CONTROL_PROFILES (
  ACCOUNT_CONTROL_PROFILE_OID     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  PROFILE_TYPE_CID                varchar2(200),
  CARD_TYPE_CID                   varchar2(200),
  PROFILE_CATEGORY_CID            varchar2(200),
  DESCRIPTION                     varchar2(200),
  CLIENT_MID                      varchar2(200),
  CARD_PROGRAM_OID                varchar2(200),
  APPLICATION_TYPE_OID            varchar2(200),
  APPLICATION_OID                 varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  CARD_OFFER_OID                  varchar2(200),
  CARD_PRODUCT_OID                varchar2(200),
  constraint ACCOUNT_CONTROL_PROFILES_PK primary key (ACCOUNT_CONTROL_PROFILE_OID) using index);

create table ACCOUNT_CONTROLS (
  ACCOUNT_CONTROL_OID             varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_CONTROL_PROFILE_OID     varchar2(200),
  EFFECTIVE_ON                    date,
  EXPIRES_ON                      date,
  IS_ERROR_PROD_REST_COLL         varchar2(200),
  IS_ERROR_PROD_REST_AUTH         varchar2(200),
  IS_ERROR_TIME_LIMT_COLL         varchar2(200),
  IS_ERROR_TIME_LIMT_AUTH         varchar2(200),
  VELOCITY_ASSIGNMENT_OID         varchar2(200),
  PRODUCT_RESTRICTION_OID         varchar2(200),
  TIME_LIMIT_OID                  varchar2(200),
  LOCATION_RESTRICTION_OID        varchar2(200),
  IS_ERROR_LOC_RESTRIC_COLL       varchar2(200),
  IS_ERROR_LOC_RESTRIC_AUTH       varchar2(200),
  IS_VEHICLE_ID_REQ               varchar2(200),
  IS_DRIVER_ID_REQ                varchar2(200),
  IS_ORDER_NUMBER_REQ             varchar2(200),
  IS_FLEET_ID_REQ                 varchar2(200),
  IS_CUSTOMER_SELECTION_REQ       varchar2(200),
  IS_ODOMETER_REQ                 varchar2(200),
  IS_ODOMETER_VALIDATION          varchar2(200),
  EXT_VELOCITY_CTRL_PROF_OID      varchar2(200),
  constraint ACCOUNT_CONTROLS_PK primary key (ACCOUNT_CONTROL_OID) using index);

create table ACCOUNT_AMOUNT_STATS (
  ACCOUNT_AMOUNT_STAT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_OID                     varchar2(200),
  AMOUNT_STATISTIC_CID            varchar2(200),
  TRANSACTION_COUNT               varchar2(200),
  AMOUNT                          varchar2(200),
  constraint ACCOUNT_AMOUNT_STATS_PK primary key (ACCOUNT_AMOUNT_STAT_OID) using index);

create table ACCOUNTS (
  ACCOUNT_OID                     varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCOUNT_STATUS_OID              varchar2(200),
  ACCOUNT_SUB_STATUS_OID          varchar2(200),
  CUSTOMER_MID                    varchar2(200),
  BILLING_PLAN_OID                varchar2(200),
  CREDIT_PLAN_OID                 varchar2(200),
  TERRITORY_OID                   varchar2(200),
  BILLING_FREQUENCY_OID           varchar2(200),
  CYCLE_FREQUENCY_OID             varchar2(200),
  ACCOUNT_NO                      varchar2(200),
  OPENED_ON                       date,
  ACTUAL_BALANCE                  varchar2(200),
  FINANCE_CHARGE_BALANCE          varchar2(200),
  CREDIT_LIMIT                    varchar2(200),
  PERCENT_OVER_LIMIT              varchar2(200),
  PERCENT_TRANS_ALLOW_OVER_LIMIT  varchar2(200),
  TEMP_CREDIT_AMOUNT              varchar2(200),
  AUTHORISED_BALANCE              varchar2(200),
  IS_DUNNING_USE_SUPPRESSED       varchar2(200),
  IS_OVER_CREDIT_LIMIT            varchar2(200),
  BANK_ACCOUNT_OID                varchar2(200),
  PULL_CODE_DID                   varchar2(200),
  DEBT_TRANSFER_ACCOUNT_OID       varchar2(200),
  TEMP_CREDIT_REASON_DID          varchar2(200),
  DUNNING_CODE_OID                varchar2(200),
  LAST_DUNNING_CODE_OID           varchar2(200),
  FEE_PROFILE_OID                 varchar2(200),
  EXTERNAL_ACCOUNT_CODE           varchar2(200),
  FEE_PROFILE_ASSIGNED_ON         date,
  TEMP_CREDIT_EXPIRES_ON          date,
  CLOSED_ON                       date,
  REINSTATED_ON                   date,
  LAST_BILLED_ON                  date,
  CUST_SERVICE_EXPIRES_ON         date,
  CUST_SERVICE_PULL_INITS         varchar2(200),
  TAX_NO                          varchar2(200),
  IS_SUND_POST_ACCOUNT            varchar2(200),
  IS_CARD_SYSTEM_ONLY             varchar2(200),
  ALERT_THRESHOLD_PERCENTAGE      varchar2(200),
  CREDIT_CARD_OID                 varchar2(200),
  DEPOSIT_BALANCE                 varchar2(200),
  ALERT_TYPE_CID                  varchar2(200),
  ALERT_THRESHOLD_AMOUNT          varchar2(200),
  IS_CLIENT_BAL_ALERT_SENT        varchar2(200),
  IS_CUST_BAL_ALERT_SENT          varchar2(200),
  REQUEST_TO_PAY_THRESHOLD        varchar2(200),
  SECURITY_INDICATOR              varchar2(200),
  TOTAL_SECURITY_VALUE            varchar2(200),
  TRADE_EXPOSURE_LIMIT            varchar2(200),
  RISK_GRADE_DID                  varchar2(200),
  PAYMENT_THRESHOLD_DID           varchar2(200),
  BEHAVIOURAL_SCORE               varchar2(200),
  CARD_BALANCE_ALERTS_ENABLED     varchar2(200),
  CARD_BALANCE_ALERT_THRESHOLD    varchar2(200),
  constraint ACCOUNTS_PK primary key (ACCOUNT_OID) using index);

create table ACCESS_GROUP_CLIENTS (
  ACCESS_GROUP_CLIENT_OID         varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCESS_GROUP_OID                varchar2(200),
  CLIENT_MID                      varchar2(200),
  constraint ACCESS_GROUP_CLIENTS_PK primary key (ACCESS_GROUP_CLIENT_OID) using index);

create table ACCESS_GROUPS (
  ACCESS_GROUP_OID                varchar2(200),
  UPDATE_COUNT                    varchar2(200),
  LAST_UPDATED_AT                 date,
  LAST_UPDATED_BY                 varchar2(200),
  ACCESS_MEMBER_TYPE_CID          varchar2(200),
  LOCKOUT_INACTIVITY_DAYS         varchar2(200),
  MAX_PASSWORD_ATTEMPTS           varchar2(200),
  UNIQUE_PASSWORD_RETAINED        varchar2(200),
  IS_PASSWORD_EXPIRY_FORCED       varchar2(200),
  PASSWORD_EXPIRY_DAYS            varchar2(200),
  DESCRIPTION                     varchar2(200),
  IS_LOCKED_OUT                   varchar2(200),
  CLIENT_GROUP_MID                varchar2(200),
  IS_SYSTEM_MENU_ACCESS           varchar2(200),
  constraint ACCESS_GROUPS_PK primary key (ACCESS_GROUP_OID) using index);

alter table WORK_QUEUE_ACTIONS
  add constraint WORK_QUEUE_ACTIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table WORK_QUEUE_ACTIONS
  add constraint WORK_QUEUE_ACTIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUES
  add constraint WORK_QUEUES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table WORK_QUEUES
  add constraint WORK_QUEUES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUES
  add constraint WORK_QUEUES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUES
  add constraint WORK_QUEUES_FK4
  foreign key (WORK_QUEUE_USER_GROUP_OID) references WORK_QUEUE_USER_GROUPS(WORK_QUEUE_USER_GROUP_OID);

alter table WORK_FILE_BATCH_CONTROLS
  add constraint WORK_FILE_BATCH_CONTROLS_FK1
  foreign key (WORK_FILE_OID) references WORK_FILES(WORK_FILE_OID);

alter table WALLET_CARDS
  add constraint WALLET_CARDS_FK1
  foreign key (WALLET) references WALLET(WALLET);

alter table WALLET
  add constraint WALLET_FK1
  foreign key (INTERNET_USER_OID) references INTERNET_USERS(INTERNET_USER_OID);

alter table VELOCITY_TYPE_VALUES
  add constraint VELOCITY_TYPE_VALUES_FK1
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table VELOCITY_TYPE_VALUES
  add constraint VELOCITY_TYPE_VALUES_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table VELOCITY_TYPE_VALUES
  add constraint VELOCITY_TYPE_VALUES_FK3
  foreign key (VELOCITY_TYPE_OID) references VELOCITY_TYPES(VELOCITY_TYPE_OID);

alter table VELOCITY_TYPE_PRODUCTS
  add constraint VELOCITY_TYPE_PRODUCTS_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table VELOCITY_TYPE_PRODUCTS
  add constraint VELOCITY_TYPE_PRODUCTS_FK2
  foreign key (VELOCITY_TYPE_OID) references VELOCITY_TYPES(VELOCITY_TYPE_OID);

alter table VELOCITY_TYPES
  add constraint VELOCITY_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table VELOCITY_TYPES
  add constraint VELOCITY_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VELOCITY_TYPES
  add constraint VELOCITY_TYPES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VELOCITY_TYPES
  add constraint VELOCITY_TYPES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VELOCITY_TYPES
  add constraint VELOCITY_TYPES_FK5
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK1
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK2
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK3
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK4
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK5
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK6
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK7
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VELOCITY_ASSIGNMENTS
  add constraint VELOCITY_ASSIGNMENTS_FK8
  foreign key (VELOCITY_TYPE_VALUE_OID) references VELOCITY_TYPE_VALUES(VELOCITY_TYPE_VALUE_OID);

alter table VEHICLE_PRODUCT_TOTALS
  add constraint VEHICLE_PRODUCT_TOTALS_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table VEHICLE_PRODUCT_TOTALS
  add constraint VEHICLE_PRODUCT_TOTALS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VEHICLE_PRODUCT_TOTALS
  add constraint VEHICLE_PRODUCT_TOTALS_FK3
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table VEHICLE_ODO_READINGS
  add constraint VEHICLE_ODO_READINGS_FK1
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table VEHICLE_ODO_READINGS
  add constraint VEHICLE_ODO_READINGS_FK2
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table VEHICLE_ODOMETER_SPANS
  add constraint VEHICLE_ODOMETER_SPANS_FK1
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table VEHICLES
  add constraint VEHICLES_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table VEHICLES
  add constraint VEHICLES_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table VEHICLES
  add constraint VEHICLES_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table VEHICLES
  add constraint VEHICLES_FK4
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table VEHICLES
  add constraint VEHICLES_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VEHICLES
  add constraint VEHICLES_FK6
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table VALIDATION_FIELDS
  add constraint VALIDATION_FIELDS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK2
  foreign key (ACCESS_GROUP_OID) references ACCESS_GROUPS(ACCESS_GROUP_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK3
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK5
  foreign key (VALIDATION_CONTEXT_FLD_OID) references VALIDATION_CONTEXT_FLDS(VALIDATION_CONTEXT_FLD_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK7
  foreign key (VALIDATION_FIELD_OID) references VALIDATION_FIELDS(VALIDATION_FIELD_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table VALIDATION_CONTROLS
  add constraint VALIDATION_CONTROLS_FK10
  foreign key (INTERNET_ACCESS_GROUP_OID) references INTERNET_ACCESS_GROUPS(INTERNET_ACCESS_GROUP_OID);

alter table VALIDATION_CONTEXT_FLDS
  add constraint VALIDATION_CONTEXT_FLDS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table USER_RELATIONSHIPS
  add constraint USER_RELATIONSHIPS_FK1
  foreign key (USER_OID) references USERS(USER_OID);

alter table USER_RELATIONSHIPS
  add constraint USER_RELATIONSHIPS_FK2
  foreign key (USER_OID) references USERS(USER_OID);

alter table USER_PASSWORD_LOGS
  add constraint USER_PASSWORD_LOGS_FK1
  foreign key (USER_OID) references USERS(USER_OID);

alter table USERS
  add constraint USERS_FK1
  foreign key (ACCESS_GROUP_OID) references ACCESS_GROUPS(ACCESS_GROUP_OID);

alter table USERS
  add constraint USERS_FK2
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table USERS
  add constraint USERS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table UNALLOCATED_FUNDS
  add constraint UNALLOCATED_FUNDS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table UNALLOCATED_FUNDS
  add constraint UNALLOCATED_FUNDS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table UI_PROD_RESTRICT_TITLES
  add constraint UI_PROD_RESTRICT_TITLES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table UI_PROD_RESTRICT_MAP_ITEMS
  add constraint UI_PROD_RESTRICT_MAP_ITEMS_FK1
  foreign key (UI_PROD_RESTRICT_ITEM_OID) references UI_PROD_RESTRICT_ITEMS(UI_PROD_RESTRICT_ITEM_OID);

alter table UI_PROD_RESTRICT_MAP_ITEMS
  add constraint UI_PROD_RESTRICT_MAP_ITEMS_FK2
  foreign key (UI_PROD_RESTRICT_MAP_OID) references UI_PROD_RESTRICT_MAPS(UI_PROD_RESTRICT_MAP_OID);

alter table UI_PROD_RESTRICT_MAPS
  add constraint UI_PROD_RESTRICT_MAPS_FK1
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table UI_PROD_RESTRICT_ITEMS
  add constraint UI_PROD_RESTRICT_ITEMS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table UI_PROD_RESTRICT_ITEMS
  add constraint UI_PROD_RESTRICT_ITEMS_FK2
  foreign key (UI_PROD_RESTRICT_TITLE_OID) references UI_PROD_RESTRICT_TITLES(UI_PROD_RESTRICT_TITLE_OID);

alter table TRANS_TRANSLATIONS
  add constraint TRANS_TRANSLATIONS_FK1
  foreign key (BANKING_ACCOUNT_MAP) references BANKING_ACCOUNT_MAP(BANKING_ACCOUNT_MAP);

alter table TRANS_TRANSLATIONS
  add constraint TRANS_TRANSLATIONS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table TRANS_TRANSLATIONS
  add constraint TRANS_TRANSLATIONS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_STEPS_ERRORS
  add constraint TRANS_STEPS_ERRORS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table TRANS_STEPS_ERRORS
  add constraint TRANS_STEPS_ERRORS_FK2
  foreign key (TRANSACTION_STEP_OID) references TRANSACTION_STEPS(TRANSACTION_STEP_OID);

alter table TRANS_REPORT_CATEGORIES
  add constraint TRANS_REPORT_CATEGORIES_FK1
  foreign key (CLIENT_TRANS_TYPE_OID) references CLIENT_TRANS_TYPES(CLIENT_TRANS_TYPE_OID);

alter table TRANS_REPORT_CATEGORIES
  add constraint TRANS_REPORT_CATEGORIES_FK2
  foreign key (REPORT_CATEGORIE_OID) references REPORT_CATEGORIES(REPORT_CATEGORIE_OID);

alter table TRANS_REFUND_TYPES
  add constraint TRANS_REFUND_TYPES_FK1
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_REFUND_TYPES
  add constraint TRANS_REFUND_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_REBATE_BREAKDOWNS
  add constraint TRANS_REBATE_BREAKDOWNS_FK1
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table TRANS_REBATE_BREAKDOWNS
  add constraint TRANS_REBATE_BREAKDOWNS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table TRANS_REBATE_BREAKDOWNS
  add constraint TRANS_REBATE_BREAKDOWNS_FK3
  foreign key (TRANSACTION_REBATE_OID) references TRANSACTION_REBATES(TRANSACTION_REBATE_OID);

alter table TRANS_DISPUTE_TYPES
  add constraint TRANS_DISPUTE_TYPES_FK1
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_DISPUTE_TYPES
  add constraint TRANS_DISPUTE_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_DISPUTE_TYPES
  add constraint TRANS_DISPUTE_TYPES_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_DISPUTE_TYPES
  add constraint TRANS_DISPUTE_TYPES_FK4
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_DISPUTE_TYPES
  add constraint TRANS_DISPUTE_TYPES_FK5
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK2
  foreign key (BILLING_PLAN_OID) references BILLING_PLANS(BILLING_PLAN_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK4
  foreign key (CREDIT_PLAN_OID) references CREDIT_PLANS(CREDIT_PLAN_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK5
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK6
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table WORK_QUEUE_USER_GROUPS
  add constraint WORK_QUEUE_USER_GROUPS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK3
  foreign key (DIARY_NOTE_OID) references DIARY_NOTES(DIARY_NOTE_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK4
  foreign key (DISPUTED_TRANSACTION_OID) references DISPUTED_TRANSACTIONS(DISPUTED_TRANSACTION_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK5
  foreign key (COLLECTION_EVENT_OID) references COLLECTION_EVENTS(COLLECTION_EVENT_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK6
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK8
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table WORK_QUEUE_ITEMS
  add constraint WORK_QUEUE_ITEMS_FK9
  foreign key (USER_OID) references USERS(USER_OID);

alter table WORK_QUEUE_GROUP_USERS
  add constraint WORK_QUEUE_GROUP_USERS_FK1
  foreign key (WORK_QUEUE_USER_GROUP_OID) references WORK_QUEUE_USER_GROUPS(WORK_QUEUE_USER_GROUP_OID);

alter table WORK_QUEUE_GROUP_USERS
  add constraint WORK_QUEUE_GROUP_USERS_FK2
  foreign key (USER_OID) references USERS(USER_OID);

alter table WORK_QUEUE_ACTION_ITEMS
  add constraint WORK_QUEUE_ACTION_ITEMS_FK1
  foreign key (WORK_QUEUE_ACTION_OID) references WORK_QUEUE_ACTIONS(WORK_QUEUE_ACTION_OID);

alter table WORK_QUEUE_ACTION_ITEMS
  add constraint WORK_QUEUE_ACTION_ITEMS_FK2
  foreign key (WORK_QUEUE_ITEM_OID) references WORK_QUEUE_ITEMS(WORK_QUEUE_ITEM_OID);

alter table WORK_QUEUE_ACTION_ITEMS
  add constraint WORK_QUEUE_ACTION_ITEMS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table WORK_QUEUE_ACTION_ITEMS
  add constraint WORK_QUEUE_ACTION_ITEMS_FK4
  foreign key (USER_OID) references USERS(USER_OID);

alter table TRANS_ADJUSTMENT_TYPES
  add constraint TRANS_ADJUSTMENT_TYPES_FK1
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_ADJUSTMENT_TYPES
  add constraint TRANS_ADJUSTMENT_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_ADJUSTMENT_TYPES
  add constraint TRANS_ADJUSTMENT_TYPES_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANS_ADJUSTMENT_ITEMS
  add constraint TRANS_ADJUSTMENT_ITEMS_FK1
  foreign key (ADJUSTMENT_TYPE_OID) references ADJUSTMENT_TYPES(ADJUSTMENT_TYPE_OID);

alter table TRANS_ADJUSTMENT_ITEMS
  add constraint TRANS_ADJUSTMENT_ITEMS_FK2
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table TRANS_ADJUSTMENT_ITEMS
  add constraint TRANS_ADJUSTMENT_ITEMS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table TRANS_ADJUSTMENT_ITEMS
  add constraint TRANS_ADJUSTMENT_ITEMS_FK4
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSLATIONS
  add constraint TRANSLATIONS_FK1
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table TRANSACTION_TYPE_STEPS
  add constraint TRANSACTION_TYPE_STEPS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_TYPE_STEPS
  add constraint TRANSACTION_TYPE_STEPS_FK2
  foreign key (TRANSACTION_STEP_OID) references TRANSACTION_STEPS(TRANSACTION_STEP_OID);

alter table TRANSACTION_TYPE_STEPS
  add constraint TRANSACTION_TYPE_STEPS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANSACTION_TYPES
  add constraint TRANSACTION_TYPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_TYPES
  add constraint TRANSACTION_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_TYPES
  add constraint TRANSACTION_TYPES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_TYPES
  add constraint TRANSACTION_TYPES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_STEPS
  add constraint TRANSACTION_STEPS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_REBATES
  add constraint TRANSACTION_REBATES_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table TRANSACTION_REBATES
  add constraint TRANSACTION_REBATES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_REBATES
  add constraint TRANSACTION_REBATES_FK3
  foreign key (REBATE_OID) references REBATES(REBATE_OID);

alter table TRANSACTION_REBATES
  add constraint TRANSACTION_REBATES_FK4
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_POINTS
  add constraint TRANSACTION_POINTS_FK1
  foreign key (POINT_PROGRAM_OID) references POINT_PROGRAMS(POINT_PROGRAM_OID);

alter table TRANSACTION_POINTS
  add constraint TRANSACTION_POINTS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_NOTES
  add constraint TRANSACTION_NOTES_FK1
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK1
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK2
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK3
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK4
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK6
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK7
  foreign key (PRODUCT_TRANSLATION_OID) references PRODUCT_TRANSLATIONS(PRODUCT_TRANSLATION_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK8
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_LINE_ITEMS
  add constraint TRANSACTION_LINE_ITEMS_FK9
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table TRANSACTION_FEES
  add constraint TRANSACTION_FEES_FK1
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table TRANSACTION_FEES
  add constraint TRANSACTION_FEES_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_EXCEPTIONS
  add constraint TRANSACTION_EXCEPTIONS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table TRANSACTION_EXCEPTIONS
  add constraint TRANSACTION_EXCEPTIONS_FK2
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table TRANSACTION_EXCEPTIONS
  add constraint TRANSACTION_EXCEPTIONS_FK3
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table TRANSACTION_EXCEPTIONS
  add constraint TRANSACTION_EXCEPTIONS_FK4
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table TRANSACTION_ERRORS
  add constraint TRANSACTION_ERRORS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTION_ERRORS
  add constraint TRANSACTION_ERRORS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK2
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK3
  foreign key (CAPTURE_TYPE_OID) references CAPTURE_TYPES(CAPTURE_TYPE_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK4
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK5
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK6
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK7
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK8
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK9
  foreign key (LOCATION_RECONCILIATION_OID) references LOCATION_RECONCILIATIONS(LOCATION_RECONCILIATION_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK10
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK11
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK12
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK13
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK14
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK15
  foreign key (PRODUCT_TRANSLATION_OID) references PRODUCT_TRANSLATIONS(PRODUCT_TRANSLATION_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK16
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK17
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK18
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table TRANSACTIONS
  add constraint TRANSACTIONS_FK19
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table TIME_LIMITS
  add constraint TIME_LIMITS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table TERRITORIES
  add constraint TERRITORIES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table TERRITORIES
  add constraint TERRITORIES_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table TERRITORIES
  add constraint TERRITORIES_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table TERRITORIES
  add constraint TERRITORIES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TERRITORIES
  add constraint TERRITORIES_FK5
  foreign key (USER_OID) references USERS(USER_OID);

alter table TERMINALS
  add constraint TERMINALS_FK1
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table TERMINALS
  add constraint TERMINALS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TERMINALS
  add constraint TERMINALS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TAX_RATES
  add constraint TAX_RATES_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table TAX_RATES
  add constraint TAX_RATES_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table TAX_RATES
  add constraint TAX_RATES_FK3
  foreign key (RATE_CODE_OID) references RATE_CODES(RATE_CODE_OID);

alter table TAX_NUMBERS
  add constraint TAX_NUMBERS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table TAX_NUMBERS
  add constraint TAX_NUMBERS_FK2
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table TAX_NUMBERS
  add constraint TAX_NUMBERS_FK3
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table TAX_NUMBERS
  add constraint TAX_NUMBERS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SYS_TOT_TRANS_TYPES
  add constraint SYS_TOT_TRANS_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table SYS_TOT_TRANS_TYPES
  add constraint SYS_TOT_TRANS_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SYS_TOT_TRANS_TYPES
  add constraint SYS_TOT_TRANS_TYPES_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table SYS_TOT_PROCESS_ERRORS
  add constraint SYS_TOT_PROCESS_ERRORS_FK1
  foreign key (SYS_TOT_PROCESS_CONTROL_OID) references SYS_TOT_PROCESS_CONTROLS(SYS_TOT_PROCESS_CONTROL_OID);

alter table SYS_TOT_PROCESS_ERRORS
  add constraint SYS_TOT_PROCESS_ERRORS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table SYS_TOT_PROCESS_ERRORS
  add constraint SYS_TOT_PROCESS_ERRORS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table SYS_TOT_PROCESS_ERRORS
  add constraint SYS_TOT_PROCESS_ERRORS_FK4
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table SYS_TOT_PROCESS_ERRORS
  add constraint SYS_TOT_PROCESS_ERRORS_FK5
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table SYS_TOT_PROCESS_CONTROLS
  add constraint SYS_TOT_PROCESS_CONTROLS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table SYS_TOT_CARD_PGM_TRANS
  add constraint SYS_TOT_CARD_PGM_TRANS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table SYS_TOT_CARD_PGM_TRANS
  add constraint SYS_TOT_CARD_PGM_TRANS_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table SYS_TOT_CARD_PGM_TRANS
  add constraint SYS_TOT_CARD_PGM_TRANS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table SUSPENSE_TRANS_AUDITS
  add constraint SUSPENSE_TRANS_AUDITS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table SUSPENSE_TRANS_AUDITS
  add constraint SUSPENSE_TRANS_AUDITS_FK2
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table SUSPENSE_TRANS_AUDITS
  add constraint SUSPENSE_TRANS_AUDITS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SUSPENSE_ITEM_AUDITS
  add constraint SUSPENSE_ITEM_AUDITS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table SUSPENSE_ITEM_AUDITS
  add constraint SUSPENSE_ITEM_AUDITS_FK2
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table SUSPENSE_ITEM_AUDITS
  add constraint SUSPENSE_ITEM_AUDITS_FK3
  foreign key (SUSPENSE_TRANS_AUDIT_OID) references SUSPENSE_TRANS_AUDITS(SUSPENSE_TRANS_AUDIT_OID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK2
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK3
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK4
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SUSPENDED_TRANSACTIONS
  add constraint SUSPENDED_TRANSACTIONS_FK6
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table SUSPENDED_LINE_ITEMS
  add constraint SUSPENDED_LINE_ITEMS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table SUSPENDED_LINE_ITEMS
  add constraint SUSPENDED_LINE_ITEMS_FK2
  foreign key (POS_TRANS_LINE_ITEM_OID) references POS_TRANS_LINE_ITEMS(POS_TRANS_LINE_ITEM_OID);

alter table SUSPENDED_LINE_ITEMS
  add constraint SUSPENDED_LINE_ITEMS_FK3
  foreign key (SUSPENDED_TRANSACTION_OID) references SUSPENDED_TRANSACTIONS(SUSPENDED_TRANSACTION_OID);

alter table SUNDRY_HOLDINGS_LOGS
  add constraint SUNDRY_HOLDINGS_LOGS_FK1
  foreign key (SUNDRY_HOLDING_OID) references SUNDRY_HOLDINGS(SUNDRY_HOLDING_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK2
  foreign key (ADJUSTMENT_TYPE_OID) references ADJUSTMENT_TYPES(ADJUSTMENT_TYPE_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK4
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK6
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK7
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK8
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK9
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK10
  foreign key (REQUEST_TRANS_TYPE_OID) references REQUEST_TRANS_TYPES(REQUEST_TRANS_TYPE_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SUNDRY_HOLDINGS
  add constraint SUNDRY_HOLDINGS_FK12
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SUBSCRIPTIONS
  add constraint SUBSCRIPTIONS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table SUBSCRIPTIONS
  add constraint SUBSCRIPTIONS_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table SUBSCRIPTIONS
  add constraint SUBSCRIPTIONS_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table SUBSCRIPTIONS
  add constraint SUBSCRIPTIONS_FK4
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK3
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK4
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK5
  foreign key (RPT_EXTR_CUSTOMER_OID) references RPT_EXTR_CUSTOMERS(RPT_EXTR_CUSTOMER_OID);

alter table STORED_REPORTS
  add constraint STORED_REPORTS_FK6
  foreign key (RPT_EXTR_MERCHANT_OID) references RPT_EXTR_MERCHANTS(RPT_EXTR_MERCHANT_OID);

alter table STORED_ATTACHMENTS
  add constraint STORED_ATTACHMENTS_FK1
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table STORED_ATTACHMENTS
  add constraint STORED_ATTACHMENTS_FK2
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table STORED_ALERTS
  add constraint STORED_ALERTS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table STORED_ALERTS
  add constraint STORED_ALERTS_FK2
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table STORED_ALERTS
  add constraint STORED_ALERTS_FK3
  foreign key (TRANSACTION_EXCEPTION_OID) references TRANSACTION_EXCEPTIONS(TRANSACTION_EXCEPTION_OID);

alter table STORED_ALERTS
  add constraint STORED_ALERTS_FK4
  foreign key (REPORT_DELIVERY_TYPE_OID) references REPORT_DELIVERY_TYPES(REPORT_DELIVERY_TYPE_OID);

alter table STATES
  add constraint STATES_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table STATEMENT_MESSAGES
  add constraint STATEMENT_MESSAGES_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table SETTINGS
  add constraint SETTINGS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SETTINGS
  add constraint SETTINGS_FK2
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table SETTINGS
  add constraint SETTINGS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table SETTINGS
  add constraint SETTINGS_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table SERVER_CALL_STATISTICS
  add constraint SERVER_CALL_STATISTICS_FK1
  foreign key (SERVER_CALL_REPLAY_OID) references SERVER_CALL_REPLAYS(SERVER_CALL_REPLAY_OID);

alter table SERVER_CALL_REPLAY_STEPS
  add constraint SERVER_CALL_REPLAY_STEPS_FK1
  foreign key (SERVER_CALL_REPLAY_OID) references SERVER_CALL_REPLAYS(SERVER_CALL_REPLAY_OID);

alter table RPT_EXTR_TRANS_NOTES
  add constraint RPT_EXTR_TRANS_NOTES_FK1
  foreign key (RPT_EXTR_TRANSACTION_OID) references RPT_EXTR_TRANSACTIONS(RPT_EXTR_TRANSACTION_OID);

alter table RPT_EXTR_TRANS_KEYS
  add constraint RPT_EXTR_TRANS_KEYS_FK1
  foreign key (RPT_EXTR_TRANSACTION_OID) references RPT_EXTR_TRANSACTIONS(RPT_EXTR_TRANSACTION_OID);

alter table RPT_EXTR_TRANS_FEES
  add constraint RPT_EXTR_TRANS_FEES_FK1
  foreign key (RPT_EXTR_TRANSACTION_OID) references RPT_EXTR_TRANSACTIONS(RPT_EXTR_TRANSACTION_OID);

alter table RPT_EXTR_MERC_TRANS_NOTES
  add constraint RPT_EXTR_MERC_TRANS_NOTES_FK1
  foreign key (RPT_EXTR_MERC_TRAN_OID) references RPT_EXTR_MERC_TRANS(RPT_EXTR_MERC_TRAN_OID);

alter table RPT_EXTR_MERC_TRANS
  add constraint RPT_EXTR_MERC_TRANS_FK1
  foreign key (RPT_EXTR_MERC_RECON_OID) references RPT_EXTR_MERC_RECONS(RPT_EXTR_MERC_RECON_OID);

alter table RPT_EXTR_MERC_ITEMS
  add constraint RPT_EXTR_MERC_ITEMS_FK1
  foreign key (RPT_EXTR_MERC_TRAN_OID) references RPT_EXTR_MERC_TRANS(RPT_EXTR_MERC_TRAN_OID);

alter table RPT_EXTR_MERC_CHILDREN
  add constraint RPT_EXTR_MERC_CHILDREN_FK1
  foreign key (RPT_EXTR_MERCHANT_OID) references RPT_EXTR_MERCHANTS(RPT_EXTR_MERCHANT_OID);

alter table RPT_EXTR_LINE_ITEMS
  add constraint RPT_EXTR_LINE_ITEMS_FK1
  foreign key (RPT_EXTR_TRANSACTION_OID) references RPT_EXTR_TRANSACTIONS(RPT_EXTR_TRANSACTION_OID);

alter table RPT_EXTR_FINANCIALS
  add constraint RPT_EXTR_FINANCIALS_FK1
  foreign key (RPT_EXTR_CUSTOMER_OID) references RPT_EXTR_CUSTOMERS(RPT_EXTR_CUSTOMER_OID);

alter table RPT_EXTR_CHILDREN
  add constraint RPT_EXTR_CHILDREN_FK1
  foreign key (RPT_EXTR_CUSTOMER_OID) references RPT_EXTR_CUSTOMERS(RPT_EXTR_CUSTOMER_OID);

alter table RFCO_REQUESTS
  add constraint RFCO_REQUESTS_FK1
  foreign key (DISPUTED_TRANSACTION_OID) references DISPUTED_TRANSACTIONS(DISPUTED_TRANSACTION_OID);

alter table RFCO_REQUESTS
  add constraint RFCO_REQUESTS_FK2
  foreign key (SUSPENDED_TRANSACTION_OID) references SUSPENDED_TRANSACTIONS(SUSPENDED_TRANSACTION_OID);

alter table RESTART_PROCESS
  add constraint RESTART_PROCESS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK3
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK4
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK5
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table REQUEST_TRANS_TYPES
  add constraint REQUEST_TRANS_TYPES_FK6
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table REPRICE_BATCH_HEADERS
  add constraint REPRICE_BATCH_HEADERS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPRICE_BATCH_HEADERS
  add constraint REPRICE_BATCH_HEADERS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPRICE_BATCH_HEADERS
  add constraint REPRICE_BATCH_HEADERS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPRICE_BATCH_HEADERS
  add constraint REPRICE_BATCH_HEADERS_FK4
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table REPRICE_BATCH_HEADERS
  add constraint REPRICE_BATCH_HEADERS_FK5
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table REPRICE_ADJ_LINE_ITEMS
  add constraint REPRICE_ADJ_LINE_ITEMS_FK1
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table REPRICE_ADJ_LINE_ITEMS
  add constraint REPRICE_ADJ_LINE_ITEMS_FK2
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table REPRICE_ADJ_LINE_ITEMS
  add constraint REPRICE_ADJ_LINE_ITEMS_FK3
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table REPRICE_ADJ_LINE_ITEMS
  add constraint REPRICE_ADJ_LINE_ITEMS_FK4
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table REPRICE_ADJ_LINE_ITEMS
  add constraint REPRICE_ADJ_LINE_ITEMS_FK5
  foreign key (REPRICE_ADJUSTMENT_OID) references REPRICE_ADJUSTMENTS(REPRICE_ADJUSTMENT_OID);

alter table REPRICE_ADJUSTMENTS
  add constraint REPRICE_ADJUSTMENTS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPRICE_ADJUSTMENTS
  add constraint REPRICE_ADJUSTMENTS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table REPRICE_ADJUSTMENTS
  add constraint REPRICE_ADJUSTMENTS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPRICE_ADJUSTMENTS
  add constraint REPRICE_ADJUSTMENTS_FK4
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table REPRICE_ADJUSTMENTS
  add constraint REPRICE_ADJUSTMENTS_FK5
  foreign key (REPRICE_BATCH_HEADER_OID) references REPRICE_BATCH_HEADERS(REPRICE_BATCH_HEADER_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK4
  foreign key (DETAIL_GROUP_TYPE_OID) references DETAIL_GROUP_TYPES(DETAIL_GROUP_TYPE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK5
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK9
  foreign key (REPORT_CATEGORIE_OID) references REPORT_CATEGORIES(REPORT_CATEGORIE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK10
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK11
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK12
  foreign key (REPORT_DELIVERY_TYPE_OID) references REPORT_DELIVERY_TYPES(REPORT_DELIVERY_TYPE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK13
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK14
  foreign key (REPORT_DELIVERY_TYPE_OID) references REPORT_DELIVERY_TYPES(REPORT_DELIVERY_TYPE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK15
  foreign key (REPORT_FREQUENCIE_OID) references REPORT_FREQUENCIES(REPORT_FREQUENCIE_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK16
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK17
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TYPES
  add constraint REPORT_TYPES_FK18
  foreign key (REPORT_SORT_OPTION_OID) references REPORT_SORT_OPTIONS(REPORT_SORT_OPTION_OID);

alter table REPORT_TRANSACTION_ERRORS
  add constraint REPORT_TRANSACTION_ERRORS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table REPORT_TRANSACTION_ERRORS
  add constraint REPORT_TRANSACTION_ERRORS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_TRANSACTION_ERRORS
  add constraint REPORT_TRANSACTION_ERRORS_FK3
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_TEMPLATE_PARAMS
  add constraint REPORT_TEMPLATE_PARAMS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TEMPLATE_PARAMS
  add constraint REPORT_TEMPLATE_PARAMS_FK2
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table REPORT_TEMPLATES
  add constraint REPORT_TEMPLATES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_TEMPLATES
  add constraint REPORT_TEMPLATES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_SORT_OPTIONS
  add constraint REPORT_SORT_OPTIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_SORT_OPTIONS
  add constraint REPORT_SORT_OPTIONS_FK2
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_SORT_OPTIONS
  add constraint REPORT_SORT_OPTIONS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_FREQUENCIES
  add constraint REPORT_FREQUENCIES_FK1
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table REPORT_FREQUENCIES
  add constraint REPORT_FREQUENCIES_FK2
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_FREQUENCIES
  add constraint REPORT_FREQUENCIES_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_DELIVERY_TYPES
  add constraint REPORT_DELIVERY_TYPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_DELIVERY_TYPES
  add constraint REPORT_DELIVERY_TYPES_FK2
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table REPORT_DELIVERY_TYPES
  add constraint REPORT_DELIVERY_TYPES_FK3
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_DELIVERY_TYPES
  add constraint REPORT_DELIVERY_TYPES_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_DELIVERY_TYPES
  add constraint REPORT_DELIVERY_TYPES_FK5
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table REPORT_COVER_PAGES
  add constraint REPORT_COVER_PAGES_FK1
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table REPORT_COVER_PAGES
  add constraint REPORT_COVER_PAGES_FK2
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table REPORT_CATEGORIES
  add constraint REPORT_CATEGORIES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_CATEGORIES
  add constraint REPORT_CATEGORIES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REPORT_ASSIGNMENT_PARAMS
  add constraint REPORT_ASSIGNMENT_PARAMS_FK1
  foreign key (REPORT_TEMPLATE_PARAM_OID) references REPORT_TEMPLATE_PARAMS(REPORT_TEMPLATE_PARAM_OID);

alter table REPORT_ASSIGNMENT_PARAMS
  add constraint REPORT_ASSIGNMENT_PARAMS_FK2
  foreign key (REPORT_ASSIGNMENT_OID) references REPORT_ASSIGNMENTS(REPORT_ASSIGNMENT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK3
  foreign key (REPORT_DELIVERY_TYPE_OID) references REPORT_DELIVERY_TYPES(REPORT_DELIVERY_TYPE_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK4
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK5
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK6
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK7
  foreign key (RELATIONSHIP_OID) references RELATIONSHIPS(RELATIONSHIP_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK8
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REPORT_ASSIGNMENTS
  add constraint REPORT_ASSIGNMENTS_FK12
  foreign key (REPORT_SORT_OPTION_OID) references REPORT_SORT_OPTIONS(REPORT_SORT_OPTION_OID);

alter table REMITTANCE_NUMBERS
  add constraint REMITTANCE_NUMBERS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REMITTANCE_NUMBERS
  add constraint REMITTANCE_NUMBERS_FK2
  foreign key (DETAIL_GROUP_TYPE_OID) references DETAIL_GROUP_TYPES(DETAIL_GROUP_TYPE_OID);

alter table RELATIONSHIP_ASSIGNMENTS
  add constraint RELATIONSHIP_ASSIGNMENTS_FK1
  foreign key (RELATIONSHIP_OID) references RELATIONSHIPS(RELATIONSHIP_OID);

alter table RELATIONSHIPS
  add constraint RELATIONSHIPS_FK1
  foreign key (HIERARCHIE_OID) references HIERARCHIES(HIERARCHIE_OID);

alter table RELATIONSHIPS
  add constraint RELATIONSHIPS_FK2
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table RELATIONSHIPS
  add constraint RELATIONSHIPS_FK3
  foreign key (RELATIONSHIP_OID) references RELATIONSHIPS(RELATIONSHIP_OID);

alter table REBATE_VALUES
  add constraint REBATE_VALUES_FK1
  foreign key (REBATE_OID) references REBATES(REBATE_OID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK3
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK5
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_PROFILES
  add constraint REBATE_PROFILES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_OUT_OPTIONS
  add constraint REBATE_OUT_OPTIONS_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table REBATE_OUT_OPTIONS
  add constraint REBATE_OUT_OPTIONS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table REBATE_OUT_OPTIONS
  add constraint REBATE_OUT_OPTIONS_FK3
  foreign key (REBATE_PROFILE_OID) references REBATE_PROFILES(REBATE_PROFILE_OID);

alter table REBATE_CONTRIBUTIONS
  add constraint REBATE_CONTRIBUTIONS_FK1
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table REBATE_CONTRIBUTIONS
  add constraint REBATE_CONTRIBUTIONS_FK2
  foreign key (REBATE_OID) references REBATES(REBATE_OID);

alter table REBATE_CONTRIBUTIONS
  add constraint REBATE_CONTRIBUTIONS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_COMPONENT_TYPES
  add constraint REBATE_COMPONENT_TYPES_FK1
  foreign key (REBATE_CATEGORIE_OID) references REBATE_CATEGORIES(REBATE_CATEGORIE_OID);

alter table REBATE_COMPONENT_TYPES
  add constraint REBATE_COMPONENT_TYPES_FK2
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK7
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK8
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table REBATE_CATEGORIES
  add constraint REBATE_CATEGORIES_FK9
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table REBATES
  add constraint REBATES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table REBATES
  add constraint REBATES_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table REBATES
  add constraint REBATES_FK3
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table REBATES
  add constraint REBATES_FK4
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table REBATES
  add constraint REBATES_FK5
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table REBATES
  add constraint REBATES_FK6
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table REBATES
  add constraint REBATES_FK7
  foreign key (REBATE_CATEGORIE_OID) references REBATE_CATEGORIES(REBATE_CATEGORIE_OID);

alter table REBATES
  add constraint REBATES_FK8
  foreign key (REBATE_PROFILE_OID) references REBATE_PROFILES(REBATE_PROFILE_OID);

alter table RATE_CODE_VALUES
  add constraint RATE_CODE_VALUES_FK1
  foreign key (RATE_CODE_OID) references RATE_CODES(RATE_CODE_OID);

alter table RATE_CODES
  add constraint RATE_CODES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PUMP_POS_PROMPT_ENCODING
  add constraint PUMP_POS_PROMPT_ENCODING_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PUMP_POS_PROMPT_ENCODING
  add constraint PUMP_POS_PROMPT_ENCODING_FK2
  foreign key (PUMP_CONTROL_OID) references PUMP_CONTROLS(PUMP_CONTROL_OID);

alter table PUMP_CONTROLS
  add constraint PUMP_CONTROLS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PUMP_CONTROLS
  add constraint PUMP_CONTROLS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCT_TRANSLATIONS
  add constraint PRODUCT_TRANSLATIONS_FK1
  foreign key (EXTERNAL_CLIENT_ID_OID) references EXTERNAL_CLIENT_IDS(EXTERNAL_CLIENT_ID_OID);

alter table PRODUCT_TRANSLATIONS
  add constraint PRODUCT_TRANSLATIONS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCT_THRESHOLDS
  add constraint PRODUCT_THRESHOLDS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRODUCT_THRESHOLDS
  add constraint PRODUCT_THRESHOLDS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCT_RESTRICT_PRODS
  add constraint PRODUCT_RESTRICT_PRODS_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCT_RESTRICT_PRODS
  add constraint PRODUCT_RESTRICT_PRODS_FK2
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table PRODUCT_RESTRICTION_ENC
  add constraint PRODUCT_RESTRICTION_ENC_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRODUCT_RESTRICTION_ENC
  add constraint PRODUCT_RESTRICTION_ENC_FK2
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table PRODUCT_RESTRICTIONS
  add constraint PRODUCT_RESTRICTIONS_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table PRODUCT_RESTRICTIONS
  add constraint PRODUCT_RESTRICTIONS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRODUCT_RESTRICTIONS
  add constraint PRODUCT_RESTRICTIONS_FK3
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table PRODUCT_GROUP_PRODUCTS
  add constraint PRODUCT_GROUP_PRODUCTS_FK1
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table PRODUCT_GROUP_PRODUCTS
  add constraint PRODUCT_GROUP_PRODUCTS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCT_GROUPS
  add constraint PRODUCT_GROUPS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCT_EMISSIONS
  add constraint PRODUCT_EMISSIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRODUCT_EMISSIONS
  add constraint PRODUCT_EMISSIONS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCT_EMISSIONS
  add constraint PRODUCT_EMISSIONS_FK3
  foreign key (EMISSION_TYPE_OID) references EMISSION_TYPES(EMISSION_TYPE_OID);

alter table PRODUCTS_SOLD_LOCATIONS
  add constraint PRODUCTS_SOLD_LOCATIONS_FK1
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table PRODUCTS_SOLD_LOCATIONS
  add constraint PRODUCTS_SOLD_LOCATIONS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRODUCTS
  add constraint PRODUCTS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK3
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK4
  foreign key (CARD_REQUEST_OID) references CARD_REQUESTS(CARD_REQUEST_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK5
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK7
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK8
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK10
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK12
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK13
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK14
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table PRODUCED_CARDS
  add constraint PRODUCED_CARDS_FK15
  foreign key (TIME_LIMIT_OID) references TIME_LIMITS(TIME_LIMIT_OID);

alter table PRICING_SCHEME_OPTIONS
  add constraint PRICING_SCHEME_OPTIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_SCHEME_OPTIONS
  add constraint PRICING_SCHEME_OPTIONS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_SCHEME_OPTIONS
  add constraint PRICING_SCHEME_OPTIONS_FK3
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table PRICING_SCHEME_OPTIONS
  add constraint PRICING_SCHEME_OPTIONS_FK4
  foreign key (PRICING_SCHEME_OID) references PRICING_SCHEMES(PRICING_SCHEME_OID);

alter table PRICING_SCHEME_OPTIONS
  add constraint PRICING_SCHEME_OPTIONS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_SCHEMES
  add constraint PRICING_SCHEMES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_SCHEMES
  add constraint PRICING_SCHEMES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_SCHEMES
  add constraint PRICING_SCHEMES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_RECIPE_RULES
  add constraint PRICING_RECIPE_RULES_FK1
  foreign key (PRICING_CATEGORIE_OID) references PRICING_CATEGORIES(PRICING_CATEGORIE_OID);

alter table PRICING_RECIPE_RULES
  add constraint PRICING_RECIPE_RULES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_RECIPE_RULES
  add constraint PRICING_RECIPE_RULES_FK3
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table PRICING_RECIPE_RULES
  add constraint PRICING_RECIPE_RULES_FK4
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table PRICING_RECIPES
  add constraint PRICING_RECIPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_RECIPES
  add constraint PRICING_RECIPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_RECIPES
  add constraint PRICING_RECIPES_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK3
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK4
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK5
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK6
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_PROFILES
  add constraint PRICING_PROFILES_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_OPTION_RECIPES
  add constraint PRICING_OPTION_RECIPES_FK1
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table PRICING_OPTION_RECIPES
  add constraint PRICING_OPTION_RECIPES_FK2
  foreign key (PRICING_RECIPE_OID) references PRICING_RECIPES(PRICING_RECIPE_OID);

alter table PRICING_OPTION_PRODUCTS
  add constraint PRICING_OPTION_PRODUCTS_FK1
  foreign key (PRICING_OPTION_OID) references PRICING_OPTIONS(PRICING_OPTION_OID);

alter table PRICING_OPTION_PRODUCTS
  add constraint PRICING_OPTION_PRODUCTS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRICING_OPTIONS
  add constraint PRICING_OPTIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PRICING_CONTROLS
  add constraint PRICING_CONTROLS_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table PRICING_CONTROLS
  add constraint PRICING_CONTROLS_FK2
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table PRICING_CONTROLS
  add constraint PRICING_CONTROLS_FK3
  foreign key (PRICING_SCHEME_OID) references PRICING_SCHEMES(PRICING_SCHEME_OID);

alter table PRICING_CATEGORIES
  add constraint PRICING_CATEGORIES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PRICING_CATEGORIES
  add constraint PRICING_CATEGORIES_FK2
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table POS_TRANS_LINE_ITEMS
  add constraint POS_TRANS_LINE_ITEMS_FK1
  foreign key (POS_TRANSACTION_OID) references POS_TRANSACTIONS(POS_TRANSACTION_OID);

alter table POS_TRANSACTIONS
  add constraint POS_TRANSACTIONS_FK1
  foreign key (POS_LOCN_RECONCILIATION_OID) references POS_LOCN_RECONCILIATIONS(POS_LOCN_RECONCILIATION_OID);

alter table POS_TRANSACTIONS
  add constraint POS_TRANSACTIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POS_TRANSACTIONS
  add constraint POS_TRANSACTIONS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POS_TRANSACTIONS
  add constraint POS_TRANSACTIONS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POS_LOCN_RECONCILIATIONS
  add constraint POS_LOCN_RECONCILIATIONS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POS_LOCN_RECONCILIATIONS
  add constraint POS_LOCN_RECONCILIATIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POS_INTERFACE
  add constraint POS_INTERFACE_FK1
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table POINT_VALUES
  add constraint POINT_VALUES_FK1
  foreign key (POINT_PROGRAM_OID) references POINT_PROGRAMS(POINT_PROGRAM_OID);

alter table POINT_TOTALS
  add constraint POINT_TOTALS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table POINT_TOTALS
  add constraint POINT_TOTALS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table POINT_TOTALS
  add constraint POINT_TOTALS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK1
  foreign key (POINT_CATEGORIE_OID) references POINT_CATEGORIES(POINT_CATEGORIE_OID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK2
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK4
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK5
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table POINT_PROGRAMS
  add constraint POINT_PROGRAMS_FK6
  foreign key (POINT_PROFILE_OID) references POINT_PROFILES(POINT_PROFILE_OID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK3
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK6
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table POINT_PROFILES
  add constraint POINT_PROFILES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POINT_OUT_OPTIONS
  add constraint POINT_OUT_OPTIONS_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table POINT_OUT_OPTIONS
  add constraint POINT_OUT_OPTIONS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table POINT_OUT_OPTIONS
  add constraint POINT_OUT_OPTIONS_FK3
  foreign key (POINT_PROFILE_OID) references POINT_PROFILES(POINT_PROFILE_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK3
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK5
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table POINT_CATEGORIES
  add constraint POINT_CATEGORIES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PERIOD_BALANCES
  add constraint PERIOD_BALANCES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table PERIOD_BALANCES
  add constraint PERIOD_BALANCES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PERIOD_ACCOUNT_TOTALS
  add constraint PERIOD_ACCOUNT_TOTALS_FK1
  foreign key (PERIOD_BALANCE_OID) references PERIOD_BALANCES(PERIOD_BALANCE_OID);

alter table PERIOD_ACCOUNT_TOTALS
  add constraint PERIOD_ACCOUNT_TOTALS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PAYMENT_REQUESTS
  add constraint PAYMENT_REQUESTS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table PAYMENT_GATEWAY_LOGS
  add constraint PAYMENT_GATEWAY_LOGS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table PAYMENT_GATEWAY_LOGS
  add constraint PAYMENT_GATEWAY_LOGS_FK2
  foreign key (PAYMENT_GATEWAY_OID) references PAYMENT_GATEWAYS(PAYMENT_GATEWAY_OID);

alter table PAYMENT_GATEWAYS
  add constraint PAYMENT_GATEWAYS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table PAYMENT_GATEWAYS
  add constraint PAYMENT_GATEWAYS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table PAYMENT_GATEWAYS
  add constraint PAYMENT_GATEWAYS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table PAYMENT_GATEWAYS
  add constraint PAYMENT_GATEWAYS_FK4
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table PAYMENT_GATEWAYS
  add constraint PAYMENT_GATEWAYS_FK5
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table PAYMENT_ARRANGEMENTS
  add constraint PAYMENT_ARRANGEMENTS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table PAYMENT_ARRANGEMENTS
  add constraint PAYMENT_ARRANGEMENTS_FK2
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table OPERATION_LIMIT
  add constraint OPERATION_LIMIT_FK1
  foreign key (ACCESS_GROUP_OID) references ACCESS_GROUPS(ACCESS_GROUP_OID);

alter table OPERATION_LIMIT
  add constraint OPERATION_LIMIT_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table OPERATION_LIMIT
  add constraint OPERATION_LIMIT_FK3
  foreign key (USER_OID) references USERS(USER_OID);

alter table OLS_EXTERNAL_CONTENT_LINKS
  add constraint OLS_EXTERNAL_CONTENT_LINKS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table OLS_EXTERNAL_CONTENT_LINKS
  add constraint OLS_EXTERNAL_CONTENT_LINKS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table OLS_EXTERNAL_CONTENT_LINKS
  add constraint OLS_EXTERNAL_CONTENT_LINKS_FK3
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table OLS_CONFIG
  add constraint OLS_CONFIG_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table NUMBERING_SEQUENCE_RESERVES
  add constraint NUMBERING_SEQUENCE_RESERVES_FK1
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table NUMBERING_SEQUENCES
  add constraint NUMBERING_SEQUENCES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table NUMBERING_SEQUENCES
  add constraint NUMBERING_SEQUENCES_FK2
  foreign key (NUMBERING_METHOD_OID) references NUMBERING_METHODS(NUMBERING_METHOD_OID);

alter table NUMBERING_METHODS
  add constraint NUMBERING_METHODS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK1
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK2
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK4
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK5
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK6
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK7
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK8
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK9
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK10
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK11
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_MERCHANTS
  add constraint M_MERCHANTS_FK12
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_LOCATION_GROUPS
  add constraint M_LOCATION_GROUPS_FK1
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_LOCATION_GROUPS
  add constraint M_LOCATION_GROUPS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK1
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK2
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK3
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK4
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK5
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK6
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK7
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK8
  foreign key (MONTH_OID) references MONTHS(MONTH_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK9
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK10
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK11
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK12
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK13
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK14
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK15
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table M_LOCATIONS
  add constraint M_LOCATIONS_FK16
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK1
  foreign key (CUSTOMER_VALUE_OID) references CUSTOMER_VALUES(CUSTOMER_VALUE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK2
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK3
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK4
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK5
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK6
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK7
  foreign key (CARD_GRAPHIC_OID) references CARD_GRAPHICS(CARD_GRAPHIC_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK8
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK9
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK10
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK11
  foreign key (CARD_REISSUE_PROFILE_OID) references CARD_REISSUE_PROFILES(CARD_REISSUE_PROFILE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK12
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK13
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK14
  foreign key (MONTH_OID) references MONTHS(MONTH_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK15
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK16
  foreign key (INDUSTRIE_OID) references INDUSTRIES(INDUSTRIE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK17
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK18
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK19
  foreign key (MAIL_INDICATOR_OID) references MAIL_INDICATORS(MAIL_INDICATOR_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK20
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK21
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK22
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK23
  foreign key (PUMP_CONTROL_OID) references PUMP_CONTROLS(PUMP_CONTROL_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK24
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_CUSTOMERS
  add constraint M_CUSTOMERS_FK25
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table M_CLIENT_GROUPS
  add constraint M_CLIENT_GROUPS_FK1
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK1
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK2
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK3
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK4
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK5
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK6
  foreign key (ADJUSTMENT_TYPE_OID) references ADJUSTMENT_TYPES(ADJUSTMENT_TYPE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK7
  foreign key (BANK_ACCOUNT_OID) references BANK_ACCOUNTS(BANK_ACCOUNT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK8
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK9
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK10
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK11
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK12
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK13
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK14
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK15
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK16
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK17
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK18
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK19
  foreign key (RATE_CODE_OID) references RATE_CODES(RATE_CODE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK20
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK21
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK22
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK23
  foreign key (TIMEZONE_OID) references TIMEZONES(TIMEZONE_OID);

alter table M_CLIENTS
  add constraint M_CLIENTS_FK24
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MONTH_CUST_PRD_LOC_TOTS
  add constraint MONTH_CUST_PRD_LOC_TOTS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table MONTH_CUST_PRD_LOC_TOTS
  add constraint MONTH_CUST_PRD_LOC_TOTS_FK2
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table MONTH_CUST_PRD_LOC_TOTS
  add constraint MONTH_CUST_PRD_LOC_TOTS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table MONTH_CUST_PRD_LOC_TOTS
  add constraint MONTH_CUST_PRD_LOC_TOTS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MERCH_AGRMNT_VALUE_TYPES
  add constraint MERCH_AGRMNT_VALUE_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MERCH_AGRMNT_VALUES
  add constraint MERCH_AGRMNT_VALUES_FK1
  foreign key (MERCH_AGRMNT_VALUE_TYPE_OID) references MERCH_AGRMNT_VALUE_TYPES(MERCH_AGRMNT_VALUE_TYPE_OID);

alter table MERCHANT_TRANS_GROUPS
  add constraint MERCHANT_TRANS_GROUPS_FK1
  foreign key (CAPTURE_TYPE_OID) references CAPTURE_TYPES(CAPTURE_TYPE_OID);

alter table MERCHANT_TRANS_GROUPS
  add constraint MERCHANT_TRANS_GROUPS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MERCHANT_TRANS_GROUPS
  add constraint MERCHANT_TRANS_GROUPS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table MERCHANT_TRANS_GROUPS
  add constraint MERCHANT_TRANS_GROUPS_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MERCHANT_PRICING_LOGS
  add constraint MERCHANT_PRICING_LOGS_FK1
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table MERCHANT_PRICING_LOGS
  add constraint MERCHANT_PRICING_LOGS_FK2
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table MERCHANT_PAYMENT_TYPES
  add constraint MERCHANT_PAYMENT_TYPES_FK1
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table MERCHANT_PAYMENT_TYPES
  add constraint MERCHANT_PAYMENT_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table MERCHANT_PAYMENT_TYPES
  add constraint MERCHANT_PAYMENT_TYPES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MERCHANT_PAYMENT_TYPES
  add constraint MERCHANT_PAYMENT_TYPES_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK1
  foreign key (BANK_ACCOUNT_OID) references BANK_ACCOUNTS(BANK_ACCOUNT_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK2
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK3
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK4
  foreign key (RELATIONSHIP_ASSIGNMENT_OID) references RELATIONSHIP_ASSIGNMENTS(RELATIONSHIP_ASSIGNMENT_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK5
  foreign key (MERCHANT_PAYMENT_TYPE_OID) references MERCHANT_PAYMENT_TYPES(MERCHANT_PAYMENT_TYPE_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK6
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK7
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table MERCHANT_AGREEMENTS
  add constraint MERCHANT_AGREEMENTS_FK8
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table MENU_ACCESS_RESTRICTIONS
  add constraint MENU_ACCESS_RESTRICTIONS_FK1
  foreign key (ACCESS_GROUP_OID) references ACCESS_GROUPS(ACCESS_GROUP_OID);

alter table MENU_ACCESS_RESTRICTIONS
  add constraint MENU_ACCESS_RESTRICTIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MEMBERS
  add constraint MEMBERS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MASTER_PIN_KEY
  add constraint MASTER_PIN_KEY_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK1
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK5
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table MARKETING_MESSAGES
  add constraint MARKETING_MESSAGES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MAN_TRANS_LINE_ITEMS
  add constraint MAN_TRANS_LINE_ITEMS_FK1
  foreign key (MANUAL_TRANSACTION_OID) references MANUAL_TRANSACTIONS(MANUAL_TRANSACTION_OID);

alter table MAN_TRANS_BATCH_HEADERS
  add constraint MAN_TRANS_BATCH_HEADERS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MAN_TRANS_BATCH_HEADERS
  add constraint MAN_TRANS_BATCH_HEADERS_FK2
  foreign key (BATCH_TYPE_OID) references BATCH_TYPES(BATCH_TYPE_OID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK4
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK5
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table MANUAL_VOUCHERS
  add constraint MANUAL_VOUCHERS_FK6
  foreign key (TRANS_TRANSLATION_OID) references TRANS_TRANSLATIONS(TRANS_TRANSLATION_OID);

alter table MANUAL_TRANSACTIONS
  add constraint MANUAL_TRANSACTIONS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table MANUAL_TRANSACTIONS
  add constraint MANUAL_TRANSACTIONS_FK2
  foreign key (MAN_TRANS_BATCH_HEADER_OID) references MAN_TRANS_BATCH_HEADERS(MAN_TRANS_BATCH_HEADER_OID);

alter table MANUAL_TRANSACTIONS
  add constraint MANUAL_TRANSACTIONS_FK3
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table MANUAL_AUTH_REQUEST
  add constraint MANUAL_AUTH_REQUEST_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table MANUAL_AUTH_REQUEST
  add constraint MANUAL_AUTH_REQUEST_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table MAIL_INDICATORS
  add constraint MAIL_INDICATORS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table LOCATION_RESTRICT_LOCS
  add constraint LOCATION_RESTRICT_LOCS_FK1
  foreign key (LOCATION_RESTRICTION_OID) references LOCATION_RESTRICTIONS(LOCATION_RESTRICTION_OID);

alter table LOCATION_RESTRICTIONS
  add constraint LOCATION_RESTRICTIONS_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table LOCATION_RESTRICTIONS
  add constraint LOCATION_RESTRICTIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK2
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK3
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK4
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK5
  foreign key (POS_LOCN_RECONCILIATION_OID) references POS_LOCN_RECONCILIATIONS(POS_LOCN_RECONCILIATION_OID);

alter table LOCATION_RECONCILIATIONS
  add constraint LOCATION_RECONCILIATIONS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LOCATION_PROD_TRANSLATION
  add constraint LOCATION_PROD_TRANSLATION_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table LOCATION_PROD_TRANSLATION
  add constraint LOCATION_PROD_TRANSLATION_FK2
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table LOCATION_PROD_TRANSLATION
  add constraint LOCATION_PROD_TRANSLATION_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table LOCATION_PRODUCT_TOTALS
  add constraint LOCATION_PRODUCT_TOTALS_FK1
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table LOCATION_PRODUCT_TOTALS
  add constraint LOCATION_PRODUCT_TOTALS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table LOCATION_PRODUCT_TOTALS
  add constraint LOCATION_PRODUCT_TOTALS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK2
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK3
  foreign key (TRANSACTION_LINE_ITEM_OID) references TRANSACTION_LINE_ITEMS(TRANSACTION_LINE_ITEM_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK4
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK6
  foreign key (REBATE_OID) references REBATES(REBATE_OID);

alter table LINE_ITEM_COMPONENTS
  add constraint LINE_ITEM_COMPONENTS_FK7
  foreign key (REBATE_PROFILE_OID) references REBATE_PROFILES(REBATE_PROFILE_OID);

alter table LETTER_VARIABLES
  add constraint LETTER_VARIABLES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LETTER_TEXT_LINES
  add constraint LETTER_TEXT_LINES_FK1
  foreign key (LETTER_DEFINITION_OID) references LETTER_DEFINITIONS(LETTER_DEFINITION_OID);

alter table LETTER_TEMPLATES
  add constraint LETTER_TEMPLATES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table LETTER_TEMPLATES
  add constraint LETTER_TEMPLATES_FK2
  foreign key (LETTER_FONT_OID) references LETTER_FONTS(LETTER_FONT_OID);

alter table LETTER_FONTS
  add constraint LETTER_FONTS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK3
  foreign key (LETTER_DEFINITION_OID) references LETTER_DEFINITIONS(LETTER_DEFINITION_OID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK4
  foreign key (LETTER_DEFINITION_OID) references LETTER_DEFINITIONS(LETTER_DEFINITION_OID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK5
  foreign key (LETTER_TEMPLATE_OID) references LETTER_TEMPLATES(LETTER_TEMPLATE_OID);

alter table LETTER_DEFINITIONS
  add constraint LETTER_DEFINITIONS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table LETTER_ASSIGN_VARIABLES
  add constraint LETTER_ASSIGN_VARIABLES_FK1
  foreign key (LETTER_ASSIGNMENT_OID) references LETTER_ASSIGNMENTS(LETTER_ASSIGNMENT_OID);

alter table LETTER_ASSIGN_VARIABLES
  add constraint LETTER_ASSIGN_VARIABLES_FK2
  foreign key (LETTER_VARIABLE_OID) references LETTER_VARIABLES(LETTER_VARIABLE_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK1
  foreign key (LETTER_EXTRACT_OID) references LETTER_EXTRACTS(LETTER_EXTRACT_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK2
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK3
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK4
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK6
  foreign key (LETTER_DEFINITION_OID) references LETTER_DEFINITIONS(LETTER_DEFINITION_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK7
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK8
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table LETTER_ASSIGNMENTS
  add constraint LETTER_ASSIGNMENTS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table IVR_REQUEST_LOGS
  add constraint IVR_REQUEST_LOGS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERNET_USER_PWD_LOGS
  add constraint INTERNET_USER_PWD_LOGS_FK1
  foreign key (INTERNET_USER_OID) references INTERNET_USERS(INTERNET_USER_OID);

alter table INTERNET_USER_ACCESS_ALERTS
  add constraint INTERNET_USER_ACCESS_ALERTS_FK1
  foreign key (INTERNET_USER_ACCES_OID) references INTERNET_USER_ACCESS(INTERNET_USER_ACCES_OID);

alter table INTERNET_USER_ACCESS_ALERTS
  add constraint INTERNET_USER_ACCESS_ALERTS_FK2
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table INTERNET_USER_ACCESS
  add constraint INTERNET_USER_ACCESS_FK1
  foreign key (INTERNET_USER_OID) references INTERNET_USERS(INTERNET_USER_OID);

alter table INTERNET_USER_ACCESS
  add constraint INTERNET_USER_ACCESS_FK2
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK1
  foreign key (INTERNET_ACCESS_GROUP_OID) references INTERNET_ACCESS_GROUPS(INTERNET_ACCESS_GROUP_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK4
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK5
  foreign key (INTERNET_USER_OID) references INTERNET_USERS(INTERNET_USER_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK6
  foreign key (PRIVATE_QUESTION_OID) references PRIVATE_QUESTIONS(PRIVATE_QUESTION_OID);

alter table INTERNET_USERS
  add constraint INTERNET_USERS_FK7
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table INTERNET_ROLES_PERMISSIONS
  add constraint INTERNET_ROLES_PERMISSIONS_FK1
  foreign key (INTERNET_ACCESS_GROUP_OID) references INTERNET_ACCESS_GROUPS(INTERNET_ACCESS_GROUP_OID);

alter table INTERNET_ROLES_PERMISSIONS
  add constraint INTERNET_ROLES_PERMISSIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERNET_ROLES_PERMISSIONS
  add constraint INTERNET_ROLES_PERMISSIONS_FK3
  foreign key (INTERNET_PERMISSION_OID) references INTERNET_PERMISSIONS(INTERNET_PERMISSION_OID);

alter table INTERNET_ACCESS_GROUPS
  add constraint INTERNET_ACCESS_GROUPS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERNET_ACCESS_GROUPS
  add constraint INTERNET_ACCESS_GROUPS_FK2
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table INTERF_FILE_INC_LOG_ROWS
  add constraint INTERF_FILE_INC_LOG_ROWS_FK1
  foreign key (INTERFACE_FILE_INC_LOG_OID) references INTERFACE_FILE_INC_LOGS(INTERFACE_FILE_INC_LOG_OID);

alter table INTERFACE_FILE_PARAMS
  add constraint INTERFACE_FILE_PARAMS_FK1
  foreign key (INTERFACE_FILE_OID) references INTERFACE_FILES(INTERFACE_FILE_OID);

alter table INTERFACE_FILE_LOG_PARAMS
  add constraint INTERFACE_FILE_LOG_PARAMS_FK1
  foreign key (INTERFACE_FILE_LOG_OID) references INTERFACE_FILE_LOGS(INTERFACE_FILE_LOG_OID);

alter table INTERFACE_FILE_LOGS
  add constraint INTERFACE_FILE_LOGS_FK1
  foreign key (INTERFACE_FILE_OID) references INTERFACE_FILES(INTERFACE_FILE_OID);

alter table INTERFACE_FILE_INC_LOGS
  add constraint INTERFACE_FILE_INC_LOGS_FK1
  foreign key (INTERFACE_FILE_OID) references INTERFACE_FILES_INCOMING(INTERFACE_FILE_OID);

alter table INTERFACE_FILES_INCOMING
  add constraint INTERFACE_FILES_INCOMING_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table INTERFACE_FILES
  add constraint INTERFACE_FILES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table INTERFACE_FILES
  add constraint INTERFACE_FILES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTERFACE_FILES
  add constraint INTERFACE_FILES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INTEREST_RATES
  add constraint INTEREST_RATES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table INTEREST_RATES
  add constraint INTEREST_RATES_FK2
  foreign key (RATE_CODE_OID) references RATE_CODES(RATE_CODE_OID);

alter table INET_HOST_CLIENT_MAP
  add constraint INET_HOST_CLIENT_MAP_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table INET_HOST_CLIENT_MAP
  add constraint INET_HOST_CLIENT_MAP_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table INDUSTRIES
  add constraint INDUSTRIES_FK1
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table IFO_AUDIT_TABLES
  add constraint IFO_AUDIT_TABLES_FK1
  foreign key (AUDIT_CATEGORY_TABLE_OID) references AUDIT_CATEGORY_TABLES(AUDIT_CATEGORY_TABLE_OID);

alter table IFO_AUDIT_TABLES
  add constraint IFO_AUDIT_TABLES_FK2
  foreign key (INTERFACE_FILE_OID) references INTERFACE_FILES(INTERFACE_FILE_OID);

alter table IFO_AUDIT_COL_EXCLUSIONS
  add constraint IFO_AUDIT_COL_EXCLUSIONS_FK1
  foreign key (IFO_AUDIT_COLUMN_OID) references IFO_AUDIT_COLUMNS(IFO_AUDIT_COLUMN_OID);

alter table IFO_AUDIT_COLUMNS
  add constraint IFO_AUDIT_COLUMNS_FK1
  foreign key (IFO_AUDIT_TABLE_OID) references IFO_AUDIT_TABLES(IFO_AUDIT_TABLE_OID);

alter table IFO_AUDIT_COLUMNS
  add constraint IFO_AUDIT_COLUMNS_FK2
  foreign key (AUDIT_CATEGORY_COLUMN_OID) references AUDIT_CATEGORY_COLUMNS(AUDIT_CATEGORY_COLUMN_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK1
  foreign key (REQUEST_TRANS_TYPE_OID) references REQUEST_TRANS_TYPES(REQUEST_TRANS_TYPE_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK2
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK3
  foreign key (ADJUSTMENT_TYPE_OID) references ADJUSTMENT_TYPES(ADJUSTMENT_TYPE_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK4
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK5
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK6
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK7
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK8
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK9
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table HOLDING_TRANSACTIONS
  add constraint HOLDING_TRANSACTIONS_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table HIERARCHIES
  add constraint HIERARCHIES_FK1
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table GL_TOTALS
  add constraint GL_TOTALS_FK1
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_TOTALS
  add constraint GL_TOTALS_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK3
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK4
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK5
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK7
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK9
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_SUMMARIES
  add constraint GL_SUMMARIES_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_PROFITCOST_CENTRE_MAPS
  add constraint GL_PROFITCOST_CENTRE_MAPS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GL_PROFITCOST_CENTRE_MAPS
  add constraint GL_PROFITCOST_CENTRE_MAPS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_INVOICE_NUMBER_RANGES
  add constraint GL_INVOICE_NUMBER_RANGES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GL_INVOICE_NUMBER_ASSIGNMENTS
  add constraint GL_INVOICE_NUMBER_ASSIGNMENTS_FK1
  foreign key (GL_EXTR_LEDGER_DETAIL_OID) references GL_EXTR_LEDGER_DETAILS(GL_EXTR_LEDGER_DETAIL_OID);

alter table GL_INVOICE_NUMBER_ASSIGNMENTS
  add constraint GL_INVOICE_NUMBER_ASSIGNMENTS_FK2
  foreign key (GL_INVOICE_NUMBER_OID) references GL_INVOICE_NUMBERS(GL_INVOICE_NUMBER_OID);

alter table GL_INVOICE_NUMBERS
  add constraint GL_INVOICE_NUMBERS_FK1
  foreign key (GL_INVOICE_NUMBER_RANGE_OID) references GL_INVOICE_NUMBER_RANGES(GL_INVOICE_NUMBER_RANGE_OID);

alter table GL_EXTR_LEDGER_DETAILS
  add constraint GL_EXTR_LEDGER_DETAILS_FK1
  foreign key (GL_SUMMARIE_OID) references GL_SUMMARIES(GL_SUMMARIE_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK1
  foreign key (GL_PROFITCOST_CENTRE_MAP_OID) references GL_PROFITCOST_CENTRE_MAPS(GL_PROFITCOST_CENTRE_MAP_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK2
  foreign key (GL_PROFITCOST_CENTRE_MAP_OID) references GL_PROFITCOST_CENTRE_MAPS(GL_PROFITCOST_CENTRE_MAP_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK4
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK6
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK7
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK8
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK10
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK11
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK12
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK13
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK14
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK15
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK16
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_NUMBERS
  add constraint GL_ACCOUNT_NUMBERS_FK17
  foreign key (BANKING_ACCOUNT_MAP) references BANKING_ACCOUNT_MAP(BANKING_ACCOUNT_MAP);

alter table GL_ACCOUNT_CODES
  add constraint GL_ACCOUNT_CODES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GL_ACCOUNT_CODES
  add constraint GL_ACCOUNT_CODES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GL_ACCOUNT_CODES
  add constraint GL_ACCOUNT_CODES_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table GEOGRAPHIC_AREA_TYPES
  add constraint GEOGRAPHIC_AREA_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table GEOGRAPHIC_AREAS
  add constraint GEOGRAPHIC_AREAS_FK1
  foreign key (GEOGRAPHIC_AREA_TYPE_OID) references GEOGRAPHIC_AREA_TYPES(GEOGRAPHIC_AREA_TYPE_OID);

alter table GENERATED_PAYMENTS
  add constraint GENERATED_PAYMENTS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table GENERATED_PAYMENTS
  add constraint GENERATED_PAYMENTS_FK2
  foreign key (MERCHANT_AGREEMENT_OID) references MERCHANT_AGREEMENTS(MERCHANT_AGREEMENT_OID);

alter table GENERATED_PAYMENTS
  add constraint GENERATED_PAYMENTS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table GENERATED_PAYMENTS
  add constraint GENERATED_PAYMENTS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table GENERATED_PAYMENTS
  add constraint GENERATED_PAYMENTS_FK5
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table FREQUENCIES
  add constraint FREQUENCIES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table FREQUENCIES
  add constraint FREQUENCIES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEE_VALUES
  add constraint FEE_VALUES_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table FEE_VALUES
  add constraint FEE_VALUES_FK2
  foreign key (FEE_EFFECTIVE_DATE_OID) references FEE_EFFECTIVE_DATES(FEE_EFFECTIVE_DATE_OID);

alter table FEE_VALUES
  add constraint FEE_VALUES_FK3
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK3
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK4
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK5
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK6
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK7
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK8
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEE_PROFILES
  add constraint FEE_PROFILES_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEE_EFFECTIVE_DATES
  add constraint FEE_EFFECTIVE_DATES_FK1
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table FEE_CONTROLS
  add constraint FEE_CONTROLS_FK1
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table FEE_CONTROLS
  add constraint FEE_CONTROLS_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table FEE_CONTROLS
  add constraint FEE_CONTROLS_FK3
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table FEE_CONTROLS
  add constraint FEE_CONTROLS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEES
  add constraint FEES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEES
  add constraint FEES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table FEES
  add constraint FEES_FK3
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table FEES
  add constraint FEES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FEES
  add constraint FEES_FK5
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table FEES
  add constraint FEES_FK6
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table FEES
  add constraint FEES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table FAL_TABLES
  add constraint FAL_TABLES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table EXT_VELOCITY_CTRL_PROF
  add constraint EXT_VELOCITY_CTRL_PROF_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table EXTERNAL_CLIENT_IDS
  add constraint EXTERNAL_CLIENT_IDS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table EXCHANGE_RATES
  add constraint EXCHANGE_RATES_FK1
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table EXCHANGE_RATES
  add constraint EXCHANGE_RATES_FK2
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table EVENT_REQUESTS
  add constraint EVENT_REQUESTS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table EVENT_REQUESTS
  add constraint EVENT_REQUESTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table EVENT_REQUESTS
  add constraint EVENT_REQUESTS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ENCRYPTED_PINS
  add constraint ENCRYPTED_PINS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table ENCRYPTED_PINS
  add constraint ENCRYPTED_PINS_FK2
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table ENCODING_PROMPTS
  add constraint ENCODING_PROMPTS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ENCODING_PROMPTS
  add constraint ENCODING_PROMPTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ENCODING_PROMPTS
  add constraint ENCODING_PROMPTS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table EMISSION_VALUES
  add constraint EMISSION_VALUES_FK1
  foreign key (EMISSION_TYPE_OID) references EMISSION_TYPES(EMISSION_TYPE_OID);

alter table EMISSION_TYPES
  add constraint EMISSION_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DUNNING_CONTROLS
  add constraint DUNNING_CONTROLS_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table DUNNING_CONTROLS
  add constraint DUNNING_CONTROLS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DUNNING_CONTROLS
  add constraint DUNNING_CONTROLS_FK3
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table DUNNING_CODES
  add constraint DUNNING_CODES_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table DUNNING_CODES
  add constraint DUNNING_CODES_FK2
  foreign key (STATEMENT_MESSAGE_OID) references STATEMENT_MESSAGES(STATEMENT_MESSAGE_OID);

alter table DRIVER_PRODUCT_TOTALS
  add constraint DRIVER_PRODUCT_TOTALS_FK1
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table DRIVER_PRODUCT_TOTALS
  add constraint DRIVER_PRODUCT_TOTALS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table DRIVER_PRODUCT_TOTALS
  add constraint DRIVER_PRODUCT_TOTALS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DRIVERS
  add constraint DRIVERS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table DRIVERS
  add constraint DRIVERS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DRIVERS
  add constraint DRIVERS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table DISTRICTS_ZONES
  add constraint DISTRICTS_ZONES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DISTRIBUTOR_REFERENCES
  add constraint DISTRIBUTOR_REFERENCES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DISTRIBUTOR_REFERENCES
  add constraint DISTRIBUTOR_REFERENCES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DISPUTE_REASONS
  add constraint DISPUTE_REASONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DISPUTE_REASONS
  add constraint DISPUTE_REASONS_FK2
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table DISPUTED_TRANSACTIONS
  add constraint DISPUTED_TRANSACTIONS_FK1
  foreign key (DISPUTE_REASON_OID) references DISPUTE_REASONS(DISPUTE_REASON_OID);

alter table DISPUTED_TRANSACTIONS
  add constraint DISPUTED_TRANSACTIONS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DISPUTED_TRANSACTIONS
  add constraint DISPUTED_TRANSACTIONS_FK3
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK2
  foreign key (WORK_QUEUE_ACTION_ITEM_OID) references WORK_QUEUE_ACTION_ITEMS(WORK_QUEUE_ACTION_ITEM_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK3
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK4
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK5
  foreign key (DISPUTED_TRANSACTION_OID) references DISPUTED_TRANSACTIONS(DISPUTED_TRANSACTION_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK6
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK7
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK8
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK10
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK11
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table DIARY_NOTES
  add constraint DIARY_NOTES_FK12
  foreign key (USER_OID) references USERS(USER_OID);

alter table DETAIL_GROUP_TYPES
  add constraint DETAIL_GROUP_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DETAIL_GROUP_TYPES
  add constraint DETAIL_GROUP_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DETAIL_GROUP_TRANS
  add constraint DETAIL_GROUP_TRANS_FK1
  foreign key (DETAIL_GROUP_OID) references DETAIL_GROUPS(DETAIL_GROUP_OID);

alter table DETAIL_GROUP_TRANS
  add constraint DETAIL_GROUP_TRANS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK2
  foreign key (AGING_BUCKET_OID) references AGING_BUCKETS(AGING_BUCKET_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK3
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK4
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK5
  foreign key (PAYMENT_REQUEST_OID) references PAYMENT_REQUESTS(PAYMENT_REQUEST_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DETAIL_GROUPS
  add constraint DETAIL_GROUPS_FK7
  foreign key (DETAIL_GROUP_TYPE_OID) references DETAIL_GROUP_TYPES(DETAIL_GROUP_TYPE_OID);

alter table DESCRIPTIONS
  add constraint DESCRIPTIONS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DESCRIPTIONS
  add constraint DESCRIPTIONS_FK2
  foreign key (DESCRIPTION_TYPE_OID) references DESCRIPTION_TYPES(DESCRIPTION_TYPE_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK12
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK13
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK14
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK15
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK16
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK17
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK18
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK19
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK20
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK21
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK22
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK23
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEPRC_CARD_PROGRAM_CARD_TYPES
  add constraint DEPRC_CARD_PROGRAM_CARD_TYPES_FK24
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK3
  foreign key (CARD_PRODUCT_GROUP_OID) references CARD_PRODUCT_GROUPS(CARD_PRODUCT_GROUP_OID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK4
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK5
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table DEFAULT_PRICING_PROFILE
  add constraint DEFAULT_PRICING_PROFILE_FK6
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table CUST_CARD_TEMPLATE_FIELDS
  add constraint CUST_CARD_TEMPLATE_FIELDS_FK1
  foreign key (BULK_CARD_COLUMN_OID) references BULK_CARD_COLUMNS(BULK_CARD_COLUMN_OID);

alter table CUST_CARD_TEMPLATE_FIELDS
  add constraint CUST_CARD_TEMPLATE_FIELDS_FK2
  foreign key (CUSTOMER_CARD_TEMPLATE_OID) references CUSTOMER_CARD_TEMPLATES(CUSTOMER_CARD_TEMPLATE_OID);

alter table CUSTOMER_VAS_OFFERINGS
  add constraint CUSTOMER_VAS_OFFERINGS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CUSTOMER_VAS_OFFERINGS
  add constraint CUSTOMER_VAS_OFFERINGS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_VAS_OFFERINGS
  add constraint CUSTOMER_VAS_OFFERINGS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table CUSTOMER_VALUE_TYPES
  add constraint CUSTOMER_VALUE_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CUSTOMER_VALUES
  add constraint CUSTOMER_VALUES_FK1
  foreign key (CUSTOMER_VALUE_TYPE_OID) references CUSTOMER_VALUE_TYPES(CUSTOMER_VALUE_TYPE_OID);

alter table CUSTOMER_REPORT_LEVELS
  add constraint CUSTOMER_REPORT_LEVELS_FK1
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table CUSTOMER_REPORT_LEVELS
  add constraint CUSTOMER_REPORT_LEVELS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_PRODUCT_TOT_LOCS
  add constraint CUSTOMER_PRODUCT_TOT_LOCS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_PRODUCT_TOT_LOCS
  add constraint CUSTOMER_PRODUCT_TOT_LOCS_FK2
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table CUSTOMER_PRODUCT_TOT_LOCS
  add constraint CUSTOMER_PRODUCT_TOT_LOCS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table CUSTOMER_PRODUCT_TOT_LOCS
  add constraint CUSTOMER_PRODUCT_TOT_LOCS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CUSTOMER_PRODUCT_TOT_LOCS
  add constraint CUSTOMER_PRODUCT_TOT_LOCS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CUSTOMER_PRODUCT_TOTALS
  add constraint CUSTOMER_PRODUCT_TOTALS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_PRODUCT_TOTALS
  add constraint CUSTOMER_PRODUCT_TOTALS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table CUSTOMER_PRODUCT_TOTALS
  add constraint CUSTOMER_PRODUCT_TOTALS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CUSTOMER_PRODUCT_TOTALS
  add constraint CUSTOMER_PRODUCT_TOTALS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK2
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK3
  foreign key (CARD_PRODUCT_GROUP_OID) references CARD_PRODUCT_GROUPS(CARD_PRODUCT_GROUP_OID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK5
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table CUSTOMER_PRICING_LOGS
  add constraint CUSTOMER_PRICING_LOGS_FK6
  foreign key (PRODUCT_GROUP_OID) references PRODUCT_GROUPS(PRODUCT_GROUP_OID);

alter table CUSTOMER_COST_CENTRES
  add constraint CUSTOMER_COST_CENTRES_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_CARD_TEMPLATES
  add constraint CUSTOMER_CARD_TEMPLATES_FK1
  foreign key (BULK_CARD_TEMPLATE_OID) references BULK_CARD_TEMPLATES(BULK_CARD_TEMPLATE_OID);

alter table CUSTOMER_CARD_TEMPLATES
  add constraint CUSTOMER_CARD_TEMPLATES_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_CARD_PROFILES
  add constraint CUSTOMER_CARD_PROFILES_FK1
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table CUSTOMER_CARD_PROFILES
  add constraint CUSTOMER_CARD_PROFILES_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_CARD_PROFILES
  add constraint CUSTOMER_CARD_PROFILES_FK3
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table CUSTOMER_CARD_PROFILES
  add constraint CUSTOMER_CARD_PROFILES_FK4
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CUSTOMER_CARD_PROFILES
  add constraint CUSTOMER_CARD_PROFILES_FK5
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CUSTOMER_CARD_PRODUCTS
  add constraint CUSTOMER_CARD_PRODUCTS_FK1
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CUSTOMER_CARD_PRODUCTS
  add constraint CUSTOMER_CARD_PRODUCTS_FK2
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CUSTOMER_CARD_PRODUCTS
  add constraint CUSTOMER_CARD_PRODUCTS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_CARD_PRODUCTS
  add constraint CUSTOMER_CARD_PRODUCTS_FK4
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CUSTOMER_ACCNT_PROFILES
  add constraint CUSTOMER_ACCNT_PROFILES_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CUSTOMER_ACCNT_PROFILES
  add constraint CUSTOMER_ACCNT_PROFILES_FK2
  foreign key (ACCOUNT_CONTROL_PROFILE_OID) references ACCOUNT_CONTROL_PROFILES(ACCOUNT_CONTROL_PROFILE_OID);

alter table CURRENCIES
  add constraint CURRENCIES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CREDIT_PLANS
  add constraint CREDIT_PLANS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CREDIT_PLANS
  add constraint CREDIT_PLANS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CREDIT_PLANS
  add constraint CREDIT_PLANS_FK3
  foreign key (INTEREST_RATE_OID) references INTEREST_RATES(INTEREST_RATE_OID);

alter table CREDIT_PLANS
  add constraint CREDIT_PLANS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CREDIT_PLANS
  add constraint CREDIT_PLANS_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CREDIT_CARDS
  add constraint CREDIT_CARDS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table CREDIT_CARDS
  add constraint CREDIT_CARDS_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table CREDIT_CARDS
  add constraint CREDIT_CARDS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COUNTRIES
  add constraint COUNTRIES_FK1
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table COUNTRIES
  add constraint COUNTRIES_FK2
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table COST_CENTRE_VEHICLE_TOTS
  add constraint COST_CENTRE_VEHICLE_TOTS_FK1
  foreign key (COST_CENTRE_OID) references COST_CENTRES(COST_CENTRE_OID);

alter table COST_CENTRE_VEHICLE_TOTS
  add constraint COST_CENTRE_VEHICLE_TOTS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table COST_CENTRE_VEHICLE_TOTS
  add constraint COST_CENTRE_VEHICLE_TOTS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COST_CENTRE_VEHICLE_TOTS
  add constraint COST_CENTRE_VEHICLE_TOTS_FK4
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table COST_CENTRE_DRIVER_TOTS
  add constraint COST_CENTRE_DRIVER_TOTS_FK1
  foreign key (COST_CENTRE_OID) references COST_CENTRES(COST_CENTRE_OID);

alter table COST_CENTRE_DRIVER_TOTS
  add constraint COST_CENTRE_DRIVER_TOTS_FK2
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table COST_CENTRE_DRIVER_TOTS
  add constraint COST_CENTRE_DRIVER_TOTS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table COST_CENTRE_DRIVER_TOTS
  add constraint COST_CENTRE_DRIVER_TOTS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COST_CENTRE_CARD_TOTALS
  add constraint COST_CENTRE_CARD_TOTALS_FK1
  foreign key (COST_CENTRE_OID) references COST_CENTRES(COST_CENTRE_OID);

alter table COST_CENTRE_CARD_TOTALS
  add constraint COST_CENTRE_CARD_TOTALS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table COST_CENTRE_CARD_TOTALS
  add constraint COST_CENTRE_CARD_TOTALS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table COST_CENTRE_CARD_TOTALS
  add constraint COST_CENTRE_CARD_TOTALS_FK4
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table COST_CENTRE_CARD_TOTALS
  add constraint COST_CENTRE_CARD_TOTALS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COST_CENTRES
  add constraint COST_CENTRES_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table COST_CENTRES
  add constraint COST_CENTRES_FK2
  foreign key (CUSTOMER_COST_CENTRE_OID) references CUSTOMER_COST_CENTRES(CUSTOMER_COST_CENTRE_OID);

alter table COST_CENTRES
  add constraint COST_CENTRES_FK3
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table COST_CENTRES
  add constraint COST_CENTRES_FK4
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table CONTACT_TYPES
  add constraint CONTACT_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CONTACT_RANKINGS
  add constraint CONTACT_RANKINGS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CONTACT_RANKINGS
  add constraint CONTACT_RANKINGS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CONTACT_RANKINGS
  add constraint CONTACT_RANKINGS_FK3
  foreign key (CONTACT_TYPE_OID) references CONTACT_TYPES(CONTACT_TYPE_OID);

alter table CONTACTS
  add constraint CONTACTS_FK1
  foreign key (CONTACT_TYPE_OID) references CONTACT_TYPES(CONTACT_TYPE_OID);

alter table CONTACTS
  add constraint CONTACTS_FK2
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table CONTACTS
  add constraint CONTACTS_FK3
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table CONTACTS
  add constraint CONTACTS_FK4
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table CONSTANT_DESCRIPTIONS
  add constraint CONSTANT_DESCRIPTIONS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CONSTANT_DESCRIPTIONS
  add constraint CONSTANT_DESCRIPTIONS_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table CONSTANTS
  add constraint CONSTANTS_FK1
  foreign key (CONSTANT_TYPE_OID) references CONSTANT_TYPES(CONSTANT_TYPE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK1
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK2
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK3
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK4
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK5
  foreign key (GEOGRAPHIC_AREA_OID) references GEOGRAPHIC_AREAS(GEOGRAPHIC_AREA_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK6
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK7
  foreign key (COMPONENT_TYPE_OID) references COMPONENT_TYPES(COMPONENT_TYPE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK8
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK9
  foreign key (CUSTOMER_VALUE_OID) references CUSTOMER_VALUES(CUSTOMER_VALUE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK10
  foreign key (CUSTOMER_VALUE_OID) references CUSTOMER_VALUES(CUSTOMER_VALUE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK11
  foreign key (CUSTOMER_VALUE_OID) references CUSTOMER_VALUES(CUSTOMER_VALUE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK12
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK13
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK14
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK15
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK16
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK17
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table COMPONENT_VALUES
  add constraint COMPONENT_VALUES_FK18
  foreign key (MERCH_AGRMNT_VALUE_OID) references MERCH_AGRMNT_VALUES(MERCH_AGRMNT_VALUE_OID);

alter table COMPONENT_TYPES
  add constraint COMPONENT_TYPES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COMPONENT_TYPES
  add constraint COMPONENT_TYPES_FK2
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table COMPONENT_TYPES
  add constraint COMPONENT_TYPES_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table COMPONENT_TYPES
  add constraint COMPONENT_TYPES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COLLECTION_EVENTS
  add constraint COLLECTION_EVENTS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table COLLECTION_EVENTS
  add constraint COLLECTION_EVENTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table COLLECTION_EVENTS
  add constraint COLLECTION_EVENTS_FK3
  foreign key (DISPUTED_TRANSACTION_OID) references DISPUTED_TRANSACTIONS(DISPUTED_TRANSACTION_OID);

alter table COLLECTION_EVENTS
  add constraint COLLECTION_EVENTS_FK4
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table COLLECTION_EVENTS
  add constraint COLLECTION_EVENTS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CLIENT_TRANS_TYPES
  add constraint CLIENT_TRANS_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_TRANS_TYPES
  add constraint CLIENT_TRANS_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CLIENT_TRANS_TYPES
  add constraint CLIENT_TRANS_TYPES_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table CLIENT_TRANS_FEES
  add constraint CLIENT_TRANS_FEES_FK1
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table CLIENT_TRANS_FEES
  add constraint CLIENT_TRANS_FEES_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table CLIENT_TRANS_FEES
  add constraint CLIENT_TRANS_FEES_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_TRANS_FEES
  add constraint CLIENT_TRANS_FEES_FK4
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CLIENT_TRANS_DETAIL_GRPS
  add constraint CLIENT_TRANS_DETAIL_GRPS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_TRANS_DETAIL_GRPS
  add constraint CLIENT_TRANS_DETAIL_GRPS_FK2
  foreign key (DETAIL_GROUP_TYPE_OID) references DETAIL_GROUP_TYPES(DETAIL_GROUP_TYPE_OID);

alter table CLIENT_TRANS_DETAIL_GRPS
  add constraint CLIENT_TRANS_DETAIL_GRPS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CLIENT_RETENTION_PERIODS
  add constraint CLIENT_RETENTION_PERIODS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_RETENTION_PERIODS
  add constraint CLIENT_RETENTION_PERIODS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CLIENT_PVK
  add constraint CLIENT_PVK_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_GROUP_LANGUAGES
  add constraint CLIENT_GROUP_LANGUAGES_FK1
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table CLIENT_GROUP_LANGUAGES
  add constraint CLIENT_GROUP_LANGUAGES_FK2
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table CLIENT_DATA_DEPENDENCIES
  add constraint CLIENT_DATA_DEPENDENCIES_FK1
  foreign key (CLIENT_DATA_CONTROL_OID) references CLIENT_DATA_CONTROLS(CLIENT_DATA_CONTROL_OID);

alter table CLIENT_DATA_DEPENDENCIES
  add constraint CLIENT_DATA_DEPENDENCIES_FK2
  foreign key (CLIENT_DATA_CONTROL_OID) references CLIENT_DATA_CONTROLS(CLIENT_DATA_CONTROL_OID);

alter table CLIENT_COUNTRIES
  add constraint CLIENT_COUNTRIES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_COUNTRIES
  add constraint CLIENT_COUNTRIES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CLIENT_COUNTRIES
  add constraint CLIENT_COUNTRIES_FK3
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK2
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK3
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK4
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK5
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK6
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK7
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK8
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK9
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK10
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK11
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK12
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK13
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK14
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK15
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_COLLECTN_SETTINGS
  add constraint CLIENT_COLLECTN_SETTINGS_FK16
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table CLIENT_BANKING_DETAILS
  add constraint CLIENT_BANKING_DETAILS_FK1
  foreign key (BANK_OID) references BANKS(BANK_OID);

alter table CLIENT_BANKING_DETAILS
  add constraint CLIENT_BANKING_DETAILS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CLIENT_BANKING_DETAILS
  add constraint CLIENT_BANKING_DETAILS_FK3
  foreign key (BANK_BRANCHE_OID) references BANK_BRANCHES(BANK_BRANCHE_OID);

alter table CLIENT_BANKING_DETAILS
  add constraint CLIENT_BANKING_DETAILS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_VELOCITY_TOTALS
  add constraint CARD_VELOCITY_TOTALS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_VELOCITY_TOTALS
  add constraint CARD_VELOCITY_TOTALS_FK2
  foreign key (VELOCITY_TYPE_OID) references VELOCITY_TYPES(VELOCITY_TYPE_OID);

alter table CARD_TRANSFER_REQUESTS
  add constraint CARD_TRANSFER_REQUESTS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_TRANSFER_REQUESTS
  add constraint CARD_TRANSFER_REQUESTS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_TRANSFER_REQUESTS
  add constraint CARD_TRANSFER_REQUESTS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_TRANSFER_CARDS
  add constraint CARD_TRANSFER_CARDS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_TRANSFER_CARDS
  add constraint CARD_TRANSFER_CARDS_FK2
  foreign key (CARD_TRANSFER_REQUEST_OID) references CARD_TRANSFER_REQUESTS(CARD_TRANSFER_REQUEST_OID);

alter table CARD_TRANSFER_CARDS
  add constraint CARD_TRANSFER_CARDS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_STATUS_OLS_MAPPING
  add constraint CARD_STATUS_OLS_MAPPING_FK1
  foreign key (CARD_STATUS_OL_OID) references CARD_STATUS_OLS(CARD_STATUS_OL_OID);

alter table CARD_STATUS_OLS_MAPPING
  add constraint CARD_STATUS_OLS_MAPPING_FK2
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_OLS
  add constraint CARD_STATUS_OLS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_STATUS_OLS
  add constraint CARD_STATUS_OLS_FK2
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_LOGS
  add constraint CARD_STATUS_LOGS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_STATUS_LOGS
  add constraint CARD_STATUS_LOGS_FK2
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_CHG_LOGS
  add constraint CARD_STATUS_CHG_LOGS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_STATUS_CHG_LOGS
  add constraint CARD_STATUS_CHG_LOGS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_STATUS_CHG_LOGS
  add constraint CARD_STATUS_CHG_LOGS_FK3
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_CHG_LOGS
  add constraint CARD_STATUS_CHG_LOGS_FK4
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_CHG_LOGS
  add constraint CARD_STATUS_CHG_LOGS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_STATUS_CHANGES
  add constraint CARD_STATUS_CHANGES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_STATUS_CHANGES
  add constraint CARD_STATUS_CHANGES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_STATUS_CHANGES
  add constraint CARD_STATUS_CHANGES_FK3
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS_CHANGES
  add constraint CARD_STATUS_CHANGES_FK4
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_STATUS
  add constraint CARD_STATUS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_STATUS
  add constraint CARD_STATUS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_REQUESTS
  add constraint CARD_REQUESTS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_REQUESTS
  add constraint CARD_REQUESTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_REISS_PROF_CONTROLS
  add constraint CARD_REISS_PROF_CONTROLS_FK1
  foreign key (CARD_REISSUE_PROFILE_OID) references CARD_REISSUE_PROFILES(CARD_REISSUE_PROFILE_OID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK1
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK5
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK6
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table CARD_REISSUE_PROFILES
  add constraint CARD_REISSUE_PROFILES_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PROGRAM_TRANS_TOTS
  add constraint CARD_PROGRAM_TRANS_TOTS_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table CARD_PROGRAM_TRANS_TOTS
  add constraint CARD_PROGRAM_TRANS_TOTS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PROGRAM_TRANS_TOTS
  add constraint CARD_PROGRAM_TRANS_TOTS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CARD_PROGRAM_OFFERS
  add constraint CARD_PROGRAM_OFFERS_FK1
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CARD_PROGRAM_OFFERS
  add constraint CARD_PROGRAM_OFFERS_FK2
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK4
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PROGRAMS
  add constraint CARD_PROGRAMS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCT_TOTALS
  add constraint CARD_PRODUCT_TOTALS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_PRODUCT_TOTALS
  add constraint CARD_PRODUCT_TOTALS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_PRODUCT_TOTALS
  add constraint CARD_PRODUCT_TOTALS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table CARD_PRODUCT_TOTALS
  add constraint CARD_PRODUCT_TOTALS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCT_GRP_PRODUCTS
  add constraint CARD_PRODUCT_GRP_PRODUCTS_FK1
  foreign key (CARD_PRODUCT_GROUP_OID) references CARD_PRODUCT_GROUPS(CARD_PRODUCT_GROUP_OID);

alter table CARD_PRODUCT_GRP_PRODUCTS
  add constraint CARD_PRODUCT_GRP_PRODUCTS_FK2
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CARD_PRODUCT_GROUPS
  add constraint CARD_PRODUCT_GROUPS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK1
  foreign key (CARD_GRAPHIC_OID) references CARD_GRAPHICS(CARD_GRAPHIC_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK11
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK12
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK13
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK14
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK15
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK16
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK17
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK18
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK19
  foreign key (CARD_BRAND_OID) references CARD_BRANDS(CARD_BRAND_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK20
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK21
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK22
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK23
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK24
  foreign key (NUMBERING_SEQUENCE_OID) references NUMBERING_SEQUENCES(NUMBERING_SEQUENCE_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK25
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK26
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK27
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK28
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK29
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK30
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK31
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK32
  foreign key (CLIENT_PVK) references CLIENT_PVK(CLIENT_PVK);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK33
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK34
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK35
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK36
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK37
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK38
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK39
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK40
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK41
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK42
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK43
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK44
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK45
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK46
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK47
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_PRODUCTS
  add constraint CARD_PRODUCTS_FK48
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_OFFER_RULES
  add constraint CARD_OFFER_RULES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_OFFER_RULES
  add constraint CARD_OFFER_RULES_FK2
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CARD_OFFER_RULES
  add constraint CARD_OFFER_RULES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_OFFER_RULES
  add constraint CARD_OFFER_RULES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_OFFERS
  add constraint CARD_OFFERS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_OFFERS
  add constraint CARD_OFFERS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_OFFERS
  add constraint CARD_OFFERS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_NUMBER_SERIES
  add constraint CARD_NUMBER_SERIES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table CARD_HOTLIST_LOGS
  add constraint CARD_HOTLIST_LOGS_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_GRAPHICS_PRODS
  add constraint CARD_GRAPHICS_PRODS_FK1
  foreign key (CARD_GRAPHIC_OID) references CARD_GRAPHICS(CARD_GRAPHIC_OID);

alter table CARD_GRAPHICS_PRODS
  add constraint CARD_GRAPHICS_PRODS_FK2
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table CARD_GRAPHICS
  add constraint CARD_GRAPHICS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_GRAPHICS
  add constraint CARD_GRAPHICS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK3
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK6
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK7
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK8
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK9
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_CONTROL_PROFILES
  add constraint CARD_CONTROL_PROFILES_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK1
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK2
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK3
  foreign key (TIME_LIMIT_OID) references TIME_LIMITS(TIME_LIMIT_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK4
  foreign key (VELOCITY_ASSIGNMENT_OID) references VELOCITY_ASSIGNMENTS(VELOCITY_ASSIGNMENT_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK5
  foreign key (LOCATION_RESTRICTION_OID) references LOCATION_RESTRICTIONS(LOCATION_RESTRICTION_OID);

alter table CARD_CONTROLS
  add constraint CARD_CONTROLS_FK6
  foreign key (EXT_VELOCITY_CTRL_PROF) references EXT_VELOCITY_CTRL_PROF(EXT_VELOCITY_CTRL_PROF);

alter table CARD_CONTACTS
  add constraint CARD_CONTACTS_FK1
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table CARD_CONTACTS
  add constraint CARD_CONTACTS_FK2
  foreign key (ADDRESSE_OID) references ADDRESSES(ADDRESSE_OID);

alter table CARD_BULK_REISSUES
  add constraint CARD_BULK_REISSUES_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_BULK_REISSUES
  add constraint CARD_BULK_REISSUES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARD_BRANDS
  add constraint CARD_BRANDS_FK1
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table CARD_ALLOCATION_ORDERS
  add constraint CARD_ALLOCATION_ORDERS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARD_ALLOCATION_ORDERS
  add constraint CARD_ALLOCATION_ORDERS_FK2
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARD_ALLOCATION_ORDERS
  add constraint CARD_ALLOCATION_ORDERS_FK3
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CARDS_HSM
  add constraint CARDS_HSM_FK1
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARDS_HSM
  add constraint CARDS_HSM_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CARDS
  add constraint CARDS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table CARDS
  add constraint CARDS_FK2
  foreign key (CARD_CONTACT_OID) references CARD_CONTACTS(CARD_CONTACT_OID);

alter table CARDS
  add constraint CARDS_FK3
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table CARDS
  add constraint CARDS_FK4
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table CARDS
  add constraint CARDS_FK5
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table CARDS
  add constraint CARDS_FK6
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table CARDS
  add constraint CARDS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table CARDS
  add constraint CARDS_FK8
  foreign key (CARD_BALANCE_OID) references CARD_BALANCES(CARD_BALANCE_OID);

alter table CARDS
  add constraint CARDS_FK9
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table CARDS
  add constraint CARDS_FK10
  foreign key (DRIVER_OID) references DRIVERS(DRIVER_OID);

alter table CARDS
  add constraint CARDS_FK11
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table CARDS
  add constraint CARDS_FK12
  foreign key (CARD_CONTACT_OID) references CARD_CONTACTS(CARD_CONTACT_OID);

alter table CARDS
  add constraint CARDS_FK13
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARDS
  add constraint CARDS_FK14
  foreign key (PUMP_CONTROL_OID) references PUMP_CONTROLS(PUMP_CONTROL_OID);

alter table CARDS
  add constraint CARDS_FK15
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table CARDS
  add constraint CARDS_FK16
  foreign key (CUSTOMER_REPORT_LEVEL_OID) references CUSTOMER_REPORT_LEVELS(CUSTOMER_REPORT_LEVEL_OID);

alter table CARDS
  add constraint CARDS_FK17
  foreign key (CUSTOMER_REPORT_LEVEL_OID) references CUSTOMER_REPORT_LEVELS(CUSTOMER_REPORT_LEVEL_OID);

alter table CARDS
  add constraint CARDS_FK18
  foreign key (CUSTOMER_REPORT_LEVEL_OID) references CUSTOMER_REPORT_LEVELS(CUSTOMER_REPORT_LEVEL_OID);

alter table CARDS
  add constraint CARDS_FK19
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table CARDS
  add constraint CARDS_FK20
  foreign key (VEHICLE_OID) references VEHICLES(VEHICLE_OID);

alter table CAPTURE_TYPES
  add constraint CAPTURE_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CALENDAR
  add constraint CALENDAR_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table CALENDAR
  add constraint CALENDAR_FK2
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table BULK_CARD_TEMPLATES
  add constraint BULK_CARD_TEMPLATES_FK1
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table BULK_CARD_TEMPLATES
  add constraint BULK_CARD_TEMPLATES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BULK_CARD_TEMPLATES
  add constraint BULK_CARD_TEMPLATES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_TEMPLATES
  add constraint BULK_CARD_TEMPLATES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_TEMPLATES
  add constraint BULK_CARD_TEMPLATES_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK4
  foreign key (INTERNET_USER_OID) references INTERNET_USERS(INTERNET_USER_OID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_HEADERS
  add constraint BULK_CARD_HEADERS_FK6
  foreign key (BULK_CARD_TEMPLATE_OID) references BULK_CARD_TEMPLATES(BULK_CARD_TEMPLATE_OID);

alter table BULK_CARD_ERRORS
  add constraint BULK_CARD_ERRORS_FK1
  foreign key (BULK_CARD_DETAIL_OID) references BULK_CARD_DETAILS(BULK_CARD_DETAIL_OID);

alter table BULK_CARD_ERRORS
  add constraint BULK_CARD_ERRORS_FK2
  foreign key (BULK_CARD_HEADER_OID) references BULK_CARD_HEADERS(BULK_CARD_HEADER_OID);

alter table BULK_CARD_ERRORS
  add constraint BULK_CARD_ERRORS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK1
  foreign key (BULK_CARD_HEADER_OID) references BULK_CARD_HEADERS(BULK_CARD_HEADER_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK2
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK3
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK4
  foreign key (LOCATION_RESTRICTION_OID) references LOCATION_RESTRICTIONS(LOCATION_RESTRICTION_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK5
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK6
  foreign key (PUMP_CONTROL_OID) references PUMP_CONTROLS(PUMP_CONTROL_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_DETAILS
  add constraint BULK_CARD_DETAILS_FK9
  foreign key (TIME_LIMIT_OID) references TIME_LIMITS(TIME_LIMIT_OID);

alter table BULK_CARD_COLUMN_DETAILS
  add constraint BULK_CARD_COLUMN_DETAILS_FK1
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table BULK_CARD_COLUMN_DETAILS
  add constraint BULK_CARD_COLUMN_DETAILS_FK2
  foreign key (BULK_CARD_COLUMN_OID) references BULK_CARD_COLUMNS(BULK_CARD_COLUMN_OID);

alter table BULK_CARD_COLUMNS
  add constraint BULK_CARD_COLUMNS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BULK_CARD_COLUMNS
  add constraint BULK_CARD_COLUMNS_FK2
  foreign key (BULK_CARD_TEMPLATE_OID) references BULK_CARD_TEMPLATES(BULK_CARD_TEMPLATE_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK2
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK4
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BILLING_PLANS
  add constraint BILLING_PLANS_FK8
  foreign key (STATEMENT_MESSAGE_OID) references STATEMENT_MESSAGES(STATEMENT_MESSAGE_OID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK1
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK2
  foreign key (CAPTURE_TYPE_OID) references CAPTURE_TYPES(CAPTURE_TYPE_OID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK3
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK4
  foreign key (TRANS_TRANSLATION_OID) references TRANS_TRANSLATIONS(TRANS_TRANSLATION_OID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK5
  foreign key (TRANS_TRANSLATION_OID) references TRANS_TRANSLATIONS(TRANS_TRANSLATION_OID);

alter table BATCH_TYPES
  add constraint BATCH_TYPES_FK6
  foreign key (EXTERNAL_CLIENT_ID_OID) references EXTERNAL_CLIENT_IDS(EXTERNAL_CLIENT_ID_OID);

alter table BANK_BRANCHES
  add constraint BANK_BRANCHES_FK1
  foreign key (BANK_OID) references BANKS(BANK_OID);

alter table BANK_BRANCHES
  add constraint BANK_BRANCHES_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BANK_ACCOUNTS
  add constraint BANK_ACCOUNTS_FK1
  foreign key (BANK_OID) references BANKS(BANK_OID);

alter table BANK_ACCOUNTS
  add constraint BANK_ACCOUNTS_FK2
  foreign key (BANK_BRANCHE_OID) references BANK_BRANCHES(BANK_BRANCHE_OID);

alter table BANKS
  add constraint BANKS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BANKS
  add constraint BANKS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BANKING_TRANS_GL_SUMMARIES
  add constraint BANKING_TRANS_GL_SUMMARIES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BANKING_TRANS_GL_SUMMARIES
  add constraint BANKING_TRANS_GL_SUMMARIES_FK2
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table BANKING_TRANS_GL_SUMMARIES
  add constraint BANKING_TRANS_GL_SUMMARIES_FK3
  foreign key (GL_ACCOUNT_NUMBER_OID) references GL_ACCOUNT_NUMBERS(GL_ACCOUNT_NUMBER_OID);

alter table BANKING_TRANS_CATEGORIES
  add constraint BANKING_TRANS_CATEGORIES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table BANKING_TRANS_CATEGORIES
  add constraint BANKING_TRANS_CATEGORIES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK4
  foreign key (BANKING_TRANS_CATEGORIE_OID) references BANKING_TRANS_CATEGORIES(BANKING_TRANS_CATEGORIE_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK5
  foreign key (GL_ACCOUNT_NUMBER_OID) references GL_ACCOUNT_NUMBERS(GL_ACCOUNT_NUMBER_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK6
  foreign key (BANKING_HEADER_OID) references BANKING_HEADERS(BANKING_HEADER_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK7
  foreign key (BANKING_STATEMENT_OID) references BANKING_STATEMENTS(BANKING_STATEMENT_OID);

alter table BANKING_TRANSACTIONS
  add constraint BANKING_TRANSACTIONS_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table BANKING_STATEMENTS
  add constraint BANKING_STATEMENTS_FK1
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table BANKING_STATEMENTS
  add constraint BANKING_STATEMENTS_FK2
  foreign key (BANKING_HEADER_OID) references BANKING_HEADERS(BANKING_HEADER_OID);

alter table BAD_DEBT_ACCOUNTS
  add constraint BAD_DEBT_ACCOUNTS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table AUTH_TRANS_LINE_ITEMS
  add constraint AUTH_TRANS_LINE_ITEMS_FK1
  foreign key (AUTH_TRANSACTION_LOG_OID) references AUTH_TRANSACTION_LOGS(AUTH_TRANSACTION_LOG_OID);

alter table AUTH_TRANS_EXCEPTIONS
  add constraint AUTH_TRANS_EXCEPTIONS_FK1
  foreign key (AUTH_TRANSACTION_LOG_OID) references AUTH_TRANSACTION_LOGS(AUTH_TRANSACTION_LOG_OID);

alter table AUTH_TRANS_EXCEPTIONS
  add constraint AUTH_TRANS_EXCEPTIONS_FK2
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table AUTH_TRANS_EXCEPTIONS
  add constraint AUTH_TRANS_EXCEPTIONS_FK3
  foreign key (TRANSACTION_STEP_OID) references TRANSACTION_STEPS(TRANSACTION_STEP_OID);

alter table AUTH_TRANSACTION_LOGS
  add constraint AUTH_TRANSACTION_LOGS_FK1
  foreign key (TRANSACTION_ERROR_OID) references TRANSACTION_ERRORS(TRANSACTION_ERROR_OID);

alter table AUTH_ACQUIRING_INST
  add constraint AUTH_ACQUIRING_INST_FK1
  foreign key (EXTERNAL_CLIENT_ID_OID) references EXTERNAL_CLIENT_IDS(EXTERNAL_CLIENT_ID_OID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK3
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK4
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK5
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK6
  foreign key (AUDIT_CATEGORY_TABLE_OID) references AUDIT_CATEGORY_TABLES(AUDIT_CATEGORY_TABLE_OID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK7
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK8
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table AUDIT_DETAILS
  add constraint AUDIT_DETAILS_FK9
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table AUDIT_CATEGORY_TABLES
  add constraint AUDIT_CATEGORY_TABLES_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table AUDIT_CATEGORY_TABLES
  add constraint AUDIT_CATEGORY_TABLES_FK2
  foreign key (AUDIT_TABLE_OID) references AUDIT_TABLES(AUDIT_TABLE_OID);

alter table AUDIT_CATEGORY_COLUMNS
  add constraint AUDIT_CATEGORY_COLUMNS_FK1
  foreign key (AUDIT_CATEGORY_TABLE_OID) references AUDIT_CATEGORY_TABLES(AUDIT_CATEGORY_TABLE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK1
  foreign key (APPL_TYPE_TRANSFER_OID) references APPL_TYPE_TRANSFERS(APPL_TYPE_TRANSFER_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK2
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK3
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK5
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK7
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK8
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK9
  foreign key (CARD_REISSUE_PROFILE_OID) references CARD_REISSUE_PROFILES(CARD_REISSUE_PROFILE_OID);

alter table APP_TYPE_TRANS_PROFILES
  add constraint APP_TYPE_TRANS_PROFILES_FK10
  foreign key (CARD_REISSUE_PROFILE_OID) references CARD_REISSUE_PROFILES(CARD_REISSUE_PROFILE_OID);

alter table APP_TYPE_TRANS_CARD_PRODS
  add constraint APP_TYPE_TRANS_CARD_PRODS_FK1
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APP_TYPE_TRANS_CARD_PRODS
  add constraint APP_TYPE_TRANS_CARD_PRODS_FK2
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APP_TYPE_TRANS_CARD_PRODS
  add constraint APP_TYPE_TRANS_CARD_PRODS_FK3
  foreign key (APPL_TYPE_TRANSFER_OID) references APPL_TYPE_TRANSFERS(APPL_TYPE_TRANSFER_OID);

alter table APP_TYPE_TRANS_CARD_FEE
  add constraint APP_TYPE_TRANS_CARD_FEE_FK1
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APP_TYPE_TRANS_CARD_FEE
  add constraint APP_TYPE_TRANS_CARD_FEE_FK2
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APP_TYPE_TRANS_CARD_FEE
  add constraint APP_TYPE_TRANS_CARD_FEE_FK3
  foreign key (APPL_TYPE_TRANSFER_OID) references APPL_TYPE_TRANSFERS(APPL_TYPE_TRANSFER_OID);

alter table APP_TYPE_TRANS_CARD_FEE
  add constraint APP_TYPE_TRANS_CARD_FEE_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APP_TYPE_TRANS_CARD_FEE
  add constraint APP_TYPE_TRANS_CARD_FEE_FK5
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APP_TYPE_TRANS_CARD_CTRLS
  add constraint APP_TYPE_TRANS_CARD_CTRLS_FK1
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table APP_TYPE_TRANS_CARD_CTRLS
  add constraint APP_TYPE_TRANS_CARD_CTRLS_FK2
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table APP_TYPE_TRANS_CARD_CTRLS
  add constraint APP_TYPE_TRANS_CARD_CTRLS_FK3
  foreign key (APPL_TYPE_TRANSFER_OID) references APPL_TYPE_TRANSFERS(APPL_TYPE_TRANSFER_OID);

alter table APP_TYPE_TRANS_CARD_CTRLS
  add constraint APP_TYPE_TRANS_CARD_CTRLS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APP_TYPE_TRANS_CARD_CTRLS
  add constraint APP_TYPE_TRANS_CARD_CTRLS_FK5
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APPL_TYPE_TRANSFERS
  add constraint APPL_TYPE_TRANSFERS_FK1
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table APPL_TYPE_TRANSFERS
  add constraint APPL_TYPE_TRANSFERS_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table APPL_TYPE_TRANSFERS
  add constraint APPL_TYPE_TRANSFERS_FK3
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table APPL_TYPE_TRANSFERS
  add constraint APPL_TYPE_TRANSFERS_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APPL_ACCNT_PROFILES
  add constraint APPL_ACCNT_PROFILES_FK1
  foreign key (ACCOUNT_CONTROL_PROFILE_OID) references ACCOUNT_CONTROL_PROFILES(ACCOUNT_CONTROL_PROFILE_OID);

alter table APPL_ACCNT_PROFILES
  add constraint APPL_ACCNT_PROFILES_FK2
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_VAS_OFFERINGS
  add constraint APPLICATION_VAS_OFFERINGS_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_VAS_OFFERINGS
  add constraint APPLICATION_VAS_OFFERINGS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table APPLICATION_VAS_OFFERINGS
  add constraint APPLICATION_VAS_OFFERINGS_FK3
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table APPLICATION_TYPE_REPORTS
  add constraint APPLICATION_TYPE_REPORTS_FK1
  foreign key (REPORT_TYPE_OID) references REPORT_TYPES(REPORT_TYPE_OID);

alter table APPLICATION_TYPE_REPORTS
  add constraint APPLICATION_TYPE_REPORTS_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table APPLICATION_TYPES
  add constraint APPLICATION_TYPES_FK1
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table APPLICATION_TYPES
  add constraint APPLICATION_TYPES_FK2
  foreign key (ACCOUNT_STATU_OID) references ACCOUNT_STATUS(ACCOUNT_STATU_OID);

alter table APPLICATION_TYPES
  add constraint APPLICATION_TYPES_FK3
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table APPLICATION_TYPES
  add constraint APPLICATION_TYPES_FK4
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table APPLICATION_STATUS
  add constraint APPLICATION_STATUS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table APPLICATION_STATUS
  add constraint APPLICATION_STATUS_FK2
  foreign key (LETTER_DEFINITION_OID) references LETTER_DEFINITIONS(LETTER_DEFINITION_OID);

alter table APPLICATION_STATUS
  add constraint APPLICATION_STATUS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APPLICATION_STATUS
  add constraint APPLICATION_STATUS_FK4
  foreign key (WORK_QUEUE_OID) references WORK_QUEUES(WORK_QUEUE_OID);

alter table APPLICATION_STATUS
  add constraint APPLICATION_STATUS_FK5
  foreign key (REPORT_TEMPLATE_OID) references REPORT_TEMPLATES(REPORT_TEMPLATE_OID);

alter table APPLICATION_SOURCES
  add constraint APPLICATION_SOURCES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table APPLICATION_SIGNATORIES
  add constraint APPLICATION_SIGNATORIES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_SIGNATORIES
  add constraint APPLICATION_SIGNATORIES_FK2
  foreign key (APPLICATION_ADDRESSE_OID) references APPLICATION_ADDRESSES(APPLICATION_ADDRESSE_OID);

alter table APPLICATION_FUEL_USAGE
  add constraint APPLICATION_FUEL_USAGE_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_FUEL_USAGE
  add constraint APPLICATION_FUEL_USAGE_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table APPLICATION_FUEL_USAGE
  add constraint APPLICATION_FUEL_USAGE_FK3
  foreign key (APPLICATION_FUEL_DETAIL_OID) references APPLICATION_FUEL_DETAILS(APPLICATION_FUEL_DETAIL_OID);

alter table APPLICATION_FUEL_DETAILS
  add constraint APPLICATION_FUEL_DETAILS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table APPLICATION_CONTACTS
  add constraint APPLICATION_CONTACTS_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_CONTACTS
  add constraint APPLICATION_CONTACTS_FK2
  foreign key (APPLICATION_ADDRESSE_OID) references APPLICATION_ADDRESSES(APPLICATION_ADDRESSE_OID);

alter table APPLICATION_CONTACTS
  add constraint APPLICATION_CONTACTS_FK3
  foreign key (APPLICATION_ADDRESSE_OID) references APPLICATION_ADDRESSES(APPLICATION_ADDRESSE_OID);

alter table APPLICATION_CONTACTS
  add constraint APPLICATION_CONTACTS_FK4
  foreign key (CONTACT_TYPE_OID) references CONTACT_TYPES(CONTACT_TYPE_OID);

alter table APPLICATION_CARD_PROFILES
  add constraint APPLICATION_CARD_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_CARD_PROFILES
  add constraint APPLICATION_CARD_PROFILES_FK2
  foreign key (CARD_CONTROL_PROFILE_OID) references CARD_CONTROL_PROFILES(CARD_CONTROL_PROFILE_OID);

alter table APPLICATION_CARD_PROFILES
  add constraint APPLICATION_CARD_PROFILES_FK3
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APPLICATION_CARD_PROFILES
  add constraint APPLICATION_CARD_PROFILES_FK4
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table APPLICATION_CARD_PROFILES
  add constraint APPLICATION_CARD_PROFILES_FK5
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APPLICATION_CARD_PRODUCTS
  add constraint APPLICATION_CARD_PRODUCTS_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table APPLICATION_CARD_PRODUCTS
  add constraint APPLICATION_CARD_PRODUCTS_FK2
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table APPLICATION_CARD_PRODUCTS
  add constraint APPLICATION_CARD_PRODUCTS_FK3
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table APPLICATION_CARD_PRODUCTS
  add constraint APPLICATION_CARD_PRODUCTS_FK4
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table APPLICATION_BANK_ACCOUNTS
  add constraint APPLICATION_BANK_ACCOUNTS_FK1
  foreign key (BANK_OID) references BANKS(BANK_OID);

alter table APPLICATION_BANK_ACCOUNTS
  add constraint APPLICATION_BANK_ACCOUNTS_FK2
  foreign key (BANK_BRANCHE_OID) references BANK_BRANCHES(BANK_BRANCHE_OID);

alter table APPLICATION_ADDRESSES
  add constraint APPLICATION_ADDRESSES_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table APPLICATION_ADDRESSES
  add constraint APPLICATION_ADDRESSES_FK2
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK1
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK2
  foreign key (APPLICATION_SOURCE_OID) references APPLICATION_SOURCES(APPLICATION_SOURCE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK3
  foreign key (APPLICATION_STATU_OID) references APPLICATION_STATUS(APPLICATION_STATU_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK4
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK5
  foreign key (APPLICATION_BANK_ACCOUNT_OID) references APPLICATION_BANK_ACCOUNTS(APPLICATION_BANK_ACCOUNT_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK6
  foreign key (BILLING_PLAN_OID) references BILLING_PLANS(BILLING_PLAN_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK7
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK8
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK9
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK10
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK11
  foreign key (CARD_GRAPHIC_OID) references CARD_GRAPHICS(CARD_GRAPHIC_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK12
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK13
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK14
  foreign key (CREDIT_PLAN_OID) references CREDIT_PLANS(CREDIT_PLAN_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK15
  foreign key (CARD_REISSUE_PROFILE_OID) references CARD_REISSUE_PROFILES(CARD_REISSUE_PROFILE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK16
  foreign key (CURRENCIE_OID) references CURRENCIES(CURRENCIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK17
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK18
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK19
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK20
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK21
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK22
  foreign key (MONTH_OID) references MONTHS(MONTH_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK23
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK24
  foreign key (INDUSTRIE_OID) references INDUSTRIES(INDUSTRIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK25
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK26
  foreign key (LANGUAGE_OID) references LANGUAGES(LANGUAGE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK27
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK28
  foreign key (APPLICATION_ADDRESSE_OID) references APPLICATION_ADDRESSES(APPLICATION_ADDRESSE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK29
  foreign key (PRICING_PROFILE_OID) references PRICING_PROFILES(PRICING_PROFILE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK30
  foreign key (PUMP_CONTROL_OID) references PUMP_CONTROLS(PUMP_CONTROL_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK31
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK32
  foreign key (APPLICATION_ADDRESSE_OID) references APPLICATION_ADDRESSES(APPLICATION_ADDRESSE_OID);

alter table APPLICATIONS
  add constraint APPLICATIONS_FK33
  foreign key (MAIL_INDICATOR_OID) references MAIL_INDICATORS(MAIL_INDICATOR_OID);

alter table ALLOCATIONS
  add constraint ALLOCATIONS_FK1
  foreign key (DETAIL_GROUP_OID) references DETAIL_GROUPS(DETAIL_GROUP_OID);

alter table ALLOCATIONS
  add constraint ALLOCATIONS_FK2
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table AGING_BUCKETS
  add constraint AGING_BUCKETS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table AGED_AMOUNTS
  add constraint AGED_AMOUNTS_FK1
  foreign key (AGING_BUCKET_OID) references AGING_BUCKETS(AGING_BUCKET_OID);

alter table AGED_AMOUNTS
  add constraint AGED_AMOUNTS_FK2
  foreign key (PERIOD_BALANCE_OID) references PERIOD_BALANCES(PERIOD_BALANCE_OID);

alter table ADJ_TRANS_LINE_ITEMS
  add constraint ADJ_TRANS_LINE_ITEMS_FK1
  foreign key (PRODUCT_OID) references PRODUCTS(PRODUCT_OID);

alter table ADJ_TRANS_LINE_ITEMS
  add constraint ADJ_TRANS_LINE_ITEMS_FK2
  foreign key (ADJ_TRANSACTION_OID) references ADJ_TRANSACTIONS(ADJ_TRANSACTION_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK1
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK2
  foreign key (LOCATION_MID) references M_LOCATIONS(LOCATION_MID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK3
  foreign key (POS_LOCN_RECONCILIATION_OID) references POS_LOCN_RECONCILIATIONS(POS_LOCN_RECONCILIATION_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK4
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK5
  foreign key (MERCHANT_MID) references M_MERCHANTS(MERCHANT_MID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK6
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK7
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK8
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK9
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADJ_TRANSACTIONS
  add constraint ADJ_TRANSACTIONS_FK10
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK2
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK3
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK5
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK6
  foreign key (TRANSACTION_TYPE_OID) references TRANSACTION_TYPES(TRANSACTION_TYPE_OID);

alter table ADJUSTMENT_TYPES
  add constraint ADJUSTMENT_TYPES_FK7
  foreign key (GL_ACCOUNT_CODE_OID) references GL_ACCOUNT_CODES(GL_ACCOUNT_CODE_OID);

alter table ADDRESS_CLEANSING
  add constraint ADDRESS_CLEANSING_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADDRESS_CLEANSING
  add constraint ADDRESS_CLEANSING_FK2
  foreign key (CONTACT_TYPE_OID) references CONTACT_TYPES(CONTACT_TYPE_OID);

alter table ADDRESS_CLEANSING
  add constraint ADDRESS_CLEANSING_FK3
  foreign key (MEMBER_OID) references MEMBERS(MEMBER_OID);

alter table ADDRESS_CLEANSING
  add constraint ADDRESS_CLEANSING_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ADDRESSES
  add constraint ADDRESSES_FK1
  foreign key (COUNTRIE_OID) references COUNTRIES(COUNTRIE_OID);

alter table ADDRESSES
  add constraint ADDRESSES_FK2
  foreign key (STATE_OID) references STATES(STATE_OID);

alter table ACCOUNT_VELOCITY_TOTALS
  add constraint ACCOUNT_VELOCITY_TOTALS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_VELOCITY_TOTALS
  add constraint ACCOUNT_VELOCITY_TOTALS_FK2
  foreign key (VELOCITY_TYPE_OID) references VELOCITY_TYPES(VELOCITY_TYPE_OID);

alter table ACCOUNT_TOTALS
  add constraint ACCOUNT_TOTALS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_TOTALS
  add constraint ACCOUNT_TOTALS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_TOTALS
  add constraint ACCOUNT_TOTALS_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_SUB_STATUS_CHANGES
  add constraint ACCOUNT_SUB_STATUS_CHANGES_FK1
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table ACCOUNT_SUB_STATUS_CHANGES
  add constraint ACCOUNT_SUB_STATUS_CHANGES_FK2
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table ACCOUNT_SUB_STATUS
  add constraint ACCOUNT_SUB_STATUS_FK1
  foreign key (CARD_STATU_OID) references CARD_STATUS(CARD_STATU_OID);

alter table ACCOUNT_SUB_STATUS
  add constraint ACCOUNT_SUB_STATUS_FK2
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ACCOUNT_SUB_STATUS
  add constraint ACCOUNT_SUB_STATUS_FK3
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ACCOUNT_SUB_STATUS
  add constraint ACCOUNT_SUB_STATUS_FK4
  foreign key (ACCOUNT_STATU_OID) references ACCOUNT_STATUS(ACCOUNT_STATU_OID);

alter table ACCOUNT_STATUS_LOGS
  add constraint ACCOUNT_STATUS_LOGS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_STATUS_LOGS
  add constraint ACCOUNT_STATUS_LOGS_FK2
  foreign key (ACCOUNT_STATU_OID) references ACCOUNT_STATUS(ACCOUNT_STATU_OID);

alter table ACCOUNT_STATUS_LOGS
  add constraint ACCOUNT_STATUS_LOGS_FK3
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table ACCOUNT_STATUS
  add constraint ACCOUNT_STATUS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ACCOUNT_STATUS
  add constraint ACCOUNT_STATUS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_FEE_LOGS
  add constraint ACCOUNT_FEE_LOGS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_FEE_LOGS
  add constraint ACCOUNT_FEE_LOGS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table ACCOUNT_FEE_LOGS
  add constraint ACCOUNT_FEE_LOGS_FK3
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table ACCOUNT_FEE_ITEMS
  add constraint ACCOUNT_FEE_ITEMS_FK1
  foreign key (ACCOUNT_FEE_OID) references ACCOUNT_FEES(ACCOUNT_FEE_OID);

alter table ACCOUNT_FEE_ITEMS
  add constraint ACCOUNT_FEE_ITEMS_FK2
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table ACCOUNT_FEE_ITEMS
  add constraint ACCOUNT_FEE_ITEMS_FK3
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table ACCOUNT_FEE_ITEMS
  add constraint ACCOUNT_FEE_ITEMS_FK4
  foreign key (TRANSACTION_FEE_OID) references TRANSACTION_FEES(TRANSACTION_FEE_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK3
  foreign key (CARD_OID) references CARDS(CARD_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK4
  foreign key (FEE_OID) references FEES(FEE_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK5
  foreign key (FEE_VALUE_OID) references FEE_VALUES(FEE_VALUE_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK6
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_FEES
  add constraint ACCOUNT_FEES_FK7
  foreign key (TRANSACTION_OID) references TRANSACTIONS(TRANSACTION_OID);

alter table ACCOUNT_EVENT_STATS
  add constraint ACCOUNT_EVENT_STATS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_EVENT_STATS
  add constraint ACCOUNT_EVENT_STATS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK1
  foreign key (APPLICATION_OID) references APPLICATIONS(APPLICATION_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK2
  foreign key (APPLICATION_TYPE_OID) references APPLICATION_TYPES(APPLICATION_TYPE_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK3
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK4
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK5
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK6
  foreign key (CARD_OFFER_OID) references CARD_OFFERS(CARD_OFFER_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK7
  foreign key (CARD_PRODUCT_OID) references CARD_PRODUCTS(CARD_PRODUCT_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK8
  foreign key (CARD_PROGRAM_OID) references CARD_PROGRAMS(CARD_PROGRAM_OID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK9
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table ACCOUNT_CONTROL_PROFILES
  add constraint ACCOUNT_CONTROL_PROFILES_FK10
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK1
  foreign key (ACCOUNT_CONTROL_PROFILE_OID) references ACCOUNT_CONTROL_PROFILES(ACCOUNT_CONTROL_PROFILE_OID);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK2
  foreign key (EXT_VELOCITY_CTRL_PROF) references EXT_VELOCITY_CTRL_PROF(EXT_VELOCITY_CTRL_PROF);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK3
  foreign key (LOCATION_RESTRICTION_OID) references LOCATION_RESTRICTIONS(LOCATION_RESTRICTION_OID);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK4
  foreign key (PRODUCT_RESTRICTION_OID) references PRODUCT_RESTRICTIONS(PRODUCT_RESTRICTION_OID);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK5
  foreign key (TIME_LIMIT_OID) references TIME_LIMITS(TIME_LIMIT_OID);

alter table ACCOUNT_CONTROLS
  add constraint ACCOUNT_CONTROLS_FK6
  foreign key (VELOCITY_ASSIGNMENT_OID) references VELOCITY_ASSIGNMENTS(VELOCITY_ASSIGNMENT_OID);

alter table ACCOUNT_AMOUNT_STATS
  add constraint ACCOUNT_AMOUNT_STATS_FK1
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNT_AMOUNT_STATS
  add constraint ACCOUNT_AMOUNT_STATS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK1
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK2
  foreign key (BANK_ACCOUNT_OID) references BANK_ACCOUNTS(BANK_ACCOUNT_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK3
  foreign key (BILLING_PLAN_OID) references BILLING_PLANS(BILLING_PLAN_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK4
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK5
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK6
  foreign key (CREDIT_CARD_OID) references CREDIT_CARDS(CREDIT_CARD_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK7
  foreign key (CREDIT_PLAN_OID) references CREDIT_PLANS(CREDIT_PLAN_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK8
  foreign key (CUSTOMER_MID) references M_CUSTOMERS(CUSTOMER_MID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK9
  foreign key (FREQUENCIE_OID) references FREQUENCIES(FREQUENCIE_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK10
  foreign key (ACCOUNT_OID) references ACCOUNTS(ACCOUNT_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK11
  foreign key (DUNNING_CODE_OID) references DUNNING_CODES(DUNNING_CODE_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK12
  foreign key (FEE_PROFILE_OID) references FEE_PROFILES(FEE_PROFILE_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK13
  foreign key (DUNNING_CODE_OID) references DUNNING_CODES(DUNNING_CODE_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK14
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK15
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK16
  foreign key (DESCRIPTION_OID) references DESCRIPTIONS(DESCRIPTION_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK17
  foreign key (ACCOUNT_STATU_OID) references ACCOUNT_STATUS(ACCOUNT_STATU_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK18
  foreign key (ACCOUNT_SUB_STATU_OID) references ACCOUNT_SUB_STATUS(ACCOUNT_SUB_STATU_OID);

alter table ACCOUNTS
  add constraint ACCOUNTS_FK19
  foreign key (TERRITORIE_OID) references TERRITORIES(TERRITORIE_OID);

alter table ACCESS_GROUP_CLIENTS
  add constraint ACCESS_GROUP_CLIENTS_FK1
  foreign key (CLIENT_MID) references M_CLIENTS(CLIENT_MID);

alter table ACCESS_GROUP_CLIENTS
  add constraint ACCESS_GROUP_CLIENTS_FK2
  foreign key (ACCESS_GROUP_OID) references ACCESS_GROUPS(ACCESS_GROUP_OID);

alter table ACCESS_GROUPS
  add constraint ACCESS_GROUPS_FK1
  foreign key (CLIENT_GROUP_MID) references M_CLIENT_GROUPS(CLIENT_GROUP_MID);

alter table ACCESS_GROUPS
  add constraint ACCESS_GROUPS_FK2
  foreign key (CONSTANT_OID) references CONSTANTS(CONSTANT_OID);

