#
# DDL forward generator
#
#  - custom XSD parser for Sparx Enterprise Architect
#
TBL = /<xs:complexType name="(.+)">/
COL = /<xs:element name="(.+)" type="xs:(.+)" min/
REF = /<xs:element name="(.+)" type="(.+)" min/
TB0 = /<\/xs:complexType>/

def get_pk(v)
  case v
  when /^M_(.+)S/; "#{$1}_MID"
  when /^(.+)S/;   "#{$1}_OID"
  else v
  end
end

fk  = []
tbl = nil
n   = 0
ARGF.readlines.each do |r|
  case r
  when TBL
    tbl = $1
    n   = 0
    puts "create table #{tbl} ("
  when TB0
    k   = get_pk(tbl)
    puts "  constraint #{tbl}_PK primary key (#{k}) using index);\n\n"
  when COL
    col, t = $1, $2
    puts sprintf("  %-32s", col)+(t=='string' ? 'varchar2(200)' : t)+','
  when REF
    k, ref = $1, $2
    fk << [ tbl, n+=1, ref ]
  end
end

fk.each do |t, n, r|
  k = get_pk(r)
  puts "alter table #{t}\n  add constraint #{t}_FK#{n}\n  foreign key (#{k}) references #{r}(#{k});\n\n"
end




