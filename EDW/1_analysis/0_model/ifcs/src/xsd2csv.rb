#
# DDL forward generator
#
#  - custom XSD parser for Sparx Enterprise Architect
#
ANA = /<xs:documentation>(.+)<\/xs:documentation>/
TBL = /<xs:complexType name="(.+)">/
COL = /<xs:element name="(.+)" type="xs:(.+)" min/
COX = /<\/xs:element>/
REF = /<xs:element name="(.+)" type="(.+)" min/

def get_pk(v)
  case v
  when /^M_(.+)S/; "#{$1}_MID"
  when /^(.+)S/;   "#{$1}_OID"
  else v
  end
end

fk  = []
tbl, doc, col, t = nil, nil, nil, nil
n, c = 0, 0
ARGF.readlines.each do |r|
  case r
  when ANA
    doc = $1
  when TBL
    tbl = $1
    n   = c = 0
  when COL
    col, t = $1, $2
    puts "TABLE|#{tbl}||||#{doc}" if c==0
    doc =  nil
    c   += 1
  when COX
    puts "COLUMN|#{tbl}|#{c}|#{col}|#{t}|#{doc}"
  when REF
    k, ref = $1, $2
    fk << [ tbl, n+=1, ref ]
  end
end

fk.each do |t, n, r|
  k = get_pk(r)
  puts "FK|#{t}|#{n}|#{r}|#{k}"
end




