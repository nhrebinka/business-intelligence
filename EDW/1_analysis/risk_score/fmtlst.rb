require 'win32ole'

SRC="/devel/chochain/wex/bi4/test/risk_score/" 

xl = WIN32OLE.new('Excel.Application');

Dir['2013/*.xlsx', '2014/*.xlsx', '2015/*.xlsx'].sort.each do |f|
  wb = xl.workbooks.open SRC+f
  ws = wb.worksheets('Column Definitions')
  puts f+'|def|'+ws.range('A1:A100').value.flatten.join('|')
  puts f+'|desc|'+ws.range('B1:B100').value.flatten.join('|')
  wb.saved = true
  wb.close
end
