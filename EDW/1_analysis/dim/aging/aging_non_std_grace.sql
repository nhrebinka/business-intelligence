--
-- non-standardard aging with 1 day grace period
--
select
  o.BUSINESS_DATE,
  o.CUST_ID,
  SUM(decode(o.AR_AGE,  0, o.total)) AGE_00,
  SUM(decode(o.AR_AGE, -1, o.total)) AGE_YET,
  SUM(decode(o.AR_AGE,  1, o.total)) AGE_30,
  SUM(decode(o.AR_AGE,  2, o.total)) AGE_60,
  SUM(decode(o.AR_AGE,  3, o.total)) AGE_90,
  SUM(decode(o.AR_AGE,  4, o.total)) AGE_120,
  SUM(decode(o.AR_AGE,  5, o.total)) AGE_150,
  SUM(decode(o.AR_AGE,  6, o.total)) AGE_180,
  SUM(decode(o.AR_AGE,  7, o.total)) AGE_LONG,
  SUM(decode(o.DU_AGE,  0, o.total)) DUE_00,
  SUM(decode(o.DU_AGE, -1, o.total)) DUE_YET,
  SUM(decode(o.DU_AGE,  1, o.total)) DUE_30,
  SUM(decode(o.DU_AGE,  2, o.total)) DUE_60,
  SUM(decode(o.DU_AGE,  3, o.total)) DUE_90,
  SUM(decode(o.DU_AGE,  4, o.total)) DUE_120,
  SUM(decode(o.DU_AGE,  5, o.total)) DUE_150,
  SUM(decode(o.DU_AGE,  6, o.total)) DUE_180,
  SUM(decode(o.DU_AGE,  7, o.total)) DUE_LONG
FROM (
  --
  -- non-standardard aging without min dates
  -- note: the result turns out the same as with min dates
  --
  SELECT /*+ parallel(pi, 16) full(pi) index_ffs(pia, PSCITEM_ACTIVITY) parallel_index(pia, PSCITEM_ACTIVITY, 16) */
    pwcd.BUSINESS_DATE,
    pia.CUST_ID,
    CASE 
    WHEN floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30) < 0 THEN -1
    WHEN floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30) > 6 THEN 7
    ELSE floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30)
    END AR_AGE,
    CASE 
    WHEN floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30) < 0 THEN -1
    WHEN floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30) > 6 THEN 7
    ELSE floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30)
    END DU_AGE,
    SUM(pia.ENTRY_AMT) TOTAL
  FROM PS_ITEM_ACTIVITY pia
  JOIN PS_ITEM pi ON (1=1 
    AND pia.ITEM          = pi.ITEM
    AND pia.CUST_ID       = pi.CUST_ID
    AND pia.BUSINESS_UNIT = pi.BUSINESS_UNIT
  )
  JOIN PS_WX_CUST_DAILY pwcd ON (1=1
    AND pia.CUST_ID       = pwcd.CUST_ID
    AND pia.BUSINESS_UNIT = pwcd.BUSINESS_UNIT
    AND pia.POST_DT      <= pwcd.BUSINESS_DATE
    AND pwcd.BUSINESS_DATE BETWEEN TO_DATE ('04/01/2014', 'MM/DD/YYYY') 
                           AND     TO_DATE ('04/01/2014', 'MM/DD/YYYY')
  )
  JOIN (
    SELECT CUST_ID FROM (
      SELECT /*+ parallel 16 */ CUST_ID, AGING_ID,
        ROW_NUMBER() OVER (PARTITION BY CUST_ID ORDER BY EFFDT DESC) rn 
      FROM  PS_CUST_CREDIT
    )
    WHERE rn=1
      AND AGING_ID<>'STD'
  ) mcc ON (mcc.CUST_ID = pia.CUST_ID)
  WHERE 1=1
  GROUP BY 
    pwcd.BUSINESS_DATE,
    pia.CUST_ID,
    CASE 
    WHEN floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30) < 0 THEN -1
    WHEN floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30) > 6 THEN 7
    ELSE floor((pwcd.BUSINESS_DATE-pi.ACCOUNTING_DT+1)/30)
    END,
    CASE 
    WHEN floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30) < 0 THEN -1
    WHEN floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30) > 6 THEN 7
    ELSE floor((pwcd.BUSINESS_DATE-pi.DUE_DT+1)/30)
    END
  HAVING SUM(pia.ENTRY_AMT)<>0
) o
GROUP BY o.BUSINESS_DATE, o.CUST_ID
;
