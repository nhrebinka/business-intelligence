CREATE OR REPLACE PROCEDURE           F_DAILY_ACCOUNT_AGGREGATES
AS
   /*
   NAME                    :-     F_DAILY_ACCOUNT_AGGREGATES
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    06/2010
   DECRIPTION :-   PROCEURE TO  load daily Account Aggregates  into F_DAILY_ACCOUNT_AGGREGATES
   LOG            : -   VERSION 1.0

                     :- Version 2.0 Modified By Venugopal Kancharla
                       DAte :- 10/11/2010
                       Added the F_SEED_DATES procedure to autoamte the history  laoding
                       and added the  indexes Drop and Recreate ...

                  :-  Version 2.1 Modified By Venu KAncharla
                     Date :- 12/12/2010
                     Adding the Filter in the where Clause

                 :- version 2,2
                  commented  DW_Current_FLG = '1'  01/17/2011 by VK since this is not needed

               :- Version 2.3
                Added the new Seed Dates to the PROD D_SEED_DATES    By VK on  03/31/2011

              :- Version 2.4
                 Added BY VK  on 05/24/2011  to elimate the Card counts  for Future Dates  for re-issues  ....durinng historic  Data processing


             :- Version 2.5
                   Added By VK on 07/25/2013 Code to Calculate MAX_NUMBER_OF_OUTRSTANDING CARDS . .(Fleet Size ) This is being added to
                   imporve performance of BO queries ...

             :-  Version 2.6
                   Added By VK  on 09/06/2013
                       changes for the  Fleet Size Logic based on the Business Inputs..  adding the Account_hist_key

             :-  Version 2.7
                   Added By VK  on 09/26/2013
                       Added Moved and Converted Filters apart from ther existing Terminated Filters ..To move these Card status from the outstanding Card Count ...


  */


   v_calendar_dt_start                   DATE;
   v_calendar_dt_end                     DATE;
   v_first_day_in_month_date_dt          DATE ;

   v_first_date_key                      INTEGER ;
   V_PART                                VARCHAR2 (10);
   V_SQL                                 VARCHAR2 (4000);


BEGIN
   -- Seed Procedure and Indexes management  Added By venu on 11/09/2010

   D_SEED_DATES (v_calendar_dt_start, v_calendar_dt_end);

   DBMS_OUTPUT.PUT_LINE (v_calendar_dt_start);
   DBMS_OUTPUT.PUT_LINE (v_calendar_dt_end);

   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT, D.DATE_KEY , D.FIRST_DAY_IN_MONTH_DATE_DT , D.LAST_DAY_IN_MONTH_DATE_DT
            FROM D_DATE D
           WHERE D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start
                                        AND v_calendar_dt_end)
   LOOP
    BEGIN

            DELETE FROM  F_DAILY_ACCOUNT_SNAPSHOT F
            WHERE  F.DATE_KEY =  rec.DATE_KEY  ;   --- Added By Vk for Version 2.4 so that the  PROC can be made Rerunable ....

            COMMIT ;

      INSERT INTO F_DAILY_ACCOUNT_SNAPSHOT DAS (
                     DAS.DAILY_ACCOUNT_SNAPSHOT_KEY,
                     DAS.ACCOUNT_KEY,
                     DAS.PROGRAM_KEY,
                     DAS.DATE_KEY,
                     DAS.REGION_KEY,
                     /* daily cards snapshot */
                     DAS.OUTSTANDING_CARDS_COUNT,
                     DAS.TERMINAL_CARD_COUNT,
                     DAS.TOTAL_CARDS_COUNT,
                     DAS.CARDS_IN_TRANSITION_COUNT,
                     DAS.VIRTUAL_OUTSTANDING_CARD_COUNT,
                     DAS.VIRTUAL_TERMINAL_CARD_COUNT,
                     DAS.VIRTUAL_TOTAL_CARD_COUNT,
                     DAS.VIRTUAL_CARDS_IN_TRANS_COUNT,
                     DAS.MAG_OUTSTANDING_CARD_COUNT,
                     DAS.MAG_TERMINAL_CARD_COUNT,
                     DAS.MAG_TOTAL_CARD_COUNT,
                     DAS.MAG_CARDS_IN_TRANS_COUNT,
                     /* daily card  transactions */
                     DAS.ISSUED_CARDS_COUNT,
                     DAS.TERMINATED_CARDS_COUNT,
                     DAS.TRANS_CARDS_COUNT,
                     DAS.VIRTUAL_ISSUED_CARDS_COUNT,
                     DAS.VIRTUAL_TERMINATED_CARDS_COUNT,
                     DAS.VIRTUAL_TRANS_CARDS_COUNT,
                     DAS.MAG_ISSUED_CARDS_COUNT,
                     DAS.MAG_TERMINATED_CARDS_COUNT,
                     DAS.MAG_TRANS_CARD_COUNT,
                     DAS.ROW_CREATE_DTTM,
                     DAS.ROW_LAST_MOD_DTTM,
                     DAS.ROW_LAST_MOD_PROC_NM,
                     DAS.ROW_LAST_MOD_PROC_SEQ_NBR ,
                     DAS.ACCOUNT_HIST_KEY  )
         (SELECT DAILY_ACCOUNT_SNAPSHOT_KEY_SEQ.NEXTVAL
                    AS DAILY_ACCOUNT_SNAPSHOT_KEY,
                 FRA.ACCOUNT_KEY,
                 FRA.PROGRAM_KEY,
                 FRA.DATE_KEY,
                 FRA.REGION_KEY,
                 FRA.OUTSTANDING_CARDS_COUNT,
                 FRA.TERMINAL_CARD_COUNT,
                 FRA.TOTAL_CARDS_COUNT,
                 FRA.CARDS_IN_TRANSITION_COUNT,
                 FRA.VIRTUAL_OUTSTANDING_CARD_COUNT,
                 FRA.VIRTUAL_TERMINAL_CARD_COUNT,
                 FRA.VIRTUAL_TOTAL_CARD_COUNT,
                 FRA.VIRTUAL_CARDS_IN_TRANS_COUNT,
                 FRA.MAG_OUTSTANDING_CARD_COUNT,
                 FRA.MAG_TERMINAL_CARD_COUNT,
                 FRA.MAGSTRIPE_DEVICE_COUNT,
                 FRA.MAG_CARDS_IN_TRANS_COUNT,
                 FRA.ISSUED_CARDS_COUNT,
                 FRA.TERMINATED_CARDS_COUNT,
                 FRA.TRANS_CARDS_COUNT,
                 FRA.VIRTUAL_ISSUED_CARDS_COUNT,
                 FRA.VIRTUAL_TERMINATED_CARDS_COUNT,
                 FRA.VIRTUAL_TRANS_CARDS_COUNT,
                 FRA.MAG_ISSUED_CARDS_COUNT,
                 FRA.MAG_TERMINATED_CARDS_COUNT,
                 FRA.MAG_TRANS_CARD_COUNT,
                 FRA.ROW_CREATE_DTTM,
                 FRA.ROW_LAST_MOD_DTTM,
                 FRA.ROW_LAST_MOD_PROC_NM,
                 FRA.ROW_LAST_MOD_PROC_SEQ_NBR ,
                 FRA.ACCOUNT_HIST_KEY
            FROM (  SELECT A.ACCOUNT_KEY AS ACCOUNT_KEY,
                           PDE.PROGRAM_KEY AS PROGRAM_KEY,
                           rec.DATE_KEY AS DATE_KEY,
                           0 AS REGION_KEY,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC IN ( 'Terminated' , 'Moved' , 'Converted') --added By VK on 09/26/2013  per Bill Kelley ..to remove these Card types ..
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.TERMINATED_COUNT1
                                   ELSE
                                      0
                                END)
                              AS OUTSTANDING_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.TERMINATED_COUNT1
                                 ELSE
                                    0
                              END)
                              AS TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS TOTAL_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.IN_TRANSITION_COUNT1
                                 ELSE
                                    0
                              END)
                              AS CARDS_IN_TRANSITION_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.VIRTUAL_DEVICE_COUNT1
                                   ELSE
                                      0
                                END)
                              AS VIRTUAL_OUTSTANDING_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.TERMINATED_COUNT1
                                 ELSE
                                    0
                              END),
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TOTAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_CARDS_IN_TRANS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.PURCHASE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                           - SUM (
                                CASE
                                   WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                        AND PD.TERMINATED_DATE <=
                                               REC.CALENDAR_DATE_DT
                                   THEN
                                      PD.MAGSTRIPE_DEVICE_COUNT1
                                   ELSE
                                      0
                                END)
                              AS MAG_OUTSTANDING_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END),
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'Terminated'
                                      AND PD.TERMINATED_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_TERMINAL_CARD_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE <= REC.CALENDAR_DATE_DT
                                      AND PD.ACTIVATED_DATE <=
                                             REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAGSTRIPE_DEVICE_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.CARD_STATUS_DESC = 'In Transition'
                                      AND PD.IN_TRANSITION_DATE <=
                                             REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_CARDS_IN_TRANS_COUNT,
                           /* daily card  transactions */
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE       = REC.CALENDAR_DATE_DT
                                   AND PD.ACTIVATED_DATE <= REC.CALENDAR_DATE_DT -- Added BY VK  on 05/24/2011  to elimate the Card counts for Future Dates for re-issues  ....during historic  Data processing
                                 THEN
                                    1
                                 ELSE
                                    0
                              END)
                              AS ISSUED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.TERMINATED_DATE = REC.CALENDAR_DATE_DT
                                 THEN
                                    1
                                 ELSE
                                    0
                              END)
                              AS TERMINATED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.IN_TRANSITION_DATE =
                                         REC.CALENDAR_DATE_DT
                                 THEN
                                    1
                                 ELSE
                                    0
                              END)
                              AS TRANS_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE = REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_ISSUED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.TERMINATED_DATE = REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TERMINATED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.IN_TRANSITION_DATE =
                                         REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.VIRTUAL_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS VIRTUAL_TRANS_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.ISSUED_DATE = REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_ISSUED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.TERMINATED_DATE = REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_TERMINATED_CARDS_COUNT,
                           SUM (
                              CASE
                                 WHEN PD.IN_TRANSITION_DATE =
                                         REC.CALENDAR_DATE_DT
                                 THEN
                                    PD.MAGSTRIPE_DEVICE_COUNT1
                                 ELSE
                                    0
                              END)
                              AS MAG_TRANS_CARD_COUNT,
                           SYSDATE AS ROW_CREATE_DTTM,
                           SYSDATE AS ROW_LAST_MOD_DTTM,
                           'F_DAILY_ACCOUNT_AGGREGATES' AS ROW_LAST_MOD_PROC_NM,
                           1 AS ROW_LAST_MOD_PROC_SEQ_NBR ,
                           A.ACCOUNT_HIST_KEY
                      FROM        D_Account A
                           JOIN     F_Purchase_Device_Event PDE
                              ON (A.ACCOUNT_KEY = PDE.ACCOUNT_KEY )
                           JOIN     D_Purchase_Device PD
                              ON ( PD.PURCHASE_DEVICE_KEY  =   PDE.PURCHASE_DEVICE_KEY )
                     WHERE
                                   A.ACCOUNT_CLOSED_DATE    >= REC.CALENDAR_DATE_DT
                           AND A.ACCOUNT_OPEN_DATE      <= REC.CALENDAR_DATE_DT -- filter for active accounts
                  --    AND A.CURRENT_RECORD_FLG = '1'    commented out on 01/17/2011 by VK since this is not needed ...
                  GROUP BY
                            A.ACCOUNT_KEY, PDE.PROGRAM_KEY , A.ACCOUNT_HIST_KEY  ) FRA);

      COMMIT;
      END ;

      --  Added By VK on 07/25/2013 For MAX Number Of outstnding CARD COUNTS

       V_PART := 'YM_' || TO_CHAR (REC.LAST_DAY_IN_MONTH_DATE_DT, 'YYYY_MM');

      -- Code to get the First Day of the month

      SELECT D.DATE_KEY
        INTO
            V_FIRST_DATE_KEY
      FROM   D_DATE D
      WHERE
           D.CALENDAR_DATE_DT =  rec.FIRST_DAY_IN_MONTH_DATE_DT ;

      -- Actual Code to find the MAx Number of outstanding CARDCOUNT ,From the starting of the month until that day ....
      -- Note Max number outstnading CArd Count is updated where Account_key = Account_hist_key


      V_SQL :=  'MERGE INTO /*+ parallel */ EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT PARTITION ('
                || V_PART || ')FDAS
                 USING
                    ( SELECT      ACCOUNT_HIST_KEY ,
                                  MAX(MAX_OUTSTANDING_CARD_COUNT) MAX_OUTSTANDING_CARD_COUNT
                       FROM
                           (SELECT   F.ACCOUNT_HIST_KEY             ACCOUNT_HIST_KEY,
                                     F.DATE_KEY                     DATE_KEY,
                                     SUM(F.OUTSTANDING_CARDS_COUNT) MAX_OUTSTANDING_CARD_COUNT
                                FROM
                                     EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT PARTITION ('
                     || V_PART ||') F
                              WHERE
                                    F.DATE_KEY BETWEEN '||V_FIRST_DATE_KEY||' and '
                     || rec.date_key || '
                             GROUP BY F.ACCOUNT_HIST_KEY,
                                      F.DATE_KEY
                           )
                       GROUP BY
                             ACCOUNT_HIST_KEY
                       )  A
      ON
        ( FDAS.ACCOUNT_KEY = A.ACCOUNT_HIST_KEY)
      WHEN MATCHED
      THEN
         UPDATE SET
              FDAS.MAX_OUTSTANDING_CARDS_COUNT = A.MAX_OUTSTANDING_CARD_COUNT
       WHERE
           FDAS.DATE_KEY = '|| REC.DATE_KEY
        ;



      DBMS_OUTPUT.PUT_LINE ( 'V_SQL ' || V_SQL ) ;

       EXECUTE IMMEDIATE V_SQL ;


      COMMIT ;

     END LOOP;

END F_DAILY_ACCOUNT_AGGREGATES;

/