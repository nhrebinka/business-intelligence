--
-- Account Max Outstanding Card Count fix2 - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht288233_max_card_count2.log;

alter session enable parallel dml;

drop procedure f_monthly_account_aggregates_f;
drop procedure f_monthly_account_aggregates_t;

@f_monthly_account_agg_h_dfix.prc

update edw_owner.xo_workspace set ws_class = 'COOL' where ws_name = 'X_D_SITE_A06';

begin
  f_monthly_account_agg_h_dfix('2014-05');
  f_monthly_account_agg_h_dfix('2014-04');
  f_monthly_account_agg_h_dfix('2014-03');
  f_monthly_account_agg_h_dfix('2014-02');
  f_monthly_account_agg_h_dfix('2014-01');

  f_monthly_account_agg_h_dfix('2013-12');
  f_monthly_account_agg_h_dfix('2013-11');
  f_monthly_account_agg_h_dfix('2013-10');
  f_monthly_account_agg_h_dfix('2013-09');
  f_monthly_account_agg_h_dfix('2013-08');
  f_monthly_account_agg_h_dfix('2013-07');
  f_monthly_account_agg_h_dfix('2013-06');
  f_monthly_account_agg_h_dfix('2013-05');
  f_monthly_account_agg_h_dfix('2013-04');
  f_monthly_account_agg_h_dfix('2013-03');
  f_monthly_account_agg_h_dfix('2013-02');
  f_monthly_account_agg_h_dfix('2013-01');

  f_monthly_account_agg_h_dfix('2012-12');
  f_monthly_account_agg_h_dfix('2012-11');
  f_monthly_account_agg_h_dfix('2012-10');
  f_monthly_account_agg_h_dfix('2012-09');
  f_monthly_account_agg_h_dfix('2012-08');
  f_monthly_account_agg_h_dfix('2012-07');
  f_monthly_account_agg_h_dfix('2012-06');
  f_monthly_account_agg_h_dfix('2012-05');
  f_monthly_account_agg_h_dfix('2012-04');
  f_monthly_account_agg_h_dfix('2012-03');
  f_monthly_account_agg_h_dfix('2012-02');
  f_monthly_account_agg_h_dfix('2012-01');

  f_monthly_account_agg_h_dfix('2011-12');
  f_monthly_account_agg_h_dfix('2011-11');
  f_monthly_account_agg_h_dfix('2011-10');
  f_monthly_account_agg_h_dfix('2011-09');
  f_monthly_account_agg_h_dfix('2011-08');
  f_monthly_account_agg_h_dfix('2011-07');
  f_monthly_account_agg_h_dfix('2011-06');
  f_monthly_account_agg_h_dfix('2011-05');
  f_monthly_account_agg_h_dfix('2011-04');
  f_monthly_account_agg_h_dfix('2011-03');
  f_monthly_account_agg_h_dfix('2011-02');
  f_monthly_account_agg_h_dfix('2011-01');

  f_monthly_account_agg_h_dfix('2010-12');
  f_monthly_account_agg_h_dfix('2010-11');
  f_monthly_account_agg_h_dfix('2010-10');
  f_monthly_account_agg_h_dfix('2010-09');
  f_monthly_account_agg_h_dfix('2010-08');
  f_monthly_account_agg_h_dfix('2010-07');
  f_monthly_account_agg_h_dfix('2010-06');
  f_monthly_account_agg_h_dfix('2010-05');
  f_monthly_account_agg_h_dfix('2010-04');
  f_monthly_account_agg_h_dfix('2010-03');
  f_monthly_account_agg_h_dfix('2010-02');
  f_monthly_account_agg_h_dfix('2010-01');

  f_monthly_account_agg_h_dfix('2009-12');
  f_monthly_account_agg_h_dfix('2009-11');
  f_monthly_account_agg_h_dfix('2009-10');
  f_monthly_account_agg_h_dfix('2009-09');
  f_monthly_account_agg_h_dfix('2009-08');
  f_monthly_account_agg_h_dfix('2009-07');
  f_monthly_account_agg_h_dfix('2009-06');
  f_monthly_account_agg_h_dfix('2009-05');
  f_monthly_account_agg_h_dfix('2009-04');
  f_monthly_account_agg_h_dfix('2009-03');
  f_monthly_account_agg_h_dfix('2009-02');
  f_monthly_account_agg_h_dfix('2009-01');

  f_monthly_account_agg_h_dfix('2008-12');
  f_monthly_account_agg_h_dfix('2008-11');
  f_monthly_account_agg_h_dfix('2008-10');
  f_monthly_account_agg_h_dfix('2008-09');
  f_monthly_account_agg_h_dfix('2008-08');
  f_monthly_account_agg_h_dfix('2008-07');
  f_monthly_account_agg_h_dfix('2008-06');
  f_monthly_account_agg_h_dfix('2008-05');
  f_monthly_account_agg_h_dfix('2008-04');
  f_monthly_account_agg_h_dfix('2008-03');
  f_monthly_account_agg_h_dfix('2008-02');
  f_monthly_account_agg_h_dfix('2008-01');
end;
/

SPOOL OFF

