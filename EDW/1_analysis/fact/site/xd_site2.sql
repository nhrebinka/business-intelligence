--
-- create the daily aggr. table for 
--    purchases per site/merchant/program per revenue month
-- 
-- note:
--  it takes about 10 min on PDEDW with 12 threads (default)
--
-- column name abbriviation rule used: 
--   1. amount => amt
--   2. fees   => fee
--   3. maintenance => maint
--   4. transaction => trans
--   5. interchange => intchg
--
-- todo:
--   1. use an updatable join view with parallel UPDATE
--   2. use an anti-hash join with parallel INSERT to refresh
--
DROP   TABLE x_d_site2_#{N} purge;

CREATE TABLE x_d_site2_#{N}
tablespace d_bi
pctused 0
pctfree 0
storage (
  initial 40M
  next    40M
  maxextents unlimited
)
compress for all operations
parallel (degree 64)
nologging
nomonitoring
AS
-- INSERT /*+ append */ INTO x_d_site2_yyyymm
  SELECT /*+ parallel(tx, 64) use_hash(dt, tx) */
  tx.REVENUE_DATE_KEY,
  tx.POSTING_DATE_KEY,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
  ns.NAICS_KEY,
  ns.SIC_KEY,
  tx.ROW_SOURCE_SYS_NM                   source_system_name,
  /* measures */
  CAST(COUNT(
    CASE
    WHEN (tx.GROSS_SPEND_AMOUNT<>0 AND tx.WEX_TRANSACTION_ITEM_SEQ_NBR=1)
    THEN tx.WEX_TRANSACTION_ID
    ELSE NULL
    END
  ) AS integer)                          trans_ticket_count,
  CAST(
   SUM(tx.NBR_OF_UNITS_PURCHASED_COUNT) 
   AS integer
  )                                      nbr_of_units_purchased_count,
  CAST(
   SUM(tx.TRANSACTION_LINE_ITEM_COUNT1)
   AS integer
  )                                      trans_line_item_count,
  /* gross */
  SUM(tx.NET_REVENUE_AMOUNT)             net_revenue_amt,	
  SUM(tx.GROSS_SPEND_AMOUNT)             gross_spend_amt,	
  SUM(tx.GROSS_SPEND_LOCAL_AMOUNT)       gross_spend_local_amt,	
  SUM(tx.GROSS_REVENUE_AMOUNT)           gross_revenue_amt,	
  SUM(tx.GROSS_NON_REVENUE_AMOUNT)       gross_non_revenue_amt,	
  SUM(tx.DISCOUNT_AMOUNT)                gross_discount_amt,
  SUM(tx.PURCHASE_ITEM_AMOUNT)           gross_purchase_item_amt,
  SUM(tx.PURCHASE_ITEM_LOCAL_AMOUNT)     gross_purchase_item_local_amt,
  /*
   site does not have the following

   SUM(tx.ALL_OTHER_ANCILLARY_AMOUNT)    gross_all_other_ancillary_amt,
   SUM(tx.REBATE_AMOUNT)                 gross_rebate_amt,
   SUM(tx.WAIVED_REVENUE_AMOUNT)         gross_waived_revenue_amt,	
   SUM(tx.ALL_OTHER_ACILLARY_AMOUNT)     gross_all_other_ancillary_amt,
   SUM(tx.CARD_FEES_AMOUNT)              fee_card_amt,
   SUM(tx.FINANCE_FEE_AMOUNT)            fee_finance_amt,	
   SUM(tx.LATE_FEES_AMOUNT)	             fee_late_amt,
   SUM(tx.WAIVED_LATE_FEES_AMOUNT)       fee_late_waived_amt,	
  */
  SUM(tx.CURRENCY_CONVERSION_FEE_AMOUNT) fee_currency_conversion_amt,	
  /* spend fuel */
  SUM(tx.SPEND_FUEL_ONLY_AMOUNT)         spend_fuel_only_amt,
  SUM(tx.SPEND_FUEL_DIESEL_AMOUNT)       spend_fuel_diesel_amt,
  SUM(tx.SPEND_FUEL_GAS_AMOUNT)          spend_fuel_gas_amt,
  SUM(tx.SPEND_FUEL_OTHER_AMOUNT)        spend_fuel_other_amt,
  SUM(tx.SPEND_MAINTENANCE_AMOUNT)       spend_maint_amt,
  SUM(tx.SPEND_OTHER_AMOUNT)             spend_other_amt,
  /* spend fuel local */
  SUM(tx.SPEND_FUEL_ONLY_LOCAL_AMOUNT)   spend_fuel_only_local_amt,
  SUM(tx.SPEND_FUEL_DIESEL_LOCAL_AMOUNT) spend_fuel_diesel_local_amt,
  SUM(tx.SPEND_FUEL_GAS_LOCAL_AMOUNT)    spend_fuel_gas_local_amt,
  SUM(tx.SPEND_FUEL_OTHER_LOCAL_AMOUNT)  spend_fuel_other_local_amt,
  SUM(tx.SPEND_MAINTENANCE_LOCAL_AMOUNT) spend_maint_local_amt,
  SUM(tx.SPEND_OTHER_LOCAL_AMOUNT)       spend_other_local_amt,
  /* interchange */
  SUM(tx.INTERCHANGE_TOTAL_AMOUNT)       intchg_total_amt,
  SUM(tx.FUEL_ONLY_INTERCHANGE_AMOUNT)   intchg_fuel_only_amt,	
  SUM(tx.INTERCHANGE_FUEL_DIESEL_AMT)    intchg_fuel_diesel_amt,
  SUM(tx.INTERCHANGE_FUEL_GAS_AMT)       intchg_fuel_gas_amt,
  SUM(tx.INTERCHANGE_FUEL_OTHER_AMT)     intchg_fuel_other_amt,
  SUM(tx.MAINTENANCE_ONLY_INTCHG_AMOUNT) intchg_maint_only_amt,
  SUM(tx.ALL_OTHER_INTERCHANGE_AMOUNT)   intchg_all_other_amt,
  /* interchange fee */
  SUM(tx.INTERCHANGE_RATE_ACTUAL_AMOUNT) intchg_rate_actual_amt,
  SUM(tx.INTCHG_LINE_ITEM_FLAT_FEE_AMT)  intchg_line_item_flat_fee_amt,
  SUM(tx.INTCHG_TRANS_FLAT_FEE_AMT)      intchg_trans_flat_fee_amt,
  SUM(tx.INTERCHANGE_PER_UNIT_FEE_AMT)   intchg_per_unit_fee_amt,
  /* tax */
  SUM(tx.TOTAL_TAX_AMOUNT)               tax_total_amt,
  SUM(tx.FEDERAL_TAX_AMOUNT)             tax_federal_amt,
  SUM(tx.NON_FEDERAL_TAX_AMOUNT)         tax_non_federal_amt,
  SUM(tx.STATE_TAX_AMOUNT)               tax_state_amt,
  SUM(tx.LOCAL_TAX_AMOUNT)               tax_local_amt,
  SUM(tx.TOTAL_TAX_LOCAL_AMOUNT)         tax_total_local_amt,
  /* sales tax */
  SUM(tx.STATE_SALES_TAX_AMOUNT)         tax_sales_state_amt,
  SUM(tx.COUNTY_SALES_TAX_AMOUNT)        tax_sales_county_amt,
  SUM(tx.CITY_SALES_TAX_AMOUNT)          tax_sales_city_amt,
  /* excise tax */
  SUM(tx.FEDERAL_EXCISE_TAX_AMOUNT)      tax_excise_federal_amt,
  SUM(tx.STATE_EXCISE_TAX_AMOUNT)        tax_excise_state_amt,
  SUM(tx.COUNTY_EXCISE_TAX_AMOUNT)       tax_excise_county_amt,
  SUM(tx.CITY_EXCISE_TAX_AMOUNT)         tax_excise_city_amt,
  /* special tax */
  SUM(tx.STATE_SPECIAL_TAX_AMOUNT)       tax_speical_state_amt,
  SUM(tx.COUNTY_SPECIAL_TAX_AMOUNT)      tax_special_county_amt,
  SUM(tx.CITY_SPECIAL_TAX_AMOUNT)        tax_special_city_amt,
  /* exempt */
  SUM(tx.TAX_EXEMPT_SPEND_AMOUNT)        tax_exempt_spend_amt,
  SUM(tx.TAX_EXEMPT_SPEND_LOCAL_AMOUNT)  tax_exempt_spend_local_amt,
  /* qualtity gallons */
  SUM(tx.PURCHASE_GALLONS_QTY)           purchase_gallons_qty,
  SUM(tx.FUEL_GAS_GALLONS_QTY)           fuel_gas_gallons_qty,
  SUM(tx.FUEL_DIESEL_GALLONS_QTY)        fuel_diesel_gallons_qty,
  SUM(tx.FUEL_OTHER_GALLONS_QTY)         fuel_other_gallons_qty,
  SUM(tx.PRIVATE_SITE_GALLONS_QTY)       private_site_gallons_qty,
  /* qualtity liters */
  SUM(tx.PURCHASE_LITRES_QTY)            purchase_litres_qty,
  SUM(tx.FUEL_GAS_LITRES_QTY)            fuel_gas_litres_qty,
  SUM(tx.FUEL_DIESEL_LITRES_QTY)         fuel_diesel_litres_qty,
  SUM(tx.FUEL_OTHER_LITRES_QTY)          fuel_other_litres_qty,
  SUM(tx.PRIVATE_SITE_LITRES_QTY)        private_site_litres_qty,
  /* session_id YYYYMMDD.HH24MISS.PID */
  ''' || p_ses_id || '''                 session_id,
  SUM(tx.MANUAL_REBATE_AMOUNT)           manual_rebate_amt,
  SUM(tx.TRUCK_STOP_FEE_AMOUNT)          truck_stop_fee_amt,
  ac.ACCOUNT_KEY,
  ac.ACCOUNT_HIST_KEY,
  tx.ACCOUNT_REGION_KEY,
  FROM F_TRANSACTION_LINE_ITEM PARTITION (' || X_PART || ') tx
  JOIN D_DATE    dt ON (tx.REVENUE_DATE_KEY = dt.DATE_KEY)
  JOIN D_ACCOUNT ac ON (ac.ACCOUNT_KEY = 
    CASE WHEN tx.PURCHASE_ACCOUNT_KEY = 0 
    THEN tx.BILLING_ACCOUNT_KEY ELSE tx.PURCHASE_ACCOUNT_KEY
    END
  )
  /* get naics and sic keys */
  JOIN (
    SELECT 
      ACCOUNT_HIST_KEY, 
      NAICS_KEY,
      SIC_KEY,
      ROW_LAST_MOD_DTTM,
      max(ROW_LAST_MOD_DTTM) OVER (PARTITION BY ACCOUNT_HIST_KEY) last_mod
    FROM F_ACCOUNT_NAICS_SIC_EVENT
  ) ns ON (1=1
    AND ns.ROW_LAST_MOD_DTTM = ns.last_mod
    AND ns.ACCOUNT_HIST_KEY  = ac.ACCOUNT_HIST_KEY
  )
  WHERE 1=1 /* 0: can creates table schema without data */
    AND dt.MONTH_YEAR_ABBR = '2013-01'
  GROUP BY
    tx.REVENUE_DATE_KEY,
    tx.POSTING_DATE_KEY,
    tx.PROGRAM_KEY,
    tx.POS_AND_SITE_KEY,
    ns.NAICS_KEY,
    ns.SIC_KEY,
    tx.ROW_SOURCE_SYS_NM,
    ac.ACCOUNT_KEY,
    ac.ACCOUNT_HIST_KEY,
    tx.ACCOUNT_REGION_KEY
;

INSERT INTO xo_workspace VALUES (
 'HOT', 'D_SITE_AGG', 'X_D_SITE2_#{N}', '0.0.0', 'FREE', sysdate, sysdate
);

exec dbms_stats.gather_table_stats('EDW_OWNER', 'X_D_SITE2_#{N}');
GRANT SELECT ON X_D_SITE2_#{N} TO EDW_OWNER_SELECT;
GRANT SELECT ON X_D_SITE2_#{N} TO BORPTRNR;


