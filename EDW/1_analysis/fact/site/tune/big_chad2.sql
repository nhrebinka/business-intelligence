/* Formatted on 3/7/2014 3:49:34 PM (QP5 v5.215.12089.38647) */
  SELECT D_POSTING_DATE_VW.POST_MONTH_YEAR_ABBR,
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_DIESEL_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_GAS_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_OTHER_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_DIESEL_GALLONS_QTY),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_GAS_GALLONS_QTY),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_OTHER_GALLONS_QTY),
         D_POS_AND_SITE.SITE_CITY,
         D_POS_AND_SITE.SITE_STATE_PROV_CODE
    FROM D_POS_AND_SITE
         INNER JOIN F_TRANSACTION_LINE_ITEM
            ON (F_TRANSACTION_LINE_ITEM.POS_AND_SITE_KEY =
                   D_POS_AND_SITE.POS_AND_SITE_KEY)
         INNER JOIN D_REVENUE_DATE_VW
            ON (F_TRANSACTION_LINE_ITEM.REVENUE_DATE_KEY =
                   D_REVENUE_DATE_VW.REV_DATE_KEY)
         INNER JOIN F_DATE_EVENT
            ON (F_DATE_EVENT.REVENUE_DATE_KEY = D_REVENUE_DATE_VW.REV_DATE_KEY)
         INNER JOIN D_POSTING_DATE_VW
            ON (F_DATE_EVENT.POSTING_DATE_KEY = D_POSTING_DATE_VW.POST_DATE_KEY)
         INNER JOIN D_SITE_REGION_VW
            ON (F_TRANSACTION_LINE_ITEM.SITE_REGION_KEY =
                   D_SITE_REGION_VW.SITE_REGION_KEY)
         INNER JOIN D_ACCOUNT_HIST_VW
            ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY =
                   F_TRANSACTION_LINE_ITEM.PURCHASE_ACCOUNT_KEY)
         INNER JOIN D_ACCOUNT_CURRENT_VW
            ON (D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY =
                   D_ACCOUNT_CURRENT_VW.ACCOUNT_HIST_KEY)
         INNER JOIN S_SIC_LOOKUP
            ON (S_SIC_LOOKUP.SIC_LOOKUP_KEY =
                   D_ACCOUNT_CURRENT_VW.SIC_LOOKUP_KEY)
   WHERE (    NVL (
                 CASE
                    WHEN INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ') = 0
                    THEN
                       S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION
                    ELSE
                       SUBSTR (
                          S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION,
                            INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ')
                          + 2)
                 END,
                 'N/A') IN
                 ('Unknown')
          AND D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID NOT IN ('9100000937386')
          AND D_SITE_REGION_VW.SITE_COUNTRY_NAME <> 'CANADA'
          AND D_SITE_REGION_VW.SITE_STATE_PROVINCE_CODE NOT IN ('N/', 'Un')
          AND D_POS_AND_SITE.MERCHANT_PREFIX_ID <> '02'
          AND (D_POSTING_DATE_VW.post_CALENDAR_DATE BETWEEN TRUNC (
                                                                 LAST_DAY (
                                                                    ADD_MONTHS (
                                                                       SYSDATE,
                                                                       -13))
                                                               + 1)
                                                        AND LAST_DAY (
                                                               ADD_MONTHS (
                                                                  SYSDATE,
                                                                  -1))))
GROUP BY D_POSTING_DATE_VW.POST_MONTH_YEAR_ABBR,
         D_POS_AND_SITE.SITE_CITY,
         D_POS_AND_SITE.SITE_STATE_PROV_CODE;
