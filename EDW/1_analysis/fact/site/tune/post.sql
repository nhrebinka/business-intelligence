select /*+ parallel 12 */ 
  count(*), 
  d.date_key, 
  d.month_year_abbr 
from f_monthly_revenue_snapshot f, d_date d 
where d.date_key = f.posting_date_key 
group by d.date_key, d.month_year_abbr 
order by d.month_year_abbr;
