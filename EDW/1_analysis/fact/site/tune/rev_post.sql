select /*+ fact(f) use_hash(f r) use_hash(f p) parallel 12 */ 
  count(*), 
  r.date_key, 
  p.date_key,
  r.month_year_abbr,
  p.month_year_abbr
from f_monthly_revenue_snapshot f, d_date r, d_date p
where (r.date_key = f.posting_date_key or p.date_key = f.revenue_date_key)
group by r.date_key, r.month_year_abbr, p.date_key, p.month_year_abbr 
order by r.month_year_abbr, p.month_year_abbr;
