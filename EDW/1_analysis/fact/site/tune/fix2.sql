select 
  post_month_year_abbr,
  case 
    when sum(fuel_gas_amount)=0 then 0
    else round(sum(fuel_gas_amount)/sum(fuel_gas_gallons_qty),3)
  end as gas_ppg,
  site_country_name,
  site_state_prov_code,
  site_city,
  sum(fuel_gas_amount)          gas_amt,
  sum(fuel_gas_gallons_qty)     gas_qty,
  sum(fuel_diesel_amount)       diesel_amt,
  sum(fuel_diesel_gallons_qty)  diesel_qty,
  sum(fuel_other_amount)        other_amt,
  sum(fuel_other_gallons_qty)   other_qty
from x_site_by_pgm
where 1=1
--  and rownum < 100
group by 
  post_month_year_abbr,
  site_country_name,
  site_state_prov_code,
  site_city
order by
  case 
    when sum(fuel_gas_amount)=0 then 0
    else round(sum(fuel_gas_amount)/sum(fuel_gas_gallons_qty),3)
  end desc
;

