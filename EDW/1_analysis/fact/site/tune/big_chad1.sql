/* Formatted on 3/7/2014 1:01:07 PM (QP5 v5.215.12089.38647) */
  SELECT /*+ parallel 36 */
        D_PROGRAM.PROGRAM_PACKAGE_NAME,
         D_PROGRAM.PROGRAM_NAME,
         SUM (F_TRANSACTION_LINE_ITEM.GROSS_SPEND_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.PURCHASE_GALLONS_QTY),
         COUNT (
            DISTINCT CASE
                        WHEN F_TRANSACTION_LINE_ITEM.GROSS_SPEND_AMOUNT <> 0
                        THEN
                           F_TRANSACTION_LINE_ITEM.wex_transaction_id
                        ELSE
                           NULL
                     END),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_ONLY_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_MAINTENANCE_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_OTHER_AMOUNT),
         D_POS_AND_SITE.MERCHANT_PREFIX_ID,
         D_POS_AND_SITE.SOURCE_MERCHANT_SITE_ID,
         D_POS_AND_SITE.MERCHANT_NAME,
         COUNT (
            DISTINCT CASE
                        WHEN     F_TRANSACTION_LINE_ITEM.GROSS_SPEND_AMOUNT <>
                                    0
                             AND F_TRANSACTION_LINE_ITEM.SPEND_FUEL_ONLY_AMOUNT <>
                                    0
                        THEN
                           F_TRANSACTION_LINE_ITEM.wex_transaction_id
                        ELSE
                           NULL
                     END),
         COUNT (
            DISTINCT CASE
                        WHEN     F_TRANSACTION_LINE_ITEM.GROSS_SPEND_AMOUNT <>
                                    0
                             AND (   F_TRANSACTION_LINE_ITEM.SPEND_MAINTENANCE_AMOUNT <>
                                        0
                                  OR F_TRANSACTION_LINE_ITEM.SPEND_OTHER_AMOUNT <>
                                        0)
                        THEN
                           F_TRANSACTION_LINE_ITEM.wex_transaction_id
                        ELSE
                           NULL
                     END),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_DIESEL_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_GAS_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.SPEND_FUEL_OTHER_AMOUNT),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_DIESEL_GALLONS_QTY),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_GAS_GALLONS_QTY),
         SUM (F_TRANSACTION_LINE_ITEM.FUEL_OTHER_GALLONS_QTY),
         D_POS_AND_SITE.LEGACY_WEX_SITE_ID,
         D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR
    FROM D_POS_AND_SITE
         INNER JOIN F_TRANSACTION_LINE_ITEM
            ON (F_TRANSACTION_LINE_ITEM.POS_AND_SITE_KEY =
                   D_POS_AND_SITE.POS_AND_SITE_KEY)
         INNER JOIN D_REVENUE_DATE_VW
            ON (F_TRANSACTION_LINE_ITEM.REVENUE_DATE_KEY =
                   D_REVENUE_DATE_VW.REV_DATE_KEY)
         INNER JOIN D_PROGRAM
            ON (F_TRANSACTION_LINE_ITEM.PROGRAM_KEY = D_PROGRAM.PROGRAM_KEY)
   WHERE (   (    D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR IN
                     ('2013-06',
                      '2013-10',
                      '2013-03',
                      '2013-11',
                      '2013-12',
                      '2013-08',
                      '2013-05',
                      '2013-07',
                      '2013-09',
                      '2014-01',
                      '2014-02',
                      '2013-04')
              AND D_PROGRAM.PROGRAM_PACKAGE_NAME IN ('Marathon Universal D4')
              AND (   D_POS_AND_SITE.MERCHANT_PREFIX_ID IN ('93')
                   OR D_POS_AND_SITE.SOURCE_MERCHANT_SITE_ID IN ('NONE')))
          OR (    D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR IN
                     ('2013-06',
                      '2013-10',
                      '2013-03',
                      '2013-11',
                      '2013-12',
                      '2013-08',
                      '2013-05',
                      '2013-07',
                      '2013-09',
                      '2014-01',
                      '2014-02',
                      '2013-04')
              AND D_PROGRAM.PROGRAM_PACKAGE_NAME IN ('Marathon Universal D4')
              AND (    D_POS_AND_SITE.MERCHANT_PREFIX_ID NOT IN ('93')
                   AND D_POS_AND_SITE.SOURCE_MERCHANT_SITE_ID NOT IN ('NONE')))
          OR (    D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR IN
                     ('2013-06',
                      '2013-10',
                      '2013-03',
                      '2013-11',
                      '2013-12',
                      '2013-08',
                      '2013-05',
                      '2013-07',
                      '2013-09',
                      '2014-01',
                      '2014-02',
                      '2013-04')
              AND D_PROGRAM.PROGRAM_PACKAGE_NAME IN
                     ('Marathon Fleet D4', 'Marathon Private Label Funded'))
          OR (    D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR IN
                     ('2013-06',
                      '2013-10',
                      '2013-03',
                      '2013-11',
                      '2013-12',
                      '2013-08',
                      '2013-05',
                      '2013-07',
                      '2013-09',
                      '2014-01',
                      '2014-02',
                      '2013-04')
              AND D_POS_AND_SITE.MERCHANT_PREFIX_ID IN ('93')
              AND D_PROGRAM.PROGRAM_PACKAGE_NAME NOT IN
                     ('Marathon Fleet D4', 'Marathon Private Label Funded')
              AND D_PROGRAM.PROGRAM_PACKAGE_NAME NOT IN
                     ('Marathon Universal D4')))
GROUP BY D_PROGRAM.PROGRAM_PACKAGE_NAME,
         D_PROGRAM.PROGRAM_NAME,
         D_POS_AND_SITE.MERCHANT_PREFIX_ID,
         D_POS_AND_SITE.SOURCE_MERCHANT_SITE_ID,
         D_POS_AND_SITE.MERCHANT_NAME,
         D_POS_AND_SITE.LEGACY_WEX_SITE_ID,
         D_REVENUE_DATE_VW.REV_MONTH_YEAR_ABBR
