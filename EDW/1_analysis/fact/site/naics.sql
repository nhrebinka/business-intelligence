create table /*+ append */ x_site_test
as
SELECT /*+ no_index(x) px_join_filter(x, d) hash(ac) parallel(x, 64) */
  x.REVENUE_DATE_KEY,  x.PROGRAM_KEY, 
  ac.NAICS_KEY, ac.SIC_KEY,
  SUM(x.GROSS_SPEND_AMOUNT) total
FROM F_TRANSACTION_LINE_ITEM x
JOIN D_DATE dt ON (x.REVENUE_DATE_KEY = dt.DATE_KEY)
JOIN (
  SELECT 
  account_key,
  naics_key,
  sic_key
  FROM D_ACCOUNT ac
  JOIN (
    SELECT
    ACCOUNT_HIST_KEY,
    NAICS_KEY,
    SIC_KEY,
    ROW_LAST_MOD_DTTM,
    MAX(ROW_LAST_MOD_DTTM) OVER (PARTITION BY ACCOUNT_HIST_KEY) last_mod
    FROM F_ACCOUNT_NAICS_SIC_EVENT
  ) ns ON (1=1
    AND ns.ROW_LAST_MOD_DTTM = ns.last_mod
    AND ns.ACCOUNT_HIST_KEY = ac.ACCOUNT_HIST_KEY
  )
) ac ON (x.PURCHASE_ACCOUNT_KEY = ac.ACCOUNT_KEY)
WHERE dt.calendar_date_dt BETWEEN to_date('2013-01-01', 'yyyy-mm-dd') AND to_date('2013-01-31', 'yyyy-mm-dd')
GROUP BY 
  x.REVENUE_DATE_KEY, x.PROGRAM_KEY, 
  ac.NAICS_KEY, ac.SIC_KEY
;

SELECT 
  account_key,
  naics_key,
  sic_key
FROM D_ACCOUNT ac
JOIN (
  SELECT
    ACCOUNT_HIST_KEY,
    NAICS_KEY,
    SIC_KEY,
    ROW_LAST_MOD_DTTM,
    MAX(ROW_LAST_MOD_DTTM) OVER (PARTITION BY ACCOUNT_HIST_KEY) last_mod
  FROM F_ACCOUNT_NAICS_SIC_EVENT
) ns ON (1=1
  AND ns.ROW_LAST_MOD_DTTM = ns.last_mod
  AND ns.ACCOUNT_HIST_KEY = ac.ACCOUNT_HIST_KEY
)
