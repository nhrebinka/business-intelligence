CREATE OR REPLACE PROCEDURE           F_CREDIT_APP_EVENT_LOAD
IS
/******************************************************************************
   NAME:      F_CREDIT_APPLICATION _EVENT_LOAD
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/05/2012  Venu Kancharla    1. Created this procedure.

     ---------  ----------  ---------------  ------------------------------------
  2.0       06/04/2012  Venu Kancharla      ChaNGES FOR nEW ATTRIBUTES AND RENAMES

  ----------  ----------- ------------------ ---------------------------------------
  3.0      06/17/2012  Venu Kancharla       Changes for New attributes


   NOTES:



******************************************************************************/
BEGIN

INSERT /*+ append */
      INTO  EDW_OWNER.F_CREDIT_APPLICATION_EVENT
                                         (  CREDIT_APPLICATION_KEY              ,
                                            SALES_STAGE_KEY                     ,
                                            USER_KEY                            ,
                                            PROGRAM_KEY                         ,
                                            CREDIT_APP_EVENT_DATE_KEY           ,
                                            CAMPAIGN_KEY                        ,
                                            APPLICATION_NUMBER                  ,
                                            APPLICATION_RECEIVED_DATE       ,
                                            APPLICATION_APPROVED_DATE           ,
                                            APPLICATION_DECLINED_DATE           ,
                                            APPLICATION_BANK_RECEIVED_DATE     ,
                                            --APPLICATION_SUBMITTED_DATE        ,
                                            APPLICATION_FINAL_DECISION_DT       ,
                                            APPLICATION_SIGNER                  ,
                                            ROW_CREATE_DTTM                     ,
                                            ROW_LAST_MOD_DTTM                   ,
                                            ROW_LAST_MOD_PROC_NM                ,
                                            ROW_LAST_MOD_PROC_SEQ_NBR           ,
                                            ROW_SOURCE_SYS_NM                   ,
                                            CREDIT_APPLICATION_EVENT_CNT1       ,
                                            OPPORTUNITY_KEY                     ,
                                            ACCOUNT_KEY                         ,
                                            ACCOUNT_HIST_KEY                    ,
                                            LAST_INFORMATION_SENT_DATE          ,
                                            APPLICATION_APPROVED_CNT1           ,
                                            APPLICATION_DECLINED_CNT1           ,
                                            APPLICATION_NOT_DECISIONED_CNT      ,
                                            CREDIT_LIMIT_ASSIGNED_AMT           ,
                                            CREDIT_LIMIT_REQUESTED_AMT          ,
                                            APPLICATION_REMAIN_DECLINED_DT      ,
                                            CREDIT_COMMENTS                     ,
                                            APPLICATION_BANK_RECEIVED_CNT1      ,
                                            SECONDARY_SALES_REP_KEY,
                                            APPLICATION_IN_PROGRESS_CNT1
                                           )
                                   (SELECT /*+ parallel (FCE, 4 ) */
                                            CREDIT_APPLICATION_KEY              ,
                                            SALES_STAGE_KEY                     ,
                                            USER_KEY                            ,
                                            PROGRAM_KEY                         ,
                                            CREDIT_APP_EVENT_DATE_KEY           ,
                                            CAMPAIGN_KEY                        ,
                                            APPLICATION_NUMBER                  ,
                                            APPLICATION_RECEIVED_DATE       ,
                                            APPLICATION_APPROVED_DATE           ,
                                            APPLICATION_DECLINED_DATE           ,
                                            APPLICATION_BANK_RECEIVED_DATE      ,
                                            --APPLICATION_SUBMITTED_DATE        ,
                                            APPLICATION_FINAL_DECISION_DT       ,
                                            APPLICATION_SIGNER                  ,
                                            ROW_CREATE_DTTM                     ,
                                            ROW_LAST_MOD_DTTM                   ,
                                            ROW_LAST_MOD_PROC_NM                ,
                                            ROW_LAST_MOD_PROC_SEQ_NBR           ,
                                            ROW_SOURCE_SYS_NM                   ,
                                            CREDIT_APPLICATION_EVENT_CNT1       ,
                                            OPPORTUNITY_KEY                     ,
                                            ACCOUNT_KEY                         ,
                                            ACCOUNT_HIST_KEY                    ,
                                            LAST_INFORMATION_SENT_DATE          ,
                                            APPLICATION_APPROVED_CNT1           ,
                                            APPLICATION_DECLINED_CNT1           ,
                                            APPLICATION_NOT_DECISIONED_CNT      ,
                                            CREDIT_LIMIT_ASSIGNED_AMT           ,
                                            CREDIT_LIMIT_REQUESTED_AMT          ,
                                            APPLICATION_REMAIN_DECLINED_DT      ,
                                            CREDIT_COMMENTS                     ,
                                            APPLICATION_BANK_RECEIVED_CNT1      ,
                                            SECONDARY_SALES_REP_KEY,
                                            APPLICATION_IN_PROGRESS_CNT1
                                      FROM
                                           EDW_STAGE_OWNER.F_CREDIT_APPLICATION_EVENT FCE );

COMMIT;

END F_CREDIT_APP_EVENT_LOAD ;

/