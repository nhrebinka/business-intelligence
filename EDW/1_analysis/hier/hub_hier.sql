select owner, table_name, column_name
from dba_tab_columns
where owner in ('STAGE_OWNER', 'EDM_OWNER')
and column_name like '%\_PAR\_%' escape '\'
and table_name not like 'BIN%'
order by 1,2,3
;
