col INST_NAME format a30
COL SID FORMAT A10
select sys_context('USERENV','INSTANCE_NAME') INST_NAME, SYS_CONTEXT('USERENV','SID') SID FROM DUAL;

alter session set statistics_level='ALL';
set termout off


-- put your query between this and the closing comment
SELECT /*+ no_parallel(d) no_index(x) */
  x.REVENUE_DATE_KEY,  x.PROGRAM_KEY, 
  SUM(x.GROSS_SPEND_AMOUNT) total
FROM F_TRANSACTION_LINE_ITEM x
JOIN D_DATE d ON (x.REVENUE_DATE_KEY = d.DATE_KEY)
WHERE d.calendar_date_dt BETWEEN to_date('2013-01-01', 'yyyy-mm-dd') AND to_date('2013-12-31', 'yyyy-mm-dd')
GROUP BY x.REVENUE_DATE_KEY, x.PROGRAM_KEY
;


-- put your query between this and the previous comment
set termout on;
spool edw_query-01.log append
select * from table(dbms_xplan.display_cursor(null,null,'allstats last'));
spool off
