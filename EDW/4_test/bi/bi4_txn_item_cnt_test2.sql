with 
ss as (
  SELECT 
    a.account_hist_key, a.account_key, count(*)
  FROM D_DATE d
  JOIN F_MONTHLY_REVENUE_SNAPSHOT    x ON (d.DATE_KEY = x.REVENUE_DATE_KEY)
  JOIN D_ACCOUNT                     a ON (x.ACCOUNT_KEY = a.ACCOUNT_KEY)
  JOIN F_BUSINESS_SEGMENT_ACCT_EVENT e ON (a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY)
  JOIN D_BUSINESS_SEGMENT            s ON (e.BUSINESS_SEGMENT_KEY = s.BUSINESS_SEGMENT_KEY)
  WHERE d.CALENDAR_DATE_DT = TO_DATE ('06/30/2013', 'MM/DD/YYYY')
  GROUP BY a.account_hist_key, a.account_key
),
ss1 as (
  SELECT 
    x.TRANSACTION_LINE_ITEM_COUNT1,
    s.BUSINESS_SEGMENT_ID,
    RANK () OVER (
      PARTITION BY e.ACCOUNT_HIST_KEY
      ORDER BY e.ROW_LAST_MOD_DTTM DESC
    ) RN
  FROM 
    F_MONTHLY_REVENUE_SNAPSHOT x,
    D_DATE d,
    D_ACCOUNT a,
    D_BUSINESS_SEGMENT s,
    F_BUSINESS_SEGMENT_ACCT_EVENT e
  WHERE
    x.ACCOUNT_KEY = a.ACCOUNT_KEY
    AND a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY
    AND e.BUSINESS_SEGMENT_KEY = s.BUSINESS_SEGMENT_KEY
    AND d.DATE_KEY = x.REVENUE_DATE_KEY
    AND d.CALENDAR_DATE_DT =  TO_DATE ('06/30/2013', 'MM/DD/YYYY')
),
bo as (
  SELECT
    a.account_hist_key, a.account_key, count(*)
  FROM D_REVENUE_DATE_VW d
  JOIN F_MONTHLY_REVENUE_SNAPSHOT    x ON (d.REV_DATE_KEY=x.REVENUE_DATE_KEY)
  JOIN D_ACCOUNT_HIST_VW             a ON (x.ACCOUNT_KEY=a.ACCOUNT_KEY)
  JOIN ( 
    select account_hist_key, business_segment_key from (
      select 
        account_hist_key,
        business_segment_key, 
        row_last_mod_dttm,
        row_number() over (partition by account_hist_key order by row_last_mod_dttm desc) rn
      from f_business_segment_acct_event
    )
    where rn=1)                      e ON (e.ACCOUNT_HIST_KEY=a.ACCOUNT_HIST_KEY)
  JOIN D_BUSINESS_SEGMENT            s ON (e.BUSINESS_SEGMENT_KEY=s.BUSINESS_SEGMENT_KEY)
  WHERE d.REV_MONTH_YEAR_ABBR  IN ('2013-06')
  GROUP BY a.account_hist_key, a.account_key
),
ssa as (
  SELECT 
    a.account_hist_key, a.account_key, e.business_segment_key,
    row_number() over (partition by a.account_hist_key, a.account_key order by e.business_segment_key) sn
  FROM D_ACCOUNT a
  JOIN F_BUSINESS_SEGMENT_ACCT_EVENT e ON (a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY)
),
ssa1 as (
  select account_hist_key, account_key, business_segment_key from (
    SELECT 
      a.account_hist_key, a.account_key, e.BUSINESS_SEGMENT_key,
      RANK () OVER (
        PARTITION BY e.ACCOUNT_HIST_KEY ORDER BY e.ROW_LAST_MOD_DTTM DESC
      ) RN
    FROM D_ACCOUNT a, F_BUSINESS_SEGMENT_ACCT_EVENT e
    WHERE a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY
  )
  where rn=1
),
boa as (
  SELECT 
    a.account_hist_key, a.account_key, e.business_segment_key,
    row_number() over (partition by a.account_hist_key, a.account_key order by e.business_segment_key) sn
  FROM D_ACCOUNT_HIST_VW a
  JOIN ( 
    select account_hist_key, business_segment_key from (
      select 
        account_hist_key,
        business_segment_key, 
        row_last_mod_dttm,
        row_number() over (partition by account_hist_key order by row_last_mod_dttm desc) rn
      from f_business_segment_acct_event
    )
    where rn=1
  ) e ON (e.ACCOUNT_HIST_KEY=a.ACCOUNT_HIST_KEY)
)
select * from ssa1
minus
select * from boa
;
