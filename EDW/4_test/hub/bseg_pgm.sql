create table x_bseg_pgm
tablespace d_bi
as
select 
  count(*) cnt,
  case
    when c.bseg='wx' and count(*) > 80 then 'WEX81+'
    when c.bseg='wx' then 'WEX<80'
    else c.bseg
  end bseg,
  program_product_name,
  edm_spnr_pgm_key, government_flg, revolver_flg, program_name, spnr_nm, pgm_nm
from (
  select /*+ parallel(pd, 64) full(pd) */
  case
    when government_flg='Y'                    then 'GOVERNMENT'
    when revolver_flg='Y'                      then 'REVOLVER'
    when program_name in ('PACIFIC PRIDE')     then 'PAC PRIDE CUSTOMER NETWORK'
    when program_name in ('BRANDED UNIVERSAL') then 'BRANDED UNIVERSAL'
    when program_name in (
      'UNFUNDED PRIVATE LABEL',      -- 7
      'PRIVATE LABEL',               -- 69
      'Private Label Funded',        -- 1
      'Private Label Unfunded',      -- 2
      'D4', 'Basic', 'Enhanced'      -- 5,1,0
    )                                          then 'PRIVATE LABEL'
    when program_name in (
      'Partner Billed',              -- 7
      'PARTNER BILLED CO-BRAND'      -- 12
    )                                          then 'PARTNER BILLED COBRAND'
    when program_name in (
      'Affinity',                    -- 20
      'FLEET BILLED CO-BRAND'        -- 34
    )                                          then 'FLEET BILLED COBRAND'
    when program_name in (
      'WEX DIRECT',                  -- 17
      'Universal'                    -- 5
    )                                          then 'wx'
    else 'ALL OTHER'
  end bseg,
  program_product_name,
  edm_spnr_pgm_key, government_flg, revolver_flg, program_name, spnr_nm, pgm_nm
  from m_acct 
  join m_spnr_pgm        using (edm_spnr_pgm_key)
  join m_purch_device pd using (edm_acct_key)
  where 1=1
    and program_product_line_name in ('Fleet Card')
    and pd.pd_sts not in ('Terminated', 'Moved', 'Converted')
) c
  group by 
    bseg,
    program_product_name,
    edm_spnr_pgm_key, government_flg, revolver_flg, program_name, spnr_nm, pgm_nm
;

