#
# BI4.0 HUB - source validation
#
require 'minitest/autorun'
require 'oracle'

def self.test_order
  :alpha
end

describe 'Source/Hub Comparisons' do
  def _validate(qlist)
    rst = qlist.map do |s|
      a = []
      Oracle.do(s) {|r| a << r.join('|') }
      a.join("\n")
    end
    assert_equal rst[0], rst[1], "1vs2"
  end

  it '1A m_gl_account' do
      _validate([
"
select account, descr
from (
  select 
    account, 
    descr,
    effdt,
    row_number() over (partition by account order by effdt desc) rn
  from stage_owner.src_ps_gl_account_tbl
  where eff_status = 'A'
) b
where b.rn = 1
order by account
", "
select gl_account_id, gl_account_name
from   m_gl_account
order by gl_account_id
"
      ])
  end

  it '1B m_cost_center' do
      _validate([
"
select b.deptid, descr
from (
select 
  setid, 
  deptid,
  descr,
  effdt,
  row_number() over (partition by setid, deptid order by effdt desc) rn
from STAGE_OWNER.SRC_PS_GL_DEPT_TBL
where setid = 'WCORP'
) b
where rn=1
order by b.deptid, descr
", "
select cost_center_id, cost_center_name
from   m_cost_center
order by cost_center_id, cost_center_name
"
      ])
  end

  it '1C t_ledger' do
      _validate([
"
select 
  deptid, 
  fiscal_year, 
  accounting_period, 
  business_unit, 
  operating_unit, 
  product,
  account, 
  chartfield1
from stage_owner.src_ps_gl_ledger
where 1=1
and fiscal_year > 2012
and accounting_period between 1 and 12
order by 
  deptid, 
  fiscal_year, 
  accounting_period,
  business_unit, 
  operating_unit, 
  product,
  account,
  chartfield1
", "
select
  cost_center_id,
  fiscal_year,
  accounting_period,
  business_unit,
  operating_unit,
  product,
  gl_account_id,
  chartfield1
from t_ledger
where 1=1
and fiscal_year > 2012
and accounting_period between 1 and 12
order by 
  cost_center_id,
  fiscal_year, 
  accounting_period,
  business_unit,
  operating_unit,
  product,
  gl_account_id,
  chartfield1
"
      ])
  end
end

__END__




