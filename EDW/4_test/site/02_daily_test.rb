#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

Vec = Struct.new(:d, :k, :p)

HINT  = 'parallel 64'
TLIST = [
    Vec.new('22-feb-2013', 227, 'YM_2013_02'),
    Vec.new('22-may-2013', 571, 'YM_2013_05'),
    Vec.new('22-oct-2013', 571, 'YM_2013_10')
]

def self.test_order
  :alpha
end

describe 'Site Agg - Daily Tests' do
  def _validate(qlist)
    rst = qlist.map do |s|
      a = []
      Oracle.do(s) {|r| a << r.join('|') }
      a.join("\n")
    end
    assert_equal rst[0], rst[1], "1vs2"
    assert_equal rst[0], rst[2], "1vs3"
  end

  it '2A validate for 1 program - 1 day' do
    TLIST.each do |t|
      _validate([
"
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMT)
from edw_owner.X_D_SITE_B f, edw_owner.D_DATE dt
where f.REVENUE_date_key = dt.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
","
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMOUNT)
from edw_owner.F_TRANSACTION_LINE_ITEM PARTITION (#{t.p}) f, edw_owner.D_DATE dt
where f.REVENUE_date_key = dt.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
","
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMT)
from  f_daily_site_snapshot_vw f, edw_owner.D_DATE dt
where f.REVENUE_date_key = dt.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
"
      ])
    end
  end
end

__END__




