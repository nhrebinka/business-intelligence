#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

def self.test_order
    :alpha
end

describe 'Site Agg - TransactionLineItem count, 1 Site, 1 Day' do
    def _validate(qlist)
        rst = qlist.map do |s|
            a = []
            Oracle.do(s) {|r| a << r.join('|') }
            a.join("\n")
        end
        assert_equal rst[0], rst[1], "1vs2"
        assert_equal rst[0], rst[2], "1vs3"
    end

    it '13A TLI Count 1 Site, 1 Day' do
        _validate([
"
select
sum( AGG.TRANS_LINE_ITEM_COUNT)
from  EDW_OWNER.X_D_SITE_B    AGG,  EDW_OWNER.D_DATE dt
where AGG.REVENUE_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT= '16-nov-2013'
and pos_and_site_key=6951508
","
select
sum( f.TRANSACTION_LINE_ITEM_COUNT1)
from edw_owner.F_TRANSACTION_LINE_ITEM  PARTITION (YM_2013_11) F,  EDW_OWNER.D_DATE dt
where F.REVENUE_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT= '16-nov-2013'
and pos_and_site_key=6951508
","
select
sum( AGG.TRANS_LINE_ITEM_COUNT)
from   EDW_OWNER.F_DAILY_SITE_SNAPSHOT_VW      AGG,  EDW_OWNER.D_DATE dt
where AGG.REVENUE_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT= '16-nov-2013'
and pos_and_site_key=6951508
"
        ])
    end
end

__END__




