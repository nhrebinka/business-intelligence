#
# PurchaseDevice reactivation - single field update case
#
require 'minitest/autorun'
require 'purchase_device'

class PurchaseDeviceSpec < MiniTest::Spec
    def self.test_order
        :alpha
    end

    PROC_NM0 = 'pd_spec_before'
    PROC_NM1 = 'pd_spec_after'
    TD       = 3600*4
    KEY      = 1000
    RANGE    = 1..20
    CLIST    = %w(
        ACTIVATED_COUNT1 ACTIVATED_DATE AUDIT_TEST_COUNT1 AUDIT_TEST_KEY CARD_STATUS_CODE CARD_STATUS_DATE CARD_STATUS_DESC CURRENT_RECORD_FLG EMBOSSED_CARD_NUMBER_ID IN_TRANSITION_COUNT1 IN_TRANSITION_DATE ISSUED_DATE MAGSTRIPE_DEVICE_COUNT1 SUSPENDED_COUNT1 SUSPENDED_DATE TERMINATED_COUNT1 TERMINATED_DATE TIED_TO_AN_ASSET_FLG TIED_TO_A_PERSON_FLG TIED_TO_A_SITE_FLG VIRTUAL_DEVICE_COUNT1)

    def self.prep
        PurchaseDevice.new.wipe_subject
        
        @@t = Time.now
        @@i = 0
    end

    def self.fixture(d, i, k, t0)
        fix  = {
            AUDIT_TEST_KEY:            0,
            AUDIT_TEST_COUNT1:         1,
            ISSUED_DATE:               t0-TD*4,
            ACTIVATED_COUNT1:          1,
            ACTIVATED_DATE:            t0-TD*3,
            TERMINATED_COUNT1:         1,
            TERMINATED_DATE:           t0-TD*2,
            IN_TRANSITION_COUNT1:      1,
            IN_TRANSITION_DATE:        t0+TD*2,
            SUSPENDED_COUNT1:          1,
            SUSPENDED_DATE:            t0+TD*4,
            CURRENT_RECORD_FLG:        1,
            EMBOSSED_CARD_NUMBER_ID:   '',
            TIED_TO_AN_ASSET_FLG:      1,
            CARD_STATUS_CODE:          'ACTD',
            CARD_STATUS_DESC:          'Activated',
            CARD_STATUS_DATE:          t0-TD,
            TIED_TO_A_PERSON_FLG:      0,
            TIED_TO_A_SITE_FLG:        0,
            MAGSTRIPE_DEVICE_COUNT1:   1,
            VIRTUAL_DEVICE_COUNT1:     1,
            ROW_EFF_BEGIN_DTTM:        t0-TD*6,
            ROW_EFF_END_DTTM:          t0+TD*6,
            ROW_CREATE_DTTM:           t0,
            ROW_LAST_MOD_DTTM:         t0,
            ROW_LAST_MOD_PROC_NM:      PROC_NM0,
            ROW_LAST_MOD_PROC_SEQ_NBR: 0,
            ROW_MD5_CHECKSUM_HEX:      '0000'
        }
        fix[:AUDIT_TEST_KEY]            = k
        fix[:EMBOSSED_CARD_NUMBER_ID]   = k.to_s
        fix[:ROW_LAST_MOD_PROC_SEQ_NBR] = i

        d.add_fixture(fix)

        AuditSpec.new(d.tname, k, t0, PROC_NM1, i)
    end

    prep
    describe PurchaseDeviceSpec do
        before do
            @i  = (@@i+=1)
            @d  = PurchaseDevice.new
            @t0 = @@t
            @t1 = @@t+TD
        end

        it '1 has audit_list' do 
          @d.audit_list.join('|').must_equal CLIST.join('|')
        end

        it '2 create fixture' do
            k = KEY+@i
            s = PurchaseDeviceSpec.fixture(@d, @i, k, @t0)
            (@d.find_by_key(k) || [])[0].must_equal k
        end

        it '3 assert positive' do
            i = @i
            (CLIST - [ 'AUDIT_TEST_KEY' ]).each do |cname|
                k = KEY+i
                s = PurchaseDeviceSpec.fixture(@d, i, k, @t0)
                v = case cname
                    when /_DATE/ 
                        @t1+TD*i
                    when /_COUNT1|_FLG|_CODE/ 
                        (i+2).to_s
                    else
                        (i+TD).to_s
                    end
                a = {
                    cname.to_sym       => v,
                    AUDIT_TEST_KEY:       s.tkey,
                    ROW_LAST_MOD_PROC_NM: PROC_NM1,
                    ROW_LAST_MOD_DTTM:    @t1
                }
                @d.validate(s, a).must_equal true
                i += 1
            end
        end
    end
end


