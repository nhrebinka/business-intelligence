#
# OrgInventoryConfig class - extended from IronAudit
#
require 'iron_audit'

class OrgInventoryConfig < IronAudit
    def initialize
        super('ORG_INVENTORY_CONFIG')
    end
end

if __FILE__ == $0
    require 'minitest/autorun'

    describe OrgInventoryConfig do
        before do
            @d = OrgInventoryConfig.new
        end
        it 'has table_name' do
            @d.tname.must_equal 'ORG_INVENTORY_CONFIG'
        end
        it 'has audit_list' do 
            @d.audit_list.join('|').must_equal "INVENTORY_NAME|ORGANIZATION_KEY|PROCESSOR_PLATFORM_ID|CURRENCY_CODE|ACTIVE_FOR_ORDERING_FLAG|START_DATE|END_DATE|PURGE_DATE|MAX_DAILY_ORDER_QUANTITY|INITIAL_CREDIT_LIMIT|CURRENTLY_AVAILABLE|AVERAGE_DAILY_USE|MIN_INVENTORY_QUANTITY|MAX_INVENTORY_QUANTITY|EST_DAILY_ORDER_QTY|EST_DAILY_ORDER_UNTIL_DATE"
        end
    end

end
__END__
