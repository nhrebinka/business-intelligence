#
# AuditSpec class - for creating test spec to work with MiniTest::Spec
#
# 1. #cond   - generating SQL query for audit table entry
# 2. #assert - assertion checking expected dif spec and result set
#
# Note:
#    Audit table: Natural key = (SESSION_ID, PROC_NM, PROC_SEQ)
# 
# * embedded audit module
# * reperformance - repeating by the auditor of a computation made by the client
#
class AuditSpec
    TB_AUDIT = 'UA_DIM_AUDIT'
    FMT_DT   = '%Y-%m-%d %H:%M:%S'

    class << self   # a little metaprogramming here, Ruby rules
        def attr(k); send :attr_reader, k; end
    end
    VLIST = %w(
        dim_key table_name column_name
        before_value after_value before_at after_at updated_at
        schema_name proc_nm proc_seq_nbr session_id
    ).each do |k|
        self.attr(k) # here we create the new attribute
    end

    attr_reader :tname, :tkey, :valid
    def initialize(tname, tkey, ts, proc_nm, proc_seq)
        @tname, 
        @tkey,
        @_ts,
        @_proc_nm,
        @_proc_seq = tname, tkey, ts, proc_nm, proc_seq
    end
    def cond
       ts = @_ts.strftime(FMT_DT)
       "
select * from #{TB_AUDIT}
where 1=1
and table_name   = '#{@tname}'
and dim_key      = #{@tkey}
and proc_nm      = '#{@_proc_nm}'
and proc_seq_nbr = #{@_proc_seq}
"
    end
    #
    # assertion
    #   dif - hash of difference { f1: [ v01, v11 ], f2: [ v02, v12 ],..}
    #   rst - query result array [ v11, v12, v13, ... ]
    #
    def assert(dif, rst)
        #
        # assign result array to field_list as attributes (meta programming)
        # so we can use field name as attributes
        #
        r = rst.clone
        VLIST.each do |k|
            v = r.shift
            self.instance_variable_set("@#{k}", v)
        end
        #
        # check natural key attributes
        #
        return false unless
            _cmp(table_name,   @tname)     &&
            _cmp(dim_key,      @tkey)      &&
            _cmp(before_at,    @_ts)       &&
            _cmp(proc_nm,      @_proc_nm)  && 
            _cmp(proc_seq_nbr, @_proc_seq)

        #
        # check data attributes
        #
        d = dif[column_name] || dif[column_name.to_sym]
        return d && 
            _cmp(before_value, d[0]) &&
            _cmp(after_value,  d[1]) &&
            _cmp(after_at,     dif[:ROW_LAST_MOD_DTTM][1])

    rescue Exception=>e
        STDERR.puts e.inspect
        false
    end
    def _cmp(a, b)
        to_s = lambda {|v|
            case v
            when Time, Date
                v.strftime(FMT_DT)
            else
                v.to_s
            end
        }
        a1 = to_s.call(a)
        b1 = to_s.call(b)
#        STDERR.puts "cmp #{a.class}:#{a1} == #{b.class}:#{b1}"
        a1 == b1
    end
end

if __FILE__ == $0
    require 'minitest/autorun'

    TKEY   = 123
    TNAME  = 'ABC'
    CNAME  = 'DEF'
    SNAME  = 'MEMEME'
    P_NM   = 'AuditSpec'
    P_SQ   = 1
    S_ID   = 456
    T0     = Time.now
    describe AuditSpec do
        before do
            @as = AuditSpec.new(TNAME, TKEY, T0, P_NM, P_SQ)
        end
        it 'gen SQL condition' do
            @as.cond.must_equal "
select * from UA_DIM_AUDIT
where 1=1
and table_name   = '#{TNAME}'
and dim_key      = #{TKEY}
and proc_nm      = '#{P_NM}'
and proc_seq_nbr = #{P_SQ}
"
        end
        it 'assert' do 
            t1 = T0+86400
            rst = [
                TKEY,
                TNAME, 
                CNAME,
                T0.to_s,
                t1.to_s,
                T0,
                t1,
                t1,
                SNAME,
                P_NM,
                P_SQ,
                S_ID,
            ]
            dif = {
                CNAME => [ T0.to_s, t1.to_s ],  # the column to compare
                ROW_LAST_MOD_DTTM: [ T0, t1 ]   # must have field
            }
            @as.assert(dif, rst).must_equal true
        end
    end
end
__END__


