#
# PurchaseDevice class - extended from AUditedDim
#
require 'audited_dim'

class PurchaseDevice < AuditedDim
    def initialize
        super('D_AUDIT_TEST')
    end
end

if __FILE__ == $0
    require 'minitest/autorun'

    describe PurchaseDevice do
        before do
            @d = PurchaseDevice.new
        end    
        it 'has table_name' do
            @d.tname.must_equal 'D_AUDIT_TEST'
        end
        it 'has audit_list' do 
            @d.audit_list.join('|').must_equal "ACTIVATED_COUNT1|ACTIVATED_DATE|AUDIT_TEST_COUNT1|AUDIT_TEST_KEY|CARD_STATUS_CODE|CARD_STATUS_DATE|CARD_STATUS_DESC|CURRENT_RECORD_FLG|EMBOSSED_CARD_NUMBER_ID|IN_TRANSITION_COUNT1|IN_TRANSITION_DATE|ISSUED_DATE|MAGSTRIPE_DEVICE_COUNT1|SUSPENDED_COUNT1|SUSPENDED_DATE|TERMINATED_COUNT1|TERMINATED_DATE|TIED_TO_AN_ASSET_FLG|TIED_TO_A_PERSON_FLG|TIED_TO_A_SITE_FLG|VIRTUAL_DEVICE_COUNT1"
        end
    end

end
__END__
