#
# PREDW vs PDEDW Tally validation - dimensions
#
require 'minitest/autorun'
require 'oracle'

PDEDW = [ ENV['DB_UID'].split('/'), ENV['DB_TNS'] ].flatten
PREDW = [ ENV['LDAP'].split('/'),   'PREDW'       ].flatten

RANGE = "to_date('20080101','yyyymmdd') AND to_date('20140430','yyyymmdd')"
SQL   = "
  select /*+ parallel(x, 64) full(x) */
    to_char(row_last_mod_dttm, 'yyyy-mm'), count(*)
  from edw_owner.%TBL% x
  where row_last_mod_dttm BETWEEN #{RANGE}
  group by to_char(row_last_mod_dttm, 'yyyy-mm')
  order by 1
"

describe 'Dimension comparisions' do
  def _fetch(dim, db)
    a = []
    s = SQL.gsub!(/%TBL%/, dim)
    o = Oracle.new(db)
    o.run(s) {|r| a << r.join('|') if r }
    o.close
    a.join("\n")
  end
    
  it '1A dimension tally' do
    [
       'd_program',
       'd_account',
       'd_purchase_device'
    ].each do |dim|
      a = _fetch(dim, PREDW)
      b = _fetch(dim, PDEDW)
      assert_equal a, b
    end
 end
end

__END__




