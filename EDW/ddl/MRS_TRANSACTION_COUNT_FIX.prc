CREATE OR REPLACE PROCEDURE EDW_OWNER.MRS_TRANSACTION_COUNT_FIX (
   p_calendar_dt_start   IN DATE := TRUNC (SYSDATE),
   p_calendar_dt_end     IN DATE := TRUNC (SYSDATE))
AS
   /*
   NAME                    :-    DRS_TRANSACTION_LINE_ITEM_COUNT
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    01/27/2011
   DECRIPTION :-   PROCEURE TO change (historically) the TRANSACTION_LINE_ITEM_COUNT1 represent the number of swipe line items only.


*/


   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;
BEGIN
   v_calendar_dt_start := p_calendar_dt_start;
   v_calendar_dt_end := p_calendar_dt_end;


   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT - D.DAY_IN_MONTH_NUMBER + 1               AS FIRST_OF_MONTH,
                       D.CALENDAR_DATE_DT                                                                AS LAST_OF_MONTH,
                       D.DATE_KEY                                                                                AS DATE_KEY
            FROM D_DATE D
           WHERE   
                        D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start    AND v_calendar_dt_end
                 AND D.LAST_DAY_IN_MONTH_FLG = 'Yes'
           )
   LOOP
      MERGE INTO F_MONTHLY_REVENUE_SNAPSHOT F
           USING 
                    (SELECT --  MONTHLY_REVENUE_KEY_SEQ.NEXTVAL MONTHLY_REVENUE_SNAPSHOT_KEY,
                                 ACCOUNT_KEY,
                                 PROGRAM_KEY,
                                 POSTING_DATE_KEY,
                                 REVENUE_DATE_KEY,
                                 TRANSACTION_LINE_ITEM_COUNT1,
                                 ROW_LAST_MOD_DTTM,
                                ROW_LAST_MOD_PROC_NM
                     FROM 
                             ( SELECT DRS1.ACCOUNT_KEY                                               AS ACCOUNT_KEY,
                                             DRS1.PROGRAM_KEY                                             AS PROGRAM_KEY,
                                            (SELECT MAX (D2.DATE_KEY)
                                              FROM D_DATE D2
                                             WHERE D2.MONTH_YEAR_NAME = DRS1.P_DATE)    AS POSTING_DATE_KEY,
                                            (SELECT MAX (D2.DATE_KEY)
                                              FROM D_DATE D2
                                             WHERE D2.MONTH_YEAR_NAME = DRS1.R_DATE)   AS REVENUE_DATE_KEY,
                                            SUM (DRS1.TRANSACTION_LINE_ITEM_COUNT1)     AS TRANSACTION_LINE_ITEM_COUNT1,
                                            SYSDATE                                                               AS ROW_LAST_MOD_DTTM, 
                                            'MRS_TRANSACTION_COUNT_FIX'                           AS ROW_LAST_MOD_PROC_NM
                              FROM 
                                   (SELECT 
                                                DRS.ACCOUNT_KEY                                            AS ACCOUNT_KEY,
                                                DRS.PROGRAM_KEY                                            AS PROGRAM_KEY,
                                               (SELECT D2.MONTH_YEAR_NAME
                                                FROM D_DATE D2
                                                WHERE DRS.POSTING_DATE_KEY= D2.DATE_KEY) AS P_DATE,
                                               (SELECT D1.MONTH_YEAR_NAME 
                                                FROM D_DATE D1
                                                WHERE DRS.REVENUE_DATE_KEY=D1.DATE_KEY) AS R_DATE,
                                               DRS.TRANSACTION_LINE_ITEM_COUNT1              AS TRANSACTION_LINE_ITEM_COUNT1
                                      FROM 
                                                F_DAILY_REVENUE_SNAPSHOT DRS
                                       JOIN
                                               D_DATE D
                                         ON
                                               (  D.DATE_KEY =   DRS.REVENUE_DATE_KEY
                                                AND D.CALENDAR_DATE_DT 
                                                         BETWEEN REC.FIRST_OF_MONTH
                                                         AND REC.LAST_OF_MONTH
                                               )
                                 ) DRS1
                          GROUP BY 
                                   DRS1.ACCOUNT_KEY,
                                   DRS1.PROGRAM_KEY,
                                   DRS1.P_DATE,
                                   DRS1.R_DATE)
                      ) A
              ON 
                   (  
                        F.ACCOUNT_KEY          = A.ACCOUNT_KEY
                  AND F.REVENUE_DATE_KEY = A.REVENUE_DATE_KEY
                  AND F.POSTING_DATE_KEY = A.POSTING_DATE_KEY
                  )
      WHEN MATCHED
      THEN
              UPDATE  SET
                                    F.TRANSACTION_LINE_ITEM_COUNT1 = A.TRANSACTION_LINE_ITEM_COUNT1,
                                    F.ROW_LAST_MOD_DTTM                 = A.ROW_LAST_MOD_DTTM,
                                    F.ROW_LAST_MOD_PROC_NM           = A.ROW_LAST_MOD_PROC_NM;

      COMMIT;
   END LOOP;
END MRS_TRANSACTION_COUNT_FIX;
/