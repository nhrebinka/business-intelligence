CREATE OR REPLACE FUNCTION md5hash (v_input_string in varchar2 )  
 RETURN varchar2       
is
   v_checksum varchar2(50);
   begin
    
    v_checksum :=RAWTOHEX(UTL_RAW.cast_to_raw(DBMS_OBFUSCATION_TOOLKIT.md5 (input_string=> v_input_string )));
   RETURN  v_checksum ;
   end ;
/


grant execute on MD5HASH to DWLOADER  ; 
grant execute on MD5HASH to USER_LDAP  ; 
