/*
	Grants Select permissions 
	
	Author: Venu  Kancharla
	History: 9/24/2010
	
*/


set verify off
set serveroutput on  
set head off
set feedback off
set echo off 
set LINES 1000

spool /i1/&env/hub/logs/grant_exec.sql


select 'grant execute on ' ||uo.owner||'.'||uo.object_name || ' to ' ||  &user || ';' 
from all_objects  uo , user_users uu 
where uo.object_type in ('PROCEDURE','PACKAGE' , 'FUNCTION' )
and uo.owner = uu.username 
/


spool off 

set echo on
set feedback on
set head on

spool  /i1/&env/hub/logs/grant_exec.log ; 

start /i1/&env/hub/logs/grant_exec.sql ; 

set verify on 

spool off
