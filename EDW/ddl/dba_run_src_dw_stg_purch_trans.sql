SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000
   
spool /i1/prod/hub/data/src_dw_stg_purch_trans.dat ; 

(SELECT /*+ parallel (s, 4) */
           S.DW_CYCLE_CALENDAR_KEY
        || '~|~'
        ||  S.DW_TTA_AGRMNT_KEY
        || '~|~'
        || S.DW_RETAIL_DISC_KEY
        || '~|~'
        || S.DW_TAX_KEY
        || '~|~'
        || S.DW_NETWORK_KEY
        || '~|~'
        || S.DW_SPNR_KEY
        || '~|~'
        || S.DW_DRIVER_KEY
        || '~|~'
        || S.DW_ACCT_KEY
        || '~|~'
        || S.DW_PURCH_DEVICE_KEY
        || '~|~'
        || S.DW_SPNR_PGM_KEY
        || '~|~'
        || S.DW_PRODUCT_KEY
        || '~|~'
        || S.DW_ACCT_AGRMNT_KEY
        || '~|~'
        || S.DW_SITE_KEY
        || '~|~'
        || S.DW_POST_DT_KEY
        || '~|~'
        || S.DW_TRANS_DT_KEY
        || '~|~'
        || S.DW_POS_KEY
        || '~|~'
        || S.DW_FEE_KEY
        || '~|~'
        || S.DW_CNTRCT_PRICE_KEY
        || '~|~'
        || S.DW_MRCH_KEY
        || '~|~'
        || S.DW_MRCH_AGRMNT_KEY
        || '~|~'
        || S.TRANS_ITEM_SEQ_NBR
        || '~|~'
        || S.TRANS_BATCH_ID
        || '~|~'
        || S.ACCT_PRE_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.AUTH_APPRV_CD
        || '~|~'
        || S.TRANS_ID
        || '~|~'
        || S.SETTLE_NET_DISC_FLG
        || '~|~'
        || S.SETTLE_NET_TAX_FLG
        || '~|~'
        || S.SETTLE_NET_FEE_FLG
        || '~|~'
        || S.ACTION_CD
        || '~|~'
        || S.AUTH_MATCH_FLG
        || '~|~'
        || S.ORIG_TRANS_ID
        || '~|~'
        || TO_CHAR( S.LOCAL_TRANS_DT,  'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.NETWORK_POS_NBR
        || '~|~'
        || S.SITE_TKT_NBR
        || '~|~'
        || TO_CHAR(  S.POSTING_DT , 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.LOCAL_TRANS_TM
        || '~|~'
        || S.MICROFILM_NBR
        || '~|~'
        || S.RADIO_FREQ_ID
        || '~|~'
        || TO_CHAR( S.TP_PROCESS_DT , 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.INTERNAL_TRANS_FLG
        || '~|~'
        ||TO_CHAR( S.RECEIVE_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.CAPTURE_INPUT_MODE
        || '~|~'
        || S.PROCESSING_CD
        || '~|~'
        || S.CAPTURE_DEVICE
        || '~|~'
        || S.SERVICE_LEVEL_CD
        || '~|~'
        || S.TRAN_TYPE
        || '~|~'
        || S.ERROR_OVERRIDE_FLG
        || '~|~'
        || S.TRANS_TYPE_RSN_DESC
        || '~|~'
        || S.CURR_ODOMETER_READING
        || '~|~'
        || S.US_PRE_STLMT_PPU_AMT
        || '~|~'
        || S.ACCT_PRE_STLMT_PPU_AMT
        || '~|~'
        || S.PRICE_USED_CD
        || '~|~'
        || S.SITE_PRE_STLMT_PPU_AMT
        || '~|~'
        || S.ACCT_STLMT_PPU_AMT
        || '~|~'
        || S.NBR_OF_PROD_ITEMS
        || '~|~'
        || S.TAX_RATE
        || '~|~'
        || S.SITE_STLMT_PPU_AMT
        || '~|~'
        || S.TAX_EXEMPT_FLG
        || '~|~'
        || S.NOT_TAX_EXEMPT_RSN
        || '~|~'
        || S.DISC_FACTOR
        || '~|~'
        || S.FROM_ACCT_CRCY_RATE
        || '~|~'
        || S.FROM_TERM_CRCY_RATE
        || '~|~'
        || S.DISC_CALC_MTHD
        || '~|~'
        || S.ACCT_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.US_PRE_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.DW_TRANS_TYPE
        || '~|~'
        || S.SITE_PRE_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.SITE_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.POS_PROMPT_NM_1
        || '~|~'
        || S.POS_PROMPT_DATA_1
        || '~|~'
        || S.POS_PROMPT_NM_2
        || '~|~'
        || S.POS_PROMPT_DATA_2
        || '~|~'
        || S.POS_PROMPT_DATA_3
        || '~|~'
        || S.POS_PROMPT_NM_3
        || '~|~'
        || S.US_STLMT_ITEM_DTL_AMT
        || '~|~'
        ||TO_CHAR(  S.DW_CREATE_DT , 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.DW_TRANS_DTL_TYPE
        || '~|~'
        ||TO_CHAR( S.DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || S.DW_LAST_UPDT_SESSION_NM
        || '~|~'
        || S.DW_SOURCE_SYS
        || '~|~'
        || S.AUTHHDRID
        || '~|~'
        || S.HIST_PROCESSED_CD
        || '~|~'
        || S.DW_PROP_PROD_CD_KEY
        || '~|~'
        || S.APPLY_CCI_FLG
        || '~|~'
        || S.DW_CNTRCT_PRICE_HDR_KEY
        || '~|~'
        || S.US_SPNR_STLMT_CP_PPU_AMT
        || '~|~'
        || S.US_SITE_STLMT_ITEM_DTL_NET_AMT
        || '~|~'
        || S.US_SITE_STLMT_ITEM_DTL_PMT_AMT
        || '~|~'
        || S.US_ACCT_STLMT_ITEM_DTL_NET_AMT
        || '~|~'
        || S.US_SPNR_STLMT_ITEM_DTL_CP_AMT
        || '~|~'
        || S.US_ACCT_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.US_SITE_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.US_FUNDER_STLMT_ITEM_DTL_AMT
        || '~|~'
        || S.SETTLE_NET_CP_ADJ_FLG
        || '~|~'
        || S.US_ACCT_STLMT_PPU_AMT
        || '~|~'
        || S.US_SITE_STLMT_PPU_AMT
        || '~|~'
        || S.OUT_OF_NETW_TRANS_FLG
        || '~|~'
        || S.DW_ASSET_KEY
        || '~|~'
        || S.DW_RPT_ACCT_KEY
        || '~|~'
        || S.FW_PURCH_TRANS_KEY
        || '~|~'
        || S.DIVERTED_FLG
        || '~|~'
        || S.SITE_TRANS_GROSS_AMT
        || '~|~'
        || S.ACCT_TRANS_GROSS_AMT
        || '~|~'
        || S.US_TRANS_GROSS_AMT
        || '~|~'
        || S.SITE_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.ACCT_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.US_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.SITE_PRE_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.ACCT_PRE_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.US_PRE_STLMT_NBR_TRANS_UNTS
        || '~|~'
        || S.SITE_STLMT_UNIT_OF_MEAS
        || '~|~'
        || S.ACCT_STLMT_UNIT_OF_MEAS
        || '~|~'
        || S.US_STLMT_UNIT_OF_MEAS
        || '~|~'
        || S.SITE_STLMT_ITEM_DTL_NET_AMT
        || '~|~'
        || S.SITE_STLMT_ITEM_DTL_PMT_AMT
        || '~|~'
        || S.SITE_DISC_VOL_ACCUM_UNTS
        || '~|~'
        || S.ACCT_DISC_VOL_ACCUM_UNTS
        || '~|~'
        || S.US_DISC_VOL_ACCUM_UNTS
        || '~|~'
        || S.SITE_CURRENCY_CD
        || '~|~'
        || S.ACCT_CURRENCY_CD
        || '~|~'
        || DA.ACCT_ROW_ID
        || '~|~'
        || DD.PD_ROW_ID
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || DSP.PGM_ROW_ID
        || '~|~'
        || DP.POS_ROW_ID
        || '~|~'
        || DS.SITE_ROW_ID
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || DPD.IND_STD_PROD_ROW_ID
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || ''
        || '~|~'
        || S.MRCH_TAX_RECV_ACCT_ID
        || '~|~'
        || S.FED_CCI_TAX_RECV_ACCT_ID
        || '~|~'
        || S.STATE_CCI_TAX_RECV_ACCT_ID
   FROM FW_PURCH_TRANS PARTITION ("2011_07") s,
        DW_PURCH_DEVICE DD,
        DW_PRODUCT DPD,
        DW_POS DP,
        DW_SITE ds,
        DW_ACCT da,
        dw_spnr_pgm dsp
  WHERE     S.DW_ACCT_KEY = DA.DW_ACCT_KEY
        AND S.DW_POS_KEY = DP.DW_POS_KEY
        AND S.DW_PRODUCT_KEY = DPD.DW_PRODUCT_KEY
        AND S.DW_SITE_KEY = DS.DW_SITE_KEY
        AND S.DW_PURCH_DEVICE_KEY = DD.DW_PURCH_DEVICE_KEY
        AND S.DW_SPNR_PGM_KEY = DSP.DW_SPNR_PGM_KEY
        AND S.POSTING_DT = '26-JUL-2011');


spool off; 