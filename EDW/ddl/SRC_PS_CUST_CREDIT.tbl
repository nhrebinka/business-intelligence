
SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON
DEFINE &&env = &1

SPOOL /i1/&&env/hub/logs/SRC_PS_CUST_CRFEDIT.tbl.log ; 

DROP TABLE SRC_PS_CUST_CREDIT CASCADE CONSTRAINTS;

CREATE TABLE SRC_PS_CUST_CREDIT
(
  SETID              VARCHAR2(5 BYTE),
  CUST_ID            VARCHAR2(15 BYTE),
  EFFDT              DATE,
  EFF_STATUS         VARCHAR2(1 BYTE),
  CR_LIMIT           NUMBER(26,3),
  CR_LIMIT_RANGE     INTEGER,
  CR_LIMIT_DT        DATE,
  CR_LIMIT_CORP      NUMBER(26,3),
  CR_LIM_CORP_RANGE  INTEGER,
  CR_LIMIT_CORP_DT   DATE,
  CR_LIMIT_REV_DT    DATE,
  DISPUTE_STATUS     VARCHAR2(3 BYTE),
  DISPUTE_DT         DATE,
  DISPUTE_AMOUNT     NUMBER(26,3),
  COLLECTION_STATUS  VARCHAR2(3 BYTE),
  COLLECTION_DT      DATE,
  RISK_CODE          VARCHAR2(2 BYTE),
  CREDIT_CLASS       VARCHAR2(2 BYTE),
  CREDIT_CHECK       VARCHAR2(1 BYTE),
  MAX_ORDER_AMT      NUMBER(26,3),
  CUSTCR_PCT_OVR     INTEGER,
  CORPCR_PCT_OVR     INTEGER,
  CURRENCY_CD        VARCHAR2(3 BYTE),
  RT_TYPE            VARCHAR2(5 BYTE),
  LAST_MAINT_OPRID   VARCHAR2(30 BYTE),
  DATE_LAST_MAINT    VARCHAR2(30 BYTE),
  AGING_CATEGORY     VARCHAR2(2 BYTE),
  AGING_ID           VARCHAR2(5 BYTE),
  BACKLOG_DAYS       INTEGER,
  AR_1099C           VARCHAR2(1 BYTE),
  BANKRUPT_FLG       VARCHAR2(1 BYTE),
  MARKET_VALUE       NUMBER(26,3),
  DESCRLONG          VARCHAR2(4000 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDM_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE 
     LOAD WHEN ( SETID != 'E')
    LOGFILE        edm_log_dir:'src_ps_cust_credit.log'   
    BADFILE        edm_log_dir:'src_ps_cust_credit.bad'
    DISCARDFILE    edm_log_dir:'src_ps_cust_credit.dis'   
    FIELDS  TERMINATED BY '~|~'  
    MISSING FIELD VALUES ARE NULL 
    REJECT ROWS WITH ALL NULL FIELDS
      (
          SETID              ,
          CUST_ID            ,
          EFFDT              CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = '0' ,
          EFF_STATUS         ,
          CR_LIMIT           ,
          CR_LIMIT_RANGE     ,
          CR_LIMIT_DT        CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF CR_LIMIT_DT  = BLANKS,
          CR_LIMIT_CORP      ,
          CR_LIM_CORP_RANGE  ,
          CR_LIMIT_CORP_DT   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF CR_LIMIT_CORP_DT  = BLANKS,
          CR_LIMIT_REV_DT    CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF CR_LIMIT_REV_DT = '00/00/0000 00:00:00',
          DISPUTE_STATUS     ,
          DISPUTE_DT         CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF DISPUTE_DT = '00/00/0000 00:00:00',
          DISPUTE_AMOUNT     ,
          COLLECTION_STATUS  ,
          COLLECTION_DT      CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF COLLECTION_DT ='00/00/0000 00:00:00',
          RISK_CODE          ,
          CREDIT_CLASS       ,
          CREDIT_CHECK       ,
          MAX_ORDER_AMT      ,
          CUSTCR_PCT_OVR     ,
          CORPCR_PCT_OVR     ,
          CURRENCY_CD        ,
          RT_TYPE            ,
          LAST_MAINT_OPRID   ,
          DATE_LAST_MAINT    ,
          AGING_CATEGORY     ,
          AGING_ID           ,
          BACKLOG_DAYS       ,
          AR_1099C           ,
          BANKRUPT_FLG       ,
          MARKET_VALUE       ,
          DESCRLONG                
              )
                )
     LOCATION (EDM_DATA_DIR:'src_ps_cust_credit.dat')
  )
REJECT LIMIT 0
NOPARALLEL
NOMONITORING;



GRANT ALTER, SELECT ON SRC_PS_CUST_CREDIT TO DWLOADER;

GRANT ALTER, SELECT ON SRC_PS_CUST_CREDIT TO EDM_OWNER;

GRANT ALTER, SELECT ON SRC_PS_CUST_CREDIT TO STAGE_OWNER_SELECT;

SPOOL OFF; 
