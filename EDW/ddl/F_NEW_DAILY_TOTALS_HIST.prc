CREATE OR REPLACE PROCEDURE F_NEW_DAILY_TOTALS_HIST(
   p_calendar_dt_start   IN DATE := TRUNC (SYSDATE),
   p_calendar_dt_end     IN DATE := TRUNC (SYSDATE))
AS
   /*
   NAME                    :-     F_NEW_DAILY_TOTALS_HIST
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    02/2011
   DECRIPTION :-   PROCEURE TO  load daily REVENUE Aggregates  into F_DAILY_TOTALS_SNAPSHOT
                           
   LOG            : -   VERSION 1.0
                    The purpose of this procedure is to recreate the history of the F_DAILY_TOTALS_SNAPSHOT table
                    due to the change in grain brought about by adding the SITE_REGION_KEY to that table.  The old grain of the table was 
                    one record per day.  The new grain is one record per region per day.

/* Note :- the Start Date and END DATE input paramters always needs to be the Calendar Start date and Calendar END Date ...
even if we are trying to  calculate the Aggrgates for a Partial Month .
Note Added by Venu  Kancharla 11/03/2010

*/


   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;


BEGIN


   v_calendar_dt_start := p_calendar_dt_start;
   v_calendar_dt_end := p_calendar_dt_end;




   /* ********************************** */
   /*                                    */
   /* Populate F_DAILY_TOTALS_SNAPSHOT  */
   /*                                    */
   /* ********************************** */
   --------------------------------------------------------------------------------
   --------------------------------------------------------------------------------
   --------------------------------------------------------------------------------

--DELETE FROM  F_DAILY_TOTALS_SNAPSHOT F  where F.REVENUE_DATE_KEY
-- in
--( SELECT DATE_KEY  FROm D_DATE D  WHERE D.CALENDAR_DATE_DT between v_calendar_dt_start and  v_calendar_dt_end) ;

--COMMIT ;

   INSERT /*+append */  INTO F_DAILY_TOTALS_SNAPSHOT DTS (
                  DTS.DAILY_TOTALS_SNAPSHOT_KEY,
                  DTS.POSTING_DATE_KEY,
                  DTS.REVENUE_DATE_KEY,
                  DTS.SITE_REGION_KEY,
                  DTS.PURCHASE_ITEM_AMOUNT,
                  DTS.DISCOUNT_AMOUNT,
                  DTS.REBATE_AMOUNT,
                  DTS.INTERCHANGE_RATE_ACTUAL_AMOUNT,
                  DTS.INTCHG_TRANS_FLAT_FEE_AMT,
                  DTS.GROSS_REVENUE_AMOUNT,
                  DTS.TOTAL_TAX_AMOUNT,
                  DTS.GROSS_SPEND_AMOUNT,
                  DTS.TAX_EXEMPT_SPEND_AMOUNT,
                  DTS.PURCHASE_GALLONS_QTY,
                  DTS.PURCHASE_LITRES_QTY,
                  DTS.FEDERAL_TAX_AMOUNT,
                  DTS.STATE_TAX_AMOUNT,
                  DTS.LOCAL_TAX_AMOUNT,
                  DTS.COUNTY_SALES_TAX_AMOUNT,
                  DTS.CITY_SALES_TAX_AMOUNT,
                  DTS.COUNTY_SPECIAL_TAX_AMOUNT,
                  DTS.STATE_EXCISE_TAX_AMOUNT,
                  DTS.COUNTY_EXCISE_TAX_AMOUNT,
                  DTS.FEDERAL_EXCISE_TAX_AMOUNT,
                  DTS.STATE_SPECIAL_TAX_AMOUNT,
                  DTS.STATE_SALES_TAX_AMOUNT,
                  DTS.CITY_EXCISE_TAX_AMOUNT,
                  DTS.CITY_SPECIAL_TAX_AMOUNT,
                  DTS.INTERCHANGE_TOTAL_AMOUNT,
                  DTS.NET_REVENUE_AMOUNT,
                  DTS.TRANSACTION_LINE_ITEM_COUNT1,
                  DTS.INTCHG_LINE_ITEM_FLAT_FEE_AMT,
                  DTS.INTERCHANGE_PER_UNIT_FEE_AMT,
                  DTS.LATE_FEES_AMOUNT,
                  DTS.CARD_FEES_AMOUNT,
                  DTS.ALL_OTHER_ANCILLARY_AMOUNT,
                  DTS.WAIVED_REVENUE_AMOUNT,
                  DTS.FINANCE_FEE_AMOUNT,
                  DTS.PURCHASE_ITEM_LOCAL_AMOUNT,
                  DTS.NON_FEDERAL_TAX_AMOUNT,
                  DTS.FUEL_ONLY_INTERCHANGE_AMOUNT,
                  DTS.MAINTENANCE_ONLY_INTCHG_AMOUNT,
                  DTS.ALL_OTHER_INTERCHANGE_AMOUNT,
                  DTS.SPEND_FUEL_ONLY_AMOUNT,
                  DTS.SPEND_FUEL_GAS_AMOUNT,
                  DTS.SPEND_FUEL_DIESEL_AMOUNT,
                  DTS.SPEND_FUEL_OTHER_AMOUNT,
                  DTS.SPEND_MAINTENANCE_AMOUNT,
                  DTS.SPEND_OTHER_AMOUNT,
                  DTS.SPEND_FUEL_ONLY_LOCAL_AMOUNT,
                  DTS.SPEND_FUEL_GAS_LOCAL_AMOUNT,
                  DTS.SPEND_FUEL_DIESEL_LOCAL_AMOUNT,
                  DTS.SPEND_FUEL_OTHER_LOCAL_AMOUNT,
                  DTS.SPEND_MAINTENANCE_LOCAL_AMOUNT,
                  DTS.SPEND_OTHER_LOCAL_AMOUNT,
                  DTS.TOTAL_TAX_LOCAL_AMOUNT,
                  DTS.GROSS_SPEND_LOCAL_AMOUNT,
                  DTS.TAX_EXEMPT_SPEND_LOCAL_AMOUNT,
                  DTS.GROSS_NON_REVENUE_AMOUNT,
                  DTS.FUEL_GAS_GALLONS_QTY,
                  DTS.FUEL_GAS_LITRES_QTY,
                  DTS.FUEL_DIESEL_GALLONS_QTY,
                  DTS.FUEL_DIESEL_LITRES_QTY,
                  DTS.FUEL_OTHER_GALLONS_QTY,
                  DTS.FUEL_OTHER_LITRES_QTY,
                  DTS.INTERCHANGE_FUEL_GAS_AMT,
                  DTS.INTERCHANGE_FUEL_DIESEL_AMT,
                  DTS.INTERCHANGE_FUEL_OTHER_AMT,
                  DTS.ROW_CREATE_DTTM,
                  DTS.ROW_LAST_MOD_DTTM,
                  DTS.ROW_LAST_MOD_PROC_NM,
                  DTS.ROW_LAST_MOD_PROC_SEQ_NBR,
                  DTS.PRIVATE_SITE_GALLONS_QTY,
                  DTS.PRIVATE_SITE_LITRES_QTY,
                  DTS.WAIVED_LATE_FEES_AMOUNT)
      (SELECT DAILY_TOTALS_SNAPSHOT_KEY_SEQ.NEXTVAL DAILY_TOTALS_SNAPSHOT_KEY,
              TLI1.POSTING_DATE_KEY,
              TLI1.REVENUE_DATE_KEY,
              TLI1.SITE_REGION_KEY,
              TLI1.PURCHASE_ITEM_AMOUNT,
              TLI1.DISCOUNT_AMOUNT,
              TLI1.REBATE_AMOUNT,
              TLI1.INTERCHANGE_RATE_ACTUAL_AMOUNT,
              TLI1.INTCHG_TRANS_FLAT_FEE_AMT,
              TLI1.GROSS_REVENUE_AMOUNT,
              TLI1.TOTAL_TAX_AMOUNT,
              TLI1.GROSS_SPEND_AMOUNT,
              TLI1.TAX_EXEMPT_SPEND_AMOUNT,
              TLI1.PURCHASE_GALLONS_QTY,
              TLI1.PURCHASE_LITRES_QTY,
              TLI1.FEDERAL_TAX_AMOUNT,
              TLI1.STATE_TAX_AMOUNT,
              TLI1.LOCAL_TAX_AMOUNT,
              TLI1.COUNTY_SALES_TAX_AMOUNT,
              TLI1.CITY_SALES_TAX_AMOUNT,
              TLI1.COUNTY_SPECIAL_TAX_AMOUNT,
              TLI1.STATE_EXCISE_TAX_AMOUNT,
              TLI1.COUNTY_EXCISE_TAX_AMOUNT,
              TLI1.FEDERAL_EXCISE_TAX_AMOUNT,
              TLI1.STATE_SPECIAL_TAX_AMOUNT,
              TLI1.STATE_SALES_TAX_AMOUNT,
              TLI1.CITY_EXCISE_TAX_AMOUNT,
              TLI1.CITY_SPECIAL_TAX_AMOUNT,
              TLI1.INTERCHANGE_TOTAL_AMOUNT,
              TLI1.NET_REVENUE_AMOUNT,
              TLI1.TRANSACTION_LINE_ITEM_COUNT1,
              TLI1.INTCHG_LINE_ITEM_FLAT_FEE_AMT,
              TLI1.INTERCHANGE_PER_UNIT_FEE_AMT,
              TLI1.LATE_FEES_AMOUNT,
              TLI1.CARD_FEES_AMOUNT,
              TLI1.ALL_OTHER_ANCILLARY_AMOUNT,
              TLI1.WAIVED_REVENUE_AMOUNT,
              TLI1.FINANCE_FEE_AMOUNT,
              TLI1.PURCHASE_ITEM_LOCAL_AMOUNT,
              TLI1.NON_FEDERAL_TAX_AMOUNT,
              TLI1.FUEL_ONLY_INTERCHANGE_AMOUNT,
              TLI1.MAINTENANCE_ONLY_INTCHG_AMOUNT,
              TLI1.ALL_OTHER_INTERCHANGE_AMOUNT,
              TLI1.SPEND_FUEL_ONLY_AMOUNT,
              TLI1.SPEND_FUEL_GAS_AMOUNT,
              TLI1.SPEND_FUEL_DIESEL_AMOUNT,
              TLI1.SPEND_FUEL_OTHER_AMOUNT,
              TLI1.SPEND_MAINTENANCE_AMOUNT,
              TLI1.SPEND_OTHER_AMOUNT,
              TLI1.SPEND_FUEL_ONLY_LOCAL_AMOUNT,
              TLI1.SPEND_FUEL_GAS_LOCAL_AMOUNT,
              TLI1.SPEND_FUEL_DIESEL_LOCAL_AMOUNT,
              TLI1.SPEND_FUEL_OTHER_LOCAL_AMOUNT,
              TLI1.SPEND_MAINTENANCE_LOCAL_AMOUNT,
              TLI1.SPEND_OTHER_LOCAL_AMOUNT,
              TLI1.TOTAL_TAX_LOCAL_AMOUNT,
              TLI1.GROSS_SPEND_LOCAL_AMOUNT,
              TLI1.TAX_EXEMPT_SPEND_LOCAL_AMOUNT,
              TLI1.GROSS_NON_REVENUE_AMOUNT,
              TLI1.FUEL_GAS_GALLONS_QTY,
              TLI1.FUEL_GAS_LITRES_QTY,
              TLI1.FUEL_DIESEL_GALLONS_QTY,
              TLI1.FUEL_DIESEL_LITRES_QTY,
              TLI1.FUEL_OTHER_GALLONS_QTY,
              TLI1.FUEL_OTHER_LITRES_QTY,
              TLI1.INTERCHANGE_FUEL_GAS_AMT,
              TLI1.INTERCHANGE_FUEL_DIESEL_AMT,
              TLI1.INTERCHANGE_FUEL_OTHER_AMT,
              TLI1.ROW_CREATE_DTTM,
              TLI1.ROW_LAST_MOD_DTTM,
              TLI1.ROW_LAST_MOD_PROC_NM,
              TLI1.ROW_LAST_MOD_PROC_SEQ_NBR,
              TLI1.PRIVATE_SITE_GALLONS_QTY,
              TLI1.PRIVATE_SITE_LITRES_QTY,
              TLI1.WAIVED_LATE_FEES_AMOUNT
         FROM ( SELECT                                 /*+ parallel (TLI,8)*/
        TLI.POSTING_DATE_KEY                                                         AS POSTING_DATE_KEY,
                        TLI.REVENUE_DATE_KEY                                                         AS REVENUE_DATE_KEY,
                        TLI.SITE_REGION_KEY                                                     AS SITE_REGION_KEY,
                        SUM (TLI.PURCHASE_ITEM_AMOUNT)                                      AS PURCHASE_ITEM_AMOUNT,
                        SUM (TLI.DISCOUNT_AMOUNT)                                               AS DISCOUNT_AMOUNT,
                        SUM (TLI.REBATE_AMOUNT)                                                   AS REBATE_AMOUNT,
                        SUM (TLI.INTERCHANGE_RATE_ACTUAL_AMOUNT)                   AS INTERCHANGE_RATE_ACTUAL_AMOUNT,
                        SUM (TLI.INTCHG_TRANS_FLAT_FEE_AMT)                             AS INTCHG_TRANS_FLAT_FEE_AMT,
                        SUM (TLI.GROSS_REVENUE_AMOUNT)                                    AS GROSS_REVENUE_AMOUNT,
                        SUM (TLI.TOTAL_TAX_AMOUNT)                                            AS TOTAL_TAX_AMOUNT,
                        SUM (TLI.GROSS_SPEND_AMOUNT)                                        AS GROSS_SPEND_AMOUNT,
                        SUM (TLI.TAX_EXEMPT_SPEND_AMOUNT)                               AS TAX_EXEMPT_SPEND_AMOUNT,
                        SUM (TLI.PURCHASE_GALLONS_QTY)                                      AS PURCHASE_GALLONS_QTY,
                        SUM (TLI.PURCHASE_LITRES_QTY)                                         AS PURCHASE_LITRES_QTY,
                        SUM (TLI.FEDERAL_TAX_AMOUNT)                                         AS FEDERAL_TAX_AMOUNT,
                        SUM (TLI.STATE_TAX_AMOUNT)                                            AS STATE_TAX_AMOUNT,
                        SUM (TLI.LOCAL_TAX_AMOUNT)                                            AS LOCAL_TAX_AMOUNT,
                        SUM (TLI.COUNTY_SALES_TAX_AMOUNT)                              AS COUNTY_SALES_TAX_AMOUNT,
                        SUM (TLI.CITY_SALES_TAX_AMOUNT)                                    AS CITY_SALES_TAX_AMOUNT,
                        SUM (TLI.COUNTY_SPECIAL_TAX_AMOUNT )                           AS COUNTY_SPECIAL_TAX_AMOUNT,
                        SUM (TLI.STATE_EXCISE_TAX_AMOUNT)                                AS STATE_EXCISE_TAX_AMOUNT,
                        SUM (TLI.COUNTY_EXCISE_TAX_AMOUNT)                              AS COUNTY_EXCISE_TAX_AMOUNT,
                        SUM (TLI.FEDERAL_EXCISE_TAX_AMOUNT)                             AS FEDERAL_EXCISE_TAX_AMOUNT,
                        SUM (TLI.STATE_SPECIAL_TAX_AMOUNT)                              AS STATE_SPECIAL_TAX_AMOUNT,
                        SUM (TLI.STATE_SALES_TAX_AMOUNT)                                 AS STATE_SALES_TAX_AMOUNT,
                        SUM (TLI.CITY_EXCISE_TAX_AMOUNT)                                   AS CITY_EXCISE_TAX_AMOUNT,
                        SUM (TLI.CITY_SPECIAL_TAX_AMOUNT)                                 AS CITY_SPECIAL_TAX_AMOUNT,
                        SUM (TLI.INTERCHANGE_TOTAL_AMOUNT)                             AS INTERCHANGE_TOTAL_AMOUNT,
                        SUM (TLI.NET_REVENUE_AMOUNT)                                        AS NET_REVENUE_AMOUNT,
                       SUM (CASE  WHEN        TLI.WEX_TRANSACTION_ITEM_SEQ_NBR = 1
                                                    and TLI.GROSS_SPEND_AMOUNT <> 0
                                          THEN
                                                TLI.TRANSACTION_LINE_ITEM_COUNT1
                                          ELSE
                                                 0
                               END  )                                                                           AS   TRANSACTION_LINE_ITEM_COUNT1   ,   -- Added Izzy and Venu on 01/27/2011
                        SUM (TLI.INTCHG_LINE_ITEM_FLAT_FEE_AMT)                        AS INTCHG_LINE_ITEM_FLAT_FEE_AMT,
                        SUM (TLI.INTERCHANGE_PER_UNIT_FEE_AMT)                        AS INTERCHANGE_PER_UNIT_FEE_AMT,
                        SUM (TLI.LATE_FEES_AMOUNT)                                             AS LATE_FEES_AMOUNT,
                        SUM (TLI.CARD_FEES_AMOUNT)                                             AS CARD_FEES_AMOUNT,
                        SUM (TLI.ALL_OTHER_ANCILLARY_AMOUNT)                           AS ALL_OTHER_ANCILLARY_AMOUNT,
                        SUM (TLI.WAIVED_REVENUE_AMOUNT)                                   AS WAIVED_REVENUE_AMOUNT,
                        SUM (TLI.FINANCE_FEE_AMOUNT)                                          AS FINANCE_FEE_AMOUNT,
                        SUM (TLI.PURCHASE_ITEM_LOCAL_AMOUNT)                          AS PURCHASE_ITEM_LOCAL_AMOUNT,
                        SUM (TLI.NON_FEDERAL_TAX_AMOUNT)                                 AS NON_FEDERAL_TAX_AMOUNT,
                        SUM (TLI.FUEL_ONLY_INTERCHANGE_AMOUNT)                       AS FUEL_ONLY_INTERCHANGE_AMOUNT,
                        SUM (TLI.MAINTENANCE_ONLY_INTCHG_AMOUNT)                   AS MAINTENANCE_ONLY_INTCHG_AMOUNT,
                        SUM (TLI.ALL_OTHER_INTERCHANGE_AMOUNT)                      AS ALL_OTHER_INTERCHANGE_AMOUNT,
                        SUM (TLI.SPEND_FUEL_ONLY_AMOUNT)                                  AS SPEND_FUEL_ONLY_AMOUNT,
                        SUM (TLI.SPEND_FUEL_GAS_AMOUNT)                                   AS SPEND_FUEL_GAS_AMOUNT,
                        SUM (TLI.SPEND_FUEL_DIESEL_AMOUNT)                               AS SPEND_FUEL_DIESEL_AMOUNT,
                        SUM (TLI.SPEND_FUEL_OTHER_AMOUNT)                               AS SPEND_FUEL_OTHER_AMOUNT,
                        SUM (TLI.SPEND_MAINTENANCE_AMOUNT)                             AS SPEND_MAINTENANCE_AMOUNT,
                        SUM (TLI.SPEND_OTHER_AMOUNT)                                        AS SPEND_OTHER_AMOUNT,
                        SUM (TLI.SPEND_FUEL_ONLY_LOCAL_AMOUNT)                       AS SPEND_FUEL_ONLY_LOCAL_AMOUNT,
                        SUM (TLI.SPEND_FUEL_GAS_LOCAL_AMOUNT)                        AS SPEND_FUEL_GAS_LOCAL_AMOUNT,
                        SUM (TLI.SPEND_FUEL_DIESEL_LOCAL_AMOUNT)                    AS SPEND_FUEL_DIESEL_LOCAL_AMOUNT,
                        SUM (TLI.SPEND_FUEL_OTHER_LOCAL_AMOUNT)                    AS SPEND_FUEL_OTHER_LOCAL_AMOUNT,
                        SUM (TLI.SPEND_MAINTENANCE_LOCAL_AMOUNT)                   AS SPEND_MAINTENANCE_LOCAL_AMOUNT,
                        SUM (TLI.SPEND_OTHER_LOCAL_AMOUNT)                             AS SPEND_OTHER_LOCAL_AMOUNT,
                        SUM (TLI.TOTAL_TAX_LOCAL_AMOUNT)                                 AS TOTAL_TAX_LOCAL_AMOUNT,
                        SUM (TLI.GROSS_SPEND_LOCAL_AMOUNT)                             AS GROSS_SPEND_LOCAL_AMOUNT,
                        SUM (TLI.TAX_EXEMPT_SPEND_LOCAL_AMOUNT)                    AS TAX_EXEMPT_SPEND_LOCAL_AMOUNT,
                        SUM (TLI.GROSS_NON_REVENUE_AMOUNT)                             AS GROSS_NON_REVENUE_AMOUNT,
                        SUM (TLI.FUEL_GAS_GALLONS_QTY)                                       AS FUEL_GAS_GALLONS_QTY,
                        SUM (TLI.FUEL_GAS_LITRES_QTY)                                          AS FUEL_GAS_LITRES_QTY,
                        SUM (TLI.FUEL_DIESEL_GALLONS_QTY)                                   AS FUEL_DIESEL_GALLONS_QTY,
                        SUM (TLI.FUEL_DIESEL_LITRES_QTY)                                      AS FUEL_DIESEL_LITRES_QTY,
                        SUM (TLI.FUEL_OTHER_GALLONS_QTY)                                   AS FUEL_OTHER_GALLONS_QTY,
                            SUM (TLI.FUEL_OTHER_LITRES_QTY)                                  AS FUEL_OTHER_LITRES_QTY,
                        SUM (TLI.INTERCHANGE_FUEL_GAS_AMT)                               AS  INTERCHANGE_FUEL_GAS_AMT,
                        SUM (TLI.INTERCHANGE_FUEL_DIESEL_AMT)                           AS INTERCHANGE_FUEL_DIESEL_AMT,
                        SUM (TLI.INTERCHANGE_FUEL_OTHER_AMT)                           AS INTERCHANGE_FUEL_OTHER_AMT,
                        SYSDATE                                                                              AS ROW_CREATE_DTTM,
                        SYSDATE                                                                              AS ROW_LAST_MOD_DTTM,
                        'F_NEW_DAILY_TOTALS_HIST'                                                    AS ROW_LAST_MOD_PROC_NM,
                        1                                                                                          AS ROW_LAST_MOD_PROC_SEQ_NBR,
                        SUM (TLI.PRIVATE_SITE_GALLONS_QTY)                                AS PRIVATE_SITE_GALLONS_QTY,
                        SUM (TLI.PRIVATE_SITE_LITRES_QTY)                                   AS PRIVATE_SITE_LITRES_QTY,
                        SUM (TLI.WAIVED_LATE_FEES_AMOUNT)                                AS WAIVED_LATE_FEES_AMOUNT
                   FROM    F_TRANSACTION_LINE_ITEM TLI

                        JOIN
                           D_DATE D
                        ON ( D.DATE_KEY = TLI.REVENUE_DATE_KEY
                          and D.CALENDAR_DATE_DT between  v_calendar_dt_start and v_calendar_dt_end )
               GROUP BY TLI.REVENUE_DATE_KEY, TLI.POSTING_DATE_KEY, TLI.SITE_REGION_KEY  ) TLI1);

               

   COMMIT;
END F_NEW_DAILY_TOTALS_HIST;
/