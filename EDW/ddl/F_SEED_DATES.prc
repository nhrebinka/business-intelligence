CREATE OR REPLACE PROCEDURE EDW_OWNER.F_SEED_DATES (P_START_DT   OUT DATE,
                                                    P_END_DT     OUT DATE)
AS
   /*
   NAME        :-     F_SEED_DATES
   AUTHOUR  :-    Venugopal KAncharla
   DATE         :-    11/11/2010
   DECRIPTION :-   PROCEURE TO Calculate Seed Data for  aggregate Data loading Based  onm the Transaction Data loaded in
                           EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM
   LOG            : -   VERSION 1.0
                     :- Version 1.1 
                              Changed from posting_date Key to Revenue_date_key  VK 

  */



   V_START_DT   DATE;
   V_END_DT     DATE;
BEGIN
   SELECT MIN (D.CALENDAR_DATE_DT), MAX (D.CALENDAR_DATE_DT)
     INTO V_START_DT, V_END_DT
     FROM EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM F, D_DATE D
    WHERE   D.DATE_KEY = F.REVENUE_DATE_KEY        -- D.DATE_KEY = F.POSTING_DATE_KEY
          AND F.ROW_SOURCE_SYS_NM IN ('TANDEM', 'TP/CP');

   p_start_dt := v_start_dt;
   p_end_dt := v_end_dt;
END F_SEED_DATES;
/