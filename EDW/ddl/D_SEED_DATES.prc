CREATE OR REPLACE PROCEDURE EDW_OWNER.D_SEED_DATES (P_START_DT   OUT DATE,
                                                    P_END_DT     OUT DATE)
AS
   /*
   NAME            :-     D_SEED_DATES
   AUTHOUR      :-     Venugopal KAncharla
   DATE            :-    03/30/2011
   DECRIPTION  :-   PROCEURE TO Calculate Seed Data for  Daily Account Aggregates  and Monthly Account Aggreagtes.
                        ( we need this seaprate Seed dates for Dimensions to  process these aggregates 7 Days a week )
   LOG              : -  VERSION 1.0
                      :

  */

   V_START_DT   DATE;
   V_END_DT     DATE;
BEGIN
   SELECT CALENDAR_DATE_DT
     INTO V_START_DT
     FROM D_DATE
    WHERE SEQUENTIAL_DAY_NUMBER IN
             (SELECT D.SEQUENTIAL_DAY_NUMBER + 1
                FROM D_DATE D
               WHERE DATE_KEY =
                        (SELECT MAX (DATE_KEY) FROM F_DAILY_ACCOUNT_SNAPSHOT));

 -- Commented by venu this Code block is not needed  ..we want daily  aggregates  processing  to calculate  for the next day only ...       

/*SELECT TRUNC (MAX (D.ROW_LAST_MOD_DTTM))
   INTO V_END_DT 
  FROM 
           D_PURCHASE_DEVICE D;
           
*/            

 v_end_dt := v_start_dt  ; 


-- Check to make  sure that the   END DATE is always  greater than or equal to Start Date ... 

   IF V_END_DT >= V_START_DT
   THEN
      p_start_dt := v_start_dt;
      p_end_dt := v_end_dt;
   END IF;

END D_SEED_DATES;
/