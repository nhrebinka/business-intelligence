CREATE OR REPLACE PROCEDURE STAGE_OWNER.SRC_TDM_PRE_TRANS_LOAD
AS

/**************************************************************************************************************
   NAME:     SRC_TDM_PRE_TRANS_LOAD
   PURPOSE:  Procedure to load daily transaction data from TANDEM SOURCE FILES TO SRC_TDM_TRANS

   REVISIONS:
   Ver         Date            Author                          Description
   ---------  ----------      ---------------------------  -----------------------------------------
   1.0       12/2/2011      Venugopal Kancharla      Created the Procedure for regular transactions
   1.1      12/26/2011     Sagar Patel                    Updated the Procudure to include ReD Prism fields and added INSERT Query for VVD Transactions.
   1.2      02/01/2012    Venugopal Kancharla      Changed the mapping on  VVD's to  point to SRC_TDM_FUL2MST.CURRENCY_CODE instead of  FULOTMP.currency code  for Currency Code 
                                                                         and logic added to move currency code from FUL2MST if the CURRENCY_CODE from TMP is null ...    
                                                                       Added the columns for the  BULK insert for VVD instead of leaving them ( Good Programing Practice , there should not be blind inserts  

***************************************************************************************************************/

s_max_row_num           NUMBER:=0;

BEGIN

 STAGE_OWNER_TRUNC ('SRC_TDM_TRANS' ) ;

INSERT /*+ append */
      INTO  SRC_TDM_TRANS (ROW_NUM,
                           CURRENCY_CODE,
                           TICKET_NUMBER,
                           GROSS_DOLLARS,
                           GALLONS_UNITS,
                           EXCHANGE_RATE,
                           WEX_FEE_AMOUNT,
                           CONTRACT_PRICE_FLAG,
                           VOLUME_DISCOUNT_AMT,
                           TRANSACTION_DATE,
                           TRANSACTION_TIME,
                           POSTING_DATE,
                           WEX_TRANSACTION_ID,
                           LEGACY_WEX_SITE_ID,
                           PRODUCT_CODE,
                           WEX_ACCT_NBR,
                           WEX_PD_NBR,
                           CLIENT_MERCHANT,
                           CLIENT_STATE,
                           TAX_TYPE_CODE_1,
                           TAX_AMT_1,
                           TAX_TYPE_CODE_2,
                           TAX_AMT_2,
                           TAX_TYPE_CODE_3,
                           TAX_AMT_3,
                           TAX_TYPE_CODE_4,
                           TAX_AMT_4,
                           TAX_TYPE_CODE_5,
                           TAX_AMT_5,
                           TAX_TYPE_CODE_6,
                           TAX_AMT_6,
                           TAX_TYPE_CODE_7,
                           TAX_AMT_7,
                           TAX_TYPE_CODE_8,
                           TAX_AMT_8,
                           TAX_TYPE_CODE_9,
                           TAX_AMT_9,
                           TAX_TYPE_CODE_10,
                           TAX_AMT_10,
                           TAX_TYPE_CODE_11,
                           TAX_AMT_11,
                           TAX_TYPE_CODE_12,
                           TAX_AMT_12,
                           TAX_TYPE_CODE_13,
                           TAX_AMT_13,
                           TAX_TYPE_CODE_14,
                           TAX_AMT_14,
                           TAX_TYPE_CODE_15,
                           TAX_AMT_15,
                           TAX_EXEMPT_INDICATOR_1,
                           TAX_EXEMPT_INDICATOR_2,
                           TAX_EXEMPT_INDICATOR_3,
                           TAX_EXEMPT_INDICATOR_4,
                           TAX_EXEMPT_INDICATOR_5,
                           TAX_EXEMPT_INDICATOR_6,
                           TAX_EXEMPT_INDICATOR_7,
                           TAX_EXEMPT_INDICATOR_8,
                           TAX_EXEMPT_INDICATOR_9,
                           TAX_EXEMPT_INDICATOR_10,
                           TAX_EXEMPT_INDICATOR_11,
                           TAX_EXEMPT_INDICATOR_12,
                           TAX_EXEMPT_INDICATOR_13,
                           TAX_EXEMPT_INDICATOR_14,
                           TAX_EXEMPT_INDICATOR_15,
                           MERCHANT_PREFIX,
                           MICROFILM_NUMBER,
                           WEX_TRANSACTION_CODE,
                           CONTROL_CODE,
                           TAX_RATE_1,
                           TAX_RATE_2,
                           TAX_RATE_3,
                           TAX_RATE_4,
                           TAX_RATE_5,
                           TAX_RATE_6,
                           TAX_RATE_7,
                           TAX_RATE_8,
                           TAX_RATE_9,
                           TAX_RATE_10,
                           TAX_RATE_11,
                           TAX_RATE_12,
                           TAX_RATE_13,
                           TAX_RATE_14,
                           TAX_RATE_15,
                           FILING_INDICATOR_1,
                           FILING_INDICATOR_2,
                           FILING_INDICATOR_3,
                           FILING_INDICATOR_4,
                           FILING_INDICATOR_5,
                           FILING_INDICATOR_6,
                           FILING_INDICATOR_7,
                           FILING_INDICATOR_8,
                           FILING_INDICATOR_9,
                           FILING_INDICATOR_10,
                           FILING_INDICATOR_11,
                           FILING_INDICATOR_12,
                           FILING_INDICATOR_13,
                           FILING_INDICATOR_14,
                           FILING_INDICATOR_15,
                           SETTLE_ENTITY,
                           FULTMP_SETTLE_GROSS_NET_FLAG,
                           FULTMP_EFPS_DISCOUNT_AMT,
                           FULTMP_FEDERAL_TAX,
                           FULTMP_STATE_TAX,
                           FULTMP_OTHER_TAX,
                           FULTMP_GALLONS_UNITS,
                           FULTMP_GROSS_DOLLARS,
                           FULTMP_VOLUME_DISCOUNT_AMT,
                           FULTMP_WEX_FEE_AMOUNT,
                           EFPS_DISCOUNT_AMT,
                           FEDERAL_TAX,
                           STATE_TAX,
                           OTHER_TAX,
                           TAX_FUEL_TYPE_CODE,
                           JURISDICTION_CODE_1,
                           JURISDICTION_CODE_2,
                           CITY,
                           FULTMP_CURRENCY_CODE,
                           FULTMP_NETWORK_IDENTIFIER,
                           NET_TRAN_TYPE,
                           BATCH_NUM,
                           AUTHORIZATION_CODE,
                           PIN_PLAIN,
                           ODOMETER,
                           VEHICLE_NUMBER,
                           CARD_SUFFIX,
                           FULTMP_EXCHANGE_RATE)
   (SELECT ROWNUM ROW_NUM,
           CURRENCY_CODE,
           TICKET_NUMBER,
           GROSS_DOLLARS,
           GALLONS_UNITS,
           EXCHANGE_RATE,
           WEX_FEE_AMOUNT,
           CONTRACT_PRICE_FLAG,
           VOLUME_DISCOUNT_AMT,
           TRANSACTION_DATE,
           TRANSACTION_TIME,
           POSTING_DATE,
           WEX_TRANSACTION_ID,
           LEGACY_WEX_SITE_ID,
           PRODUCT_CODE,
           WEX_ACCT_NBR,
           WEX_PD_NBR,
           CLIENT_MERCHANT,
           CLIENT_STATE,
           TAX_TYPE_CODE_1,
           NVL(TAX_AMT_1, 0),
           TAX_TYPE_CODE_2,
            NVL(TAX_AMT_2, 0),
           TAX_TYPE_CODE_3,
            NVL(TAX_AMT_3, 0),
           TAX_TYPE_CODE_4,
            NVL(TAX_AMT_4, 0),
           TAX_TYPE_CODE_5,
            NVL(TAX_AMT_5, 0),
           TAX_TYPE_CODE_6,
            NVL(TAX_AMT_6, 0),
           TAX_TYPE_CODE_7,
            NVL(TAX_AMT_7, 0),
           TAX_TYPE_CODE_8,
            NVL(TAX_AMT_8, 0),
           TAX_TYPE_CODE_9,
            NVL(TAX_AMT_9, 0),
           TAX_TYPE_CODE_10,
            NVL(TAX_AMT_10, 0),
           TAX_TYPE_CODE_11,
            NVL(TAX_AMT_11, 0),
           TAX_TYPE_CODE_12,
            NVL(TAX_AMT_12, 0),
           TAX_TYPE_CODE_13,
            NVL(TAX_AMT_13, 0),
           TAX_TYPE_CODE_14,
            NVL(TAX_AMT_14, 0),
           TAX_TYPE_CODE_15,
            NVL(TAX_AMT_15, 0),
           TAX_EXEMPT_INDICATOR_1,
           TAX_EXEMPT_INDICATOR_2,
           TAX_EXEMPT_INDICATOR_3,
           TAX_EXEMPT_INDICATOR_4,
           TAX_EXEMPT_INDICATOR_5,
           TAX_EXEMPT_INDICATOR_6,
           TAX_EXEMPT_INDICATOR_7,
           TAX_EXEMPT_INDICATOR_8,
           TAX_EXEMPT_INDICATOR_9,
           TAX_EXEMPT_INDICATOR_10,
           TAX_EXEMPT_INDICATOR_11,
           TAX_EXEMPT_INDICATOR_12,
           TAX_EXEMPT_INDICATOR_13,
           TAX_EXEMPT_INDICATOR_14,
           TAX_EXEMPT_INDICATOR_15,
           MERCHANT_PREFIX,
           MICROFILM_NUMBER,
           WEX_TRANSACTION_CODE,
           CONTROL_CODE,
           TAX_RATE_1,
           TAX_RATE_2,
           TAX_RATE_3,
           TAX_RATE_4,
           TAX_RATE_5,
           TAX_RATE_6,
           TAX_RATE_7,
           TAX_RATE_8,
           TAX_RATE_9,
           TAX_RATE_10,
           TAX_RATE_11,
           TAX_RATE_12,
           TAX_RATE_13,
           TAX_RATE_14,
           TAX_RATE_15,
           FILING_INDICATOR_1,
           FILING_INDICATOR_2,
           FILING_INDICATOR_3,
           FILING_INDICATOR_4,
           FILING_INDICATOR_5,
           FILING_INDICATOR_6,
           FILING_INDICATOR_7,
           FILING_INDICATOR_8,
           FILING_INDICATOR_9,
           FILING_INDICATOR_10,
           FILING_INDICATOR_11,
           FILING_INDICATOR_12,
           FILING_INDICATOR_13,
           FILING_INDICATOR_14,
           FILING_INDICATOR_15,
           SETTLE_ENTITY,
           FULTMP_SETTLE_GROSS_NET_FLAG,
           FULTMP_EFPS_DISCOUNT_AMT,
           FULTMP_FEDERAL_TAX,
           FULTMP_STATE_TAX,
           FULTMP_OTHER_TAX,
           FULTMP_GALLONS_UNITS,
           FULTMP_GROSS_DOLLARS,
           FULTMP_VOLUME_DISCOUNT_AMT,
           FULTMP_WEX_FEE_AMOUNT,
           EFPS_DISCOUNT_AMT,
           FEDERAL_TAX,
           STATE_TAX,
           OTHER_TAX,
           TAX_FUEL_TYPE_CODE,
           JURISDICTION_CODE_1,
           JURISDICTION_CODE_2,
           CITY,
           FULTMP_CURRENCY_CODE,
           FULTMP_NETWORK_IDENTIFIER,
           NET_TRAN_TYPE,
           BATCH_NUM,
           AUTHORIZATION_CODE,
           PIN_PLAIN,
           ODOMETER,
           VEHICLE_NUMBER,
           CARD_SUFFIX,
           FULTMP_EXCHANGE_RATE
      FROM (SELECT CURRENCY_CODE,
                     TICKET_NUMBER,
                     GROSS_DOLLARS,
                     GALLONS_UNITS,
                     EXCHANGE_RATE,
                     WEX_FEE_AMOUNT,
                     CONTRACT_PRICE_FLAG,
                     VOLUME_DISCOUNT_AMT,
                     TRANSACTION_DATE,
                     TRANSACTION_TIME,
                     POSTING_DATE,
                     WEX_TRANSACTION_ID,
                     LEGACY_WEX_SITE_ID,
                     PRODUCT_CODE,
                     WEX_ACCT_NBR,
                     WEX_PD_NBR,
                     CLIENT_MERCHANT,
                     CLIENT_STATE,
                     TAX_TYPE_CODE_1,
                     TAX_AMT_1,
                     TAX_TYPE_CODE_2,
                     TAX_AMT_2,
                     TAX_TYPE_CODE_3,
                     TAX_AMT_3,
                     TAX_TYPE_CODE_4,
                     TAX_AMT_4,
                     TAX_TYPE_CODE_5,
                     TAX_AMT_5,
                     TAX_TYPE_CODE_6,
                     TAX_AMT_6,
                     TAX_TYPE_CODE_7,
                     TAX_AMT_7,
                     TAX_TYPE_CODE_8,
                     TAX_AMT_8,
                     TAX_TYPE_CODE_9,
                     TAX_AMT_9,
                     TAX_TYPE_CODE_10,
                     TAX_AMT_10,
                     TAX_TYPE_CODE_11,
                     TAX_AMT_11,
                     TAX_TYPE_CODE_12,
                     TAX_AMT_12,
                     TAX_TYPE_CODE_13,
                     TAX_AMT_13,
                     TAX_TYPE_CODE_14,
                     TAX_AMT_14,
                     TAX_TYPE_CODE_15,
                     TAX_AMT_15,
                     TAX_EXEMPT_INDICATOR_1,
                     TAX_EXEMPT_INDICATOR_2,
                     TAX_EXEMPT_INDICATOR_3,
                     TAX_EXEMPT_INDICATOR_4,
                     TAX_EXEMPT_INDICATOR_5,
                     TAX_EXEMPT_INDICATOR_6,
                     TAX_EXEMPT_INDICATOR_7,
                     TAX_EXEMPT_INDICATOR_8,
                     TAX_EXEMPT_INDICATOR_9,
                     TAX_EXEMPT_INDICATOR_10,
                     TAX_EXEMPT_INDICATOR_11,
                     TAX_EXEMPT_INDICATOR_12,
                     TAX_EXEMPT_INDICATOR_13,
                     TAX_EXEMPT_INDICATOR_14,
                     TAX_EXEMPT_INDICATOR_15,
                     MERCHANT_PREFIX,
                     MICROFILM_NUMBER,
                     WEX_TRANSACTION_CODE,
                     CONTROL_CODE,
                     TAX_RATE_1,
                     TAX_RATE_2,
                     TAX_RATE_3,
                     TAX_RATE_4,
                     TAX_RATE_5,
                     TAX_RATE_6,
                     TAX_RATE_7,
                     TAX_RATE_8,
                     TAX_RATE_9,
                     TAX_RATE_10,
                     TAX_RATE_11,
                     TAX_RATE_12,
                     TAX_RATE_13,
                     TAX_RATE_14,
                     TAX_RATE_15,
                     FILING_INDICATOR_1,
                     FILING_INDICATOR_2,
                     FILING_INDICATOR_3,
                     FILING_INDICATOR_4,
                     FILING_INDICATOR_5,
                     FILING_INDICATOR_6,
                     FILING_INDICATOR_7,
                     FILING_INDICATOR_8,
                     FILING_INDICATOR_9,
                     FILING_INDICATOR_10,
                     FILING_INDICATOR_11,
                     FILING_INDICATOR_12,
                     FILING_INDICATOR_13,
                     FILING_INDICATOR_14,
                     FILING_INDICATOR_15,
                     SETTLE_ENTITY,
                     FULTMP_SETTLE_GROSS_NET_FLAG,
                     FULTMP_EFPS_DISCOUNT_AMT,
                     FULTMP_FEDERAL_TAX,
                     FULTMP_STATE_TAX,
                     FULTMP_OTHER_TAX,
                     FULTMP_GALLONS_UNITS,
                     FULTMP_GROSS_DOLLARS,
                     FULTMP_VOLUME_DISCOUNT_AMT,
                     FULTMP_WEX_FEE_AMOUNT,
                     EFPS_DISCOUNT_AMT,
                     FEDERAL_TAX,
                     STATE_TAX,
                     OTHER_TAX,
                     TAX_FUEL_TYPE_CODE,
                     JURISDICTION_CODE_1,
                     JURISDICTION_CODE_2,
                     CITY,
                     FULTMP_CURRENCY_CODE,
                     FULTMP_NETWORK_IDENTIFIER,
                     NET_TRAN_TYPE,
                     BATCH_NUM,
                     AUTHORIZATION_CODE,
                     PIN_PLAIN,
                     ODOMETER,
                     VEHICLE_NUMBER,
                     CARD_SUFFIX,
                     FULTMP_EXCHANGE_RATE
                FROM ( (SELECT SRC_TDM_FUL1MST.CURRENCY_CODE,
                               SRC_TDM_FUL1MST.TICKET_NUMBER,
                               SRC_TDM_FUL1MST.GROSS_DOLLARS,
                               SRC_TDM_FUL1MST.GALLONS_UNITS,
                               SRC_TDM_FUL1MST.EXCHANGE_RATE,
                               SRC_TDM_FUL1MST.WEX_FEE_AMOUNT,
                               SRC_TDM_FUL1MST.CONTRACT_PRICE_FLAG,
                               SRC_TDM_FUL1MST.VOLUME_DISCOUNT_AMT,
                               SRC_TDM_FUL1MST.TRANSACTION_DATE,
                               SRC_TDM_FUL1MST.TRANSACTION_TIME,
                               SRC_TDM_FUL1MST.POSTING_DATE,
                               SRC_TDM_FUL1MST.WEX_TRANSACTION_ID,
                               SRC_TDM_FUL1MST.LEGACY_WEX_SITE_ID,
                               SRC_TDM_FUL1MST.PRODUCT_CODE,
                                  SRC_TDM_FUL1MST.CLIENT_MERCHANT
                               || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                               || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                               || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                               || SRC_TDM_FUL1MST.CHECK_DIGIT
                                  WEX_ACCT_NBR,
                                  SRC_TDM_FUL1MST.CLIENT_MERCHANT
                               || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                               || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                               || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                               || SRC_TDM_FUL1MST.CHECK_DIGIT
                               || LPAD (SRC_TDM_FUL1MST.VEHICLE_NUMBER, 4, 0)
                               || SRC_TDM_FUL1MST.CARD_SUFFIX
                                  WEX_PD_NBR,
                               SRC_TDM_FUL1MST.CLIENT_MERCHANT,
                               SRC_TDM_FUL1MST.CLIENT_STATE,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_1,
                               SRC_TDM_HPD0TAX.TAX_AMT_1,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_2,
                               SRC_TDM_HPD0TAX.TAX_AMT_2,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_3,
                               SRC_TDM_HPD0TAX.TAX_AMT_3,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_4,
                               SRC_TDM_HPD0TAX.TAX_AMT_4,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_5,
                               SRC_TDM_HPD0TAX.TAX_AMT_5,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_6,
                               SRC_TDM_HPD0TAX.TAX_AMT_6,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_7,
                               SRC_TDM_HPD0TAX.TAX_AMT_7,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_8,
                               SRC_TDM_HPD0TAX.TAX_AMT_8,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_9,
                               SRC_TDM_HPD0TAX.TAX_AMT_9,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_10,
                               SRC_TDM_HPD0TAX.TAX_AMT_10,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_11,
                               SRC_TDM_HPD0TAX.TAX_AMT_11,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_12,
                               SRC_TDM_HPD0TAX.TAX_AMT_12,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_13,
                               SRC_TDM_HPD0TAX.TAX_AMT_13,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_14,
                               SRC_TDM_HPD0TAX.TAX_AMT_14,
                               SRC_TDM_HPD0TAX.TAX_TYPE_CODE_15,
                               SRC_TDM_HPD0TAX.TAX_AMT_15,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_1,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_2,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_3,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_4,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_5,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_6,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_7,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_8,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_9,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_10,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_11,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_12,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_13,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_14,
                               SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_15,
                               SRC_TDM_FUL1MST.MERCHANT_PREFIX,
                               SRC_TDM_FUL1MST.MICROFILM_NUMBER,
                               SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE,
                               SRC_TDM_FUL1MST.CONTROL_CODE,
                               SRC_TDM_HPD0TAX.TAX_RATE_1,
                               SRC_TDM_HPD0TAX.TAX_RATE_2,
                               SRC_TDM_HPD0TAX.TAX_RATE_3,
                               SRC_TDM_HPD0TAX.TAX_RATE_4,
                               SRC_TDM_HPD0TAX.TAX_RATE_5,
                               SRC_TDM_HPD0TAX.TAX_RATE_6,
                               SRC_TDM_HPD0TAX.TAX_RATE_7,
                               SRC_TDM_HPD0TAX.TAX_RATE_8,
                               SRC_TDM_HPD0TAX.TAX_RATE_9,
                               SRC_TDM_HPD0TAX.TAX_RATE_10,
                               SRC_TDM_HPD0TAX.TAX_RATE_11,
                               SRC_TDM_HPD0TAX.TAX_RATE_12,
                               SRC_TDM_HPD0TAX.TAX_RATE_13,
                               SRC_TDM_HPD0TAX.TAX_RATE_14,
                               SRC_TDM_HPD0TAX.TAX_RATE_15,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_1,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_2,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_3,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_4,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_5,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_6,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_7,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_8,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_9,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_10,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_11,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_12,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_13,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_14,
                               SRC_TDM_HPD0TAX.FILING_INDICATOR_15,
                               SRC_TDM_FUL1MST.SETTLE_ENTITY,
                               SRC_TDM_FUL0TMP.SETTLE_GROSS_NET_FLAG
                                  FULTMP_SETTLE_GROSS_NET_FLAG,
                               SRC_TDM_FUL0TMP.EFPS_DISCOUNT_AMT
                                  FULTMP_EFPS_DISCOUNT_AMT,
                               SRC_TDM_FUL0TMP.FEDERAL_TAX FULTMP_FEDERAL_TAX,
                               SRC_TDM_FUL0TMP.STATE_TAX FULTMP_STATE_TAX,
                               SRC_TDM_FUL0TMP.OTHER_TAX FULTMP_OTHER_TAX,
                               SRC_TDM_FUL0TMP.GALLONS_UNITS
                                  FULTMP_GALLONS_UNITS,
                               SRC_TDM_FUL0TMP.GROSS_DOLLARS
                                  FULTMP_GROSS_DOLLARS,
                               SRC_TDM_FUL0TMP.VOLUME_DISCOUNT_AMT
                                  FULTMP_VOLUME_DISCOUNT_AMT,
                               SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                                  FULTMP_WEX_FEE_AMOUNT,
                               SRC_TDM_FUL1MST.EFPS_DISCOUNT_AMT,
                               SRC_TDM_FUL1MST.FEDERAL_TAX,
                               SRC_TDM_FUL1MST.STATE_TAX,
                               SRC_TDM_FUL1MST.OTHER_TAX,
                               SRC_TDM_HPD0TAX.TAX_FUEL_TYPE_CODE,
                               SRC_TDM_HPD0TAX.JURISDICTION_CODE_1,
                               SRC_TDM_HPD0TAX.JURISDICTION_CODE_2,
                               SRC_TDM_HPD0TAX.CITY,
                               SRC_TDM_FUL0TMP.CURRENCY_CODE
                                  FULTMP_CURRENCY_CODE,
                               --SRC_TDM_FUL1MST.WEX_TRANSACTION_SEQ_NBR,
                               SRC_TDM_FUL0TMP.NETWORK_IDENTIFIER
                                  FULTMP_NETWORK_IDENTIFIER,
                               SRC_TDM_FUL1MST.NET_TRAN_TYPE,
                               SRC_TDM_FUL1MST.BATCH_NUM,
                               SRC_TDM_FUL1MST.AUTHORIZATION_CODE,
                               SRC_TDM_FUL1MST.PIN_PLAIN,
                               SRC_TDM_FUL1MST.ODOMETER,
                               SRC_TDM_FUL1MST.VEHICLE_NUMBER,
                               SRC_TDM_FUL1MST.CARD_SUFFIX,
                               SRC_TDM_FUL0TMP.EXCHANGE_RATE FULTMP_EXCHANGE_RATE
                          FROM SRC_TDM_FUL1MST,
                               SRC_TDM_HPD0TAX,
                               SRC_TDM_FUL0TMP
                         WHERE     TRIM (SRC_TDM_FUL1MST.WEX_TRANSACTION_ID) =
                                      TRIM (SRC_TDM_FUL0TMP.WEX_TRANSACTION_ID)
                               AND SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE =
                                      SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                               AND SRC_TDM_FUL1MST.PRODUCT_CODE =
                                      SRC_TDM_FUL0TMP.PRODUCT_CODE
                               AND SRC_TDM_FUL1MST.CONTROL_CODE =
                                      SRC_TDM_FUL0TMP.CONTROL_CODE
                               AND SRC_TDM_FUL1MST.MICROFILM_NUMBER =
                                      SRC_TDM_FUL0TMP.MICROFILM_NUMBER
                               -- AND SRC_TDM_FUL1MST.WEX_FEE_AMOUNT                    =    SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                               AND SRC_TDM_FUL1MST.PRODUCT_CODE <> 55
                               AND SRC_TDM_FUL0TMP.GROSS_DOLLARS <> 0 -- eliminate Vvariable Volume Discounts
                               --  AND  SRC_TDM_FUL0TMP.GALLONS_UNITS  <>   0 -- eliminate Vvariable Volume Discounts
                               AND SRC_TDM_FUL0TMP.CONTROL_CODE NOT IN
                                      (03, 13, 23, 33, 43, 53, 63, 73, 83, 93)
                               AND SRC_TDM_FUL1MST.WEX_TRANSACTION_ID =
                                      SRC_TDM_HPD0TAX.WEX_TRANSACTION_ID(+)
                               AND SRC_TDM_FUL1MST.POSTING_DATE =
                                      SRC_TDM_HPD0TAX.POSTING_DATE(+))
                      UNION
                      (SELECT SRC_TDM_FUL1MST.CURRENCY_CODE,
                              SRC_TDM_FUL1MST.TICKET_NUMBER,
                              SRC_TDM_FUL1MST.GROSS_DOLLARS,
                              SRC_TDM_FUL1MST.GALLONS_UNITS,
                              SRC_TDM_FUL1MST.EXCHANGE_RATE,
                              SRC_TDM_FUL1MST.WEX_FEE_AMOUNT,
                              SRC_TDM_FUL1MST.CONTRACT_PRICE_FLAG,
                              SRC_TDM_FUL1MST.VOLUME_DISCOUNT_AMT,
                              SRC_TDM_FUL1MST.TRANSACTION_DATE,
                              SRC_TDM_FUL1MST.TRANSACTION_TIME,
                              SRC_TDM_FUL1MST.POSTING_DATE,
                              SRC_TDM_FUL1MST.WEX_TRANSACTION_ID,
                              SRC_TDM_FUL1MST.LEGACY_WEX_SITE_ID,
                              SRC_TDM_FUL1MST.PRODUCT_CODE,
                                 SRC_TDM_FUL1MST.CLIENT_MERCHANT
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                              || SRC_TDM_FUL1MST.CHECK_DIGIT
                                 WEX_ACCT_NBR,
                                 SRC_TDM_FUL1MST.CLIENT_MERCHANT
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                              || SRC_TDM_FUL1MST.CHECK_DIGIT
                              || LPAD (SRC_TDM_FUL1MST.VEHICLE_NUMBER, 4, 0)
                              || SRC_TDM_FUL1MST.CARD_SUFFIX
                                 WEX_PD_NBR,
                              SRC_TDM_FUL1MST.CLIENT_MERCHANT,
                              SRC_TDM_FUL1MST.CLIENT_STATE,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_1,
                              SRC_TDM_HPD0TAX.TAX_AMT_1,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_2,
                              SRC_TDM_HPD0TAX.TAX_AMT_2,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_3,
                              SRC_TDM_HPD0TAX.TAX_AMT_3,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_4,
                              SRC_TDM_HPD0TAX.TAX_AMT_4,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_5,
                              SRC_TDM_HPD0TAX.TAX_AMT_5,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_6,
                              SRC_TDM_HPD0TAX.TAX_AMT_6,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_7,
                              SRC_TDM_HPD0TAX.TAX_AMT_7,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_8,
                              SRC_TDM_HPD0TAX.TAX_AMT_8,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_9,
                              SRC_TDM_HPD0TAX.TAX_AMT_9,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_10,
                              SRC_TDM_HPD0TAX.TAX_AMT_10,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_11,
                              SRC_TDM_HPD0TAX.TAX_AMT_11,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_12,
                              SRC_TDM_HPD0TAX.TAX_AMT_12,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_13,
                              SRC_TDM_HPD0TAX.TAX_AMT_13,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_14,
                              SRC_TDM_HPD0TAX.TAX_AMT_14,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_15,
                              SRC_TDM_HPD0TAX.TAX_AMT_15,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_1,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_2,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_3,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_4,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_5,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_6,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_7,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_8,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_9,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_10,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_11,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_12,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_13,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_14,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_15,
                              SRC_TDM_FUL1MST.MERCHANT_PREFIX,
                              SRC_TDM_FUL1MST.MICROFILM_NUMBER,
                              SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE,
                              SRC_TDM_FUL1MST.CONTROL_CODE,
                              SRC_TDM_HPD0TAX.TAX_RATE_1,
                              SRC_TDM_HPD0TAX.TAX_RATE_2,
                              SRC_TDM_HPD0TAX.TAX_RATE_3,
                              SRC_TDM_HPD0TAX.TAX_RATE_4,
                              SRC_TDM_HPD0TAX.TAX_RATE_5,
                              SRC_TDM_HPD0TAX.TAX_RATE_6,
                              SRC_TDM_HPD0TAX.TAX_RATE_7,
                              SRC_TDM_HPD0TAX.TAX_RATE_8,
                              SRC_TDM_HPD0TAX.TAX_RATE_9,
                              SRC_TDM_HPD0TAX.TAX_RATE_10,
                              SRC_TDM_HPD0TAX.TAX_RATE_11,
                              SRC_TDM_HPD0TAX.TAX_RATE_12,
                              SRC_TDM_HPD0TAX.TAX_RATE_13,
                              SRC_TDM_HPD0TAX.TAX_RATE_14,
                              SRC_TDM_HPD0TAX.TAX_RATE_15,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_1,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_2,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_3,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_4,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_5,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_6,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_7,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_8,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_9,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_10,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_11,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_12,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_13,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_14,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_15,
                              SRC_TDM_FUL1MST.SETTLE_ENTITY,
                              SRC_TDM_FUL0TMP.SETTLE_GROSS_NET_FLAG
                                 FULTMP_SETTLE_GROSS_NET_FLAG,
                              SRC_TDM_FUL0TMP.EFPS_DISCOUNT_AMT
                                 FULTMP_EFPS_DISCOUNT_AMT,
                              SRC_TDM_FUL0TMP.FEDERAL_TAX FULTMP_FEDERAL_TAX,
                              SRC_TDM_FUL0TMP.STATE_TAX FULTMP_STATE_TAX,
                              SRC_TDM_FUL0TMP.OTHER_TAX FULTMP_OTHER_TAX,
                              SRC_TDM_FUL0TMP.GALLONS_UNITS
                                 FULTMP_GALLONS_UNITS,
                              SRC_TDM_FUL0TMP.GROSS_DOLLARS
                                 FULTMP_GROSS_DOLLARS,
                              SRC_TDM_FUL0TMP.VOLUME_DISCOUNT_AMT
                                 FULTMP_VOLUME_DISCOUNT_AMT,
                              SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                                 FULTMP_WEX_FEE_AMOUNT,
                              SRC_TDM_FUL1MST.EFPS_DISCOUNT_AMT,
                              SRC_TDM_FUL1MST.FEDERAL_TAX,
                              SRC_TDM_FUL1MST.STATE_TAX,
                              SRC_TDM_FUL1MST.OTHER_TAX,
                              SRC_TDM_HPD0TAX.TAX_FUEL_TYPE_CODE,
                              SRC_TDM_HPD0TAX.JURISDICTION_CODE_1,
                              SRC_TDM_HPD0TAX.JURISDICTION_CODE_2,
                              SRC_TDM_HPD0TAX.CITY,
                              SRC_TDM_FUL0TMP.CURRENCY_CODE
                                 FULTMP_CURRENCY_CODE,
                              --SRC_TDM_FUL1MST.WEX_TRANSACTION_SEQ_NBR,
                              SRC_TDM_FUL0TMP.NETWORK_IDENTIFIER
                                 FULTMP_NETWORK_IDENTIFIER,
                              SRC_TDM_FUL1MST.NET_TRAN_TYPE,
                              SRC_TDM_FUL1MST.BATCH_NUM,
                               SRC_TDM_FUL1MST.AUTHORIZATION_CODE,
                               SRC_TDM_FUL1MST.PIN_PLAIN,
                               SRC_TDM_FUL1MST.ODOMETER,
                               SRC_TDM_FUL1MST.VEHICLE_NUMBER,
                               SRC_TDM_FUL1MST.CARD_SUFFIX,
                               SRC_TDM_FUL0TMP.EXCHANGE_RATE FULTMP_EXCHANGE_RATE
                         FROM SRC_TDM_FUL1MST, SRC_TDM_HPD0TAX, SRC_TDM_FUL0TMP
                        WHERE     SRC_TDM_FUL1MST.CLIENT_MERCHANT =
                                     SRC_TDM_FUL0TMP.CLIENT_MERCHANT
                              AND SRC_TDM_FUL1MST.CLIENT_STATE =
                                     SRC_TDM_FUL0TMP.CLIENT_STATE
                              AND SRC_TDM_FUL1MST.CLIENT_ZEROES =
                                     SRC_TDM_FUL0TMP.CLIENT_ZEROES
                              AND SRC_TDM_FUL1MST.ACCOUNT_NUMBER =
                                     SRC_TDM_FUL0TMP.ACCOUNT_NUMBER
                              AND SRC_TDM_FUL1MST.CHECK_DIGIT =
                                     SRC_TDM_FUL0TMP.CHECK_DIGIT
                              AND SRC_TDM_FUL1MST.VEHICLE_NUMBER =
                                     SRC_TDM_FUL0TMP.VEHICLE_NUMBER
                              AND SRC_TDM_FUL1MST.TRANSACTION_DATE =
                                     SRC_TDM_FUL0TMP.TRANSACTION_DATE
                              AND TRIM (SRC_TDM_FUL1MST.TRANSACTION_TIME) =
                                     TRIM (SRC_TDM_FUL0TMP.TRANSACTION_TIME)
                              AND SRC_TDM_FUL1MST.POSTING_DATE =
                                     TO_DATE (SRC_TDM_FUL0TMP.POSTING_DATE,
                                              'YYMMDD')
                              AND TRIM (SRC_TDM_FUL1MST.TICKET_NUMBER) =
                                     TRIM (SRC_TDM_FUL0TMP.TICKET_NUMBER)
                              AND SUBSTR (
                                     TRIM (SRC_TDM_FUL1MST.TRANSACTION_TIME),
                                     1,
                                     4) =
                                     SUBSTR (
                                        TRIM (SRC_TDM_FUL0TMP.TRANSACTION_TIME),
                                        1,
                                        4)
                              AND SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE =
                                     SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                              AND SRC_TDM_FUL1MST.PRODUCT_CODE =
                                     SRC_TDM_FUL0TMP.PRODUCT_CODE
                              AND SRC_TDM_FUL1MST.CONTROL_CODE =
                                     SRC_TDM_FUL0TMP.CONTROL_CODE
                              --  AND SRC_TDM_FUL1MST.MICROFILM_NUMBER                   =         SRC_TDM_FUL0TMP.MICROFILM_NUMBER
                              AND SRC_TDM_FUL0TMP.GROSS_DOLLARS <> 0 -- eliminate Vvariable Volume Discounts
                              --    AND  SRC_TDM_FUL0TMP.GALLONS_UNITS  <>   0 -- eliminate Vvariable Volume Discounts
                              AND SRC_TDM_FUL1MST.WEX_FEE_AMOUNT =
                                     SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                              AND SRC_TDM_FUL1MST.PRODUCT_CODE <> 55
                              AND SRC_TDM_FUL0TMP.CONTROL_CODE NOT IN
                                     (03, 13, 23, 33, 43, 53, 63, 73, 83, 93)
                              AND SRC_TDM_FUL1MST.WEX_TRANSACTION_ID =
                                     SRC_TDM_HPD0TAX.WEX_TRANSACTION_ID(+)
                              AND SRC_TDM_FUL1MST.POSTING_DATE =
                                     SRC_TDM_HPD0TAX.POSTING_DATE(+)
                              AND NOT EXISTS
                                         (SELECT 'x'
                                            FROM SRC_TDM_FUL1MST F1
                                           WHERE     TRIM (
                                                        F1.WEX_TRANSACTION_ID) =
                                                        TRIM (
                                                           SRC_TDM_FUL0TMP.WEX_TRANSACTION_ID)
                                                 AND F1.WEX_TRANSACTION_CODE =
                                                        SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                                                 AND F1.PRODUCT_CODE =
                                                        SRC_TDM_FUL0TMP.PRODUCT_CODE
                                                 AND F1.CONTROL_CODE =
                                                        SRC_TDM_FUL0TMP.CONTROL_CODE
                                                 AND F1.MICROFILM_NUMBER =
                                                        SRC_TDM_FUL0TMP.MICROFILM_NUMBER
                                                 AND F1.PRODUCT_CODE <> 55
                                                 AND F1.CONTROL_CODE NOT IN
                                                        (03,
                                                         13,
                                                         23,
                                                         33,
                                                         43,
                                                         53,
                                                         63,
                                                         73,
                                                         83,
                                                         93)))
                      UNION
                      (SELECT SRC_TDM_FUL1MST.CURRENCY_CODE,
                              SRC_TDM_FUL1MST.TICKET_NUMBER,
                              SRC_TDM_FUL1MST.GROSS_DOLLARS,
                              SRC_TDM_FUL1MST.GALLONS_UNITS,
                              SRC_TDM_FUL1MST.EXCHANGE_RATE,
                              SRC_TDM_FUL1MST.WEX_FEE_AMOUNT,
                              SRC_TDM_FUL1MST.CONTRACT_PRICE_FLAG,
                              SRC_TDM_FUL1MST.VOLUME_DISCOUNT_AMT,
                              SRC_TDM_FUL1MST.TRANSACTION_DATE,
                              SRC_TDM_FUL1MST.TRANSACTION_TIME,
                              SRC_TDM_FUL1MST.POSTING_DATE,
                              SRC_TDM_FUL1MST.WEX_TRANSACTION_ID,
                              SRC_TDM_FUL1MST.LEGACY_WEX_SITE_ID,
                              SRC_TDM_FUL1MST.PRODUCT_CODE,
                                 SRC_TDM_FUL1MST.CLIENT_MERCHANT
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                              || SRC_TDM_FUL1MST.CHECK_DIGIT
                                 WEX_ACCT_NBR,
                                 SRC_TDM_FUL1MST.CLIENT_MERCHANT
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_STATE, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.CLIENT_ZEROES, 2, 0)
                              || LPAD (SRC_TDM_FUL1MST.ACCOUNT_NUMBER, 6, 0)
                              || SRC_TDM_FUL1MST.CHECK_DIGIT
                              || LPAD (SRC_TDM_FUL1MST.VEHICLE_NUMBER, 4, 0)
                              || SRC_TDM_FUL1MST.CARD_SUFFIX
                                 WEX_PD_NBR,
                              SRC_TDM_FUL1MST.CLIENT_MERCHANT,
                              SRC_TDM_FUL1MST.CLIENT_STATE,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_1,
                              SRC_TDM_HPD0TAX.TAX_AMT_1,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_2,
                              SRC_TDM_HPD0TAX.TAX_AMT_2,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_3,
                              SRC_TDM_HPD0TAX.TAX_AMT_3,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_4,
                              SRC_TDM_HPD0TAX.TAX_AMT_4,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_5,
                              SRC_TDM_HPD0TAX.TAX_AMT_5,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_6,
                              SRC_TDM_HPD0TAX.TAX_AMT_6,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_7,
                              SRC_TDM_HPD0TAX.TAX_AMT_7,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_8,
                              SRC_TDM_HPD0TAX.TAX_AMT_8,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_9,
                              SRC_TDM_HPD0TAX.TAX_AMT_9,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_10,
                              SRC_TDM_HPD0TAX.TAX_AMT_10,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_11,
                              SRC_TDM_HPD0TAX.TAX_AMT_11,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_12,
                              SRC_TDM_HPD0TAX.TAX_AMT_12,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_13,
                              SRC_TDM_HPD0TAX.TAX_AMT_13,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_14,
                              SRC_TDM_HPD0TAX.TAX_AMT_14,
                              SRC_TDM_HPD0TAX.TAX_TYPE_CODE_15,
                              SRC_TDM_HPD0TAX.TAX_AMT_15,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_1,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_2,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_3,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_4,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_5,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_6,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_7,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_8,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_9,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_10,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_11,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_12,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_13,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_14,
                              SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_15,
                              SRC_TDM_FUL1MST.MERCHANT_PREFIX,
                              SRC_TDM_FUL1MST.MICROFILM_NUMBER,
                              SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE,
                              SRC_TDM_FUL1MST.CONTROL_CODE,
                              SRC_TDM_HPD0TAX.TAX_RATE_1,
                              SRC_TDM_HPD0TAX.TAX_RATE_2,
                              SRC_TDM_HPD0TAX.TAX_RATE_3,
                              SRC_TDM_HPD0TAX.TAX_RATE_4,
                              SRC_TDM_HPD0TAX.TAX_RATE_5,
                              SRC_TDM_HPD0TAX.TAX_RATE_6,
                              SRC_TDM_HPD0TAX.TAX_RATE_7,
                              SRC_TDM_HPD0TAX.TAX_RATE_8,
                              SRC_TDM_HPD0TAX.TAX_RATE_9,
                              SRC_TDM_HPD0TAX.TAX_RATE_10,
                              SRC_TDM_HPD0TAX.TAX_RATE_11,
                              SRC_TDM_HPD0TAX.TAX_RATE_12,
                              SRC_TDM_HPD0TAX.TAX_RATE_13,
                              SRC_TDM_HPD0TAX.TAX_RATE_14,
                              SRC_TDM_HPD0TAX.TAX_RATE_15,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_1,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_2,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_3,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_4,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_5,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_6,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_7,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_8,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_9,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_10,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_11,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_12,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_13,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_14,
                              SRC_TDM_HPD0TAX.FILING_INDICATOR_15,
                              SRC_TDM_FUL1MST.SETTLE_ENTITY,
                              SRC_TDM_FUL0TMP.SETTLE_GROSS_NET_FLAG
                                 FULTMP_SETTLE_GROSS_NET_FLAG,
                              SRC_TDM_FUL0TMP.EFPS_DISCOUNT_AMT
                                 FULTMP_EFPS_DISCOUNT_AMT,
                              SRC_TDM_FUL0TMP.FEDERAL_TAX FULTMP_FEDERAL_TAX,
                              SRC_TDM_FUL0TMP.STATE_TAX FULTMP_STATE_TAX,
                              SRC_TDM_FUL0TMP.OTHER_TAX FULTMP_OTHER_TAX,
                              SRC_TDM_FUL0TMP.GALLONS_UNITS
                                 FULTMP_GALLONS_UNITS,
                              SRC_TDM_FUL0TMP.GROSS_DOLLARS
                                 FULTMP_GROSS_DOLLARS,
                              SRC_TDM_FUL0TMP.VOLUME_DISCOUNT_AMT
                                 FULTMP_VOLUME_DISCOUNT_AMT,
                              SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                                 FULTMP_WEX_FEE_AMOUNT,
                              SRC_TDM_FUL1MST.EFPS_DISCOUNT_AMT,
                              SRC_TDM_FUL1MST.FEDERAL_TAX,
                              SRC_TDM_FUL1MST.STATE_TAX,
                              SRC_TDM_FUL1MST.OTHER_TAX,
                              SRC_TDM_HPD0TAX.TAX_FUEL_TYPE_CODE,
                              SRC_TDM_HPD0TAX.JURISDICTION_CODE_1,
                              SRC_TDM_HPD0TAX.JURISDICTION_CODE_2,
                              SRC_TDM_HPD0TAX.CITY,
                              SRC_TDM_FUL0TMP.CURRENCY_CODE
                                 FULTMP_CURRENCY_CODE,
                              --SRC_TDM_FUL1MST.WEX_TRANSACTION_SEQ_NBR,
                              SRC_TDM_FUL0TMP.NETWORK_IDENTIFIER
                                 FULTMP_NETWORK_IDENTIFIER,
                              SRC_TDM_FUL1MST.NET_TRAN_TYPE,
                              SRC_TDM_FUL1MST.BATCH_NUM,
                               SRC_TDM_FUL1MST.AUTHORIZATION_CODE,
                               SRC_TDM_FUL1MST.PIN_PLAIN,
                               SRC_TDM_FUL1MST.ODOMETER,
                               SRC_TDM_FUL1MST.VEHICLE_NUMBER,
                               SRC_TDM_FUL1MST.CARD_SUFFIX,
                               SRC_TDM_FUL0TMP.EXCHANGE_RATE FULTMP_EXCHANGE_RATE
                         FROM SRC_TDM_FUL1MST, SRC_TDM_HPD0TAX, SRC_TDM_FUL0TMP
                        WHERE     SRC_TDM_FUL1MST.CLIENT_MERCHANT =
                                     SRC_TDM_FUL0TMP.CLIENT_MERCHANT
                              AND SRC_TDM_FUL1MST.CLIENT_STATE =
                                     SRC_TDM_FUL0TMP.CLIENT_STATE
                              AND SRC_TDM_FUL1MST.CLIENT_ZEROES =
                                     SRC_TDM_FUL0TMP.CLIENT_ZEROES
                              AND SRC_TDM_FUL1MST.ACCOUNT_NUMBER =
                                     SRC_TDM_FUL0TMP.ACCOUNT_NUMBER
                              AND SRC_TDM_FUL1MST.CHECK_DIGIT =
                                     SRC_TDM_FUL0TMP.CHECK_DIGIT
                              AND SRC_TDM_FUL1MST.VEHICLE_NUMBER =
                                     SRC_TDM_FUL0TMP.VEHICLE_NUMBER
                              AND SRC_TDM_FUL1MST.TRANSACTION_DATE =
                                     SRC_TDM_FUL0TMP.TRANSACTION_DATE
                              AND TRIM (SRC_TDM_FUL1MST.TRANSACTION_TIME) >
                                     TRIM (SRC_TDM_FUL0TMP.TRANSACTION_TIME)
                              AND SRC_TDM_FUL1MST.POSTING_DATE =
                                     TO_DATE (SRC_TDM_FUL0TMP.POSTING_DATE,
                                              'YYMMDD')
                              AND TRIM (SRC_TDM_FUL1MST.TICKET_NUMBER) =
                                     TRIM (SRC_TDM_FUL0TMP.TICKET_NUMBER)
                              AND SUBSTR (
                                     TRIM (SRC_TDM_FUL1MST.TRANSACTION_TIME),
                                     1,
                                     4) =
                                     SUBSTR (
                                        TRIM (SRC_TDM_FUL0TMP.TRANSACTION_TIME),
                                        1,
                                        4)
                              AND SRC_TDM_FUL1MST.WEX_TRANSACTION_CODE =
                                     SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                              AND SRC_TDM_FUL1MST.PRODUCT_CODE =
                                     SRC_TDM_FUL0TMP.PRODUCT_CODE
                              AND SRC_TDM_FUL1MST.CONTROL_CODE =
                                     SRC_TDM_FUL0TMP.CONTROL_CODE
                              AND SRC_TDM_FUL1MST.PRODUCT_CODE <> 55
                              AND SRC_TDM_FUL0TMP.CONTROL_CODE NOT IN
                                     (03, 13, 23, 33, 43, 53, 63, 73, 83, 93)
                              AND SRC_TDM_FUL0TMP.GROSS_DOLLARS <> 0 -- eliminate Vvariable Volume Discounts
                              --      AND  SRC_TDM_FUL0TMP.GALLONS_UNITS  <>   0 -- eliminate Vvariable Volume Discounts
                              AND SRC_TDM_FUL1MST.WEX_TRANSACTION_ID =
                                     SRC_TDM_HPD0TAX.WEX_TRANSACTION_ID(+)
                              AND SRC_TDM_FUL1MST.POSTING_DATE =
                                     SRC_TDM_HPD0TAX.POSTING_DATE(+)
                              AND NOT EXISTS
                                         (SELECT 'x'
                                            FROM SRC_TDM_FUL1MST F
                                           WHERE     F.CLIENT_MERCHANT =
                                                        SRC_TDM_FUL0TMP.CLIENT_MERCHANT
                                                 AND F.CLIENT_STATE =
                                                        SRC_TDM_FUL0TMP.CLIENT_STATE
                                                 AND F.CLIENT_ZEROES =
                                                        SRC_TDM_FUL0TMP.CLIENT_ZEROES
                                                 AND F.ACCOUNT_NUMBER =
                                                        SRC_TDM_FUL0TMP.ACCOUNT_NUMBER
                                                 AND F.CHECK_DIGIT =
                                                        SRC_TDM_FUL0TMP.CHECK_DIGIT
                                                 AND F.VEHICLE_NUMBER =
                                                        SRC_TDM_FUL0TMP.VEHICLE_NUMBER
                                                 AND F.TRANSACTION_DATE =
                                                        SRC_TDM_FUL0TMP.TRANSACTION_DATE
                                                 AND TRIM (F.TRANSACTION_TIME) =
                                                        TRIM (
                                                           SRC_TDM_FUL0TMP.TRANSACTION_TIME)
                                                 AND F.POSTING_DATE =
                                                        TO_DATE (
                                                           SRC_TDM_FUL0TMP.POSTING_DATE,
                                                           'YYMMDD')
                                                 AND TRIM (F.TICKET_NUMBER) =
                                                        TRIM (
                                                           SRC_TDM_FUL0TMP.TICKET_NUMBER)
                                                 AND F.WEX_TRANSACTION_CODE =
                                                        SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                                                 AND F.PRODUCT_CODE =
                                                        SRC_TDM_FUL0TMP.PRODUCT_CODE
                                                 AND F.CONTROL_CODE =
                                                        SRC_TDM_FUL0TMP.CONTROL_CODE
                                                 AND F.PRODUCT_CODE <> 55
                                                 AND F.CONTROL_CODE NOT IN
                                                        (03,
                                                         13,
                                                         23,
                                                         33,
                                                         43,
                                                         53,
                                                         63,
                                                         73,
                                                         83,
                                                         93))))
            ORDER BY WEX_TRANSACTION_ID));

   COMMIT;


 -- Select the max number of the row counts in SRC_TDM_TRANS
 SELECT MAX(ROW_NUM) INTO s_max_row_num
 FROM SRC_TDM_TRANS;

-- Insert VVD Transactions into SRC_TDM_TRANS
 INSERT /*+ APPEND */  
       INTO SRC_TDM_TRANS
              (           ROW_NUM,
                           CURRENCY_CODE,
                           TICKET_NUMBER,
                           GROSS_DOLLARS,
                           GALLONS_UNITS,
                           EXCHANGE_RATE,
                           WEX_FEE_AMOUNT,
                           CONTRACT_PRICE_FLAG,
                           VOLUME_DISCOUNT_AMT,
                           TRANSACTION_DATE,
                           TRANSACTION_TIME,
                           POSTING_DATE,
                           WEX_TRANSACTION_ID,
                           LEGACY_WEX_SITE_ID,
                           PRODUCT_CODE,
                           WEX_ACCT_NBR,
                           WEX_PD_NBR,
                           CLIENT_MERCHANT,
                           CLIENT_STATE,
                           TAX_TYPE_CODE_1,
                           TAX_AMT_1,
                           TAX_TYPE_CODE_2,
                           TAX_AMT_2,
                           TAX_TYPE_CODE_3,
                           TAX_AMT_3,
                           TAX_TYPE_CODE_4,
                           TAX_AMT_4,
                           TAX_TYPE_CODE_5,
                           TAX_AMT_5,
                           TAX_TYPE_CODE_6,
                           TAX_AMT_6,
                           TAX_TYPE_CODE_7,
                           TAX_AMT_7,
                           TAX_TYPE_CODE_8,
                           TAX_AMT_8,
                           TAX_TYPE_CODE_9,
                           TAX_AMT_9,
                           TAX_TYPE_CODE_10,
                           TAX_AMT_10,
                           TAX_TYPE_CODE_11,
                           TAX_AMT_11,
                           TAX_TYPE_CODE_12,
                           TAX_AMT_12,
                           TAX_TYPE_CODE_13,
                           TAX_AMT_13,
                           TAX_TYPE_CODE_14,
                           TAX_AMT_14,
                           TAX_TYPE_CODE_15,
                           TAX_AMT_15,
                           TAX_EXEMPT_INDICATOR_1,
                           TAX_EXEMPT_INDICATOR_2,
                           TAX_EXEMPT_INDICATOR_3,
                           TAX_EXEMPT_INDICATOR_4,
                           TAX_EXEMPT_INDICATOR_5,
                           TAX_EXEMPT_INDICATOR_6,
                           TAX_EXEMPT_INDICATOR_7,
                           TAX_EXEMPT_INDICATOR_8,
                           TAX_EXEMPT_INDICATOR_9,
                           TAX_EXEMPT_INDICATOR_10,
                           TAX_EXEMPT_INDICATOR_11,
                           TAX_EXEMPT_INDICATOR_12,
                           TAX_EXEMPT_INDICATOR_13,
                           TAX_EXEMPT_INDICATOR_14,
                           TAX_EXEMPT_INDICATOR_15,
                           MERCHANT_PREFIX,
                           MICROFILM_NUMBER,
                           WEX_TRANSACTION_CODE,
                           CONTROL_CODE,
                           TAX_RATE_1,
                           TAX_RATE_2,
                           TAX_RATE_3,
                           TAX_RATE_4,
                           TAX_RATE_5,
                           TAX_RATE_6,
                           TAX_RATE_7,
                           TAX_RATE_8,
                           TAX_RATE_9,
                           TAX_RATE_10,
                           TAX_RATE_11,
                           TAX_RATE_12,
                           TAX_RATE_13,
                           TAX_RATE_14,
                           TAX_RATE_15,
                           FILING_INDICATOR_1,
                           FILING_INDICATOR_2,
                           FILING_INDICATOR_3,
                           FILING_INDICATOR_4,
                           FILING_INDICATOR_5,
                           FILING_INDICATOR_6,
                           FILING_INDICATOR_7,
                           FILING_INDICATOR_8,
                           FILING_INDICATOR_9,
                           FILING_INDICATOR_10,
                           FILING_INDICATOR_11,
                           FILING_INDICATOR_12,
                           FILING_INDICATOR_13,
                           FILING_INDICATOR_14,
                           FILING_INDICATOR_15,
                           SETTLE_ENTITY,
                           FULTMP_SETTLE_GROSS_NET_FLAG,
                           FULTMP_EFPS_DISCOUNT_AMT,
                           FULTMP_FEDERAL_TAX,
                           FULTMP_STATE_TAX,
                           FULTMP_OTHER_TAX,
                           FULTMP_GALLONS_UNITS,
                           FULTMP_GROSS_DOLLARS,
                           FULTMP_VOLUME_DISCOUNT_AMT,
                           FULTMP_WEX_FEE_AMOUNT,
                           EFPS_DISCOUNT_AMT,
                           FEDERAL_TAX,
                           STATE_TAX,
                           OTHER_TAX,
                           TAX_FUEL_TYPE_CODE,
                           JURISDICTION_CODE_1,
                           JURISDICTION_CODE_2,
                           CITY,
                           FULTMP_CURRENCY_CODE,
                           FULTMP_NETWORK_IDENTIFIER,
                           NET_TRAN_TYPE,
                           BATCH_NUM,
                           AUTHORIZATION_CODE,
                           PIN_PLAIN,
                           ODOMETER,
                           VEHICLE_NUMBER,
                           CARD_SUFFIX,
                           FULTMP_EXCHANGE_RATE , 
                           VVD_FLG
             )
                               (SELECT s_max_row_num + ROWNUM,
                                         SRC_TDM_FUL2MST.CURRENCY_CODE,  -- Changed by VR on 02/01/2012 to point to FUL2MST instead  fof FUL0TMP  ( this should point to FUL2MST not FUL0TMP)  
                                         SRC_TDM_FUL2MST.TICKET_NUMBER,
                                         SRC_TDM_FUL2MST.GROSS_DOLLARS,
                                         SRC_TDM_FUL2MST.GALLONS_UNITS,
                                         SRC_TDM_FUL0TMP.EXCHANGE_RATE,
                                         SRC_TDM_FUL2MST.WEX_FEE_AMOUNT,
                                         '' CONTRACT_PRICE_FLAG,
                                         SRC_TDM_FUL2MST.VOLUME_DISCOUNT_AMT,
                                         SRC_TDM_FUL2MST.TRANSACTION_DATE,
                                         SRC_TDM_FUL2MST.TRANSACTION_TIME,
                                         TO_DATE(SRC_TDM_FUL2MST.POSTING_DATE, 'YYMMDD' ) POSTING_DATE,
                                         SRC_TDM_FUL2MST.WEX_TRANSACTION_ID,
                                         SRC_TDM_FUL2MST.LEGACY_WEX_SITE_ID,
                                         SRC_TDM_FUL2MST.PRODUCT_CODE,
                                         SRC_TDM_FUL2MST.CLIENT_MERCHANT||LPAD (SRC_TDM_FUL2MST.CLIENT_STATE, 2, 0)
                                                                                                    ||LPAD (SRC_TDM_FUL2MST.CLIENT_ZEROES, 2, 0)
                                                                                                    ||LPAD (SRC_TDM_FUL2MST.ACCOUNT_NUMBER, 6, 0)
                                                                                                    ||SRC_TDM_FUL2MST.CHECK_DIGIT                               WEX_ACCT_NBR,
                                         SRC_TDM_FUL2MST.CLIENT_MERCHANT||LPAD (SRC_TDM_FUL2MST.CLIENT_STATE, 2, 0)
                                                                                                    ||LPAD (SRC_TDM_FUL2MST.CLIENT_ZEROES, 2, 0)
                                                                                                    ||LPAD (SRC_TDM_FUL2MST.ACCOUNT_NUMBER, 6, 0)
                                                                                                    ||SRC_TDM_FUL2MST.CHECK_DIGIT
                                                                                                    ||LPAD (SRC_TDM_FUL2MST.VEHICLE_NUMBER, 4, 0)
                                                                                                    ||SRC_TDM_FUL2MST.CARD_SUFFIX                               WEX_PD_NBR,
                                         SRC_TDM_FUL2MST.CLIENT_MERCHANT,
                                         SRC_TDM_FUL2MST.CLIENT_STATE,
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_1,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_1, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_2,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_2, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_3,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_3, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_4,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_4, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_5,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_5, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_6,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_6, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_7,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_7, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_8,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_8, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_9,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_9, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_10,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_10, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_11,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_11, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_12,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_12, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_13,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_13, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_14,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_14, 0),
                                         SRC_TDM_HPD0TAX.TAX_TYPE_CODE_15,
                                         NVL(SRC_TDM_HPD0TAX.TAX_AMT_15, 0),
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_1,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_2,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_3,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_4,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_5,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_6,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_7,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_8,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_9,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_10,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_11,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_12,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_13,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_14,
                                         SRC_TDM_HPD0TAX.TAX_EXEMPT_INDICATOR_15,
                                         SRC_TDM_FUL2MST.MERCHANT_PREFIX,
                                         SRC_TDM_FUL2MST.MICROFILM_NUMBER,
                                         SRC_TDM_FUL2MST.WEX_TRANSACTION_CODE,
                                         SRC_TDM_FUL2MST.CONTROL_CODE,
                                         SRC_TDM_HPD0TAX.TAX_RATE_1,
                                         SRC_TDM_HPD0TAX.TAX_RATE_2,
                                         SRC_TDM_HPD0TAX.TAX_RATE_3,
                                         SRC_TDM_HPD0TAX.TAX_RATE_4,
                                         SRC_TDM_HPD0TAX.TAX_RATE_5,
                                         SRC_TDM_HPD0TAX.TAX_RATE_6,
                                         SRC_TDM_HPD0TAX.TAX_RATE_7,
                                         SRC_TDM_HPD0TAX.TAX_RATE_8,
                                         SRC_TDM_HPD0TAX.TAX_RATE_9,
                                         SRC_TDM_HPD0TAX.TAX_RATE_10,
                                         SRC_TDM_HPD0TAX.TAX_RATE_11,
                                         SRC_TDM_HPD0TAX.TAX_RATE_12,
                                         SRC_TDM_HPD0TAX.TAX_RATE_13,
                                         SRC_TDM_HPD0TAX.TAX_RATE_14,
                                         SRC_TDM_HPD0TAX.TAX_RATE_15,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_1,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_2,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_3,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_4,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_5,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_6,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_7,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_8,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_9,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_10,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_11,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_12,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_13,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_14,
                                         SRC_TDM_HPD0TAX.FILING_INDICATOR_15,
                                         SRC_TDM_FUL2MST.SETTLE_ENTITY,
                                         SRC_TDM_FUL0TMP.SETTLE_GROSS_NET_FLAG                 TMP_SETTLE_GROSS_NET_FLAG,
                                         SRC_TDM_FUL0TMP.EFPS_DISCOUNT_AMT                        TMP_EFPS_DISCOUNT_AMT,
                                         SRC_TDM_FUL0TMP.FEDERAL_TAX                                    TMP_FEDERAL_TAX,
                                         SRC_TDM_FUL0TMP.STATE_TAX                                       TMP_STATE_TAX,
                                         SRC_TDM_FUL0TMP.OTHER_TAX                                       TMP_OTHER_TAX,
                                         SRC_TDM_FUL0TMP.GALLONS_UNITS                                 TMP_GALLONS_UNITS,
                                         SRC_TDM_FUL0TMP.GROSS_DOLLARS                                TMP_GROSS_DOLLARS,
                                         SRC_TDM_FUL0TMP.VOLUME_DISCOUNT_AMT                    TMP_VOLUME_DISCOUNT_AMT,
                                         SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT                             TMP_WEX_FEE_AMOUNT,
                                         SRC_TDM_FUL2MST.EFPS_DISCOUNT_AMT,
                                         SRC_TDM_FUL2MST.FEDERAL_TAX,
                                         SRC_TDM_FUL2MST.STATE_TAX,
                                         SRC_TDM_FUL2MST.OTHER_TAX,
                                         SRC_TDM_HPD0TAX.TAX_FUEL_TYPE_CODE,
                                         SRC_TDM_HPD0TAX.JURISDICTION_CODE_1,
                                         SRC_TDM_HPD0TAX.JURISDICTION_CODE_2,
                                         SRC_TDM_HPD0TAX.CITY,
                                         CASE 
                                                  WHEN  TRIM(SRC_TDM_FUL0TMP.CURRENCY_CODE) IS NULL 
                                                  THEN   SRC_TDM_FUL2MST.CURRENCY_CODE 
                                                   ELSE 
                                                           SRC_TDM_FUL0TMP.CURRENCY_CODE
                                          END                                                                           TMP_CURRENCY_CODE,
                                         --SRC_TDM_FUL2MST.WEX_TRANSACTION_SEQ_NBR,
                                         SRC_TDM_FUL0TMP.NETWORK_IDENTIFIER                 TMP_NETWORK_IDENTIFIER,
                                         SRC_TDM_FUL2MST.NET_TRAN_TYPE,
                                         SRC_TDM_FUL2MST.BATCH_NUM,
                                         SRC_TDM_FUL2MST.AUTHORIZATION_CODE,
                                         SRC_TDM_FUL2MST.PIN_PLAIN,
                                         SRC_TDM_FUL2MST.ODOMETER,
                                         SRC_TDM_FUL2MST.VEHICLE_NUMBER,
                                         SRC_TDM_FUL2MST.CARD_SUFFIX,
                                         SRC_TDM_FUL0TMP.EXCHANGE_RATE FULTMP_EXCHANGE_RATE,
                                         'Y' VVD_FLG
                               FROM SRC_TDM_FUL2MST, 
                                        SRC_TDM_FUL0TMP, 
                                        SRC_TDM_HPD0TAX
                               WHERE  
                                                  SRC_TDM_FUL2MST.CLIENT_MERCHANT                           = SRC_TDM_FUL0TMP.CLIENT_MERCHANT
                                           AND SRC_TDM_FUL2MST.CLIENT_STATE                                  = SRC_TDM_FUL0TMP.CLIENT_STATE
                                           AND SRC_TDM_FUL2MST.CLIENT_ZEROES                                 = SRC_TDM_FUL0TMP.CLIENT_ZEROES
                                           AND SRC_TDM_FUL2MST.ACCOUNT_NUMBER                            = SRC_TDM_FUL0TMP.ACCOUNT_NUMBER
                                           AND SRC_TDM_FUL2MST.CHECK_DIGIT                                     = SRC_TDM_FUL0TMP.CHECK_DIGIT
                                           AND SRC_TDM_FUL2MST.VEHICLE_NUMBER                              = SRC_TDM_FUL0TMP.VEHICLE_NUMBER
                                           AND SRC_TDM_FUL2MST.TRANSACTION_DATE                          = SRC_TDM_FUL0TMP.TRANSACTION_DATE
                                           AND TRIM (SRC_TDM_FUL2MST.TRANSACTION_TIME)               = TRIM ( SRC_TDM_FUL0TMP.TRANSACTION_TIME)
                                           AND TO_DATE (SRC_TDM_FUL2MST.POSTING_DATE,'YYMMDD')  = TO_DATE (SRC_TDM_FUL0TMP.POSTING_DATE,'YYMMDD')
                                           AND TRIM (SRC_TDM_FUL2MST.TICKET_NUMBER)                     = TRIM (SRC_TDM_FUL0TMP.TICKET_NUMBER)
                                           AND SRC_TDM_FUL2MST.WEX_TRANSACTION_CODE                 = SRC_TDM_FUL0TMP.WEX_TRANSACTION_CODE
                                           AND SRC_TDM_FUL2MST.PRODUCT_CODE                                 = SRC_TDM_FUL0TMP.PRODUCT_CODE
                                           AND SRC_TDM_FUL2MST.CONTROL_CODE                                 = SRC_TDM_FUL0TMP.CONTROL_CODE
                                           AND SRC_TDM_FUL2MST.PRODUCT_CODE                                 <> 55
                                           AND SRC_TDM_FUL0TMP.GROSS_DOLLARS                                =  0  -- eliminate Vvariable Volume Discounts
                                           --  AND  SRC_TDM_FUL0TMP.GALLONS_UNITS <> 0 -- eliminate Vvariable Volume Discounts
                                           --   AND SRC_TDM_FUL2MST.MICROFILM_NUMBER  =  SRC_TDM_FUL0TMP.MICROFILM_NUMBER
                                           -- AND SRC_TDM_FUL2MST.WEX_FEE_AMOUNT = SRC_TDM_FUL0TMP.WEX_FEE_AMOUNT
                                           AND SRC_TDM_FUL0TMP.CONTROL_CODE                           NOT IN  
                                                                                                                                            (03, 13, 23, 33, 43, 53, 63, 73, 83, 93)
                                           AND SRC_TDM_FUL2MST.WEX_TRANSACTION_ID                       = SRC_TDM_HPD0TAX.WEX_TRANSACTION_ID (+)
                                           AND TO_DATE(SRC_TDM_FUL2MST.POSTING_DATE, 'YYMMDD')   =  SRC_TDM_HPD0TAX.POSTING_DATE(+)
                                           );
COMMIT;

END SRC_TDM_PRE_TRANS_LOAD;
/