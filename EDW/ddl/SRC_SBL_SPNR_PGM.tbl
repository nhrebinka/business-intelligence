
SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON
DEFINE &&env = &1

SPOOL /i1/&&env/hub/logs/SRC_SBL_SPNR_PGM.tbl.log ; 

DROP TABLE SRC_SBL_SPNR_PGM CASCADE CONSTRAINTS;

CREATE TABLE SRC_SBL_SPNR_PGM
(
  DS_SPNR_PGM_KEY                NUMBER(15),
  SPNR_NM                        VARCHAR2(100 BYTE),
  PGM_ROW_ID                     VARCHAR2(15 BYTE),
  PGM_END_DT                     DATE,
  PGM_NM                         VARCHAR2(50 BYTE),
  PGM_START_DT                   DATE,
  PGM_DESC                       VARCHAR2(255 BYTE),
  SPNR_ROW_ID                    VARCHAR2(15 BYTE),
  PGM_SUB_TYPE                   VARCHAR2(30 BYTE),
  PGM_APPROVED_DT                DATE,
  PGM_APPROVED_FLG               CHAR(1 BYTE),
  PAR_PGM_ROW_ID                 VARCHAR2(15 BYTE),
  SPNR_PRC_LST_APPROVED_DT       DATE,
  LINE_OF_BUS_NM                 VARCHAR2(30 BYTE),
  COBRAND_ALLOWED_FLG            CHAR(1 BYTE),
  PGM_CONTACT_FIRST_NM           VARCHAR2(50 BYTE),
  PGM_CONTACT_LAST_NM            VARCHAR2(50 BYTE),
  CURR_CONV_SURCHARGE_RATE       NUMBER(20,8),
  CURRENCY_CD                    VARCHAR2(3 BYTE),
  INTL_UNITS_SUPPORTED_CD        VARCHAR2(22 BYTE),
  LANGUAGE_CD                    VARCHAR2(30 BYTE),
  MAX_TRANS_AGE_CD               VARCHAR2(30 BYTE),
  SPNR_PRC_LIST_NM               VARCHAR2(100 BYTE),
  PGM_SHORT_NM                   VARCHAR2(30 BYTE),
  DW_CREATE_DT                   DATE,
  PGM_STS                        VARCHAR2(30 BYTE),
  TAX_PART_FLG                   CHAR(1 BYTE),
  DW_LAST_UPDT_DT                DATE,
  CLIENT_ID                      VARCHAR2(4 BYTE),
  DW_LAST_UPDT_SESSION_NM        VARCHAR2(100 BYTE),
  ONLINE_INV_URL                 VARCHAR2(100 BYTE),
  WEX_FUNDED_FLG                 CHAR(1 BYTE),
  PHONE_NBR                      VARCHAR2(40 BYTE),
  DW_SOURCE_SYS                  VARCHAR2(30 BYTE),
  SRC_SYS_LAST_UPDT_DT           DATE,
  DW_PROCESSED_CD                VARCHAR2(2 BYTE),
  OVL_MACRO_FILE_NM              VARCHAR2(30 BYTE),
  CUSTOMER_SERVICE_PHONE_NBR     VARCHAR2(100 BYTE),
  CUSTOMER_SERVICE_FAX_NBR       VARCHAR2(100 BYTE),
  PGM_URL                        VARCHAR2(100 BYTE),
  PGM_ENROLLMENT_URL             VARCHAR2(100 BYTE),
  LANDSCAPE_LOGO_FILE_NM         VARCHAR2(100 BYTE),
  PORTRAIT_LOGO_FILE_NM          VARCHAR2(100 BYTE),
  LOGO_BITMAP_FILE_NM            VARCHAR2(100 BYTE),
  FUNDED_INVOICE_BACK_FILE_NM    VARCHAR2(100 BYTE),
  EXCEPTION_SIGNUP_FORM_NM       VARCHAR2(30 BYTE),
  AUTH_PROD_SET_ROW_ID           VARCHAR2(15 BYTE),
  SALES_PROD_SET_ROW_ID          VARCHAR2(15 BYTE),
  FEDERAL_CCI_TAX_EXEMPT_FLG     CHAR(1 BYTE),
  DISTRIBUTOR_HOST_ID            VARCHAR2(15 BYTE),
  ID_OUT_OF_NETW_TRANS_FLG       CHAR(1 BYTE),
  DFLT_MRCH_AGRMNT_PGM_ROW_ID    VARCHAR2(15 BYTE),
  UNFUNDED_INVOICE_BACK_FILE_NM  VARCHAR2(100 BYTE),
  RELATION_CD                    VARCHAR2(6 BYTE),
  AUTO_PRMPT_CORR_FLG            CHAR(1 BYTE),
  STATE_CCI_TAX_EXEMPT_FLG       CHAR(1 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDM_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE 
    LOGFILE          EDM_LOG_DIR:'src_sbl_spnr_pgm.log'    
    BADFILE          EDM_LOG_DIR:'src_sbl_spnr_pgm.bad'
    DISCARDFILE   EDM_LOG_DIR:'src_sbl_spnr_pgm.dis'
    FIELDS  TERMINATED BY '~|~'
    LRTRIM
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS
 (DS_SPNR_PGM_KEY                ,
  SPNR_NM                        ,
  PGM_ROW_ID                     ,
  PGM_END_DT                     CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  PGM_END_DT  = BLANKS ,
  PGM_NM                         ,
  PGM_START_DT                   CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  PGM_START_DT  = BLANKS ,
  PGM_DESC                       ,
  SPNR_ROW_ID                    ,
  PGM_SUB_TYPE                   ,
  PGM_APPROVED_DT                CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  PGM_APPROVED_DT  = BLANKS ,
  PGM_APPROVED_FLG               ,
  PAR_PGM_ROW_ID                 ,
  SPNR_PRC_LST_APPROVED_DT       CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  SPNR_PRC_LST_APPROVED_DT  = BLANKS ,
  LINE_OF_BUS_NM                 ,
  COBRAND_ALLOWED_FLG            ,
  PGM_CONTACT_FIRST_NM           ,
  PGM_CONTACT_LAST_NM            ,
  CURR_CONV_SURCHARGE_RATE       ,
  CURRENCY_CD                    ,
  INTL_UNITS_SUPPORTED_CD        ,
  LANGUAGE_CD                    ,
  MAX_TRANS_AGE_CD               ,
  SPNR_PRC_LIST_NM               ,
  PGM_SHORT_NM                   ,
  DW_CREATE_DT                   CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  DW_CREATE_DT  = BLANKS ,
  PGM_STS                        ,
  TAX_PART_FLG                   ,
  DW_LAST_UPDT_DT                CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  DW_LAST_UPDT_DT  = BLANKS ,
  CLIENT_ID                      ,
  DW_LAST_UPDT_SESSION_NM        ,
  ONLINE_INV_URL                 ,
  WEX_FUNDED_FLG                 ,
  PHONE_NBR                      ,
  DW_SOURCE_SYS                  ,
  SRC_SYS_LAST_UPDT_DT           CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  SRC_SYS_LAST_UPDT_DT  = BLANKS ,
  DW_PROCESSED_CD                ,
  OVL_MACRO_FILE_NM              ,
  CUSTOMER_SERVICE_PHONE_NBR     ,
  CUSTOMER_SERVICE_FAX_NBR       ,
  PGM_URL                        ,
  PGM_ENROLLMENT_URL             ,
  LANDSCAPE_LOGO_FILE_NM         ,
  PORTRAIT_LOGO_FILE_NM          ,
  LOGO_BITMAP_FILE_NM            ,
  FUNDED_INVOICE_BACK_FILE_NM    ,
  EXCEPTION_SIGNUP_FORM_NM       ,
  AUTH_PROD_SET_ROW_ID           ,
  SALES_PROD_SET_ROW_ID          ,
  FEDERAL_CCI_TAX_EXEMPT_FLG     ,
  DISTRIBUTOR_HOST_ID            ,
  ID_OUT_OF_NETW_TRANS_FLG       ,
  DFLT_MRCH_AGRMNT_PGM_ROW_ID    ,
  UNFUNDED_INVOICE_BACK_FILE_NM  ,
  RELATION_CD                    ,
  AUTO_PRMPT_CORR_FLG            ,
  STATE_CCI_TAX_EXEMPT_FLG   )
      )
     LOCATION (EDM_DATA_DIR:'src_sbl_spnr_pgm.dat')
  )
REJECT LIMIT 0
NOPARALLEL
NOMONITORING;


GRANT ALTER, SELECT ON SRC_SBL_SPNR_PGM TO DWLOADER;

GRANT ALTER, SELECT ON SRC_SBL_SPNR_PGM TO EDM_OWNER;

GRANT ALTER, SELECT ON SRC_SBL_SPNR_PGM TO STAGE_OWNER_SELECT;



SPOOL OFF ; 