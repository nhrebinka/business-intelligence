    INSERT INTO xo_session (
      SESSION_KEY,
      SESSION_ID,
      SESSION_TYPE,
      STATUS,
      SOURCE_NAME,
      REV_YM_ABBR,
      REV_BGN_DT,
      REV_END_DT,
      RUN_ID,
      BATCH_ID,
      CREATED_AT,
      UPDATED_AT
    ) 
    VALUES (
      XO_SESSION_KEY_SEQ.NEXTVAL,
      to_char(systimestamp, 'YYYYMMDD.HHMISS.FF4'),
      'D_SITE_AGG',
      'INIT',
      'EDW_OWNER.F_TRANSACTION_LINE_ITEM',
      '2014-03',
      (select min(calendar_date_dt) from d_date where month_year_abbr='2014-03'),
      (select max(calendar_date_dt) from d_date where month_year_abbr='2014-03'),
      to_char(sysdate, 'YYYYMMDD.HHMISS'),
      to_char(sysdate, 'YYYYMMDD'),
      sysdate,
      sysdate
    );
