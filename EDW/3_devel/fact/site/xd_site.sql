--
-- create the daily aggr. table for 
--    purchases per site/merchant/program per revenue month
-- 
-- note:
--  it takes about 15 min on PDEDW with 12 threads (default)
--
--
-- column name abbriviation rule used: 
--   1. amount => amt
--   2. fees   => fee
--   3. maintenance => maint
--   4. transaction => trans
--   5. interchange => intchg
--
-- todo:
--   1. use an updatable join view with parallel UPDATE to refresh the updated rows, 
--   2. use an anti-hash join with parallel INSERT to refresh the new rows.
--
DROP   TABLE xd_site_a00;

CREATE TABLE xd_site_a00
tablespace d_bi
storage (
  initial 100M
  next    100M
  maxextents unlimited
)
compress for oltp
parallel (degree 12)
nologging
AS
-- INSERT /*+ append */ INTO xd_site_yyyymm
SELECT /*+ star_transformation fact(tx) parallel(tx, 12) */
  tx.REVENUE_DATE_KEY,
  tx.POSTING_DATE_KEY,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
--
  CASE
    WHEN si.SIC_LEVEL_I_DESCRIPTION = 'Unknown'     THEN 'N/A'
    WHEN si.SIC_LEVEL_I_DESCRIPTION is NULL         THEN 'N/A'
    WHEN INSTR(si.SIC_LEVEL_I_DESCRIPTION, '. ')=0  THEN si.SIC_LEVEL_I_DESCRIPTION
    ELSE SUBSTR(si.SIC_LEVEL_I_DESCRIPTION, INSTR(si.SIC_LEVEL_I_DESCRIPTION, '. ')+2)
  END as sic_abstract,
  ni.NAICS_LEVEL_I_DESCRIPTION  naics_level_1,
  ni.NAICS_LEVEL_II_DESCRIPTION naics_level_2,
--
  COUNT(
    CASE
    WHEN tx.GROSS_SPEND_AMOUNT <> 0
    THEN tx.wex_transaction_id
    ELSE NULL
    END
  ) trans_ticket_count,
--
  SUM(tx.NBR_OF_UNITS_PURCHASED_COUNT)   nbr_of_units_purchased_count,
  SUM(tx.TRANSACTION_LINE_ITEM_COUNT1)   trans_line_item_count,
--
  SUM(tx.FUEL_ONLY_INTERCHANGE_AMOUNT)   fuel_only_intchg_amt,	
  SUM(tx.MAINTENANCE_ONLY_INTCHG_AMOUNT) maint_only_intchg_amt,
  SUM(tx.ALL_OTHER_INTERCHANGE_AMOUNT)   all_other_intchg_amt,
  SUM(tx.ALL_OTHER_ANCILLARY_AMOUNT)     all_other_ancillary_amt,
  SUM(tx.CARD_FEES_AMOUNT)               card_fees_amt,
  SUM(tx.CURRENCY_CONVERSION_FEE_AMOUNT) currency_conversion_fee_amt,	
  SUM(tx.DISCOUNT_AMOUNT)                discount_amt,
  SUM(tx.FINANCE_FEE_AMOUNT)             finance_fee_amt,	
  SUM(tx.LATE_FEES_AMOUNT)	             late_fee_amt,
  SUM(tx.REBATE_AMOUNT)                  rebate_amt,
  SUM(tx.WAIVED_LATE_FEES_AMOUNT)        waived_late_fee_amt,	
  SUM(tx.WAIVED_REVENUE_AMOUNT)          waived_revenue_amt,	
  SUM(tx.NET_REVENUE_AMOUNT)             net_revenue_amt,	
--
  SUM(tx.GROSS_SPEND_AMOUNT)             gross_spend_amt,	
  SUM(tx.GROSS_SPEND_LOCAL_AMOUNT)       gross_spend_local_amt,	
  SUM(tx.GROSS_REVENUE_AMOUNT)           gross_revenue_amt,	
  SUM(tx.GROSS_NON_REVENUE_AMOUNT)       gross_non_revenue_amt,	
--
  SUM(tx.PURCHASE_ITEM_AMOUNT)           purchase_item_amt,
  SUM(tx.PURCHASE_ITEM_LOCAL_AMOUNT)     purchase_item_local_amt,
--
  SUM(tx.SPEND_FUEL_ONLY_AMOUNT)         spend_fuel_only_amt,
  SUM(tx.SPEND_FUEL_ONLY_LOCAL_AMOUNT)   spend_fuel_only_local_amt,
  SUM(tx.SPEND_FUEL_DIESEL_AMOUNT)       spend_fuel_diesel_amt,
  SUM(tx.SPEND_FUEL_DIESEL_LOCAL_AMOUNT) spend_fuel_diesel_local_amt,
  SUM(tx.SPEND_FUEL_GAS_AMOUNT)          spend_fuel_gas_amt,
  SUM(tx.SPEND_FUEL_GAS_LOCAL_AMOUNT)    spend_fuel_gas_local_amt,
  SUM(tx.SPEND_FUEL_OTHER_AMOUNT)        spend_fuel_other_amt,
  SUM(tx.SPEND_FUEL_OTHER_LOCAL_AMOUNT)  spend_fuel_other_local_amt,
  SUM(tx.SPEND_MAINTENANCE_AMOUNT)       spend_maint_amt,
  SUM(tx.SPEND_MAINTENANCE_LOCAL_AMOUNT) spend_maint_local_amt,
  SUM(tx.SPEND_OTHER_AMOUNT)             spend_other_amt,
  SUM(tx.SPEND_OTHER_LOCAL_AMOUNT)       spend_other_local_amt,
-- interchange
  SUM(tx.INTERCHANGE_FUEL_DIESEL_AMT)    intchg_fuel_diesel_amt,
  SUM(tx.INTERCHANGE_FUEL_GAS_AMT)       intchg_fuel_gas_amt,
  SUM(tx.INTERCHANGE_FUEL_OTHER_AMT)     intchg_fuel_other_amt,
  SUM(tx.INTERCHANGE_TOTAL_AMOUNT)       intchg_total_amt,
--
  SUM(tx.INTCHG_LINE_ITEM_FLAT_FEE_AMT)  intchg_line_item_flat_fee_amt,
  SUM(tx.INTCHG_TRANS_FLAT_FEE_AMT)      intchg_trans_flat_fee_amt,
  SUM(tx.INTERCHANGE_PER_UNIT_FEE_AMT)   intchg_per_unit_fee_amt,
  AVG(tx.INTERCHANGE_RATE)               intchg_rate_avg,
  SUM(tx.INTERCHANGE_RATE_ACTUAL_AMOUNT) intchg_rate_actual_amt,
-- tax
  SUM(tx.TOTAL_TAX_AMOUNT)               total_tax_amt,
  SUM(tx.TOTAL_TAX_LOCAL_AMOUNT)         total_tax_local_amt,
-- 
  SUM(tx.FEDERAL_TAX_AMOUNT)             federal_tax_amt,
  SUM(tx.STATE_TAX_AMOUNT)               state_tax_amt,
  SUM(tx.LOCAL_TAX_AMOUNT)               local_tax_amt,
  SUM(tx.NON_FEDERAL_TAX_AMOUNT)         non_federal_tax_amt,
-- excise tax
  SUM(tx.FEDERAL_EXCISE_TAX_AMOUNT)      federal_excise_tax_amt,
  SUM(tx.STATE_EXCISE_TAX_AMOUNT)        state_excise_tax_amt,
  SUM(tx.COUNTY_EXCISE_TAX_AMOUNT)       county_excise_tax_amt,
  SUM(tx.CITY_EXCISE_TAX_AMOUNT)         city_excise_tax_amt,
-- sales tax
  SUM(tx.STATE_SALES_TAX_AMOUNT)         state_sales_tax_amt,
  SUM(tx.COUNTY_SALES_TAX_AMOUNT)        county_sales_tax_amt,
  SUM(tx.CITY_SALES_TAX_AMOUNT)          city_sales_tax_amt,
-- special tax
  SUM(tx.STATE_SPECIAL_TAX_AMOUNT)       state_special_tax_amt,
  SUM(tx.COUNTY_SPECIAL_TAX_AMOUNT)      county_special_tax_amt,
  SUM(tx.CITY_SPECIAL_TAX_AMOUNT)        city_special_tax_amt,
-- exempt
  SUM(tx.TAX_EXEMPT_SPEND_AMOUNT)        tax_exempt_spend_amt,
  SUM(tx.TAX_EXEMPT_SPEND_LOCAL_AMOUNT)  tax_exempt_spend_local_amt,
-- consignment_id YYYYMMDD.HHMISS.PID
  '20130320.010203.0001'                 session_id
FROM F_TRANSACTION_LINE_ITEM tx
  JOIN D_DATE       dt ON (tx.REVENUE_DATE_KEY     = dt.DATE_KEY)
  JOIN D_ACCOUNT    ac ON (tx.PURCHASE_ACCOUNT_KEY = ac.ACCOUNT_KEY)
  JOIN S_SIC_LOOKUP si ON (ac.SIC_LOOKUP_KEY       = si.SIC_LOOKUP_KEY)
  JOIN D_NAICS      ni ON (ac.NAICS_INDUSTRY_CODE  = ni.NAICS_CODE)
--
WHERE 1=0
  AND dt.MONTH_YEAR_ABBR = '2013-01'
--
GROUP BY
  tx.REVENUE_DATE_KEY,
  tx.POSTING_DATE_KEY,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
  CASE
    WHEN si.SIC_LEVEL_I_DESCRIPTION = 'Unknown'     THEN 'N/A'
    WHEN si.SIC_LEVEL_I_DESCRIPTION is NULL         THEN 'N/A'
    WHEN INSTR(si.SIC_LEVEL_I_DESCRIPTION, '. ')=0  THEN si.SIC_LEVEL_I_DESCRIPTION
    ELSE SUBSTR(si.SIC_LEVEL_I_DESCRIPTION, INSTR(si.SIC_LEVEL_I_DESCRIPTION, '. ')+2)
  END,
  ni.NAICS_LEVEL_I_DESCRIPTION,
  ni.NAICS_LEVEL_II_DESCRIPTION
;

--exec dbms_stats.gather_table_stats('EDW_OWNER', 'XD_SITE');

