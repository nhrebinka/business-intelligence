CREATE OR REPLACE PROCEDURE           DTS_TRANSACTION_LINE_ITEM_CNT (
   p_calendar_dt_start   IN DATE := TRUNC (SYSDATE),
   p_calendar_dt_end     IN DATE := TRUNC (SYSDATE))
AS
   /*
   NAME                    :-    DTS_TRANSACTION_LINE_ITEM_COUNT
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    01/24/2011
   DECRIPTION :-   PROCEURE TO change (historically) the TRANSACTION_LINE_ITEM_COUNT1 represent the number of swipe line items only.


*/


   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;
BEGIN
   v_calendar_dt_start := p_calendar_dt_start;
   v_calendar_dt_end := p_calendar_dt_end;

   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT, D.DATE_KEY
            FROM D_DATE D
           WHERE D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start
                                        AND v_calendar_dt_end)
   LOOP
      MERGE INTO F_DAILY_TOTALS_SNAPSHOT DTS
           USING (  SELECT                              /*+ parallel (TLI,8)*/
                          DRS.POSTING_DATE_KEY AS POSTING_DATE_KEY,
                           DRS.REVENUE_DATE_KEY AS REVENUE_DATE_KEY,
                           SUM (DRS.TRANSACTION_LINE_ITEM_COUNT1)
                              AS TRANSACTION_LINE_ITEM_COUNT1
                      FROM    F_DAILY_REVENUE_SNAPSHOT DRS
                           JOIN
                              D_DATE D
                           ON D.DATE_KEY = DRS.REVENUE_DATE_KEY
                              AND D.DATE_KEY = rec.DATE_KEY
                  GROUP BY DRS.REVENUE_DATE_KEY, DRS.POSTING_DATE_KEY) A
              ON (A.REVENUE_DATE_KEY = DTS.REVENUE_DATE_KEY
                  AND A.POSTING_DATE_KEY = DTS.POSTING_DATE_KEY)
      WHEN MATCHED
      THEN
         UPDATE SET
            DTS.TRANSACTION_LINE_ITEM_COUNT1 = A.TRANSACTION_LINE_ITEM_COUNT1,
            DTS.ROW_LAST_MOD_PROC_NM = 'DTS_TRANSACTION_LINE_ITEM_CNT',
            DTS.ROW_LAST_MOD_DTTM = SYSDATE;

      COMMIT;
   END LOOP;
END DTS_TRANSACTION_LINE_ITEM_CNT;

/