drop table x_part purge;

create table x_part (
   x_part_key      integer,
   start_date      DATE,
   whatever        varchar2(10),
   session_id      number(10),
   part_id         number(1) generated always as (mod(session_id, 6)) virtual
)
PARTITION BY RANGE (part_id) INTERVAL(1)
( 
   PARTITION p0 VALUES LESS THAN (0),
   PARTITION p1 VALUES LESS THAN (1),
   PARTITION p2 VALUES LESS THAN (2),
   PARTITION p3 VALUES LESS THAN (3),
   PARTITION p4 VALUES LESS THAN (4),
   PARTITION p5 VALUES LESS THAN (5)
)
tablespace d_bi
; 

alter table x_part truncate partition for (1);

desc x_part;

CREATE TABLE x_user (
  id           NUMBER,
  username     VARCHAR2(20),
  first_letter VARCHAR2(1) GENERATED ALWAYS AS (
    UPPER(SUBSTR(TRIM(username), 1, 1))
  ) VIRTUAL
)
PARTITION BY LIST (first_letter)
(
  PARTITION part_a_g VALUES ('A','B','C','D','E','F','G'),
  PARTITION part_h_n VALUES ('H','I','J','K','L','M','N'),
  PARTITION part_o_u VALUES ('O','P','Q','R','S','T','U'),
  PARTITION part_v_z VALUES ('V','W','X','Y','Z')
);
