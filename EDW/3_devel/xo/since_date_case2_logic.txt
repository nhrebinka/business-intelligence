1. get all user entered dates (3 minutes on EDM)
    SELECT /*+ parallel(m, 64) parallel(x, 64) use_hash(m, x) */
      m.edm_acct_key,
      m.wex_acct_nbr,
      m.acct_orig_activ_dt  cur_activ_dt,
      mu.wex_acct_nbr       legacy_acct_nbr, 
      mu.acct_orig_activ_dt legacy_activ_dt,
      mu.since_dt,
      mu.sales_rep_cd
    FROM m_acct m     -- get every accounts under the same L1
    JOIN ( 
      SELECT /*+ parallel(m,64) parallel(x,64) use_hash(ox, x) */
        m.ult_par_acct_nbr, m.wex_acct_nbr, m.acct_orig_activ_dt, m.sales_rep_cd, 
        to_date(ox.name,'MM/DD/YYYY') since_dt
      FROM (
        SELECT /*+ parallel(a) nocache */ 
          a.target_ou_id, a.name
        FROM stage_owner.src_sbl_s_evt_act a 
        WHERE a.todo_cd = 'Legacy Setup Date'
      ) ox 
      JOIN edm_owner.m_acct_xref x ON (x.acct_source_pk = ox.target_ou_id)
      JOIN edm_owner.m_acct      m ON (1=1
        AND m.edm_acct_key = x.edm_acct_key
        AND m.acct_type NOT IN ('Customer', 'National ID')
      )
    ) mu ON (mu.ult_par_acct_nbr = m.ult_par_acct_nbr)
    order by m.wex_acct_nbr;

2. join to all the Onboarding sales (seconds on EDM)
    SELECT /*+ use_hash(u, x) */ x.user_source_pk, u.user_title, u.user_first_name, u.user_last_name
    FROM edm_owner.m_user u
    JOIN edm_owner.m_user_xref x ON (u.edm_user_key = x.edm_user_key)
    WHERE x.user_source_pk IN ('WEXLINK', 'TBOYDEN', 'KCYR', 'LDOWNEY') 
      OR (1=1
        AND u.user_department = 'US - MP Boarding'
        AND u.user_title LIKE '%Strategic Imp%'
      )
    order by x.user_source_pk;

