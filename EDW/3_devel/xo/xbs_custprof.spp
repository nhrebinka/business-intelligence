/*
  Name : xbs_custprof_obj Object - extending xbs_base_obj
  Spec.: Custom Profitability aggregation. Refactored from xbs_site_obj

  Package dependency
    xbs_session_obj
    xbs_config_obj
    xbs_base_obj
      log4me
      xos_workspace
      xos_session

  Revision History: 
  1.0 20140318 CC - first version (refactor from xbs_site_obj)
*/
-- ============================================================================
-- object spec
--
CREATE OR REPLACE TYPE Xbs_Custprof UNDER Xbs_Base (
  CONSTRUCTOR FUNCTION Xbs_Custprof(
    p_env          varchar2,
    p_version_id   varchar2 := NULL
  ) RETURN SELF AS RESULT
) FINAL;
/
show errors

-- ============================================================================
-- object body
--
CREATE OR REPLACE TYPE BODY Xbs_Custprof AS

CONSTRUCTOR FUNCTION Xbs_Custprof (
  p_env          varchar2,
  p_version_id   varchar2 := NULL
)
RETURN SELF AS RESULT
AS
  X_SES_TYPE constant varchar2(40) := 'M_GL_ACCT_SNAP';
  X_SRC_SQL  constant CLOB := '
  SELECT /*+ parallel */
    GL_ACCT_MONTHLY_SNAPSHOT_KEY,
    GL_ACCOUNTING_DATE_KEY,
    BUSINESS_SECTOR_KEY,
    BUSINESS_SEGMENT_KEY,
    FR_CATEGORY_KEY,
    COST_CENTER_KEY,
    PROGRAM_KEY,
    ACCOUNT_KEY,
    ACCOUNT_HIST_KEY,
    AMOUNT,
    DEPRECIATION_AMOUNT,
    SNAPSHOT_DATE_KEY,
    ''#{SESSION_ID}''
  FROM  #{SOURCE_TB}
  WHERE #{RUNLEVEL}
  ';
  v_ses_obj Xbs_Session := Xbs_Session(
    X_SES_TYPE,                                    -- ses_type
    'EDW_STAGE_OWNER.F_GL_ACCT_MONTHLY_SNAPSHOT',  -- seed_tb
    'EDW_STAGE_OWNER.F_GL_ACCT_MONTHLY_SNAPSHOT',  -- src_tb
    'F_GL_ACCT_MONTHLY_SNAPSHOT_VW',               -- tgt_vw
    X_SRC_SQL                                      -- src_sql
  );

BEGIN
  (SELF AS Xbs_Base).init(
    v_ses_obj,    -- pass by reference
    p_env,
    p_version_id
  );
  RETURN;
END;

END;
/
show errors
-- ============================================================================

/*
-- 
-- 1. for DEV - run at least once for development,
-- 2. for PRD - uncomment the following before release
--
DELETE FROM xb_config where session_type = 'M_GL_ACCT_SNAP';
INSERT INTO xb_config VALUES(
  'DEV', 'M_GL_ACCT_SNAP', '1.0.0', systimestamp,
  Xbo_Config(1)
);
INSERT INTO xb_config VALUES(
  'PRD', 'M_GL_ACCT_SNAP', '1.0.0', systimestamp,
  Xbo_Config(0)
);
COMMIT;

CREATE OR REPLACE PROCEDURE F_GL_ACCT_MON_SNAPSHOT_LOAD
AS
  v_bobj Xbs_Custprof := Xbs_Custprof('DEV');
BEGIN
  -- Daily site aggregate proc - calls underneath xbs_site_agg package
  -- and perform the following steps:
  --
  -- 1. take seed dates from EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM table,
  --    create a entry in xo_session table
  -- 2. mark a cached result from previous run stale
  -- 3. execute the session to create the daily result, including
  --    rebuild the daily view and monthly view.
  --
  v_bobj.run_daily;
END F_GL_ACCT_MON_SNAPSHOT_LOAD;
/
show errors

GRANT EXECUTE ON F_GL_ACCT_MON_SNAPSHOT_LOAD TO DWLOADER;
*/





