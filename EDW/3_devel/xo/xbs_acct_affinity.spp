/*
  Name : xbs_acct_affinity Package
  Spec.: Package that management Account and Program affinity issue.

  Dependent Package:
    xos_tokenizer
    log4me

  Revision History:
  v1.0 20141106 CC - to correct account to multi-affinity programs
*/
-- ============================================================================
-- package spec
--
CREATE OR REPLACE PACKAGE EDW_OWNER.xbs_acct_affinity AS
  /*
  NAME               :-   F_DAILY_AFFINITY_PROGRAM_CHECK
  Original AUTHOUR   :-   VENU KANCHARLA
  DATE               :-   06/18/2013

  DECRIPTION         :-   
    PROCEURE TO CHECK IF A  PROGRAM HAS BEEN MODIFIED FROM 701 
    Affinity to Something else ..? This is happening on the classic due to 
    the plastic code changes for Affinity Programs. This Procedure would 
    identify suck propgrams and correct the data appropriately.

  /*
  NAME               :-   F_DAILY_AFFINITY_PROGRAM_CHECK
  Original AUTHOUR   :-   VENU KANCHARLA
  DATE               :-   06/18/2013

  DECRIPTION         :-   
    PROCEURE TO CHECK IF A  PROGRAM HAS BEEN MODIFIED FROM 701 
    Affinity to Something else ..? This is happening on the classic due to 
    the plastic code changes for Affinity Programs. This Procedure would 
    identify suck propgrams and correct the data appropriately.

  Revision History
  1.0 Venu Kancharla
  1.1 Venu Kancharla - Removed the Filter For Program Name So that it can 
                       handle all duplicate programs

  2.0 Venu Kancharla - added another Routine to handle the Duplicate Cards 
                       for Puchase Device Event
  2.1 20140604: CC   - add full hints to avoid the need for pre-run Stat 
                       on F_TRANSACTION_LINE_ITEM

  3.0 20141106: CC   - update to PROGRAM_NAME change; refactor code structure
  3.1 20141231: CC   - check both fact tables for duplicates, i.e.
                       F_TRANSACTION_LINE_ITEM and F_PURCHASE_DEVICE_EVENT.
  3.2 20150318: CC   - add v_old_pgm_key instead of hardcoded 701
  3.3 20150327: CC   - analyze partition at new month begin
  3.4 20150421: CC   - add optional date range input

  4.0 20150615: CC   - use bulk update
  */
  FUNCTION  dup_list(p_part varchar2)   RETURN KV_Maplist;
  FUNCTION  table_list(p_part varchar2) RETURN TokList;

  PROCEDURE check_one_month(p_proc varchar2, p_date date);
  PROCEDURE replace_account_program(
    p_proc    varchar2,
    p_acct    number,
    p_pgm_in  number,
    p_pgm_out number,
    p_part    varchar2 := 'YM_' || to_char(sysdate, 'YYYY_MM')
  );
  PROCEDURE replace_account_program(
    p_proc    varchar2,
    p_acct    number,
    p_pgm_in  number,
    p_pgm_out number,
    p_part    varchar2 := 'YM_' || to_char(sysdate, 'YYYY_MM')
  );
  PROCEDURE replace_account_program(
    p_map     KV_Maplist,
    p_part    varchar2 := 'YM_' || to_char(sysdate, 'YYYY_MM')
  );
END xbs_acct_affinity;
/
show errors

-- ============================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY xbs_acct_affinity AS
-- ----------------------------------------------------------------------------
-- scan throug F_TRANSACTION_LINE_ITEM to find conflicts i.e. account with 
-- multiple programs
--
PROCEDURE check_one_month(p_proc varchar2, p_date date) AS
  -- vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  --
  -- here are the default values that might need to be changed
  -- in the future depends on the slowly changing business rules
  --
  X_PGM_SPNR_NM  constant XBS.Var := '(''Affin 53'',''Affinity 54'')';
  X_OLD_PGM_KEY0 constant XBS.Int := 700; -- PROGRAM_KEY for 'Affinity 54'
  X_OLD_PGM_KEY1 constant XBS.Int := 701; -- PROGRAM_KEY for 'Affin 53'
  --
  -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  X_SQL XBS.Lob := '
  WITH
  dup1 AS (
    SELECT /*+ full(f) parallel(f, 64) use_hash(f, p) */ 
      UNIQUE f.ACCOUNT_KEY k, f.PROGRAM_KEY p, p.PROGRAM_SPONSOR_NAME n
    FROM F_PURCHASE_DEVICE_EVENT f
    JOIN D_PROGRAM p ON (p.PROGRAM_KEY = f.PROGRAM_KEY)
    WHERE f.ACCOUNT_KEY IN (
      SELECT ACCOUNT_KEY FROM (
        SELECT /*+ parallel(x, 64) full(x) */ 
          UNIQUE ACCOUNT_KEY, PROGRAM_KEY
        FROM   F_PURCHASE_DEVICE_EVENT x
      )
      GROUP BY ACCOUNT_KEY HAVING count(*) > 1
    )
  ),
  dup2 AS (
    SELECT /*+ full(f) parallel(f, 64) use_hash(f, p) */
      UNIQUE f.PURCHASE_ACCOUNT_KEY k, f.PROGRAM_KEY p, p.PROGRAM_SPONSOR_NAME n
    FROM F_TRANSACTION_LINE_ITEM PARTITION(#{PART}) f
    JOIN D_PROGRAM p ON (p.PROGRAM_KEY = f.PROGRAM_KEY)
    WHERE f.PURCHASE_ACCOUNT_KEY IN ( 
      SELECT PURCHASE_ACCOUNT_KEY FROM (
        SELECT /*+ parallel(x,64) full(x) */
           UNIQUE PURCHASE_ACCOUNT_KEY, PROGRAM_KEY
        FROM F_TRANSACTION_LINE_ITEM PARTITION(#{PART}) x
      )
      GROUP BY PURCHASE_ACCOUNT_KEY HAVING count(*) > 1
    )
  ),
  dup_all AS (
    SELECT * FROM dup1
    UNION
    SELECT * FROM dup2
  ),
  map AS (
    SELECT 
      k, 
      max(CASE WHEN n IN (' || X_PGM_SPNR_NM || ') THEN p ELSE 0 END) f,
      max(CASE WHEN n IN (' || X_PGM_SPNR_NM || ') THEN 0 ELSE p END) t
    FROM dup_all
    GRUOP BY k;
  )
  SELECT * FROM map
  ';
BEGIN
  log4me.info('*** INIT ' || X_PRC, X_SES_ID);
  --
  -- find the processing data from the seed table
  --
  xos_session_mgr.get_date_range(
    p_bgn_dt,
    p_end_dt,
    'EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM',
    v_min_dt,
    v_max_dt
  );
  IF (v_min_dt IS NULL OR v_max_dt IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20400, 'no date range given');
  END IF;
  --
  -- Create cursor for Looping thru if we find multiple accounts for Affinity Changes ...
  --
  v_part := 'YM_' || to_char(v_min_dt, 'YYYY_MM');
  v_sql  := replace(X_SQL, '#{PART}', v_part);
  --
  -- The following Cursor is to identify the Duplicate programs occuriing on the Revenue Side
  --
  log4me.debug('starting ' || X_PRC, v_sql, X_SES_ID);
  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_lst;
  log4me.info(X_PRC || ' ' || SQL%ROWCOUNT || ' dup accounts found', X_SES_ID);
  --
  --
  -- Create cursor for Looping thru if we find multiple accounts for Affinity Changes ...
  --
  IF v_lst.COUNT > 0 THEN
    replace_account_program(v_lst, p_part);
  END IF;

EXCEPTION WHEN OTHERS THEN
  log4me.debug(X_PRC, v_sql);
  RAISE;
END check_one_month;

-- ----------------------------------------------------------------------------
-- fix program for a given account in all account/program related tables
--
PROCEDURE replace_account_program(
  p_map     KV_Maplist,
  p_part    varchar2 := 'YM_' || to_char(sysdate-1, 'YYYY_MM')
)
AS
  X_PRC  constant XBS.Var := 'replace_account_program';
  X_PART constant XBS.Var := 'PARTITION (' || p_part || ')';
  X_TBL  constant XBS_Array := XBS_Array(
    'f_transaction_line_item  ' || X_PART,
    'f_daily_aging_snapshot   ' || X_PART,
    'f_daily_revenue_snapshot ' || X_PART,
    'f_daily_account_snapshot ' || X_PART,
    'f_monthly_account_snapshot',
    'f_monthly_revenue_snapshot',
    'f_accounts_receivable_entry',
    'f_purchase_device_event',
    'f_account_naics_sic_event'
  );
  X_SQL constant XBS.Str := '
    MERGE /*+ parallel(x, 64) append */ INTO #{TABLE} x
    USING TABLE(:m) r
    ON (x.purchase_account_key = r.key)
    WHEN MATCHED THEN UPDATE SET
      program_key          = r.to_val,
      row_last_mod_proc_nm = ''' || X_PRC || ''',
      row_last_mod_dttm    = sysdate
    WHERE program_key = r.from_val
  ';  
  v_tli_sql constant XBS.Str := '
    MERGE /*+ parallel(x, 64) append */ INTO f_transaction_line_item ' || v_part || ' x
  ';
  v_sql XBS.Str;
 
BEGIN
  IF p_map.COUNT <= 0 THEN RETURN; END IF;

  log4me.debug(p_proc || ' cleanup start', '[PKEY:' || p_pgm_out || ', AKEY:' || p_acct ||']');
  --
  -- data fix for affinity program, account association
  --
  FOR i IN 1..X_TBL.COUNT LOOP
    v_sql := replace(X_SQL, '#{TABLE}', X_TBL(i));
    BEGIN
      EXECUTE IMMEDIATE v_sql USING v_map;
      v_cnt := SQL%ROWCOUNT;
      COMMIT;
      log4me.debug(X_PRC || ' ' || v_cnt || ' rows merged', v_sql);
    EXCEPTION WHEN OTHERS THEN
      log4me.debug(X_PRC || ' failed', v_sql);
    END;      
  END LOOP;
  log4me.debug(X_PRC || ' clean up done', '');
END replace_account_program;

END xbs_acct_affinity;
/
show errors
-- ============================================================================

CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_AFFINITY_PROGRAM_CHECK
AS
BEGIN
  xbs_acct_affinity.check_account_affinity;
END;
/
show errors

GRANT EXECUTE ON EDW_OWNER.F_DAILY_AFFINITY_PROGRAM_CHECK TO DWLOADER;

