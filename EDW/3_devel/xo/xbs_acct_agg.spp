ALTER SESSION SET PLSQL_CCFLAGS = 'VCARD:false';

CREATE OR REPLACE PACKAGE xbs_acct_agg AS
/*
Name: xbs_acct_agg - account aggregates with max_card_count and attrition_size

Desc: account aggregates into 
        1. F_DAILY_ACCOUNT_AGGREGATES
        2. F_MONTHLY_ACCOUNT_AGGREGATES

Note1:
  Added BY VK  on 05/24/2011 
  to elimate the Card counts for Future Dates for re-issues
  during historic  Data processing

Note2:
  added By VK on 09/26/2013 per Bill Kelley to remove these Card types

Revision:
  1.0 20100615 Izzy Reinish 
               Init version.
  2.0 20101011 Venugopal Kancharla
               Added the F_SEED_DATES procedure to autoamte the history loading
               and added the indexes Drop and Recreate.
  2.1 20101212 Venu KAncharla
               Adding the Filter in the where Clause.
  2.2 20110117 VK
               DW_Current_FLG = '1', commented since this is not needed.
  2.3 20110331 VK
               Added the new Seed Dates to the PROD D_SEED_DATES.
  2.4 20110524 VK
               Elimate the Card counts for Future Dates for re-issues durinng
               historic Data processing so that the PROC can be made Rerunable
  2.5 20130725 VK
               Code to Calculate MAX_NUMBER_OF_OUTRSTANDING CARDS.
               This is being added to imporve performance of BO queries.
  2.6 20130906 VK
               Changes for the  Fleet Size Logic based on the Business Inputs.
               Adding the Account_hist_key.
  2.7 20130926 VK
               Added Moved and Converted Filters apart from ther existing 
               Terminated Filters to move these Card status from the 
               outstanding Card Count.
  2.8 20140515 VK
               Added extra routine to max count to account for any account 
               where aCCOUNT_KEY <> aCCOUNT_HIST_KEY
  2.9 20140710 VK
               Fixed the logic for Max Outstanding Card Count
  3.0 20150401 Lee - Modulization, add attrition fleet size, and rerun logic
  3.1 20150615 Lee - Add attrition size to D_ACCOUNT
-- ============================================================================
*/
  FMT_DT    constant XBS.Var := 'YYYY-MM-DD';

  G_PRC     constant XBS.Var := 'D_ACCT_AGG';
  G_SEED_TB constant XBS.Var := 'EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM';
  G_DLY_TB  constant XBS.Var := 'EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT';
  G_MLY_TB  constant XBS.Var := 'EDW_OWNER.F_MONTHLY_ACCOUNT_SNAPSHOT';
  --
  -- public methods
  --
  PROCEDURE run_one_session (
    p_bgn_dt date    := NULL, 
    p_end_dt date    := NULL, 
    p_ses_id varchar2:= NULL
  );
  --
  -- private methods
  --
  FUNCTION  get_date_key(p_date date)                 RETURN integer;
  FUNCTION  get_month_key(p_dkey integer)             RETURN integer;
  FUNCTION  prc_str(p_name varchar2, p_date date)     RETURN varchar2;

  PROCEDURE purge(p_date date,                   p_ses_id varchar2 := NULL);
  PROCEDURE run_daily(p_date date,               p_ses_id varchar2 := NULL);
  PROCEDURE fix_max_card_count(p_date date,      p_ses_id varchar2 := NULL);
  PROCEDURE fix_max_attrition_count(p_date date, p_ses_id varchar2 := NULL);

  PROCEDURE rerun(p_date date, p_ses_id varchar2 := NULL);  -- this is untested
END xbs_acct_agg;
/
show errors

-- ============================================================================
CREATE OR REPLACE PACKAGE BODY xbs_acct_agg AS
-- ----------------------------------------------------------------------------
-- public methods
--
PROCEDURE run_one_session(
  p_bgn_dt date     := NULL, 
  p_end_dt date     := NULL, 
  p_ses_id varchar2 := NULL
) IS
  v_min_dt XBS.Dt;
  v_max_dt XBS.Dt;
  v_date   XBS.Dt;
  v_prc    XBS.Var;

BEGIN
  log4me.info(G_PRC || ' INIT ', p_ses_id);
  xos_session_mgr.get_date_range(
    p_bgn_dt,
    p_end_dt,
    G_SEED_TB,
    v_min_dt,
    v_max_dt
  );
  IF (v_min_dt IS NULL OR v_max_dt IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20400, 'no date range given');
  END IF;

  v_prc := G_PRC || '[' ||
    to_char(v_min_dt, FMT_DT) || ',' ||
    to_char(v_max_dt, FMT_DT) ||
    ']';
  --
  -- day-by-day walk through given day range
  --
  FOR rec IN(
    SELECT 
      DATE_KEY         AS key,
      CALENDAR_DATE_DT AS dt
    FROM D_DATE
    WHERE CALENDAR_DATE_DT BETWEEN v_min_dt AND v_max_dt
    ORDER BY DATE_KEY
  )
  LOOP
    v_date := rec.dt;
    --
    -- plow through one session (a day)
    --
    purge(v_date, p_ses_id);
    run_daily(v_date, p_ses_id);
    fix_max_card_count(v_date, p_ses_id);
    fix_max_attrition_count(v_date, p_ses_id);
  END LOOP;
  log4me.info(G_PRC || ' DONE ', p_ses_id);

EXCEPTION WHEN OTHERS THEN
  log4me.err(G_PRC || ' FAIL', p_ses_id);
  RAISE;
END run_one_session;

-- ============================================================================
-- private methods
--
FUNCTION get_date_key(p_date date) RETURN integer IS
  v_dk XBS.Int;
BEGIN
  SELECT DATE_KEY INTO v_dk FROM D_DATE
  WHERE CALENDAR_DATE_DT = p_date;
  RETURN v_dk;
END get_date_key;

-- ----------------------------------------------------------------------------
FUNCTION get_month_key(p_dkey integer) RETURN integer IS
  v_dk XBS.Int;
BEGIN
  SELECT DATE_KEY INTO v_dk FROM D_DATE
  WHERE CALENDAR_DATE_DT = (
    SELECT LAST_DAY_IN_MONTH_DATE_DT FROM D_DATE
    WHERE DATE_KEY = p_dkey
  );
  RETURN v_dk;
END get_month_key;

-- ----------------------------------------------------------------------------
FUNCTION prc_str(p_name varchar2, p_date date) RETURN varchar2 IS
BEGIN
  RETURN G_PRC || '#' || p_name || '(' || to_char(p_date, FMT_DT) || ')';
END prc_str;

-- ----------------------------------------------------------------------------
PROCEDURE purge(p_date date, p_ses_id varchar2 := NULL) IS 
  X_PRC constant XBS.Var := prc_str('purge', p_date);
  X_DK  constant XBS.Int := get_date_key(p_date);
  X_TB  constant XBS.Var := 
    G_DLY_TB || ' PARTITION (YM_' || to_char(p_date, 'YYYY_MM') || ')';
  X_SQL constant XBS.Lob := '
    DELETE /*+ parallel */ FROM ' || X_TB || '
    WHERE DATE_KEY = ' || X_DK || '
  ';
BEGIN
  EXECUTE IMMEDIATE X_SQL;
  log4me.debug(
    X_PRC || ' => ' || SQL%ROWCOUNT || ' rows deleted', X_SQL, p_ses_id);
  COMMIT;

EXCEPTION WHEN OTHERS THEN
  log4me.err(X_PRC || ' FAIL', p_ses_id);
  log4me.debug(X_PRC || ' failed', X_SQL, p_ses_id);
  RAISE;
END purge;

-- ----------------------------------------------------------------------------
PROCEDURE run_daily(p_date date, p_ses_id varchar2 := NULL) IS
  X_PRC  constant XBS.Var := prc_str('run_daily', p_date);
  X_DK   constant XBS.Int := get_date_key(p_date);
BEGIN
  log4me.debug(X_PRC, '', p_ses_id);

  INSERT /*+ append */ INTO F_DAILY_ACCOUNT_SNAPSHOT (
    DAILY_ACCOUNT_SNAPSHOT_KEY,
    ACCOUNT_HIST_KEY,
    ACCOUNT_KEY,
    PROGRAM_KEY,
    DATE_KEY,
    REGION_KEY,
    /* up-to-date daily cards snapshot */
    OUTSTANDING_CARDS_COUNT,
    TERMINAL_CARD_COUNT,
    TOTAL_CARDS_COUNT,
    CARDS_IN_TRANSITION_COUNT,
    VIRTUAL_OUTSTANDING_CARD_COUNT,
    VIRTUAL_TERMINAL_CARD_COUNT,
    VIRTUAL_TOTAL_CARD_COUNT,
    VIRTUAL_CARDS_IN_TRANS_COUNT,
    MAG_OUTSTANDING_CARD_COUNT,
    MAG_TERMINAL_CARD_COUNT,
    MAG_TOTAL_CARD_COUNT,
    MAG_CARDS_IN_TRANS_COUNT,
    /* daily card  transactions */
    ISSUED_CARDS_COUNT,
    TERMINATED_CARDS_COUNT,
    TRANS_CARDS_COUNT,
    VIRTUAL_ISSUED_CARDS_COUNT,
    VIRTUAL_TERMINATED_CARDS_COUNT,
    VIRTUAL_TRANS_CARDS_COUNT,
    MAG_ISSUED_CARDS_COUNT,
    MAG_TERMINATED_CARDS_COUNT,
    MAG_TRANS_CARD_COUNT,
    /* bookkeeping fields */
    ROW_CREATE_DTTM,
    ROW_LAST_MOD_DTTM,
    ROW_LAST_MOD_PROC_NM,
    ROW_LAST_MOD_PROC_SEQ_NBR,
    /* extra measures */
    MAX_OUTSTANDING_CARDS_COUNT,
    MAX_ATTRITION_CARDS_COUNT
  )
  SELECT 
    daily_account_snapshot_key_seq.nextval AS DAILY_ACCOUNT_SNAPSHOT_KEY,
    ACCOUNT_HIST_KEY,
    ACCOUNT_KEY,
    PROGRAM_KEY,
    X_DK        AS DATE_KEY,
    0           AS REGION_KEY,
    /* up-to-date daily cards snapshot */
    tot_card    AS OUTSTANDING_CARD_COUNT,
    tot_cx      AS TERMINAL_CARD_COUNT,
    tot_ci      AS TOTAL_CARDS_COUNT,
    tot_ct      AS CARDS_IN_TRANSITION_COUNT,
    tot_vcard   AS VIRTUAL_OUTSTANDING_CARD_COUNT,
    tot_vx      AS VIRTUAL_TERMINAL_CARD_COUNT,
    tot_vi      AS VIRTUAL_TOTAL_CARD_COUNT,
    tot_vt      AS VIRTUAL_CARDS_IN_TRANS_COUNT,
    tot_mcard   AS MAG_OUTSTANDING_CARD_COUNT,
    tot_mx      AS MAG_TERMINAL_CARD_COUNT,
    tot_mi      AS MAGSTRIPE_DEVICE_COUNT,
    tot_mt      AS MAG_CARDS_IN_TRANS_COUNT,
    /* daily card  transactions */
    cnt_ci      AS ISSUED_CARDS_COUNT,
    cnt_cx      AS TERMINATED_CARDS_COUNT,
    cnt_ct      AS TRANS_CARDS_COUNT,
    cnt_vi      AS VIRTUAL_ISSUED_CARDS_COUNT,
    cnt_vx      AS VIRTUAL_TERMINATED_CARDS_COUNT,
    cnt_vt      AS VIRTUAL_TRANS_CARDS_COUNT,
    cnt_mi      AS MAG_ISSUED_CARDS_COUNT,
    cnt_mx      AS MAG_TERMINATED_CARDS_COUNT,
    cnt_mt      AS MAG_TRANS_CARD_COUNT,
    /* bookkeeping fields */
    sysdate     AS ROW_CREATE_DTTM,
    sysdate     AS ROW_LAST_MOD_DTTM,
    X_PRC       AS ROW_LAST_MOD_PROC_NM,
    1           AS ROW_LAST_MOD_PROC_SEQ_NBR,
    /* extra measures */
    0           AS MAX_OUTSTANDING_CARDS_COUNT,
    0           AS MAX_ATTRITION_CARDS_COUNT
  FROM (
    SELECT
      a.ACCOUNT_HIST_KEY,
      a.ACCOUNT_KEY,
      pde.PROGRAM_KEY,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN pd.PURCHASE_DEVICE_COUNT1 ELSE 0 END)
      - sum(
        CASE
        WHEN pd.CARD_STATUS_DESC IN ( 'Terminated' , 'Moved' , 'Converted')
          AND pd.TERMINATED_DATE <= p_date /* Note1 */
        THEN pd.TERMINATED_COUNT1 ELSE 0 END
      )                                                  AS tot_card,
      sum(
        CASE 
        WHEN pd.CARD_STATUS_DESC = 'Terminated' 
          AND pd.TERMINATED_DATE <= p_date
        THEN pd.TERMINATED_COUNT1 ELSE 0 END)            AS tot_cx,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date 
          AND pd.ACTIVATED_DATE <= p_date /* Note1*/
        THEN pd.PURCHASE_DEVICE_COUNT1 ELSE 0 END)       AS tot_ci,
      sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'In Transition'
          AND pd.IN_TRANSITION_DATE <= p_date
        THEN pd.IN_TRANSITION_COUNT1 ELSE 0 END)         AS tot_ct,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN pd.PURCHASE_DEVICE_COUNT1 ELSE 0 END)
      - sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'Terminated'
          AND pd.TERMINATED_DATE <= p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS tot_vcard,
      sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'Terminated'
          AND pd.TERMINATED_DATE <= p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS tot_vx,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS tot_vi,
      sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'In Transition'
          AND pd.IN_TRANSITION_DATE <= p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS tot_vt,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN pd.PURCHASE_DEVICE_COUNT1 ELSE 0 END)
      - sum( 
        CASE
        WHEN pd.CARD_STATUS_DESC = 'Terminated'
          AND pd.TERMINATED_DATE <= p_date
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS tot_mcard,
      sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'Terminated'
          AND pd.TERMINATED_DATE <= p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS tot_mx,
      sum(
        CASE
        WHEN pd.ISSUED_DATE <= p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS tot_mi,
      sum(
        CASE
        WHEN pd.CARD_STATUS_DESC = 'In Transition'
          AND pd.IN_TRANSITION_DATE <= p_date
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS tot_mt,
      /* daily card  transactions */
      sum(
        CASE
        WHEN pd.ISSUED_DATE = p_date
          AND pd.ACTIVATED_DATE <= p_date /* Note1 */
        THEN 1 ELSE 0 END)                               AS cnt_ci,
      sum(
        CASE
        WHEN pd.TERMINATED_DATE = p_date
        THEN 1 ELSE 0 END)                               AS cnt_cx,
      sum(
        CASE
        WHEN pd.IN_TRANSITION_DATE = p_date
        THEN 1 ELSE 0 END)                               AS cnt_ct,
      sum(
        CASE
        WHEN pd.ISSUED_DATE = p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS cnt_vi,
      sum(
        CASE
        WHEN pd.TERMINATED_DATE = p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS cnt_vx,
      sum(
        CASE
        WHEN pd.IN_TRANSITION_DATE = p_date
        THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)        AS cnt_vt,
      sum(
        CASE 
        WHEN pd.ISSUED_DATE = p_date
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS cnt_mi,
      sum(
        CASE 
        WHEN pd.TERMINATED_DATE = p_date
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS cnt_mx,
      sum(
        CASE
        WHEN pd.IN_TRANSITION_DATE = p_date
        THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END)      AS cnt_mt
    FROM D_ACCOUNT a
    JOIN (
      --
      -- get only the latest account/program pair
      --
      SELECT /*+ parallel(x, 64) */
        ACCOUNT_KEY,
        max(PROGRAM_KEY) KEEP (
          DENSE_RANK LAST ORDER BY ROW_LAST_MOD_DTTM
        ) OVER (PARTITION BY ACCOUNT_KEY) PROGRAM_KEY,
        PURCHASE_DEVICE_KEY
      FROM F_PURCHASE_DEVICE_EVENT x
    ) pde ON (a.ACCOUNT_KEY = pde.ACCOUNT_KEY)
    JOIN D_PURCHASE_DEVICE pd ON (
      pd.PURCHASE_DEVICE_KEY = pde.PURCHASE_DEVICE_KEY)
    WHERE ACCOUNT_CLOSED_DATE >= p_date
    AND   ACCOUNT_OPEN_DATE   <= p_date 
    /* AND a.CURRENT_RECORD_FLG = '1' -- Note2 */
    GROUP BY a.ACCOUNT_KEY, pde.PROGRAM_KEY , a.ACCOUNT_HIST_KEY
  );
  log4me.debug(X_PRC || ' => ' || SQL%ROWCOUNT || ' rows', '', p_ses_id);
  COMMIT;

EXCEPTION WHEN OTHERS THEN
  log4me.err(X_PRC || ' FAIL', p_ses_id);
  RAISE;
END run_daily;

-- ----------------------------------------------------------------------------
-- new logic that enable rerun
-- 
PROCEDURE rerun(p_date date, p_ses_id varchar2 := NULL) IS
  X_PRC  constant varchar2(200) := prc_str('rerun', p_date);
  X_DK   constant integer       := get_date_key(p_date); 
BEGIN
  log4me.debug(X_PRC, '', p_ses_id);

  INSERT INTO F_DAILY_ACCOUNT_SNAPSHOT (
    DAILY_ACCOUNT_SNAPSHOT_KEY,
    ACCOUNT_KEY,
    PROGRAM_KEY,
    DATE_KEY,
    REGION_KEY,
    /* up-to-date daily cards snapshot */
    OUTSTANDING_CARDS_COUNT,
    TERMINAL_CARD_COUNT,
    TOTAL_CARDS_COUNT,
    CARDS_IN_TRANSITION_COUNT,
    VIRTUAL_OUTSTANDING_CARD_COUNT,
    VIRTUAL_TERMINAL_CARD_COUNT,
    VIRTUAL_TOTAL_CARD_COUNT,
    VIRTUAL_CARDS_IN_TRANS_COUNT,
    MAG_OUTSTANDING_CARD_COUNT,
    MAG_TERMINAL_CARD_COUNT,
    MAG_TOTAL_CARD_COUNT,
    MAG_CARDS_IN_TRANS_COUNT,
    /* daily card  transactions */
    ISSUED_CARDS_COUNT,
    TERMINATED_CARDS_COUNT,
    TRANS_CARDS_COUNT,
    VIRTUAL_ISSUED_CARDS_COUNT,
    VIRTUAL_TERMINATED_CARDS_COUNT,
    VIRTUAL_TRANS_CARDS_COUNT,
    MAG_ISSUED_CARDS_COUNT,
    MAG_TERMINATED_CARDS_COUNT,
    MAG_TRANS_CARD_COUNT,
    /* bookkeeping fields */
    ROW_CREATE_DTTM,
    ROW_LAST_MOD_DTTM,
    ROW_LAST_MOD_PROC_NM,
    ROW_LAST_MOD_PROC_SEQ_NBR,
    /* extra(new) fields */
    ACCOUNT_HIST_KEY,
    MAX_OUTSTANDING_CARDS_COUNT,
    MAX_ATTRITION_CARDS_COUNT
  )
  WITH
  ac AS (
    SELECT /*+ parallel(a, 64) */
      a.ACCOUNT_HIST_KEY, 
      a.ACCOUNT_KEY,
      CASE /* current or future */
        WHEN a.ACCOUNT_OPEN_DATE   > p_date THEN 'F' ELSE 'C' END || 
      CASE /* active, attrition, or closed */
        WHEN a.ACCOUNT_CLOSED_DATE > p_date THEN 'A'
        WHEN (p_date - a.ACCOUNT_CLOSED_DATE) < 365 THEN 'T'
        ELSE 'X' 
      END status
    FROM D_ACCOUNT a
    WHERE a.ACCOUNT_OPEN_DATE <= p_date            /* current account */
  ),
  ax AS (
    --
    -- get only the latest program_key for the same account_hist_key
    --
    SELECT /*+ use_hash(pde, ac) */
      ac.status,
      ac.ACCOUNT_KEY,
      ac.ACCOUNT_HIST_KEY,
      max(PROGRAM_KEY) KEEP (
        DENSE_RANK LAST ORDER BY pde.ROW_LAST_MOD_DTTM
      ) OVER (PARTITION BY pde.ACCOUNT_KEY) PROGRAM_KEY,
      pde.PURCHASE_DEVICE_KEY
    FROM ac 
    JOIN f_purchase_device_event pde ON (ac.ACCOUNT_KEY = pde.ACCOUNT_KEY)
    AND  pde.DATE_KEY <= X_DK
  ),
  pd AS (
    SELECT 
    sign(trunc(p_date) - trunc(CARD_STATUS_DATE)) AS dif,
    /*
     --
     -- Venu's rule
     --
    CASE 
      WHEN MAGSTRIPE_DEVICE_COUNT1>0 THEN 'M'
      WHEN VIRTUAL_DEVICE_COUNT1  >0 THEN 'V'
      WHEN PURCHASE_DEVICE_COUNT1 >0 THEN 'C'
      ELSE '-'
    END ||
    CASE 
      WHEN CARD_STATUS_DESC IN ('Terminated', 'Moved', 'Converted') 
                                   AND TERMINATED_DATE    <= p_date THEN 'X'
      WHEN CARD_STATUS_DESC IN ('Suspended') 
                                   AND SUSPENDED_DATE     <= p_date THEN 'S'
      WHEN CARD_STATUS_DESC IN ('Activated') 
                                   AND ACTIVATED_DATE     <= p_date THEN 'A'
      WHEN CARD_STATUS_DESC IN ('In Transition')
                                   AND IN_TRANSITION_DATE <= p_date THEN 'T'
      WHEN ISSUED_DATE <= p_date                                    THEN 'I'
      ELSE '-'
    END AS opc,
    */
    CASE 
      WHEN PURCHASE_DEVICE_COUNT1 >0 THEN 'C'
      WHEN VIRTUAL_DEVICE_COUNT1  >0 THEN 'V'
      WHEN MAGSTRIPE_DEVICE_COUNT1>0 THEN 'M'
      ELSE '-'
    END || 
    CASE 
      WHEN TERMINATED_DATE    <= p_date THEN 'X'
      WHEN SUSPENDED_DATE     <= p_date THEN 'S'
      WHEN ACTIVATED_DATE     <= p_date THEN 'A'
      WHEN IN_TRANSITION_DATE <= p_date THEN 'T'
      WHEN ISSUED_DATE        <= p_date THEN 'I'
      ELSE '-'
    END AS opc,
    x.*
    FROM D_PURCHASE_DEVICE x
    WHERE CARD_STATUS_DATE <= p_date
  ),
  snap AS (
    SELECT
      ax.ACCOUNT_HIST_KEY,
      ax.ACCOUNT_KEY,
      ax.PROGRAM_KEY,
      /* all card snapshot count */
      sum(decode(pd.opc, 'CA',1, 'CS',1, 0))                      AS tot_ci,
      sum(decode(pd.opc, 'CT',1,         0))                      AS tot_ct,
      sum(decode(pd.opc, 'CX',1,         0))                      AS tot_cx,
$IF $$VCARD $THEN
      /* total counts */
      sum(decode(pd.opc, 'VA',1, 'VS',1, 0))                      AS tot_vi,
      sum(decode(pd.opc, 'VT',1,         0))                      AS tot_vt,
      sum(decode(pd.opc, 'VX',1,         0))                      AS tot_vx,
      sum(decode(pd.opc, 'MA',1, 'MS',1, 0))                      AS tot_mi,
      sum(decode(pd.opc, 'MT',1,         0))                      AS tot_mt,
      sum(decode(pd.opc, 'MX',1,         0))                      AS tot_mx,
      /* daily card  transactions */
      sum(decode(pd.dif, 0,decode(pd.opc, 'CA',1, 'CS',1, 0), 0)) AS cnt_ci,
      sum(decode(pd.dif, 0,decode(pd.opc, 'CT',1,         0), 0)) AS cnt_ct,
      sum(decode(pd.dif, 0,decode(pd.opc, 'CX',1,         0), 0)) AS cnt_cx,
      /* daily virtual card  transactions */
      sum(decode(pd.dif, 0,decode(pd.opc, 'VA',1, 'VS',1, 0), 0)) AS cnt_vi,
      sum(decode(pd.dif, 0,decode(pd.opc, 'VT',1,         0), 0)) AS cnt_vt,
      sum(decode(pd.dif, 0,decode(pd.opc, 'VX',1,         0), 0)) AS cnt_vx,
      /* daily mag card  transactions */
      sum(decode(pd.dif, 0,decode(pd.opc, 'MA',1, 'MS',1, 0), 0)) AS cnt_mi,
      sum(decode(pd.dif, 0,decode(pd.opc, 'MT',1,         0), 0)) AS cnt_mt,
      sum(decode(pd.dif, 0,decode(pd.opc, 'MX',1,         0), 0)) AS cnt_mx
$ELSE
      /* total counts */
      0 tot_vi, 0 tot_vt, 0 tot_vx, 0 tot_mi, 0 tot_mt, 0 tot_mx,
      /* daily card  transactions */
      sum(decode(pd.dif, 0,decode(pd.opc, 'CA',1, 'CS',1, 0), 0)) AS cnt_ci,
      sum(decode(pd.dif, 0,decode(pd.opc, 'CT',1,         0), 0)) AS cnt_ct,
      sum(decode(pd.dif, 0,decode(pd.opc, 'CX',1,         0), 0)) AS cnt_cx,
      /* daily vcard and mag card  transactions */
      0 cnt_vi, 0 cnt_vt, 0 cnt_vx, 0 cnt_mi, 0 cnt_mt, 0 cnt_mx
$END
    FROM ax
    JOIN pd ON (pd.PURCHASE_DEVICE_KEY = ax.PURCHASE_DEVICE_KEY)
    GROUP BY
      ax.ACCOUNT_HIST_KEY,
      ax.ACCOUNT_KEY, 
      ax.PROGRAM_KEY
  ),
  snap_mx AS (
    SELECT 
      snap.*,
      min(ACCOUNT_KEY)   OVER (PARTITION BY ACCOUNT_HIST_KEY) AS min_key,
      max(tot_ci-tot_cx) OVER (PARTITION BY ACCOUNT_HIST_KEY) AS MAX_OUTSTANDING_CARDS_COUNT
    FROM snap    
  )
  SELECT
    daily_account_snapshot_key_seq.nextval AS daily_account_snapshot_key,
    ACCOUNT_KEY,
    PROGRAM_KEY,
    X_DK             AS DATE_KEY,
    0                AS REGION_KEY,
    /* snapshot cards counts */
    tot_ci - tot_cx  AS OUTSTANDING_CARD_COUNT,
    tot_cx           AS TOTAL_TERMINAL_CARD_COUNT,
    tot_ci           AS TOTAL_CARD_COUNT,
    tot_ct           AS CARD_IN_TRANS_COUNT, 
    /* snapshot virtual cards */
    tot_ci - tot_vx  AS VIRTUAL_OUTSTANDING_CARD_COUNT,
    tot_vx           AS VIRTUAL_TERMINAL_CARC_COUNT,
    tot_vi           AS VIRTUAL_TOTAL_CARD_COUNT,
    tot_vt           AS VIRTUAL_CARD_IN_TRANS_COUNT,
    /* snapshot mag cards */
    tot_ci - tot_mx  AS MAG_OUTSTANDING_CARD_COUNT,
    tot_mx           AS MAG_TERMINAL_CARD_COUNT,
    tot_mi           AS MAG_TOTAL_CARD_COUNT,
    tot_mt           AS MAG_CARD_IN_TRANS_COUNT,
    /* daily card transactions */
    cnt_ci           AS ISSUED_CARDS_COUNT,
    cnt_cx           AS TERMINATED_CARDS_COUNT,
    cnt_ct           AS TRANS_CARDS_COUNT,
    /* daily virtual cards */
    cnt_vi           AS VIRTUAL_ISSUED_CARDS_COUNT,
    cnt_vx           AS VIRTUAL_TERMINATED_CARDS_COUNT,
    cnt_vt           AS VIRTUAL_TRANS_CARDS_COUNT,
    /* daily mag cards */
    cnt_mi           AS MAG_ISSUED_CARDS_COUNT,
    cnt_mx           AS MAG_TERMINATED_CARDS_COUNT,
    cnt_mt           AS MAG_TRANS_CARD_COUNT,
    /* bookkeeping fields */
    sysdate          AS ROW_CREATE_DTTM,
    sysdate          AS ROW_LAST_MOD_DTTM,
    X_PRC            AS ROW_LAST_MOD_PROC_NM,
    1                AS ROW_LAST_MOD_PROC_SEQ_NBR,
    /* extra(new) fields */
    ACCOUNT_HIST_KEY,
    CASE WHEN ACCOUNT_KEY=min_key 
      THEN MAX_OUTSTANDING_CARDS_COUNT  ELSE 0 
    END              AS MAX_OUTSTANDING_CARDS_COUNT,
    0                AS MAX_ATTRITION_CARDS_COUNT
  FROM snap_mx;
  log4me.debug(X_PRC || ' => ' || SQL%ROWCOUNT || ' rows', '', p_ses_id);
  COMMIT;

EXCEPTION WHEN OTHERS THEN
  log4me.err(X_PRC || ' FAIL', p_ses_id);
  RAISE;
END rerun;

-- ----------------------------------------------------------------------------
PROCEDURE fix_max_card_count(p_date date, p_ses_id varchar2 := NULL) IS
  X_PRC constant XBS.Var := prc_str('max_card_count', p_date);
  X_DK  constant XBS.Int := get_date_key(p_date);
  X_TB  constant XBS.Var := 
    G_DLY_TB || ' PARTITION (YM_' || to_char(p_date, 'YYYY_MM') || ')';

  -- Actual Code to find the MAX Number of outstanding CARDCOUNT,
  -- From the starting of the month until that day
  -- Note Max number outstnading card Count is updated 
  -- where account_key = minimum account_key number

  X_SQL  constant XBS.Lob := '
  MERGE INTO ' || X_TB || ' fdas
  USING (
    SELECT    
      f1.ACCOUNT_HIST_KEY,
      MIN(f2.MIN_ACCOUNT_KEY)          MIN_ACCOUNT_KEY,
      MAX(term)                        max_term,
      MAX(tot)                         max_tot
    FROM (
      SELECT  
        f.ACCOUNT_HIST_KEY,
        sum(f.TERMINATED_CARDS_COUNT)  term,
        sum(f.OUTSTANDING_CARDS_COUNT) tot
      FROM ' || X_TB || ' f
      WHERE F.DATE_KEY <= :dk
      GROUP BY   
        f.ACCOUNT_HIST_KEY,
        f.DATE_KEY
    ) f1, (
      SELECT  
        min(ACCOUNT_KEY)               MIN_ACCOUNT_KEY,
        ACCOUNT_HIST_KEY  
      FROM ' || X_TB || ' f
      WHERE DATE_KEY = :dk
      GROUP BY 
        ACCOUNT_HIST_KEY,
        DATE_KEY
    ) f2
    WHERE f2.ACCOUNT_HIST_KEY = f1.ACCOUNT_HIST_KEY
    GROUP BY f1.ACCOUNT_HIST_KEY
  ) a ON (fdas.ACCOUNT_KEY = a.MIN_ACCOUNT_KEY)
  WHEN MATCHED THEN UPDATE SET
--  fdas.MAX_TERMINATED_CARDS_COUNT  = a.max_term,
    fdas.MAX_OUTSTANDING_CARDS_COUNT = a.max_tot
  WHERE fdas.DATE_KEY = :dk
  ';

BEGIN
  EXECUTE IMMEDIATE X_SQL USING X_DK, X_DK, X_DK;
  log4me.debug(X_PRC || ' => ' || SQL%ROWCOUNT || ' rows', X_SQL, p_ses_id);
  COMMIT;

EXCEPTION WHEN OTHERS THEN
  log4me.debug(X_PRC || ' failed', X_SQL, p_ses_id);
  log4me.err(X_PRC || ' FAILED', p_ses_id);
  RAISE;
END fix_max_card_count;

-- ----------------------------------------------------------------------------
-- 1. find all accounts that were closed in the past 1 month
--      for a given day within the past 13 months,
--        rollup the total outstanding card count with the same hist_key 
-- 2. find the max(account_key) associated with a given hist_key
--      and assign the max_outstanding_card_count to it
--
PROCEDURE fix_max_attrition_count(p_date date, p_ses_id varchar2 := NULL) IS
  X_PRC  constant XBS.Var := prc_str('max_attrition_count', p_date);
  X_TB   constant XBS.Var := 
    G_DLY_TB || ' PARTITION (YM_' || to_char(p_date, 'YYYY_MM') || ')';
  X_SQL  constant XBS.Lob := '
    WITH 
    ua AS (
      SELECT ACCOUNT_HIST_KEY, ACCOUNT_CLOSED_DATE FROM (
        SELECT 
          ACCOUNT_HIST_KEY,
          ACCOUNT_CLOSED_DATE,
          row_number() OVER (
            PARTITION BY ACCOUNT_HIST_KEY ORDER BY ACCOUNT_CLOSED_DATE
          ) rn
        FROM D_ACCOUNT
        WHERE ACCOUNT_CLOSED_DATE BETWEEN trunc(:dt,''month'') AND :dt
      )
      WHERE rn=1
    ),
    snap AS (
      SELECT /*+ parallel(x,64) use_hash(x,d) */
        x.DATE_KEY,
        max(d.CALENDAR_DATE_DT)      AS CALENDAR_DATE_DT,
        x.ACCOUNT_HIST_KEY           AS hkey,
        max(x.ACCOUNT_KEY)           AS max_akey,
        sum(terminated_cards_count)  AS term,
        sum(outstanding_cards_count) AS tot
      FROM F_DAILY_ACCOUNT_SNAPSHOT x
      JOIN ua ON ua.ACCOUNT_HIST_KEY = x.ACCOUNT_HIST_KEY
      JOIN D_DATE d ON d.DATE_KEY = x.DATE_KEY
      WHERE x.DATE_KEY BETWEEN add_months(trunc(:dt,''month''),-12) AND :dt
      GROUP BY d.CALENDAR_DATE_KEY, x.DATE_KEY, x.ACCOUNT_HIST_KEY
    ),
    mx AS (
      SELECT /*+ parallel */
        DATE_KEY,
        CALENDAR_DATE_DT,
        max_akey,
        max(term) OVER (PARTITION BY hkey) AS max_term,
        max(tot)  OVER (PARTITION BY hkey) AS max_attr
      FROM snap
    )
    SELECT KV_Map(date_key, max_akey, max_attr)
    FROM mx
    WHERE CALENDAR_DATE_DT = :dt
    AND   max_attr > 0
  ';
  --
  -- Note: 
  --   Due to an Oracle 11.2.0 bug, an ERROR Ora-600 when using MERGE syntax
  --   So, bulk update is used instead.
  --
  X_SQL_MRG  constant XBS.Lob := '
    MERGE INTO #{TB} t
    USING table(:r) a
    ON (1=1
      AND t.ACCOUNT_KEY = a.from_val
      AND t.DATE_KEY    = a.map_key
    )
    WHEN MATCHED THEN UPDATE SET
      t.MAX_ATTRITION_CARDS_COUNT = a.to_val
  ';
  --
  -- the UPDATE alternate 
  --
  X_SQL_UPD  constant XBS.Lob := '
    UPDATE #{TB} t
    SET t.MAX_ATTRITION_CARDS_COUNT = :t
    WHERE t.ACCOUNT_KEY = :f
      AND t.DATE_KEY    = :d
  ';
  X_SQL_ACCT constant XBS.Lob := '
    UPDATE D_ACCOUNT t
    SET t.MAX_ATTRITION_CARDS_COUNT = :t,
        t.ROW_LAST_MOD_DTTM         = sysdate,
        t.ROW_LAST_MOD_PROC_NM      = ''' || X_PRC || '''
    WHERE t.MAX_ATTRITION_CARDS_COUNT=0 /* change once only */
      AND t.ACCOUNT_KEY = :f
      AND :d > 0
  ';
  v_rec  KV_Maplist;
  v_mkey XBS.Int;

  -- --------------------------------------------------------------------------
  PROCEDURE bulk_update(p_msg varchar2, p_sql varchar2) IS
    v_cnt  XBS.Int;
  BEGIN
    --
    -- loop through all closed accounts
    --
    FORALL i IN 1..v_rec.COUNT 
      EXECUTE IMMEDIATE p_sql 
      USING v_rec(i).to_val, v_rec(i).from_val, v_rec(i).map_key;
    v_cnt := SQL%ROWCOUNT;
    COMMIT;
    log4me.debug(X_PRC || '=>' || v_cnt || ' rows ' || p_msg, p_sql, p_ses_id);

  EXCEPTION WHEN OTHERS THEN
    log4me.debug(X_PRC || ' failed', p_sql, p_ses_id);
    RAISE;
  END bulk_update;

BEGIN
  --
  -- fetch the data set
  --
  EXECUTE IMMEDIATE X_SQL BULK COLLECT INTO v_rec
  USING p_date, p_date, p_date, p_date, p_date;
  log4me.debug(X_PRC || '=>' || SQL%ROWCOUNT || ' rows fetched', X_SQL, p_ses_id);
  --
  -- update daily snapshot
  --
  bulk_update('daily', replace(X_SQL_UPD, '#{TB}', X_TB));
  --
  -- update monthly snapshot
  -- Note: v_rec is altered here
  --
  v_mkey := get_month_key(v_rec(1).map_key);
  FOR i IN 1..v_rec.COUNT LOOP
    v_rec(i).map_key := v_mkey;
  END LOOP;
  bulk_update('monthly', replace(X_SQL_UPD, '#{TB}', G_MLY_TB));
  --
  -- update D_ACCOUNT table
  --
  bulk_update('D_ACCOUNT', X_SQL_ACCT);

EXCEPTION WHEN OTHERS THEN
  log4me.err(X_PRC || ' FAIL', p_ses_id);
  RAISE;
END fix_max_attrition_count;

END; -- PACKAGE BODY xbs_acct_agg
/
show errors

-- ============================================================================
CREATE OR REPLACE PROCEDURE F_DAILY_ACCOUNT_AGGREGATES (
  p_bgn_dt date := NULL,
  p_end_dt date := NULL
) IS
BEGIN
  /*
  Name: F_DAILY_ACCOUNT_AGGREGATES - daily account aggregates
  Desc: Daily account aggregates into F_DAILY_ACCOUNT_SNAPSHOT table
        See xbs_acct_agg_daily package for detail
  */
  xbs_acct_agg.run_one_session(p_bgn_dt, p_end_dt, XBS.run_id);
END F_DAILY_ACCOUNT_AGGREGATES;
/
show errors

GRANT EXECUTE ON F_DAILY_ACCOUNT_AGGREGATES TO DWLOADER;
