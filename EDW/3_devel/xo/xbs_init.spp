--
-- XBS basic object type and functions
--
CREATE OR REPLACE TYPE XBS_Array FORCE AS VARRAY(1000) OF varchar2(4000);
/
CREATE OR REPLACE TYPE XBS_List  FORCE AS TABLE OF varchar2(4000);
/
CREATE OR REPLACE TYPE KV_Map FORCE AS OBJECT(
  map_key  integer,
  from_val integer,
  to_val   integer
);
/
CREATE OR REPLACE TYPE KV_Maplist FORCE AS TABLE OF KV_Map;
/
-- =========================================================================
CREATE OR REPLACE PACKAGE XBS AS
  DEBUG constant boolean := true;

  /*
  Name : xbs_init package - for basic function and subtype

  Spec.: To enable Oracle OO capabilities
    #run_id - return default run_id string
    #log    - dbms_output.put_line (can be conditionally turned off)
    #now    - get current time in integer
    #blip   - display progress

    #parse_schema - split owner.object_name
    #split  - Parse simple String into array of varchar2 (aka XBS_Array)
              with a single delimiter (default to '|').

    #join   - concatenate XBS_List or XBS_Array into a varchar2
              with optional right padding to each element
  Usage:
    select join(split('1|2|3|4'),'~',10) from dual;
    select split('1|2|3|4') from dual;
    select split('1,2,3,4',',') from dual;

  Revision:
    20150615 CC - borrow from GBP
  */
  SUBTYPE Int  IS BINARY_INTEGER;
  SUBTYPE Var  IS varchar2(400);
  SUBTYPE Str  IS varchar2(4000);
  SUBTYPE Lob  IS varchar2(32767);
  SUBTYPE Dt   IS Date;

  TYPE Hash    IS TABLE OF ANYDATA INDEX BY Var;
  TYPE H_Hash  IS TABLE OF Hash    INDEX BY Var;

  -- ----------------------------------------------------------------------
  FUNCTION  run_id RETURN varchar2;
  PROCEDURE log(p_str varchar2);
  PROCEDURE parse_schema(
    p_schema  varchar2,
    o_own OUT varchar2, 
    o_tbl OUT varchar2
  );
  FUNCTION mem_usage                                     RETURN integer;
  FUNCTION now                                           RETURN integer;
  FUNCTION blip(
    p_msg    varchar2,
    p_when   integer, 
    p_nrows  integer)                                    RETURN integer;
  FUNCTION list2array(p_lst IN XBS_List)                 RETURN XBS_Array;
  FUNCTION split(p_str IN varchar2, p_x varchar2 := '|') RETURN XBS_Array;
  FUNCTION join(
    p_list IN XBS_Array,
    p_x    varchar2 := ',', -- delimiter
    p_rpad integer  := 0    -- optional right padding to the size
  ) RETURN varchar2;
END;
/
show errors

-- ========================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY XBS AS
-- -------------------------------------------------------------------------
FUNCTION run_id RETURN varchar2 IS
BEGIN
  RETURN to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4');
END run_id;

-- -------------------------------------------------------------------------
PROCEDURE log(p_str varchar2) IS
BEGIN
  dbms_output.put_line(p_str);
END log;

-- -------------------------------------------------------------------------
PROCEDURE parse_schema(
  p_schema  varchar2,
  o_own OUT varchar2, 
  o_tbl OUT varchar2
) AS
  v_tname XBS_Array := CASE WHEN instr(p_schema, '.')>0 
    THEN XBS.split(p_schema, '.') ELSE XBS_Array(USER, p_schema)
  END;
BEGIN
  o_own := upper(v_tname(1));
  o_tbl := upper(v_tname(2));
END parse_schema; 

-- ------------------------------------------------------------------------
/*
  SELECT 
    to_char(ssn.sid, '9999')                       sid,
    to_char(prc.spid, '999999999')                 pid,
    nvl(lower(ssn.machine), ins.host_name)         hostname,
    nvl(ssn.username, nvl(bgp.name, 'background')) username,
    to_char((se1.value/1024)/1024, '999G999G990D00') || ' MB' current_size,
    to_char((se2.value/1024)/1024, '999G999G990D00') || ' MB' max_size
  FROM 
    v$sesstat se1, 
    v$sesstat se2, 
    v$session ssn, 
    v$bgprocess bgp,
    v$process prc, 
    v$instance ins, 
    v$statname stat1, 
    v$statname stat2
  WHERE se1.statistic# = stat1.statistic# and stat1.name = 'session pga memory'
    AND se2.statistic# = stat2.statistic# and stat2.name = 'session pga memory max'
    AND se1.sid = ssn.sid
    AND se2.sid = ssn.sid
    AND ssn.paddr = bgp.paddr (+)
    AND ssn.paddr = prc.addr (+)
  ;
  
  select name, sum(value/1024)/1024 MB
  from v$statname n,
    v$session s,
    v$sesstat t
  where s.sid=t.sid
    and n.statistic# = t.statistic#
    and s.type = 'USER'
    and s.username = user
    and n.name in (
      'session pga memory', 'session pga memory max',
      'session uga memory', 'session uga memory max'
    )
  group by name;
*/
FUNCTION mem_usage RETURN integer IS
  X_PGA constant varchar2(20) := 'session pga memory';
  v_use integer;
BEGIN
  SELECT ms.value INTO v_use
  FROM  v$mystat ms, v$statname sn 
  WHERE  ms.statistic#=sn.statistic# 
    AND  sn.name = X_PGA;

  RETURN v_use;
END mem_usage;

-- -------------------------------------------------------------------------
FUNCTION now RETURN integer IS
BEGIN
  RETURN dbms_utility.get_time;
END now;

-- -------------------------------------------------------------------------
FUNCTION blip(
  p_msg    varchar2,
  p_when   integer, 
  p_nrows  integer) RETURN integer
IS
  X_FMT  constant XBS.Var := '999G999G990D00';
  X_ELPS constant XBS.Int := now - p_when;
  X_MEM  constant XBS.Var := trim(to_char(mem_usage/1024/1024, X_FMT));
  X_RPS  constant XBS.Var := trim(to_char((100*p_nrows+0.5)/(X_ELPS+0.001), X_FMT));
  
BEGIN
  log(p_msg || 
    CASE WHEN p_nrows>0
    THEN ' (avg. ' || X_RPS || ' RPS, PGA=' || X_MEM || 'M)'
    ELSE ''
    END
  );
  RETURN now;
END blip;

-- -------------------------------------------------------------------------
FUNCTION list2array(p_lst IN XBS_List) RETURN XBS_Array IS
  v_ary XBS_Array := XBS_Array();
BEGIN
  IF p_lst IS NULL OR p_lst.COUNT=0 THEN RETURN NULL; END IF;

  v_ary.extend(p_lst.COUNT);
  FOR i IN 1..p_lst.COUNT LOOP
    v_ary(i) := p_lst(i);
  END LOOP;
  RETURN v_ary;
END list2array;

-- ------------------------------------------------------------------------
-- split()
--
FUNCTION split(p_str IN varchar2, p_x varchar2 := '|') RETURN XBS_Array IS
  INC  constant Int := length(p_x);

  v_ary XBS_Array:= XBS_Array();
  i1    Int      := 1;
  i2    Int      := instr(p_str, p_x, i1);

  PROCEDURE add_tok(p_tok varchar2) AS 
  BEGIN
    v_ary.extend(1);
    v_ary(v_ary.COUNT) := CASE WHEN p_tok=p_x THEN '' ELSE p_tok END;
  END;

BEGIN
  LOOP
    i2 := instr(p_str, p_x, i1);
    EXIT WHEN i2 IS NULL OR i2 = 0;

    $IF dbms_db_version.version>10 $THEN
      PRAGMA INLINE(add_tok, 'YES');
    $END
    add_tok(substr(p_str, i1, i2 - i1));

    i1 := i2 + INC;
  END LOOP;
  IF p_str IS NOT NULL THEN 
    add_tok(substr(p_str, i1, length(p_str)-i1+1));
  END IF;
  RETURN v_ary;
END split;

-- ------------------------------------------------------------------------
-- join()
--
FUNCTION join(
  p_list IN XBS_Array, 
  p_x    varchar2:= ',', -- delimiter
  p_rpad integer := 0    -- optional right padding to the size
) RETURN varchar2 IS
  v_str Lob := '';
BEGIN
  IF p_list IS NULL OR p_list.COUNT=0 THEN RETURN ''; END IF;

  FOR i IN p_list.FIRST..p_list.LAST
  LOOP
    v_str := v_str || CASE WHEN p_rpad > 0 
      THEN rpad(p_list(i), p_rpad, p_x) 
      ELSE (p_list(i) || CASE WHEN i=p_list.COUNT THEN '' ELSE p_x END)
    END;
  END LOOP;
  RETURN v_str;
END join;

-- ----------------------------------------------------------------------
END;
/
show errors

