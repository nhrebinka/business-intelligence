/*
  To expand HUB and EDW for future projects, a new SOURCE_SYSTEM_CODE field
  has been added to the dimensions involved starting with IFCS project.

  The procedure performs the following:
  1. update SOURCE_SYSTEM_CODE of the following dimensions

    * D_PURCHASE_DEVICE
    * D_POS_AND_SITE

   with data cross-referenced rom the M_* and M_*_XREF tables

  ---------------------------------------------------------------------------  
  Note:
    The SOURCE_SYSTEM_CODE is updated with the following implicited 
    defined order. 

      1. CARD1
      2. FSV
      3. SIEBEL
      4. TANDEM

    Should there be a change in business rule, this code needs to be
    revised.
  ===========================================================================
*/
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ifcs1r1_migration.log;

alter session enable parallel dml;

declare
  FMT_SES  constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';

  X_PROC   constant varchar2(80) := 'update dim.SOURCE_SYSTEM_CODE field';
  X_SES_ID constant varchar2(40) := to_char(systimestamp, FMT_SES);

  v_msg varchar2(200);
  v_cnt varchar2(100);

  -- ------------------------------------------------------------------------
  -- update d_purchase_device
  --
  procedure update_pd as 
  begin
    v_msg := 'update D_PURCHASE_DEVICE';
    merge /*+ parallel(d, 64) append */ into d_purchase_device d
    using (
      select * from (
        select /*+ parallel(x, 64) */ 
          m.emboss_pd_nbr, x.dw_source_sys,
          row_number() over (partition by emboss_pd_nbr order by dw_source_sys) rn
        from m_purch_device m, m_purch_device_xref x
        where m.edm_purch_device_key = x.edm_purch_device_key
          and dw_source_sys is not null
      )
      where rn = 1
    ) u
    on (d.embossed_card_number_id = u.emboss_pd_nbr)
    when matched then update
      set d.source_system_code = u.dw_source_sys
    ;
    v_cnt := SQL%ROWCOUNT || ' rows';
    commit;
    log4me.debug(v_msg, v_cnt, X_SES_ID);
  end update_pd;

  -- ------------------------------------------------------------------------
  -- update d_pos_and_site
  --
  procedure update_pns as 
  begin
    v_msg := 'update D_POS_AND_SITE';
    merge /*+ parallel(d, 64) append */ into d_pos_and_site d
    using (
      select * from (
        select /*+ parallel(x, 64) */ 
          m.grp_mrch_id, m.pos_id, m.legacy_wex_site_id, x.dw_source_sys,
          row_number() over (partition by grp_mrch_id, pos_id order by dw_source_sys) rn
        from m_pos m, m_pos_xref x
        where m.edm_pos_key = x.edm_pos_key
          and dw_source_sys is not null
      )
      where rn = 1
    ) u
    on (1=1
      and d.source_group_merchant_id = u.grp_mrch_id
      and d.source_merchant_pos_id   = u.pos_id
    )
    when matched then update 
      set d.source_system_code = u.dw_source_sys
    ;
    v_cnt := SQL%ROWCOUNT || ' rows';  
    commit;
    log4me.debug(v_msg, v_cnt, X_SES_ID);
  end update_pns;

  -- ------------------------------------------------------------------------
  -- fill the null d_pos_and_site with SIEBEL
  --
  procedure update_pns_null as
  begin
    v_msg := 'update D_POS_AND_SITE with NULL, set to SIEBEL';
    update /*+ parallel(d, 64) append */ d_pos_and_site d
    set source_system_code = 'SIEBEL'
    where source_system_code is null
      and row_create_dttm < to_date('20141001', 'YYYYMMDD');
    v_cnt := SQL%ROWCOUNT || ' rows';
    commit;
    log4me.debug(v_msg, v_cnt, X_SES_ID);
  end update_pns_null;

begin
  log4me.info('INIT ' || X_PROC, X_SES_ID);
  update_pd;
  update_pns;
  update_pns_null;
  log4me.info('DONE ' || X_PROC, X_SES_ID);

exception
when others then
  rollback;
  log4me.err('FAIL ' || X_PROC || '#' || v_msg || ':' || SQLERRM, X_SES_ID);
  raise;
end;
/
show errors

SPOOL OFF

