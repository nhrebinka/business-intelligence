declare
  v_sql varchar2(2000);
begin
  for y in 2012..2013 loop
    for m in 1..12 loop
      v_sql := '
        alter table f_transaction_line_item
        move partition ym_' || y || '_' || trim(to_char(m, '09')) || '
        compress parallel 64
        update indexes
      ';
      execute immediate v_sql;
    end loop;
  end loop;
end;
/
show errors
