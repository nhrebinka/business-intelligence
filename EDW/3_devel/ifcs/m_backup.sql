--
-- IFCS - CiC - table backup before deployment
--
-- ------------------------------------------------------------------------
alter session enable parallel ddl;
alter session enable parallel dml;

declare
  x_bk_name constant varchar2(20) := '_BK201412';
  x_list    constant TokList := TokList(
    'M_ACCT',
    'M_POS',
    'M_SITE',
    'M_PURCH_DEVICE',
    'M_PRODUCT',
    'D_ACCOUNT',
    'D_POS_AND_SITE',
    'D_PURCHASE_DEVICE',
    'D_LINE_ITEM_TYPE'
  );
  v_tbl varchar2(80);

  procedure do_one(p_tbl varchar2) as
    x_sql CLOB := '
      CREATE /*+ append */ TABLE ' || p_tbl || x_bk_name || '
      PARALLEL (DEGREE 16)
      TABLESPACE D_BI
      AS
      SELECT /*+ parllel(x, 16) */ * FROM ' || p_tbl || ' x
    ';
    x_gnt CLOB := 'grant select on ' || v_tbl || ' to edm_owner_select';
    v_cnt varchar2(40);
  begin
    execute immediate x_sql;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    commit;
    log4me.debug('creating ' || p_tbl || v_cnt, x_sql);

    execute immediate x_gnt;
    log4me.debug('granting ' || v_tbl, x_gnt);
  end do_one;

begin
  for i in 1..x_list.COUNT loop
    v_tbl := x_list(i);
 
    do_one(v_tbl);
    if instr(v_tbl, 'M_')=1 then 
      do_one(v_tbl || '_XREF');
    end if;
  end loop;

  commit;
end;
/
