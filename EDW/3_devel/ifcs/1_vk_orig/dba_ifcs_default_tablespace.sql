SET SERVEROUTPUT ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
SET LINESIZE 2000
SET SPOOL ON 

/*
   NAME         :-    dba_ifcs_default_tablespace.sql
   AUTHOUR      :-    Bill Wright  and Venugopal KAncharla
   DATE         :-    11/08/2015
   DECRIPTION   :-   SQL  To create tablespace called  ifcs_default for the mart being built for WES CIC  
   LOG          : -   VERSION 1.0

  */


SPOOL /i1/&env/hub/logs/dba_ifcs_default_tablespace.log ;

create bigfile tablespace ifcs_default datafile size 10g autoextend on next 1g maxsize unlimited;

SPOOL OFF ;  




