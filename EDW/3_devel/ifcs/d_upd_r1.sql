/*
  To expand HUB and EDW for future projects, a new SOURCE_SYSTEM_CODE field
  has been added to the dimensions involved starting with IFCS project.

  The procedure performs the following:
  1. update SOURCE_SYSTEM_CODE of the following dimensions

    * D_PURCHASE_DEVICE
    * D_POS_AND_SITE

   with data cross-referenced rom the M_* and M_*_XREF tables

  ---------------------------------------------------------------------------  
  Note:
    The SOURCE_SYSTEM_CODE is updated with the following implicited 
    defined order. 

      1. CARD1
      2. FSV
      3. SIEBEL
      4. TANDEM

    Should there be a change in business rule, this code needs to be
    revised.
  ===========================================================================
*/
alter session enable parallel dml;

declare
  FMT_SES  constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';

  X_PROC   constant varchar2(80) := 'one-shot source_system_code update';
  X_SES_ID constant varchar2(40) := to_char(systimestamp, FMT_SES);
  X_LST    constant toklist      := toklist(
    'D_PROGRAM',
    'D_LINE_ITEM_TYPE',
    'D_POS_AND_SITE',
    'D_PURCHASE_DEVICE'
  );

  v_msg varchar2(200);
  v_cnt varchar2(100);

  -- ------------------------------------------------------------------------
  -- update d_purchase_device
  --
  procedure update_pd as 
  begin
    v_msg := 'update D_PURCHASE_DEVICE';
    merge /*+ parallel(d, 64) append */ into d_purchase_device d
    using (
      select * from (
        select /*+ parallel(x, 64) */ 
          m.emboss_pd_nbr, x.dw_source_sys,
          row_number() over (partition by emboss_pd_nbr order by dw_source_sys) rn
        from m_purch_device m, m_purch_device_xref x
        where m.edm_purch_device_key = x.edm_purch_device_key
          and dw_source_sys is not null
      )
      where rn = 1
    ) u
    on (d.embossed_card_number_id = u.emboss_pd_nbr)
    when matched then update
      set d.source_system_code = u.dw_source_sys,
          d.row_last_mod_proc_nm = X_PROC,
          d.row_last_mod_dttm    = sysdate
    ;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    commit;
    log4me.info(v_msg || v_cnt, X_SES_ID);
  end update_pd;

  -- ------------------------------------------------------------------------
  -- update d_pos_and_site
  --
  procedure update_pns as 
  begin
    v_msg := 'update D_POS_AND_SITE';
    merge /*+ parallel(d, 64) append */ into d_pos_and_site d
    using (
      select * from (
        select /*+ parallel(x, 64) */ 
          m.grp_mrch_id, m.pos_id, m.legacy_wex_site_id, x.dw_source_sys,
          row_number() over (partition by grp_mrch_id, pos_id order by dw_source_sys) rn
        from m_pos m, m_pos_xref x
        where m.edm_pos_key = x.edm_pos_key
          and dw_source_sys is not null
      )
      where rn = 1
    ) u
    on (1=1
      and d.source_group_merchant_id = u.grp_mrch_id
      and d.source_merchant_pos_id   = u.pos_id
    )
    when matched then update 
      set d.source_system_code = u.dw_source_sys,
          d.row_last_mod_proc_nm = X_PROC,
          d.row_last_mod_dttm    = sysdate
    ;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    commit;
    log4me.info(v_msg || v_cnt, X_SES_ID);
  end update_pns;

  -- ------------------------------------------------------------------------
  -- fill null source_system_code with SIEBEL
  --
  procedure update_null_to_default as
    v_sql CLOB;
  begin
    for i in 1..X_LST.count loop
      v_msg := 'update ' || X_LST(i) || ' with NULL, default to SIEBEL';
      v_sql := '
        update /*+ parallel(d, 64) append */ ' || X_LST(i) || ' d
        set d.source_system_code = ''SIEBEL'',
           d.row_last_mod_proc_nm   = ''' || X_PROC || ' NULL' || ''',
           d.row_last_mod_dttm      = sysdate
        where d.source_system_code is null
      ';
      execute immediate v_sql;
      v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
      commit;
      log4me.debug(v_msg || v_cnt, v_sql, X_SES_ID);
    end loop;
  end update_null_to_default;

  procedure set_not_null as
    v_sql CLOB;
  begin
    for i in 1..X_LST.count loop
      v_msg := 'flipping ' || X_LST(i) || '.SOURCE_SYSTEM_CODE to NOT NULL';
      v_sql := '
        alter table ' || X_LST(i) || ' 
        modify (source_system_code varchar2(10) not null)
      ';
      execute immediate v_sql;
      commit;
      log4me.debug(v_msg, v_sql, X_SES_ID);
    end loop;
  end set_not_null;

begin
  log4me.info('INIT ' || X_PROC, X_SES_ID);
  update_pd;
  update_pns;
  update_null_to_default;
  set_not_null;
  log4me.info('DONE ' || X_PROC, X_SES_ID);

exception
when others then
  rollback;
  log4me.err('FAIL ' || X_PROC || '#' || v_msg || ':' || SQLERRM, X_SES_ID);
  raise;
end;
/
show errors

