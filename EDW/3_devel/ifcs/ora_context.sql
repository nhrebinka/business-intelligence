--
-- PDBIHUB.EDM_OWNER.XD_POS_AND_SITE
--
/*
 --
 -- initialize tablespace
 --
declare
  x_param varchar2(200) := 'tablespace I_BI storage (initial 100M next 100M minextents 1 maxextents unlimited pctincrease 0)';
  x_lst   toklist := toklist(
    'I_TABLE', 'K_TABLE', 'R_TABLE', 'N_TABLE', 'P_TABLE', 'I_INDEX'
  );
begin
  ctx_ddl.create_preference('CTX_STORE_TS', 'BASIC_STORAGE');
  for i in 1..x_lst.count loop
    ctx_ddl.set_attribute('CTX_STORE_TS', x_lst(i), x_param);
  end loop;

  ctx_ddl.create_index_set('site_name');
end;
/
show errors
--
-- create table
--
*/
drop table xd_pos_and_site_addr force;

create table /*+ append */ xd_pos_and_site_addr 
tablespace d_bi
parallel 64
as 
select /*+ full parallel(x,64) */
  pos_and_site_key,
  site_address_line_1   || ' ' ||
  decode(site_address_line_2, 'N/A', '', site_address_line_2)  || ' '  ||
  decode(site_address_line_3, 'N/A', '', site_address_line_3)  || ', ' ||
  site_city             || ' ' ||
  site_state_prov_code  || ' ' ||
  site_postal_code      || ', ' ||
  decode(site_country_name,'United States', 'USA', site_country_name) || ' ' ||
  site_region_name                  as site_addr,
  merchant_name         || ' ' ||
  decode(site_brand_name,'UNBRANDED','','UNKNOWN','',site_brand_name) || ' ' ||
  site_subdistrict_name || ' ' ||
  site_name                         as site_name
from xd_pos_and_site x
where current_record_flg='1'
;
--
-- create the index
--
drop index xct_pos_and_site_addr force;

create index xctx_pos_and_site_addr
on xd_pos_and_site_addr (site_addr)
indextype is ctxsys.context
parallel 64
;

drop index xct_pos_and_site_name force;

create index xctx_pos_and_site_name
on xd_pos_and_site_addr (site_name)
indextype is ctxsys.ctxcat
parallel 64
;

--
-- CONTAINS()
--
-- find site_addr with cat or dog in it
--
select score(1), x.* from xd_pos_and_site_addr 
where contains(site_addr, 'dog | cat', 1)>20 order by score(1) desc;
--
-- find fuzzy similar to Caneda but not Canada
--
select score(1), x.* from xd_pos_and_site_addr 
where contains(site_addr, 'fuzzy(Caneda, 70, 6, weight) ~ Canada', 1)>0 order by score(1) desc;

--
-- CATSEARCH()
--
-- Example:
--   SELECT * FROM auction 
--   WHERE CATSEARCH(title, 'CD | DVD | Speaker', 'order by bid_close desc')>0;
--
-- Find anything with '66' BUT NOT with 'PHILL*'
--
select * from xd_pos_and_site_addr 
where catsearch(site_name, '66 - PHILL*', null)>0;



