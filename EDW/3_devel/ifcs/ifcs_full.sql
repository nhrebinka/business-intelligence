SELECT /*+ full(x) use_hash(x, pd) */
  d.PURCH_MONTH_YEAR_ABBR AS "Purchase Month",
  a.ACCOUNT_COUNTRY_NAME AS "Account Country Name",
  substr(a.SOURCE_ACCOUNT_ID,1,6) AS "Account Issuer Number",
/*  
  d.PURCH_CALENDAR_DATE AS "Calendar Date",
  a.NATIONAL_ACCOUNT_OPEN_DATE AS "Contract Start Date", 
  substr(a.CUSTOMER_NAME,16) AS "Customer Name",
  substr(a.CUSTOMER_NAME,1,15) AS "Customer Number",
  substr(a.ACCOUNT_NAME,10) AS "Account Name",
  substr(a.SOURCE_ACCOUNT_ID,7,15) AS "Account Number",
  substr(a.SOURCE_ACCOUNT_ID,1,6) AS "Issuer Number",
  pc.PLATTS_PRICE AS "PLATTS Price",
  pc.PUMPPRICELESSX AS "Price Less X",
  pd.EMBOSSED_CARD_NUMBER_ID AS "PAN Number",
*/
  it.NACS_ITEM_PRODUCT_CODE AS "Product Code",
  it.WEX_NACS_MARKETING_PROD_CLASS AS "Product Grouping",
  ps.PRIVATE_SITE_FLG AS "Prvt Site Flag",
  substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2) AS "Company Code",
  substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5) AS "Site Country Code",
  x.ACCT_CURRENCY_CODE AS "Currency",
  x.ACCT_PURCHASE_UNITS_NAME AS "Purchase Units Name",
/*
  x.WEX_TRANSACTION_ID AS "WEX Trans ID",
  x.VAT_RATE AS "VAT Rate",
  x.VAT_FACTOR AS "VAT Factor",
*/
  sum(CASE WHEN(x.ACCT_PURCHASE_LITRES_QTY <>0)  THEN 1 ELSE 0  END) AS "Fuel transaction count",
  sum(CASE WHEN(x.WEX_TRANSACTION_ID IS NOT NULL) THEN 1 ELSE 0 END) AS "Transaction Count",
  sum(x.ACCT_PURCHASE_LITRES_QTY) AS "Litres",
  (sum(x.ACCT_PURCH_ITEM_AMOUNT)) - 
    (sum(x.ACCT_VAT_TAX_AMOUNT)) - 
    (sum(x.ACCT_REBATE_AMOUNT)) AS "Ticket Amount",
  ((sum(x.ACCT_PURCH_ITEM_AMOUNT)) - 
    (sum(x.ACCT_VAT_TAX_AMOUNT)) - 
    (sum(x.ACCT_REBATE_AMOUNT))) + 
    (sum(x.ACCT_REBATE_AMOUNT)) + 
    (sum(x.ACCT_FEE_AMOUNT)) AS "Invoice Amt",
  sum(x.ACCT_VAT_TAX_AMOUNT) AS "VAT Amt",
  sum(x.ACCT_DUTY_AMOUNT) AS "Duty Amt",
  sum(x.ACCT_REBATE_AMOUNT) AS "Rebate Amt",
  sum(x.ACCT_FEE_AMOUNT) AS "Fee Amt",
  sum(x.ACCT_PURCH_ITEM_AMOUNT) AS "Purchased Item Amt",
  sum(x.ACCT_NBR_OF_UNITS_PURCH_COUNT) AS "Purchased Item Count"
/*
 ,
  CASE
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('380', '280', '100', '203', '208', '348', '372', '442', '616', '724', '756') 
        AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 
            (sum(x.ACCT_PURCH_ITEM_AMOUNT)/decode(sum(x.ACCT_PURCHASE_LITRES_QTY), 0, 1000000) -
            pc.PUMPPRICELESSX)*sum(x.ACCT_PURCHASE_LITRES_QTY)
    WHEN ( substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('056','826','250','528','578') AND ( substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 
            (sum(x.ACCT_PURCHASE_LITRES_QTY)*pc.PLATTS_PRICE)
    WHEN ( substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'SH' then
        (sum(x.ACCT_PURCH_ITEM_AMOUNT)/decode(sum(x.ACCT_PURCHASE_LITRES_QTY),0, 1000000)-
                        pc.PUMPPRICELESSX)*sum(x.ACCT_PURCHASE_LITRES_QTY)
    ELSE  0
  END AS "Cost to WES",
  CASE
    WHEN ( substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('250') THEN  
    Sum(((x.ACCT_VAT_TAX_AMOUNT * -1)/decode(x.VAT_FACTOR,0, 1000000))
        + (x.ACCT_REBATE_AMOUNT) 
        - (x.ACCT_FEE_AMOUNT)
        - (x.ACCT_DUTY_AMOUNT))
    ELSE (sum(x.ACCT_PURCH_ITEM_AMOUNT) 
    + sum(x.ACCT_REBATE_AMOUNT) 
    - sum(x.ACCT_FEE_AMOUNT)
    - sum(x.ACCT_DUTY_AMOUNT))
  END AS "Customer Cost",
  CASE
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '04' THEN 'Ireland'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '05' THEN 'United Kingdom'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '09' THEN 'Belgium'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '13' THEN 'France'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '14' THEN 'Germany'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '16' THEN 'Italy'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '17' THEN 'Luxembourg'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '18' THEN 'Netherlands'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '19' THEN 'Norway'
    ELSE'Unknown'
  END AS "Issuer Country Name",
  CASE 
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2))) = 'ES' THEN 'ESSO' 
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2))) = 'SH' THEN 'SHELL' 
    ELSE 'other'
  END AS "Company Name",
  CASE
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '040' THEN 'Austria'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '056' THEN 'Belgium'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '100' THEN 'Bulgaria'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '203' THEN 'Czech Rep.'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '208' THEN 'Denmark'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '250' THEN 'France'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '280' THEN 'Germany'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '348' THEN 'Hungary'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '372' THEN 'Ireland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '380' THEN 'Italy'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '442' THEN 'Luxembourg'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '528' THEN 'Netherlands'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '578' THEN 'Norway'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '616' THEN 'Poland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '724' THEN 'Spain'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '756' THEN 'Switzerland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '826' THEN 'United Kingdom'
    ELSE 'other'
  END AS "Site Country Name",
  CASE
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('380', '280', '100', '203', '208', '348', '372', '442', '616', '724', '756') AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 'PriceLessX'
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('056','826','250','528','578') AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 'Platts'
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'SH' THEN 'PriceLessX'
    ELSE 'Other'
  END AS "Site Pricing",
  CASE
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT020','INT021','INT022','INT026','INT027','INT028','INT029') THEN 'Diesel'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT023') THEN 'Premium Diesel'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT010','INT011','INT013','INT015','INT016','INT017') THEN 'Unleaded'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT012','INT014') THEN 'Premium Unleaded'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT081','INT082','INT086','INT087','INT088','INT089','INT180','INT181','INT182','INT183','INT184','INT185','INT186','INT187','INT280','INT281','INT282','INT283') THEN 'Tolls'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT491','INT510','INT554','INTTP4') THEN 'Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INTTP9') THEN 'Service Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT550','INT490','INT560') THEN 'Card Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT481','INT482','INT483','INT484','INT485','INT486','INT487','INT488','INT492','INT493','INT494','INT495') THEN 'Surcharges'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT018','INT019','INT025','INT030','INT031','INT032','INT033','INT034','INT035','INT040','INT041','INT042','INT043','INT044','INT045','INT046','INT047','INT048','INT049') THEN 'Non-Fuel'
     ELSE 'other'
  END AS "Product Sub Grouping" 
*/
FROM D_LINE_ITEM_TYPE it
  INNER JOIN F_TRANSACTION_LINE_ITEM x  ON (it.LINE_ITEM_TYPE_KEY=x.LINE_ITEM_TYPE_KEY)
  INNER JOIN D_POS_AND_SITE          ps ON (ps.POS_AND_SITE_KEY=x.POS_AND_SITE_KEY)
/*  
  INNER JOIN D_POSTING_DATE_VW ON (D_POSTING_DATE_VW.POST_DATE_KEY=x.POSTING_DATE_KEY)
*/
  INNER JOIN D_PURCHASE_DATE_VW      d  ON (d.PURCH_DATE_KEY=x.PURCHASE_DATE_KEY)
  INNER JOIN D_ACCOUNT_HIST_VW          ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY=x.PURCHASE_ACCOUNT_KEY)
  INNER JOIN D_ACCOUNT_CURRENT_VW   a   ON (a.ACCOUNT_HIST_KEY=D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY)
  INNER JOIN D_PURCHASE_DEVICE      pd  ON (pd.PURCHASE_DEVICE_KEY=x.PURCHASE_DEVICE_KEY)
  INNER JOIN IFCS_STAGE_OWNER.IFCS_DAILY_PRICE_TABLE pc ON (1=1
    AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)=pc.COMPANYCODE)
    AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)=pc.COUNTRYCODE) 
    AND (ps.PRIVATE_SITE_FLG=pc.UNMANNED)
    AND (it.NACS_ITEM_PRODUCT_CODE=pc.PRODUCTCODE)
    AND (d.PURCH_DATE_KEY=pc.DATE_KEY)
  )
GROUP BY
  d.PURCH_MONTH_YEAR_ABBR,
  a.ACCOUNT_COUNTRY_NAME, 
  substr(a.SOURCE_ACCOUNT_ID,1,6),
/*  
  d.PURCH_CALENDAR_DATE,
  substr(a.CUSTOMER_NAME,16), 
  substr(a.CUSTOMER_NAME,1,15), 
  substr(a.ACCOUNT_NAME,10), 
  substr(a.SOURCE_ACCOUNT_ID,7,15), 
  a.NATIONAL_ACCOUNT_OPEN_DATE,
  pc.PLATTS_PRICE,
  pc.PUMPPRICELESSX,
  pd.EMBOSSED_CARD_NUMBER_ID,
  x.WEX_TRANSACTION_ID, 
  x.VAT_RATE, 
  x.VAT_FACTOR, 
*/
  it.NACS_ITEM_PRODUCT_CODE, 
  it.WEX_NACS_MARKETING_PROD_CLASS,
  ps.PRIVATE_SITE_FLG, 
  substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2), 
  substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5),
  x.ACCT_CURRENCY_CODE, 
  x.ACCT_PURCHASE_UNITS_NAME
/*
  , 
  CASE
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '04' THEN 'Ireland'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '05' THEN 'United Kingdom'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '09' THEN 'Belgium'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '13' THEN 'France'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '14' THEN 'Germany'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '16' THEN 'Italy'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '17' THEN 'Luxembourg'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '18' THEN 'Netherlands'
    WHEN (substr(a.SOURCE_ACCOUNT_ID,5,2)) = '19' THEN 'Norway'
    ELSE'Unknown'
  END,
  CASE 
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2))) = 'ES' THEN 'ESSO' 
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2))) = 'SH' THEN 'SHELL' 
    ELSE'other'
  END,
  CASE
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '040' THEN 'Austria'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '056' THEN 'Belgium'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '100' THEN 'Bulgaria'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '203' THEN 'Czech Rep.'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '208' THEN 'Denmark'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '250' THEN 'France'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '280' THEN 'Germany'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '348' THEN 'Hungary'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '372' THEN 'Ireland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '380' THEN 'Italy'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '442' THEN 'Luxembourg'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '528' THEN 'Netherlands'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '578' THEN 'Norway'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '616' THEN 'Poland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '724' THEN 'Spain'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '756' THEN 'Switzerland'
    WHEN ((substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5))) = '826' THEN 'United Kingdom'
    ELSE'other'
  END,
  CASE
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('380', '280', '100', '203', '208', '348', '372', '442', '616', '724', '756') AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 'PriceLessX'
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5)) IN ('056','826','250','528','578') AND (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'ES' THEN 'Platts'
    WHEN (substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2)) = 'SH' THEN 'PriceLessX'
    ELSE 'Other'
  END,
  CASE
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT020','INT021','INT022','INT026','INT027','INT028','INT029') THEN 'Diesel'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT023') THEN 'Premium Diesel'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT010','INT011','INT013','INT015','INT016','INT017') THEN 'Unleaded'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT012','INT014') THEN 'Premium Unleaded'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT081','INT082','INT086','INT087','INT088','INT089','INT180','INT181','INT182','INT183','INT184','INT185','INT186','INT187','INT280','INT281','INT282','INT283') THEN 'Tolls'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT491','INT510','INT554','INTTP4') THEN 'Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INTTP9') THEN 'Service Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT550','INT490','INT560') THEN 'Card Fees'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT481','INT482','INT483','INT484','INT485','INT486','INT487','INT488','INT492','INT493','INT494','INT495') THEN 'Surcharges'
    WHEN ((it.NACS_ITEM_PRODUCT_CODE)) IN ('INT018','INT019','INT025','INT030','INT031','INT032','INT033','INT034','INT035','INT040','INT041','INT042','INT043','INT044','INT045','INT046','INT047','INT048','INT049') THEN 'Non-Fuel'
    ELSE 'other'
  END
*/
;

