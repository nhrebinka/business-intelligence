CREATE TYPE x_person_t AS OBJECT (
  idno           NUMBER,
  name           VARCHAR2(30),
  phone          VARCHAR2(20),
  MAP    MEMBER FUNCTION get_idno RETURN NUMBER,
  STATIC FUNCTION show_super (person_obj in x_person_t) RETURN VARCHAR2,
  MEMBER FUNCTION show RETURN VARCHAR2)
  NOT FINAL;
/

CREATE TYPE BODY x_person_t AS
  MAP MEMBER FUNCTION get_idno RETURN NUMBER IS
  BEGIN
    RETURN idno;
  END;

-- static function that can be called by subtypes
  STATIC FUNCTION show_super (person_obj in x_person_t) RETURN VARCHAR2 IS
  BEGIN
    RETURN 'Id: ' || TO_CHAR(person_obj.idno) || ', Name: ' || person_obj.name;
  END;

-- function that can be overriden by subtypes 
  MEMBER FUNCTION show RETURN VARCHAR2 IS
  BEGIN
    RETURN x_person_t.show_super ( SELF );
  END; 

END;
/
