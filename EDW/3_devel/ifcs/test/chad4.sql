/* Formatted on 2/5/2015 11:00:33 AM (QP5 v5.240.12305.39446) */
  SELECT
       D_PURCHASE_DATE_VW.PURCH_MONTH_NAME,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '040' THEN
           'Austria'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '056' THEN
           'Belgium'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '100' THEN
           'Bulgaria'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '203' THEN
           'Czech Rep.'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '208' THEN
           'Denmark'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '250' THEN
           'France'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '280' THEN
           'Germany'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '348' THEN
           'Hungary'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '372' THEN
           'Ireland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '380' THEN
           'Italy'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '442' THEN
           'Luxembourg'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '528' THEN
           'Netherlands'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '578' THEN
           'Norway'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '616' THEN
           'Poland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '724' THEN
           'Spain'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '756' THEN
           'Switzerland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '826' THEN
           'United Kingdom'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '703' THEN
           'Slovakia'
         ELSE
           'other'
       END,
       SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCHASE_LITRES_QTY),
       (SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT)),
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'ES' THEN
           'ESSO'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'SH' THEN
           'SHELL'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'GA' THEN
           'GALP'
         ELSE
           'Other'
       END
  FROM
       D_POS_AND_SITE
       INNER JOIN
       F_TRANSACTION_LINE_ITEM
         ON (D_POS_AND_SITE.POS_AND_SITE_KEY =
               F_TRANSACTION_LINE_ITEM.POS_AND_SITE_KEY)
       INNER JOIN
       D_PURCHASE_DATE_VW
         ON (D_PURCHASE_DATE_VW.PURCH_DATE_KEY =
               F_TRANSACTION_LINE_ITEM.PURCHASE_DATE_KEY)
  WHERE
       (D_PURCHASE_DATE_VW.PURCH_MONTH_NAME IN
          ('May', 'June', 'July', 'August')
AND     D_PURCHASE_DATE_VW.PURCH_YEAR_NAME IN ('2014'))
  GROUP BY
       D_PURCHASE_DATE_VW.PURCH_MONTH_NAME,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '040' THEN
           'Austria'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '056' THEN
           'Belgium'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '100' THEN
           'Bulgaria'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '203' THEN
           'Czech Rep.'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '208' THEN
           'Denmark'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '250' THEN
           'France'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '280' THEN
           'Germany'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '348' THEN
           'Hungary'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '372' THEN
           'Ireland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '380' THEN
           'Italy'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '442' THEN
           'Luxembourg'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '528' THEN
           'Netherlands'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '578' THEN
           'Norway'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '616' THEN
           'Poland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '724' THEN
           'Spain'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '756' THEN
           'Switzerland'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '826' THEN
           'United Kingdom'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  3,
                  5
                ))) = '703' THEN
           'Slovakia'
         ELSE
           'other'
       END,
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'ES' THEN
           'ESSO'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'SH' THEN
           'SHELL'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'GA' THEN
           'GALP'
         ELSE
           'Other'
       END
