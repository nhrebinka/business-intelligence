/* Formatted on 2/5/2015 10:59:17 AM (QP5 v5.240.12305.39446) */
  SELECT
       D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         1,
         2
       ),
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         7,
         15
       ),
       SUBSTR(D_ACCOUNT_CURRENT_VW.ACCOUNT_NAME, 10),
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         1,
         6
       ),
       CASE
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '04' THEN
           'Ireland'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '05' THEN
           'United Kingdom'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '09' THEN
           'Belgium'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '13' THEN
           'France'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '14' THEN
           'Germany'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '16' THEN
           'Italy'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '17' THEN
           'Luxembourg'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '18' THEN
           'Netherlands'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '19' THEN
           'Norway'
         ELSE
           'Unknown'
       END,
       D_ACCOUNT_CURRENT_VW.ACCOUNT_COUNTRY_NAME,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         3,
         5
       ),
       D_POS_AND_SITE.SOURCE_MERCHANT_POS_ID,
       D_POS_AND_SITE.PRIVATE_SITE_FLG,
       D_LINE_ITEM_TYPE.NACS_ITEM_PRODUCT_CODE,
       D_LINE_ITEM_TYPE.ITEM_NAME,
       SUM(
         CASE
           WHEN (F_TRANSACTION_LINE_ITEM.WEX_TRANSACTION_ID IS NOT NULL) THEN
             1
           ELSE
             0
         END
       ),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_NBR_OF_UNITS_PURCH_COUNT),
       (SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT)),
       ((SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT))
        - (SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT))
        - (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT)))
       + (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT))
       + (SUM(F_TRANSACTION_LINE_ITEM.SITE_FEE_AMOUNT)),
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       F_TRANSACTION_LINE_ITEM.WEX_TRANSACTION_ID,
       SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_DUTY_AMOUNT)
  FROM
       D_LINE_ITEM_TYPE
       INNER JOIN
       F_TRANSACTION_LINE_ITEM
         ON (D_LINE_ITEM_TYPE.LINE_ITEM_TYPE_KEY =
               F_TRANSACTION_LINE_ITEM.LINE_ITEM_TYPE_KEY)
       INNER JOIN
       D_POS_AND_SITE
         ON (D_POS_AND_SITE.POS_AND_SITE_KEY =
               F_TRANSACTION_LINE_ITEM.POS_AND_SITE_KEY)
       INNER JOIN
       D_PURCHASE_DATE_VW
         ON (D_PURCHASE_DATE_VW.PURCH_DATE_KEY =
               F_TRANSACTION_LINE_ITEM.PURCHASE_DATE_KEY)
       INNER JOIN
       D_ACCOUNT_HIST_VW
         ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY =
               F_TRANSACTION_LINE_ITEM.PURCHASE_ACCOUNT_KEY)
       INNER JOIN
       D_ACCOUNT_CURRENT_VW
         ON (D_ACCOUNT_CURRENT_VW.ACCOUNT_HIST_KEY =
               D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY)
  WHERE
       (CASE
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '04' THEN
            'Ireland'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '05' THEN
            'United Kingdom'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '09' THEN
            'Belgium'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '13' THEN
            'France'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '14' THEN
            'Germany'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '16' THEN
            'Italy'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '17' THEN
            'Luxembourg'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '18' THEN
            'Netherlands'
          WHEN (SUBSTR(
                  D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                  5,
                  2
                )) = '19' THEN
            'Norway'
          ELSE
            'Unknown'
        END IN ('Luxembourg')
AND     SUBSTR(
          D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
          7,
          15
        ) IN ('001170275')
AND     D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE BETWEEN '01-01-2013 00:00:00'
                                                   AND '31-12-2014 00:00:00')
  GROUP BY
       D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         1,
         2
       ),
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         7,
         15
       ),
       SUBSTR(D_ACCOUNT_CURRENT_VW.ACCOUNT_NAME, 10),
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         1,
         6
       ),
       CASE
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '04' THEN
           'Ireland'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '05' THEN
           'United Kingdom'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '09' THEN
           'Belgium'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '13' THEN
           'France'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '14' THEN
           'Germany'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '16' THEN
           'Italy'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '17' THEN
           'Luxembourg'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '18' THEN
           'Netherlands'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '19' THEN
           'Norway'
         ELSE
           'Unknown'
       END,
       D_ACCOUNT_CURRENT_VW.ACCOUNT_COUNTRY_NAME,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         3,
         5
       ),
       D_POS_AND_SITE.SOURCE_MERCHANT_POS_ID,
       D_POS_AND_SITE.PRIVATE_SITE_FLG,
       D_LINE_ITEM_TYPE.NACS_ITEM_PRODUCT_CODE,
       D_LINE_ITEM_TYPE.ITEM_NAME,
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       F_TRANSACTION_LINE_ITEM.WEX_TRANSACTION_ID
