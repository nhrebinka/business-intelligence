/* Formatted on 2/5/2015 10:59:31 AM (QP5 v5.240.12305.39446) */
  SELECT
       D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE,
       D_POS_AND_SITE.SOURCE_MERCHANT_POS_ID,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         3,
         5
       ),
       D_LINE_ITEM_TYPE.NACS_ITEM_PRODUCT_CODE,
       D_POS_AND_SITE.PRIVATE_SITE_FLG,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         1,
         2
       ),
       D_LINE_ITEM_TYPE.WEX_NACS_MARKETING_PROD_CLASS,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'ES' THEN
           'ESSO'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'SH' THEN
           'SHELL'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'GA' THEN
           'GALP'
         ELSE
           'Other'
       END,
       SUM(F_TRANSACTION_LINE_ITEM.SITE_NBR_OF_UNITS_PURCH_COUNT),
       (SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT))
       - (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT)),
       ((SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT))
        - (SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT))
        - (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT)))
       + (SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT))
       + (SUM(F_TRANSACTION_LINE_ITEM.SITE_FEE_AMOUNT)),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_VAT_TAX_AMOUNT),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_REBATE_AMOUNT),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_FEE_AMOUNT),
       SUM(F_TRANSACTION_LINE_ITEM.SITE_PURCH_ITEM_AMOUNT),
       F_TRANSACTION_LINE_ITEM.VAT_RATE,
       SUM(F_TRANSACTION_LINE_ITEM.SITE_DUTY_AMOUNT),
       CASE
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '04' THEN
           'Ireland'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '05' THEN
           'United Kingdom'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '09' THEN
           'Belgium'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '13' THEN
           'France'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '14' THEN
           'Germany'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '16' THEN
           'Italy'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '17' THEN
           'Luxembourg'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '18' THEN
           'Netherlands'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '19' THEN
           'Norway'
         ELSE
           'Unknown'
       END,
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       SUM(
         CASE
           WHEN (F_TRANSACTION_LINE_ITEM.WEX_TRANSACTION_ID IS NOT NULL) THEN
             1
           ELSE
             0
         END
       ),
       D_ACCOUNT_CURRENT_VW.ACCOUNT_COUNTRY_NAME,
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         7,
         15
       )
  FROM
       D_LINE_ITEM_TYPE
       INNER JOIN
       F_TRANSACTION_LINE_ITEM
         ON (D_LINE_ITEM_TYPE.LINE_ITEM_TYPE_KEY =
               F_TRANSACTION_LINE_ITEM.LINE_ITEM_TYPE_KEY)
       INNER JOIN
       D_POS_AND_SITE
         ON (D_POS_AND_SITE.POS_AND_SITE_KEY =
               F_TRANSACTION_LINE_ITEM.POS_AND_SITE_KEY)
       INNER JOIN
       D_PURCHASE_DATE_VW
         ON (D_PURCHASE_DATE_VW.PURCH_DATE_KEY =
               F_TRANSACTION_LINE_ITEM.PURCHASE_DATE_KEY)
       INNER JOIN
       D_ACCOUNT_HIST_VW
         ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY =
               F_TRANSACTION_LINE_ITEM.PURCHASE_ACCOUNT_KEY)
       INNER JOIN
       D_ACCOUNT_CURRENT_VW
         ON (D_ACCOUNT_CURRENT_VW.ACCOUNT_HIST_KEY =
               D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY)
  WHERE
       D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE BETWEEN '01-07-2012 00:00:00'
                                                  AND '31-07-2012 00:00:00'
  GROUP BY
       D_PURCHASE_DATE_VW.PURCH_CALENDAR_DATE,
       D_POS_AND_SITE.SOURCE_MERCHANT_POS_ID,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         3,
         5
       ),
       D_LINE_ITEM_TYPE.NACS_ITEM_PRODUCT_CODE,
       D_POS_AND_SITE.PRIVATE_SITE_FLG,
       SUBSTR(
         D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
         1,
         2
       ),
       D_LINE_ITEM_TYPE.WEX_NACS_MARKETING_PROD_CLASS,
       CASE
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'ES' THEN
           'ESSO'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'SH' THEN
           'SHELL'
         WHEN ((SUBSTR(
                  D_POS_AND_SITE.SOURCE_GROUP_MERCHANT_ID,
                  1,
                  2
                ))) = 'GA' THEN
           'GALP'
         ELSE
           'Other'
       END,
       F_TRANSACTION_LINE_ITEM.VAT_RATE,
       CASE
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '04' THEN
           'Ireland'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '05' THEN
           'United Kingdom'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '09' THEN
           'Belgium'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '13' THEN
           'France'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '14' THEN
           'Germany'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '16' THEN
           'Italy'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '17' THEN
           'Luxembourg'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '18' THEN
           'Netherlands'
         WHEN (SUBSTR(
                 D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
                 5,
                 2
               )) = '19' THEN
           'Norway'
         ELSE
           'Unknown'
       END,
       F_TRANSACTION_LINE_ITEM.SITE_CURRENCY_CODE,
       D_ACCOUNT_CURRENT_VW.ACCOUNT_COUNTRY_NAME,
       SUBSTR(
         D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         7,
         15
       )
