WITH
dv AS (  /* daily view */
  SELECT /*+ full(x) parallel(64,x) use_hash(x, pd) */
  d.PURCH_DATE_KEY                        AS date_key,
  a.ACCOUNT_COUNTRY_NAME                  AS account_country_name,
  substr(a.SOURCE_ACCOUNT_ID,5,2)         AS issuer_country,
  substr(a.SOURCE_ACCOUNT_ID,1,6)         AS account_issuer_number,
  it.NACS_ITEM_PRODUCT_CODE               AS product_code,
  it.WEX_NACS_MARKETING_PROD_CLASS        AS product_grouping,
  ps.PRIVATE_SITE_FLG                     AS private_site,
  substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2) AS company_code,
  substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5) AS site_country_code,
  x.ACCT_CURRENCY_CODE                    AS currency,
  x.ACCT_PURCHASE_UNITS_NAME              AS purchase_units_name,
  sum(x.ACCT_PURCHASE_LITRES_QTY)         AS litres,
  sum(x.ACCT_PURCH_ITEM_AMOUNT)           AS purch_item_amt,
  sum(x.ACCT_VAT_TAX_AMOUNT)              AS vat_amt,
  sum(x.ACCT_VAT_TAX_AMOUNT/x.VAT_FACTOR) AS adjusted_vat_amt,
  sum(x.ACCT_REBATE_AMOUNT)               AS rebate_amt,
  sum(x.ACCT_FEE_AMOUNT)                  AS fee_amt,
  sum(x.ACCT_DUTY_AMOUNT)                 AS duty_amt,
  sum(x.ACCT_NBR_OF_UNITS_PURCH_COUNT)    AS purchased_item_count,
  sum(CASE WHEN(x.ACCT_PURCHASE_LITRES_QTY <>0)   THEN 1 ELSE 0 END) AS fuel_transaction_count,
  sum(CASE WHEN(x.WEX_TRANSACTION_ID IS NOT NULL) THEN 1 ELSE 0 END) AS transaction_count
  FROM F_TRANSACTION_LINE_ITEM x
  JOIN D_LINE_ITEM_TYPE     it ON (it.LINE_ITEM_TYPE_KEY=x.LINE_ITEM_TYPE_KEY)
  JOIN D_POS_AND_SITE       ps ON (ps.POS_AND_SITE_KEY=x.POS_AND_SITE_KEY)
  JOIN D_PURCHASE_DATE_VW   d  ON (d.PURCH_DATE_KEY=x.PURCHASE_DATE_KEY)
  JOIN D_PURCHASE_DEVICE    pd ON (pd.PURCHASE_DEVICE_KEY=x.PURCHASE_DEVICE_KEY)
  JOIN D_ACCOUNT_HIST_VW    ah ON (ah.ACCOUNT_KEY=x.PURCHASE_ACCOUNT_KEY)
  JOIN D_ACCOUNT_CURRENT_VW a  ON (a.ACCOUNT_HIST_KEY=ah.ACCOUNT_HIST_KEY)
  GROUP BY
  d.PURCH_DATE_KEY,
  a.ACCOUNT_COUNTRY_NAME, 
  substr(a.SOURCE_ACCOUNT_ID,1,6),
  substr(a.SOURCE_ACCOUNT_ID,5,2),
  it.NACS_ITEM_PRODUCT_CODE, 
  it.WEX_NACS_MARKETING_PROD_CLASS,
  substr(ps.SOURCE_GROUP_MERCHANT_ID,1,2), 
  substr(ps.SOURCE_GROUP_MERCHANT_ID,3,5),
  ps.PRIVATE_SITE_FLG,
  x.ACCT_CURRENCY_CODE, 
  x.ACCT_PURCHASE_UNITS_NAME
),
mv AS (  /* monthly view */
  SELECT /*+ full(pc) parallel(pc,64) use_hash(pc, d) */
  d.PURCH_MONTH_YEAR_ABBR AS purchase_month,
  dv.account_country_name,
  dv.issuer_country,
  dv.account_issuer_number,
  dv.product_code,
  dv.product_grouping,
  dv.private_site,
  dv.company_code,
  dv.site_country_code,
  dv.currency,
  dv.purchase_units_name,
  sum(dv.litres),
  sum(dv.purch_item_amt),
  sum(pc.PUMPPRICELESSX*dv.litres) AS pumpx_amt,
  sum(dv.litres*pc.PLATTS_PRICE)   AS platts_amt,
  sum(dv.vat_amt),
  sum(dv.adjusted_vat_amt),
  sum(dv.rebate_amt),
  sum(dv.fee_amt),
  sum(dv.duty_amt),
  sum(dv.purchased_item_count),
  sum(dv.fuel_transaction_count),
  sum(dv.transaction_count)
  FROM dv
  JOIN D_PURCHASE_DATE_VW d  ON (dv.DATE_KEY=d.PURCH_DATE_KEY)
  LEFT OUTER JOIN IFCS_STAGE_OWNER.IFCS_DAILY_PRICE_TABLE pc ON (1=1
    AND (dv.company_code     =pc.COMPANYCODE)
    AND (dv.site_country_code=pc.COUNTRYCODE) 
    AND (dv.private_site     =pc.UNMANNED)
    AND (dv.product_code     =pc.PRODUCTCODE)
    AND (dv.date_key         =pc.DATE_KEY)
  )
  GROUP BY 
  d.PURCH_MONTH_YEAR_ABBR,
  dv.account_country_name,
  dv.issuer_country,
  dv.account_issuer_number,
  dv.product_code,
  dv.product_grouping,
  dv.private_site,
  dv.company_code,
  dv.site_country_code,
  dv.currency,
  dv.purchase_units_name
)
select * from mv
;

