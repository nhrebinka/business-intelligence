--
-- IFCS - CiC - table backup before deployment
--
-- ------------------------------------------------------------------------
-- 
alter session enable parallel ddl;
alter session enable parallel dml;

declare
  x_bk_name constant varchar2(20) := '_BK412';
  x_list    constant TokList := TokList(
    'D_ACCOUNT',
    'D_PURCHASE_DEVICE',
    'D_POS_AND_SITE',
    'D_LINE_ITEM_TYPE',
    'F_PURCHASE_DEVICE_EVENT'
  );
  v_tbl varchar2(80);

  procedure do_one(p_tbl varchar2) as
    x_dst varchar2(120) := p_tbl || x_bk_name;
    x_sql CLOB := '
      CREATE /*+ append */ TABLE ' || x_dst || '
      PARALLEL (DEGREE 16)
      TABLESPACE D_BI
      AS
      SELECT /*+ parllel(x, 16) */ * FROM ' || p_tbl || ' x
    ';
    x_gnt CLOB := 'grant select on ' || x_dst || ' to edw_owner_select';
    v_cnt varchar2(40);
  begin
    execute immediate x_sql;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    commit;
    log4me.debug('creating ' || p_tbl || v_cnt, x_sql);

    execute immediate x_gnt;
    log4me.debug('granting ' || x_dst, x_gnt);
  end do_one;

begin
  for i in 1..x_list.COUNT loop
    v_tbl := x_list(i);
    do_one(v_tbl);
  end loop;

  commit;
end;
/
