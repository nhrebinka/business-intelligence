set serveroutput on
set pagesize 0
set linesize 80

alter session enable parallel dml;

declare
  v_cnt integer := 0;
begin
  for r in (
    select /*+ noparallel full */ pos_and_site_key , site_addr
    from xd_pos_and_site_addr
    where trim(site_addr) <> 'Unknown Unknown Unknown, Unknown Un Unknown, Unknown Unknown'
  )
  loop
    insert /*+ append */ into xd_site_match
    select /*+ parallel(i, 64) */
      r.pos_and_site_key from_key,
      i.pos_and_site_key to_key, 
      utl_match.jaro_winkler_similarity(i.site_addr, r.site_addr) score,
      r.site_addr        from_addr,
      i.site_addr        to_addr
    from xd_pos_and_site_addr i
    where i.pos_and_site_key <> r.pos_and_site_key
    and utl_match.jaro_winkler_similarity(i.site_addr, r.site_addr) > 95
    and rownum < 10;
    commit;

    v_cnt := v_cnt + 1;
    if mod(v_cnt, 1000)=0 then
      log4me.info('OK: ' || v_cnt || r.pos_and_site_key);
    end if;
  end loop;

exception when others then
  log4me.info('ERR:' || v_cnt);
  raise;  
end;
/
show errors
  
