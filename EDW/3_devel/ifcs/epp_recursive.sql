drop table x_epp_hdr purge;
create table x_epp_hdr (
batch_id varchar2(40),
k        integer,
v        varchar2(1000)
)
tablespace d_bi;

drop table x_epp_dtl purge;
create table x_epp_dtl (
batch_id varchar2(40),
p        integer,
k        integer,
v        varchar2(4000)
)
tablespace d_bi;

with
ep as (
  select rn, substr(hdr,1,3) id, cast(substr(hdr,2,1) as integer) m, substr(hdr,4,6) v, hdr, dtl
  from x_epp
),
t4 as (
  select rn-cast(substr(hdr,40,10) as integer) r1, rn-1 r2
  from ep
  where id='T40'
),
r50 as (
  select ep.rn, t4.r1, t4.r2, ep.hdr, ep.dtl
  from ep, t4
  where ep.rn between t4.r1 and t4.r2
),
m4 as (
  select rn, m, id, hdr 
  from ep 
  where m<5 and substr(hdr,1,1) <> 'T' 
),
h4 as (
  select p_rn, rn, id, m, hdr from (
    select 
      max(r2.rn)   over (partition by r1.rn order by r1.id) p_rn,
      row_number() over (partition by r1.rn order by r1.id) n,
      r1.* 
    from m4 r1, m4 r2
    where r2.rn <= r1.rn
    and (r2.m = r1.m-1 or r2.m=1)
  )
  where n=1
),
r (lv, rn, id, path) as (
  select 1, rn, id, to_clob(hdr)
  from h4
  where p_rn=rn
  union all
  select lv+1, h4.rn, h4.id, to_clob(r.path || '~|~' || h4.hdr)
  from r, h4
  where r.rn = h4.p_rn
)
-- search breadth first by id set ord
cycle rn set cyc to 1 default 0
select * from r;

--
-- for header
--
insert into x_epp_hdr
select 
  '12345' batch_id, 
  rn      k, 
  concat_pad(split(path,'~|~'),100) v
from r where lv=4;

--
-- for detail
--
insert into x_epp_dtl
select 
  '12345' batch_id,
  r.rn    p, 
  r50.rn  k,
  rpad(r50.hdr,100,'~') || r50.dtl v
from r, r50
where r.lv=4 and r50.r1-1 = r.rn
;
--
-- to query
--
select 
  pkey, 
  d.key, 
  hdr, 
  dtl 
from x_epp_hdr h, x_epp_dtl d
where d.batch_id = h.batch_id
and   d.pkey = h.key
order by d.key;
