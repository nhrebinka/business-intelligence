CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_AGING_SNAPSHOT_LOAD
IS
--******************************************************************************
--  NAME:       F_DAILY_AGING_SNAPSHOT_LOAD
--  PURPOSE:
--   REVISIONS:
--  Ver        Date        Author           Description
--  ---------  ----------  ---------------  ------------------------------------
--  1.0        04/26/2011   SSirigiri       1. Created this procedure.
--   NOTES:
--
--  Automatically available Auto Replace Keywords:
--     Object Name:     F_DAILY_AGING_SNAPSHOT_LOAD
--     Sysdate:         04/26/2011
--     Date and Time:   04/26/2011, 9:53:33 AM,
--     Username:        SSirigiri
--******************************************************************************/
BEGIN
   INSERT                                                        /*+ append */
         INTO EDW_OWNER.F_DAILY_AGING_SNAPSHOT (ACCOUNT_REGION_KEY,
                                      AR_AGE_UNBILLED_AMOUNT,
                                      AR_AGE_CURRENT_AMOUNT,
                                      AR_AGE_30DAY_AMOUNT,
                                      AR_AGE_60DAY_AMOUNT,
                                      AR_AGE_90DAY_AMOUNT,
                                      AR_AGE_120DAY_AMOUNT,
                                      AR_AGE_150DAY_AMOUNT,
                                      AR_AGE_180DAY_AMOUNT,
                                      AR_AGE_TOTAL_PASTDUE_AMOUNT,
                                      AR_AGE_TOTAL_AMOUNT,
                                      AR_OLDEST_PASTDUE_DAYS_COUNT,
                                      REGULATORY_AGE_CURRENT_AMOUNT,
                                      REGULATORY_AGE_30DAY_AMOUNT,
                                      REGULATORY_AGE_60DAY_AMOUNT,
                                      REGULATORY_AGE_90DAY_AMOUNT,
                                      REGULATORY_AGE_120DAY_AMOUNT,
                                      REGULATORY_AGE_150DAY_AMOUNT,
                                      REGULATORY_AGE_TOT_PASTDUE_AMT,
                                      AR_30DAY_PASTDUE_COUNT1,
                                      AR_60DAY_PASTDUE_COUNT1,
                                      AR_90DAY_PASTDUE_COUNT1,
                                      AR_120DAY_PASTDUE_COUNT1,
                                      AR_150DAY_PASTDUE_COUNT1,
                                      AR_180DAY_PASTDUE_COUNT1,
                                      REGULATORY_30DAY_PASTDUE_CNT1,
                                      REGULATORY_60DAY_PASTDUE_CNT1,
                                      REGULATORY_90DAY_PASTDUE_CNT1,
                                      REGULATORY_120DAY_PASTDUE_CNT1,
                                      REGULATORY_150DAY_PASTDUE_CNT1,
                                      REGULATORY_AGE_TOTAL_AMOUNT,
                                      REGULATORY_OLDEST_PASTDUE_CNT,
                                      REGULATORY_RISK_01DAY_AMOUNT,
                                      REGULATORY_RISK_30DAY_AMOUNT,
                                      REGULATORY_RISK_60DAY_AMOUNT,
                                      REGULATORY_RISK_90DAY_AMOUNT,
                                      REGULATORY_RISK_120DAY_AMOUNT,
                                      REGULATORY_RISK_150DAY_AMOUNT,
                                      REGULATORY_RISK_CURRENT_AMT,
                                      CREDIT_LIMIT_AMOUNT,
                                      ROW_CREATE_DTTM,
                                      ROW_LAST_MOD_DTTM,
                                      ROW_LAST_MOD_PROC_NM,
                                      ROW_LAST_MOD_PROC_SEQ_NBR,
                                      ROW_SOURCE_SYS_NM,
                                      DAILY_AGING_SNAPSHOT_KEY,
                                      REVENUE_DATE_KEY,
                                      AGING_CATEGORY_KEY,
                                      ACCOUNT_KEY,
                                      ACCOUNT_HIST_KEY,
                                      PROGRAM_KEY)
      (SELECT                                       /*+ parallel (FTLI, 4 ) */
             ACCOUNT_REGION_KEY,
              AR_AGE_UNBILLED_AMOUNT,
              AR_AGE_CURRENT_AMOUNT,
              AR_AGE_30DAY_AMOUNT,
              AR_AGE_60DAY_AMOUNT,
              AR_AGE_90DAY_AMOUNT,
              AR_AGE_120DAY_AMOUNT,
              AR_AGE_150DAY_AMOUNT,
              AR_AGE_180DAY_AMOUNT,
              AR_AGE_TOTAL_PASTDUE_AMOUNT,
              AR_AGE_TOTAL_AMOUNT,
              AR_OLDEST_PASTDUE_DAYS_COUNT,
              REGULATORY_AGE_CURRENT_AMOUNT,
              REGULATORY_AGE_30DAY_AMOUNT,
              REGULATORY_AGE_60DAY_AMOUNT,
              REGULATORY_AGE_90DAY_AMOUNT,
              REGULATORY_AGE_120DAY_AMOUNT,
              REGULATORY_AGE_150DAY_AMOUNT,
              REGULATORY_AGE_TOT_PASTDUE_AMT,
              AR_30DAY_PASTDUE_COUNT1,
              AR_60DAY_PASTDUE_COUNT1,
              AR_90DAY_PASTDUE_COUNT1,
              AR_120DAY_PASTDUE_COUNT1,
              AR_150DAY_PASTDUE_COUNT1,
              AR_180DAY_PASTDUE_COUNT1,
              REGULATORY_30DAY_PASTDUE_CNT1,
              REGULATORY_60DAY_PASTDUE_CNT1,
              REGULATORY_90DAY_PASTDUE_CNT1,
              REGULATORY_120DAY_PASTDUE_CNT1,
              REGULATORY_150DAY_PASTDUE_CNT1,
              REGULATORY_AGE_TOTAL_AMOUNT,
              REGULATORY_OLDEST_PASTDUE_CNT,
              REGULATORY_RISK_01DAY_AMOUNT,
              REGULATORY_RISK_30DAY_AMOUNT,
              REGULATORY_RISK_60DAY_AMOUNT,
              REGULATORY_RISK_90DAY_AMOUNT,
              REGULATORY_RISK_120DAY_AMOUNT,
              REGULATORY_RISK_150DAY_AMOUNT,
              REGULATORY_RISK_CURRENT_AMT,
              CREDIT_LIMIT_AMOUNT,
              ROW_CREATE_DTTM,
              ROW_LAST_MOD_DTTM,
              ROW_LAST_MOD_PROC_NM,
              ROW_LAST_MOD_PROC_SEQ_NBR,
              ROW_SOURCE_SYS_NM,
              DAILY_AGING_SNAPSHOT_KEY,
              REVENUE_DATE_KEY,
              AGING_CATEGORY_KEY,
              ACCOUNT_KEY,
              ACCOUNT_HIST_KEY,
              PROGRAM_KEY
         FROM EDW_STAGE_OWNER.F_DAILY_AGING_SNAPSHOT);
   COMMIT;
END F_DAILY_AGING_SNAPSHOT_LOAD;
/
