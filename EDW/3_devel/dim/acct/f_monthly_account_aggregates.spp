CREATE OR REPLACE PROCEDURE F_MONTHLY_ACCOUNT_AGG_BY_DATES (
  p_min_dt DATE,
  p_max_dt DATE,
  p_ses_id VARCHAR DEFAULT to_char(SYSTIMESTAMP, 'YYYYMMDD.HH24MISS.FF4')
)
AS
/*
  NAME      : F_MONTHLY_ACCOUNT_AGG_BY_DATES
  DECRIPTION: load  Month Account Aggregates from daily by date range

  NOTE      :
    The Start Date and END DATE input paramters always needs to be the
    Calendar Start date and Calendar END Date even if we are trying to
    calculate the Aggrgates for a Partial Month.

  REVISION  :
  1.0 20100615 Izzy Reinish - Original
  2.0 20101011 Venugopal Kancharla
      Added the F_SEED_DATES procedure to autoamte the history loading, and
      added the  indexes Drop and Recreate, and
      added the dynamic Merge statement   to Tune Izzy's Sql :-)
  2.1 20101212 Venu Kancharla
      Changes for Daily run
  2.2 20100117 Venu Kancharla
      for Hash Join daily runs
  2.3 20100120 Venu Kancharla
      Backed out the HASH JOIN hints, and
      add NOTE section
  2.4 20110331 Venu Kancharla
      Added the D_SEED_DATES PROC for Daily Processing, and
      the PURCHSE DEVICE EVENT tabkles in the Merge statement
  2.5 20111028 Venu Kancharla
      Active Card Count Done
  2.6 20111128 Venu Kancharla
      Fix the Bug for the LAST DAY Of the month
  2.7 20130529 Venu Kancharla
      Fix the Bug for the V_START_DT of  the month
  2.8 20140512 Venu Kancharla
      Add the account_hist_key and MAX outstnding Card Counts
  3.0 20140716 Lee
      Reformat for readability, and
      Modulize F_MONTHLY_ACCOUNT_AGG_BY_DATES from F_MONTHLY_ACCOUNT_AGGREGATES.
*/
  v_proc constant VARCHAR(30) := 'F_MONTHLY_ACCOUNT_AGG_BY_DATES';
  v_msg  constant VARCHAR(80) :=
    v_proc || '(' ||
    to_char(p_min_dt, 'yyyymmdd') || ',' ||
    to_char(p_max_dt, 'yyyymmdd') || ')';
BEGIN
  -- Here there should be a Delete statement to
  -- Delete the Record for that Month (Added By Venu compatible for Daily Run)
  log4me.debug('starting ' || v_msg, '', p_ses_id);

  MERGE INTO F_MONTHLY_ACCOUNT_SNAPSHOT mas USING (
    SELECT
      snaps.ACCOUNT_KEY,
      snaps.PROGRAM_KEY,
      snaps.DATE_KEY,
      snaps.OUTSTANDING_CARDS_COUNT,
      snaps.TERMINAL_CARD_COUNT,
      snaps.TOTAL_CARDS_COUNT,
      snaps.CARDS_IN_TRANSITION_COUNT,
      snaps.VIRTUAL_OUTSTANDING_CARD_COUNT,
      snaps.VIRTUAL_TERMINAL_CARD_COUNT,
      snaps.VIRTUAL_TOTAL_CARD_COUNT,
      snaps.VIRTUAL_CARDS_IN_TRANS_COUNT,
      snaps.MAG_OUTSTANDING_CARD_COUNT,
      snaps.MAG_TERMINAL_CARD_COUNT,
      snaps.MAG_TOTAL_CARD_COUNT,
      snaps.MAG_CARDS_IN_TRANS_COUNT,
      sums.ISSUED_CARDS_COUNT,
      sums.TERMINATED_CARDS_COUNT,
      sums.TRANS_CARDS_COUNT,
      sums.VIRTUAL_ISSUED_CARDS_COUNT,
      sums.VIRTUAL_TERMINATED_CARDS_COUNT,
      sums.VIRTUAL_TRANS_CARDS_COUNT,
      sums.MAG_ISSUED_CARDS_COUNT,
      sums.MAG_TERMINATED_CARDS_COUNT,
      sums.MAG_TRANS_CARD_COUNT,
      0                                         AS ACTIVE_CARD_COUNT,
      SYSDATE                                   AS ROW_CREATE_DTTM,
      SYSDATE                                   AS ROW_LAST_MOD_DTTM,
      v_proc                                    AS ROW_LAST_MOD_PROC_NM,
      1                                         AS ROW_LAST_MOD_PROC_SEQ_NBR,
      -- the following added by venu 09/25/2013
      snaps.MAX_OUTSTANDING_CARDS_COUNT,
      snaps.ACCOUNT_HIST_KEY
    FROM (
      --
      -- monthly summary portion
      --
      SELECT /*+ parallel(d) use_hash(d, das) */
        das.ACCOUNT_KEY,
        das.PROGRAM_KEY,
        sum(das.ISSUED_CARDS_COUNT)             AS ISSUED_CARDS_COUNT,
        sum(das.TERMINATED_CARDS_COUNT)         AS TERMINATED_CARDS_COUNT,
        sum(das.TRANS_CARDS_COUNT)              AS TRANS_CARDS_COUNT,
        sum(das.VIRTUAL_ISSUED_CARDS_COUNT)     AS VIRTUAL_ISSUED_CARDS_COUNT,
        sum(das.VIRTUAL_TERMINATED_CARDS_COUNT) AS VIRTUAL_TERMINATED_CARDS_COUNT,
        sum(das.VIRTUAL_TRANS_CARDS_COUNT)      AS VIRTUAL_TRANS_CARDS_COUNT,
        sum(das.MAG_ISSUED_CARDS_COUNT)         AS MAG_ISSUED_CARDS_COUNT,
        sum(das.MAG_TERMINATED_CARDS_COUNT)     AS MAG_TERMINATED_CARDS_COUNT,
        sum(das.MAG_TRANS_CARD_COUNT)           AS MAG_TRANS_CARD_COUNT
      FROM F_DAILY_ACCOUNT_SNAPSHOT das, D_DATE d
      WHERE d.DATE_KEY = das.DATE_KEY
        AND d.CALENDAR_DATE_DT BETWEEN p_min_dt AND p_max_dt
      GROUP BY
        das.ACCOUNT_KEY,
        das.PROGRAM_KEY
    ) sums
    JOIN (
      --
      -- get account info from their last existing date of the month
      --
      SELECT
        das.ACCOUNT_KEY,
        das.PROGRAM_KEY,
        ldom.DATE_KEY,
        das.OUTSTANDING_CARDS_COUNT,
        das.TERMINAL_CARD_COUNT,
        das.TOTAL_CARDS_COUNT,
        das.CARDS_IN_TRANSITION_COUNT,
        das.VIRTUAL_OUTSTANDING_CARD_COUNT,
        das.VIRTUAL_TERMINAL_CARD_COUNT,
        das.VIRTUAL_TOTAL_CARD_COUNT,
        das.VIRTUAL_CARDS_IN_TRANS_COUNT,
        das.MAG_OUTSTANDING_CARD_COUNT,
        das.MAG_TERMINAL_CARD_COUNT,
        das.MAG_TOTAL_CARD_COUNT,
        das.MAG_CARDS_IN_TRANS_COUNT,
        -- the folling were added by venu 09/25/2013
        das.ACCOUNT_HIST_KEY,
        das.MAX_OUTSTANDING_CARDS_COUNT
      FROM F_DAILY_ACCOUNT_SNAPSHOT das
      JOIN (
        --
        -- find the max existing date for every account
        -- add PARALLEL hint to enable partition-wise join
        -- or the FFS+ordering (Oracle default) will take more than 10 minutes
        --
        SELECT /*+ parallel(das) parallel(d) use_hash(d, das) */
          das.ACCOUNT_KEY                  AS ACCOUNT_KEY,
          MAX(d.DATE_KEY)                  AS MAX_DATE_KEY,
          MAX(d.LAST_DAY_IN_MONTH_DATE_DT) AS LAST_DT
        FROM F_DAILY_ACCOUNT_SNAPSHOT das, D_DATE d
        WHERE 1=1
          AND d.CALENDAR_DATE_DT BETWEEN p_min_dt AND p_max_dt
          AND das.DATE_KEY  = d.DATE_KEY
        GROUP BY das.ACCOUNT_KEY
      ) a ON (a.MAX_DATE_KEY = das.DATE_KEY AND a.ACCOUNT_KEY = das.ACCOUNT_KEY)
      JOIN D_DATE ldom ON (ldom.CALENDAR_DATE_DT = a.LAST_DT)
    ) snaps ON (1=1
      AND snaps.ACCOUNT_KEY = sums.ACCOUNT_KEY
      AND snaps.PROGRAM_KEY = sums.PROGRAM_KEY
    )
  ) ab ON (1=1
    --
    -- find account by date
    --
    AND mas.ACCOUNT_KEY = ab.ACCOUNT_KEY
    AND mas.DATE_KEY    = ab.DATE_KEY
  )
  WHEN MATCHED THEN UPDATE
  SET
    --  mas.ACCOUNT_KEY                 = ab.ACCOUNT_KEY,
    mas.PROGRAM_KEY                     = ab.PROGRAM_KEY,
    --  mas.DATE_KEY                    = ab.DATE_KEY,
    mas.OUTSTANDING_CARDS_COUNT         = ab.OUTSTANDING_CARDS_COUNT,
    mas.TERMINAL_CARD_COUNT             = ab.TERMINAL_CARD_COUNT,
    mas.TOTAL_CARDS_COUNT               = ab.TOTAL_CARDS_COUNT,
    mas.CARDS_IN_TRANSITION_COUNT       = ab.CARDS_IN_TRANSITION_COUNT,
    /* virtual cards */
    mas.VIRTUAL_OUTSTANDING_CARD_COUNT  = ab.VIRTUAL_OUTSTANDING_CARD_COUNT,
    mas.VIRTUAL_TERMINAL_CARD_COUNT     = ab.VIRTUAL_TERMINAL_CARD_COUNT,
    mas.VIRTUAL_TOTAL_CARD_COUNT        = ab.VIRTUAL_TOTAL_CARD_COUNT,
    mas.VIRTUAL_CARDS_IN_TRANS_COUNT    = ab.VIRTUAL_CARDS_IN_TRANS_COUNT,
    /* mag cards */
    mas.MAG_OUTSTANDING_CARD_COUNT      = ab.MAG_OUTSTANDING_CARD_COUNT,
    mas.MAG_TERMINAL_CARD_COUNT         = ab.MAG_TERMINAL_CARD_COUNT,
    mas.MAG_TOTAL_CARD_COUNT            = ab.MAG_TOTAL_CARD_COUNT,
    mas.MAG_CARDS_IN_TRANS_COUNT        = ab.MAG_CARDS_IN_TRANS_COUNT,
    /* daily counts */
    mas.ISSUED_CARDS_COUNT              = ab.ISSUED_CARDS_COUNT,
    mas.TERMINATED_CARDS_COUNT          = ab.TERMINATED_CARDS_COUNT,
    mas.TRANS_CARDS_COUNT               = ab.TRANS_CARDS_COUNT,
    mas.VIRTUAL_ISSUED_CARDS_COUNT      = ab.VIRTUAL_ISSUED_CARDS_COUNT,
    mas.VIRTUAL_TERMINATED_CARDS_COUNT  = ab.VIRTUAL_TERMINATED_CARDS_COUNT,
    mas.VIRTUAL_TRANS_CARDS_COUNT       = ab.VIRTUAL_TRANS_CARDS_COUNT,
    mas.MAG_ISSUED_CARDS_COUNT          = ab.MAG_ISSUED_CARDS_COUNT,
    mas.MAG_TERMINATED_CARDS_COUNT      = ab.MAG_TERMINATED_CARDS_COUNT,
    mas.MAG_TRANS_CARD_COUNT            = ab.MAG_TRANS_CARD_COUNT,
    mas.ACTIVE_CARD_COUNT               = ab.ACTIVE_CARD_COUNT,
    /* house keeping */
    mas.ROW_CREATE_DTTM                 = ab.ROW_CREATE_DTTM,
    mas.ROW_LAST_MOD_DTTM               = ab.ROW_LAST_MOD_DTTM,
    mas.ROW_LAST_MOD_PROC_NM            = ab.ROW_LAST_MOD_PROC_NM,
    mas.ROW_LAST_MOD_PROC_SEQ_NBR       = ab.ROW_LAST_MOD_PROC_SEQ_NBR,
    /* the following added by venu 09/25/2013 */
    mas.MAX_OUTSTANDING_CARDS_COUNT     = ab.MAX_OUTSTANDING_CARDS_COUNT,
    mas.ACCOUNT_HIST_KEY                = ab.ACCOUNT_HIST_KEY
  WHEN NOT MATCHED THEN INSERT (
    mas.MONTHLY_ACCOUNT_SNAPSHOT_KEY,
    mas.ACCOUNT_KEY,
    mas.PROGRAM_KEY,
    mas.DATE_KEY,
    /* daily cards snapshot */
    mas.OUTSTANDING_CARDS_COUNT,
    mas.TERMINAL_CARD_COUNT,
    mas.TOTAL_CARDS_COUNT,
    mas.CARDS_IN_TRANSITION_COUNT,
    mas.VIRTUAL_OUTSTANDING_CARD_COUNT,
    mas.VIRTUAL_TERMINAL_CARD_COUNT,
    mas.VIRTUAL_TOTAL_CARD_COUNT,
    mas.VIRTUAL_CARDS_IN_TRANS_COUNT,
    mas.MAG_OUTSTANDING_CARD_COUNT,
    mas.MAG_TERMINAL_CARD_COUNT,
    mas.MAG_TOTAL_CARD_COUNT,
    mas.MAG_CARDS_IN_TRANS_COUNT,
    /* daily card  transactions */
    mas.ISSUED_CARDS_COUNT,
    mas.TERMINATED_CARDS_COUNT,
    mas.TRANS_CARDS_COUNT,
    mas.VIRTUAL_ISSUED_CARDS_COUNT,
    mas.VIRTUAL_TERMINATED_CARDS_COUNT,
    mas.VIRTUAL_TRANS_CARDS_COUNT,
    mas.MAG_ISSUED_CARDS_COUNT,
    mas.MAG_TERMINATED_CARDS_COUNT,
    mas.MAG_TRANS_CARD_COUNT,
    mas.ACTIVE_CARD_COUNT,
    /* house keeping fields */
    mas.ROW_CREATE_DTTM,
    mas.ROW_LAST_MOD_DTTM,
    mas.ROW_LAST_MOD_PROC_NM,
    mas.ROW_LAST_MOD_PROC_SEQ_NBR,
    /* the following added by venu 09/25/2013 */
    mas.MAX_OUTSTANDING_CARDS_COUNT,
    mas.ACCOUNT_HIST_KEY
  )
  VALUES (
    MON_ACCOUNT_SNAPSHOT_KEY_SEQ.NEXTVAL,
    ab.ACCOUNT_KEY,
    ab.PROGRAM_KEY,
    ab.DATE_KEY,
    ab.OUTSTANDING_CARDS_COUNT,
    ab.TERMINAL_CARD_COUNT,
    ab.TOTAL_CARDS_COUNT,
    ab.CARDS_IN_TRANSITION_COUNT,
    ab.VIRTUAL_OUTSTANDING_CARD_COUNT,
    ab.VIRTUAL_TERMINAL_CARD_COUNT,
    ab.VIRTUAL_TOTAL_CARD_COUNT,
    ab.VIRTUAL_CARDS_IN_TRANS_COUNT,
    ab.MAG_OUTSTANDING_CARD_COUNT,
    ab.MAG_TERMINAL_CARD_COUNT,
    ab.MAG_TOTAL_CARD_COUNT,
    ab.MAG_CARDS_IN_TRANS_COUNT,
    ab.ISSUED_CARDS_COUNT,
    ab.TERMINATED_CARDS_COUNT,
    ab.TRANS_CARDS_COUNT,
    ab.VIRTUAL_ISSUED_CARDS_COUNT,
    ab.VIRTUAL_TERMINATED_CARDS_COUNT,
    ab.VIRTUAL_TRANS_CARDS_COUNT,
    ab.MAG_ISSUED_CARDS_COUNT,
    ab.MAG_TERMINATED_CARDS_COUNT,
    ab.MAG_TRANS_CARD_COUNT,
    ab.ACTIVE_CARD_COUNT,
    ab.ROW_CREATE_DTTM,
    ab.ROW_LAST_MOD_DTTM,
    ab.ROW_LAST_MOD_PROC_NM,
    ab.ROW_LAST_MOD_PROC_SEQ_NBR,
    -- the following added by venu 09/25/2013
    ab.MAX_OUTSTANDING_CARDS_COUNT,
    ab.ACCOUNT_HIST_KEY
  );
  COMMIT;
  log4me.debug('complete ' || v_msg, '', p_ses_id);
END F_MONTHLY_ACCOUNT_AGG_BY_DATES;
/
show errors

CREATE OR REPLACE PROCEDURE F_MONTHLY_ACCOUNT_AGGREGATES
AS
/*
  NAME      : F_MONTHLY_ACCOUNT_AGGREGATES
  DECRIPTION: load Month Account Aggregates

  NOTE      :
    The Start Date and END DATE input paramters always needs to be the
    Calendar Start date and Calendar END Date even if we are trying to
    calculate the Aggrgates for a Partial Month.

  REVISION  :
  1.0 20100615 Izzy Reinish - Original
  2.0 20101011 Venugopal Kancharla - Features
  3.0 20140716 Lee - Modulize into F_MONTHLY_ACCOUNT_AGG_BY_DATES
*/
  SESSION_TYPE constant VARCHAR2(40) := 'F_MONTHLY_ACCOUNT_AGGREGATES';
  FMT_DT       constant varchar2(20) := 'YYYY-MM-DD';

  v_ses_id VARCHAR2(40) := to_char(SYSTIMESTAMP, 'YYYYMMDD.HH24MISS.FF4');
  v_proc   VARCHAR2(80);

  v_min_dt DATE;
  v_max_dt DATE;

  v_part   VARCHAR2(10);
  v_sql    CLOB;
BEGIN
  -- 11/09/2010 Following block added by Venu
  -- to make this procedure data driven and to improve performance
/*
  v_sql := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI1';
  EXECUTE IMMEDIATE v_sql;

  v_sql := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI2';
  EXECUTE IMMEDIATE v_sql;

  v_sql := 'DROP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI3';
  EXECUTE IMMEDIATE v_sql;
*/
  --
  -- get date range from data loaded in EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM
  --
  D_SEED_DATES(v_min_dt, v_max_dt);

  -- fix the issue with the last day of the month ... 05/29/2013 VK
  v_min_dt := v_min_dt - 1;
  v_max_dt := LAST_DAY(v_max_dt-1);   -- refer to the note above

  DBMS_OUTPUT.PUT_LINE(v_max_dt);

  v_proc :=
    SESSION_TYPE || ' ' ||
    to_char(v_min_dt, FMT_DT) || ' to ' ||
    to_char(v_max_dt, FMT_DT);
  log4me.info('INIT ' || v_proc, v_ses_id);

  -- ADDED 11/2/2010 by Izzy
  -- to allow proc to be run acroos multi months for historic loads
  FOR rec IN (
    SELECT
      d.CALENDAR_DATE_DT - d.DAY_IN_MONTH_NUMBER + 1 AS FIRST_OF_MONTH,
      d.CALENDAR_DATE_DT                             AS LAST_OF_MONTH,
      d.DATE_KEY                                     AS DATE_KEY
    FROM D_DATE d
    WHERE 1=1
      AND d.CALENDAR_DATE_DT BETWEEN v_min_dt AND v_max_dt
      AND d.LAST_DAY_IN_MONTH_FLG = 'Yes'
  )
  LOOP
    --
    -- work on one month at a time
    --
    F_MONTHLY_ACCOUNT_AGG_BY_DATES(
      rec.FIRST_OF_MONTH,
      rec.LAST_OF_MONTH,
      v_ses_id
    );
    --
    -- ADDED BY VENU on  11/02/2010
    -- to correct purchase device count
    --
    UPDATE F_MONTHlY_ACCOUNT_SNAPSHOT f
    SET
      f.ACTIVE_CARD_COUNT = 0
    WHERE DATE_KEY = rec.DATE_KEY;
    COMMIT;

    v_part := 'YM_' || TO_CHAR(rec.LAST_OF_MONTH, 'YYYY_MM');
    DBMS_OUTPUT.PUT_LINE(v_part);

    v_sql := '
    MERGE INTO F_MONTHLY_ACCOUNT_SNAPSHOT mas USING (
      SELECT
        a.acct_key      AK,
        COUNT(a.pd_key) PD_CNT
      FROM (
        SELECT
          MAX(f.ACCOUNT_KEY)  ACCT_KEY,
          PURCHASE_DEVICE_KEY PD_KEY
        FROM
          F_TRANSACTION_LINE_ITEM PARTITION(' || v_part || ') tl,
          D_ACCOUNT_HIST_VW d,
          D_ACCOUNT_HIST_VW df,
          F_MONTHLY_ACCOUNT_SNAPSHOT f
        WHERE 1=1
          AND d.ACCOUNT_KEY       = tl.PURCHASE_ACCOUNT_KEY
          AND f.ACCOUNT_KEY       = df.ACCOUNT_KEY
          AND d.ACCOUNT_HIST_KEY  = df.ACCOUNT_HIST_KEY
          AND f.DATE_KEY          =  ' || rec.DATE_KEY || '
          AND tl.PURCHASE_DEVICE_KEY <> 0
          AND tl.GROSS_SPEND_AMOUNT  <> 0
        GROUP BY
          tl.PURCHASE_DEVICE_KEY
      ) a
      GROUP BY
        a.acct_key
    ) b ON (mas.ACCOUNT_KEY = b.AK)
    WHEN MATCHED THEN UPDATE
    SET
      mas.ACTIVE_CARD_COUNT = b.PD_CNT
    WHERE mas.DATE_KEY = ' || rec.DATE_KEY
    ;
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
  END LOOP;

 /*
  v_sql := '
    CREATE BITMAP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI1
    ON F_MONTHLY_ACCOUNT_SNAPSHOT(ACCOUNT_KEY)
    TABLESPACE I_BI
    NOLOGGING
    COMPUTE STATISTICS
  ';
  EXECUTE IMMEDIATE v_sql;

  v_sql := '
    CREATE BITMAP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI2
    ON F_MONTHLY_ACCOUNT_SNAPSHOT(PROGRAM_KEY)
    TABLESPACE I_BI
    NOLOGGING
    COMPUTE STATISTICS
  ';
  EXECUTE IMMEDIATE v_sql;

  v_sql := '
    CREATE BITMAP INDEX F_MONTHLY_ACCOUNT_SNAPSHOT_UI3
    ON F_MONTHLY_ACCOUNT_SNAPSHOT(DATE_KEY)
    TABLESPACE I_BI
    NOLOGGING
    COMPUTE STATISTICS
  ';
  EXECUTE IMMEDIATE v_sql;
*/
  log4me.info('DONE ' || v_proc, v_ses_id);
EXCEPTION
  WHEN OTHERS THEN
    log4me.err('FAIL ' || SQLCODE || ' ' || v_proc, v_ses_id);
END F_MONTHLY_ACCOUNT_AGGREGATES;
/
show errors
