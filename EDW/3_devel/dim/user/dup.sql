select *
from d_user
where user_id in (
  select user_id 
  from d_user 
  group by user_id 
  having count(*) > 1
)
order by user_id
;
