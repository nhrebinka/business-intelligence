SET ECHO         ON
SET SERVEROUTPUT ON
SET NEWPAGE      0
SET SPACE        0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL    ON
SET TAB          OFF
SET ESCAPE       ON
SET LINESIZE     2000

SPOOL /i1/&env/hub/logs/dba_oneshot_predw_HT281016.log;
-- ---------------------------------------------------------------------------
--
-- Domain: EDM and EDW
-- Table : D_USER
-- Spec  : A one shot update to add USER_HIST_KEY field for Type 2 conformation
--         It links back to the original record before the change.
-- Note  : We are assuming the simple logic of USER_ID is sufficient to identify
--         The user. At the time of 20140408, there were 1066 records out of 
--         25K or so users were updated. They were also validated manually with
--         the following rules
--         1. same USER_ID 
--         2. same FIRST_NAME
--         3. same LAST_NAME (some possibly due to last name change after marriage)
--         4. different ROLE, this is most of the case.
--
-- History:
--   20140408: Cho Lee - first version
--
-- ---------------------------------------------------------------------------
--
-- Add the new (or forgotten) hist_key column
--
alter table d_user add user_hist_key integer;
--
-- We do the one shot update for all the "current" records
--
update d_user set user_hist_key = user_key where current_record_flg = 1;
--
-- Oracle does not allow update on join, so we use the mighty MERGE
--
merge into d_user u1
using (
  select min(user_key) min_key, user_id from d_user
  group by user_id
) u0
on (
  1=1
  and u1.user_id = u0.user_id
  -- and maybe other fields like first_name, ...
)
when matched then update set user_hist_key = u0.min_key;
--
--
select count(*) from d_user where user_hist_key is null;
-- 
-- Hopefully, we get a 0 back. If not, start scratching your head.
-- Now, all records are updated we can turn the field NOT NULL
--
-- 20140409 CC - commented the following out. We will use ETL to enforce NOT NULL
--
-- alter table d_user modify user_hist_key integer not null;
--
-- We should add an index to our new field in case some one need to join to it
--
create index d_user_i1 on d_user(
  user_hist_key
) 
tablespace i_bi;
--
-- All done!
--
COMMIT;
SPOOL OFF;
/
-- ---------------------------------------------------------------------------



