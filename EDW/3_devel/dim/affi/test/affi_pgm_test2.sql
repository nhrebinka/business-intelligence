--
-- unit test case for affinity program check store prcedure
--
declare
  X_PRC         varchar2(100) := 'affi_pgm_test2';
  X_UPD         varchar2(100) := '%0454%';
  X_PART        varchar2(20)  := 'YM_2015_04';
  X_OLD_PGM_KEY varchar2(20)  := '700';

  X_FIND CLOB := '
  SELECT transaction_line_item_key FROM (
    SELECT /*+ parallel(x,64) */
      transaction_line_item_key,
      row_number() over (
        partition by program_key, purchase_account_key 
        order by transaction_line_item_key) rn
    FROM f_transaction_line_item partition (ym_2015_04) x
    WHERE revenue_date_key = 12875
    AND program_key in (700, 47, 601, 607, 715)
    AND purchase_account_key in (7594112, 7594136, 7594147, 7594182)
  )
  WHERE rn=1    
  ';
  X_SQL CLOB := '
    UPDATE /*+ parallel(x, 64) */ 
      f_transaction_line_item partition (' || X_PART || ') x
    SET program_key = :a,
        row_last_mod_proc_nm=:b,
        row_last_mod_dttm =sysdate
    WHERE transaction_line_item_key := :k
  ';
  X_PGM TokList := TokList(
    '47', '601', '607', '715'
  );
  X_VEC TokList := TokList(     -- exists in earlier date
    '7594112', -- pkey=47,  3 txn
    '7594136', -- pkey=601, 17 txn
    '7594147', -- pkey=607, 2 txn
    '7594182'  -- pkey=715, 1 txn
  );

  v_key  integer;
  v_msg  varchar2(200);
  --
  -- setting up and teardown test vectors
  --
  procedure setup(p_init boolean := TRUE) as
    v_from varchar2(20);
    v_to   varchar2(20);
  begin
    for i in 1..X_VEC.COUNT loop
      begin
        v_from := case when p_init then X_OLD_PGM_KEY else X_PGM(i) end;
        v_to   := case when p_init then X_PGM(i) else X_OLD_PGM_KEY end;
        v_msg  := X_PRC || 
          case when p_init then ' setup ' else ' reset ' end || 
          X_VEC(i) || ' [' || v_from || ' => ' || v_to || ']';
        execute immediate X_SQL using
          cast(v_to as integer),
          '0454 one-shot; ' || v_msg,
          cast(v_from as integer),
          cast(X_VEC(i) as integer);
        log4me.debug(v_msg || ' => ' || SQL%ROWCOUNT || ' rows updated', X_SQL);
        commit;

      exception
      when others then
        rollback;
        log4me.err(v_msg);
      end;
    end loop;
  end setup;

  procedure teardown as
  begin
    setup(FALSE);
  end teardown;
  --
  -- verify test vectors
  --
  procedure check_vectors as
--    type rec_tt is table of f_transaction_line_item%ROWTYPE;
    type rec_tt is table of integer;
    v_list rec_tt;
  begin
    v_msg := X_PRC || ' vector';
    execute immediate 
      X_FIND || ' ORDER BY program_key, purchase_account_key'
    bulk collect into v_list;
    log4me.debug(
      v_msg || ' => ' || v_list.count || ' rows', X_FIND
    );
    for i in 1..v_list.count loop
      log4me.debug(v_msg || ' ' ||
        'tli=' || v_list(i)
--        'ac=' || v_list(i).purchase_account_key,
--        'dt='  || v_list(i).revenue_date_key          || ', ' || 
--        'tli=' || v_list(i).transaction_line_item_key || ', ' ||
--        'pg='  || v_list(i).program_key
      );
    end loop;
  end check_vectors;

begin
  log4me.info('*** ' || X_PRC || ' INIT');
  log4me.info('*** ' || X_PRC || ' SETTING UP...');
  setup;
  check_vectors;
  --
  -- vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  --
  log4me.info('*** ' || X_PRC || ' TEST STARTS HERE...');
  f_daily_affinity_program_check;
  check_vectors;
  log4me.info('*** ' || X_PRC || ' TEST ENDS HERE...');
  --
  -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  --
  log4me.info('*** ' || X_PRC || ' TEARING DOWN...');
  teardown;
  check_vectors;
  log4me.info('*** ' || X_PRC || ' DONE');
end;
/
show errors
  
