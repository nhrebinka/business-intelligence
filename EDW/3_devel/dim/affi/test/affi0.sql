-- purchase_account_key associates to multiple program_key
select purchase_account_key, count(*)
from (
  SELECT /*+ parallel 64 */
    unique
    PURCHASE_ACCOUNT_KEY,
    PROGRAM_KEY
  FROM F_TRANSACTION_LINE_ITEM partition (ym_2014_04)
)
group by purchase_account_key
having count(*) > 1



