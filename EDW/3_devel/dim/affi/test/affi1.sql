--
-- Find account with multiple programs in F_TRANSACTION_LINE_ITEM table
--
SELECT /*+ full(f) parallel(f, 64) cache(dp) */
  f.PURCHASE_ACCOUNT_KEY, 
  f.PROGRAM_KEY
FROM F_TRANSACTION_LINE_ITEM partition (ym_2014_04) f, D_PROGRAM dp
WHERE 1=1
  AND f.PROGRAM_KEY = DP.PROGRAM_KEY
  AND dp.PROGRAM_SPONSOR_NAME NOT IN ('Affin 53')
  AND dp.PROGRAM_SPONSOR_NAME NOT IN ('Affinity 54')
  AND f.PURCHASE_ACCOUNT_KEY IN (
  -- purchase_account_key associates to multiple program_key
  SELECT PURCHASE_ACCOUNT_KEY
  FROM (
    SELECT /*+ full(x) parallel(x, 64) cache(p) */
      x.PURCHASE_ACCOUNT_KEY, 
      x.PROGRAM_KEY
    FROM F_TRANSACTION_LINE_ITEM partition (ym_2014_04) x, D_PROGRAM p
    WHERE p.PROGRAM_NAME = ('Affinity')
      AND p.PROGRAM_KEY  = x.PROGRAM_KEY
    GROUP BY x.PURCHASE_ACCOUNT_KEY, x.PROGRAM_KEY
  )
  GROUP BY PURCHASE_ACCOUNT_KEY
  HAVING COUNT (*) > 1
)
GROUP BY f.PURCHASE_ACCOUNT_KEY, f.PROGRAM_KEY
