--
-- test case for affinity program check store prcedure
--
declare
  X_PRC         varchar2(100) := 'affi_pgm_test';
  X_OLD_PGM_KEY integer       := 701;
  X_NEW_PGM_KEY integer       := 634;

  X_FIND CLOB := '
    SELECT /*+ parallel(x,64) */ * 
    FROM f_purchase_device_event x
    WHERE date_key >= 12500
    AND   row_last_mod_proc_nm like ''' || X_PRC || '%''
  ';
  X_INIT CLOB := '
    UPDATE /*+ parallel(x, 64) */ f_purchase_device_event
    SET program_key =' || X_NEW_PGM_KEY || ',
        row_last_mod_proc_nm=''' || X_PRC || ''',
        row_last_mod_dttm =sysdate
    WHERE program_key = ' || X_OLD_PGM_KEY || ' AND
  ';
  X_RESET CLOB := '
    UPDATE /*+ parallel(x, 64) */ f_purchase_device_event
    SET program_key =' || X_OLD_PGM_KEY || ',
        row_last_mod_proc_nm=''' || X_PRC || ' reset' || ''',
        row_last_mod_dttm =sysdate
    WHERE program_key = ' || X_NEW_PGM_KEY || ' AND
  ';
  X_VEC TokList := TokList(                   -- exists in earlier date
    'account_key=7613618 AND date_key=12539', -- 12547
    'account_key=8000695 AND date_key=12539', -- 12546
    'account_key=9001244 AND date_key=12551', -- 12548
    'account_key=9002361 AND date_key=12551'  -- 12548
  );

  v_msg  varchar2(200);
  v_cond varchar2(200);
  v_sql  CLOB;
  --
  -- setting up and teardown test vectors
  --
  procedure setup(p_init boolean := TRUE) as
  begin
    for i in 1..X_VEC.COUNT loop
      v_cond := X_VEC(i);

      begin
        v_msg := X_PRC || 
          CASE WHEN p_init THEN ' update ' ELSE ' reset ' END || v_cond;
        v_sql := CASE WHEN p_init THEN X_INIT ELSE X_RESET END || v_cond;
        EXECUTE IMMEDIATE v_sql;
        log4me.debug(v_msg || ' => ' || SQL%ROWCOUNT || ' rows', v_sql);
        commit;

      exception
      when others then
        rollback;
        log4me.err(v_msg);
      end;
    end loop;
  end setup;

  procedure teardown as
    v_acct_lst TokList := TokList(7613618, 8000695, 9001244, 9002361);
    v_part0    varchar2(20) := 'YM_2014_05';
  begin
    setup(FALSE);
    for i in 1..v_acct_lst.COUNT loop
      XOS_ACCOUNT_AFFINITY_FIX(
        X_PRC || ' reset[' || X_NEW_PGM_KEY || ' => ' || X_OLD_PGM_KEY || ']',
        cast(v_acct_lst(i) as integer),
        X_NEW_PGM_KEY,
        X_OLD_PGM_KEY,
        v_part0
      );
    end loop;
  end teardown;
  --
  -- verify test vectors
  --
  procedure check_vectors as
    type rec_tt is table of f_purchase_device_event%ROWTYPE;
    v_list rec_tt;
  begin
    v_msg := X_PRC || ' vector';
    v_sql := X_FIND;
    execute immediate v_sql bulk collect into v_list;
    log4me.debug(v_msg || ' => ' || v_list.count || ' rows', v_sql);

    for i in 1..v_list.count loop
      log4me.debug(v_msg || ' ac=' || v_list(i).account_key,
        'dt='  || v_list(i).date_key                  || ', ' || 
        'pde=' || v_list(i).purchase_device_event_key || ', ' ||
        'pg='  || v_list(i).program_key
      );
    end loop;
  end check_vectors;

begin
  log4me.info('*** ' || X_PRC || ' SETTING UP...');
  setup;
  check_vectors;
  --
  -- starting the test
  --
  f_daily_affinity_program_check;
  --
  -- validate the results
  --
  check_vectors;

  log4me.info('*** ' || X_PRC || ' TEARING DOWN...');
  teardown;

  check_vectors;
  log4me.info('*** ' || X_PRC || ' DONE.');
end;
/

  
