with 
p701 as (
  select /*+ parallel(x,64) */ rowid
  from f_purchase_device_event x 
  where program_key in (700, 701)
  and row_last_mod_dttm > to_date('20140501','YYYYMMDD')
)
select rowidtochar(rowid), x.* 
from f_purchase_device_event x 
where rowid in (
  select * from p701
)
order by row_last_mod_dttm desc
; 
