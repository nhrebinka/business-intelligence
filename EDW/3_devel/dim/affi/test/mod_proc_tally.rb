require 'oracle'

Oracle.new do |o|
    (2012..2014).each do |y|
        (1..12).each do |m|
            pati = "ym_#{y}_#{'%02d' % m}"
            sql = "
select /*+ parallel 64 */
'#{pati}', row_last_mod_proc_nm, count(*)
from f_transaction_line_item partition (#{pati})
group by row_last_mod_proc_nm
order by row_last_mod_proc_nm
"
            o.run(sql) do |r|
                puts r.join('|')
            end
        end
    end
end

__END__
  
