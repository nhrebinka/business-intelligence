--
-- Find account with multiple programs in F_PURCHASE_DEVICE_EVENT table
--
SELECT /*+ full(f) parallel(f, 64) cache(dp) */
  f.ACCOUNT_KEY, 
  f.PROGRAM_KEY
FROM F_PURCHASE_DEVICE_EVENT f, D_PROGRAM dp
WHERE 1=1
  AND f.PROGRAM_KEY   = DP.PROGRAM_KEY
  AND dp.PROGRAM_SPONSOR_NAME NOT IN ('Affin 53')
  AND dp.PROGRAM_SPONSOR_NAME NOT IN ('Affinity 54')
  AND f.ACCOUNT_KEY IN ( 
  SELECT ACCOUNT_KEY FROM (
    SELECT /*+ full(x) parallel(x, 64) cache(p) */
      x.ACCOUNT_KEY,  x.PROGRAM_KEY
    FROM F_PURCHASE_DEVICE_EVENT x, D_PROGRAM p
    WHERE 1=1
      AND p.PROGRAM_NAME = 'Affinity'
      AND p.PROGRAM_KEY  = x.PROGRAM_KEY
    GROUP BY x.ACCOUNT_KEY, x.PROGRAM_KEY
  )
  GROUP BY ACCOUNT_KEY HAVING count(*) > 1
)
GROUP BY f.ACCOUNT_KEY, f.PROGRAM_KEY
;
