--
-- Affinity accounts: 
--   16941: All
--    7136: program_sponsor_name not in ('Affin 53', 'Affinity 54')
--
  select /*+ parallel 64 */
    edm_acct_key
  from  m_acct
  where edm_spnr_pgm_key in (
    select edm_spnr_pgm_key 
    from   m_spnr_pgm
    where 1=1
      and pgm_nm like '%Affi%'
      and program_sponsor_name not in ('Affin 53', 'Affinity 54')
  )
;

--
-- find duplicated accounts
--
select /*+ parallel 64 */
  count(*), dw_source_sys, acct_source_pk
from  m_acct_xref
where edm_acct_key in (
  select /*+ parallel 64 */
    edm_acct_key
  from  m_acct
  where edm_spnr_pgm_key in (
    select edm_spnr_pgm_key 
    from   m_spnr_pgm
    where 1=1
      and pgm_nm like '%Affi%'
      and program_sponsor_name not in ('Affin 53', 'Affinity 54')
  )
)
group by dw_source_sys, acct_source_pk
having count(*) > 1
