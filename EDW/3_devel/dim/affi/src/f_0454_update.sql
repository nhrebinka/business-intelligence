--
-- from Satheesh
--
MERGE /*+ parallel */
     INTO
       EDW_OWNER.F_TRANSACTION_LINE_ITEM FT
  USING
       (  SELECT
               DA.ACCOUNT_KEY, DP.PROGRAM_KEY
          FROM
               LINK_OWNER.M_ACCT MA,
               LINK_OWNER.D_ACCOUNT DA,
               LINK_OWNER.M_SPNR_PGM PG,
               LINK_OWNER.D_PROGRAM DP
          WHERE
               DA.SOURCE_ACCOUNT_ID = MA.WEX_ACCT_NBR
        --AND DA.CURRENT_RECORD_FLG = 1
        AND    MA.EDM_LAST_UPDT_SESSION_NM = 'affinity 0454 one shot update'
        AND    MA.EDM_SPNR_PGM_KEY = PG.EDM_SPNR_PGM_KEY
        AND    PG.PROGRAM_ID = DP.PROGRAM_ID) S
  ON
       (S.ACCOUNT_KEY = FT.PURCHASE_ACCOUNT_KEY)
WHEN MATCHED THEN
  UPDATE SET
    FT.PROGRAM_KEY           = S.PROGRAM_KEY,
    FT.ROW_LAST_MOD_PROC_NM  = 'affinity 0454 one shot update'
