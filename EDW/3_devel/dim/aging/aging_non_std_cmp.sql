--
-- non-standardard aging without min dates
-- note: the result turns out the same as with min dates
--
SELECT
  pwcd.BUSINESS_DATE,
  pia.CUST_ID,
  SUM(pia.ENTRY_AMT) TOTAL
FROM PS_ITEM_ACTIVITY pia
JOIN PS_ITEM pi ON (1=1 
  AND pia.ITEM          = pi.ITEM
  AND pia.CUST_ID       = pi.CUST_ID
  AND pia.BUSINESS_UNIT = pi.BUSINESS_UNIT
)
JOIN PS_WX_CUST_DAILY pwcd ON (1=1
  AND pia.CUST_ID       = pwcd.CUST_ID
  AND pia.BUSINESS_UNIT = pwcd.BUSINESS_UNIT
  AND pia.POST_DT      <= pwcd.BUSINESS_DATE
  AND pwcd.BUSINESS_DATE BETWEEN TO_DATE ('04/01/2014', 'MM/DD/YYYY') AND TO_DATE ('04/01/2014', 'MM/DD/YYYY')
)
JOIN (
  SELECT CUST_ID FROM (
    SELECT /*+ parallel 16 */ 
      CUST_ID, AGING_ID,
      ROW_NUMBER() OVER (PARTITION BY CUST_ID ORDER BY EFFDT DESC) rn 
    FROM  PS_CUST_CREDIT
  )
  WHERE rn=1
    AND AGING_ID<>'STD'
) mcc ON (mcc.CUST_ID = pia.CUST_ID)
WHERE 1=1
AND pia.CUST_ID ='0401008824617' 
GROUP BY 
  pwcd.BUSINESS_DATE,
  pia.CUST_ID
HAVING SUM(pia.ENTRY_AMT)<>0
;

-- current
SELECT
  PWCD.BUSINESS_DATE,
  PIA.CUST_ID,
  PIA.ITEM,
  SUM (PIA.ENTRY_AMT) TOTAL
FROM PS_ITEM_ACTIVITY PIA, PS_ITEM PI, PS_WX_CUST_DAILY PWCD
, (
  SELECT CUST_ID, AGING_ID
  FROM PS_CUST_CREDIT
  WHERE (CUST_ID, EFFDT) IN (
    SELECT MCC1.CUST_ID, MAX (MCC1.EFFDT) EFFDT
    FROM PS_CUST_CREDIT MCC1
    GROUP BY MCC1.CUST_ID)
) MCC
WHERE 1=1
  AND PWCD.BUSINESS_DATE BETWEEN TO_DATE ('04/01/2014', 'MM/DD/YYYY')
                         AND TO_DATE ('04/01/2014', 'MM/DD/YYYY')
  AND PIA.CUST_ID         = PWCD.CUST_ID
  AND PIA.BUSINESS_UNIT  = PWCD.BUSINESS_UNIT
  AND PIA.POST_DT        <= PWCD.BUSINESS_DATE
  AND PIA.ITEM           = PI.ITEM
  AND PIA.CUST_ID        = PI.CUST_ID
  AND PIA.BUSINESS_UNIT = PI.BUSINESS_UNIT
  AND PIA.CUST_ID = MCC.CUST_ID
  AND MCC.AGING_ID != 'STD'
  AND PIA.CUST_ID ='0401008824617' 
HAVING SUM(PIA.ENTRY_AMT) <> 0
GROUP BY PWCD.BUSINESS_DATE, PIA.CUST_ID, PIA.ITEM
;
