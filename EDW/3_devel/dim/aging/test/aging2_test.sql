SELECT 
  BUSINESS_DATE,
  CUST_ID,
  SUM(AGE_00_TEMP),
  SUM(AGE_99_TEMP),
  SUM(AGE_30_TEMP),
  SUM(AGE_60_TEMP),
  SUM(AGE_90_TEMP),
  SUM(AGE_120_TEMP),
  SUM(AGE_150_TEMP),
  SUM(AGE_180_TEMP)
FROM (  
  SELECT /*+ PARALLEL(PI, 4) FULL(PI) INDEX_FFS(PIA, PSCITEM_ACTIVITY) */
    pwcd.BUSINESS_DATE,
    pia.CUST_ID,
    pia.ITEM,
    SUM(pia.ENTRY_AMT),
    MIN(pi.ACCOUNTING_DT),
    MIN(pi.DUE_DT),
    (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) AR_AGE_OLD_DAY,
    (pwcd.BUSINESS_DATE - MIN(pi.DUE_DT)) REG_AGE_OLD_DAY,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) >= 181
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_180_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 151 AND 180
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_150_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 121 AND 150
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_120_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 91 AND 120
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_90_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 61 AND 90
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_60_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 31 AND 60
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_30_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) BETWEEN 0 AND 30
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_00_TEMP,
    CASE
      WHEN (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) < 0
      THEN
        SUM(pia.ENTRY_AMT)
      END AGE_99_TEMP
  FROM 
    PS_ITEM_ACTIVITY partition (ym_2014_05) pia,
    PS_ITEM          partition (ym_2014_05) pi, 
    PS_WX_CUST_DAILY pwcd, (
      SELECT /*+ parallel(PS_CUST_CREDIT, 4) */
        CUST_ID, 
        AGING_ID
      FROM PS_CUST_CREDIT
      WHERE (CUST_ID, EFFDT) IN (
        SELECT 
          mcc1.CUST_ID, 
          MAX(mcc1.EFFDT) EFFDT
        FROM PS_CUST_CREDIT mcc1
        GROUP BY mcc1.CUST_ID
      )
    ) mcc
  WHERE pwcd.BUSINESS_DATE BETWEEN TO_DATE('05/28/2014', 'MM/DD/YYYY')
                               AND TO_DATE('05/28/2014', 'MM/DD/YYYY')
    AND pia.POST_DT      <= pwcd.BUSINESS_DATE
    AND pia.CUST_ID       = pwcd.CUST_ID
    AND pia.BUSINESS_UNIT = pwcd.BUSINESS_UNIT
    --
    AND pia.ITEM          = pi.ITEM
    AND pia.CUST_ID       = pi.CUST_ID
    AND pia.BUSINESS_UNIT = pi.BUSINESS_UNIT
--    AND pia.POST_DT       = pi.POST_DT
    AND pia.CUST_ID       = mcc.CUST_ID
    AND mcc.AGING_ID != 'STD'
/*
    AND pia.CUST_ID IN (
     '9100002198268'
     , '9100000215622' 
     , '6133004806006'
     , '6131005289434'
    )
*/
  HAVING SUM(pia.ENTRY_AMT) <> 0
  GROUP BY pwcd.BUSINESS_DATE, pia.CUST_ID, pia.ITEM
)
GROUP BY BUSINESS_DATE, CUST_ID
