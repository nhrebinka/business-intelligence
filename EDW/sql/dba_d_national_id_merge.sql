SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000



spool /i1/&&env/hub/logs/dba_d_national_id_merge.log ; 
  

merge  into D_ACCOUNT D  using EDM_OWNER.D_ACCOUNT@PRHUB.PROD  M 
 on  ( D.ACCOUNT_KEY   = M.ACCOUNT_KEY  ) 
 WHEN MATCHED THEN 
   UPDATE SET 
                  D.NATIONAL_ACCOUNT_ID  = M.NATIONAL_ACCOUNT_ID  ,
                  D.ROW_LAST_MOD_DTTM    = sysdate  ,
                  D.ROW_LAST_MOD_PROC_NM   = 'Data Fix for NATIONAL ACCOUNT ID update from EDM_OWNER  D_Account' ; 
                  
commit ; 

  
spool off ; 
    

  


