SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_s_opty.dat ;  

SELECT A.ROW_ID
  || '~|~'
  || TO_CHAR ( A.CREATED, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.CREATED_BY,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR (A.LAST_UPD, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.LAST_UPD_BY,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.DCKING_NUM
  || '~|~'
  || A.MODIFICATION_NUM
  || '~|~'
  || REPLACE(REPLACE(A.CONFLICT_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.BU_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.NAME,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_BU_DNRM_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_BU_MANL_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_BU_SYS_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.CURCY_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.ENTERPRISE_FLAG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_REP_DNRM_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_REP_MANL_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_REP_SYS_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.ACTL_CLS_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.ALIAS_NAME,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.ASGN_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.ASGN_USR_EXCLD_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.BDGTED_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.BDGT_AMT
  || '~|~'
  || REPLACE(REPLACE(A.BDGT_AMT_CURCY_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.BDGT_AMT_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.BDGT_FLG_UPD_BY,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.CHANNEL_TYPE_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.CMPT_STATUS_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.COMPETITION_DESC,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.CURR_STG_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.CUST_URGENCY_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.DESC_TEXT,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.EVAL_PERIOD
  || '~|~'
  || TO_CHAR(A.EVAL_START_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.EXCH_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.FUND_STAT_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.INTEGRATION_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.LEAD_QUALITY_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.OPTY_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.OU_ASGN_PH_AC,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.OU_ASGN_PH_CC,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.OU_ROLE_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.OU_STAT_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PAR_OPTY_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PREF_LANG_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PRIV_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PROG_NAME,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_CMPT_OU_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_CON_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_CS_PATH_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_DEPT_OU_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_INT_DEPT_OU_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_MKT_SEG_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_OU_ADDR_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_OU_INDUST_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_PER_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_POSTN_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_PROD_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_QUOTE_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_REP_ASGN_TYPE,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_SRC_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_TERR_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.REASON_WON_LOST_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.REVENUE_CLASS,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.SALES_METHOD_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.STG_NAME,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.STG_START_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.WIN_CONFIDENCE_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_01,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.X_ATTRIB_02
  || '~|~'
  || A.X_ATTRIB_03
  || '~|~'
  || A.X_ATTRIB_04
  || '~|~'
  || A.X_ATTRIB_05
  || '~|~'
  || A.X_ATTRIB_06
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_07,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_08,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_09,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_10,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_11,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_12,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_13,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_14,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_15,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_16,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_17,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_18,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_19, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_20, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_21,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_22,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_23, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_24, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_25,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_26,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_27,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_28,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_29,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_30,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.X_ATTRIB_31
  || '~|~'
  || A.X_ATTRIB_32
  || '~|~'
  || A.X_ATTRIB_33
  || '~|~'
  || A.X_ATTRIB_34
  || '~|~'
  || A.X_ATTRIB_35
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_36,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_37,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_38,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_39,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_40,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_41, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_42, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_43, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_44, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || TO_CHAR(A.X_ATTRIB_45, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.X_ATTRIB_46,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.X_ATTRIB_47
  || '~|~'
  || A.X_ATTRIB_48
  || '~|~'
  || TO_CHAR(A.BDGT_AVAILABLE_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.CAMP_CON_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.EXEC_PRIORITY_TS, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || A.NUM_RC_PERIODS
  || '~|~'
  || REPLACE(REPLACE(A.PR_OPTY_INDUST_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_PROJ_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.PR_PRTNR_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.RC_PERIOD_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.STATUS_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.SUM_CLASS_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.SUM_COST_AMT
  || '~|~'
  || A.SUM_DOWNSIDE_AMT
  || '~|~'
  || TO_CHAR(A.SUM_EFFECTIVE_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || A.SUM_MARGIN_AMT
  || '~|~'
  || A.SUM_REVN_AMT
  || '~|~'
  || REPLACE(REPLACE(A.SUM_REVN_ITEM_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.SUM_TYPE_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.SUM_UPSIDE_AMT
  || '~|~'
  || A.SUM_WIN_PROB
  || '~|~'
  || REPLACE(REPLACE(A.EXEC_PRIORITY_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.REVN_SPLIT_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.SUM_COMMIT_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_CREDIT_STATUS,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.X_PRSP_CR_LIMIT
  || '~|~'
  || REPLACE(REPLACE(A.X_APPSTAT_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_APSTAT_REASON_CD,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.X_CONTRACT_EXP_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.X_CONTRACT_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.X_DIDS_SENT_DT, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.X_FLT_BILLABLE_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(A.X_FLT_DATA_FLG,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || A.X_REQ_CRDT_LN_AMT
  || '~|~'
  || REPLACE(REPLACE(A.X_STATION_APP_CODE_ID,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || TO_CHAR(A.DB_LAST_UPD, 'MM/DD/YYYY hh24:mi:ss')
  || '~|~'
  || REPLACE(REPLACE(A.DB_LAST_UPD_SRC,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(M_POSTN.login,CHR(13),CHR(32)),CHR(10),CHR(32)) 
  || '~|~'
  || REPLACE(REPLACE(M.login,CHR(13),CHR(32)),CHR(10),CHR(32))
  || '~|~'
  || REPLACE(REPLACE(F.name,CHR(13),CHR(32)),CHR(10),CHR(32)) 
  || '~|~'
  || REPLACE(REPLACE(b.loc,CHR(13),CHR(32)),CHR(10),CHR(32)) 
FROM SIEBEL.S_OPTY A,
  SIEBEL.s_org_ext b,
  SIEBEL.s_src D,
  SIEBEL.s_doc_agree E,
  SIEBEL.s_stg F,
  SIEBEL.s_postn L,
  SIEBEL.s_user M,
  SIEBEL.s_user M_POSTN
WHERE A.pr_src_id          = D.row_id
AND a.pr_dept_ou_id        = b.row_id
AND A.x_attrib_28          = E.row_id
AND A.curr_stg_id          = F.row_id
AND A.created_by           = M.row_id(+)
AND A.pr_postn_id          = L.row_id(+)
AND L.pr_emp_id            = M_POSTN.row_id(+)
AND SUBSTR (F.name, 1, 2) IN ('06', '10') ; 



spool off;

