SET ECHO         ON
SET NEWPAGE      0
SET SPACE        0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL    ON
SET TAB         OFF
set LINESIZE    2000

-- Modify src_sys_last_upd_dt, so source to m siebel map update all the mp accounts(Force update)

spool /i1/&&env/hub/logs/m_acct_upd.log;
 
UPDATE EDM_OWNER.M_ACCT MA
   SET MA.SRC_SYS_LAST_UPDT_DT = '05-JUN-2004'
 WHERE MA.WEX_ACCT_NBR IN
          (SELECT MA.WEX_ACCT_NBR
             FROM EDM_OWNER.M_ACCT MA, EDM_OWNER.M_ACCT_XREF MX
            WHERE MA.EDM_ACCT_KEY = MX.EDM_ACCT_KEY
                  AND MX.DW_SOURCE_SYS = 'SIEBEL');


COMMIT;


-- update natioanl_account_id attribute in M_ACCT to null, since after this change there is no mpping for this attribute


 
UPDATE EDM_OWNER.M_ACCT MA
   SET MA.NATIONAL_ACCOUNT_ID = NULL
 WHERE MA.WEX_ACCT_NBR IN
          (SELECT MA.WEX_ACCT_NBR
             FROM EDM_OWNER.M_ACCT MA, EDM_OWNER.M_ACCT_XREF MX
            WHERE     MA.EDM_ACCT_KEY = MX.EDM_ACCT_KEY
                  AND MX.DW_SOURCE_SYS = 'SIEBEL'
                  AND MA.NATIONAL_ACCOUNT_ID IS NOT NULL);
 COMMIT;
 
spool off