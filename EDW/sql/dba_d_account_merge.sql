SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000
DEFINE &&env = &1


spool /i1/&&env/hub/logs/dba_d_account_merge.log ; 
  

merge  into D_ACCOUNT D  using EDM_OWNER.D_ACCOUNT@PRHUB.PROD  M 
 on  ( D.ACCOUNT_KEY   = M.ACCOUNT_KEY  ) 
 WHEN MATCHED THEN 
   UPDATE SET 
                  D.ROW_EFF_END_DTTM        =   M.ROW_EFF_END_DTTM       ,    
                  D.ROW_LAST_MOD_DTTM       =   sysdate  ,
                  D.ROW_LAST_MOD_PROC_NM    =  'Data Fix for ROW_EFF_END_DTTM' 
   WHERE D.ROW_EFF_END_DTTM = '31-DEC-9999' 
     and D.CURRENT_RECORD_FLG  = '0'  
 ; 
 
 COMMIT ;  


  
spool off ; 
    

  


