SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON


SPOOL  /i1/&&env/hub/logs/HT_218537_F_TRANS.log ;

-- 801828

DELETE FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_07) F
      WHERE F.TRANSACTION_LINE_ITEM_KEY IN
               (  SELECT                              /*+ parallel (f , 8)  */
                        MAX (TRANSACTION_LINE_ITEM_KEY)
                    FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_07) f
                   WHERE ROW_SOURCE_SYS_NM IN ('TANDEM', 'TP/CP')
                GROUP BY F.WEX_TRANSACTION_ID,
                         WEX_TRANSACTION_ITEM_SEQ_NBR,
                         ROW_SOURCE_SYS_NM
                  HAVING COUNT (*) > 1);



COMMIT;

-- 468549

DELETE FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_08) F
      WHERE F.TRANSACTION_LINE_ITEM_KEY IN
               (  SELECT                              /*+ parallel (f , 8)  */
                        MAX (TRANSACTION_LINE_ITEM_KEY)
                    FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_08) f
                   WHERE ROW_SOURCE_SYS_NM IN ('TANDEM', 'TP/CP')
                GROUP BY F.WEX_TRANSACTION_ID,
                         WEX_TRANSACTION_ITEM_SEQ_NBR,
                         ROW_SOURCE_SYS_NM
                  HAVING COUNT (*) > 1);



COMMIT;


--175278

DELETE FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_09) F
      WHERE F.TRANSACTION_LINE_ITEM_KEY IN
               (  SELECT                              /*+ parallel (f , 8)  */
                        MAX (TRANSACTION_LINE_ITEM_KEY)
                    FROM F_TRANSACTION_LINE_ITEM PARTITION (YM_2011_09) f
                   WHERE ROW_SOURCE_SYS_NM IN ('TANDEM', 'TP/CP')
                GROUP BY F.WEX_TRANSACTION_ID,
                         WEX_TRANSACTION_ITEM_SEQ_NBR,
                         ROW_SOURCE_SYS_NM
                  HAVING COUNT (*) > 1);



COMMIT;

SPOOL OFF ; 	