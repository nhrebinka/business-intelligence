insert into wb_owner.d_region (
REGION_KEY,
REGION_NAME,
REGION_CODE,
DISTRICT_NAME,
DISTRICT_CODE,
SUBDISTRICT_NAME,
SUBDISTRICT_CODE,
ROW_CREATE_DTTM,
ROW_LAST_MOD_DTTM,
ROW_LAST_MOD_PROC_NM,
ROW_LAST_MOD_PROC_SEQ_NBR
)
values (
0,           -- REGION_KEY,
'Unknown',   -- REGION_NAME,
' ',         -- REGION_CODE,
'Unknown',   -- DISTRICT_NAME,
' ',         -- DISTRICT_CODE,
'Unknown',   -- SUBDISTRICT_NAME,
' ',         -- SUBDISTRICT_CODE,
sysdate,     -- ROW_CREATE_DTTM,
sysdate,     -- ROW_LAST_MOD_DTTM,
'Eric',      -- ROW_LAST_MOD_PROC_NM,
0            -- ROW_LAST_MOD_PROC_SEQ_NBR
);
