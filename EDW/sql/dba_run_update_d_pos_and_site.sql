SET ECHO        ON
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON
SET HEADING     ON
SET TRIMSPOOL   ON
SET TAB         OFF
SET LINESIZE    2000

SPOOL /i1/&&env/hub/logs/dba_run_update_d_pos_and_site.log;

/*   Script_name     :-  dba_run_update_d_pos_and_site.sql 
     Desc            :-  Data Fix For Merchant_NAME , LEGAL_OWNER_NAME, BRAND_NAME   
     Authour         :-  VK  (Venu Kancharla) 
     Date            :-  11/01/2011


*/ 

      
      UPDATE   D_POS_AND_SITE DPS 
         SET  
               DPS.MERCHANT_NAME 	= 'Mobil' , 
               DPS.LEGAL_OWNER_NAME 	= 'Mobil' , 
               DPS.BRAND_OWNER_NAME 	= 'Mobil' 
       WHERE 
               DPS.LEGAL_OWNER_NAME  like 'Citibank%'       ; 
               
     Commit ;   
 
spool off ; 


