/* grant_all.sql
   By:  Venugopal Kancharla 
   Date:  10/27/04
   Description: Script to generate grant all permissions on tables, views, sequences.
       			To be run from account granting the privileges.
				  	    Respond to prompt with account name being granted the privileges. 

   log        : Changed by venu on 02/2/2005
                to write the dynamic scripts to /i1/&&env/hub/logs/ and incluse the  prefix .
                changed by venu 0n 02/10/2005 
                to include owner prefix before the objects 
                added exit at the end of the file 05/26/2005 
*/
set verify off
set serveroutput on  
set head off
set feedback off
set echo off 

spool /i1/&&env/hub/logs/&&grant_all_account.run_grant_all.sql

select 'grant all on ' || uo.owner||'.'||uo.object_name || ' to ' || '&&grant_all_account' || ';'
from all_objects  uo , user_users uu 
where uo.object_type in ('TABLE',  'SEQUENCE')  
and uo.owner = uu.username 
/
spool off ; 

set echo on
set feedback on
set head on

spool  /i1/&&env/hub/logs/&&grant_all_account.run_grant_all.log ; 

start /i1/&&env/hub/logs/&&grant_all_account.run_grant_all.sql ; 

set verify on 

spool off ; 
exit ; 