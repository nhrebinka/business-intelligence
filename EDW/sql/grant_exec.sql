/* grant_execute.sql
   By:  Venugopal Kancharla  
   Date:  10/27/04
   Description: Script to generate grant select permissions on tables and views.
   				   To be run from account granting the privileges.
				       Respond to prompt with account name being granted the privileges. 

   log        : Changed by venu on 02/2/2005  to write the dynamic scripts to /i1/&&env/hub/logs/ and incluse the  prefix .
                changed by venu 0n 02/10/2005 to include owner prefix before the objects     
                changed by venu 0n 04/04/2005 include views in object type 

*/
set verify off
set serveroutput off 
set head off
set feedback off
set echo off 
set pagesize 1000
set timing off 
set time off 

spool /i1/&&env/hub/logs/&&grant_execute_account.run_grant_execute.sql

select 'grant EXECUTE on ' ||uo.owner||'.'||uo.object_name || ' to ' ||  '&&grant_execute_account' || ';' 
from all_objects  uo , user_users uu 
where uo.object_type in ('PROCEDURE','PACKAGE' ,'FUNCTION' )
and uo.owner = uu.username 
/

spool off

set echo on
set feedback on
set head on

spool  /i1/&&env/hub/logs/&&grant_execute_account.run_grant_execute.log ; 

start  /i1/&&env/hub/logs/&&grant_execute_account.run_grant_execute.sql   ; 

set verify on 

spool off

exit 
