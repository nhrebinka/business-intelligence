SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000
DEFINE &&env = &1


spool /i1/&&env/hub/logs/edm_dwloader_synonym.sql.log ; 
  
create synonym SRC_CLM_CLUSTER  for STAGE_OWNER.SRC_CLM_CLUSTER ;

spool off ; 