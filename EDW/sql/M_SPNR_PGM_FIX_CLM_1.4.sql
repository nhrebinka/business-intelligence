SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/m_spnr_pgm_update_clm_1.4.log ; 

--Query to update M_SPNR_PGM;

UPDATE M_SPNR_PGM M
set       M.PROGRAM_NAME  = 'Universal',
           M.PROGRAM_SERVICE_OPTION_CODE = '1',
           M.PROGRAM_PRODUCT_NAME = 'Proprietary-Direct',
           M.PROGRAM_SPONSOR_NAME = 'WEX'
           where EDM_SPNR_PGM_KEY IN (2032,2163);
           
 Commit;
 
 
UPDATE M_SPNR_PGM M
SET       M.PROGRAM_PACKAGE_NAME= 'ExxonMobil Universal Universal',
             M.PROGRAM_MANAGER_NAME = 'ExxonMobil'
             WHERE EDM_SPNR_PGM_KEY = 2032;
             
 COMMIT;
 
 
 UPDATE M_SPNR_PGM M
 SET       M.PROGRAM_PACKAGE_NAME = 'Sunoco Universal Universal',
             M.PROGRAM_MANAGER_NAME = 'Sunoco'
             WHERE EDM_SPNR_PGM_KEY = 2163;
             
 COMMIT; 

--Query to update D_PROGRAM@QTBIHUB--


  
UPDATE D_PROGRAM D
set       D.PROGRAM_NAME = 'Universal',
           D.SERVICE_OPTION_CODE = '1',
          D.PROGRAM_SPONSOR_NAME = 'WEX',
          D.PRODUCT_NAME = 'Proprietary-Direct',
          D.PROGRAM_PACKAGE_NAME= 'Sunoco Universal Universal',
          D.PROGRAM_MANAGER_NAME = 'Sunoco'
          WHERE D.PROGRAM_KEY = 315 ; 
       
COMMIT;




  
  
UPDATE D_PROGRAM D
set      D.PROGRAM_NAME = 'Universal',
          D.SERVICE_OPTION_CODE = '1',
          D.PROGRAM_SPONSOR_NAME = 'WEX',
           D.PRODUCT_NAME = 'Proprietary-Direct',
          D.PROGRAM_PACKAGE_NAME= 'ExxonMobil Universal Universal',
          D.PROGRAM_MANAGER_NAME = 'ExxonMobil'
          WHERE D.PROGRAM_KEY = 571 ; 
          
          COMMIT;
                         
                         
spool off ;                          
                         