

SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_product.dat;

SELECT                                        /*+parallel (dw_PRODUCT  , 4) */
      Dw_PRODUCT_KEY
       || '~|~'
       || LANGUAGE_SHORT_NM
       || '~|~'
       || IND_STD_PROD_ROW_ID
       || '~|~'
       || LANGUAGE_NM
       || '~|~'
       || LANGUAGE_CD
       || '~|~'
       || PROD_LONG_NM
       || '~|~'
       || PROD_STS
       || '~|~'
       || BILL_CD
       || '~|~'
       || PROD_DESC
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || IND_STD_PROD_CD
       || '~|~'
       || ''
       || '~|~'
       || WEX_SETTLEMENT_CD
       || '~|~'
       || PROD_SHORT_NM
       || '~|~'
       || NEG_PPU_MULTIPLIER_NBR
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
  FROM DW_PRODUCT
 WHERE DW_CURRENT_FLG = 'Y' AND DW_PRODUCT_KEY <> 99;

spool off;


