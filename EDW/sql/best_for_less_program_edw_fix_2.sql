SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/Best_for_less_program_edw_fix_2.log ; 

UPDATE  EDW_OWNER.F_TRANSACTION_LINE_ITEM F
   SET F.PROGRAM_KEY = 724
WHERE 
 F.PURCHASE_ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;       


UPDATE  EDW_OWNER.F_ACCOUNTS_RECEIVABLE_ENTRY F
   SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;


UPDATE EDW_OWNER.F_MONTHLY_REVENUE_SNAPSHOT F
   SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;      

UPDATE  EDW_OWNER.F_MONTHLY_ACCOUNT_SNAPSHOT F
   SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;  


UPDATE  EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT F
   SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;  


UPDATE EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT F
SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;  

UPDATE EDW_OWNER.F_PURCHASE_DEVICE_EVENT F
SET F.PROGRAM_KEY = 724
WHERE 
 F.ACCOUNT_KEY NOT IN
                                              (SELECT ACCOUNT_KEY
                                                 FROM D_ACCOUNT D
                                                WHERE D.SOURCE_ACCOUNT_ID IN
                                                         ('0453005217609',
                                                          '0453005220033',
                                                          '0453005222914',
                                                          '0453005222948',
                                                          '0453005227400',
                                                          '0453005229802',
                                                          '0453005244496',
                                                          '0453005252168',
                                                          '0453005269014',
                                                          '0453005156773',
                                                          '0453005215298',
                                                          '0453005215330',
                                                          '0453005217534',
                                                          '0453005739610',
                                                          '0453005739685',
                                                          '0453005744768',
                                                          '0453005771175'))
       AND F.PROGRAM_KEY = 732;

COMMIT;  


spool off;