/* Formatted on 7/18/2011 3:49:53 PM (QP5 v5.163.1008.3004) */
SET ECHO        ON
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON
SET HEADING     ON
SET TRIMSPOOL   ON
SET TAB         OFF
SET LINESIZE    2000

SPOOL /i1/&&env/hub/logs/dba_run_update_national_id.log;

MERGE INTO EDM_OWNER.D_ACCOUNT B
     USING (SELECT SOURCE_ACCOUNT_ID, NATIONAL_ACCOUNT_ID
                  FROM 
                           EDM_OWNER.D_ACCOUNT
                 WHERE 
                       (SOURCE_ACCOUNT_ID, ROW_EFF_BEGIN_DTTM) 
                       IN
                           (SELECT D1.SOURCE_ACCOUNT_ID,
                                MAX (D1.ROW_EFF_BEGIN_DTTM)
                            FROM 
                                     EDM_OWNER.D_ACCOUNT D1
                            WHERE 
                                     D1.SOURCE_ACCOUNT_ID 
                                     IN
                                           (SELECT D.SOURCE_ACCOUNT_ID
                                              FROM EDM_OWNER.D_ACCOUNT D
                                             WHERE D.CURRENT_RECORD_FLG = '1'
                                                   AND NVL (TRIM (D.NATIONAL_ACCOUNT_ID), 'Unknown') = 'Unknown'
                                            )
                                        AND NVL (TRIM (D1.NATIONAL_ACCOUNT_ID), 'Unknown') <> 'Unknown'
                            GROUP BY 
                                       D1.SOURCE_ACCOUNT_ID
                         )
                 ) A
        ON (B.SOURCE_ACCOUNT_ID = A.SOURCE_ACCOUNT_ID)
WHEN MATCHED
THEN
   UPDATE SET
      B.NATIONAL_ACCOUNT_ID = A.NATIONAL_ACCOUNT_ID,
      B.ROW_LAST_MOD_PROC_NM = 'Data Fix for National ID ',
      B.ROW_LAST_MOD_DTTM = SYSDATE
           WHERE NVL (TRIM (B.NATIONAL_ACCOUNT_ID), 'Unknown') = 'Unknown';

COMMIT;

MERGE INTO EDM_OWNER.D_ACCOUNT D
     USING (SELECT ACCOUNT_KEY,
                   EDM_OWNER.MD5HASH (
                         TRIM (FUNDER_NAME)
                      || ' '
                      || TRIM (FUNDING_TYPE_NAME)
                      || ' '
                      || TRIM (ACCOUNT_REGION_CODE)
                      || ' '
                      || TRIM (ACCOUNT_REGION_NAME)
                      || ' '
                      || TRIM (ACCOUNT_DISTRICT_NAME)
                      || ' '
                      || TRIM (ACCOUNT_DISTRICT_CODE)
                      || ' '
                      || TRIM (ACCOUNT_SUBDISTRICT_NAME)
                      || ' '
                      || TRIM (ACCOUNT_SUBDISTRICT_CODE)
                      || ' '
                      || TRIM (ACCOUNT_POSTAL_CODE)
                      || ' '
                      || TRIM (ACCOUNT_COUNTRY_NAME)
                      || ' '
                      || TRIM (PROGRAM_NAME)
                      || ' '
                      || TRIM (ACCOUNT_NAME)
                      || ' '
                      || TRIM (ACCOUNT_STATUS_DESC)
                      || ' '
                      || TRIM (DIVERSION_ACCOUNT_FLG)
                      || ' '
                      || TRIM (BILLING_ACCOUNT_FLG)
                      || ' '
                      || TRIM (RELATED_BILLING_ACCOUNT_ID)
                      || ' '
                      || TRIM (TO_CHAR (SIC_LOOKUP_KEY))
                      || ' '
                      || TRIM (ACCOUNT_MANAGER_NAME)
                      || ' '
                      || TRIM (PURCHASE_ACCOUNT_FLG)
                      || ' '
                      || TRIM (CUSTOMER_NAME)
                      || ' '
                      || TRIM (ATTRITION_REASON_DESC)
                      || ' '
                      || TRIM (TO_CHAR (RISK_GRADE))
                      || ' '
                      || TRIM (
                            TO_CHAR (ACCOUNT_CLOSED_DATE,
                                     'MM/DD/YYYY HH24:mi:SS'))
                      || ' '
                      || TRIM (ATTRITION_TYPE_NAME)
                      || ' '
                      || TRIM (ATTRITION_REASON_CODE)
                      || ' '
                      || TRIM (SOURCE_SYSTEM_CODE)
                      || ' '
                      || TRIM (TAX_EXEMPT_FLG)
                      || ' '
                      || TRIM (CLM_CLUSTER_NAME)
                      || ' '
                      || TRIM (TO_CHAR (LAST_CLUSTERED_DATE, 'MM/DD/YYYY'))
                      || ' '
                      || TRIM (PRODUCT_PURCHASE_RESTRICTION)
                      || ' '
                      || TRIM (PAYMENT_METHOD)
                      || ' '
                      || TRIM (EFPS_REQUIRED_FLG)
                      || ' '
                      || TRIM (EFPS_FLG)
                      || ' '
                      || TRIM (EFPS_TIME_INTERVAL))
                      HASH_1
              FROM EDM_OWNER.D_ACCOUNT) D1
        ON (D.ACCOUNT_KEY = D1.ACCOUNT_KEY)
WHEN MATCHED
THEN
   UPDATE SET
      D.ROW_MD5_CHECKSUM_HEX_T2 = D1.HASH_1,
      D.ROW_LAST_MOD_DTTM            = SYSDATE,
      D.ROW_LAST_MOD_PROC_NM      = 'ROW_MD5_CHECKSUM_HEX_T2 UPDATE FOR NATIONAL_ID FIX '
WHERE 
     D.ROW_MD5_CHECKSUM_HEX_T2 <> D1.HASH_1;


COMMIT;


  SPOOL OFF;