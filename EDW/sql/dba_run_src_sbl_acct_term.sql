SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000
   

spool /i1/&&env/hub/data/src_sbl_acct_termed.dat ; 

SELECT  /*+  parallel (d, 4) */
      DW_ACCT_KEY
       || '~|~'
       || WEX_ACCT_NBR
       || '~|~'
       || TO_CHAR (ACCT_TERM_DT, 'MM/DD/YYYY hh24:mi:ss')
  FROM DW_ACCT D
 WHERE D.DW_CURRENT_FLG = 'Y' AND D.DW_ACCT_KEY <> 99
 and D.ACCT_TERM_DT < D.DW_EFF_DT 
 and D.ACCT_TERM_DT  between  '01-JAN-2006' and '18-OCT-2010'     ; 


spool off ;     
  
