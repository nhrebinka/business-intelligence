SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/dba_run_indexes.log ; 

DROP INDEX F_TRANSACTION_LINE_ITEM_UI14 ; 

CREATE INDEX F_TRANSACTION_LINE_ITEM_UI14 ON F_TRANSACTION_LINE_ITEM
(REVENUE_DATE_KEY, POSTING_DATE_KEY,PURCHASE_DATE_KEY )
  TABLESPACE I_F_TRANS_LINE_ITEM
NOLOGGING
LOCAL (  
  PARTITION YM_2008_01
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_02
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_03
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_04
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_05
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_06
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_07
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_08
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_09
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_10
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_11
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2008_12
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_01
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_02
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_03
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_04
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_05
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_06
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_07
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_08
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_09
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_10
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_11
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2009_12
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_01
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_02
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_03
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_04
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_05
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_06
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_07
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_08
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_09
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_10
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_11
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2010_12
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2011_01
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2011_02
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2011_03
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2011_04
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
   , 
  PARTITION YM_2011_05
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_06
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_07
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_08
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_09
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_10
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_11
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION YM_2011_12
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
    ,  
  PARTITION MAX_VALUE
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE I_F_TRANS_LINE_ITEM
)
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT ) COMPUTE STATISTICS ;



DROP INDEX DATE_UI5 ; 

CREATE  UNIQUE INDEX DATE_UI5 ON D_DATE
(DATE_KEY, CALENDAR_DATE_DT)
TABLESPACE I_BI
GLOBAL PARTITION BY HASH (DATE_KEY)
PARTITIONS 4
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT ) COMPUTE STATISTICS;

SPOOL OFF ; 