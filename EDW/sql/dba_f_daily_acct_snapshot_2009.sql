SET ECHO     ON 
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON 
SET HEADING   ON 
SET TRIMSPOOL ON
SET TAB       OFF
set LINESIZE  2000

spool /i1/&&env/hub/logs/dba_f_daily_acct_snapshot_2009.log ; 

select systimestamp from  dual ; 

ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_01 UPDATE GLOBAL INDEXES ; 
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_02 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_03 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_04 UPDATE GLOBAL INDEXES ; 
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_05 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_06 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_07 UPDATE GLOBAL INDEXES ; 
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_08 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_09 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_10 UPDATE GLOBAL INDEXES ; 
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_11 UPDATE GLOBAL INDEXES ;
ALTER  TABLE F_DAILY_ACCOUNT_SNAPSHOT  TRUNCATE PARTITION  YM_2009_12 UPDATE GLOBAL INDEXES ;

select systimestamp from  dual ; 

EXEC F_DAILY_ACCOUNT_AGGREGATES_H ( '01-JAN-2009' , '31-DEC-2009' ) ; 

select systimestamp from  dual ; 

EXEC F_MONTHLY_ACCOUNT_AGGREGATES_H ( '01-JAN-2009' , '31-DEC-2009' ) ; 

select systimestamp from  dual ; 
      
spool off ;  