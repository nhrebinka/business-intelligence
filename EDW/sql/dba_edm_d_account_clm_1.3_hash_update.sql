SET ECHO    ON 
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON 
SET HEADING      ON 
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000


spool /i1/&&env/hub/logs/dba_edm_d_account_clm_1.3_hash_update.log ; 

 UPDATE D_ACCOUNT D 
 SET     D.LAST_CLUSTERED_DATE = '31-DEC-9999' 
 Where 
    D.LAST_CLUSTERED_DATE  IS NULL  ;
     
  COMMIT ;
  
 UPDATE D_ACCOUNT D 
 SET    D.EFPS_TIME_INTERVAL = 'Unknown'  
 Where 
    D.EFPS_TIME_INTERVAL  IS NULL  ;
        
    COMMIT ; 
     
 UPDATE D_ACCOUNT D 
 SET    D.EFPS_REQUIRED_FLG  = 'Unkn'  
 Where 
           D.EFPS_REQUIRED_FLG  IS NULL  ;
           
       COmmit ;
    
       
 UPDATE D_ACCOUNT D 
 SET    D.EFPS_FLG  = 'Unkn'  
 Where 
           D.EFPS_FLG  IS NULL  ;
           
    COmmit ;
    
    
 UPDATE D_ACCOUNT D 
 SET    D.PAYMENT_METHOD  = 'Unknown'  
 Where 
           D.PAYMENT_METHOD  IS NULL  ;
           
 COMMIT ; 
  
 UPDATE D_ACCOUNT D 
 SET    D.PRODUCT_PURCHASE_RESTRICTION  = 'Unknown'  
 Where 
           D.PRODUCT_PURCHASE_RESTRICTION  IS NULL  ;
          
   COMMIT ;      
   
   
  /***************************************************************************** 
    Stretch  the frozen  attributes of the dimensions Back in  time  This is the One time Hsitory update Only .....
   */             
         
MERGE INTO D_ACCOUNT D  
USING   ( select  
                        D.SOURCE_ACCOUNT_ID ,   
                        D.ACQUISITION_CHANNEL , 
                        MAX(D.ROW_CREATE_DTTM) ROW_CREATE_DTTM  
             from 
                        D_ACCOUNT  D 
             WHERE
                         NVL(D.ACQUISITION_CHANNEL , 'Unknown') <>  'Unknown' 
             GROUP BY         
                        D.SOURCE_ACCOUNT_ID ,  
                         D.ACQUISITION_CHANNEL 
              ) D1 
ON   (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED THEN 
UPDATE SET  
          D.ACQUISITION_CHANNEL             = D1.ACQUISITION_CHANNEL , 
          D.ROW_LAST_MOD_DTTM             = SYSDATE,
          D.ROW_LAST_MOD_PROC_NM       = 'ONE TIME HISTORY UPDATE FOR CLM 1.3 T1 Strectch'
 WHERE D.ROW_CREATE_DTTM <   D1.ROW_CREATE_DTTM  ;        
   
 COMMIT ; 
           
MERGE INTO D_ACCOUNT D  
USING    ( select  
                       D.SOURCE_ACCOUNT_ID ,  
                        D.COUPON_CODE          , 
                        MAX(D.ROW_CREATE_DTTM) ROW_CREATE_DTTM  
              from 
                       D_ACCOUNT  D 
               WHERE
                         NVL(D.COUPON_CODE , 'Unknown') <>  'Unknown' 
               GROUP BY         
                            D.SOURCE_ACCOUNT_ID ,
                             D.COUPON_CODE 
             ) D1 
ON   (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED THEN 
UPDATE SET  
          D.COUPON_CODE                          = D1.COUPON_CODE, 
          D.ROW_LAST_MOD_DTTM             = SYSDATE,
          D.ROW_LAST_MOD_PROC_NM       = 'ONE TIME HISTORY UPDATE FOR CLM 1.3 T1 Strectch'
 WHERE D.ROW_CREATE_DTTM <   D1.ROW_CREATE_DTTM  ;
 
  COMMIT ; 
 
 
MERGE INTO D_ACCOUNT D
     USING (SELECT D.SOURCE_ACCOUNT_ID,
                              D.OPPORTUNITY_NUMBER,
                              MAX (D.ROW_CREATE_DTTM) ROW_CREATE_DTTM
                FROM      
                               D_ACCOUNT D
                WHERE 
                             NVL (OPPORTUNITY_NUMBER, 'Unknown') <> 'Unknown'
                GROUP BY 
                             D.SOURCE_ACCOUNT_ID, 
                             D.OPPORTUNITY_NUMBER
                ) D1
        ON (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED
THEN
   UPDATE SET
                    D.OPPORTUNITY_NUMBER       = D1.OPPORTUNITY_NUMBER,
                    D.ROW_LAST_MOD_DTTM       = SYSDATE,
                    D.ROW_LAST_MOD_PROC_NM  = 'ONE TIME HISTORY UPDATE FOR CLM 1.3 T1 Strectch'
           WHERE D.ROW_CREATE_DTTM < D1.ROW_CREATE_DTTM;
           
  COMMIT ;
  
  
  MERGE INTO D_ACCOUNT D
     USING (  SELECT D.SOURCE_ACCOUNT_ID,
                              D.SALES_REP_ID,
                               MAX (D.ROW_CREATE_DTTM) ROW_CREATE_DTTM
                FROM D_ACCOUNT D
               WHERE
                            NVL (SALES_REP_ID, 'Unknown') <> 'Unknown'
            GROUP BY D.SOURCE_ACCOUNT_ID, 
                           D.SALES_REP_ID
              ) D1
        ON (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED
THEN
   UPDATE SET
      D.SALES_REP_ID                      = D1.SALES_REP_ID,
      D.ROW_LAST_MOD_DTTM        = SYSDATE,
      D.ROW_LAST_MOD_PROC_NM  =  'ONE TIME HISTORY UPDATE FOR CLM 1.3 T1 Strectch'
           WHERE D.ROW_CREATE_DTTM < D1.ROW_CREATE_DTTM;
            
COMMIT ;  

-- *******************************************************************************
-- Update the Remaining Null Attributes 
--********************************************************************************

 UPDATE D_ACCOUNT D 
 SET D.ACQUISITION_CHANNEL = 'Unknown' 
 WHERE 
    D.ACQUISITION_CHANNEL IS NULL  ; 
    
 COMMIT ;
 
 UPDATE D_ACCOUNT D 
 SET D.COUPON_CODE   = 'Unknown' 
 WHERE 
    D.COUPON_CODE  IS NULL  ;
    
    COMMIT ; 
    
 UPDATE D_ACCOUNT D 
 SET D.OPPORTUNITY_NUMBER     = 'Unknown' 
 WHERE 
    D.OPPORTUNITY_NUMBER  IS NULL  ;
    
    COMMIT ; 
   
 UPDATE D_ACCOUNT D 
 SET D.SALES_REP_ID      = 'Unknown' 
 WHERE 
    D.SALES_REP_ID   IS NULL  ;
         
   COMMIT ;  
    
  MERGE INTO D_ACCOUNT D 
     USING
      (select 
               ACCOUNT_KEY ,  
                MD5HASH (   TRIM(FUNDER_NAME)|| ' ' ||
                                    TRIM(FUNDING_TYPE_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_REGION_CODE)|| ' ' ||
                                    TRIM(ACCOUNT_REGION_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_DISTRICT_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_DISTRICT_CODE)|| ' ' ||
                                    TRIM(ACCOUNT_SUBDISTRICT_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_SUBDISTRICT_CODE)|| ' ' ||
                                    TRIM(ACCOUNT_POSTAL_CODE)|| ' ' ||
                                    TRIM(ACCOUNT_COUNTRY_NAME)|| ' ' ||
                                    TRIM(PROGRAM_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_NAME)|| ' ' ||
                                    TRIM(ACCOUNT_STATUS_DESC)|| ' ' ||
                                    TRIM(DIVERSION_ACCOUNT_FLG)|| ' '||
                                    TRIM(BILLING_ACCOUNT_FLG)|| ' ' ||
                                    TRIM(RELATED_BILLING_ACCOUNT_ID)|| ' '||
                                    TRIM(TO_CHAR(SIC_LOOKUP_KEY)) || ' ' ||
                                    TRIM(ACCOUNT_MANAGER_NAME)|| ' ' ||
                                    TRIM(PURCHASE_ACCOUNT_FLG) || ' ' ||
                                    TRIM(CUSTOMER_NAME)|| ' ' ||
                                    TRIM(ATTRITION_REASON_DESC)|| ' ' ||
                                    TRIM(TO_CHAR(RISK_GRADE))|| ' ' ||
                                    TRIM(TO_CHAR(ACCOUNT_CLOSED_DATE , 'MM/DD/YYYY HH24:mi:SS' ))|| ' ' ||
                                    TRIM(ATTRITION_TYPE_NAME)|| ' ' ||
                                    TRIM(ATTRITION_REASON_CODE) || ' ' ||
                                    TRIM(SOURCE_SYSTEM_CODE) || ' ' ||
                                    TRIM(TAX_EXEMPT_FLG) || ' ' ||
                                    TRIM(CLM_CLUSTER_NAME)|| ' ' ||
                                    TRIM(TO_CHAR(LAST_CLUSTERED_DATE,'MM/DD/YYYY'))|| ' ' ||
                                    TRIM(PRODUCT_PURCHASE_RESTRICTION)|| ' ' ||
                                    TRIM(PAYMENT_METHOD)|| ' ' ||
                                    TRIM(EFPS_REQUIRED_FLG)|| ' ' ||
                                    TRIM(EFPS_FLG)|| ' ' ||
                                    TRIM(EFPS_TIME_INTERVAL)
                ) HASH_1   
    FROm D_ACCOUNT )  D1   
ON (  D.ACCOUNT_KEY = D1.ACCOUNT_KEY )  
when matched
THEN 
  UPDATE SET 
        D.ROW_MD5_CHECKSUM_HEX_T2  = D1.HASH_1 , 
        D.ROW_LAST_MOD_DTTM             = SYSDATE,
        D.ROW_LAST_MOD_PROC_NM       = 'ROW_MD5_CHECKSUM_HEX_T2 UPDATE BY SQL FOR 1.3 ' 
      WHERE
           D.ROW_MD5_CHECKSUM_HEX_T2  <>  D1.HASH_1    ; 
        
        
COMMIT ; 
           
spool OFF  ;

 
 