

SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_addr.dat ; 

SELECT                                            /*+parallel (DW_ADDR , 4) */
      DW_ADDR_KEY
       || '~|~'
       || ADDR_ROW_ID
       || '~|~'
       || SPNR_ACCT_NBR
       || '~|~'
       || ADDR_TYPE
       || '~|~'
       || ADDR_LN_1
       || '~|~'
       || ADDR_LN_2
       || '~|~'
       || ADDR_LN_3
       || '~|~'
       || ADDR_CITY
       || '~|~'
       || ADDR_STATE_PROV_CD
       || '~|~'
       || ADDR_ZIP_POSTAL_CD
       || '~|~'
       || ADDR_COUNTY_CD
       || '~|~'
       || ADDR_COUNTY_NM
       || '~|~'
       || ADDR_COUNTRY_CD
       || '~|~'
       || ADDR_PHONE_NBR
       || '~|~'
       || ADDR_FAX_NBR
       || '~|~'
       || ADDR_TIME_ZONE
       || '~|~'
       || ADDR_METRO_STAT_AREA_NBR
       || '~|~'
       || ADDR_METRO_STAT_AREA_DSC
       || '~|~'
       || ADDR_DELIV_POINT_CD
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || ' '
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || ATTENTION_LINE
       || '~|~'
       || ' '
  FROM DW_ADDR
 WHERE DW_CURRENT_FLG = 'Y' AND DW_ADDR_KEY <> 99;

Spool OFF;

