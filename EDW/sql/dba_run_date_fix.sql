SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/dba_run_date_fix.log ; 


UPDATE D_DATE D 
  SET    D.CALENDAR_DATE_DT     = '31-DEC-9999' , 
         D.ROW_LAST_MOD_PROC_NM = 'ONE SHOT UPDATE FOR FUTURE DATE FOR CLM 1.4',
         D.ROW_LAST_MOD_DTTM    =  SYSDATE 
  Where 
      D.DATE_KEY = -3 ;


COMMIT ; 

spool off;