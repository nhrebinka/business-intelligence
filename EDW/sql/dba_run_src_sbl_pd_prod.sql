

SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000


spool /i1/&&env/hub/data/src_sbl_pd_prod.dat

SELECT                                        /*+parallel (dw_PD_PROD  , 4) */
      Dw_PD_PROD_KEY
       || '~|~'
       || PD_PROD_ROW_ID
       || '~|~'
       || PD_ACCEPT_LVL
       || '~|~'
       || PD_ACCEPT_LVL_DESC
       || '~|~'
       || PD_PROD_NM
       || '~|~'
       || PD_PROD_SPNR_DISPLAY_NM
       || '~|~'
       || DESIGN_CD
       || '~|~'
       || USAGE_TYPE
       || '~|~'
       || PD_DEVICE_TYPE
       || '~|~'
       || EMBOSS_SPEC
       || '~|~'
       || EMBOSS_PD_NBR_GEN_ALGORITHM
       || '~|~'
       || SUPPRESS_PD_LABEL_FLG
       || '~|~'
       || BANK_ID_NBR
       || '~|~'
       || SEND_PD_HLDR
       || '~|~'
       || SEND_FUELING_INSTR
       || '~|~'
       || SUPPRESS_PD_LTR_FLG
       || '~|~'
       || SITE_PD_FLG
       || '~|~'
       || PD_PROD_STS
       || '~|~'
       || TO_CHAR (PD_PROD_STS_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || PD_EMBOSS_FEE
       || '~|~'
       || PD_RPLCMNT_FEE
       || '~|~'
       || PLASTIC_TOPPER_COLOR
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || ''
  FROM DW_PD_PROD;

 -- WHERE DW_CURRENT_FLG = 'Y'

spool off;



