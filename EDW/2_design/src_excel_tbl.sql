--
-- Wex BI4.0 - DDL for Excel-based source external tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_EXCEL_COST_TO_MKT_CHL
(
  NAME                   VARCHAR2(100 BYTE),
  START_DATE             DATE,
  END_DATE               DATE,
  MARKETING_SUB_CHANNEL  VARCHAR2(200 BYTE),
  "7-ELEVEN"             NUMBER(20,2),
  ALON                   NUMBER(20,2),
  CIRCLE_K               NUMBER(20,2),
  CITGO                  NUMBER(20,2),
  ENTERPRISE             NUMBER(20,2),
  EXXONMOBIL             NUMBER(20,2),
  FUELTRAC               NUMBER(20,2),
  GET_GO                 NUMBER(20,2),
  GULF                   NUMBER(20,2),
  HESS                   NUMBER(20,2),
  IMPERIAL               NUMBER(20,2),
  LUKOIL                 NUMBER(20,2),
  MAPCO                  NUMBER(20,2),
  MARATHON               NUMBER(20,2),
  MEIJER                 NUMBER(20,2),
  MURPHY_OIL             NUMBER(20,2),
  PEP_BOYS               NUMBER(20,2),
  PHILLIPS66             NUMBER(20,2),
  QUIKTRIP               NUMBER(20,2),
  RACETRAC               NUMBER(20,2),
  SEARS                  NUMBER(20,2),
  SHEETZ                 NUMBER(20,2),
  SUNOCO                 NUMBER(20,2),
  TESORO                 NUMBER(20,2),
  VALVOLINE              NUMBER(20,2),
  WAWA                   NUMBER(20,2),
  WEX                    NUMBER(20,2),
  ALLOTHER               NUMBER(20,2)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDM_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_mkt_chl.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      MARKETING_SUB_CHANNEL,
      "7-ELEVEN",
      ALON,
      CIRCLE_K,
      CITGO,
      ENTERPRISE,
      EXXONMOBIL,
      FUELTRAC,
      GET_GO,
      GULF,
      HESS,
      IMPERIAL,
      LUKOIL,
      MAPCO,
      MARATHON,
      MEIJER,
      MURPHY_OIL,
      PEP_BOYS,
      PHILLIPS66,
      QUIKTRIP,
      RACETRAC,
      SEARS,
      SHEETZ,
      SUNOCO,
      TESORO,
      VALVOLINE,
      WAWA,
      WEX,
      ALLOTHER
    )
     )
     LOCATION (EDM_DATA_DIR:'src_excel_cost_to_mkt_chl.dat')
  )
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_SERVE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_serve.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_serve.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_serve.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_serve.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ADJUDICATE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_adjudicate.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_adjudicate.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_adjudicate.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_adjudicate.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ACQUIRE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_acquire.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_acquire.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_acquire.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_acquire.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_SALES
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_sales.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_sales.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_sales.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_sales.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_IT
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_it.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_it.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_it.dis'
    SKIP 1
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_it.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_PROGRAM
(
  PSRELATIONCODE             VARCHAR2(50 BYTE),
  PROGRAM                    VARCHAR2(200 BYTE),
  PARENT                     VARCHAR2(200 BYTE),
  DATEADDED                  DATE,
  SIEBELPROGRAMNAME          VARCHAR2(200 BYTE),
  SIEBELROWID                VARCHAR2(200 BYTE),
  PROFITABILITYTYPE          VARCHAR2(200 BYTE),
  PROFITABILITYRELATIONSHIP  VARCHAR2(200 BYTE),
  MARKETINGRELATIONSHIP      VARCHAR2(200 BYTE),
  MARKETINGPARTNER           VARCHAR2(200 BYTE)
)
ORGANIZATION EXTERNAL (
  TYPE ORACLE_LOADER
  DEFAULT DIRECTORY EDM_DATA_DIR
  ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_program.log'
    BADFILE      edm_log_dir:'src_excel_program.bad'
    DISCARDFILE  edm_log_dir:'src_excel_program.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      PSRELATIONCODE,
      PROGRAM,
      PARENT,
      DATEADDED CHAR  DATE_FORMAT DATE MASK "MM/DD/YYYY HH24:mi:ss" NULLIF DATEADDED = BLANKS,
      SIEBELPROGRAMNAME,
      SIEBELROWID,
      PROFITABILITYTYPE,
      PROFITABILITYRELATIONSHIP,
      MARKETINGRELATIONSHIP,
      MARKETINGPARTNER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_program.dat')
)
REJECT LIMIT 2  -- to prevent bad headers
NOPARALLEL
;
