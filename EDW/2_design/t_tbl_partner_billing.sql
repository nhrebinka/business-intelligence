--
-- Wex BI4.2 - DDL for EDM Temp tables for Partner Billing
--
-- ------------------------------------------------------------------------
CREATE TABLE T_PROGRAM_BILLING
(
    EDM_SPNR_PGM_KEY          integer             NOT NULL,
    REVENUE_DT                date                NOT NULL,
    BILL_AMOUNT               number(15,2),
    SIGN_ON_BONUS_AMT         number(15,2),
    EDM_SOURCE_SYS            VARCHAR2(100)       NOT NULL,
    PARTNER_BILLING_CUST_ID      VARCHAR2(200),
    PARTNER_BILLING_PRODUCT_NAME VARCHAR2(200),
    EDM_CREATE_DT             TIMESTAMP(6)        NOT NULL,
    EDM_LAST_UPDT_DT          TIMESTAMP(6)        NOT NULL,
    EDM_LAST_UPDT_SESSION_NM  VARCHAR2(100 BYTE)  NOT NULL
)
LOGGING
;

GRANT ALL    ON T_PROGRAM_BILLING TO DWLOADER;
GRANT SELECT ON T_PROGRAM_BILLING TO EDM_OWNER_SELECT;




