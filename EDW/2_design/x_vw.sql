--
-- compile in EDW as EDW_STAGE_OWNER
--
CREATE OR REPLACE VIEW stg_segment_allocation_vw AS
SELECT * FROM (
  SElECT
    name,
    substr(version,1,12)                        AS version,
--    max(substr(version,1,12)) over (
--      partition by name, cost_center_name, business_segment_id
--    ) as version,
    cost_center_id || ' - ' || cost_center_name AS cost_center,
    start_date,
    end_date,
    business_segment_id,
    business_segment_allocation
  FROM src_source_business_segment
)
PIVOT (
  SUM(business_segment_allocation)
  FOR business_segment_id IN (
    'WEX<=80'                  AS "WEX<=80",
    'WEX81+'                   AS "WEX81+", 
    'BRANDED UNIVERSAL'        AS "BUNIV", 
    'GOVERNMENT'               AS "GOVT",
    'FLEET BILLED COBRAND'     AS "FB COB",
    'PARTNER BILLED COBRAND'   AS "PB COB",
    'PRIVATE LABEL'            AS "PL",
    'PAC PRIDE CUSTOM NETWORK' AS "PACPRIDE",
    'REVOLVER'                 AS "REVOLVER",
    'ALL OTHER'                AS "ALLOTHER"
  )
)
ORDER BY name, version DESC, cost_center
;

CREATE OR REPLACE VIEW STG_SEGMENT_ALLOC_CURR_VW AS
SELECT * 
FROM  stg_segment_allocation_vw
WHERE (name, cost_center, version) IN (
  SELECT name, cost_center, max(version) 
  FROM   stg_segment_allocation_vw 
  GROUP BY name, cost_center
)
;

DECLARE
  EOL constant VARCHAR2(10) := 'ALLOTHER';
  
  v_name VARCHAR2(40);
  v_sql  CLOB := '
    CREATE OR REPLACE VIEW stg_mkt_channel_allocation_vw AS
    SELECT * FROM (
      SELECT
        name,
        substr(version,1,12) version,
        mkt_partner_sub_chl_id,
        start_date,
        end_date,
        mkt_partner_id,
        mkt_sub_chl_allocation
      FROM src_source_mkt_channel
    )
    PIVOT (
        SUM(mkt_sub_chl_allocation)
        FOR mkt_partner_id IN (#{LIST})
    )
    ORDER BY name, version DESC
  ';
  v_list VARCHAR2(2000) := '';
BEGIN
  FOR item IN (
    SELECT UNIQUE mkt_partner_id
    FROM   src_source_mkt_channel
    ORDER BY mkt_partner_id
  )
  LOOP
    v_name := item.mkt_partner_id;
    IF (v_name <> EOL) THEN
      v_list := v_list || '''' || v_name || ''' as "' || v_name || '",';
    END IF;
  END LOOP;
  v_list := v_list || '''' || EOL || ''' as "' || EOL || '"';

  EXECUTE IMMEDIATE REPLACE(v_sql, '#{LIST}', v_list);
END;
/

CREATE OR REPLACE VIEW STG_MKT_CHANNEL_ALLOC_CURR_VW AS
SELECT * 
FROM  stg_mkt_channel_allocation_vw
WHERE (name, mkt_partner_sub_chl_id, version) IN (
  SELECT name, mkt_partner_sub_chl_id, max(version) 
  FROM   stg_mkt_channel_allocation_vw
  GROUP BY name, mkt_partner_sub_chl_id
)
;

GRANT SELECT ON stg_segment_allocation_vw      TO edw_owner_select;
GRANT SELECT ON stg_segment_alloc_curr_vw      TO edw_owner_select;
GRANT SELECT ON stg_mkt_channel_allocation_vw  TO edw_owner_select;
GRANT SELECT ON stg_mkt_channel_alloc_curr_vw  TO edw_owner_select;

CREATE OR REPLACE PUBLIC SYNONYM stg_segment_allocation_vw     FOR edw_stage_owner.stg_segment_allocation_vw;
CREATE OR REPLACE PUBLIC SYNONYM stg_segment_alloc_curr_vw     FOR edw_stage_owner.stg_segment_alloc_curr_vw;

CREATE OR REPLACE PUBLIC SYNONYM stg_mkt_channel_allocation_vw FOR edw_stage_owner.stg_mkt_channel_allocation_vw;
CREATE OR REPLACE PUBLIC SYNONYM stg_mkt_channel_alloc_curr_vw FOR edw_stage_owner.stg_mkt_channel_alloc_curr_vw;


