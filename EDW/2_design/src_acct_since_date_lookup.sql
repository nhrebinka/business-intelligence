--
-- SINCE_DATE (TANDOM account setup date) conversion lookup table
--
CREATE TABLE SRC_ACCT_SINCE_DATE_LOOKUP
(
  ACCT_TYPE             varchar2(50),
  ACCT_STS              varchar2(30),
  EDM_ACCT_KEY          number        NOT NULL,
  WEX_ACCT_NBR          VARCHAR2(100) NOT NULL,
  CUR_ACTIV_DT          DATE,
  LEGACY_SOURCE_SYS_NM  VARCHAR2(200),
  LEGACY_XREF_NM_OR_NBR VARCHAR2(100),
  LEGACY_ACTIV_DT       DATE,
  SINCE_DATE            DATE,
  CONVERSION_RULE_CD    VARCHAR2(200),
  CONFIDENCE_LEVEL      NUMBER,
  UPDATED_CD            VARCHAR2(200),
  UPDATED_AT            TIMESTAMP     NOT NULL,
  SESSION_ID            varchar2(40)  NOT NULL
)
TABLESPACE D_BI
;
