select * from dba_directories;

OWNER  DIRECTORY_NAME         DIRECTORY_PATH
------ ---------------------- -------------------------------------------------
SYS    MASTER_KEY_DIR         /acfs01/ora/db_key
SYS    DATA_PUMP_DIR_HOTBKUP  /hotbackup/prhub
SYS    DPUMP_DIR              /i1/data/data_pump
SYS    ORACLE_OCM_CONFIG_DIR  /app/oracle/product/11.2.0.3/db_home1/ccr/state
SYS    DPUMP_LOG              /app/oracle/product/11.1.0/rdbms/log
SYS    DPUMP_ASM              +PREDW_DG_DATA/PRBIHUBC/DPUMP
SYS    EDM_DATA_DIR           /i1/stage/hub/data
SYS    DBA_HUB_DIR            /i1/stage/hub/oracle
SYS    EDM_LOG_DIR            /i1/stage/hub/logs
SYS    DATA_PUMP_DIR          /app/oracle/product/11.2.0.3/db_home1/rdbms/log/
SYS    ORACLE_OCM_CONFIG_DIR2 /app/oracle/product/11.1.0/ccr/state
