DROP TABLE STAGE_OWNER.SRC_IFCS_HKG_TRANSACTION CASCADE CONSTRAINTS;

CREATE TABLE STAGE_OWNER.SRC_IFCS_HKG_TRANSACTION
(
  ISO_CODE                  NUMBER,
  CLIENT_NUMBER             NUMBER,
  BATCH_NUMBER              VARCHAR2(6 BYTE),
  PAN                       VARCHAR2(19 BYTE),
  PAN_EXTRA                 VARCHAR2(19 BYTE),
  INVOICE_NBR               NUMBER(9),
  PROCESSING_DT             VARCHAR2(10 BYTE),
  TRANSACTION_DT            VARCHAR2(10 BYTE),
  TRANSACTION_TIME          VARCHAR2(8 BYTE),
  TICKET_NBR                VARCHAR2(8 BYTE),
  ODOMETER_NBR              NUMBER(7),
  MERCHANT_NBR              VARCHAR2(10 BYTE),
  TRANSACTION_AMT           VARCHAR2(8 BYTE),
  CURRENCY                  VARCHAR2(3 BYTE),
  AUTHORIZATION_CD          VARCHAR2(8 BYTE),
  TRANSACTION_TYPE          VARCHAR2(1 BYTE),
  PRODUCT_CD                VARCHAR2(15 BYTE),
  PRODUCT_NAME              VARCHAR2(20 BYTE),
  QUANTITY                  VARCHAR2(7 BYTE),
  UNIT_PRICE                VARCHAR2(8 BYTE),
  GST_RATE                  VARCHAR2(4 BYTE),
  SITE                      VARCHAR2(6 BYTE),
  INVOICE_DT                VARCHAR2(10 BYTE),
  ADDITNL_POS_INFO          VARCHAR2(18 BYTE),
  SITE_NAME                 VARCHAR2(32 BYTE),
  MKIII_RPRT_COMM_PRD_TYP   VARCHAR2(4 BYTE),
  MKIII_RPRT_CNTRY_CD       VARCHAR2(3 BYTE),
  MKIII_DELIVRNG_CMPNY_CD   VARCHAR2(5 BYTE),
  MKIII_SUPP_ISSUR_CURR_CD  VARCHAR2(3 BYTE),
  SUPP_ISSUR_REBATE         VARCHAR2(7 BYTE),
  SUPP_ISSUR_INVCED_AMT     VARCHAR2(9 BYTE),
  DUTY_RATE                 VARCHAR2(4 BYTE),
  DUTY_AMT                  VARCHAR2(7 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY IFCS_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
            LOGFILE       IFCS_LOG_DIR:'SRC_IFCS_HKG_TRANSACTION.log'
            BADFILE       IFCS_LOG_DIR:'SRC_IFCS_HKG_TRANSACTION.bad'
            DISCARDFILE   IFCS_LOG_DIR:'SRC_IFCS_HKG_TRANSACTION.dis'
            SKIP 2
            FIELDS LRTRIM
            MISSING FIELD VALUES ARE NULL
            REJECT ROWS WITH ALL NULL FIELDS
      ( 
        ISO_CODE                    (1:6),
        CLIENT_NUMBER               (8:14),
        BATCH_NUMBER                (16:21),
        PAN                         (23:41),
        PAN_EXTRA                   (43:61),
        INVOICE_NBR                 (63:71),
        PROCESSING_DT               (73:82),
        TRANSACTION_DT              (84:93),
        TRANSACTION_TIME            (95:102),
        TICKET_NBR                  (104:111),
        ODOMETER_NBR                (113:119),
        MERCHANT_NBR                (121:130),
        TRANSACTION_AMT             (132:139),
        CURRENCY                    (141:143),
        AUTHORIZATION_CD            (145:152),
        TRANSACTION_TYPE            (154:154),
        PRODUCT_CD                  (156:170),
        PRODUCT_NAME                (172:191),
        QUANTITY                    (193:199),
        UNIT_PRICE                  (201:208),
        GST_RATE                    (210:213),
        SITE                        (215:220),
        INVOICE_DT                  (222:231),
        ADDITNL_POS_INFO            (233:250),
        SITE_NAME                   (252:283),
        MKIII_RPRT_COMM_PRD_TYP     (285:288),
        MKIII_RPRT_CNTRY_CD         (290:292),
        MKIII_DELIVRNG_CMPNY_CD     (294:298),
        MKIII_SUPP_ISSUR_CURR_CD    (300:302),
        SUPP_ISSUR_REBATE           (304:310),
        SUPP_ISSUR_INVCED_AMT       (312:319),
        DUTY_RATE                   (321:324),
        DUTY_AMT                    (326:332)

      )
         )
     LOCATION (IFCS_DATA_DIR:'M71.PRD.ES344.D15159.T094230')
  )
REJECT LIMIT UNLIMITED
PARALLEL ( DEGREE 2 INSTANCES 1 )
NOMONITORING;


GRANT ALTER, SELECT ON STAGE_OWNER.SRC_IFCS_HKG_TRANSACTION TO STAGE_OWNER_SELECT;
