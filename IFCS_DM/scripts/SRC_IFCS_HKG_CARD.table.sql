DROP TABLE STAGE_OWNER.SRC_IFCS_HKG_CARD CASCADE CONSTRAINTS;

CREATE TABLE STAGE_OWNER.SRC_IFCS_HKG_CARD
(
  ISO_CODE                  NUMBER,
  CLIENT_NUMBER             NUMBER,
  CARD_PAN                  VARCHAR2(19 BYTE),
  CARD_STATUS               VARCHAR2(1 BYTE),
  REASON_CODE               VARCHAR2(1 BYTE),
  LAST_UPDATE_DT            VARCHAR2(10 BYTE),
  LAST_UPDATE_TIME          VARCHAR2(8 BYTE),
  CARD_EXPIRY_DATE          VARCHAR2(10 BYTE),
  CARD_ISSUE_DATE           VARCHAR2(10 BYTE),
  LAST_USE_DATE             VARCHAR2(10 BYTE),
  EMBOSS_NAME               VARCHAR2(27 BYTE),
  CARD_TYPE                 VARCHAR2(1 BYTE),
  DRIVER_NAME               VARCHAR2(16 BYTE),
  VEHICLE_REGISTRATION_NBR  VARCHAR2(16 BYTE),
  PIN_REQUEST               VARCHAR2(1 BYTE),
  PREVIOUS_CARD_NBR         VARCHAR2(19 BYTE),
  NEXT_CARD_NBR             VARCHAR2(19 BYTE),
  CARD_COBRAND_CD           VARCHAR2(2 BYTE),
  SPEEDPASS                 VARCHAR2(1 BYTE),
  DIALOG_IND                VARCHAR2(1 BYTE),
  CARD_RECEIVER_CODE        VARCHAR2(1 BYTE),
  CARDHOLDER_NAME_1         VARCHAR2(32 BYTE),
  CARDHOLDER_ADD_1          VARCHAR2(32 BYTE),
  CARDHOLDER_ADD_2          VARCHAR2(32 BYTE),
  CARDHOLDER_ADD_3          VARCHAR2(32 BYTE),
  CARDHOLDER_ADD_4          VARCHAR2(32 BYTE),
  CARDHOLDER_CITY           VARCHAR2(32 BYTE),
  CARDHOLDER_POSTCODE       VARCHAR2(12 BYTE),
  CARDHOLDER_CNTRY          VARCHAR2(3 BYTE),
  PIN_RECEIVER_CODE         VARCHAR2(1 BYTE),
  FUEL_RESTRICTION          VARCHAR2(1 BYTE),
  NON_FUEL_RESTRICTION      VARCHAR2(1 BYTE),
  SITE_RESTRICTION_CD_1     VARCHAR2(3 BYTE),
  SITE_RESTRICTION_CD_2     VARCHAR2(3 BYTE),
  SITE_RESTRICTION_CD_3     VARCHAR2(3 BYTE),
  SITE_RESTRICTION_CD_4     VARCHAR2(3 BYTE),
  SITE_RESTRICTION_CD_5     VARCHAR2(3 BYTE),
  CARD_PROFILE              VARCHAR2(4 BYTE),
  PIN_TYPE                  VARCHAR2(1 BYTE),
  INTERCHANGE_DESIGNATOR    VARCHAR2(1 BYTE),
  BLOCK_DATE                VARCHAR2(10 BYTE),
  BLOCK_TIME                VARCHAR2(8 BYTE),
  STATISTICAL_SORT_LABEL_1  VARCHAR2(20 BYTE),
  STATISTICAL_SORT_LABEL_2  VARCHAR2(20 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY IFCS_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
            LOGFILE       IFCS_LOG_DIR:'SRC_IFCS_HKG_CARD.log'
            BADFILE       IFCS_LOG_DIR:'SRC_IFCS_HKG_CARD.bad'
            DISCARDFILE   IFCS_LOG_DIR:'SRC_IFCS_HKG_CARD.dis'
            SKIP 2
            FIELDS LRTRIM
            MISSING FIELD VALUES ARE NULL
            REJECT ROWS WITH ALL NULL FIELDS
      ( 
        ISO_CODE                         (1:6),
        CLIENT_NUMBER                    (8:14),
        CARD_PAN                        (16:34),
        CARD_STATUS                     (36:36),
        REASON_CODE                     (38:38),
        LAST_UPDATE_DT                  (40:49),
        LAST_UPDATE_TIME                  (51:58),
        CARD_EXPIRY_DATE                  (60:69),
        CARD_ISSUE_DATE                  (71:80),
        LAST_USE_DATE                  (82:91),
        EMBOSS_NAME                  (93:119),
        CARD_TYPE                  (121:121),
        DRIVER_NAME                  (123:138),
        VEHICLE_REGISTRATION_NBR                  (140:155),
        PIN_REQUEST                  (157:157),
        PREVIOUS_CARD_NBR                  (159:177),
        NEXT_CARD_NBR                  (179:197),
        CARD_COBRAND_CD                 (199:200),
        SPEEDPASS                  (202:202),
        DIALOG_IND                   (204:204),
        CARD_RECEIVER_CODE                  (206:206),
        CARDHOLDER_NAME_1                  (208:239),
        CARDHOLDER_ADD_1                  (241:272),
        CARDHOLDER_ADD_2                  (274:305),
        CARDHOLDER_ADD_3                  (307:338),
        CARDHOLDER_ADD_4                  (340:371),
        CARDHOLDER_CITY                  (373:404),
        CARDHOLDER_POSTCODE                  (406:417),
        CARDHOLDER_CNTRY                  (419:421),
        PIN_RECEIVER_CODE                  (423:423),
        FUEL_RESTRICTION                   (425:425),
        NON_FUEL_RESTRICTION                  (427:427),
        SITE_RESTRICTION_CD_1                  (429:431),
        SITE_RESTRICTION_CD_2                  (433:435),
        SITE_RESTRICTION_CD_3                  (437:439),
        SITE_RESTRICTION_CD_4                  (441:443),
        SITE_RESTRICTION_CD_5                  (445:447),
        CARD_PROFILE                  (449:452),
        PIN_TYPE                   (454:454),
        INTERCHANGE_DESIGNATOR                  (456:456),
        BLOCK_DATE                  (458:467),
        BLOCK_TIME                  (469:476),
        STATISTICAL_SORT_LABEL_1                  (478:497),
        STATISTICAL_SORT_LABEL_2                  (499:518)

      )
              )
     LOCATION (IFCS_DATA_DIR:'M31.PRD.ES344.D15159.T094230')
  )
REJECT LIMIT 0
PARALLEL ( DEGREE 2 INSTANCES 1 )
NOMONITORING;


GRANT ALTER, SELECT ON STAGE_OWNER.SRC_IFCS_HKG_CARD TO STAGE_OWNER_SELECT;
