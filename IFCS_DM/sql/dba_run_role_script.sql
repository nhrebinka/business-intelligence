SET ECHO ON 
SET SERVEROUTPUT ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
SET LINESIZE 2000

/*
   NAME         :-    dba_run_role.sql
   AUTHOUR      :-    Venugopal KAncharla
   DATE         :-    11/08/2015
   DECRIPTION   :-    SQL  To create Roles for WES CIC  
   LOG          : -   VERSION 1.0

  */

 
SPOOL /i1/&env/hub/logs/dba_run_role_script.log ;

DROP ROLE IFCS_OWNER_SELECT;
CREATE ROLE IFCS_OWNER_SELECT NOT IDENTIFIED;

DROP ROLE IFCS_SATGE_OWNER_SELECT;
CREATE ROLE IFCS_STAGE_OWNER_SELECT NOT IDENTIFIED;
 

-- Grantees of EDW_OWNER_SELECT

GRANT IFCS_OWNER_SELECT TO DWLOADER;
GRANT IFCS_OWNER_SELECT TO IFCS_STAGE_OWNER;
GRANT IFCS_OWNER_SELECT TO HUBLOADER;
GRANT IFCS_OWNER_SELECT TO USER_LDAP ;
GRANT IFCS_OWNER_SELECT TO IFCS_BORPTRNR  ;

GRANT IFCS_STAGE_OWNER_SELECT TO DWLOADER;
GRANT IFCS_STAGE_OWNER_SELECT TO IFCS_OWNER;
GRANT IFCS_STAGE_OWNER_SELECT TO HUBLOADER;
GRANT IFCS_SATGE_OWNER_SELECT TO USER_LDAP ;
GRANT IFCS_STAGE_OWNER_SELECT TO IFCS_BORPTRNR  ;

SPOOL  OFF ; 
