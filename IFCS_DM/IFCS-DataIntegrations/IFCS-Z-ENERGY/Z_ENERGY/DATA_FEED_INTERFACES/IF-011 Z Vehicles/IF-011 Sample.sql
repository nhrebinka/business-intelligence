WITH last_process_date
     AS ( SELECT To_date( '2016-03-02', 'YYYY-MM-DD' ) last_proc -- Replace with last process date for the interface
          FROM   DUAL )
--select * from last_process_date

SELECT To_char( t.processed_at, 'DD/MM/YYYY' )             "Run-date",
       -- BEGIN added for debugging DO NOT EXTRACT		
       cu.customer_no,
       c.card_no,
       t.effective_at,
       -- END added for debugging DO NOT EXTRACT		
       ccc.customer_cost_centre_code                       "Cost-centre",
       ( t.customer_amount - t.customer_tax_amount ) * 100 "Net-amount",
       v.license_plate                                     "Registration-number",
       t.reference                                         "Transaction-number",
       t.customer_tax_amount * 100                         "GST"
FROM   TRANSACTIONS t
       inner join CARDS c
               ON c.card_oid = t.card_oid
       inner join M_CUSTOMERS cu
               ON c.customer_mid = cu.customer_mid
       -- BEGIN for debugging TO BE REMOVED
                  AND
                  cu.customer_no = '70000172' -- for debugging TO BE REMOVED
       -- END for debugging TO BE REMOVED
       inner join M_CLIENTS cl
               ON cl.client_mid = cu.client_mid AND
                  cl.client_mid = 50
       inner join APPLICATIONS ap
               ON ap.application_oid = cu.application_oid
       left join VEHICLES v
              ON c.vehicle_oid = v.vehicle_oid
       left join COST_CENTRES cc
              ON t.card_oid = cc.card_oid AND
                 t.effective_at BETWEEN cc.effective_on AND ( cc.expires_on + 86399 / 86400 )
       left join CUSTOMER_COST_CENTRES ccc
              ON cc.customer_cost_centre_oid = ccc.customer_cost_centre_oid
       inner join APPLICATION_TYPES apt
               ON apt.application_type_oid = ap.application_type_oid AND
                  apt.external_code = 'ZCO'
       -- BEGIN for debugging TO BE REMOVED
       --                  apt.description = 'Z Company Vehicles' -- Use until external_code added to application_types
       -- BEGIN for debugging TO BE REMOVED
       inner join last_process_date lpd
               ON t.processed_at > lpd.last_proc AND
                  t.processed_at < cl.processing_date
ORDER  BY t.processed_at, ccc.customer_cost_centre_code, v.license_plate, t.reference
