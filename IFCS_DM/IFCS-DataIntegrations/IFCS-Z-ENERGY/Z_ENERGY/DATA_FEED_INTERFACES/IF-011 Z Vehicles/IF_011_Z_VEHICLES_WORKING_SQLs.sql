SELECT * FROM V$TIMEZONE_NAMES;

SELECT SESSIONTIMEZONE, DBTIMEZONE FROM DUAL;

SELECT
SYSTIMESTAMP, NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST') AS newtime_test FROM dual;

SELECT
SYSTIMESTAMP, TO_CHAR(NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST'), 'dd-mm-yy hh24:mi:ss AM') AS newtime_test FROM dual;

SELECT *
FROM NLS_DATABASE_PARAMETERS
WHERE PARAMETER = 'NLS_TIMESTAMP_TZ_FORMAT';

SELECT 
MC.CLIENT_MID                    AS        CLIENT_MID, 
--MC.LAST_UPDATED_AT                AS        LAST_UPDATED_AT, 
SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'GMT' AS GMT_SYSTIMESTAMP , 
SYSTIMESTAMP AT TIME ZONE 'NZ' AS NZST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'EST' AS EST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'US/Eastern' AS EDT_SYSTIMESTAMP ,
CAST( SYSTIMESTAMP AS TIMESTAMP ),
FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) ,
CAST(
         FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
       
SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR AS GMT_OFFSET_SYSTIMESTAMP, 
TRUNC (SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR, 'DD') AS GMT_OFFSET_DATE , 
MC.LANGUAGE_OID        AS        LANGUAGE_OID, 
MC.CURRENCY_OID        AS        CURRENCY_OID, 
MC.NAME                AS        NAME, 
MC.COUNTRY_OID        AS        COUNTRY_OID, 
MC.ACCOUNT_NO        AS        ACCOUNT_NO, 
MC.SHORT_NAME        AS        SHORT_NAME, 
MC.TAX_NO            AS        TAX_NO, 
MC.PROCESSING_DATE    AS        PROCESSING_DATE, 
MC.CURRENT_MONTH    AS        CURRENT_MONTH, 
MC.CURRENT_YEAR        AS        CURRENT_YEAR, 
MC.TIMEZONE_OID        AS        TIMEZONE_OID, 
MC.MONTH_END_1_ON    AS        MONTH_END_1_ON, 
MC.MONTH_END_2_ON    AS        MONTH_END_2_ON, 
MC.MONTH_END_3_ON     AS        MONTH_END_3_ON
FROM
M_CLIENTS MC
WHERE 
CLIENT_MID = 63
;



SELECT
SYSTIMESTAMP ,
MCUST.LAST_UPDATED_AT ,
TO_CHAR(MCUST.LAST_UPDATED_AT,'DD/MM/YYYY')        AS     MODIFIED_DATE, 
TO_CHAR(MCUST.LAST_UPDATED_AT,'HH24:MI:SS')        AS     MODIFIED_TIME ,
TO_CHAR(NEW_TIME(MCUST.LAST_UPDATED_AT, 'GMT', 'EST'), 'DD/MM/YYYY' ) AS MODIFIED_DATE_EST ,
--MCUST.LAST_UPDATED_AT AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
CAST( MCUST.LAST_UPDATED_AT  AS TIMESTAMP ),
 FROM_TZ(
            CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ) ,
           'UTC'
         ),
CAST(
         FROM_TZ(
           CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),
           'UTC' --
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
	   TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'DD/MM/YYYY')
       AS Auckland_DATE ,
       
TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'HH24:MI:SS')
       AS Auckland_TIME

FROM
IFCS_APAC_DEV_2.M_CUSTOMERS MCUST
WHERE
MCUST.CLIENT_MID=50
;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Source qualifier SQL */   
--Updated on 20180919

	   

WITH PROCESS_DATE_OVERRIDE AS 
( SELECT 
CASE
WHEN LENGTH(TRIM('$$PROCESS_DATE_OVERRIDE')) > 0
THEN TO_DATE('$$PROCESS_DATE_OVERRIDE','DD/MM/YYYY') 
ELSE NULL
END	AS  PROCESS_DATE_OVERRIDE 
FROM   DUAL )
SELECT
T.PROCESSED_AT								AS		PROCESSED_AT , 
TO_CHAR( T.PROCESSED_AT, 'DD/MM/YYYY' )		AS		RUN_DATE,
CU.CUSTOMER_NO								AS		CUSTOMER_NO,
C.CARD_NO									AS		CARD_NO,
T.EFFECTIVE_AT								AS		EFFECTIVE_AT,
CCC.CUSTOMER_COST_CENTRE_CODE				AS		COST_CENTRE,
TO_CHAR((T.CUSTOMER_AMOUNT - T.CUSTOMER_TAX_AMOUNT ) * 100 )	AS	 NET_AMOUNT,
V.LICENSE_PLATE								AS		REGISTRATION_NUMBER,
T.REFERENCE									AS		TRANSACTION_NUMBER,
TO_CHAR(T.CUSTOMER_TAX_AMOUNT * 100	)		AS		GST
FROM  $$IFCS_SRC_SCHEMA.TRANSACTIONS T
INNER JOIN $$IFCS_SRC_SCHEMA.CARDS C
ON C.CARD_OID = T.CARD_OID
INNER JOIN $$IFCS_SRC_SCHEMA.M_CUSTOMERS CU
ON C.CUSTOMER_MID = CU.CUSTOMER_MID
INNER JOIN $$IFCS_SRC_SCHEMA.M_CLIENTS CL
ON CL.CLIENT_MID = CU.CLIENT_MID AND  CL.CLIENT_MID = $$CLIENT_MID
INNER JOIN $$IFCS_SRC_SCHEMA.APPLICATIONS AP
ON AP.APPLICATION_OID = CU.APPLICATION_OID
LEFT JOIN $$IFCS_SRC_SCHEMA.VEHICLES V
ON C.VEHICLE_OID = V.VEHICLE_OID
LEFT JOIN $$IFCS_SRC_SCHEMA.COST_CENTRES CC
ON T.CARD_OID = CC.CARD_OID AND
T.EFFECTIVE_AT BETWEEN CC.EFFECTIVE_ON AND ( CC.EXPIRES_ON + 86399 / 86400 )
LEFT JOIN $$IFCS_SRC_SCHEMA.CUSTOMER_COST_CENTRES CCC
ON CC.CUSTOMER_COST_CENTRE_OID = CCC.CUSTOMER_COST_CENTRE_OID
INNER JOIN $$IFCS_SRC_SCHEMA.APPLICATION_TYPES APT
ON APT.APPLICATION_TYPE_OID = AP.APPLICATION_TYPE_OID AND APT.EXTERNAL_CODE = 'ZCO'
INNER JOIN PROCESS_DATE_OVERRIDE PDO
ON T.PROCESSED_AT BETWEEN NVL( PDO.PROCESS_DATE_OVERRIDE, CL.PROCESSING_DATE ) - 1 AND NVL( PDO.PROCESS_DATE_OVERRIDE, CL.PROCESSING_DATE ) - 1 / 86400
ORDER  BY T.PROCESSED_AT, CCC.CUSTOMER_COST_CENTRE_CODE, V.LICENSE_PLATE, T.REFERENCE


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

'Run-date'
'Cost-centre'
'Net-amount'
'Registration-number'
'Transaction-number'
'GST'

RUN_DATE,

CUSTOMER_NO,
CARD_NO,
EFFECTIVE_AT,

COST_CENTRE,
NET_AMOUNT,
REGISTRATION_NUMBER,
TRANSACTION_NUMBER,
GST
