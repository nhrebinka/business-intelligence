SELECT * FROM V$TIMEZONE_NAMES;

SELECT SESSIONTIMEZONE, DBTIMEZONE FROM DUAL;

SELECT
SYSTIMESTAMP, NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST') AS newtime_test FROM dual;

SELECT
SYSTIMESTAMP, TO_CHAR(NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST'), 'dd-mm-yy hh24:mi:ss AM') AS newtime_test FROM dual;

SELECT *
FROM NLS_DATABASE_PARAMETERS
WHERE PARAMETER = 'NLS_TIMESTAMP_TZ_FORMAT';

SELECT 
MC.CLIENT_MID                    AS        CLIENT_MID, 
--MC.LAST_UPDATED_AT                AS        LAST_UPDATED_AT, 
SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'GMT' AS GMT_SYSTIMESTAMP , 
SYSTIMESTAMP AT TIME ZONE 'NZ' AS NZST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'EST' AS EST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'US/Eastern' AS EDT_SYSTIMESTAMP ,
CAST( SYSTIMESTAMP AS TIMESTAMP ),
FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) ,
CAST(
         FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
       
SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR AS GMT_OFFSET_SYSTIMESTAMP, 
TRUNC (SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR, 'DD') AS GMT_OFFSET_DATE , 
MC.LANGUAGE_OID        AS        LANGUAGE_OID, 
MC.CURRENCY_OID        AS        CURRENCY_OID, 
MC.NAME                AS        NAME, 
MC.COUNTRY_OID        AS        COUNTRY_OID, 
MC.ACCOUNT_NO        AS        ACCOUNT_NO, 
MC.SHORT_NAME        AS        SHORT_NAME, 
MC.TAX_NO            AS        TAX_NO, 
MC.PROCESSING_DATE    AS        PROCESSING_DATE, 
MC.CURRENT_MONTH    AS        CURRENT_MONTH, 
MC.CURRENT_YEAR        AS        CURRENT_YEAR, 
MC.TIMEZONE_OID        AS        TIMEZONE_OID, 
MC.MONTH_END_1_ON    AS        MONTH_END_1_ON, 
MC.MONTH_END_2_ON    AS        MONTH_END_2_ON, 
MC.MONTH_END_3_ON     AS        MONTH_END_3_ON
FROM
M_CLIENTS MC
WHERE 
CLIENT_MID = 63
;



SELECT
SYSTIMESTAMP ,
MCUST.LAST_UPDATED_AT ,
TO_CHAR(MCUST.LAST_UPDATED_AT,'DD/MM/YYYY')        AS     MODIFIED_DATE, 
TO_CHAR(MCUST.LAST_UPDATED_AT,'HH24:MI:SS')        AS     MODIFIED_TIME ,
TO_CHAR(NEW_TIME(MCUST.LAST_UPDATED_AT, 'GMT', 'EST'), 'DD/MM/YYYY' ) AS MODIFIED_DATE_EST ,
--MCUST.LAST_UPDATED_AT AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
CAST( MCUST.LAST_UPDATED_AT  AS TIMESTAMP ),
 FROM_TZ(
            CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ) ,
           'UTC'
         ),
CAST(
         FROM_TZ(
           CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),
           'UTC' --
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
	   TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'DD/MM/YYYY')
       AS Auckland_DATE ,
       
TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'HH24:MI:SS')
       AS Auckland_TIME

FROM
IFCS_APAC_DEV_2.M_CUSTOMERS MCUST
WHERE
MCUST.CLIENT_MID=50
;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Source qulaifier SQL IFCS*/


SELECT DISTINCT
TO_CHAR(DETAIL_GROUPS.BILLED_ON,'DD/MM/YYYY')           AS	 INVOICE_DATE,
TO_CHAR(PAYMENT_REQUESTS.DUE_ON,'DD/MM/YYYY')           AS	 INVOICE_DUE,
M_CUSTOMERS.CUSTOMER_NO                                 AS	 CUSTOMER_NUMBER,
DETAIL_GROUPS.REMITTANCE_ID                             AS   INVOICE_NUMBER,
TO_CHAR(DETAIL_GROUPS.REMITTANCE_AMOUNT * 100 )         AS	 INVOICE_VALUE
FROM $$IFCS_SRC_SCHEMA.M_CUSTOMERS
INNER JOIN $$IFCS_SRC_SCHEMA.ACCOUNTS          
ON ACCOUNTS.CUSTOMER_MID = M_CUSTOMERS.CUSTOMER_MID
INNER JOIN $$IFCS_SRC_SCHEMA.PAYMENT_REQUESTS  
ON PAYMENT_REQUESTS.ACCOUNT_OID = ACCOUNTS.ACCOUNT_OID
INNER JOIN $$IFCS_SRC_SCHEMA.DETAIL_GROUPS     
ON DETAIL_GROUPS.ACCOUNT_OID = ACCOUNTS.ACCOUNT_OID 
AND DETAIL_GROUPS.PAYMENT_REQUEST_OID = PAYMENT_REQUESTS.PAYMENT_REQUEST_OID
INNER JOIN $$IFCS_SRC_SCHEMA.CURRENCIES        
ON CURRENCIES.CURRENCY_OID = M_CUSTOMERS.CURRENCY_OID
WHERE 
DETAIL_GROUPS.REMITTANCE_ID IS NOT NULL
AND DETAIL_GROUPS.BILLED_ON IS NOT NULL
AND M_CUSTOMERS.CLIENT_MID  = $$CLIENT_MID 
AND (
ACCOUNTS.LAST_UPDATED_AT > TO_DATE('$$BATCH_RUN_DATE','DD/MM/YYYY HH24:MI:SS') 
OR M_CUSTOMERS.LAST_UPDATED_AT > TO_DATE('$$BATCH_RUN_DATE','DD/MM/YYYY HH24:MI:SS') 
OR PAYMENT_REQUESTS.LAST_UPDATED_AT > TO_DATE('$$BATCH_RUN_DATE','DD/MM/YYYY HH24:MI:SS') 
OR DETAIL_GROUPS.LAST_UPDATED_AT > TO_DATE('$$BATCH_RUN_DATE','DD/MM/YYYY HH24:MI:SS') 
)
	   
	   
	   OPTION - II - through IFCS

    SELECT ACCOUNT_OID,
           M_CUSTOMERS.CLIENT_MID || M_CUSTOMERS.CUSTOMER_NO
      FROM ACCOUNTS
INNER JOIN M_CUSTOMERS      ON M_CUSTOMERS.CUSTOMER_MID = ACCOUNTS.CUSTOMER_MID 
INNER JOIN PAYMENT_REQUESTS ON PAYMENT_REQUESTS.ACCOUNT_OID = ACCOUNTS.ACCOUNT_OID
INNER JOIN DETAIL_GROUPS    ON DETAIL_GROUPS.ACCOUNT_OID = ACCOUNTS.ACCOUNT_OID 
           AND DETAIL_GROUPS.PAYMENT_REQUEST_OID = PAYMENT_REQUESTS.PAYMENT_REQUEST_OID
     WHERE M_CUSTOMERS.CLIENT_MID = {CLIENT_MID TO BE EXTRACTED}
       AND (ACCOUNTS.LAST_UPDATED_AT > {LAST EXTRACT DATE} 
        OR M_CUSTOMERS.LAST_UPDATED_AT > {LAST EXTRACT DATE}
        OR PAYMENT_REQUESTS.LAST_UPDATED_AT > {LAST EXTRACT DATE}
        OR DETAIL_GROUPS.LAST_UPDATED_AT > {LAST EXTRACT DATE})
       AND DETAIL_GROUPS.REMITTANCE_ID IS NOT NULL


OPTION - II - through IFCS

    select distinct
           to_char(detail_groups.billed_on,'dd/mm/yyyy')           as ""invoice-date"",
           to_char(payment_requests.due_on,'dd/mm/yyyy')           as ""invoice-due"",
           m_customers.customer_no                                 as ""customer-number"",
           detail_groups.remittance_id                             as ""invoice-number"",
           detail_groups.remittance_amount * 100                   as ""invoice-value""
      from m_customers
inner join accounts          on accounts.customer_mid = m_customers.customer_mid
inner join payment_requests  on payment_requests.account_oid = accounts.account_oid
inner join detail_groups     on detail_groups.account_oid = accounts.account_oid 
                               and detail_groups.payment_request_oid = payment_requests.payment_request_oid
inner join currencies        on currencies.currency_oid = m_customers.currency_oid
     where detail_groups.remittance_id is not null
       and detail_groups.billed_on is not null
       and m_customers.client_mid  = 50 --{Client to be extracted}
       and {EXTRACT CRITERIA MENTIONED ABOVE}

	   


/* Source qulaifier SQL ICP*/

SELECT DISTINCT
TO_CHAR(DOCUMENT.INVOICEDATE,'DD/MM/YYYY')  AS 		INVOICE_DATE,
TO_CHAR(DOCUMENT.DUEDATE,'DD/MM/YYYY')      AS 		INVOICE_DUE,
CUSTOMER.CUSTOMERNUMBER                     AS 		CUSTOMER_NUMBER,
DOCUMENT.DOCUMENTNUMBER                     AS 		INVOICE_NUMBER,
NVL(DOCUMENT.TOTALGROSSAMOUNTIISC * 100,0)  AS 		INVOICE_VALUE
FROM $$ICP_SRC_SCHEMA.DOCUMENT
INNER JOIN $$ICP_SRC_SCHEMA.DOCUMENTTYPE    
ON DOCUMENTTYPE.ID_DYPE = DOCUMENT.ID_DYPE
INNER JOIN $$ICP_SRC_SCHEMA.CUSTOMER        
ON CUSTOMER.ID_CUST = DOCUMENT.ID_CUST
INNER JOIN $$ICP_SRC_SCHEMA.PARTICIPANTROLE 
ON PARTICIPANTROLE.ID_PARO = CUSTOMER.ID_CARI
INNER JOIN $$ICP_SRC_SCHEMA.PARTICIPANT     
ON PARTICIPANT.ID_PART = PARTICIPANTROLE.ID_PART
WHERE 
PARTICIPANT.PARTICIPANTID = $$CLIENT_MID
AND DOCUMENT.DUEDATE IS NOT NULL
AND DOCUMENT.INVOICEDATE IS NOT NULL
AND DOCUMENTTYPE.TYPENUMBER IN ('02','55')
AND DOCUMENT.ID_DOTY = 1
AND DOCUMENT.MODIFIEDDATE >= TO_DATE('$$BATCH_RUN_DATE','DD/MM/YYYY HH24:MI:SS')


INVOICE_DATE
INVOICE_DUE
CUSTOMER_NUMBER
INVOICE_NUMBER
INVOICE_VALUE


OPTION - I - through ICP

    SELECT DOCUMENTNUMBER
      FROM DOCUMENT
INNER JOIN CUSTOMERS       ON CUSTOMER.ID_CUST = DOCUMENT.ID_CUST
INNER JOIN PARTICIPANTROLE ON PARTICIPANTROLE.ID_PARO = CUSTOMER.ID_CARI
INNER JOIN PARTICIPANT     ON PARTICIPANT.ID_PART = PARTICIPANTROLE.ID_PART
     WHERE PARTICIPANT.PARTICIPANTID = {CLIENT_MID TO BE EXTRACTED}
       AND (DOCUMENT.MODIFIEDDATE > {LAST EXTRACT DATE} 
       AND DOCUMENT.DUEDATE IS NOT NULL
       AND DOCUMENT.INVOICEDATE IS NOT NULL
       AND DOCUMENTTYPE.TYPENUMBER IN ('02','55')
       AND DOCUMENT.ID_DOTY = 1

OPTION - I - through ICP

    select distinct
           to_char(document.invoicedate,'dd/mm/yyyy')  as ""invoice-date"",
           to_char(document.duedate,'dd/mm/yyyy')      as ""invoice-due"",
           customer.customernumber                     as ""customer-number"",
           document.documentnumber                     as ""invoice-number"",
           nvl(document.totalgrossamountiisc * 100,0)  as ""invoice-value""
      from document
inner join documenttype    on documenttype.id_dype = document.id_dype
inner join customer        on customer.id_cust = document.id_cust
inner join participantrole on participantrole.id_paro = customer.id_cari
inner join participant     on participant.id_part = participantrole.id_part
     where participant.participantid = 50 --{Client to be extracted}
       and document.duedate is not null
       and document.invoicedate is not null
       and documenttype.typenumber in ('02','55')
       and document.id_doty = 1
       and {EXTRACT CRITERIA MENTIONED ABOVE}


	   


	   
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Old header names

'Invoice-Date'
'Invoice-Due'
'Customer-Number'
'Invoice-Number'
'Invoice-Value'

-- New Header Names

'invoice-date'
'invoice-due'
'customer-number'
'invoice-number'
'invoice-value'




INVOICE_DATE
INVOICE_DUE
CUSTOMER_NUMBER
INVOICE_NUMBER
INVOICE_VALUE


