SELECT * FROM V$TIMEZONE_NAMES;

SELECT SESSIONTIMEZONE, DBTIMEZONE FROM DUAL;

SELECT
SYSTIMESTAMP, NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST') AS newtime_test FROM dual;

SELECT
SYSTIMESTAMP, TO_CHAR(NEW_TIME(SYSTIMESTAMP, 'GMT', 'EST'), 'dd-mm-yy hh24:mi:ss AM') AS newtime_test FROM dual;

SELECT *
FROM NLS_DATABASE_PARAMETERS
WHERE PARAMETER = 'NLS_TIMESTAMP_TZ_FORMAT';

SELECT 
MC.CLIENT_MID                    AS        CLIENT_MID, 
--MC.LAST_UPDATED_AT                AS        LAST_UPDATED_AT, 
SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'GMT' AS GMT_SYSTIMESTAMP , 
SYSTIMESTAMP AT TIME ZONE 'NZ' AS NZST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'EST' AS EST_SYSTIMESTAMP ,
SYSTIMESTAMP AT TIME ZONE 'US/Eastern' AS EDT_SYSTIMESTAMP ,
CAST( SYSTIMESTAMP AS TIMESTAMP ),
FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) ,
CAST(
         FROM_TZ(
           CAST( SYSTIMESTAMP AS TIMESTAMP ),
           'Pacific/Auckland'
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
       
SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR AS GMT_OFFSET_SYSTIMESTAMP, 
TRUNC (SYSTIMESTAMP AT TIME ZONE 'GMT' + INTERVAL '12' HOUR, 'DD') AS GMT_OFFSET_DATE , 
MC.LANGUAGE_OID        AS        LANGUAGE_OID, 
MC.CURRENCY_OID        AS        CURRENCY_OID, 
MC.NAME                AS        NAME, 
MC.COUNTRY_OID        AS        COUNTRY_OID, 
MC.ACCOUNT_NO        AS        ACCOUNT_NO, 
MC.SHORT_NAME        AS        SHORT_NAME, 
MC.TAX_NO            AS        TAX_NO, 
MC.PROCESSING_DATE    AS        PROCESSING_DATE, 
MC.CURRENT_MONTH    AS        CURRENT_MONTH, 
MC.CURRENT_YEAR        AS        CURRENT_YEAR, 
MC.TIMEZONE_OID        AS        TIMEZONE_OID, 
MC.MONTH_END_1_ON    AS        MONTH_END_1_ON, 
MC.MONTH_END_2_ON    AS        MONTH_END_2_ON, 
MC.MONTH_END_3_ON     AS        MONTH_END_3_ON
FROM
M_CLIENTS MC
WHERE 
CLIENT_MID = 63
;



SELECT
SYSTIMESTAMP ,
MCUST.LAST_UPDATED_AT ,
TO_CHAR(MCUST.LAST_UPDATED_AT,'DD/MM/YYYY')        AS     MODIFIED_DATE, 
TO_CHAR(MCUST.LAST_UPDATED_AT,'HH24:MI:SS')        AS     MODIFIED_TIME ,
TO_CHAR(NEW_TIME(MCUST.LAST_UPDATED_AT, 'GMT', 'EST'), 'DD/MM/YYYY' ) AS MODIFIED_DATE_EST ,
--MCUST.LAST_UPDATED_AT AT TIME ZONE 'Pacific/Auckland' AS PF_AUCK_SYSTIMESTAMP ,
CAST( MCUST.LAST_UPDATED_AT  AS TIMESTAMP ),
 FROM_TZ(
            CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ) ,
           'UTC'
         ),
CAST(
         FROM_TZ(
           CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),
           'UTC' --
         ) AT TIME ZONE 'Pacific/Auckland'
         AS TIMESTAMP
       )
       AS Auckland_Time ,
	   TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'DD/MM/YYYY')
       AS Auckland_DATE ,
       
TO_CHAR(CAST(FROM_TZ(CAST( MCUST.LAST_UPDATED_AT AS TIMESTAMP ),'UTC' ) AT TIME ZONE 'Pacific/Auckland'AS TIMESTAMP ) , 'HH24:MI:SS')
       AS Auckland_TIME

FROM
IFCS_APAC_DEV_2.M_CUSTOMERS MCUST
WHERE
MCUST.CLIENT_MID=50
;

------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* Source qulaifier SQL */   


SELECT 
M.MERCHANT_NO								AS		MERCHANT_NO, 
LOCATION_RECONCILIATIONS.STATEMENT_NUMBER	AS		REFERENCE, 
TO_CHAR(C.PROCESSING_DATE, 'DD/MM/YYYY')	AS		ENTERED_ON,
MA.AP_ACCOUNT_CODE 							AS 		MERCHANT_AP_ACCOUNT_CODE,
TO_CHAR(GP.AMOUNT * -100)						AS		AMOUNT
FROM $$IFCS_SRC_SCHEMA.GENERATED_PAYMENTS GP 
INNER JOIN $$IFCS_SRC_SCHEMA.MERCHANT_AGREEMENTS MA 
ON MA.MERCHANT_AGREEMENT_OID = GP.MERCHANT_AGREEMENT_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.RELATIONSHIP_ASSIGNMENTS RA 
ON RA.RELATIONSHIP_ASSIGNMENT_OID = MA.RELATIONSHIP_ASSIGNMENT_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.RELATIONSHIPS R 
ON R.RELATIONSHIP_OID = RA.RELATIONSHIP_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.HIERARCHIES H 
ON H.HIERARCHY_OID = R.HIERARCHY_OID AND H.IS_MERCH_AGREEEMNT_AND_REPORTS = 'Y' 
INNER JOIN $$IFCS_SRC_SCHEMA.M_MERCHANTS M 
ON M.MERCHANT_MID = H.OWNING_MEMBER_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.M_LOCATIONS L 
ON L.LOCATION_MID = R.MEMBER_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.MERCHANT_PAYMENT_TYPES MPT 
ON MPT.MERCHANT_PAYMENT_TYPE_OID = MA.MERCHANT_PAYMENT_TYPE_OID 
INNER JOIN $$IFCS_SRC_SCHEMA.M_CLIENTS C 
ON C.CLIENT_MID=M.CLIENT_MID
INNER JOIN $$IFCS_SRC_SCHEMA.TRANSACTIONS             
ON TRANSACTIONS.TRANSACTION_OID = GP.TRANSACTION_OID
INNER JOIN $$IFCS_SRC_SCHEMA.LOCATION_RECONCILIATIONS 
ON LOCATION_RECONCILIATIONS.LOCATION_RECONCILIATION_OID = TRANSACTIONS.LOCATION_RECONCILIATION_OID
WHERE C.CLIENT_MID=$$CLIENT_MID
AND GP.ENTERED_ON=C.PROCESSING_DATE-$$PROCESSING_DAYS
ORDER BY GP.ENTERED_ON


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

