select rebprf as RebateProfileId
,rebcustmid as CustomerId
,rp1.description
,rp1.effective_on
,rp1.expires_on
,apt.description as ApplicationType
--,cp.description as CardProgram
,c1.description as ProfileCategory
,c2.description as ProfileType
from
rebate_profiles rp1 
left join card_programs cp On cp.card_program_oid=rp1.card_program_oid
left join application_types apt On apt.application_type_oid=rp1.application_Type_oid,
(
select rebate_profile_oid as rebprf, customer_mid as rebcustmid
from 
(
SELECT rp.rebate_profile_oid 
     , c.customer_mid
  FROM rebate_profiles rp , m_customers c
  where c.client_mid=50
  and rp.rebate_profile_oid in (select rebate_profile_oid from rebate_profiles
 where card_program_oid in ( select card_program_oid  from card_programs where client_mid=50)
 OR application_type_oid in ( select  at.application_type_oid  from card_programs cp, application_Types at  where client_mid=50  and cp.card_program_oid=at.card_program_oid)
 Or client_mid=50)
 MINUS 
SELECT rebate_profile_oid
   , customer_mid
  FROM rebate_out_options 
 where customer_mid is not null 
 union
 select rp.rebate_profile_oid, rp.customer_mid
 from rebate_profiles rp, m_customers mc
 where rp.customer_mid is not null
 and rp.customer_mid=mc.customer_mid
 and mc.client_mid=50)) , constants c1, constants c2 
where rp1.rebate_profile_oid=rebprf
and c1.constant_oid=rp1.profile_category_cid
and c2.constant_oid=rp1.profile_type_cid
and to_char(rp1.last_updated_at, 'YYYY-MM-DD HH24:MI:SS') > nvl(to_char(sysdate-10000 , 'YYYY-MM-DD HH24:MI:SS' ), '1900-01-01 00:00:00' )  