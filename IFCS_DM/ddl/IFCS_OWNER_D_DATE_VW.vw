DROP VIEW IFCS_OWNER.D_DATE_VW;

/* Formatted on 9/23/2014 4:33:18 PM (QP5 v5.267.14150.38599) */
CREATE OR REPLACE FORCE VIEW IFCS_OWNER.D_DATE_VW
(
   DATE_KEY,
   CALENDAR_DATE_DT,
   DATE_TEXT,
   ISO_DATE_TEXT,
   ISO_WEEK_TEXT,
   ISO_MONTH_TEXT,
   OFFSET_DAY_NUMBER,
   OFFSET_WEEK_NUMBER,
   OFFSET_POST_WEEK_NUMBER,
   OFFSET_MONTH_NUMBER,
   OFFSET_QUARTER_NUMBER,
   OFFSET_YEAR_NUMBER,
   SEQUENTIAL_DAY_NUMBER,
   SEQUENTIAL_WEEK_NUMBER,
   SEQUENTIAL_MONTH_NUMBER,
   SEQUENTIAL_QUARTER_NUMBER,
   DAY_IN_MONTH_NUMBER,
   DAY_IN_CALENDAR_YEAR_NUMBER,
   WEEK_IN_MONTH_NUMBER,
   WEEK_IN_CALENDAR_YEAR_NUMBER,
   MONTH_IN_CALENDAR_YEAR_NUMBER,
   DAY_OF_WEEK_NUMBER,
   DAY_OF_WEEK_NAME,
   DAY_OF_WEEK_ABBR,
   MONTH_NAME,
   MONTH_ABBR,
   MONTH_YEAR_NAME,
   MONTH_YEAR_ABBR,
   QUARTER_NAME,
   QUARTER_ABBR,
   QUARTER_NUMBER,
   QUARTER_YEAR_NAME,
   QUARTER_YEAR_ABBR,
   YEAR_NUMBER,
   YEAR_NAME,
   YEAR_NAME_ABBR,
   EFD_MONTH_YEAR_ABBR,
   FIRST_DAY_IN_MONTH_DATE_DT,
   LAST_DAY_IN_MONTH_DATE_DT,
   LAST_BUSINESS_DAY_IN_MONTH_DT,
   VALID_REAL_DATE_FLG,
   LAST_DAY_IN_MONTH_FLG,
   US_HOLIDAY_FLG,
   CANADA_HOLIDAY_FLG,
   WEEKDAY_FLG,
   WEEKEND_FLG,
   LAST_BUSINESS_DAY_IN_MONTH_FLG,
   HOLIDAY_NAME,
   EQUIVALENT_FUEL_DAY_FACTOR,
   SPEND_DAYS_FACTOR
)
AS
   SELECT                                                              -- keys
         DATE_KEY,
          CALENDAR_DATE_DT,
          DATE_TEXT,
          -- iso
          TO_CHAR (d.CALENDAR_DATE_DT, 'YYYY-MM-DD') iso_date_text,
          TO_CHAR (d.CALENDAR_DATE_DT + 1, 'YYYY-W') iso_week_text,
          TO_CHAR (d.CALENDAR_DATE_DT, 'YYYY-MM') iso_month_text,
          -- offsets
          CAST (d.CALENDAR_DATE_DT - TRUNC (SYSDATE) AS INTEGER)
             offset_day_number,
          FLOOR ( (d.CALENDAR_DATE_DT - TRUNC (SYSDATE, 'IW')) / 7)
             offset_week_number,
          FLOOR ( (d.CALENDAR_DATE_DT - TRUNC (SYSDATE, 'IW') - 1) / 7)
             offset_post_week_number,
          FLOOR (MONTHS_BETWEEN (d.CALENDAR_DATE_DT, TRUNC (SYSDATE, 'MM')))
             offset_month_number,
          FLOOR (
               MONTHS_BETWEEN (
                  d.CALENDAR_DATE_DT,
                  ADD_MONTHS (
                     TRUNC (SYSDATE, 'YEAR'),
                     CAST (TO_CHAR (SYSDATE, 'Q') AS INTEGER) * 3 - 3))
             / 3)
             offset_quarter_number,
          CAST (d.YEAR_NUMBER - EXTRACT (YEAR FROM SYSDATE) AS INTEGER)
             offset_year_number,
          -- seq. count
          d.SEQUENTIAL_DAY_NUMBER,
          d.SEQUENTIAL_WEEK_NUMBER,
          d.SEQUENTIAL_MONTH_NUMBER,
          d.SEQUENTIAL_QUARTER_NUMBER,
          -- number, name, and abbr
          DAY_IN_MONTH_NUMBER,
          DAY_IN_CALENDAR_YEAR_NUMBER,
          WEEK_IN_MONTH_NUMBER,
          WEEK_IN_CALENDAR_YEAR_NUMBER,
          MONTH_IN_CALENDAR_YEAR_NUMBER,
          DAY_OF_WEEK_NUMBER,
          DAY_OF_WEEK_NAME,
          DAY_OF_WEEK_ABBR,
          MONTH_NAME,
          MONTH_ABBR,
          MONTH_YEAR_NAME,
          MONTH_YEAR_ABBR,
          QUARTER_NAME,
          QUARTER_ABBR,
          QUARTER_NUMBER,
          QUARTER_YEAR_NAME,
          QUARTER_YEAR_ABBR,
          d.YEAR_NUMBER,
          YEAR_NAME,
          YEAR_NAME_ABBR,
          EFD_MONTH_YEAR_ABBR,
          -- track
          FIRST_DAY_IN_MONTH_DATE_DT,
          LAST_DAY_IN_MONTH_DATE_DT,
          LAST_BUSINESS_DAY_IN_MONTH_DT,
          -- flags
          VALID_REAL_DATE_FLG,
          LAST_DAY_IN_MONTH_FLG,
          US_HOLIDAY_FLG,
          CANADA_HOLIDAY_FLG,
          WEEKDAY_FLG,
          WEEKEND_FLG,
          LAST_BUSINESS_DAY_IN_MONTH_FLG,
          -- misc.
          HOLIDAY_NAME,
          EQUIVALENT_FUEL_DAY_FACTOR,
          SPEND_DAYS_FACTOR
     FROM d_date d;


GRANT SELECT ON IFCS_OWNER.D_DATE_VW TO IFCS_BORPTRNR;

GRANT SELECT ON IFCS_OWNER.D_DATE_VW TO DWLOADER ;

GRANT SELECT ON IFCS_OWNER.D_DATE_VW TO HUBLOADER ;



