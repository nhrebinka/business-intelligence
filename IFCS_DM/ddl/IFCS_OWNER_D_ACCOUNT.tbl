SET ECHO ON 
SET SERVEROUTPUT ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
SET LINESIZE 2000

/*
   NAME         :-   ifcs_owner_d_account.sql
   AUTHOUR      :-   Venugopal KAncharla
   DATE         :-   11/14/2015
   DECRIPTION   :-   SQL  To create IFCS  OWNER OBJECTS FOR CIC  
   LOG          : -  VERSION 1.0

  */


SPOOL /i1/&env/hub/logs/ifcs_owner_d_account.log ;


ALTER TABLE IFCS_OWNER.D_ACCOUNT
 DROP PRIMARY KEY CASCADE;

DROP TABLE IFCS_OWNER.D_ACCOUNT CASCADE CONSTRAINTS;

CREATE TABLE IFCS_OWNER.D_ACCOUNT
(
  ACCOUNT_KEY                    INTEGER        NOT NULL,
  SIC_LOOKUP_KEY                 INTEGER        NOT NULL,
  SOURCE_ACCOUNT_ID              VARCHAR2(30 BYTE) NOT NULL,
  ACCOUNT_ADDRESS_LINE_1         VARCHAR2(100 BYTE) NOT NULL,
  ACCOUNT_ADDRESS_LINE_2         VARCHAR2(100 BYTE) NOT NULL,
  ACCOUNT_ADDRESS_LINE_3         VARCHAR2(100 BYTE) NOT NULL,
  ACCOUNT_CITY                   VARCHAR2(50 BYTE) NOT NULL,
  ACCOUNT_STATE_PROV_CODE        CHAR(2 BYTE)   NOT NULL,
  ACCOUNT_POSTAL_CODE            VARCHAR2(10 BYTE) NOT NULL,
  ACCOUNT_MANAGER_NAME           VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_NAME                   VARCHAR2(200 BYTE) NOT NULL,
  BANK_IDENTIFICATION_NUMBER     VARCHAR2(30 BYTE) NOT NULL,
  ACCOUNT_OPEN_DATE              DATE           NOT NULL,
  ACCOUNT_REGION_CODE            VARCHAR2(4 BYTE) NOT NULL,
  ACCOUNT_REGION_NAME            VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_COUNTRY_NAME           VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_STATUS_DESC            VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_CLOSED_DATE            DATE           NOT NULL,
  FUNDING_TYPE_NAME              VARCHAR2(200 BYTE) NOT NULL,
  CURRENT_RECORD_FLG             VARCHAR2(4 BYTE) NOT NULL,
  FUNDER_NAME                    VARCHAR2(200 BYTE) NOT NULL,
  REBATE_FLG                     VARCHAR2(4 BYTE) NOT NULL,
  PURCHASE_ACCOUNT_FLG           VARCHAR2(4 BYTE) NOT NULL,
  BILLING_ACCOUNT_FLG            VARCHAR2(4 BYTE) NOT NULL,
  SIC_INDUSTRY_CODE              VARCHAR2(4 BYTE) NOT NULL,
  NAICS_INDUSTRY_CODE            VARCHAR2(50 BYTE) NOT NULL,
  ACCOUNT_DISTRICT_NAME          VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_DISTRICT_CODE          VARCHAR2(4 BYTE) NOT NULL,
  ACCOUNT_SUBDISTRICT_NAME       VARCHAR2(200 BYTE) NOT NULL,
  ACCOUNT_SUBDISTRICT_CODE       VARCHAR2(4 BYTE) NOT NULL,
  RELATED_BILLING_ACCOUNT_ID     VARCHAR2(30 BYTE) NOT NULL,
  REPORTING_NATIONAL_ACCOUNT_ID  VARCHAR2(30 BYTE) NOT NULL,
  NATIONAL_ACCOUNT_OPEN_DATE     DATE           NOT NULL,
  CUSTOMER_NAME                  VARCHAR2(200 BYTE) NOT NULL,
  RISK_GRADE                     INTEGER        NOT NULL,
  ATTRITION_TYPE_NAME            VARCHAR2(30 BYTE) NOT NULL,
  ATTRITION_REASON_CODE          VARCHAR2(4 BYTE) NOT NULL,
  ATTRITION_REASON_DESC          VARCHAR2(200 BYTE) NOT NULL,
  ROW_EFF_BEGIN_DTTM             DATE           NOT NULL,
  ROW_EFF_END_DTTM               DATE           NOT NULL,
  ROW_CREATE_DTTM                DATE           NOT NULL,
  ROW_LAST_MOD_DTTM              DATE           NOT NULL,
  ROW_LAST_MOD_PROC_NM           VARCHAR2(200 BYTE) NOT NULL,
  ROW_LAST_MOD_PROC_SEQ_NBR      INTEGER        NOT NULL,
  ROW_MD5_CHECKSUM_HEX_T1        VARCHAR2(50 BYTE) NOT NULL,
  ROW_MD5_CHECKSUM_HEX_T2        VARCHAR2(50 BYTE) NOT NULL,
  TAX_EXEMPT_FLG                 VARCHAR2(4 BYTE),
  SOURCE_SYSTEM_CODE             VARCHAR2(10 BYTE),
  CLM_CLUSTER_NAME               VARCHAR2(200 BYTE) DEFAULT 'N/A' NOT NULL,
  ACCOUNT_HIST_KEY               INTEGER,
  LAST_CLUSTERED_DATE            DATE,
  EFPS_TIME_INTERVAL             VARCHAR2(20 BYTE),
  EFPS_REQUIRED_FLG              VARCHAR2(4 BYTE),
  EFPS_FLG                       VARCHAR2(4 BYTE),
  ACQUISITION_CHANNEL            VARCHAR2(200 BYTE),
  COUPON_CODE                    VARCHAR2(20 BYTE),
  OPPORTUNITY_NUMBER             VARCHAR2(20 BYTE),
  SALES_REP_ID                   VARCHAR2(20 BYTE),
  PAYMENT_METHOD                 VARCHAR2(200 BYTE),
  PRODUCT_PURCHASE_RESTRICTION   VARCHAR2(200 BYTE),
  CREDIT_NATIONAL_ACCOUNT_ID     VARCHAR2(30 BYTE),
  REWARDS_FLG                    VARCHAR2(1 BYTE) NOT NULL,
  RECOURSE_CODE                  VARCHAR2(20 BYTE) NOT NULL,
  RECOURSE_NAME                  VARCHAR2(20 BYTE) NOT NULL,
  BUSINESS_UNIT                  VARCHAR2(20 BYTE) NOT NULL,
  BANKRUPTCY_FLG                 VARCHAR2(20 BYTE) NOT NULL,
  DEAD_LOSS_FLG                  VARCHAR2(20 BYTE) NOT NULL,
  BILLING_CYCLE_CODE             VARCHAR2(20 BYTE) NOT NULL,
  SETID                          VARCHAR2(20 BYTE) NOT NULL,
  BANKRUPTCY_FILING_TYPE_NAME    VARCHAR2(50 BYTE) NOT NULL,
  BANKRUPTCY_PROOF_OF_CLAIM_FLG  VARCHAR2(50 BYTE) NOT NULL,
  BANKRUPTCY_DISMISSAL_DATE      DATE           NOT NULL,
  ATTORNEY_RETAIN_DATE           DATE           NOT NULL,
  BANKRUPTCY_FILE_DATE           DATE           NOT NULL,
  RECEIVABLES_ANALYST_NAME       VARCHAR2(50 BYTE) NOT NULL,
  GOVERNMENT_FLG                 VARCHAR2(1 BYTE) NOT NULL,
  ACCOUNT_SINCE_DATE             DATE
)
NOCOMPRESS 
TABLESPACE IFCS_DEFAULT
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
PARTITION BY LIST (CURRENT_RECORD_FLG)
(  
  PARTITION D_ACCOUNT_CURRENT VALUES ('1')
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION D_ACCOUNT_HIST VALUES ('0')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
CACHE
PARALLEL ( DEGREE 4 INSTANCES 1 )
MONITORING
ENABLE ROW MOVEMENT;


CREATE BITMAP INDEX IFCS_OWNER.ACCOUNT_UI1 ON IFCS_OWNER.D_ACCOUNT
(SIC_LOOKUP_KEY)
  TABLESPACE IFCS_DEFAULT
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION D_ACCOUNT_HIST
    NOLOGGING
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION D_ACCOUNT_CURRENT
    NOLOGGING
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOPARALLEL;

CREATE INDEX IFCS_OWNER.ACCOUNT_UI2 ON IFCS_OWNER.D_ACCOUNT
(ACCOUNT_HIST_KEY, ACCOUNT_KEY)
  TABLESPACE IFCS_DEFAULT
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION D_ACCOUNT_HIST
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION D_ACCOUNT_CURRENT
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );

CREATE BITMAP INDEX IFCS_OWNER.ACCOUNT_UI3 ON IFCS_OWNER.D_ACCOUNT
(CURRENT_RECORD_FLG)
  TABLESPACE IFCS_DEFAULT
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION D_ACCOUNT_HIST
    NOLOGGING
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION D_ACCOUNT_CURRENT
    NOLOGGING
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOPARALLEL;

CREATE INDEX IFCS_OWNER.ACCOUNT_UI4 ON IFCS_OWNER.D_ACCOUNT
(FUNDING_TYPE_NAME)
  TABLESPACE IFCS_DEFAULT
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
NOLOGGING
LOCAL (  
  PARTITION D_ACCOUNT_HIST
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION D_ACCOUNT_CURRENT
    NOLOGGING
    NOCOMPRESS 
    TABLESPACE IFCS_DEFAULT
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          80K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );

CREATE UNIQUE INDEX IFCS_OWNER.NK_ACCOUNT_AK1 ON IFCS_OWNER.D_ACCOUNT
(SOURCE_ACCOUNT_ID, ROW_EFF_BEGIN_DTTM)
NOLOGGING
TABLESPACE IFCS_DEFAULT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

CREATE UNIQUE INDEX IFCS_OWNER.XPKD_ACCOUNT ON IFCS_OWNER.D_ACCOUNT
(ACCOUNT_KEY)
  TABLESPACE IFCS_DEFAULT
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
GLOBAL PARTITION BY HASH (ACCOUNT_KEY) 
PARTITIONS 8
STORE IN (IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT,IFCS_DEFAULT)
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );

CREATE OR REPLACE SYNONYM IFCS_BORPTRNR.D_ACCOUNT FOR IFCS_OWNER.D_ACCOUNT;


ALTER TABLE IFCS_OWNER.D_ACCOUNT ADD (
  CONSTRAINT XPKD_ACCOUNT
  PRIMARY KEY
  (ACCOUNT_KEY)
  USING INDEX IFCS_OWNER.XPKD_ACCOUNT
  ENABLE VALIDATE,
  CONSTRAINT NK_ACCOUNT_AK1
  UNIQUE (SOURCE_ACCOUNT_ID, ROW_EFF_BEGIN_DTTM)
  USING INDEX IFCS_OWNER.NK_ACCOUNT_AK1
  ENABLE VALIDATE);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON IFCS_OWNER.D_ACCOUNT TO DWLOADER;

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON IFCS_OWNER.D_ACCOUNT TO HUBLOADER;

GRANT SELECT ON IFCS_OWNER.D_ACCOUNT TO IFCS_BORPTRNR;

GRANT SELECT ON IFCS_OWNER.D_ACCOUNT TO IFCS_OWNER_SELECT ;

GRANT SELECT ON IFCS_OWNER.D_ACCOUNT TO IFCS_STAGE_OWNER_SELECT ;

SPOOL OFF ; 