DROP TABLE STAGE_OWNER.SRC_IFCS_HKG_COMMERCIAL_COND CASCADE CONSTRAINTS;

CREATE TABLE STAGE_OWNER.SRC_IFCS_HKG_COMMERCIAL_COND
(
  ISO_CODE                   NUMBER,
  CLIENT_NUMBER              NUMBER,
  CONDITION_TYPE             VARCHAR2(1 BYTE),
  EFFECTIVE_DATE             DATE,
  END_DATE                   DATE,
  PRODUCT_CODE               VARCHAR2(4 BYTE),
  PRODUCT_GROUP              VARCHAR2(2 BYTE),
  VEHICLE_TYPE               VARCHAR2(1 BYTE),
  THRESHOLD_TYPE             VARCHAR2(1 BYTE),
  DISCOUNT_TYPE              VARCHAR2(1 BYTE),
  THRESHOLD_VALUE1           NUMBER,
  DISCOUNT_VALUE1            NUMBER(6,4),
  THRESHOLD_VALUE2           NUMBER,
  DISCOUNT_VALUE2            NUMBER(6,4),
  THRESHOLD_VALUE3           NUMBER,
  DISCOUNT_VALUE3            NUMBER(6,4),
  BENEFICIARY_CLIENT_NUMBER  NUMBER,
  COMPLEX_BONUS_NUMBER       NUMBER
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY IFCS_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
            LOGFILE       ifcs_log_dir:'SRC_IFCS__HKG_COMMERCIAL_COND.log'
            BADFILE       ifcs_log_dir:'SRC_IFCS__HKG_COMMERCIAL_COND.bad'
            DISCARDFILE   ifcs_log_dir:'SRC_IFCS__HKG_COMMERCIAL_COND.dis'
            SKIP 2
            FIELDS LRTRIM
            MISSING FIELD VALUES ARE NULL
            REJECT ROWS WITH ALL NULL FIELDS
      ( ISO_CODE                       (1:6),
        CLIENT_NUMBER                  (8:14),
        CONDITION_TYPE                 (16:16),
        EFFECTIVE_DATE                 (18:27) CHAR  date_format DATE mask "DD.MM.YYYY" NULLIF EFFECTIVE_DATE = BLANKS,
        END_DATE                       (29:38) CHAR  date_format DATE mask "DD.MM.YYYY" NULLIF END_DATE = BLANKS,
        PRODUCT_CODE                   (40:43),
        PRODUCT_GROUP                  (45:46),
        VEHICLE_TYPE                   (48:48),
        THRESHOLD_TYPE                 (50:50),
        DISCOUNT_TYPE                  (52:52),
        THRESHOLD_VALUE1               (54:58),
        DISCOUNT_VALUE1                (60:65),
        THRESHOLD_VALUE2               (67:71),
        DISCOUNT_VALUE2                (73:78),
        THRESHOLD_VALUE3               (80:84),
        DISCOUNT_VALUE3                (86:91),
        BENEFICIARY_CLIENT_NUMBER   (93:99),
        COMPLEX_BONUS_NUMBER        (101:103)
      )
             )
     LOCATION (IFCS_DATA_DIR:'M21.PRD.ES344.D15159.T094230')
  )
REJECT LIMIT UNLIMITED
PARALLEL ( DEGREE 2 INSTANCES 1 )
NOMONITORING;


GRANT ALTER, SELECT ON STAGE_OWNER.SRC_IFCS_HKG_COMMERCIAL_COND TO STAGE_OWNER_SELECT;
