SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/CREATE_SYNONYMS.sql ;

CREATE OR REPLACE SYNONYM DWLOADER.SRC_IFCS_FILE_RECON FOR STAGE_OWNER.SRC_IFCS_FILE_RECON;

CREATE OR REPLACE SYNONYM DWLOADER.SRC_IFCS_SRC_FMS_FULL FOR STAGE_OWNER.SRC_IFCS_SRC_FMS_FULL_AUDIT;

CREATE OR REPLACE SYNONYM DWLOADER.SRC_IFCS_SRC_EPP_FULL FOR STAGE_OWNER.SRC_IFCS_SRC_EPP_FULL;

spool off;