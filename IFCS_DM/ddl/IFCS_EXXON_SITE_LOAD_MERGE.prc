CREATE OR REPLACE PROCEDURE IFCS_STAGE_OWNER.IFCS_EXXON_SITE_LOAD_MERGE
AS
/*
  Name : IFCS_EXXON_SITE_LOAD_MERGE
  Spec.: procedure to Merge Site table supplied by WES 

  Revision History:
  1.0         VK
  
  1.2  enabled  the Merge  VK 02/11/2015 ...  
 
  1.3   updated the Code Bug for the Manned And unmanned flag VK 02/12/2015 
  
  1.4  Updated the code to handle sites for non-ES also GS and VK 06/03/2015

*/

BEGIN
  
  MERGE INTO IFCS_STAGE_OWNER.SITE_INITIAL_LOAD IDP
        USING ( SELECT 
                    LTYPE                   , 
                    LOC_COMPA               , 
                    CTR                     , 
                    PBL                     , 
                    SITE_ID                 , 
                    BUSINESS_LOC_NAME       , 
                    DIFF                    , 
                    POSTL_CODE              , 
                    CITY                    , 
                    STREET                  , 
                    RG                      , 
                    STATUS                  , 
                    STATUS_TEXT             , 
                    SITE_TYPE               , 
                    EM_SITETABLE_PBL        , 
                    UNMANNED_SELFPIA        , 
                    FMS_UNIQUESITEID_OCT2014, 
                    UNATTENDED              , 
                    ATTENDED                , 
                    UNIQUE_SITEID1          , 
                    CTR_WES                 , 
                    CTRY_CODE               , 
                    UNIQUE_SITEID2          , 
                    UNMANNED
                FROM  IFCS_STAGE_OWNER.SRC_IFCS_EXXON_SITE_LIST_BIDW) STG
           ON (     IDP.unique_siteid1        = STG.unique_siteid1
                AND IDP.unique_siteid2        = STG.unique_siteid2 
                AND IDP.PBL                   = STG.PBL )
   WHEN MATCHED
   THEN
      UPDATE SET    IDP.LTYPE                    = STG.LTYPE                   , 
                    IDP.LOC_COMPA                = STG.LOC_COMPA               , 
                    IDP.CTR                      = STG.CTR                     ,  
                    IDP.SITE_ID                  = STG.SITE_ID                 , 
                    IDP.BUSINESS_LOC_NAME        = STG.BUSINESS_LOC_NAME       , 
                    IDP.DIFF                     = STG.DIFF                    , 
                    IDP.POSTL_CODE               = STG.POSTL_CODE              , 
                    IDP.CITY                     = STG.CITY                    , 
                    IDP.STREET                   = STG.STREET                  , 
                    IDP.RG                       = STG.RG                      , 
                    IDP.STATUS                   = STG.STATUS                  , 
                    IDP.STATUS_TEXT              = STG.STATUS_TEXT             , 
                    IDP.SITE_TYPE                = STG.SITE_TYPE               , 
                    IDP.EM_SITETABLE_PBL         = STG.EM_SITETABLE_PBL        , 
                    IDP.UNMANNED_SELFPIA         = STG.UNMANNED_SELFPIA        , 
                    IDP.FMS_UNIQUESITEID_OCT2014 = STG.FMS_UNIQUESITEID_OCT2014, 
                    IDP.UNATTENDED               = STG.UNATTENDED              , 
                    IDP.ATTENDED                 = STG.ATTENDED                , 
                    IDP.CTR_WES                  = STG.CTR_WES                 , 
                    IDP.CTRY_CODE                = STG.CTRY_CODE               , 
                    IDP.UNMANNED                 = STG.UNMANNED                , 
                    IDP.ROW_LAST_MOD_DTTM        = sysdate                     , 
                    IDP.ROW_LAST_MOD_PROC_NM     = 'IFCS_EXXON_SITE_LOAD_MERGE MERGE'              
   WHEN NOT MATCHED
   THEN
      INSERT       (IDP.LTYPE                   , 
                    IDP.LOC_COMPA               , 
                    IDP.CTR                     , 
                    IDP.PBL                     , 
                    IDP.SITE_ID                 , 
                    IDP.BUSINESS_LOC_NAME       , 
                    IDP.DIFF                    , 
                    IDP.POSTL_CODE              , 
                    IDP.CITY                    , 
                    IDP.STREET                  , 
                    IDP.RG                      , 
                    IDP.STATUS                  , 
                    IDP.STATUS_TEXT             , 
                    IDP.SITE_TYPE               , 
                    IDP.EM_SITETABLE_PBL        , 
                    IDP.UNMANNED_SELFPIA        , 
                    IDP.FMS_UNIQUESITEID_OCT2014, 
                    IDP.UNATTENDED              , 
                    IDP.ATTENDED                , 
                    IDP.UNIQUE_SITEID1          , 
                    IDP.CTR_WES                 , 
                    IDP.CTRY_CODE               , 
                    IDP.UNIQUE_SITEID2          , 
                    IDP.UNMANNED                , 
                    IDP.ROW_CREATE_DTTM         , 
                    IDP.ROW_LAST_MOD_DTTM      , 
                    IDP.ROW_LAST_MOD_PROC_NM )
          VALUES (  STG.LTYPE                   , 
                    STG.LOC_COMPA               , 
                    STG.CTR                     , 
                    STG.PBL                     , 
                    STG.SITE_ID                 , 
                    STG.BUSINESS_LOC_NAME       , 
                    STG.DIFF                    , 
                    STG.POSTL_CODE              , 
                    STG.CITY                    , 
                    STG.STREET                  , 
                    STG.RG                      , 
                    STG.STATUS                  , 
                    STG.STATUS_TEXT             , 
                    STG.SITE_TYPE               , 
                    STG.EM_SITETABLE_PBL        , 
                    STG.UNMANNED_SELFPIA        , 
                    STG.FMS_UNIQUESITEID_OCT2014, 
                    STG.UNATTENDED              , 
                    STG.ATTENDED                , 
                    STG.UNIQUE_SITEID1          , 
                    STG.CTR_WES                 , 
                    STG.CTRY_CODE               , 
                    STG.UNIQUE_SITEID2          , 
                    STG.UNMANNED                ,
                    SYSDATE , 
                    SYSDATE , 
                    'IFCS_EXXON_SITE_LOAD_MERGE INSERT' );

   COMMIT;
   
      
 MERGE INTO IFCS_OWNER.D_POS_AND_SITE  DPS 
 USING
  ( SELECT 
         CTRY_CODE      , 
         SITE_ID        ,  
         UNIQUE_SITEID2 , 
         UNMANNED       ,
         MAX(ROW_LAST_MOD_DTTM) ROW_LAST_MOD_DTTM 
   FROM IFCS_STAGE_OWNER.SITE_INITIAL_LOAD 
   WHERE Ctry_code||SITE_ID  NOT in ('250504690' , '0000000' , '250439030' , '250439199' )  
   GROUP BY 
         CTRY_CODE      , 
         SITE_ID        ,  
         UNIQUE_SITEID2 , 
         UNMANNED     
   ) S 
   ON 
    (     S.CTRY_CODE   = DPS.SOURCE_GROUP_MERCHANT_ID  -- Substr removed   for version 1.4 ..    
    and  S.SITE_ID    = DPS.SOURCE_MERCHANT_POS_ID 
    ) 
  WHEN MATCHED 
   THEN 
      UPDATE  SET  DPS.pRIVATE_SITE_FLG       = S.UNMANNED ,
                   DPS.ROW_LAST_MOD_DTTM      = sysdate    ,    
                   DPS.ROW_LAST_MOD_PROC_NM   =  'MERGE  from IFCS_STAGE_OWNER.IFCS_EXXON_SITE_LOAD_MERGE' ;   
-- WHERE 
--   SUBSTR (DPS.SOURCE_GROUP_MERCHANT_ID, 1, 2) = 'ES'  ;    -- Done per  code  V1.4 
    
 commit ; 
   
        
   
EXCEPTION
   WHEN OTHERS
   THEN
      RAISE;
END IFCS_EXXON_SITE_LOAD_MERGE;
/