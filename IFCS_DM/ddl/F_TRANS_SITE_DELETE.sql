SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/F_TRANS_SITE_ITEM_DELETE.log ; 

DELETE FROM IFCS_OWNER.F_TRANS_SITE_LINE_ITEM WHERE
(WEX_TRANSACTION_ID,ROW_CREATE_DTTM)IN (
SELECT WEX_TRANSACTION_ID,ROW_CREATE_DTTM FROM(
SELECT WEX_TRANSACTION_ID ,ROW_CREATE_DTTM,ROW_NUMBER() OVER (PARTITION BY 
WEX_TRANSACTION_ID ORDER BY ROW_CREATE_DTTM  ) AS rn
FROM IFCS_OWNER.F_TRANS_SITE_LINE_ITEM)
WHERE RN>1);

COMMIT;

--65.947 RECORDS WILL BE DELETED 