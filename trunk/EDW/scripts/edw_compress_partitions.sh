#!/usr/bin/ksh
#############################################################################################################
#
#  File Name           : edw_compress_partitions.sh
#  Purpose	           : TO Execute Compress EDW partitions PROC
# 
#  created  by         : Venugopal Kancharla for Bi-EDW
#                      : 10/10/2011 
#  Log                 : 
#                        
# 
#
###############################################################################################################
logger -t edw_compress_partitions.sh -p local1.notice "edw_compress_partitions.sh has started"
echo  "edw_compress_partitions.sh has started"  
#----------------------------------SET DATE-------------------------------------------------------------------- 
set -x 
tdate=`date +"%Y%m%d"`
date 

echo "Check SOURCE environment Settings Unix and Oracle"

if [ ! "$MNAME" ]
then
   export MNAME=`uname -n`;
 
   case $MNAME in
        pwm-wex-143-z1)
           export ORACLE_SID=pdedw
           export ENV=dev
           break;;
        pwm-wex-143)
           export ORACLE_SID=qtedw
           export ENV=stage
           break;;
        *)
           export ORACLE_SID=predw
           export ENV=prod 
   esac;
   
fi

#---------------------------SOURCE ENV PARAMETERS--------------------------------------------------------------


. /r41/$ENV/hub/script/hub_set_env.sh

echo "Sourcing complete."

#---------------------EXECUTE SQL COMMANDS THRU EXTERNALLY IDETIFIED ACCOUNTS----------------------------------

sqlplus -s / << EOF
spool $EDW_LOG/$tdate.edw_compress_partitions.log
set serveroutput on ; 
set time on ; 
set  timing on ; 
exec EDW_COMPRESS_PARTITIONS ;
spool off;
quit;
EOF

return_code=$?

ERRCNT=`egrep -c "ORA-" $EDW_LOG/$tdate.edw_compress_partitions.log`

#--------------------------------------------------------------------------------------------------------------
if [ $return_code = 0 ] && [ $ERRCNT = 0 ]   
then
    logger -t edw_compress_partitions.sh -p local1.notice "edw_compress_partitions.sh has ended successfully "
    echo  "edw_compress_partitions.sh has ended successfully" 
else 
    echo "###### ABORT ##### "
    echo  "edw_compress_partitions.sh has failed"
    logger -t edw_compress_partitions.sh -p local1.error "edw_compress_partitions.sh has failed"
fi

echo $return_code
echo $ERRCNT 

