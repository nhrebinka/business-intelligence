#------------------------------------------------------------------------------
# File Name:	hub_set_env.sh
# Purpose  :	Used to set up envirnoment variables for BI 
#   	     :      Authour Venu Kancharla  	
#                 11/04/2010 - 
#                 Log :-  added the shell for externally identified accounts  
#                     :-  Updated the app server info 
#                     :-  added the Oracle Environment SID variabled based on the app server  
#                 09/29/2011 -  Venu Kancharla 
#                 12/13/2012 -  Venu Kancharla  added the new servers for thyone.
#----------------------------------------------------------------------------
if [ ! "$MNAME" ]
then
   export MNAME=`uname -n`;
 
   case $MNAME in
        pwm-wex-143-z1)
           export ORACLE_SID=pdedw
           export ENV=dev
           break;;
        pwm-wex-550.wrightexpress.com)
           export ORACLE_SID=qtedw
           export ENV=stage
           break;;
        pwm-wex-551.wrightexpress.com)
           export ORACLE_SID=qtedw
           export ENV=stage
           break;;
        *)
           export ORACLE_SID=predw
           export ENV=prod 
   esac;
   
fi

#---------------------------DETERMINE SOURCE ENV----------------------------------

export  EDW_SCRIPT=/r41/$ENV/hub/script
export  EDW_CTL=/r41/$ENV/hub/ctl 
export  EDW_SQL=/r41/$ENV/hub/sql

export  EDW_LOG=/i1/$ENV/hub/logs   
export  EDW_DATA=/i1/$ENV/hub/data

#---------------------------DETERMINE SOURCE ENV----------------------------------

#----------------------------------------------------------------------------
#  Set ORACLE variables 
#----------------------------------------------------------------------------

export  ORACLE_HOME=/app/oracle/product/11.1
export  ORACLE_BASE=/app/oracle
export  TNS_ADMIN=/var/opt/oracle
export  PATH=$ORACLE_HOME/bin:/usr/local/bin:/bin:$PATH
export  LD_LIBRARY_PATH=$ORACLE_HOME/lib32

# this Function is used to get the application user credentials for .. hubloader externally identified account 
# created by Wright William Oracle DBA 

. /export/home/oracle/local/bin/ext_accounts.sh



echo "$MNAME"
echo "$ORACLE_HOME"
echo "$ORACLE_SID"
echo "End of hub_set_env.sh ..."