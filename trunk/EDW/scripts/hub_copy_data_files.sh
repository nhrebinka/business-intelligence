#!/usr/bin/ksh
#############################################################################################################
#
#  File Name           : hub_copy_data_files.sh
#  Purpose	           : to  archive Daily extract files  /r41/$env/hub/data
# 
#  created  by         : Venugopal Kancharla for Bi-EDW
#                      : 09/30/2010 
#  Log                 : Added the command to delete archive files older than 60   days            
#                        09/14/2011 by Venu Kancharla    
#                      : added The Env for PROD .. Since with the VIP the server name is no longer working ...   
#                        11/14/2012 By venu kancharla                            
#
###############################################################################################################
logger -t hub_copy_data_files.sh -p local1.notice "hub_copy_data_files.sh has started"
echo  "hub_copy_data_files.csh has started"  
#----------------------------------SET DATE------------------------------------- 
set -x 
tdate=`date +"%Y%m%d"`
date 

echo "Check SOURCE environment "
#---------------------------DETERMINE SOURCE ENV----------------------------------
if [ ! "$MNAME" ]
then
   export MNAME=`uname -n`;
 
   case $MNAME in
        pwm-wex-143-z1)
           export ENV=dev
           break;;
        pwm-wex-143)
           export ENV=stage
           break;;
         *)
           export ENV=prod
     esac;
fi

#---------------------------DETERMINE SOURCE ENV----------------------------------

export  EDW_SCRIPT=/r41/$ENV/hub/script
export  EDW_CTL=/r41/$ENV/hub/ctl 
export  EDW_SQL=/r41/$ENV/hub/sql

export  EDW_LOG=/i1/$ENV/hub/logs   
export  EDW_DATA=/i1/$ENV/hub/data

#----------------------------------SOURCE DONE---------------------------------- 
echo $MNAME                                    >  /$EDW_LOG/$tdate.hub_copy_data_files.log
echo $ENV                                      >> /$EDW_LOG/$tdate.hub_copy_data_files.log
echo $tdate 				           >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo "Drop the directory if it already exists" >> /$EDW_LOG/$tdate.hub_copy_data_files.log
rm -r $EDW_DATA/$tdate                         >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo  "creating backup directory "             >> /$EDW_LOG/$tdate.hub_copy_data_files.log
mkdir $EDW_DATA/$tdate                         >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo  "copying files to  backup directory "    >> /$EDW_LOG/$tdate.hub_copy_data_files.log
cp  $EDW_DATA/*.dat $EDW_DATA/$tdate           >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo "zipping files in backup directory "      >> /$EDW_LOG/$tdate.hub_copy_data_files.log
gzip $EDW_DATA/$tdate/*.dat                    >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo "backing up files done "                  >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo "deleting files older than 60  days"      >> /$EDW_LOG/$tdate.hub_copy_data_files.log

find  $EDW_DATA -mtime +60 -type d -name "20*" -exec rm -rf {} \;   >> /$EDW_LOG/$tdate.hub_copy_data_files.log

echo "deleting files done"                     >> /$EDW_LOG/$tdate.hub_copy_data_files.log

if [ $? -ne 0 ] 
then
    echo "###### ABORT ##### "
    echo  "hub_copy_data_files.sh has failed"
    logger -t hub_copy_data_files -p local1.error "hub_copy_data_files.sh has failed"
else 
    echo  "hub_copy_data_files.sh has ended successfully" 
    logger -t hub_copy_data_files.sh -p local1.notice "hub_copy_data_files.sh has ended successfully "
fi
echo "exit"
