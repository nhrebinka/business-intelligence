--
-- create the daily aggr. table for 
--    purchases per site/merchant/program per revenue month
-- 
-- note:
--  it takes about 15 min on PDEDW with 12 threads (default)
--
--
-- column name abbriviation rule used: 
--   1. amount => amt
--   2. fees   => fee
--   3. maintenance => maint
--   4. transaction => trans
--   5. interchange => intchg
--
-- todo:
--   1. use an updatable join view with parallel UPDATE to refresh the updated rows, 
--   2. use an anti-hash join with parallel INSERT to refresh the new rows.
--
CREATE OR REPLACE VIEW f_monthly_site_snapshot_vw
AS
SELECT /*+ full(tx) parallel(x,64) */
  MAX(dt.DATE_KEY)                       revenue_date_key,
  dt.MONTH_YEAR_ABBR                     iso_year_month,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
--
  tx.NAICS_KEY,
  tx.SIC_KEY,
  tx.SOURCE_SYSTEM_NAME,
--
  SUM(tx.TRANS_TICKET_COUNT)             trans_ticket_count,
  SUM(tx.NBR_OF_UNITS_PURCHASED_COUNT)   nbr_of_units_purchased_count,
  SUM(tx.TRANS_LINE_ITEM_COUNT)          trans_line_item_count,
--
  SUM(tx.NET_REVENUE_AMT)                net_revenue_amt,	
  SUM(tx.GROSS_SPEND_AMT)                gross_spend_amt,	
  SUM(tx.GROSS_SPEND_LOCAL_AMT)          gross_spend_local_amt,	
  SUM(tx.GROSS_REVENUE_AMT)              gross_revenue_amt,	
  SUM(tx.GROSS_NON_REVENUE_AMT)          gross_non_revenue_amt,	
  SUM(tx.DISCOUNT_AMT)                   discount_amt,
  SUM(tx.PURCHASE_ITEM_AMT)              purchase_item_amt,
  SUM(tx.PURCHASE_ITEM_LOCAL_AMT)        purchase_item_local_amt,
  SUM(tx.FEE_CURRENCY_CONVERSION_AMT)    fee_currency_conversion_amt,	
-- fuel spend
  SUM(tx.SPEND_FUEL_ONLY_AMT)            spend_fuel_only_amt,
  SUM(tx.SPEND_FUEL_DIESEL_AMT)          spend_fuel_diesel_amt,
  SUM(tx.SPEND_FUEL_GAS_AMT)             spend_fuel_gas_amt,
  SUM(tx.SPEND_FUEL_OTHER_AMT)           spend_fuel_other_amt,
  SUM(tx.SPEND_MAINT_AMT)                spend_maint_amt,
  SUM(tx.SPEND_OTHER_AMT)                spend_other_amt,
-- fuel spend local
  SUM(tx.SPEND_FUEL_ONLY_LOCAL_AMT)      spend_fuel_only_local_amt,
  SUM(tx.SPEND_FUEL_DIESEL_LOCAL_AMT)    spend_fuel_diesel_local_amt,
  SUM(tx.SPEND_FUEL_GAS_LOCAL_AMT)       spend_fuel_gas_local_amt,
  SUM(tx.SPEND_FUEL_OTHER_LOCAL_AMT)     spend_fuel_other_local_amt,
  SUM(tx.SPEND_MAINT_LOCAL_AMT)          spend_maint_local_amt,
  SUM(tx.SPEND_OTHER_LOCAL_AMT)          spend_other_local_amt,
-- interchange
  SUM(tx.INTCHG_TOTAL_AMT)               intchg_total_amt,
  SUM(tx.INTCHG_FUEL_ONLY_AMT)           intchg_fuel_only_amt,
  SUM(tx.INTCHG_FUEL_DIESEL_AMT)         intchg_fuel_diesel_amt,
  SUM(tx.INTCHG_FUEL_GAS_AMT)            intchg_fuel_gas_amt,
  SUM(tx.INTCHG_FUEL_OTHER_AMT)          intchg_fuel_other_amt,
  SUM(tx.INTCHG_MAINT_ONLY_AMT)          intchg_maint_only_amt,
  SUM(tx.INTCHG_ALL_OTHER_AMT)           intchg_all_other_amt,
--
  SUM(tx.INTCHG_RATE_ACTUAL_AMT)         intchg_rate_actual_amt,
  SUM(tx.INTCHG_LINE_ITEM_FLAT_FEE_AMT)  intchg_line_item_flat_fee_amt,
  SUM(tx.INTCHG_TRANS_FLAT_FEE_AMT)      intchg_trans_flat_fee_amt,
  SUM(tx.INTCHG_PER_UNIT_FEE_AMT)        intchg_per_unit_fee_amt,
-- tax total
  SUM(tx.TAX_TOTAL_AMT)                  tax_total_amt,
  SUM(tx.TAX_FEDERAL_AMT)                tax_federal_amt,
  SUM(tx.TAX_NON_FEDERAL_AMT)            tax_non_federal_amt,
  SUM(tx.TAX_STATE_AMT)                  tax_state_amt,
  SUM(tx.TAX_LOCAL_AMT)                  tax_local_amt,
  SUM(tx.TAX_TOTAL_LOCAL_AMT)            tax_total_local_amt,
-- tax sales
  SUM(tx.TAX_SALES_STATE_AMT)            tax_sales_state_amt,
  SUM(tx.TAX_SALES_COUNTY_AMT)           tax_sales_county_amt,
  SUM(tx.TAX_SALES_CITY_AMT)             tax_sales_city_amt,
-- tax excise
  SUM(tx.TAX_EXCISE_FEDERAL_AMT)         tax_excise_federal_amt,
  SUM(tx.TAX_EXCISE_STATE_AMT)           tax_excise_state_amt,
  SUM(tx.TAX_EXCISE_COUNTY_AMT)          tax_excise_county_amt,
  SUM(tx.TAX_EXCISE_CITY_AMT)            tax_excise_city_amt,
-- tax special
  SUM(tx.TAX_SPECIAL_STATE_AMT)          tax_special_state_amt,
  SUM(tx.TAX_SPECIAL_COUNTY_AMT)         tax_special_county_amt,
  SUM(tx.TAX_SPECIAL_CITY_AMT)           tax_special_city_amt,
-- exempt
  SUM(tx.TAX_EXEMPT_SPEND_AMT)           tax_exempt_spend_amt,
  SUM(tx.TAX_EXEMPT_SPEND_LOCAL_AMT)     tax_exempt_spend_local_amt,
-- qualtity gallons
  SUM(tx.PURCHASE_GALLONS_QTY)           purchase_gallons_qty,
  SUM(tx.FUEL_GAS_GALLONS_QTY)           fuel_gas_gallons_qty,
  SUM(tx.FUEL_DIESEL_GALLONS_QTY)        fuel_diesel_gallons_qty,
  SUM(tx.FUEL_OTHER_GALLONS_QTY)         fuel_other_gallons_qty,
  SUM(tx.PRIVATE_SITE_GALLONS_QTY)       private_site_gallons_qty,
-- qualtity liters
  SUM(tx.PURCHASE_LITRES_QTY)            purchase_litres_qty,
  SUM(tx.FUEL_GAS_LITRES_QTY)            fuel_gas_litres_qty,
  SUM(tx.FUEL_DIESEL_LITRES_QTY)         fuel_diesel_litres_qty,
  SUM(tx.FUEL_OTHER_LITRES_QTY)          fuel_other_litres_qty,
  SUM(tx.PRIVATE_SITE_LITRES_QTY)        private_site_litres_qty,
  SUM(tx.MANUAL_REBATE_AMT)              manual_rebate_amt,
  SUM(tx.TRUCK_STOP_FEE_AMT)             truck_stop_fee_amt,
  SUM(tx.EV_TRANSACTION_FEE_AMT)         EV_TRANSACTION_FEE_AMT,
  SUM(tx.SPEND_FUEL_KWH_AMT)             SPEND_FUEL_KWH_AMT,
  SUM(tx.INTCHG_FUEL_KWH_AMT)            INTCHG_FUEL_KWH_AMT,
  SUM(tx.FUEL_KWH_QTY)                   FUEL_KWH_QTY
FROM F_DAILY_SITE_SNAPSHOT_VW tx
  JOIN D_DATE dt ON (tx.REVENUE_DATE_KEY = dt.DATE_KEY)
GROUP BY
  dt.MONTH_YEAR_ABBR,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
  tx.NAICS_KEY,
  tx.SIC_KEY,
  tx.SOURCE_SYSTEM_NAME
;

GRANT SELECT ON EDW_OWNER.F_MONTHLY_SITE_SNAPSHOT_VW TO EDW_OWNER_SELECT;
GRANT SELECT ON EDW_OWNER.F_MONTHLY_SITE_SNAPSHOT_VW TO BORPTRNR;
GRANT SELECT ON EDW_OWNER.F_MONTHLY_SITE_SNAPSHOT_VW TO USER_LDAP;

CREATE OR REPLACE PUBLIC SYNONYM F_MONTHLY_SITE_SNAPSHOT_VW 
FOR EDW_OWNER.F_MONTHLY_SITE_SNAPSHOT_VW;

