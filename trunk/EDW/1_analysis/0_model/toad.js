function Main() {
    var app   = System.GetInterface("Application");
    var log   = System.CreateObject("Log");
    var model = app.ActiveModel;
    var ws    = app.ActiveWorkSpace;        
    
    if (model == null || ws == null) return;
        
    var dp = System.CreateObject('DialogParams');
    dp.ScriptName  = 'WexL2P';
    dp.Caption     = 'WexL2P Info';
    dp.DialogIndex = 208;  // Unique number, must be above 200
    dp.Msg         = 'Allows you to quickly copy existing attribute to other entities. '; 
    dp.Buttons     = 3;
    dp.DlgType     = 2;
    
    if(System.ShowMessageDialogScript(dp) != 6) { return; }
    
    var ar  = [];
    var cnt = 0; 
    
    model.Lock();
    for (var i=0; i<This.Count; i++) {
        var o = This.GetObject(i);
        for (var j=0; j<model.Entities.Count; j++) {
            var e = model.Entities.GetObject(j);
            ar.push(e.Name+':'+e.Category.Name+' < '+get_rel(e));
            is_dim(e) && ar.push(dim_fix(e));
        }
    }
    model.Unlock();
    System.RefreshModel(model);                               
    var msg = ar.join("\n")+' finished.           ';
    log.Information(msg);
    System.ShowMessageDialog(1004,'InfoDialog',msg,2,4);
}

function get_rel(e) {
    //check = "AK.Relations" (AK -> PFK -> attr)
    var ar = new Array();
    for (var i=0; i<e.Keys.Count; i++) {
        var ky = e.Keys.GetObject(i);
        for (var j=0; j<ky.KeyItems.Count; j++) {
              var im = ky.KeyItems.GetObject(j);
              ar.push('^'+im.Name+':'+im.ObjectType.toString());
        }
    }
    return ar.join(',');  
}

function is_dim(e) {
    return (e.Name.indexOf('Internal') < 0) && (
           (e.Category.Name == 'Dimension1') ||
           (e.Category.Name == 'Dimension2')
    );
}

function dim_map(n) {
    var n1 = n;
    n=='Eff Begin'  && (n1='EFF_BEGIN_DTTM');
    n=='Eff End'    && (n1='EFF_END_DTTM');
    n=='Difinition' && (n1='DEF');
    return n1.toUpperCase().split(' ').join('_');
}

function dim_fix(e) {
    var en = dim_map(e.Name);
    var ar = [ 'D_' + en + "::" ];
    for (var i=0; i<e.Attributes.Count; i++) {
        var a = e.Attributes.GetObject(i);
        var n = en + '_' + dim_map(a.Name);
//        a.Name = n;                   
        ar.push("    "+n);
    }
//    e.Name = 'D_' + en;
    return ar.join(",\n");
}

function has_attr(e, f) {
    for (var i=0; i<e.Attributes.Count; i++) {
        var a = e.Attributes.GetObject(i);
        if (f == a.Name) return true;
    }
    return false;
}

function add_attr(o, e) {
    var na = e.CreateNewObject(2003); // add new attribute

    if(o.AfterScript != null) {
        na.AfterScript = o.AfterScript;
    }                    
    if(o.BeforeScript != null) {
        na.BeforeScript = o.BeforeScript;
    }
    na.Caption  = o.Caption;
    na.Comments = o.Comments;                   

    if(o.DataType != null) {
        var ot = o.DataType;    
        na.SetLinkedObject("DataType", ot); 
    }
    if(o.Domain != null) {
        var od = o.Domain;  
        na.SetLinkedObject("Domain", od);
    }
    na.DataTypeParam1 = o.DataTypeParam1;
    na.DataTypeParam2 = o.DataTypeParam2;
    na.DefaultValue   = o.DefaultValue;                      
    na.Name    = o.Name;
    na.Notes   = o.Notes;
    na.NotNull = o.NotNull;
}



