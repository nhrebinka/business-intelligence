  SELECT 
    pwcd.BUSINESS_DATE,
    pia.CUST_ID,
    sum(pia.ENTRY_AMT)
  FROM PS_ITEM_ACTIVITY PIA
  JOIN PS_WX_CUST_DAILY pwcd ON (1=1
    AND pwcd.BUSINESS_DATE BETWEEN TO_DATE('07/23/2014', 'MM/DD/YYYY')
                               AND TO_DATE('07/23/2014', 'MM/DD/YYYY')
    AND pia.CUST_ID       = pwcd.CUST_ID
    AND pia.BUSINESS_UNIT = pwcd.BUSINESS_UNIT
    AND pia.POST_DT      <= pwcd.BUSINESS_DATE
  )
  JOIN (
    SELECT /*+ parallel(PS_CUST_CREDIT , 4) */
      CUST_ID, 
      AGING_ID
    FROM PS_CUST_CREDIT
    WHERE (CUST_ID, EFFDT) IN (
      SELECT 
        CUST_ID, 
        MAX(EFFDT) EFFDT
      FROM PS_CUST_CREDIT
      WHERE AGING_ID != 'STD'
      GROUP BY CUST_ID
    )
  ) MCC ON (pia.CUST_ID  = mcc.CUST_ID)
  GROUP BY pwcd.BUSINESS_DATE, PIA.CUST_ID
;
