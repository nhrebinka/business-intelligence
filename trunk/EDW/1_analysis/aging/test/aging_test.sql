SELECT 
  o.BUSINESS_DATE,
  o.CUST_ID,
  MAX(o.AR_AGE_OLD_DAY),
  MAX(o.REG_AGE_OLD_DAY),
  SUM(o.TOTAL)
FROM (  
  SELECT /*+ PARALLEL(PI, 4) FULL(PI) INDEX_FFS(PIA, PSCITEM_ACTIVITY) */
    pwcd.BUSINESS_DATE,
    pia.CUST_ID,
    pia.ITEM,
    SUM(pia.ENTRY_AMT) TOTAL,
    MIN(pi.ACCOUNTING_DT),
    MIN(pi.DUE_DT),
    (pwcd.BUSINESS_DATE - MIN(pi.ACCOUNTING_DT)) AR_AGE_OLD_DAY,
    (pwcd.BUSINESS_DATE - MIN(pi.DUE_DT))        REG_AGE_OLD_DAY
  FROM 
    PS_ITEM_ACTIVITY partition (ym_2014_06) pia, 
    PS_ITEM          partition (ym_2014_06) pi, 
    PS_WX_CUST_DAILY pwcd
  WHERE pwcd.BUSINESS_DATE BETWEEN TO_DATE('06/30/2014','MM/DD/YYYY')
                               AND TO_DATE('06/30/2014','MM/DD/YYYY')
    AND pia.CUST_ID       = pwcd.CUST_ID
    AND pia.BUSINESS_UNIT = pwcd.BUSINESS_UNIT
    AND pia.POST_DT      <= pwcd.BUSINESS_DATE
    -- pia=pi
    AND pia.ITEM          = pi.ITEM
    AND pia.CUST_ID       = pi.CUST_ID
    AND pia.BUSINESS_UNIT = pi.BUSINESS_UNIT
    AND pia.POST_DT       = pi.POST_DT
    -- to reduce output
    AND pia.CUST_ID in (
     '0000211093',
     '0401004003125',
     '9100000110835',
     '9100000111159',
     '9100000112275'
    )
  HAVING SUM(pia.ENTRY_AMT) <> 0
  GROUP BY pwcd.BUSINESS_DATE, pia.CUST_ID, pia.ITEM
) o
GROUP BY o.BUSINESS_DATE, o.CUST_ID
