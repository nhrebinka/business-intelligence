/* Formatted on 3/7/2014 3:51:11 PM (QP5 v5.215.12089.38647) */
  SELECT D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         SUM (F_TRANSACTION_LINE_ITEM.PURCHASE_GALLONS_QTY),
         D_ACCOUNT_CURRENT_VW.SIC_INDUSTRY_CODE,
         NVL (
            CASE
               WHEN INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ') = 0
               THEN
                  S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION
               ELSE
                  SUBSTR (
                     S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION,
                     INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ') + 2)
            END,
            'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_II_DESCRIPTION, 'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_III_DESCRIPTION, 'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_IV_DESCRIPTION, 'N/A'),
         D_SITE_REGION_VW.SITE_REGION_NAME,
         D_POSTING_DATE_VW.POST_CALENDAR_DATE,
         D_PROGRAM.PRODUCT_NAME,
         D_PROGRAM.PROGRAM_NAME,
         D_PROGRAM.PROGRAM_PACKAGE_NAME,
         D_PROGRAM.PROGRAM_SPONSOR_NAME
    FROM D_POSTING_DATE_VW
         INNER JOIN F_DATE_EVENT
            ON (F_DATE_EVENT.POSTING_DATE_KEY = D_POSTING_DATE_VW.POST_DATE_KEY)
         INNER JOIN D_REVENUE_DATE_VW
            ON (F_DATE_EVENT.REVENUE_DATE_KEY = D_REVENUE_DATE_VW.REV_DATE_KEY)
         INNER JOIN F_TRANSACTION_LINE_ITEM
            ON (F_TRANSACTION_LINE_ITEM.REVENUE_DATE_KEY =
                   D_REVENUE_DATE_VW.REV_DATE_KEY)
         INNER JOIN D_PROGRAM
            ON (F_TRANSACTION_LINE_ITEM.PROGRAM_KEY = D_PROGRAM.PROGRAM_KEY)
         INNER JOIN D_SITE_REGION_VW
            ON (F_TRANSACTION_LINE_ITEM.SITE_REGION_KEY =
                   D_SITE_REGION_VW.SITE_REGION_KEY)
         INNER JOIN D_ACCOUNT_HIST_VW
            ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY =
                   F_TRANSACTION_LINE_ITEM.PURCHASE_ACCOUNT_KEY)
         INNER JOIN D_ACCOUNT_CURRENT_VW
            ON (D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY =
                   D_ACCOUNT_CURRENT_VW.ACCOUNT_HIST_KEY)
         INNER JOIN S_SIC_LOOKUP
            ON (S_SIC_LOOKUP.SIC_LOOKUP_KEY =
                   D_ACCOUNT_CURRENT_VW.SIC_LOOKUP_KEY)
   WHERE (    D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID =
                 ANY (  SELECT D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID
                          FROM D_POSTING_DATE_VW
                               INNER JOIN F_DATE_EVENT
                                  ON (F_DATE_EVENT.POSTING_DATE_KEY =
                                         D_POSTING_DATE_VW.POST_DATE_KEY)
                               INNER JOIN D_REVENUE_DATE_VW
                                  ON (F_DATE_EVENT.REVENUE_DATE_KEY =
                                         D_REVENUE_DATE_VW.REV_DATE_KEY)
                               INNER JOIN F_TRANSACTION_LINE_ITEM
                                  ON (F_TRANSACTION_LINE_ITEM.REVENUE_DATE_KEY =
                                         D_REVENUE_DATE_VW.REV_DATE_KEY)
                               INNER JOIN D_PROGRAM
                                  ON (F_TRANSACTION_LINE_ITEM.PROGRAM_KEY =
                                         D_PROGRAM.PROGRAM_KEY)
                               INNER JOIN D_SITE_REGION_VW
                                  ON (F_TRANSACTION_LINE_ITEM.SITE_REGION_KEY =
                                         D_SITE_REGION_VW.SITE_REGION_KEY)
                               INNER JOIN D_ACCOUNT_HIST_VW
                                  ON (D_ACCOUNT_HIST_VW.ACCOUNT_KEY =
                                         F_TRANSACTION_LINE_ITEM.PURCHASE_ACCOUNT_KEY)
                               INNER JOIN D_ACCOUNT_CURRENT_VW
                                  ON (D_ACCOUNT_HIST_VW.ACCOUNT_HIST_KEY =
                                         D_ACCOUNT_CURRENT_VW.ACCOUNT_HIST_KEY)
                         WHERE (    D_PROGRAM.PROGRAM_PACKAGE_NAME NOT IN
                                       ('IMP Small Business Private Label Unfunded',
                                        'IMP TP Plus Fleet Private Label Unfunded',
                                        'IMP Tiger Pro Fleet Private Label Unfunded',
                                        'INVALID',
                                        'Imperial Oil Private Label Unfunded',
                                        'N/A')
                                AND D_ACCOUNT_CURRENT_VW.ACCOUNT_OPEN_DATE <=
                                       '31-03-2012 00:00:00'
                                AND D_POSTING_DATE_VW.POST_CALENDAR_DATE BETWEEN '02-01-2014 00:00:00'
                                                                             AND '03-03-2014 00:00:00'
                                AND D_ACCOUNT_CURRENT_VW.ACCOUNT_STATUS_DESC =
                                       'Active'
                                AND D_SITE_REGION_VW.SITE_REGION_NAME IN
                                       ('Midwest',
                                        'Northeast',
                                        'SouthWest',
                                        'Southeast',
                                        'West'))
                      GROUP BY D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID
                        HAVING SUM (
                                  F_TRANSACTION_LINE_ITEM.PURCHASE_GALLONS_QTY) >
                                  0)
          AND D_POSTING_DATE_VW.POST_CALENDAR_DATE BETWEEN '02-01-2013 00:00:00'
                                                       AND '01-03-2013 00:00:00'
          AND D_SITE_REGION_VW.SITE_REGION_NAME IN
                 ('Midwest', 'Northeast', 'SouthWest', 'Southeast', 'West'))
GROUP BY D_ACCOUNT_CURRENT_VW.SOURCE_ACCOUNT_ID,
         D_ACCOUNT_CURRENT_VW.SIC_INDUSTRY_CODE,
         NVL (
            CASE
               WHEN INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ') = 0
               THEN
                  S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION
               ELSE
                  SUBSTR (
                     S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION,
                     INSTR (S_SIC_LOOKUP.SIC_LEVEL_I_DESCRIPTION, '. ') + 2)
            END,
            'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_II_DESCRIPTION, 'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_III_DESCRIPTION, 'N/A'),
         NVL (S_SIC_LOOKUP.SIC_LEVEL_IV_DESCRIPTION, 'N/A'),
         D_SITE_REGION_VW.SITE_REGION_NAME,
         D_POSTING_DATE_VW.POST_CALENDAR_DATE,
         D_PROGRAM.PRODUCT_NAME,
         D_PROGRAM.PROGRAM_NAME,
         D_PROGRAM.PROGRAM_PACKAGE_NAME,
         D_PROGRAM.PROGRAM_SPONSOR_NAME;
