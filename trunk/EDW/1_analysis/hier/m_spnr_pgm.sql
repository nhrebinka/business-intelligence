insert into x_hier
select
  cast('SPNR_PGM' as varchar2(30)) hname,
  level                            hlevel,
  connect_by_isleaf                isleaf,
  edm_par_pgm_key                  pkey, 
  edm_spnr_pgm_key                 ckey, 
  cast(spnr_nm as varchar2(100))   pid,
  cast(pgm_nm  as varchar2(100))   cid,
  sys_connect_by_path(pgm_nm, '~') path
from m_spnr_pgm
start with edm_spnr_pgm_key = edm_par_pgm_key
connect by nocycle prior edm_spnr_pgm_key = edm_par_pgm_key
order by level, edm_par_pgm_key
;
