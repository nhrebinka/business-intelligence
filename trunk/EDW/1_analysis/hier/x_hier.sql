DROP TABLE X_HIER CASCADE CONSTRAINTS;

CREATE TABLE X_HIER
(
  HNAME   VARCHAR2(30 BYTE),
  HLEVEL  NUMBER,
  ISLEAF  NUMBER,
  PKEY    NUMBER(38),
  CKEY    NUMBER(38),
  PID     VARCHAR2(100 BYTE),
  CID     VARCHAR2(100 BYTE),
  PATH    VARCHAR2(4000 BYTE)
)
TABLESPACE D_BI
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
