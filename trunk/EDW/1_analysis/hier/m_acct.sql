insert into x_hier
select
  cast('ACCT_HIER' as varchar2(30)) as hname,
  level                             as hlevel,
  connect_by_isleaf                 as isleaf,
  hier_par_edm_acct_key             as pkey, 
  edm_acct_key                      as ckey, 
  cast(hier_level_nbr as varchar2(100))  as pid,
  cast(wex_acct_nbr   as varchar2(100))  as cid,
  sys_connect_by_path(wex_acct_nbr, '~') as path
from m_acct
start with hier_level_nbr = 1
connect by nocycle prior edm_acct_key = hier_par_edm_acct_key
union all
select
  cast('ACCT_FLA' as varchar2(30)) as hname,
  level                            as hlevel,
  connect_by_isleaf                as isleaf,
  fla_par_edm_acct_key             as pkey, 
  edm_acct_key                     as ckey, 
  cast(fla_par_spnr_acct_nbr as varchar2(100)) as pid,
  cast(wex_acct_nbr          as varchar2(100)) as cid,
  sys_connect_by_path(wex_acct_nbr, '~')       as path
from m_acct
start with edm_acct_key = fla_par_edm_acct_key
connect by nocycle prior edm_acct_key = fla_par_edm_acct_key
union all
select
  cast('ACCT_ULT' as varchar2(30)) as hname,
  level                            as hlevel,
  connect_by_isleaf                as isleaf,
  ult_par_edm_acct_key             as pkey, 
  edm_acct_key                     as ckey, 
  cast(ult_par_spnr_acct_nbr as varchar2(100)) as pid,
  cast(wex_acct_nbr          as varchar2(100)) as cid,
  sys_connect_by_path(wex_acct_nbr, '~')       as path
from m_acct
start with edm_acct_key = ult_par_edm_acct_key
connect by nocycle prior edm_acct_key = ult_par_edm_acct_key
;
