insert into x_hier
select
  cast('STKHLDR' as varchar2(30))  hname,
  level                            hlevel,
  connect_by_isleaf                isleaf,
  parent_edm_stkhldr_key           pkey, 
  edm_stkhldr_key                  ckey, 
  cast(stkhldr_lvl_nm as varchar2(100)) pid,
  cast(stkhldr_nm as varchar2(100))     cid,
  sys_connect_by_path(stkhldr_nm, '~') path
from m_spnr_pgm
start with edm_stkhldr_key = parent_edm_stkhldr_key
connect by nocycle prior edm_spnr_pgm_key = edm_par_pgm_key
order by level, edm_par_pgm_key
;
