--
-- Rule:
--
--   1. Find all terminated accounts (wrt the day we run the query)
--   2. From the account closed date, count back 365 days for each of them
--   3. Scan through every daily snapshots that we've taken so far
--   4. Find the largest outstanding_card count within the 365 days for 
--      each account
--
SELECT *
FROM (  
  SELECT /*+ parallel(12) */
    da.account_hist_key,
    MAX (fdas.MAX_OUTSTANDING_CARDS_COUNT) AS FLEET_SIZE
  FROM d_date dd
  JOIN f_daily_account_snapshot fdas   ON dd.date_key = fdas.date_key
  JOIN d_account da                    ON fdas.account_key = da.account_key
  JOIN d_account_current_vw da_current ON 
    da_current.account_hist_key = da.account_hist_key
  WHERE 1=1
  /* a terminated account wrt run_date */
  AND  da_current.account_status_desc = 'Terminated'
  /* from all snapshots within 365 days before closed */
  AND  dd.calendar_date_dt >= da_current.account_closed_date - 365
  GROUP BY da.account_hist_key
) DRVD_FLEET_SIZE_ATTRITION
;
--
-- Discussion:
--  
--   1. Too many Terminated account that may not be relavent anymore
--   2. The result changes (b/c run time view of "Terminated")
--   
-- Refinement:
--
--   1. Account shows up in runday's snapshot
--   2. Account closed within 30 days
--   3. Skip 'future' snapshots (wrt to runday)
--   4. Use closed date instead of 'Terminated' status
--
select account_closed_date, account_key, account_hist_key
from d_account a, d_date d
where a.account_closed_date between d.calendar_date_dt-30 
                            and     d.calendar_date_dt
and d.calendar_date_dt = to_date('20140101', 'YYYYMMDD')
order by account_closed_date desc
;

--
-- Venu's Max card count
--
      SELECT    
        f1.ACCOUNT_HIST_KEY              ACCOUNT_HIST_KEY,
        MIN(f2.MIN_ACCOUNT_KEY)          MIN_ACCOUNT_KEY, 
        MAX(MAX_OUTSTANDING_CARD_COUNT)  MAX_OUTSTANDING_CARD_COUNT
      FROM (
        SELECT  
          f.ACCOUNT_HIST_KEY             ACCOUNT_HIST_KEY,
          f.DATE_KEY                     DATE_KEY,
          sum(f.OUTSTANDING_CARDS_COUNT) MAX_OUTSTANDING_CARD_COUNT
        FROM F_DAILY_ACCOUNT_SNAPSHOT f
        WHERE F.DATE_KEY <= v_runday_key
        GROUP BY   
          f.ACCOUNT_HIST_KEY,
          f.DATE_KEY
      ) f1, (
        SELECT  
          min(ACCOUNT_KEY)               MIN_ACCOUNT_KEY,
          ACCOUNT_HIST_KEY  
        FROM F_DAILY_ACCOUNT_SNAPSHOT f
        WHERE DATE_KEY = v_runday_key
        GROUP BY 
          ACCOUNT_HIST_KEY,
          DATE_KEY
      ) f2
      WHERE f2.ACCOUNT_HIST_KEY = f1.ACCOUNT_HIST_KEY      
      GROUP BY f1.ACCOUNT_HIST_KEY
;
...
--
-- within 365 days of account_closed_date
--
select d.calendar_date_dt, account_closed_date, account_key
from d_account a, d_date d
where d.calendar_date_dt >= a.account_closed_date - 365
and a.account_closed_date = to_date('20130101', 'YYYYMMDD')
order by account_closed_date desc
;
--
-- all closed account within past 365 days
--
select calendar_date_dt, account_closed_date , account_key
from d_account a, d_date d
where a.account_closed_date between d.calendar_date_dt-365 and d.calendar_date_dt
and d.calendar_date_dt = to_date('20130101', 'YYYYMMDD')
order by account_closed_date asc
;
