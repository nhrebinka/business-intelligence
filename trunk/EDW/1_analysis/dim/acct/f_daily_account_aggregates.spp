CREATE OR REPLACE PROCEDURE F_DAILY_ACCOUNT_AGGREGATES(
  p_bgn_dt date := NULL,
  p_end_dt date := NULL
)
AS
/*
  NAME      : F_DAILY_ACCOUNT_AGGREGATES
  DECRIPTION: load daily Account Aggregates into F_DAILY_ACCOUNT_AGGREGATES
  REVISION  :
  1.0 20100615 Izzy Reinish - Init version
  2.0 20101011 Venugopal Kancharla
      - Added the F_SEED_DATES procedure to autoamte the history  laoding
      - and added the  indexes Drop and Recreate
  2.1 20101212 Venu KAncharla
      - Adding the Filter in the where Clause
  2.2 20110117 VK
      - DW_Current_FLG = '1', commented since this is not needed
  2.3 20110331 VK
      - Added the new Seed Dates to the PROD D_SEED_DATES    
  2.4 20110524 VK
      - Elimate the Card counts for Future Dates for re-issues durinng historic  Data processing
  2.5 20130725 VK
      - Code to Calculate MAX_NUMBER_OF_OUTRSTANDING CARDS . .(Fleet Size ) 
      - This is being added to imporve performance of BO queries
  2.6 20130906 VK
      - Changes for the  Fleet Size Logic based on the Business Inputs
      - Adding the Account_hist_key
  2.7 20130926 VK
      - Added Moved and Converted Filters apart from ther existing Terminated Filters
      - To move these Card status from the outstanding Card Count
  2.8 20140515 VK
      - Added extra routine to max count to account for any account where aCCOUNT_KEY <> aCCOUNT_HIST_KEY
  2.9 20140710 VK
      - fixed the logic for Max Outstanding Card Count
  3.0 20150401 Lee 
      - modulize, add attrition fleet size

Note:
  Note1:
    Added BY VK  on 05/24/2011 
    to elimate the Card counts for Future Dates for re-issues
    during historic  Data processing

  Note2:
    added By VK on 09/26/2013 per Bill Kelley to remove these Card types
*/
  FMT_DT    constant varchar2(20) := 'YYYY-MM-DD';

  X_PRC     constant varchar2(100):= 'DAILY_ACCOUNT_AGGREGATES';
  X_SEED_TB constant varchar2(80) := 'EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM';
  X_TGT_TB  constant varchar2(100):= 'EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT';

  g_proc   varchar2(200);

  v_min_dt date;
  v_max_dt date;

  PROCEDURE purge(p_date_key integer) IS 
  --
  -- Added By Vk for Version 2.4 so that the PROC can be made Rerunable
  --
    v_sql varchar2(2000) := '
      DELETE FROM ' || X_TGT_TB || '
      WHERE DATE_KEY = ' || p_date_key || '
    ';
  BEGIN
    EXECUTE IMMEDIATE v_sql;
    log4me.debug(g_proc || ' purge => ' || SQL%ROWCOUNT || ' rows', v_sql);
    COMMIT;

  EXCEPTION WHEN OTHERS THEN
    log4me.debug(g_proc || ' purge failed', v_sql);
    log4me.err(g_proc || ' FAIL');
    RAISE;
  END purge;

  PROCEDURE run_one_day(p_date_key integer, p_date date) IS
  BEGIN
    INSERT INTO F_DAILY_ACCOUNT_SNAPSHOT (
      DAILY_ACCOUNT_SNAPSHOT_KEY,
      ACCOUNT_KEY,
      PROGRAM_KEY,
      DATE_KEY,
      REGION_KEY,
      /* up-to-date daily cards snapshot */
      OUTSTANDING_CARDS_COUNT,
      TERMINAL_CARD_COUNT,
      TOTAL_CARDS_COUNT,
      CARDS_IN_TRANSITION_COUNT,
      VIRTUAL_OUTSTANDING_CARD_COUNT,
      VIRTUAL_TERMINAL_CARD_COUNT,
      VIRTUAL_TOTAL_CARD_COUNT,
      VIRTUAL_CARDS_IN_TRANS_COUNT,
      MAG_OUTSTANDING_CARD_COUNT,
      MAG_TERMINAL_CARD_COUNT,
      MAG_TOTAL_CARD_COUNT,
      MAG_CARDS_IN_TRANS_COUNT,
      /* daily card  transactions */
      ISSUED_CARDS_COUNT,
      TERMINATED_CARDS_COUNT,
      TRANS_CARDS_COUNT,
      VIRTUAL_ISSUED_CARDS_COUNT,
      VIRTUAL_TERMINATED_CARDS_COUNT,
      VIRTUAL_TRANS_CARDS_COUNT,
      MAG_ISSUED_CARDS_COUNT,
      MAG_TERMINATED_CARDS_COUNT,
      MAG_TRANS_CARD_COUNT,
      /* bookkeeping fields */
      ROW_CREATE_DTTM,
      ROW_LAST_MOD_DTTM,
      ROW_LAST_MOD_PROC_NM,
      ROW_LAST_MOD_PROC_SEQ_NBR,
      /* extra(new) fields */
      ACCOUNT_HIST_KEY,
      MAX_OUTSTANDING_CARD_COUNT,
      FLEET_ATTRITION_CARD_COUNT
    )
    SELECT
      daily_account_snapshot_key_seq.nextval AS daily_account_snapshot_key,
      ACCOUNT_KEY,
      PROGRAM_KEY,
      p_date_key                 AS DATE_KEY,
      0                          AS REGION_KEY,
      /* up-to-date cards counts */
      tot_card - tot_card_term   AS OUTSTANDING_CARD_COUNT,
      tot_card_term              AS TOTAL_TERMINAL_CARD_COUNT,
      tot_card                   AS TOTAL_CARD_COUNT,
      tot_card_tran              AS CARD_IN_TRANS_COUNT, 
      /* virtual cards */
      tot_vcard - tot_vcard_term AS VIRTUAL_OUTSTANDING_CARD_COUNT,
      tot_vcard_term             AS VIRTUAL_TERMINAL_CARC_COUNT,
      tot_vcard                  AS VIRTUAL_TOTAL_CARD_COUNT,
      tot_vcard_tran             AS VIRTUAL_CARD_IN_TRANS_COUNT,
      /* mag cards */
      tot_mcard - tot_mcard_term AS MAG_OUTSTANDING_CARD_COUNT,
      tot_mcard_term             AS MAG_TERMINAL_CARD_COUNT,
      tot_mcard                  AS MAG_TOTAL_CARD_COUNT,
      tot_mcard_tran             AS MAG_CARD_IN_TRANS_COUNT,
      /* daily card transactions */
      ISSUED_CARD_COUNT,
      TERMINATED_CARD_COUNT,
      TRANS_CARD_COUNT,
      /* daily virtual cards */
      VIRTUAL_ISSUED_CARD_COUNT,
      VIRTUAL_TERMINATED_CARD_COUNT,
      VIRTUAL_TRANS_CARD_COUNT,
      /* daily mag cards */
      MAG_ISSUED_CARD_COUNT,
      MAG_TERMINATED_CARD_COUNT,
      MAG_TRANS_CARD_COUNT,
      /* bookkeeping fields */
      sysdate                    AS ROW_CREATE_DTTM,
      sysdate                    AS ROW_LAST_MOD_DTTM,
      X_PRC                      AS ROW_LAST_MOD_PROC_NM,
      1                          AS ROW_LAST_MOD_PROC_SEQ_NBR,
      /* extra(new) fields */
      ACCOUNT_HIST_KEY,
      0                          AS MAX_OUTSTANDING_CARD_COUNT,
      0                          AS FLEET_ATTRITION_CARD_COUNT
    FROM (
      SELECT
        ax.ACCOUNT_HIST_KEY,
        ax.ACCOUNT_KEY,
        ax.PROGRAM_KEY,
        /* up-to-date count */
        sum(pd.PURCHASE_DEVICE_COUNT1)                AS tot_card, /* Note1 */
        sum(pd.IN_TRANSITION_COUNT1)                  AS tot_card_tran,
        sum(pd.TERMINATED_COUNT1)                     AS tot_card_term,
        /* virtual cards */
        sum(pd.VIRTUAL_DEVICE_COUNT1)                 AS tot_vcard,
        sum(
          CASE WHEN pd.CARD_STATUS_DESC = 'In Transition' 
               AND pd.IN_TRANSITION_DATE <= p_date
          THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)   AS tot_vcard_tran,
        sum(
          CASE WHEN pd.CARD_STATUS_DESC = 'Terminated' 
               AND pd.TERMINATED_DATE <= p_date
          THEN pd.VIRTUAL_DEVICE_COUNT1 ELSE 0 END)   AS tot_vcard_term,
        /* mag cards */
        sum(pd.MAGSTRIPE_DEVICE_COUNT1)               AS tot_mcard,
        sum(
          CASE WHEN pd.CARD_STATUS_DESC = 'In Transition' 
               AND pd.IN_TRANSITION_DATE <= p_date
          THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END) AS tot_mcard_tran,
        sum(
          CASE WHEN pd.CARD_STATUS_DESC = 'Terminated' 
               AND pd.TERMINATED_DATE <= p_date
          THEN pd.MAGSTRIPE_DEVICE_COUNT1 ELSE 0 END) AS tot_mcard_term,
        /* daily card  transactions */
        sum(decode(pd.ISSUED_DATE,        p_date, 1, 0)) AS ISSUED_CARD_COUNT,
        sum(decode(pd.IN_TRANSITION_DATE, p_date, 1, 0)) AS TRANS_CARD_COUNT,
        sum(decode(pd.TERMINATED_DATE,    p_date, 1, 0)) AS TERMINATED_CARD_COUNT,
        /* virtual cards */
        sum(decode(pd.ISSUED_DATE,
            p_date, pd.VIRTUAL_DEVICE_COUNT1, 0))   AS VIRTUAL_ISSUED_CARD_COUNT,
        sum(decode(pd.IN_TRANSITION_DATE,
            p_date, pd.VIRTUAL_DEVICE_COUNT1, 0))   AS VIRTUAL_TRANS_CARD_COUNT,
        sum(decode(pd.TERMINATED_DATE,
            p_date, pd.VIRTUAL_DEVICE_COUNT1, 0))   AS VIRTUAL_TERMINATED_CARD_COUNT,
        /* mag cards */
        sum(decode(pd.ISSUED_DATE, 
            p_date, pd.MAGSTRIPE_DEVICE_COUNT1, 0)) AS MAG_ISSUED_CARD_COUNT,
        sum(decode(pd.IN_TRANSITION_DATE,
            p_date, pd.MAGSTRIPE_DEVICE_COUNT1, 0)) AS MAG_TRANS_CARD_COUNT,
        sum(decode(pd.TERMINATED_DATE,
            p_date, pd.MAGSTRIPE_DEVICE_COUNT1, 0)) AS MAG_TERMINATED_CARD_COUNT
      FROM (
        SELECT /*+ parallel(pde, 64) use_hash(pde, a) */
          a.ACCOUNT_HIST_KEY, 
          a.ACCOUNT_KEY,
          pde.PURCHASE_DEVICE_KEY,
          pde.PROGRAM_KEY,
          row_number() OVER (
            PARTITION BY a.ACCOUNT_HIST_KEY, a.ACCOUNT_KEY, pde.PURCHASE_DEVICE_KEY
            ORDER BY pde.ROW_LAST_MOD_DTTM DESC) rn
        FROM D_ACCOUNT a
        JOIN F_PURCHASE_DEVICE_EVENT pde ON (a.ACCOUNT_KEY = pde.ACCOUNT_KEY)
        WHERE a.ACCOUNT_CLOSED_DATE >= p_date  /* to 12/31/9999  */
        AND   a.ACCOUNT_OPEN_DATE   <= p_date  /* a real account */
      ) ax
      JOIN (
        SELECT * FROM D_PURCHASE_DEVICE 
        WHERE p_date - CASE 
          WHEN CARD_STATUS_DESC IN 
               ('Activated', 'Suspended')           THEN ACTIVATED_DATE
          WHEN CARD_STATUS_DESC IN
               ('In Transition')                    THEN IN_TRANSITION_DATE
          WHEN CARD_STATUS_DESC IN 
               ('Terminated', 'Moved', 'Converted') THEN TERMINATED_DATE
          ELSE sysdate
          END >=0
      ) pd ON (pd.PURCHASE_DEVICE_KEY = ax.PURCHASE_DEVICE_KEY)
      WHERE ax.rn = 1
      GROUP BY
        ax.ACCOUNT_HIST_KEY,
        ax.ACCOUNT_KEY, 
        ax.PROGRAM_KEY
    );
    log4me.debug(g_proc || ' insert => ' || SQL%ROWCOUNT || ' rows', '');
    COMMIT;

  EXCEPTION WHEN OTHERS THEN
    log4me.debug(g_proc || ' insert => failed', '');
    RAISE;
  END run_one_day;

  PROCEDURE update_max_card_count(p_date date) IS
    v_date_key  integer;
    v_part      varchar2(100);
    v_tgt       varchar2(200);
    v_sql       varchar2(4000);

  BEGIN
    -- Code to get the First Day of the month
    SELECT 
      DATE_KEY, 
      'YM_' || to_char(LAST_DAY_IN_MONTH_DATE_DT, 'YYYY_MM')
    INTO   v_date_key, v_part
    FROM   D_DATE
    WHERE  CALENDAR_DATE_DT = p_date;

    -- Actual Code to find the MAX Number of outstanding CARDCOUNT,
    -- From the starting of the month until that day
    -- Note Max number outstnading card Count is updated 
    -- where account_key = minimum account_key number

  SELECT
    date_key,
    account_hist_key,
    account_key,
    min_key,
    cn,
    scn,
    mcn,
    max(scn) over (partition by account_hist_key) mx,
    row_number() over (partition by account_hist_key, account_key order by date_key) rn 
  FROM (
    SELECT /*+ parallel(x,64) use_hash(a,x) */
      x.DATE_KEY,
      a.account_hist_key,
      x.account_key,
      min(x.account_key) over (partition by a.account_hist_key) min_key,
      x.outstanding_cards_count     cn,
      x.MAX_OUTSTANDING_CARDS_COUNT mcn,
      sum(x.outstanding_cards_count) over (partition by x.date_key, a.account_hist_key) scn
    FROM F_DAILY_ACCOUNT_SNAPSHOT partition (ym_2014_02) x
    JOIN D_ACCOUNT a ON (x.account_key = a.account_key)
    WHERE DATE_KEY <= v_date_key
  )
  ;

    v_tgt := X_TGT_TB || ' PARTITION (' || v_part || ')';
    v_sql := '
    MERGE INTO ' || v_tgt || ' fdas
    USING (
      SELECT    
        f1.ACCOUNT_HIST_KEY              ACCOUNT_HIST_KEY,
        MIN(f2.MIN_ACCOUNT_KEY)          MIN_ACCOUNT_KEY, 
        MAX(MAX_OUTSTANDING_CARD_COUNT)  MAX_OUTSTANDING_CARD_COUNT
      FROM (
        SELECT  
          f.ACCOUNT_HIST_KEY             ACCOUNT_HIST_KEY,
          f.DATE_KEY                     DATE_KEY,
          sum(f.OUTSTANDING_CARDS_COUNT) MAX_OUTSTANDING_CARD_COUNT
        FROM ' || v_tgt ||' f
        WHERE F.DATE_KEY <= ' || v_date_key || '
        GROUP BY   
          f.ACCOUNT_HIST_KEY,
          f.DATE_KEY
      ) f1, (
        SELECT  
          min(ACCOUNT_KEY)               MIN_ACCOUNT_KEY,
          ACCOUNT_HIST_KEY  
        FROM ' || v_tgt || ' f
        WHERE DATE_KEY = ' || v_date_key || '
        GROUP BY 
          ACCOUNT_HIST_KEY,
          DATE_KEY
      ) f2
      WHERE f2.ACCOUNT_HIST_KEY = f1.ACCOUNT_HIST_KEY      
      GROUP BY f1.ACCOUNT_HIST_KEY
    ) a ON (fdas.ACCOUNT_KEY = a.MIN_ACCOUNT_KEY)
    WHEN MATCHED THEN UPDATE SET
      fdas.MAX_OUTSTANDING_CARDS_COUNT = a.MAX_OUTSTANDING_CARD_COUNT
    WHERE fdas.DATE_KEY = ' || v_date_key;


    EXECUTE IMMEDIATE v_sql;
    log4me.debug(g_proc || ' max_card => ' || SQL%ROWCOUNT || ' rows', v_sql);
    COMMIT;

  EXCEPTION WHEN OTHERS THEN
    log4me.debug(g_proc || ' FAILED update_max_card_count', v_sql);
    log4me.err(g_proc || ' FAIL update_max_card_count');
    RAISE;
  END update_max_card_count;

BEGIN
  log4me.info(X_PRC || ' INIT ', p_ses_id);
  xos_session_mgr.get_date_range(
    p_bgn_dt,
    p_end_dt,
    X_SEED_TB,
    v_min_dt,
    v_max_dt
  );
  IF (v_min_dt IS NULL OR v_max_dt IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20400, 'no date range given');
  END IF;

  g_proc := X_PRC || '[' ||
    to_char(v_min_dt, FMT_DT) || ',' ||
    to_char(v_max_dt, FMT_DT) ||
    ']';
  --
  -- day-by-day walk through given day range
  --
  FOR rec IN(
    SELECT 
      DATE_KEY,
      CALENDAR_DATE_DT
    FROM D_DATE
    WHERE CALENDAR_DATE_DT BETWEEN v_min_dt AND v_max_dt
    ORDER BY DATE_KEY
  )
  LOOP
    purge(rec.DATE_KEY);
    run_one_day(rec.DATE_KEY, rec.calendar_date_dt);
    update_max_card_count(rec.date_key);
  END LOOP;
  log4me.info(X_PRC || ' DONE ', p_ses_id);

EXCEPTION WHEN OTHERS THEN
  log4me.err(X_PRC || ' FAIL');
  RAISE;
END F_DAILY_ACCOUNT_AGGREGATES;
/
show errors

GRANT EXECUTE ON F_DAILY_ACCOUNT_AGGREATES TO DWLOADER;
