-- 
-- TABLE: T_LEDGER 
--
DROP TABLE EDM_OWNER.T_LEDGER CASCADE CONSTRAINTS;

CREATE TABLE EDM_OWNER.T_LEDGER(
    EDM_GL_ACCOUNT_KEY          NUMBER(38, 0)    NOT NULL,
    EDM_COST_CENTER_KEY         NUMBER(38, 0)    NOT NULL,
    BUSINESS_UNIT               VARCHAR2(30)     ,
    LEDGER                      VARCHAR2(30)     ,
    GL_ACCOUNT_ID               VARCHAR2(15)     ,
    GL_ALT_ACCOUNT_ID           VARCHAR2(15)     ,
    COST_CENTER_ID              VARCHAR2(30)     ,
    OPERATING_UNIT              VARCHAR2(30)     ,
    PRODUCT                     VARCHAR2(30)     ,
    FUNDER_CODE                 VARCHAR2(30)     ,
    CLASS_FIELD                 VARCHAR2(30)     ,
    PROGRAM_CODE                VARCHAR2(30)     ,
    BUDGET_REFERENCE            VARCHAR2(30)     ,
    AFFILIATE                   VARCHAR2(30)     ,
    AFFILIATE_INTRA1            VARCHAR2(30)     ,
    AFFILIATE_INTRA2            VARCHAR2(30)     ,
    CHARTFIELD1                 VARCHAR2(30)     ,
    CHARTFIELD2                 VARCHAR2(30)     ,
    CHARTFIELD3                 VARCHAR2(30)     ,
    PROJECT_ID                  VARCHAR2(30)     ,
    BOOK_CODE                   VARCHAR2(30)     ,
    GL_ADJUSTMENT_TYPE          VARCHAR2(30)     ,
    CURRENCY_CODE               VARCHAR2(30)     ,
    STATISTICS_CODE             VARCHAR2(30)     ,
    FISCAL_YEAR                 NUMBER(38, 0)    ,
    ACCOUNTING_PERIOD           NUMBER(38, 0)    ,
    POSTED_TOTAL_AMOUNT         NUMBER(20, 2)    ,
    POSTED_BASE_AMOUNT          NUMBER(20, 2)    ,
    POSTED_TRANS_AMOUNT         NUMBER(20, 2)    ,
    BASE_CURRENCY               VARCHAR2(30)     ,
    SRC_SYS_LAST_UPDT_DT        TIMESTAMP(6)     NOT NULL,
    PROCESS_INSTANCE            VARCHAR2(100)    ,
    EDM_CREATE_DT               TIMESTAMP(6)     NOT NULL,
    EDM_LAST_UPDT_DT            TIMESTAMP(6)     NOT NULL,
    EDM_LAST_UPDT_SESSION_NM    VARCHAR2(100)    NOT NULL,
    EDM_SOURCE_SYS              VARCHAR2(30)     
)
TABLESPACE D_BI
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING
;

--
-- no RI defined for T tables
--

-- T_LEDGER
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EDM_OWNER.T_LEDGER TO DWLOADER;
GRANT SELECT ON EDM_OWNER.T_LEDGER TO EDM_OWNER_SELECT;
GRANT SELECT ON EDM_OWNER.T_LEDGER TO LINK_OWNER;
GRANT SELECT ON EDM_OWNER.T_LEDGER TO STAGE_OWNER_SELECT;


