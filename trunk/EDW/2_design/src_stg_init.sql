--
-- Wex BI4.0 - Init DDL for HUB PeopleSoft external source tables
--
-- ------------------------------------------------------------------------
-- 
DROP TABLE SRC_PS_GL_DEPT_TBL;
DROP TABLE SRC_PS_GL_ACCOUNT_TBL;
DROP TABLE SRC_PS_GL_LEDGER;

DROP TABLE SRC_SOURCE_MKT_CHANNEL;
DROP TABLE SRC_SOURCE_BUSINESS_SEGMENT;

DROP TABLE SRC_EXCEL_COST_TO_SERVE;
DROP TABLE SRC_EXCEL_COST_TO_MKT_CHL;
DROP TABLE SRC_EXCEL_COST_TO_ADJUDICATE;
DROP TABLE SRC_EXCEL_COST_TO_ACQUIRE;
DROP TABLE SRC_EXCEL_COST_OF_SALES;
DROP TABLE SRC_EXCEL_COST_OF_IT;

CREATE TABLE SRC_PS_GL_DEPT_TBL
(
  SETID              VARCHAR2(5 BYTE)           NULL,
  DEPTID             VARCHAR2(10 BYTE)          NULL,
  EFFDT              DATE                       NULL,
  MANAGER_NAME       VARCHAR2(30 BYTE)          NULL,
  ACCOUNTING_OWNER   VARCHAR2(30 BYTE)          NULL,
  COUNTRY_GRP        VARCHAR2(18 BYTE)          NULL,
  BUDGETARY_ONLY     VARCHAR2(1 BYTE)           NULL,
  SYNCID             INTEGER                    NULL,
  SYNCDTTM           DATE                       NULL,
  EFF_STATUS         VARCHAR2(1 BYTE)           NULL,
  DESCR              VARCHAR2(30 BYTE)          NULL,
  DESCRSHORT         VARCHAR2(10 BYTE)          NULL,
  COMPANY            VARCHAR2(3 BYTE)           NULL,
  SETID_LOCATION     VARCHAR2(5 BYTE)           NULL,
  LOCATION           VARCHAR2(10 BYTE)          NULL,
  TAX_LOCATION_CD    VARCHAR2(10 BYTE)          NULL,
  MANAGER_ID         VARCHAR2(11 BYTE)          NULL,
  MANAGER_POSN       VARCHAR2(8 BYTE)           NULL,
  BUDGET_YR_END_DT   INTEGER                    NULL,
  BUDGET_LVL         VARCHAR2(1 BYTE)           NULL,
  GL_EXPENSE         VARCHAR2(35 BYTE)          NULL,
  EEO4_FUNCTION      VARCHAR2(2 BYTE)           NULL,
  CAN_IND_SECTOR     VARCHAR2(3 BYTE)           NULL,
  ACCIDENT_INS       VARCHAR2(3 BYTE)           NULL,
  SI_ACCIDENT_NUM    VARCHAR2(15 BYTE)          NULL,
  HAZARD             VARCHAR2(4 BYTE)           NULL,
  ESTABID            VARCHAR2(12 BYTE)          NULL,
  RISKCD             VARCHAR2(6 BYTE)           NULL,
  GVT_DESCR40        VARCHAR2(40 BYTE)          NULL,
  GVT_SUB_AGENCY     VARCHAR2(2 BYTE)           NULL,
  GVT_PAR_LINE2      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE3      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE4      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE5      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_DESCR2     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR3     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR4     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR5     VARCHAR2(40 BYTE)          NULL,
  CLASS_UNIT_NZL     VARCHAR2(5 BYTE)           NULL,
  ORG_UNIT_AUS       VARCHAR2(4 BYTE)           NULL,
  WORK_SECTOR_AUS    VARCHAR2(1 BYTE)           NULL,
  APS_AGENT_CD_AUS   INTEGER                    NULL,
  IND_COMMITTEE_BEL  VARCHAR2(3 BYTE)           NULL,
  NACE_CD_BEL        VARCHAR2(10 BYTE)          NULL,
  FTE_EDIT_INDC      VARCHAR2(1 BYTE)           NULL,
  DEPT_TENURE_FLG    VARCHAR2(1 BYTE)           NULL,
  TL_DISTRIB_INFO    VARCHAR2(1 BYTE)           NULL,
  USE_BUDGETS        VARCHAR2(1 BYTE)           NULL,
  USE_ENCUMBRANCES   VARCHAR2(1 BYTE)           NULL,
  USE_DISTRIBUTION   VARCHAR2(1 BYTE)           NULL,
  BUDGET_DEPTID      VARCHAR2(10 BYTE)          NULL,
  DIST_PRORATE_OPTN  VARCHAR2(1 BYTE)           NULL,
  HP_STATS_DEPT_CD   VARCHAR2(3 BYTE)           NULL,
  HP_STATS_FACULTY   VARCHAR2(5 BYTE)           NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_dept_tbl.log'
    BADFILE      edm_log_dir:'src_ps_gl_dept_tbl.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_dept_tbl.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      SETID,
      DEPTID,
      EFFDT CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      MANAGER_NAME,
      ACCOUNTING_OWNER,
      COUNTRY_GRP,
      BUDGETARY_ONLY,
      SYNCID,
      SYNCDTTM CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF SYNCDTTM = BLANKS,
      EFF_STATUS,
      DESCR,
      DESCRSHORT,
      COMPANY,
      SETID_LOCATION,
      LOCATION,
      TAX_LOCATION_CD,
      MANAGER_ID,
      MANAGER_POSN,
      BUDGET_YR_END_DT,
      BUDGET_LVL,
      GL_EXPENSE,
      EEO4_FUNCTION,
      CAN_IND_SECTOR,
      ACCIDENT_INS,
      SI_ACCIDENT_NUM,
      HAZARD,
      ESTABID,
      RISKCD,
      GVT_DESCR40,
      GVT_SUB_AGENCY,
      GVT_PAR_LINE2,
      GVT_PAR_LINE3,
      GVT_PAR_LINE4,
      GVT_PAR_LINE5,
      GVT_PAR_DESCR2,
      GVT_PAR_DESCR3,
      GVT_PAR_DESCR4,
      GVT_PAR_DESCR5,
      CLASS_UNIT_NZL,
      ORG_UNIT_AUS,
      WORK_SECTOR_AUS,
      APS_AGENT_CD_AUS,
      IND_COMMITTEE_BEL,
      NACE_CD_BEL,
      FTE_EDIT_INDC,
      DEPT_TENURE_FLG,
      TL_DISTRIB_INFO,
      USE_BUDGETS,
      USE_ENCUMBRANCES,
      USE_DISTRIBUTION,
      BUDGET_DEPTID,
      DIST_PRORATE_OPTN,
      HP_STATS_DEPT_CD,
      HP_STATS_FACULTY
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_dept_tbl.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_PS_GL_ACCOUNT_TBL
(
  SETID               VARCHAR2(5 BYTE)          NULL,
  ACCOUNT             VARCHAR2(10 BYTE)         NULL,
  EFFDT               DATE                      NULL,
  DESCR               VARCHAR2(30 BYTE)         NULL,
  ACCOUNT_TYPE        VARCHAR2(1 BYTE)          NULL,
  EFF_STATUS          VARCHAR2(1 BYTE)          NULL,
  DESCRSHORT          VARCHAR2(10 BYTE)         NULL,
  BUDGETARY_ONLY      VARCHAR2(1 BYTE)          NULL,
  SYNCID              INTEGER                   NULL,
  SYNCDTTM            DATE                      NULL,
  BUDG_OVERRIDE_ACCT  VARCHAR2(1 BYTE)          NULL,
  ACCOUNTING_OWNER    VARCHAR2(30 BYTE)         NULL,
  AB_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  GL_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  PF_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  UNIT_OF_MEASURE     VARCHAR2(3 BYTE)          NULL,
  OPEN_ITEM           VARCHAR2(1 BYTE)          NULL,
  OPEN_ITEM_DESCR     VARCHAR2(10 BYTE)         NULL,
  OPEN_ITEM_EDIT_REC  VARCHAR2(15 BYTE)         NULL,
  OPEN_ITEM_EDIT_FLD  VARCHAR2(18 BYTE)         NULL,
  OPEN_ITEM_PROMPT    VARCHAR2(15 BYTE)         NULL,
  OPEN_ITEM_TOL_AMT   NUMBER(26,3)              NULL,
  CURRENCY_CD         VARCHAR2(3 BYTE)          NULL,
  STATISTICS_ACCOUNT  VARCHAR2(1 BYTE)          NULL,
  BALANCE_FWD_SW      VARCHAR2(1 BYTE)          NULL,
  CONTROL_FLAG        VARCHAR2(1 BYTE)          NULL,
  BOOK_CODE           VARCHAR2(4 BYTE)          NULL,
  BOOK_CODE_OVERRIDE  VARCHAR2(1 BYTE)          NULL,
  BAL_SHEET_IND       VARCHAR2(2 BYTE)          NULL,
  VAT_ACCOUNT_FLG     VARCHAR2(1 BYTE)          NULL,
  PHYSICAL_NATURE     VARCHAR2(1 BYTE)          NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_account_tbl.log'
    BADFILE      edm_log_dir:'src_ps_gl_account_tbl.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_account_tbl.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      SETID,
      ACCOUNT,
      EFFDT    CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      DESCR,
      ACCOUNT_TYPE,
      EFF_STATUS,
      DESCRSHORT,
      BUDGETARY_ONLY,
      SYNCID,
      SYNCDTTM CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF SYNCDTTM = BLANKS
      BUDG_OVERRIDE_ACCT,
      ACCOUNTING_OWNER,
      AB_ACCOUNT_SW,
      GL_ACCOUNT_SW,
      PF_ACCOUNT_SW,
      UNIT_OF_MEASURE,
      OPEN_ITEM,
      OPEN_ITEM_DESCR,
      OPEN_ITEM_EDIT_REC,
      OPEN_ITEM_EDIT_FLD,
      OPEN_ITEM_PROMPT,
      OPEN_ITEM_TOL_AMT,
      CURRENCY_CD,
      STATISTICS_ACCOUNT,
      BALANCE_FWD_SW,
      CONTROL_FLAG,
      BOOK_CODE,
      BOOK_CODE_OVERRIDE,
      BAL_SHEET_IND,
      VAT_ACCOUNT_FLG,
      PHYSICAL_NATURE
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_account_tbl.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_PS_GL_LEDGER
(
  BUSINESS_UNIT      VARCHAR2(5 BYTE)           NULL,
  LEDGER             VARCHAR2(10 BYTE)          NULL,
  ACCOUNT            VARCHAR2(10 BYTE)          NULL,
  ALTACCT            VARCHAR2(10 BYTE)          NULL,
  DEPTID             VARCHAR2(10 BYTE)          NULL,
  OPERATING_UNIT     VARCHAR2(8 BYTE)           NULL,
  PRODUCT            VARCHAR2(6 BYTE)           NULL,
  FUND_CODE          VARCHAR2(5 BYTE)           NULL,
  CLASS_FLD          VARCHAR2(5 BYTE)           NULL,
  PROGRAM_CODE       VARCHAR2(5 BYTE)           NULL,
  BUDGET_REF         VARCHAR2(8 BYTE)           NULL,
  AFFILIATE          VARCHAR2(5 BYTE)           NULL,
  AFFILIATE_INTRA1   VARCHAR2(10 BYTE)          NULL,
  AFFILIATE_INTRA2   VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD1        VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD2        VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD3        VARCHAR2(10 BYTE)          NULL,
  PROJECT_ID         VARCHAR2(15 BYTE)          NULL,
  BOOK_CODE          VARCHAR2(4 BYTE)           NULL,
  GL_ADJUST_TYPE     VARCHAR2(4 BYTE)           NULL,
  CURRENCY_CD        VARCHAR2(3 BYTE)           NULL,
  STATISTICS_CODE    VARCHAR2(3 BYTE)           NULL,
  FISCAL_YEAR        INTEGER                    NULL,
  ACCOUNTING_PERIOD  INTEGER                    NULL,
  POSTED_TOTAL_AMT   NUMBER(26,3)               NULL,
  POSTED_BASE_AMT    NUMBER(26,3)               NULL,
  POSTED_TRAN_AMT    NUMBER(26,3)               NULL,
  BASE_CURRENCY      VARCHAR2(3 BYTE)           NULL,
  DTTM_STAMP_SEC     DATE                       NULL,
  PROCESS_INSTANCE   NUMBER(10)                 NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_ledger.log'
    BADFILE      edm_log_dir:'src_ps_gl_ledger.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_ledger.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      BUSINESS_UNIT,
      LEDGER,
      ACCOUNT,
      ALTACCT,
      DEPTID,
      OPERATING_UNIT,
      PRODUCT,
      FUND_CODE,
      CLASS_FLD,
      PROGRAM_CODE,
      BUDGET_REF,
      AFFILIATE,
      AFFILIATE_INTRA1,
      AFFILIATE_INTRA2,
      CHARTFIELD1,
      CHARTFIELD2,
      CHARTFIELD3,
      PROJECT_ID,
      BOOK_CODE,
      GL_ADJUST_TYPE,
      CURRENCY_CD,
      STATISTICS_CODE,
      FISCAL_YEAR,
      ACCOUNTING_PERIOD,
      POSTED_TOTAL_AMT,
      POSTED_BASE_AMT,
      POSTED_TRAN_AMT,
      BASE_CURRENCY,
      DTTM_STAMP_SEC CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF DTTM_STAMP_SEC = BLANKS,
      PROCESS_INSTANCE
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_ledger.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_SOURCE_MKT_CHANNEL
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    SOURCE_EXCEL_TYPE      varchar2(100),
    START_DATE             date,
    END_DATE               date,
    MKT_PARTNER_ID         varchar2(200),
    MKT_PARTNER_SUB_CHL_ID varchar2(200),
    MKT_SUB_CHL_ALLOCATION number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_source_mkt_channel.log'
    BADFILE      edm_log_dir:'src_source_mkt_channel.bad'
    DISCARDFILE  edm_log_dir:'src_source_mkt_channel.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      SOURCE_EXCEL_TYPE,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      MKT_PARTNER_ID,
      MKT_PARTNER_SUB_CHL_ID,
      MKT_SUB_CHL_ALLOCATION
    )
  )
  LOCATION (EDM_DATA_DIR:'src_source_mkt_channel.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_SOURCE_BUSINESS_SEGMENT
(
    NAME                        varchar2(100),
    VERSION                     varchar2(100),
    SOURCE_EXCEL_TYPE           varchar2(100),
    START_DATE                  date,
    END_DATE                    date,
    COST_CENTER_ID              varchar2(200),
    COST_CENTER_NAME            varchar2(200),
    BUSINESS_SEGMENT_ID         varchar2(200),
    BUSINESS_SEGMENT_ALLOCATION number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_source_business_segment.log'
    BADFILE      edm_log_dir:'src_source_business_segment.bad'
    DISCARDFILE  edm_log_dir:'src_source_business_segment.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      SOURCE_EXCEL_TYPE,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER_ID,
      COST_CENTER_NAME,
      BUSINESS_SEGMENT_ID,
      BUSINESS_SEGMENT_ALLOCATION
    )
  )
  LOCATION (EDM_DATA_DIR:'src_source_business_segment.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_SERVE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_serve.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_serve.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_serve.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_serve.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_MKT_CHL
(
    NAME                  varchar2(100),
    START_DATE            date,
    END_DATE              date,
    MARKETING_SUB_CHANNEL varchar2(200),
    "7-ELEVEN"            number(20,2),
    ALON                  number(20,2),
    CIRCLE_K              number(20,2),
    CITGO                 number(20,2),
    ENTERPRISE            number(20,2),
    EXXONMOBIL            number(20,2),
    FUELTRAC              number(20,2),
    GET_GO                number(20,2),
    GULF                  number(20,2),
    HESS                  number(20,2),
    IMPERIAL              number(20,2),
    LUKOIL                number(20,2),
    MAPCO                 number(20,2),
    MARATHON              number(20,2),
    MEIJER                number(20,2),
    MURPHY_OIL            number(20,2),
    PEP_BOYS              number(20,2),
    PHILLIPS66            number(20,2),
    QUIKTRIP              number(20,2),
    RACETRAC              number(20,2),
    SEARS                 number(20,2),
    SHEETZ                number(20,2),
    SUNOCO                number(20,2),
    TESORO                number(20,2),
    VALVOLINE             number(20,2),
    WAWA                  number(20,2),
    WEX                   number(20,2),
    ALLOTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_mkt_chl.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      MARKETING_SUB_CHANNEL,
      "7-ELEVEN",
      ALON,
      CIRCLE_K,
      CITGO,
      ENTERPRISE,
      EXXONMOBIL,
      FUELTRAC,
      GET_GO,
      GULF,
      HESS,
      IMPERIAL,
      LUKOIL,
      MAPCO,
      MARATHON,
      MEIJER,
      MURPHY_OIL,
      PEP_BOYS,
      PHILLIPS66,
      QUIKTRIP,
      RACETRAC,
      SEARS,
      SHEETZ,
      SUNOCO,
      TESORO,
      VALVOLINE,
      WAWA,
      WEX,
      ALLOTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_mkt_chl.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ADJUDICATE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_adjudicate.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_adjudicate.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_adjudicate.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_adjudicate.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ACQUIRE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_acquire.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_acquire.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_acquire.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_acquire.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_SALES
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
   PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_sales.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_sales.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_sales.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_sales.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_IT
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_it.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_it.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_it.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_it.dat')
)
REJECT LIMIT 0
NOPARALLEL
;









