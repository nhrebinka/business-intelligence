--
-- Sequences for F tables on EDW
--
DROP SEQUENCE #{N}_KEY_SEQ;
CREATE SEQUENCE #{N}_KEY_SEQ
  START WITH 10000001
  MAXVALUE 999999999999999999999999999
  MINVALUE 10000001
  NOCYCLE
  CACHE 1000
  NOORDER;

CREATE OR REPLACE PUBLIC SYNONYM #{N}_KEY_SEQ FOR #{N}_KEY_SEQ;
GRANT ALTER, SELECT ON #{N}_KEY_SEQ TO DWLOADER;


