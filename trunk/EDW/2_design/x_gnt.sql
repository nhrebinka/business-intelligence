GRANT ALL    ON EDW_STAGE_OWNER.#{N} TO DWLOADER;
GRANT SELECT ON EDW_STAGE_OWNER.#{N} TO EDW_OWNER_SELECT;
GRANT SELECT ON EDW_STAGE_OWNER.#{N} TO EDW_OWNER;
