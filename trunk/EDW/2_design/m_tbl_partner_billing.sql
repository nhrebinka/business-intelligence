--
-- Wex BI4.0 - DDL for HUB partner billing tables
--
-- ------------------------------------------------------------------------
-- 
-- TABLE: M_SPNR_PGM
--
ALTER TABLE M_SPNR_PGM ADD (PARTNER_BILLING_CUST_ID        VARCHAR2(200));
ALTER TABLE M_SPNR_PGM ADD (PARTNER_BILLING_PRODUCT_NAME   VARCHAR2(200));



