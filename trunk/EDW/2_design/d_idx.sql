--
-- unique index #{N} = Dimension Name
--
CREATE UNIQUE INDEX XPKD_#{N} OR ON D_#{N} (
  #{N}
)
TABLESPACE I_BI
;
