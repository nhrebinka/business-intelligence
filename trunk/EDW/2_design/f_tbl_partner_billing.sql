--
-- Wex BI4.2 - DDL for EDW Fact tables for Partner Billing
--
-- ------------------------------------------------------------------------
CREATE TABLE F_PGM_BILLING_MONTHLY_SNAPSHOT
(
    PROGRAM_KEY                  integer            NOT NULL,
    REVENUE_DATE_KEY             integer            NOT NULL,
    BILL_AMOUNT                  number(15,2)       NOT NULL,
    SIGN_ON_BONUS_AMT            number(15,2)       NOT NULL,
    ROW_CREATE_DTTM              timestamp          NOT NULL,
    ROW_LAST_MOD_DTTM            timestamp          NOT NULL,
    ROW_SOURCE_SYS_NM				     varchar2(200)      NOT NULL,
    ROW_LAST_MOD_PROC_NM         varchar2(200)      NOT NULL,
    ROW_LAST_MOD_PROC_SEQ_NBR    integer            NOT NULL
)
TABLESPACE D_F_TRANS_LINE_ITEM
LOGGING
;

GRANT ALL ON F_PGM_BILLING_MONTHLY_SNAPSHOT TO DWLOADER;




