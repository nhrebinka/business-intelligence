--
-- Wex BI4.0 - DDL for Excel-based source external tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_SOURCE_MKT_CHANNEL
(
    NAME                   varchar2(100)          NOT NULL,
    VERSION                varchar2(100)          NOT NULL,
    SOURCE_EXCEL_TYPE      varchar2(100)          NOT NULL,
    START_DATE             date                   NOT NULL,
    END_DATE               date                   NOT NULL,
    MKT_PARTNER_ID         varchar2(200)          NOT NULL,
    MKT_PARTNER_SUB_CHL_ID varchar2(200)          NOT NULL,
    MKT_SUB_CHL_ALLOCATION number(20,2)           NOT NULL
)
TABLESPACE D_BI
;

CREATE TABLE SRC_SOURCE_BUSINESS_SEGMENT
(
    NAME                        varchar2(100)     NOT NULL,
    VERSION                     varchar2(100)     NOT NULL,
    SOURCE_EXCEL_TYPE           varchar2(100)     NOT NULL,
    START_DATE                  date              NOT NULL,
    END_DATE                    date              NOT NULL,
    COST_CENTER_ID              varchar2(200)     NOT NULL,
    COST_CENTER_NAME            varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ID         varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ALLOCATION number(20,2)      NOT NULL
)
TABLESPACE D_BI
;

