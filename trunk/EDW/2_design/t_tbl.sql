--
-- Wex BI4.0 - DDL for HUB Fact Temp tables
--
-- ------------------------------------------------------------------------
-- 
-- TABLE: T_ACCT_EXPENSE
--
CREATE TABLE T_ACCT_EXPENSE
(
    EDM_BUSINESS_SECTOR_KEY     INTEGER          NOT NULL,
    EDM_BUSINESS_SEGMENT_KEY    INTEGER          NOT NULL,
    EDM_FR_CATEGORY_KEY         INTEGER          NOT NULL,
    EDM_COST_CENTER_KEY         INTEGER          NOT NULL,
    EDM_SPNR_PGM_KEY            INTEGER          NOT NULL,
    EDM_ACCT_KEY                INTEGER          NOT NULL,
    REVENUE_DT                  DATE             NULL,
    SNAPSHOT_DT                 DATE             NULL,
    AMOUNT                      NUMBER(38,6)     NULL,
    DEPRECIATED_AMOUNT          NUMBER(38,6)     NULL,
    BUSINESS_SECTOR_ID          VARCHAR2(100)    NULL,
    BUSINESS_SEGMENT_ID         VARCHAR2(100)    NULL,
    FR_CATEGORY_ID              VARCHAR2(100)    NULL,
    COST_CENTER_ID              VARCHAR2(100)    NULL,
    PROGRAM_ID                  VARCHAR2(200)    NULL,
    WEX_ACCT_NBR                VARCHAR2(30)     NULL,
    MONTHLY_OVERALL_TICKET_COUNT INTEGER         NULL,
    MONTHLY_FLEET_TICKET_COUNT   INTEGER         NULL,
    MONTHLY_SEGMENT_TICKET_COUNT INTEGER         NULL
)
TABLESPACE D_BI
NOLOGGING
NOMONITORING
PARALLEL
;
-- 
-- TABLE: T_BUSINESS_SEGMENT_ACCOUNT 
--
CREATE TABLE T_BUSINESS_SEGMENT_ACCOUNT
(
    EDM_BUSINESS_SECTOR_KEY     INTEGER          NOT NULL,
    EDM_BUSINESS_SEGMENT_KEY    INTEGER          NOT NULL,
    EDM_SPNR_PGM_KEY            INTEGER          NOT NULL,
    EDM_ACCT_KEY                INTEGER          NOT NULL,
    EVENT_DT                    DATE             NULL,
    BUSINESS_SEGMENT_ID         VARCHAR2(100)    NULL,
    BUSINESS_SECTOR_ID          VARCHAR2(100)    NULL,
    MARKETING_PARTNER_NM        VARCHAR2(200)    NULL,
    PROGRAM_ID                  VARCHAR2(200)    NULL,
    WEX_ACCT_NBR                VARCHAR2(30)     NULL
)
TABLESPACE D_BI
NOLOGGING
PARALLEL
;
-- 
-- TABLE: T_LEDGER 
--
CREATE TABLE T_LEDGER
(
    EDM_GL_ACCOUNT_KEY          INTEGER          NOT NULL,
    EDM_COST_CENTER_KEY         INTEGER          NOT NULL,
    BUSINESS_UNIT               VARCHAR2(100)    NULL,
    LEDGER                      VARCHAR2(100)    NULL,
    GL_ACCOUNT_ID               VARCHAR2(100)    NULL,
    GL_ALT_ACCOUNT_ID           VARCHAR2(100)    NULL,
    COST_CENTER_ID              VARCHAR2(100)    NULL,
    OPERATING_UNIT              VARCHAR2(100)    NULL,
    PRODUCT                     VARCHAR2(100)    NULL,
    FUNDER_CODE                 VARCHAR2(100)    NULL,
    CLASS_FIELD                 VARCHAR2(100)    NULL,
    PROGRAM_CODE                VARCHAR2(100)    NULL,
    BUDGET_REFERENCE            VARCHAR2(100)    NULL,
    AFFILIATE                   VARCHAR2(100)    NULL,
    AFFILIATE_INTRA1            VARCHAR2(100)    NULL,
    AFFILIATE_INTRA2            VARCHAR2(100)    NULL,
    CHARTFIELD1                 VARCHAR2(100)    NULL,
    CHARTFIELD2                 VARCHAR2(100)    NULL,
    CHARTFIELD3                 VARCHAR2(100)    NULL,
    PROJECT_ID                  VARCHAR2(100)    NULL,
    BOOK_CODE                   VARCHAR2(100)    NULL,
    GL_ADJUSTMENT_TYPE          VARCHAR2(100)    NULL,
    CURRENCY_CODE               VARCHAR2(100)    NULL,
    STATISTICS_CODE             VARCHAR2(100)    NULL,
    FISCAL_YEAR                 NUMBER(38,0)     NULL,
    ACCOUNTING_PERIOD           NUMBER(38,0)     NULL,
    POSTED_TOTAL_AMOUNT         NUMBER(20,2)     NULL,
    POSTED_BASE_AMOUNT          NUMBER(20,2)     NULL,
    POSTED_TRANS_AMOUNT         NUMBER(20,2)     NULL,
    BASE_CURRENCY               VARCHAR2(100)    NULL,
    SRC_SYS_LAST_UPDT_DT        TIMESTAMP        NULL,
    PROCESS_INSTANCE            VARCHAR2(100)    NULL,
    EDM_SOURCE_SYS              VARCHAR2(100)    NOT NULL
)
TABLESPACE D_BI
NOLOGGING
NOMONITORING
PARALLEL
;
-- 
-- TABLE: T_MANUAL_REBATE 
--
CREATE TABLE T_MANUAL_REBATE
(
    EDM_SPNR_PGM_KEY            INTEGER          NOT NULL,
    EDM_ACCT_KEY                INTEGER          NOT NULL,
    REBATE_DT                   TIMESTAMP        NULL,
    YEAR                        NUMBER(38,0)     NULL,
    PERIOD                      NUMBER(38,0)     NULL,
    FIN_ID                      VARCHAR2(100)    NULL,
    WEX_ACCT_NBR                VARCHAR2(30)     NULL,
    PAYOUT_TYPE                 VARCHAR2(100)    NULL,
    MANUAL_REBATE_AMOUNT        NUMBER(20,2)     NULL,
    EDM_SOURCE_SYS              VARCHAR2(100)    NOT NULL
)
TABLESPACE D_BI
NOLOGGING
NOMONITORING
PARALLEL
;


