--
-- from ETL
--
SELECT
  SUM (x.TRANSACTION_LINE_ITEM_COUNT1) AS item_cnt,
  s.BUSINESS_SEGMENT_ID                AS seg
FROM D_DATE d
JOIN F_MONTHLY_REVENUE_SNAPSHOT    x ON (d.DATE_KEY = x.REVENUE_DATE_KEY)
JOIN D_ACCOUNT                     a ON (x.ACCOUNT_KEY = a.ACCOUNT_KEY)
JOIN F_BUSINESS_SEGMENT_ACCT_EVENT e ON (a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY)
JOIN D_BUSINESS_SEGMENT            s ON (e.BUSINESS_SEGMENT_KEY = s.BUSINESS_SEGMENT_KEY)
WHERE d.MONTH_YEAR_ABBR IN ('2013-06')
AND (a.account_hist_key = 487 AND a.account_key = 8459339)
GROUP BY s.BUSINESS_SEGMENT_ID
ORDER BY 2
;
/* hist_key, key, count
487	  8459339	18
3451	9045226	6
5141	9200347	7
6776	8953763	9
9124	8375492	2
13808	9171730	3
14422	9131655	2
15431	9131664	2
*/
/*
2963468	BRANDED UNIVERSAL
3133613	FLEET BILLED COBRAND
4292072	GOVERNMENT
511185	PAC PRIDE CUSTOM NETWORK
5065202	PARTNER BILLED COBRAND
3794138	PRIVATE LABEL
7602585	WEX81+
5852257	WEX<=80
*/
--
-- from BO
--
SELECT
  sum(x.TRANSACTION_LINE_ITEM_COUNT1) item_cnt,
  s.BUSINESS_SEGMENT_NAME             seg
FROM D_REVENUE_DATE_VW d
JOIN F_MONTHLY_REVENUE_SNAPSHOT x ON (d.REV_DATE_KEY=x.REVENUE_DATE_KEY)
JOIN D_ACCOUNT_HIST_VW          a ON (x.ACCOUNT_KEY=a.ACCOUNT_KEY)
JOIN ( 
  select account_hist_key, business_segment_key from (
    select 
      account_hist_key,
      business_segment_key, 
      row_last_mod_dttm,
      row_number() over (partition by account_hist_key order by row_last_mod_dttm desc) rn
    from f_business_segment_acct_event
  )
  where rn=1
  ) e ON (e.ACCOUNT_HIST_KEY=a.ACCOUNT_HIST_KEY)
JOIN D_BUSINESS_SEGMENT         s ON (e.BUSINESS_SEGMENT_KEY=s.BUSINESS_SEGMENT_KEY)
WHERE d.REV_MONTH_YEAR_ABBR IN ('2013-06')
AND (a.account_hist_key = 487 AND a.account_key = 8459339)
ORDER BY 2
;
/*
1935051	BRANDED UNIVERSAL
2248385	FLEET BILLED COBRAND
1721561	GOVERNMENT
454558	PAC PRIDE CUSTOM NETWORK
5038072	PARTNER BILLED COBRAND
3249808	PRIVATE LABEL
3902237	WEX81+
4275575	WEX<=80
*/
SELECT  
  SUM (u.TRANSACTION_LINE_ITEM_COUNT1) AS item_cnt,
  u.BUSINESS_SEGMENT_ID                AS seg
FROM (
  SELECT 
    x.TRANSACTION_LINE_ITEM_COUNT1,
    s.BUSINESS_SEGMENT_ID,
    RANK () OVER (
      PARTITION BY e.ACCOUNT_HIST_KEY
      ORDER BY e.ROW_LAST_MOD_DTTM DESC
    ) RN
  FROM 
    F_MONTHLY_REVENUE_SNAPSHOT x,
    D_DATE d,
    D_ACCOUNT a,
    D_BUSINESS_SEGMENT s,
    F_BUSINESS_SEGMENT_ACCT_EVENT e
  WHERE
    x.ACCOUNT_KEY = a.ACCOUNT_KEY
    AND a.ACCOUNT_HIST_KEY = e.ACCOUNT_HIST_KEY
    AND e.BUSINESS_SEGMENT_KEY = s.BUSINESS_SEGMENT_KEY
    AND d.DATE_KEY = x.REVENUE_DATE_KEY
    AND d.CALENDAR_DATE_DT =  TO_DATE ('06/30/2013', 'MM/DD/YYYY')
) u
WHERE u.RN = 1
GROUP BY u.BUSINESS_SEGMENT_ID 
ORDER BY 2;
