create table x_jk(c1 integer, c2 varchar2(20), dt date) tablespace d_bi;

insert into x_jk(1, 'ABC', sysdate-1);
insert into x_jk(2, 'DEF', sysdate);
insert into x_jk(3, 'GHI', sysdate+1);
