#
# PurchaseDevice - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'purchase_device'

def self.test_order
    :alpha
end

describe PurchaseDevice do
    before do
        @d = PurchaseDevice.new
    end

    it '1 wipe subject' do
        @d.wipe_subject
        @d.execute("select * from #{@d.tname}").result.must_equal []
    end

    it '2 create fixtures' do
        P_NM  = 'pd_spec_before'
        TD    = 3600*4
        KEY   = 1000
        RANGE = 1..5

        t0    = Time.now
        t1    = t0+TD

        fix  = {
            AUDIT_TEST_KEY:            0,
            AUDIT_TEST_COUNT1:         1,
            ISSUED_DATE:               t0-TD*4,
            ACTIVATED_COUNT1:          1,
            ACTIVATED_DATE:            t0-TD*3,
            TERMINATED_COUNT1:         1,
            TERMINATED_DATE:           t0-TD*2,
            IN_TRANSITION_COUNT1:      1,
            IN_TRANSITION_DATE:        t0+TD*2,
            SUSPENDED_COUNT1:          1,
            SUSPENDED_DATE:            t0+TD*4,
            CURRENT_RECORD_FLG:        1,
            EMBOSSED_CARD_NUMBER_ID:   'XXXX',
            TIED_TO_AN_ASSET_FLG:      1,
            CARD_STATUS_CODE:          'ACTD',
            CARD_STATUS_DESC:          'Activated',
            CARD_STATUS_DATE:          t0-TD,
            TIED_TO_A_PERSON_FLG:      0,
            TIED_TO_A_SITE_FLG:        0,
            MAGSTRIPE_DEVICE_COUNT1:   1,
            VIRTUAL_DEVICE_COUNT1:     1,
            ROW_EFF_BEGIN_DTTM:        t0-TD*6,
            ROW_EFF_END_DTTM:          t0+TD*6,
            ROW_CREATE_DTTM:           t0,
            ROW_LAST_MOD_DTTM:         t0,
            ROW_LAST_MOD_PROC_NM:      P_NM,
            ROW_LAST_MOD_PROC_SEQ_NBR: 0,
            ROW_MD5_CHECKSUM_HEX:      '0000'
        }

        RANGE.each do |i|
            k = KEY+i
            fix[:AUDIT_TEST_KEY]            = k
            fix[:EMBOSSED_CARD_NUMBER_ID]   = k.to_s
            fix[:ROW_LAST_MOD_PROC_SEQ_NBR] = i
                    
            @d.add_fixture(fix)
        end
        RANGE.each do |i|
            k = KEY+i
            @d.find_by_key(k)[0].must_equal k
        end
    end
end

__END__




