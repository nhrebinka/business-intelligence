#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

def self.test_order
    :alpha
end

DESC = %w(
sum(AGG.INTCHG_ALL_OTHER_AMT)
sum(AGG.TAX_EXCISE_CITY_AMT) 
sum(AGG.TAX_SALES_CITY_AMT) 
sum(AGG.TAX_SPECIAL_CITY_AMT)
sum(AGG.TAX_EXCISE_COUNTY_AMT)
sum(AGG.TAX_SALES_COUNTY_AMT) 
sum(AGG.TAX_SPECIAL_COUNTY_AMT) 
sum(AGG.FEE_CURRENCY_CONVERSION_AMT) 
sum(AGG.DISCOUNT_AMT) 
sum(AGG.TAX_EXCISE_FEDERAL_AMT) 
sum(AGG.TAX_FEDERAL_AMT) 
sum(AGG.INTCHG_FUEL_ONLY_AMT)
sum(AGG.GROSS_NON_REVENUE_AMT)
sum(AGG.GROSS_REVENUE_AMT) 
sum(AGG.GROSS_SPEND_AMT) 
sum(AGG.GROSS_SPEND_LOCAL_AMT) 
sum(AGG.INTCHG_FUEL_DIESEL_AMT)
sum(AGG.INTCHG_FUEL_GAS_AMT) 
sum(AGG.INTCHG_FUEL_OTHER_AMT) 
sum(AGG.INTCHG_LINE_ITEM_FLAT_FEE_AMT) 
sum(AGG.INTCHG_PER_UNIT_FEE_AMT)
sum(AGG.INTCHG_RATE_ACTUAL_AMT) 
sum(AGG.INTCHG_TOTAL_AMT) 
sum(AGG.INTCHG_TRANS_FLAT_FEE_AMT) 
sum(AGG.TAX_LOCAL_AMT) 
sum(AGG.INTCHG_MAINT_ONLY_AMT)
sum(AGG.NBR_OF_UNITS_PURCHASED_COUNT)
sum(AGG.NET_REVENUE_AMT)
sum(AGG.tax_NON_FEDERAL_AMT)
sum(AGG.PURCHASE_ITEM_AMT)
sum(AGG.PURCHASE_ITEM_LOCAL_AMT) 
sum(AGG.SPEND_FUEL_DIESEL_AMT)
sum(AGG.SPEND_FUEL_DIESEL_LOCAL_AMT)
sum(AGG.SPEND_FUEL_GAS_AMT)
sum(AGG.SPEND_FUEL_GAS_LOCAL_AMT)
sum(AGG.SPEND_FUEL_ONLY_AMT) 
sum(AGG.SPEND_FUEL_ONLY_LOCAL_AMT)
sum(AGG.SPEND_FUEL_OTHER_AMT) 
sum(AGG.SPEND_FUEL_OTHER_LOCAL_AMT)
sum(AGG.SPEND_MAINT_AMT) 
sum(AGG.SPEND_MAINT_LOCAL_AMT) 
sum(AGG.SPEND_OTHER_AMT) 
sum(AGG.SPEND_OTHER_LOCAL_AMT) 
sum(AGG.tax_excise_state_AMT) 
sum(AGG.tax_sales_state_AMT)
sum(AGG.TAX_SPEcial_STATE_AMT) 
sum(AGG.tax_STATE_AMT) 
sum(AGG.TAX_EXEMPT_SPEND_AMT) 
sum(AGG.TAX_EXEMPT_SPEND_LOCAL_AMT)
sum(AGG.tax_TOTAL_AMT) 
sum(AGG.tax_TOTAL_LOCAL_AMT)
sum(AGG.TRANS_LINE_ITEM_COUNT)
)

describe 'Site Agg - Monthly Sum for Program by a Revenue Date' do
    def _diff(a, b, exp={})
        aa = a.split('|')
        ba = b.split('|')
        (0...aa.size).each do |i|
            if exp[i]
                assert \
                    (aa[i].to_f-ba[i].to_f).abs < exp[i], 
                    DESC[i]+" (#{aa[i]}|#{ba[i]})>#{exp[i]}"
            else
                assert_equal aa[i]||'', ba[i]||'', DESC[i]
            end
        end
        true
    end
    def _validate(qlist)
        rst = qlist.map do |s|
            a = []
            Oracle.do(s) {|r| a << r.join('|') }
            a.join("\n")
        end
        _diff(rst[0], rst[1])
        _diff(rst[0], rst[2])
        _diff(rst[0], rst[3])
    end

    it '6A Sum All Columns for 1 month by revenue date' do
        _validate([
"
select
sum(F.ALL_OTHER_INTERCHANGE_AMOUNT), 
sum(F.CITY_EXCISE_TAX_AMOUNT), 
sum(F.CITY_SALES_TAX_AMOUNT), 
sum(F.CITY_SPECIAL_TAX_AMOUNT), 
sum(F.COUNTY_EXCISE_TAX_AMOUNT), 
sum(F.COUNTY_SALES_TAX_AMOUNT), 
sum(F.COUNTY_SPECIAL_TAX_AMOUNT), 
sum(F.CURRENCY_CONVERSION_FEE_AMOUNT), 
sum(F.DISCOUNT_AMOUNT), 
sum(F.FEDERAL_EXCISE_TAX_AMOUNT), 
sum(F.FEDERAL_TAX_AMOUNT), 
sum(F.FUEL_ONLY_INTERCHANGE_AMOUNT),
sum(F.GROSS_NON_REVENUE_AMOUNT),
sum(F.GROSS_REVENUE_AMOUNT), 
sum(F.GROSS_SPEND_AMOUNT), 
sum(F.GROSS_SPEND_LOCAL_AMOUNT), 
sum(F.INTERCHANGE_FUEL_DIESEL_AMT),
sum(F.INTERCHANGE_FUEL_GAS_AMT), 
sum(F.INTERCHANGE_FUEL_OTHER_AMT), 
sum(F.INTCHG_LINE_ITEM_FLAT_FEE_AMT), 
sum(F.INTERCHANGE_PER_UNIT_FEE_AMT),
sum(F.INTERCHANGE_RATE_ACTUAL_AMOUNT), 
sum(F.INTERCHANGE_TOTAL_AMOUNT), 
sum(F.INTCHG_TRANS_FLAT_FEE_AMT), 
sum(F.LOCAL_TAX_AMOUNT), 
sum(F.MAINTENANCE_ONLY_INTCHG_AMOUNT),
sum(F.NBR_OF_UNITS_PURCHASED_COUNT),
sum(F.NET_REVENUE_AMOUNT),
sum(F.NON_FEDERAL_TAX_AMOUNT),
sum(F.PURCHASE_ITEM_AMOUNT),
sum(F.PURCHASE_ITEM_LOCAL_AMOUNT), 
sum(F.SPEND_FUEL_DIESEL_AMOUNT),
sum(F.SPEND_FUEL_DIESEL_LOCAL_AMOUNT),
sum(F.SPEND_FUEL_GAS_AMOUNT),
sum(F.SPEND_FUEL_GAS_LOCAL_AMOUNT),
sum(F.SPEND_FUEL_ONLY_AMOUNT), 
sum(F.SPEND_FUEL_ONLY_LOCAL_AMOUNT),
sum(F.SPEND_FUEL_OTHER_AMOUNT), 
sum(F.SPEND_FUEL_OTHER_LOCAL_AMOUNT),
sum(F.SPEND_MAINTENANCE_AMOUNT), 
sum(F.SPEND_MAINTENANCE_LOCAL_AMOUNT), 
sum(F.SPEND_OTHER_AMOUNT), 
sum(F.SPEND_OTHER_LOCAL_AMOUNT), 
sum(F.STATE_EXCISE_TAX_AMOUNT), 
sum(F.STATE_SALES_TAX_AMOUNT),
sum(F.STATE_SPECIAL_TAX_AMOUNT), 
sum(F.STATE_TAX_AMOUNT), 
sum(F.TAX_EXEMPT_SPEND_AMOUNT), 
sum(F.TAX_EXEMPT_SPEND_LOCAL_AMOUNT),
sum(F.TOTAL_TAX_AMOUNT), 
sum(F.TOTAL_TAX_LOCAL_AMOUNT),
sum(F.TRANSACTION_LINE_ITEM_COUNT1)
from edw_owner.F_TRANSACTION_LINE_ITEM  PARTITION (YM_2013_02) F,  EDW_OWNER.D_DATE dt
where F.revenue_date_key = DT.DATE_KEY
","
select
sum(AGG.INTCHG_ALL_OTHER_AMT), 
sum(AGG.TAX_EXCISE_CITY_AMT), 
sum(AGG.TAX_SALES_CITY_AMT), 
sum(AGG.TAX_SPECIAL_CITY_AMT), 
 sum(AGG.TAX_EXCISE_COUNTY_AMT), 
 sum(AGG.TAX_SALES_COUNTY_AMT), 
 sum(AGG.TAX_SPECIAL_COUNTY_AMT), 
 sum(AGG.FEE_CURRENCY_CONVERSION_AMT), 
 sum(AGG.DISCOUNT_AMT), 
sum(AGG.TAX_EXCISE_FEDERAL_AMT), 
sum(AGG.TAX_FEDERAL_AMT), 
sum(AGG.INTCHG_FUEL_ONLY_AMT),
sum(AGG.GROSS_NON_REVENUE_AMT),
sum(AGG.GROSS_REVENUE_AMT), 
sum(AGG.GROSS_SPEND_AMT), 
sum(AGG.GROSS_SPEND_LOCAL_AMT), 
sum(AGG.INTCHG_FUEL_DIESEL_AMT),
sum(AGG.INTCHG_FUEL_GAS_AMT), 
sum(AGG.INTCHG_FUEL_OTHER_AMT), 
sum(AGG.INTCHG_LINE_ITEM_FLAT_FEE_AMT), 
sum(AGG.INTCHG_PER_UNIT_FEE_AMT),
sum(AGG.INTCHG_RATE_ACTUAL_AMT), 
sum(AGG.INTCHG_TOTAL_AMT), 
sum(AGG.INTCHG_TRANS_FLAT_FEE_AMT), --here
sum(AGG.TAX_LOCAL_AMT), 
sum(AGG.INTCHG_MAINT_ONLY_AMT),
sum(AGG.NBR_OF_UNITS_PURCHASED_COUNT),
sum(AGG.NET_REVENUE_AMT),
sum(AGG.tax_NON_FEDERAL_AMT),
sum(AGG.PURCHASE_ITEM_AMT),
sum(AGG.PURCHASE_ITEM_LOCAL_AMT), 
sum(AGG.SPEND_FUEL_DIESEL_AMT),
sum(AGG.SPEND_FUEL_DIESEL_LOCAL_AMT),
sum(AGG.SPEND_FUEL_GAS_AMT),
sum(AGG.SPEND_FUEL_GAS_LOCAL_AMT),
sum(AGG.SPEND_FUEL_ONLY_AMT), 
sum(AGG.SPEND_FUEL_ONLY_LOCAL_AMT),
sum(AGG.SPEND_FUEL_OTHER_AMT), 
sum(AGG.SPEND_FUEL_OTHER_LOCAL_AMT),
sum(AGG.SPEND_MAINT_AMT), 
sum(AGG.SPEND_MAINT_LOCAL_AMT), 
sum(AGG.SPEND_OTHER_AMT), 
sum(AGG.SPEND_OTHER_LOCAL_AMT), 
sum(AGG.tax_excise_state_AMT), 
sum(AGG.tax_sales_state_AMT),
sum(AGG.TAX_SPECIAL_STATE_AMT), 
sum(AGG.tax_STATE_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_LOCAL_AMT),
sum(AGG.tax_TOTAL_AMT), 
sum(AGG.tax_TOTAL_LOCAL_AMT),
sum(AGG.TRANS_LINE_ITEM_COUNT)
from  EDW_OWNER.X_D_SITE_B    AGG,  EDW_OWNER.D_DATE dt
where AGG.revenue_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT between  '01-feb-2013' and '28-feb-2013'
","
select
sum(AGG.INTCHG_ALL_OTHER_AMT), 
sum(AGG.TAX_EXCISE_CITY_AMT), 
sum(AGG.TAX_SALES_CITY_AMT), 
sum(AGG.TAX_SPECIAL_CITY_AMT), 
sum(AGG.TAX_EXCISE_COUNTY_AMT), 
sum(AGG.TAX_SALES_COUNTY_AMT), 
sum(AGG.TAX_SPECIAL_COUNTY_AMT), 
sum(AGG.FEE_CURRENCY_CONVERSION_AMT), 
sum(AGG.DISCOUNT_AMT), 
sum(AGG.TAX_EXCISE_FEDERAL_AMT), 
sum(AGG.TAX_FEDERAL_AMT), 
sum(AGG.INTCHG_FUEL_ONLY_AMT),
sum(AGG.GROSS_NON_REVENUE_AMT),
sum(AGG.GROSS_REVENUE_AMT), 
sum(AGG.GROSS_SPEND_AMT), 
sum(AGG.GROSS_SPEND_LOCAL_AMT), 
sum(AGG.INTCHG_FUEL_DIESEL_AMT),
sum(AGG.INTCHG_FUEL_GAS_AMT), 
sum(AGG.INTCHG_FUEL_OTHER_AMT), 
sum(AGG.INTCHG_LINE_ITEM_FLAT_FEE_AMT), 
sum(AGG.INTCHG_PER_UNIT_FEE_AMT),
sum(AGG.INTCHG_RATE_ACTUAL_AMT), 
sum(AGG.INTCHG_TOTAL_AMT), 
sum(AGG.INTCHG_TRANS_FLAT_FEE_AMT), --here
sum(AGG.TAX_LOCAL_AMT), 
sum(AGG.INTCHG_MAINT_ONLY_AMT),
sum(AGG.NBR_OF_UNITS_PURCHASED_COUNT),
sum(AGG.NET_REVENUE_AMT),
sum(AGG.tax_NON_FEDERAL_AMT),
sum(AGG.PURCHASE_ITEM_AMT),
sum(AGG.PURCHASE_ITEM_LOCAL_AMT), 
sum(AGG.SPEND_FUEL_DIESEL_AMT),
sum(AGG.SPEND_FUEL_DIESEL_LOCAL_AMT),
sum(AGG.SPEND_FUEL_GAS_AMT),
sum(AGG.SPEND_FUEL_GAS_LOCAL_AMT),
sum(AGG.SPEND_FUEL_ONLY_AMT), 
sum(AGG.SPEND_FUEL_ONLY_LOCAL_AMT),
sum(AGG.SPEND_FUEL_OTHER_AMT), 
sum(AGG.SPEND_FUEL_OTHER_LOCAL_AMT),
sum(AGG.SPEND_MAINT_AMT), 
sum(AGG.SPEND_MAINT_LOCAL_AMT), 
sum(AGG.SPEND_OTHER_AMT), 
sum(AGG.SPEND_OTHER_LOCAL_AMT), 
sum(AGG.tax_excise_state_AMT), 
sum(AGG.tax_sales_state_AMT),
sum(AGG.TAX_SPECIAL_STATE_AMT), 
sum(AGG.tax_STATE_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_LOCAL_AMT),
sum(AGG.tax_TOTAL_AMT), 
sum(AGG.tax_TOTAL_LOCAL_AMT),
sum(AGG.TRANS_LINE_ITEM_COUNT)
from  EDW_OWNER.X_D_SITE_B    AGG,  EDW_OWNER.D_DATE dt
where AGG.revenue_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT between  '01-feb-2013' and '28-feb-2013'
","
select
sum(AGG.INTCHG_ALL_OTHER_AMT), 
sum(AGG.TAX_EXCISE_CITY_AMT), 
sum(AGG.TAX_SALES_CITY_AMT), 
sum(AGG.TAX_SPECIAL_CITY_AMT), 
sum(AGG.TAX_EXCISE_COUNTY_AMT), 
sum(AGG.TAX_SALES_COUNTY_AMT), 
sum(AGG.TAX_SPECIAL_COUNTY_AMT), 
sum(AGG.FEE_CURRENCY_CONVERSION_AMT), 
sum(AGG.DISCOUNT_AMT), 
sum(AGG.TAX_EXCISE_FEDERAL_AMT), 
sum(AGG.TAX_FEDERAL_AMT), 
sum(AGG.INTCHG_FUEL_ONLY_AMT),
sum(AGG.GROSS_NON_REVENUE_AMT),
sum(AGG.GROSS_REVENUE_AMT), 
sum(AGG.GROSS_SPEND_AMT), 
sum(AGG.GROSS_SPEND_LOCAL_AMT), 
sum(AGG.INTCHG_FUEL_DIESEL_AMT),
sum(AGG.INTCHG_FUEL_GAS_AMT), 
sum(AGG.INTCHG_FUEL_OTHER_AMT), 
sum(AGG.INTCHG_LINE_ITEM_FLAT_FEE_AMT), 
sum(AGG.INTCHG_PER_UNIT_FEE_AMT),
sum(AGG.INTCHG_RATE_ACTUAL_AMT), 
sum(AGG.INTCHG_TOTAL_AMT), 
sum(AGG.INTCHG_TRANS_FLAT_FEE_AMT), --here
sum(AGG.TAX_LOCAL_AMT), 
sum(AGG.INTCHG_MAINT_ONLY_AMT),
sum(AGG.NBR_OF_UNITS_PURCHASED_COUNT),
sum(AGG.NET_REVENUE_AMT),
sum(AGG.tax_NON_FEDERAL_AMT),
sum(AGG.PURCHASE_ITEM_AMT),
sum(AGG.PURCHASE_ITEM_LOCAL_AMT), 
sum(AGG.SPEND_FUEL_DIESEL_AMT),
sum(AGG.SPEND_FUEL_DIESEL_LOCAL_AMT),
sum(AGG.SPEND_FUEL_GAS_AMT),
sum(AGG.SPEND_FUEL_GAS_LOCAL_AMT),
sum(AGG.SPEND_FUEL_ONLY_AMT), 
sum(AGG.SPEND_FUEL_ONLY_LOCAL_AMT),
sum(AGG.SPEND_FUEL_OTHER_AMT), 
sum(AGG.SPEND_FUEL_OTHER_LOCAL_AMT),
sum(AGG.SPEND_MAINT_AMT), 
sum(AGG.SPEND_MAINT_LOCAL_AMT), 
sum(AGG.SPEND_OTHER_AMT), 
sum(AGG.SPEND_OTHER_LOCAL_AMT), 
sum(AGG.tax_excise_state_AMT), 
sum(AGG.tax_sales_state_AMT),
sum(AGG.TAX_SPEcial_STATE_AMT), 
sum(AGG.tax_STATE_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_AMT), 
sum(AGG.TAX_EXEMPT_SPEND_LOCAL_AMT),
sum(AGG.tax_TOTAL_AMT), 
sum(AGG.tax_TOTAL_LOCAL_AMT),
sum(AGG.TRANS_LINE_ITEM_COUNT)
from  EDW_OWNER.F_DAILY_SITE_SNAPSHOT_VW  AGG,  EDW_OWNER.D_DATE dt
where AGG.revenue_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT between  '01-feb-2013' and '28-feb-2013'
"
       ])
    end
end

__END__




