#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

Vec = Struct.new(:b, :e, :p)

HINT  = 'parallel(f, 64)'
TESTS = [
    Vec.new('01-feb-2013', '28-feb-2013', 'YM_2013_02'),
    Vec.new('01-feb-2014', '28-feb-2014', 'YM_2014_02'),
    Vec.new('01-oct-2013', '31-oct-2013', 'YM_2013_10'),
]

def self.test_order
  :alpha
end

describe 'Site Agg - Monthly Tests' do
  def _validate(qlist)
    rst = qlist.map do |s|
      a = []
      Oracle.do(s) {|r| a << r.join('|') }
      a.join("\n")
    end
    assert_equal rst[0], rst[1], "1vs2"
    assert_equal rst[0], rst[2], "1vs3"
  end

  it '1A monthly total' do
    TESTS.each do |t|
      _validate([
"
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMT)
from  edw_owner.X_D_SITE_B f, edw_owner.D_DATE dt
where f.REVENUE_date_key = dt.DATE_KEY
  and dt.CALENDAR_DATE_DT between '#{t.b}' and '#{t.e}'
", "
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMOUNT)
from edw_owner.F_TRANSACTION_LINE_ITEM PARTITION (#{t.p}) f,  edw_owner.D_DATE dt
where f.REVENUE_date_key = dt.DATE_KEY
  and dt.CALENDAR_DATE_DT between '#{t.b}' and '#{t.e}'
", "
select /*+ #{HINT} */
  sum(f.GROSS_SPEND_AMT)
from  f_daily_site_snapshot_vw f,  edw_owner.D_DATE dt
where f.REVENUE_date_key = DT.DATE_KEY
  and dt.CALENDAR_DATE_DT between  '#{t.b}' and '#{t.e}'
"
      ])
    end
  end
end

__END__




