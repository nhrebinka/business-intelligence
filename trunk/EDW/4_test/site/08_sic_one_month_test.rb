#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

def self.test_order
    :alpha
end

DESC = %w(
sum(f.gross_spend_amount)
fn.sic_key
)

describe 'Site Agg - SIC for one Month' do
    def _diff(a, b, exp={})
        aa = a.split('|')
        ba = b.split('|')
        (0...aa.size).each do |i|
            if exp[i]
                assert \
                    (aa[i].to_f-ba[i].to_f).abs < exp[i], 
                    "#{DESC[i]} (#{aa[i]}|#{ba[i]})>#{exp[i]}"
            else
                assert_equal aa[i]||'', ba[i]||'', DESC[i]
            end
        end
        true
    end
    def _validate(qlist)
        rst = qlist.map do |s|
            a = []
            Oracle.do(s) {|r| a << r.join('|') }
            a.join("\n")
        end
        _diff(rst[0], rst[1])
        _diff(rst[0], rst[2])
    end

    it '8A validate SIC for one Month' do
        _validate([
"
select sum(f.gross_spend_amount), fn.sic_key
from edw_owner.F_TRANSACTION_LINE_ITEM  PARTITION (YM_2013_02) F, 
 EDW_OWNER.D_DATE dt, EDW_OWNER.D_ACCOUNT ACCT,
 EDW_OWNER.F_ACCOUNT_NAICS_SIC_EVENT FN
 WHERE F.PURCHASE_ACCOUNT_KEY = ACCT.ACCOUNT_KEY
AND  FN.ACCOUNT_HIST_KEY = ACCT.ACCOUNT_HIST_KEY
AND  F.revenue_date_key = DT.DATE_KEY
and sic_key=180
group by  fn.sic_key
","
select sum(agg.gross_spend_amt), agg.sic_key
from edw_owner.X_D_SITE_B AGG,  edw_owner.d_date dt
 WHERE AGG.revenue_date_key = DT.DATE_KEY
AND DT.CALENDAR_DATE_DT BETWEEN '01-FEB-2013' AND '28-feb-2013'
and agg.sic_key=180
group by  agg.sic_key
","
select sum(agg.gross_spend_amt), agg.sic_key
from EDW_OWNER.F_DAILY_SITE_SNAPSHOT_VW AGG,  edw_owner.d_date dt
 WHERE AGG.revenue_date_key = DT.DATE_KEY
AND DT.CALENDAR_DATE_DT BETWEEN '01-FEB-2013' AND '28-feb-2013'
and agg.sic_key=180
group by  agg.sic_key

"
       ])
    end
end

__END__




