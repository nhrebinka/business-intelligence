#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

Vec = Struct.new(:d, :k, :p)

HINT  = 'parallel 64'
TLIST = [
    Vec.new('12-feb-2013', 227, 'YM_2013_02')
]

def self.test_order
    :alpha
end

DESC = %w(
sum(f.INTCHG_ALL_OTHER_AMT)
sum(f.TAX_EXCISE_CITY_AMT) 
sum(f.TAX_SALES_CITY_AMT) 
sum(f.TAX_SPECIAL_CITY_AMT)
sum(f.TAX_EXCISE_COUNTY_AMT)
sum(f.TAX_SALES_COUNTY_AMT) 
sum(f.TAX_SPECIAL_COUNTY_AMT) 
sum(f.FEE_CURRENCY_CONVERSION_AMT) 
sum(f.DISCOUNT_AMT) 
sum(f.TAX_EXCISE_FEDERAL_AMT) 
sum(f.TAX_FEDERAL_AMT) 
sum(f.INTCHG_FUEL_ONLY_AMT)
sum(f.GROSS_NON_REVENUE_AMT)
sum(f.GROSS_REVENUE_AMT) 
sum(f.GROSS_SPEND_AMT) 
sum(f.GROSS_SPEND_LOCAL_AMT) 
sum(f.INTCHG_FUEL_DIESEL_AMT)
sum(f.INTCHG_FUEL_GAS_AMT) 
sum(f.INTCHG_FUEL_OTHER_AMT) 
sum(f.INTCHG_LINE_ITEM_FLAT_FEE_AMT) 
sum(f.INTCHG_PER_UNIT_FEE_AMT)
sum(f.INTCHG_RATE_ACTUAL_AMT) 
sum(f.INTCHG_TOTAL_AMT) 
sum(f.INTCHG_TRANS_FLAT_FEE_AMT) 
sum(f.TAX_LOCAL_AMT) 
sum(f.INTCHG_MAINT_ONLY_AMT)
sum(f.NBR_OF_UNITS_PURCHASED_COUNT)
sum(f.NET_REVENUE_AMT)
sum(f.TAX_NON_FEDERAL_AMT)
sum(f.PURCHASE_ITEM_AMT)
sum(f.PURCHASE_ITEM_LOCAL_AMT) 
sum(f.SPEND_FUEL_DIESEL_AMT)
sum(f.SPEND_FUEL_DIESEL_LOCAL_AMT)
sum(f.SPEND_FUEL_GAS_AMT)
sum(f.SPEND_FUEL_GAS_LOCAL_AMT)
sum(f.SPEND_FUEL_ONLY_AMT) 
sum(f.SPEND_FUEL_ONLY_LOCAL_AMT)
sum(f.SPEND_FUEL_OTHER_AMT) 
sum(f.SPEND_FUEL_OTHER_LOCAL_AMT)
sum(f.SPEND_MAINT_AMT) 
sum(f.SPEND_MAINT_LOCAL_AMT) 
sum(f.SPEND_OTHER_AMT) 
sum(f.SPEND_OTHER_LOCAL_AMT) 
sum(f.TAX_EXCISE_STATE_AMT) 
sum(f.TAX_SALES_STATE_AMT)
sum(f.TAX_SPECIAL_STATE_AMT) 
sum(f.TAX_STATE_AMT) 
sum(f.TAX_EXEMPT_SPEND_AMT) 
sum(f.TAX_EXEMPT_SPEND_LOCAL_AMT)
sum(f.TAX_TOTAL_AMT) 
sum(f.TAX_TOTAL_LOCAL_AMT)
sum(f.TRANS_LINE_ITEM_COUNT)
)

describe 'Site Agg - Sum for Program for a Revenue Day' do
    def _diff(a, b, exp={})
        aa = a.split('|')
        ba = b.split('|')
        (0...aa.size).each do |i|
            if exp[i]
                assert \
                    (aa[i].to_f-ba[i].to_f).abs < exp[i], 
                    "#{DESC[i]} (#{aa[i]}|#{ba[i]})>#{exp[i]}"
            else
                assert_equal aa[i]||'', ba[i]||'', DESC[i]
            end
        end
        true
    end
    def _validate(sql3)
        rst = sql3.map do |s|
            a = []
            Oracle.do(s) {|r| a << r.join('|') }
            a.join("\n")
        end
        _diff(rst[0], rst[1], {26=>100.0})
        _diff(rst[0], rst[2], {26=>100.0})
    end

    it '4A validate one site' do
        flist = DESC.join(',')
        TLIST.each do |t|
            _validate([
"
select /*+ #{HINT} */
#{flist}
from edw_owner.X_D_SITE_B f, EDW_OWNER.D_DATE dt
where f.REVENUE_DATE_KEY = DT.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
","
select /*+ #{HINT} */
sum(f.ALL_OTHER_INTERCHANGE_AMOUNT), 
sum(f.CITY_EXCISE_TAX_AMOUNT), 
sum(f.CITY_SALES_TAX_AMOUNT), 
sum(f.CITY_SPECIAL_TAX_AMOUNT), 
sum(f.COUNTY_EXCISE_TAX_AMOUNT), 
sum(f.COUNTY_SALES_TAX_AMOUNT), 
sum(f.COUNTY_SPECIAL_TAX_AMOUNT), 
sum(f.CURRENCY_CONVERSION_FEE_AMOUNT), 
sum(f.DISCOUNT_AMOUNT), 
sum(f.FEDERAL_EXCISE_TAX_AMOUNT), 
sum(f.FEDERAL_TAX_AMOUNT), 
sum(f.FUEL_ONLY_INTERCHANGE_AMOUNT),
sum(f.GROSS_NON_REVENUE_AMOUNT),
sum(f.GROSS_REVENUE_AMOUNT), 
sum(f.GROSS_SPEND_AMOUNT), 
sum(f.GROSS_SPEND_LOCAL_AMOUNT), 
sum(f.INTERCHANGE_FUEL_DIESEL_AMT),
sum(f.INTERCHANGE_FUEL_GAS_AMT), 
sum(f.INTERCHANGE_FUEL_OTHER_AMT), 
sum(f.INTCHG_LINE_ITEM_FLAT_FEE_AMT), 
sum(f.INTERCHANGE_PER_UNIT_FEE_AMT),
sum(f.INTERCHANGE_RATE_ACTUAL_AMOUNT), 
sum(f.INTERCHANGE_TOTAL_AMOUNT), 
sum(f.INTCHG_TRANS_FLAT_FEE_AMT), 
sum(f.LOCAL_TAX_AMOUNT), 
sum(f.MAINTENANCE_ONLY_INTCHG_AMOUNT),
sum(f.NBR_OF_UNITS_PURCHASED_COUNT),
sum(f.NET_REVENUE_AMOUNT),
sum(f.NON_FEDERAL_TAX_AMOUNT),
sum(f.PURCHASE_ITEM_AMOUNT),
sum(f.PURCHASE_ITEM_LOCAL_AMOUNT), 
sum(f.SPEND_FUEL_DIESEL_AMOUNT),
sum(f.SPEND_FUEL_DIESEL_LOCAL_AMOUNT),
sum(f.SPEND_FUEL_GAS_AMOUNT),
sum(f.SPEND_FUEL_GAS_LOCAL_AMOUNT),
sum(f.SPEND_FUEL_ONLY_AMOUNT), 
sum(f.SPEND_FUEL_ONLY_LOCAL_AMOUNT),
sum(f.SPEND_FUEL_OTHER_AMOUNT), 
sum(f.SPEND_FUEL_OTHER_LOCAL_AMOUNT),
sum(f.SPEND_MAINTENANCE_AMOUNT), 
sum(f.SPEND_MAINTENANCE_LOCAL_AMOUNT), 
sum(f.SPEND_OTHER_AMOUNT), 
sum(f.SPEND_OTHER_LOCAL_AMOUNT), 
sum(f.STATE_EXCISE_TAX_AMOUNT), 
sum(f.STATE_SALES_TAX_AMOUNT),
sum(f.STATE_SPECIAL_TAX_AMOUNT), 
sum(f.STATE_TAX_AMOUNT), 
sum(f.TAX_EXEMPT_SPEND_AMOUNT), 
sum(f.TAX_EXEMPT_SPEND_LOCAL_AMOUNT),
sum(f.TOTAL_TAX_AMOUNT), 
sum(f.TOTAL_TAX_LOCAL_AMOUNT),
sum(f.TRANSACTION_LINE_ITEM_COUNT1)
from edw_owner.F_TRANSACTION_LINE_ITEM PARTITION (#{t.p}) f,  
     edw_owner.D_DATE dt
where f.REVENUE_DATE_KEY = DT.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
","
select /*+ #{HINT} */
#{flist}
from  EDW_OWNER.F_DAILY_SITE_SNAPSHOT_VW f,  EDW_OWNER.D_DATE dt
where f.REVENUE_DATE_KEY = DT.DATE_KEY
  and dt.CALENDAR_DATE_DT= '#{t.d}'
  and program_key = #{t.k}
"
           ])
        end
    end
end

__END__




