#
# SiteAgg - simple setup test to check whether Oracle connection is working
#
require 'minitest/autorun'
require 'oracle'

def self.test_order
    :alpha
end

describe 'Site Agg - Ticket Count 1 Site, 1 Month' do
    def _validate(qlist)
        rst = qlist.map do |s|
            a = []
            Oracle.do(s) {|r| a << r.join('|') }
            a.join("\n")
        end
        assert_equal rst[0], rst[1], "1vs2"
        assert_equal rst[0], rst[2], "1vs3"
    end

    it '12A Ticket Count 1 Site, 1 Month' do
        _validate([
"
select
sum( AGG.TRANS_TICKET_COUNT)
from  EDW_OWNER.X_D_SITE_B     AGG,  EDW_OWNER.D_DATE dt
where AGG.REVENUE_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT between  '01-oct-2013' and '31-oct-2013'
","
select
sum( f.TRANSACTION_LINE_ITEM_COUNT1)
from edw_owner.F_TRANSACTION_LINE_ITEM  PARTITION (YM_2013_10) F,  EDW_OWNER.D_DATE dt
where F.REVENUE_date_key = DT.DATE_KEY
and GROSS_SPEND_AMOUNT<>0 
AND WEX_TRANSACTION_ITEM_SEQ_NBR=1
","
select
sum( AGG.TRANS_TICKET_COUNT)
from EDW_OWNER.F_DAILY_SITE_SNAPSHOT_VW     AGG,  EDW_OWNER.D_DATE dt
where AGG.REVENUE_date_key = DT.DATE_KEY
and DT.CALENDAR_DATE_DT between  '01-oct-2013' and '31-oct-2013'
"
        ])
    end
end

__END__




