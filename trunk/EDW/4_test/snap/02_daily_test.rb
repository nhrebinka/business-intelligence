#
# PREDW vs PDEDW Tally validation - monthly
#
require 'minitest/autorun'
require 'oracle'

PD = [ ENV['DB_UID'].split('/'), ENV['DB_TNS'] ].flatten
PR = [ ENV['LDAP'].split('/'),   'PREDW'       ].flatten

HINT  = 'parallel(x, 64) full(x) use_hash(d, x)'
RANGE = "to_date('20080101','yyyymmdd') AND to_date('20140430','yyyymmdd')"

describe 'Monthly Tests - Account, ' do
  before do
    @d = Oracle.new(PD)
    @p = Oracle.new(PR)
  end

  after do
    @d.close
    @p.close
  end

  def _validate(qlist)
    qlist.each do |s| # walk through test cases one-by-one
      puts s
      a, b = [], []
      @d.run(s) {|r| a << r.join('|') if r }
      @p.run(s) {|r| b << r.join('|') if r }
      assert_equal a.join("\n"), b.join("\n")
    end
  end

  it '1A daily tally' do
    _validate([
"
select /*+ #{HINT} */
  d.MONTH_YEAR_ABBR,
  count(*),
  sum(x.OUTSTANDING_CARDS_COUNT),
  sum(x.MAX_OUTSTANDING_CARDS_COUNT)
from edw_owner.F_DAILY_ACCOUNT_SNAPSHOT x, edw_owner.D_DATE d
where x.DATE_KEY = d.DATE_KEY
  and d.CALENDAR_DATE_DT BETWEEN #{RANGE}
group by d.MONTH_YEAR_ABBR
order by 1
", "
select /*+ #{HINT} */
  d.MONTH_YEAR_ABBR,
  count(*),
  sum(x.GROSS_SPEND_AMOUNT),
  sum(x.PURCHASE_GALLONS_QTY)
from edw_owner.F_DAILY_REVENUE_SNAPSHOT x, edw_owner.D_DATE d
where x.REVENUE_DATE_KEY = d.DATE_KEY
  and d.CALENDAR_DATE_DT BETWEEN #{RANGE}
group by d.MONTH_YEAR_ABBR
order by 1
", "
select /*+ #{HINT} */
  d.MONTH_YEAR_ABBR,
  count(*),
  sum(x.GROSS_SPEND_AMOUNT),
  sum(x.PURCHASE_GALLONS_QTY)
from edw_owner.F_DAILY_AGING_SNAPSHOT x, edw_owner.D_DATE d
where x.REVENUE_DATE_KEY = d.DATE_KEY
  and d.CALENDAR_DATE_DT BETWEEN #{RANGE}
group by d.MONTH_YEAR_ABBR
order by 1
"
    ])
 end
end

__END__




