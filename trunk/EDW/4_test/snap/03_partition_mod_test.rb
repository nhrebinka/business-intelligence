#
# PREDW vs PDEDW Tally validation - fact
#
# Note:
#   Use PDEDW for DB Environmental parameters
#
require 'minitest/autorun'
require 'oracle'

PDEDW = [ ENV['DB_UID'].split('/'), ENV['DB_TNS'] ].flatten
PREDW = [ ENV['LDAP'].split('/'),   'PREDW'       ].flatten

SQL   = "
  select /*+ parallel 64 */
  '~PART~', row_last_mod_proc_nm, count(*)
  from edw_owner.f_transaction_line_item partition (~PART~)
  group by row_last_mod_proc_nm
  order by row_last_mod_proc_nm
"

describe 'Fact partition modification check' do
  before do
    @pd = Oracle.new(PDEDW)
    @pr = Oracle.new(PREDW)
  end

  after do
    @pd.close
    @pr.close
  end

  def _cmp(y, m)
    part = "ym_#{y}_#{'%02d' % m}"
    sql  = SQL.gsub(/~PART~/, part)

    STDERR.puts "#{part}"; STDERR.flush

    a, b = [], []
    @pd.run(sql) {|r| a << r.join('|') if r }
    @pr.run(sql) {|r| b << r.join('|') if r }

    assert_equal a.join("\n"), b.join("\n")
  end
    
  it '1A F_TRANSACTION_LINE_ITEM modification tally' do
    2013.downto(2008) do |y|
      (1..12).each do |m|
        _cmp(y, m)
      end
    end
    (1..4).each do |m|
      _cmp(2014, m)
    end
  end
end

__END__




