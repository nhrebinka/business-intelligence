#
# AuditedDim - extended class from Dim specialized in trigger auditing
#
# 1. #wipe_subject - truncate the test subect table
# 2. #add_fixture  - add new fixture into subject table using Dim class ACID func.
# 3. #validate     - check the fixture spec vs audit result
#
require 'dim'
require 'audit_spec'

class AuditedDim < Dim
    DEBUG    = true
    SCHEMA   = 'EDW_OWNER'
    FMT_DT   = '%Y-%m-%d %H:%M:%S'

    def audit_list
        sql = 
"
select /*+ cache(t) */ distinct t.column_name
from all_cons_columns t 
where 1=1
  and t.table_name = '#{@tname}'
  and t.owner      = '#{SCHEMA}'
  and t.column_name not like 'ROW_LAST%'
  and t.column_name not like 'ROW_CREATE%'
  and t.column_name not like 'ROW_MD5%'
order by t.column_name
"
        execute(sql).result.map { |r| r[0] }
    end
    #
    # clean the whole test subject
    #
    def wipe_subject
        execute "truncate table #{@tname}"
    end
    #
    # adding test fixture to the subject table
    #
    def add_fixture(fix)
        k = fix[tkey] || fix[tkey.downcase]
        unless find_by_key(k)
            insert(fix)
        end
    end
    #
    # update the subject to fire trigger
    #
    def validate(spec, audit)
        #
        # update and check whether the update is successful
        #
        dif, err = _diff(spec, audit)
        return false if err && err.keys.length > 0
        #
        # now we look into the audit log
        #
        sql = spec.cond
        rst = execute(sql).result
        n   = rst && rst.length==1
        v   = n ? spec.assert(dif, rst[0]) : false
        STDERR.puts "ERR #{@tname}: 0, or 2+ trigger result found" unless n
        if DEBUG && !v
            STDERR.puts "\n"+
            "AUD=>#{audit}\n"+
            "DIF=>#{dif}\n"+
            "ERR=>#{err}\n"+
            "#{sql}>>#{rst}"
        end
        v
    end

private
    def _to_s(v)
        case v
        when Time
            v.strftime(FMT_DT)
        when nil
            nil
        else
            v.to_s
        end
    end
    #
    # compare before and after record and return the difference as hashs
    #
    def _diff(spec, audit)
        r0 = find_by_key(spec.tkey)
        unless r0
            _log "failed to find test fixture #{@tname}.key=#{spec.tkey}"
            return nil
        end
        #
        # now lets start the actions
        #
        update(audit)
        r1 = find_by_key(spec.tkey)

        err,dif = {}, {}
        column_list.each do |k|
            ks = k.to_sym
            v0 = r0.shift
            v1 = r1.shift
            x  = (v0.to_s == v1.to_s)
            unless x
                dif[ks] = [ v0, v1 ]
                a = _to_s(audit[ks])
                err[ks] = [ a, v1  ] if a && a!=v1.to_s
            end
        end
        [ dif, err ]
    rescue Exception => e
        STDERR.puts e.inspect
        nil
    end
    def _log(msg)
       STDERR.puts msg if DEBUG
    end
end

if __FILE__ == $0
require 'minitest/autorun'

class AuditedDimSpec < MiniTest::Spec
    DIM_NAME = 'D_DIMTEST'
    P_NM0    = 'audit_dim_spec_before'
    P_NM1    = 'audit_dim_spec_after'
    T0       = Time.now
    TD       = 3600*4
    KEY      = 1000
    CODE     = 'ACTD'
    RANGE    = 1..5

    def self.test_order
        :alpha              # order dependent test!
    end

    def self.prep
        @@i = 0
        Oracle.new do |o|
            o.run("
drop table #{DIM_NAME} purge
")
            o.run("
create table #{DIM_NAME} (
dimtest_key integer      not null,
c_int       integer      not null,
c_value     number(10,2) not null,
c_string    varchar2(10) not null,
c_date      date         not null,
row_last_mod_proc_nm      varchar2(100),
row_last_mod_proc_seq_nbr integer,
row_last_mod_dttm         date
)
tablespace d_bi
")
            o.run("
CREATE OR REPLACE TRIGGER TRUA_D_DIMTEST
BEFORE UPDATE ON D_DIMTEST
REFERENCING OLD AS o NEW AS n
FOR EACH ROW
BEGIN
  xos_audit_mgr.set_context(
    'D_DIMTEST',
    :o.DIMTEST_KEY,
    :o.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_PROC_NM,
    :n.ROW_LAST_MOD_PROC_SEQ_NBR
  );

  xos_audit_mgr.audit('C_DATE', :o.C_DATE, :n.C_DATE);
  xos_audit_mgr.audit('C_INT', :o.C_INT, :n.C_INT);
  xos_audit_mgr.audit('C_STRING', :o.C_STRING, :n.C_STRING);
  xos_audit_mgr.audit('C_VALUE', :o.C_VALUE, :n.C_VALUE);
  xos_audit_mgr.audit('DIMTEST_KEY', :o.DIMTEST_KEY, :n.DIMTEST_KEY);
END;
")
        end
    end

    def self.fixture(d, i, k)
        fix  = {
            DIMTEST_KEY:    k,
            C_DATE:         T0,
            C_INT:          i,
            C_STRING:       CODE,
            C_VALUE:        i*0.5,
            ROW_LAST_MOD_PROC_NM:      P_NM0,
            ROW_LAST_MOD_PROC_SEQ_NBR: i,
            ROW_LAST_MOD_DTTM:         T0
        }
        d.add_fixture(fix)

        AuditSpec.new(d.tname, k, T0, P_NM1, i)
    end

    prep
    describe AuditedDimSpec do
        before do
            @d = AuditedDim.new(DIM_NAME)
            @i = (@@i+=RANGE.end)
        end

        it '1 creates audit_list' do
            @d.audit_list.join('|').must_equal "C_DATE|C_INT|C_STRING|C_VALUE|DIMTEST_KEY"
        end

        it '2 wipe_subject' do
            @d.wipe_subject
            @d.execute("select * from #{@d.tname}").result.must_equal []
        end

        it '3 create_fixture' do
            RANGE.each do |n|
                i = @i+n
                k = KEY+i
                AuditedDimSpec.fixture(@d, i, k)
                @d.find_by_key(k)[0].must_equal k
            end
        end
        
        it '4 validate' do
            i = @i
            %w(C_DATE C_INT C_STRING C_VALUE).each do |cn|
                k = KEY+i
                s = AuditedDimSpec.fixture(@d, i, k)
                t1= T0+TD*i
                v = case cn
                  when /_DATE/ 
                      t1
                  when /_INT|_VALUE/ 
                      (i+TD)
                  else
                      (i+TD).to_s
                  end
                a = {
                    cn.to_sym          => v,
                    DIMTEST_KEY:          s.tkey,
                    ROW_LAST_MOD_PROC_NM: P_NM1,
                    ROW_LAST_MOD_DTTM:    t1
                }
                @d.validate(s, a).must_equal true
                i += 1
            end
        end 
    end
end

end
__END__
