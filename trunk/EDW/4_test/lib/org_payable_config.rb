#
# OrgPayableConfig class - extended from IronAudit class
#
require 'iron_audit'

class OrgPayableConfig < IronAudit
    def initialize
        super('ORG_PAYABLE_CONFIG')
    end
end

if __FILE__ == $0
    require 'minitest/autorun'

    describe OrgPayableConfig do
        before do
            @d = OrgPayableConfig.new
        end
        it 'has table_name' do
            @d.tname.must_equal 'ORG_PAYABLE_CONFIG'
        end
        it 'has audit_list' do 
            @d.audit_list.join('|').must_equal "ORGANIZATION_KEY|EXPIRATION_DAYS|EXP_PAYABLE_TRANS_MATCH_DAYS|PAYABLE_RECON_LOWER_THRESHOLD|PAYABLE_RECON_UPPER_THRESHOLD|PAYABLE_CREDIT_LIMIT_PAD|ENABLE_DAYS_TO_HOLD_AUTH_FLAG|ENABLE_MIN_MAX_FOR_AUTH_FLAG"
        end
    end

end
__END__
