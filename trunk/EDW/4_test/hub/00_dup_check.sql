select count(*), a.edm_acct_key, b.edm_spnr_pgm_key
from m_acct a, m_spnr_pgm b
where a.edm_spnr_pgm_key = b.edm_spnr_pgm_key
group by a.edm_acct_key,  b.edm_spnr_pgm_key
having count(*) > 1;
