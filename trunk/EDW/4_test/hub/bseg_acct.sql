create table x_bseg_acct
tablespace d_bi
as 
select /*+ parallel(pd, 64) full(pd) */
case
  when government_flg='Y'                    then 'GOVERNMENT'
  when revolver_flg='Y'                      then 'REVOLVER'
  when program_name in (
    'PAC PRIDE CUSTOM NETWORK', -- 6/25
    'PACIFIC PRIDE')                         then 'PAC PRIDE CUSTOM NETWORK'
  when trim(program_name) in (
    'PRIVATE LABEL CLOSED NETWORK', -- 6/25, extra space!
    'PRIVATE LABEL UNFUNDED',       -- 6/25
    'UNFUNDED PRIVATE LABEL',
    'PRIVATE LABEL',
    'Private Label Funded', 
    'Private Label Unfunded',
    'D4', 'Basic', 'Enhanced')               then 'PRIVATE LABEL'
  when program_name in ('BRANDED UNIVERSAL') then 'BRANDED UNIVERSAL'
  when program_name in (
    'PARTNER BILLED COBRAND', -- 6/25
    'Partner Billed',
    'PARTNER BILLED CO-BRAND'
  )                                          then 'PARTNER BILLED COBRAND'
  when program_name in (
    'FLEET BILLED COBRAND',   -- 6/25
    'Affinity',
    'FLEET BILLED CO-BRAND'
  )                                          then 'FLEET BILLED COBRAND'
  when program_name in (
    'WEX DIRECT UNIVERSAL',   -- 6/25
    'WEX DIRECT',
    'Universal'
  ) then (
    case 
      when count(*) > 80 then 'WEX81+'
      else 'WEX<=80'
    end
  )
  else 'ALL OTHER'
end bseg,
count(*) cnt, 
edm_acct_key, wex_acct_nbr,
program_product_name,
edm_spnr_pgm_key, government_flg, revolver_flg, program_name, spnr_nm, pgm_nm
from m_acct
join m_spnr_pgm        using (edm_spnr_pgm_key)
join m_purch_device pd using (edm_acct_key)
where program_product_line_name in ('Fleet Card')
and   pd_sts not in ('Terminated', 'Moved', 'Converted')
group by 
edm_acct_key, wex_acct_nbr,
program_product_name,
edm_spnr_pgm_key, government_flg, revolver_flg, program_name, spnr_nm, pgm_nm
;

--
-- validation
--
select count(*), government_flg, program_product_name, program_name, bseg
from x_bseg_acct
group by bseg, program_product_name, program_name, government_flg
order by 2 desc,3,5,4;

