SET ECHO ON
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET LINESIZE 2000

spool /i1/&&env/hub/logs/ht_00212204_datafix.log;


DELETE from EDM_OWNER.M_CAMPAIGN_XREF mcx  where trunc(mcx.EDM_LAST_UPDT_DT) = '10-JUNE-11'; 

-- 1 record should be deleted 
 
DELETE from EDM_OWNER.M_CAMPAIGN mc  where trunc(MC.EDM_LAST_UPDT_DT) = '10-JUNE-11';
-- 13018 record should be deleted 

commit;

 SPOOL OFF ; 
  
