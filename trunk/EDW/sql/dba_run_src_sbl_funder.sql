

SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_funder.dat;

SELECT                                         /*+parallel (dw_funder  , 4) */
      Dw_FUNDER_KEY
       || '~|~'
       || FUNDER_CD
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || BUSINESS_UNIT
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || FUNDER_DESC
       || '~|~'
       || FUNDER_WRITEOFF_CD
       || '~|~'
       || AGING_ID
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || ''
       || '~|~'
       || OCL_LETTER_PCT
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || OCL_SUSPEND_PCT
       || '~|~'
       || SUSP_NBR_DAYS
       || '~|~'
       || BANK_CD
       || '~|~'
       || CURRENCY_CD
       || '~|~'
       || WEX_FUNDED_FLG
       || '~|~'
       || GL_AR_ACCT_NBR
       || '~|~'
       || FIN_CHG_CAT
       || '~|~'
       || BASIS_AMT_FLG
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || MIN_AGE_CAT
       || '~|~'
       || MAX_AGE_CAT
       || '~|~'
       || TAX_IDENTIFIER_1
       || '~|~'
       || TAX_IDENTIFIER_2
  FROM DW_FUNDER
 WHERE DW_CURRENT_FLG = 'Y';

spool off ; 

