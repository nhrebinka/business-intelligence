SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000



spool /i1/&&env/hub/logs/dba_d_reasoncode_merge.log ; 
  

merge  into D_ACCOUNT D  using EDM_OWNER.D_ACCOUNT@PRHUB.PROD  M 
 on  ( D.ACCOUNT_KEY   = M.ACCOUNT_KEY  ) 
 WHEN MATCHED THEN 
   UPDATE SET 
                  D.ATTRITION_REASON_CODE   =   M.ATTRITION_REASON_CODE  ,
                  D.ATTRITION_REASON_DESC   =   M.ATTRITION_REASON_DESC  , 
                  D.ATTRITION_TYPE_NAME     =   M.ATTRITION_TYPE_NAME    ,    
                  D.ROW_EFF_END_DTTM        =   M.ROW_EFF_END_DTTM       ,    
                  D.ROW_LAST_MOD_DTTM       =   sysdate  ,
                  D.ROW_LAST_MOD_PROC_NM    =  'Data Fix for Attrirtion Reason Code'  ; 
                  
commit ; 




  
spool off ; 
    

  


