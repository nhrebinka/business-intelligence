SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_purch_device.dat ; 

-- Note PD ROW ID FROM DW pURCh DEVICE IS FROM CURR PD ROW ID 
              
/* Formatted on 4/7/2011 1:11:04 PM (QP5 v5.163.1008.3004) */
(SELECT                                                  /*+ parallel (d,4) */
       DW_PURCH_DEVICE_KEY
        || '~|~'
        || CURR_PD_ROW_ID
        || '~|~'
        || ACCT_ROW_ID
        || '~|~'
        || DEPT_ROW_ID
        || '~|~'
        || TO_CHAR (PD_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || PD_PROFILE_NM
        || '~|~'
        || PD_SITE_ROW_ID
        || '~|~'
        || EMBOSS_PD_NBR
        || '~|~'
        || EMBOSS_SOURCE_1
        || '~|~'
        || EMBOSSED_VALUE_1
        || '~|~'
        || EMBOSS_SOURCE_2
        || '~|~'
        || EMBOSSED_VALUE_2
        || '~|~'
        || EMBOSS_SOURCE_3
        || '~|~'
        || EMBOSSED_VALUE_3
        || '~|~'
        || WEX_PD_NBR
        || '~|~'
        || PD_SUFFIX_NBR
        || '~|~'
        || SPNR_PD_NBR
        || '~|~'
        || MRCH_SITE_ID
        || '~|~'
        || TO_CHAR (PD_SETUP_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (LAST_ISSUED_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (PD_RENEWAL_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (PD_EXPN_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || PD_STS
        || '~|~'
        || AUTH_CNTRL_ROW_ID
        || '~|~'
        || POS_PROMPTING
        || '~|~'
        || ''
        || '~|~'
        || PD_STS_RSN
        || '~|~'
        || TRACK_II_RESTR
        || '~|~'
        || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || DW_LAST_UPDT_SESSION_NM
        || '~|~'
        || DW_SOURCE_SYS
        || '~|~'
        || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || ''
        || '~|~'
        || PD_EXCEPTION_ROW_ID
        || '~|~'
        || ENCODED_PD_NBR
        || '~|~'
        || POS_PROMPT_CD
        || '~|~'
        || ICR_NOT_ALLOWED
        || '~|~'
        || PD_ROW_ID
        || '~|~'
        || ORIG_PD_ROW_ID
        || '~|~'
        || CROSS_ACCEPT_ACCT_NBR_FLG
        || '~|~'
        || PD_PROD_ROW_ID
        || '~|~'
        || ASSET_ROW_ID
        || '~|~'
        || DRIVER_ROW_ID
        || '~|~'
        || INT_PRMPT_CD
        || '~|~'
        || INT_PRMPTING
        || '~|~'
        || PD_DEVICE_TYPE
        || '~|~'
        || INTERNAL_SUSPEND_FLG
        || '~|~'
        || UDF1
        || '~|~'
        || UDF2
        || '~|~'
        || UDF3
        || '~|~'
        || UDF4
        || '~|~'
        || UDF5
        || '~|~'
        || UDF6
        || '~|~'
        || UDF7
        || '~|~'
        || UDF8
        || '~|~'
        || UDF9
        || '~|~'
        || UDF10
        || '~|~'
        || UDF11
        || '~|~'
        || UDF12
        || '~|~'
        || UDF13
        || '~|~'
        || UDF14
        || '~|~'
        || UDF15
        || '~|~'
        || UDF16
        || '~|~'
        || UDF17
        || '~|~'
        || UDF18
        || '~|~'
        || UDF19
        || '~|~'
        || UDF20
        || '~|~'
        || REQUESTED_BY
        || '~|~'
        || BANK_ID_NBR
        || '~|~'
        || DW_CURRENT_FLG
        || '~|~'
        || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (DW_EXPN_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || EMBOSS_PD_NBR_GEN_ALGORITHM
        || '~|~'
        || PD_ACCEPT_LVL
        || '~|~'
        || PD_ACCEPT_LVL_DESC
        || '~|~'
        || TO_CHAR (PD_ACTIV_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || PD_CNTRL_NBR
        || '~|~'
        || PD_PROD_NM
        || '~|~'
        || PD_PROD_SPNR_DISPLAY_NM
        || '~|~'
        || TO_CHAR (PD_SUSPENSE_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || TO_CHAR (PD_TERMED_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || STATION_CARD_FLG
        || '~|~'
        || TO_CHAR (STATUS_DT, 'MM/DD/YYYY hh24:mi:ss')
        || '~|~'
        || USAGE_TYPE
  FROM DW_PURCH_DEVICE D
 WHERE
   D.ACCT_ROW_ID In 
                 (select ACCT_ROW_ID   from DW_ACCT  D
                   WHERE 
                          D.DW_CURRENT_FLG = 'Y' AND D.DW_ACCT_KEY <> 99 
                      and D.DW_CREATE_DT >= '23-JUL-2011' 
                      and D.DW_SOURCE_SYS = 'SIEBEL' )   
 AND 
    (D.EMBOSS_PD_NBR, D.DW_CREATE_DT  )  IN   
                                       (  SELECT       /*+ parallel (d1,4) */
                                                 D1.EMBOSS_PD_NBR, MAX (D1.DW_CREATE_DT) 
                                          FROM 
                                                 DW_PURCH_DEVICE D1
                                          WHERE 
                                                 D1.DW_SOURCE_SYS = 'SIEBEL' 
                                          GROUP BY 
                                                 D1.EMBOSS_PD_NBR
                                          HAVING 
                                               MIN(D1.DW_CREATE_DT)  >= '23-JUL-2011' )
  ) ; 
  
  
  
  
spool off ; 
    

  


