SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/dba_run_m_pos_fix.log ; 

 
update m_pos A 
set A.LEGACY_WEX_SITE_ID='BR002288' , 
    A.SRC_SYS_LAST_UPDT_DT = A.SRC_SYS_LAST_UPDT_DT + 1 , 
    A.EDM_LAST_UPDT_SESSION_NM = 'DATa Fix to Update the Legacy WEX SITE ID'  
WHERE A.EDM_POS_KEY=129147 ; 
commit;

spool off;