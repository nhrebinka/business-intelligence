SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/logs/dba_run_prog_ht_211125.log 

delete from d_program
where program_key in (
select program_key
from d_program
where row_create_dttm > sysdate-1
and program_package_name not in ('CitiCapital Lucent/Avaya Partner Billed','Bosselman Energy D4')
);

-- Count should equal rows deleted, if yes, commit, else rollback

Spool Off ; 