/*
REM   Filename : drop_pub_synonym.sql		
REM 
REM
REM  Description: Generates script that which creates public synonyms for
REM    	          all tables in  wb_owner  schema for ?WB
REM 
REM  Log: Created:  06/12/2011   
REM  log        :  Created  by venu on 06/12/2011
REM  
REM**********************************************************************/  



set heading off ;
set feedback off ; 
set linesize 180 ; 
set time off
set timing off
set echo off 


spool /i1/&&env/hub/logs/drop_public_synonym.sql ;

select  'drop public synonym ' || uo.synonym_name ||  ' '  || ';'  
from all_synonyms   uo , user_users uu 
 where 
    uo.OWNER = 'PUBLIC' 
 and uo.TABLE_OWNER  = uu.username  
/

spool off ; 

set linesize 80 ;
set feedback on ; 
set heading on  ; 
set echo on ;

spool /i1/&&env/hub/logs/drop_public_synonym.log;

start /i1/&&env/hub/logs/drop_public_synonym.sql;

spool off
