    SET ECHO OFF
    SET NEWPAGE 0
    SET SPACE 0
    SET PAGESIZE 0
    SET FEEDBACK OFF
    SET HEADING OFF
    SET TRIMSPOOL ON
    SET TAB OFF
    set LINESIZE 2000

   
 spool src_sbl_spnr_pgm.dat  ; 
  
/* Formatted on 6/25/2010 5:09:36 PM (QP5 v5.139.911.3011) */
SELECT                                         /*+parallel (dw_spnr_pgm, 4) */
      Dw_SPNR_PGM_KEY
       || '~|~'
       || SPNR_NM
       || '~|~'
       || PGM_ROW_ID
       || '~|~'
       || TO_CHAR (PGM_END_DT, 'MM/DD/YYYY hh24:mi:ss') 
       || '~|~'
       || PGM_NM
       || '~|~'
       || TO_CHAR (PGM_START_DT, 'MM/DD/YYYY hh24:mi:ss') 
        || '~|~'
        || REPLACE(REPLACE(PGM_DESC,CHR(13),CHR(32)),CHR(10),CHR(32))
             || '~|~'
             || SPNR_ROW_ID
             || '~|~'
             || PGM_SUB_TYPE
             || '~|~'
             || TO_CHAR (PGM_APPROVED_DT, 'MM/DD/YYYY hh24:mi:ss')
             || '~|~'
             || PGM_APPROVED_FLG
             || '~|~'
             || PAR_PGM_ROW_ID
             || '~|~'
             || TO_CHAR (SPNR_PRC_LST_APPROVED_DT, 'MM/DD/YYYY hh24:mi:ss')
             || '~|~'
             || LINE_OF_BUS_NM
             || '~|~'
             || COBRAND_ALLOWED_FLG
             || '~|~'
             || PGM_CONTACT_FIRST_NM
             || '~|~'
             || PGM_CONTACT_LAST_NM
             || '~|~'
             || CURR_CONV_SURCHARGE_RATE
             || '~|~'
             || CURRENCY_CD
             || '~|~'
             || INTL_UNITS_SUPPORTED_CD
             || '~|~'
             || LANGUAGE_CD
             || '~|~'
             || MAX_TRANS_AGE_CD
             || '~|~'
             || SPNR_PRC_LIST_NM
             || '~|~'
             || PGM_SHORT_NM
             || '~|~'
             || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
             || '~|~'
             || PGM_STS
             || '~|~'
             || TAX_PART_FLG
             || '~|~'
             || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
             || '~|~'
             || CLIENT_ID
             || '~|~'
             || DW_LAST_UPDT_SESSION_NM
             || '~|~'
             || ONLINE_INV_URL
             || '~|~'
             || WEX_FUNDED_FLG
             || '~|~'
             || PHONE_NBR
             || '~|~'
             || DW_SOURCE_SYS
             || '~|~'
             || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
             || '~|~'
             || ''
             || '~|~'
             || OVL_MACRO_FILE_NM
             || '~|~'
             || CUSTOMER_SERVICE_PHONE_NBR
             || '~|~'
             || CUSTOMER_SERVICE_FAX_NBR
             || '~|~'
             || PGM_URL
             || '~|~'
             || PGM_ENROLLMENT_URL
             || '~|~'
             || LANDSCAPE_LOGO_FILE_NM
             || '~|~'
             || PORTRAIT_LOGO_FILE_NM
             || '~|~'
             || LOGO_BITMAP_FILE_NM
             || '~|~'
             || FUNDED_INVOICE_BACK_FILE_NM
             || '~|~'
             || EXCEPTION_SIGNUP_FORM_NM
             || '~|~'
             || AUTH_PROD_SET_ROW_ID
             || '~|~'
             || SALES_PROD_SET_ROW_ID
             || '~|~'
             || FEDERAL_CCI_TAX_EXEMPT_FLG
             || '~|~'
             || DISTRIBUTOR_HOST_ID
             || '~|~'
             || ID_OUT_OF_NETW_TRANS_FLG
             || '~|~'
             || DFLT_MRCH_AGRMNT_PGM_ROW_ID
             || '~|~'
             || UNFUNDED_INVOICE_BACK_FILE_NM
             || '~|~'
             || RELATION_CD
             || '~|~'
             || AUTO_PRMPT_CORR_FLG
             || '~|~'
             || STATE_CCI_TAX_EXEMPT_FLG 
             || '~|~'
             || MASTER_ACCT_ROW_ID
             FROM 
             DW_SPNR_PGM  
             WHERE DW_CURRENT_FLG = 'Y'
              AND DW_SPNR_PGM_KEY <> 99 ;
spool off ;     
    


    
  