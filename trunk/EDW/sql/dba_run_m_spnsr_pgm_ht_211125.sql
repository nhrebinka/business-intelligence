SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/logs/dba_run_m_spnsr_pgm_ht_211125.log 

/*
oneshot fix for programs
*/

create table M_SPNSR_PGM_BKP AS SELECT * FROM  M_SPNR_PGM ; 

create table M_SPNSR_PGM_XREF_BKP AS SELECT * FROM  M_SPNR_PGM_XREF ; 

update edm_owner.m_spnr_pgm p
set (pgm_nm                 
,pgm_sub_type                
,program_name               
,program_package_name               
,program_service_option_code    
,program_product_name                   
,program_product_line_name              
,program_category_name                
,program_manager_name                 
,program_sponsor_name                   
,plastic_type    
,EDM_LAST_UPDT_SESSION_NM    
,EDM_LAST_UPDT_DT  
) =(
select  
 pgm_nm
,pgm_sub_type                
,program_name                 
,program_package_name             
,program_service_option_code      
,program_product_name                  
,program_product_line_name             
,program_category_name               
,program_manager_name                  
,program_sponsor_name                   
,plastic_type     
,'Fix 5/25'
,sysdate                                   
from stage_owner.m_spnr_pgm s
where s.edm_spnr_pgm_key = p.edm_spnr_pgm_key
 and  s.edm_spnr_pgm_key IN
(1893,
1894,
1909,
1941,
1942,
1943,
1945,
1946,
1947,
1948,
1950,
1952,
1953,
1954,
1956,
1958,
1959,
1960,
1961,
1962,
1963,
1964,
1965,
1967,
1968,
1971,
1975,
1976,
1978,
1979,
1980,
1981,
1982,
1983,
1984,
1985,
1986,
1987,
1989,
1990,
1991,
1992,
1993,
1994,
1995,
1996,
1997,
1998,
1999,
2003,
2004,
2005))
where 
 p.edm_spnr_pgm_key in
(1893,
1894,
1909,
1941,
1942,
1943,
1945,
1946,
1947,
1948,
1950,
1952,
1953,
1954,
1956,
1958,
1959,
1960,
1961,
1962,
1963,
1964,
1965,
1967,
1968,
1971,
1975,
1976,
1978,
1979,
1980,
1981,
1982,
1983,
1984,
1985,
1986,
1987,
1989,
1990,
1991,
1992,
1993,
1994,
1995,
1996,
1997,
1998,
1999,
2003,
2004,
2005)
;


COMMIT ; 

 MERGE INTO T_ITEM_ACTIVITY A  
 USING M_SPNR_PGM  B 
 ON ( A.EDM_SPNR_PGM_KEY  = B.EDM_SPNR_PGM_KEY ) 
 WHEN MATCHED THEN 
  UPDATE SET
    A.PROGRAM_PACKAGE_NAME = B.PROGRAM_PACKAGE_NAME  ; 

COMMIT ; 

MERGE INTO T_PURCH_TRANS  A  
USING M_SPNR_PGM  B 
 ON ( A.EDM_SPNR_PGM_KEY  = B.EDM_SPNR_PGM_KEY ) 
WHEN MATCHED THEN  
 UPDATE SET 
    A.PROGRAM_PACKAGE_NAME = B.PROGRAM_PACKAGE_NAME  ;
  
  
 COMMIT ;
  
MERGE INTO T_ANCL_DTL  A  
USING M_SPNR_PGM  B 
 ON ( A.EDM_SPNR_PGM_KEY  = B.EDM_SPNR_PGM_KEY ) 
WHEN MATCHED THEN  
 UPDATE SET 
    A.PROGRAM_PACKAGE_NAME = B.PROGRAM_PACKAGE_NAME  ;
    
COMMIT ; 

spool  off ; 
