SET ECHO ON
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET LINESIZE 2000

spool /i1/&&env/hub/logs/dba_sbl_1980_pd_data_fix.log  ;

-- this is done to set  to low date so that updates are done from the Source 


UPDATE D_PURCHASE_DEVICE DPD
SET 
     DPD.ROW_MD5_CHECKSUM_HEX   = 0,
     DPD.ROW_LAST_MOD_DTTM      = sysdate ,
     DPD.ROW_LAST_MOD_PROC_NM   = 'Data fix For SBL 1980 Dates'
WHERE 
     DPD.CARD_STATUS_DATE       = '01-JAN-1980' 
 AND DPD.EMBOSSED_CARD_NUMBER_ID IN
                              (SELECT MPD.EMBOSS_PD_NBR
                               FROM 
                                     M_PURCH_DEVICE MPD ,
                                     M_PURCH_DEVICE_XREF MPDX
                               WHERE 
                                   MPD.EDM_PURCH_DEVICE_KEY   = MPDX.EDM_PURCH_DEVICE_KEY
                               AND MPDX.DW_SOURCE_SYS         = 'SIEBEL'
                              ) ;

--403082 rows updated.
COMMIT ;

UPDATE D_PURCHASE_DEVICE@PREDW.PROD DPD
SET 
     DPD.ROW_MD5_CHECKSUM_HEX     = 0,
     DPD.ROW_LAST_MOD_DTTM        = sysdate ,
     DPD.ROW_LAST_MOD_PROC_NM     = 'Data fix For SBL 1980 Dates'
WHERE DPD.EMBOSSED_CARD_NUMBER_ID IN
                      (SELECT DPD1.EMBOSSED_CARD_NUMBER_ID
                        FROM D_PURCHASE_DEVICE DPD1
                       WHERE DPD1.ROW_LAST_MOD_PROC_NM = 'Data fix For SBL 1980 Dates'
                      ) ;

--403108 rows updated.
commit ; 



 MERGE INTO M_PURCH_DEVICE MPD 
 USING STAGE_OWNER.SRC_SBL_PURCH_DEVICE S 
 ON 
   ( MPD.EMBOSS_PD_NBR = S.EMBOSS_PD_NBR ) 
  WHEN MATCHED THEN 
  UPDATE 
  SET 
      MPD.PD_SETUP_DT               = TRUNC(S.DW_CREATE_DT) ,
      MPD.EDM_LAST_UPDT_SESSION_NM  = 'Data fix For SBL 1980 Dates' ,
      MPD.EDM_LAST_UPDT_DT          = sysdate 
  WHERE MPD.PD_SETUP_DT             = '01-JAN-1980' ;       
    
commit ; 
  
 MERGE INTO M_PURCH_DEVICE MPD 
 USING STAGE_OWNER.SRC_SBL_PURCH_DEVICE S 
 ON 
   ( MPD.EMBOSS_PD_NBR = S.EMBOSS_PD_NBR ) 
  WHEN MATCHED THEN 
  UPDATE 
  SET 
      MPD.LAST_ISSUED_DT            = TRUNC(S.DW_CREATE_DT) ,
      MPD.EDM_LAST_UPDT_SESSION_NM  = 'Data fix For SBL 1980 Dates' ,
      MPD.EDM_LAST_UPDT_DT          = sysdate 
  WHERE MPD.LAST_ISSUED_DT          = '01-JAN-1980' ;       
    
 commit ; 
 

-- Fix  to update the terminated date for the LOST Cards       
     
MERGE INTO M_PURCH_DEVICE MPD
 USING  
 (  SELECT  EMBOSS_PD_NBR , PD_SETUP_DT , SUBSTR(EMBOSS_PD_NBR , 18 , 1) SUBPD 
     FROM M_PURCh_DEVICE 
   WHERE SUBSTR(EMBOSS_PD_NBR , 1, 17)  in  
   (select 
      /*+ parallel (mpd, 4)*/ 
    MIN(SUBSTR(MPD.EMBOSS_PD_NBR, 1, 17)) 
    from 
         M_PURCH_DEVICE MPD 
    where 
          MPD.EDM_LAST_UPDT_SESSION_NM  = 'Data fix For SBL 1980 Dates'
   Group BY 
            SUBSTR(MPD.EMBOSS_PD_NBR, 1, 17) 
   HAVING COUNT(*) > 1 )
     ) A 
   ON  (    MPD.EMBOSS_PD_NBR <  A.EMBOSS_PD_NBR 
         AND   A.SUBPD  -   SUBSTR(MPD.EMBOSS_PD_NBR , 18,1 ) = 1 
         AND SUBSTR(MPD.EMBOSS_PD_NBR, 1, 17) = SUBSTR(a.EMBOSS_PD_NBR , 1, 17) 
       )   
   WHEN MATCHED THEN 
   UPDATE 
       SET MPD.PD_SETUP_DT = A.PD_SETUP_DT
    WHERE 
       MPD.EDM_LAST_UPDT_SESSION_NM  = 'Data fix For SBL 1980 Dates'    
        ; 
 

COMMIT ; 

spool OFF ; 
    
    
   
    
     