SET ECHO    ON
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000


-- Reporting National Account ID

spool /i1/&&env/hub/logs/d_credit_national_account_id_EDM_upd.log;


MERGE INTO EDM_OWNER.D_ACCOUNT da
     USING EDM_OWNER.M_ACCT MA
        ON (DA.SOURCE_ACCOUNT_ID = MA.WEX_ACCT_NBR)
WHEN MATCHED
THEN
   UPDATE SET DA.REPORTING_NATIONAL_ACCOUNT_ID = 'Unknown'
           WHERE DA.SOURCE_SYSTEM_CODE = 'SIEBEL';

COMMIT;

-- Credit National Account ID

MERGE INTO EDM_OWNER.D_ACCOUNT DA USING EDM_OWNER.M_ACCT MA
ON ( DA.SOURCE_ACCOUNT_ID = MA.WEX_ACCT_NBR)
WHEN MATCHED
THEN 
UPDATE 
SET DA.CREDIT_NATIONAL_ACCOUNT_ID =  
    (CASE
         WHEN TRIM(MA.CREDIT_NATIONAL_ACCOUNT_ID) IS NOT NULL  
            THEN 
                 MA.CREDIT_NATIONAL_ACCOUNT_ID
            ELSE
                'Unknown' 
    END) ;

COMMIT;

-- Genrerate New Hash key 

MERGE INTO EDM_OWNER.D_ACCOUNT DA
     USING EDM_OWNER.M_ACCT MA
        ON (DA.SOURCE_ACCOUNT_ID = MA.WEX_ACCT_NBR)
WHEN MATCHED
THEN
   UPDATE SET
      DA.ROW_MD5_CHECKSUM_HEX_T2 =
         EDM_OWNER.
         md5hash (
               TRIM (FUNDER_NAME)
            || ' '
            || TRIM (FUNDING_TYPE_NAME)
            || ' '
            || TRIM (ACCOUNT_REGION_CODE)
            || ' '
            || TRIM (ACCOUNT_REGION_NAME)
            || ' '
            || TRIM (ACCOUNT_DISTRICT_NAME)
            || ' '
            || TRIM (ACCOUNT_DISTRICT_CODE)
            || ' '
            || TRIM (ACCOUNT_SUBDISTRICT_NAME)
            || ' '
            || TRIM (ACCOUNT_SUBDISTRICT_CODE)
            || ' '
            || TRIM (ACCOUNT_POSTAL_CODE)
            || ' '
            || TRIM (ACCOUNT_COUNTRY_NAME)
            || ' '
            || TRIM (PROGRAM_NAME)
            || ' '
            || TRIM (ACCOUNT_NAME)
            || ' '
            || TRIM (ACCOUNT_STATUS_DESC)
            || ' '
            || TRIM (DIVERSION_ACCOUNT_FLG)
            || ' '
            || TRIM (BILLING_ACCOUNT_FLG)
            || ' '
            || TRIM (RELATED_BILLING_ACCOUNT_ID)
            || ' '
            || TRIM (TO_CHAR (SIC_LOOKUP_KEY))
            || ' '
            || TRIM (ACCOUNT_MANAGER_NAME)
            || ' '
            || TRIM (PURCHASE_ACCOUNT_FLG)
            || ' '
            || TRIM (CUSTOMER_NAME)
            || ' '
            || TRIM (ATTRITION_REASON_DESC)
            || ' '
            || TRIM (TO_CHAR (RISK_GRADE))
            || ' '
            || TRIM (TO_CHAR (ACCOUNT_CLOSED_DATE, 'MM/DD/YYYY HH24:mi:SS'))
            || ' '
            || TRIM (ATTRITION_TYPE_NAME)
            || ' '
            || TRIM (ATTRITION_REASON_CODE)
            || ' '
            || TRIM (SOURCE_SYSTEM_CODE)
            || ' '
            || TRIM (TAX_EXEMPT_FLG)
            || ' '
            || TRIM (CLM_CLUSTER_NAME)
            || ' '
            || TRIM (TO_CHAR (LAST_CLUSTERED_DATE, 'MM/DD/YYYY'))
            || ' '
            || TRIM (PRODUCT_PURCHASE_RESTRICTION)
            || ' '
            || TRIM (PAYMENT_METHOD)
            || ' '
            || TRIM (EFPS_REQUIRED_FLG)
            || ' '
            || TRIM (EFPS_FLG)
            || ' '
            || TRIM (EFPS_TIME_INTERVAL)
            || ' '
            || TRIM (CREDIT_NATIONAL_ACCOUNT_ID)),
      DA.ROW_LAST_MOD_DTTM = SYSDATE,
      DA.ROW_LAST_MOD_PROC_NM = 'One Shot credit_national_account_id_upd';

COMMIT;

spool off;