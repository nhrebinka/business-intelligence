SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000



spool /i1/&&env/hub/logs/dba_reason_code_merge.log ; 
  

merge  into EDM_OWNER.D_ACCOUNT D  
        using  (select   MA.WEX_ACCT_NBR  , 
                 SUBSTR(MA.ACCT_STS_RSN  , 1, 2 ) ATTRITION_REASON_CODE ,
                 CASE WHEN SUBSTR(MA.ACCT_STS_RSN  , 1, 2 ) in ('01','02','12','50','51','52','55')   THEN 'Involuntary'  
                       WHEN SUBSTR(MA.ACCT_STS_RSN  , 1, 2 ) in ('03','04','05','26','27','54')        THEN 'NNCL Conversion'
                       WHEN SUBSTR(MA.ACCT_STS_RSN  , 1, 2 ) in ('30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','53')  THEN   'Migration/Conversion'
                       ELSE  'Voluntary' 
                END    ATTRITION_TYPE_NAME  , 
              MC.CODENAME     ATTRITION_REASON_DESC       
              from  M_ACCT MA , 
                       M_CODE MC    
              WHERE 
                        MA.ACCT_STS = 'Terminated'   
              and  SUBSTR(MA.ACCT_STS_RSN  , 1, 2 )  =  MC.CODEVAL  ) M    
 on  (D.SOURCE_ACCOUNT_ID  = M.WEX_ACCT_NBR) 
WHEN MATCHED THEN 
   UPDATE SET 
                  D.ATTRITION_REASON_CODE   =   m.ATTRITION_REASON_CODE  ,
                  D.ATTRITION_REASON_DESC   =   M.ATTRITION_REASON_DESC  , 
                  D.ATTRITION_TYPE_NAME     =   M.ATTRITION_TYPE_NAME ,    
                  D.ROW_LAST_MOD_DTTM       =  sysdate  ,
                  D.ROW_LAST_MOD_PROC_NM    =  'Data Fix for Attrirtion Reason Code'  
 WHERE 
        D. ACCOUNT_STATUS_DESC  =  'Terminated'      
    and D.ATTRITION_REASON_CODE like 'U%'      ; 


commit ; 



update  m_pos_xref mpx  
  set  mpx.EDM_POS_KEY          = 517389  , 
        MPX.EDM_LAST_UPDT_DT = sysdate ,
        MPX.EDM_LAST_UPDT_SESSION_NM = 'Data Fix  to fix the ZT / HI  Issue '  
  Where EDM_POS_KEY =    93143 ;
  
  commit ; 
  
  DELETE FROM M_POS where EDM_POS_KEY = 93143 ;
  
  commit ; 
  
  UPDATE M_POS MP 
  SET MP.GRP_MRCH_ID = MP.MRCH_ID , 
         MP.EDM_LAST_UPDT_DT = sysdate ,
         MP.EDM_LAST_UPDT_SESSION_NM = 'Data Fix  to fix the ZT / HI  Issue '  
  where MP.GRP_MRCH_ID = 'ZI' ; 
  
Commit ; 
  

  
spool off ; 
    

  


