SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool c:\edm\src_dw_stg_ancil_dtl_static.dat
              
  SELECT  /*+ parallel ( FW_ANCIL_DTL , 10) */ 
     F.DW_BILL_CD_KEY                ||'~|~'||
     DW_RETAIL_DISC_KEY                             ||'~|~'||
     F.DW_ACCT_KEY                      ||'~|~'|| 
     DW_ACCT_AGRMNT_KEY                             ||'~|~'|| 
     DW_ENTLMT_KEY                                  ||'~|~'|| 
     DW_DRIVER_KEY                                  ||'~|~'|| 
     DW_SPNR_KEY                                    ||'~|~'|| 
     DW_SPNR_PGM_KEY                                ||'~|~'|| 
     DW_MRCH_KEY                                    ||'~|~'|| 
     DW_MRCH_AGRMNT_KEY                             ||'~|~'|| 
     DW_POS_KEY                                     ||'~|~'|| 
     DW_SITE_KEY                                    ||'~|~'|| 
     DW_POST_DT_KEY                                 ||'~|~'|| 
     DW_CYCLE_CALENDAR_KEY                          ||'~|~'|| 
     DW_FEE_KEY                                     ||'~|~'|| 
     DW_PURCH_DEVICE_KEY                            ||'~|~'|| 
     DW_FUNDER_KEY                                  ||'~|~'|| 
     DW_DEPT_KEY                                    ||'~|~'|| 
     SRC_SYS_ID                                     ||'~|~'|| 
     ANCIL_ROW_ID                                   ||'~|~'|| 
     TO_CHAR(ANCIL_DT   ,'MM/DD/YYYY HH24:MI:SS' )          ||'~|~'|| 
     TO_CHAR(POSTING_DT ,'MM/DD/YYYY HH24:MI:SS' )          ||'~|~'|| 
     REBATE_QTY                                     ||'~|~'|| 
     UNIT_OF_MEAS                                   ||'~|~'|| 
     US_ANCIL_PER_UNT_AMT                           ||'~|~'|| 
     US_BILLED_ANCIL_AMT                            ||'~|~'|| 
     US_WAIVED_ANCIL_AMT                            ||'~|~'|| 
     ACCT_ANCIL_PER_UNT_AMT                         ||'~|~'|| 
     ACCT_BILLED_ANCIL_AMT                          ||'~|~'|| 
     ACCT_WAIVED_ANCIL_AMT                          ||'~|~'|| 
     ACCT_SPNR_BILL_AMT                             ||'~|~'|| 
     ANCIL_PCT                                      ||'~|~'|| 
     US_SPNR_BILL_AMT                               ||'~|~'|| 
     DW_ANCIL_TYPE                                  ||'~|~'|| 
     TO_CHAR (F.DW_CREATE_DT,'MM/DD/YYYY HH24:MI:SS' )          ||'~|~'||  
     TO_CHAR (F.DW_LAST_UPDT_DT,'MM/DD/YYYY HH24:MI:SS' )   ||'~|~'||  
     F.DW_LAST_UPDT_SESSION_NM                       ||'~|~'|| 
     F.DW_SOURCE_SYS                                      ||'~|~'||
     REBATE_TIER_ROW_ID                             ||'~|~'|| 
     NBR_OF_UNTS                                    ||'~|~'|| 
     FW_ANCIL_DTL_KEY                               ||'~|~'|| 
     DB.BILL_CD                                   ||'~|~'|| 
     DA.ACCT_ROW_ID   
FROM  FW_ANCIL_DTL  F , 
      DW_ACCT       DA   , 
      DW_BILL_CD    DB  
 where 
      F.DW_ACCT_KEY    = DA.DW_ACCT_KEY  
  AND F.DW_BILL_CD_KEY = DB.DW_BILL_CD_KEY  
  AND ANCIL_DT between '19-OCT-2010'  and '12-JAN-2011'   ;
  
  
SPOOL OFF ;  
  

    

  


