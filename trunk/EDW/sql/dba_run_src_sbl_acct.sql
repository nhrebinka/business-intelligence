SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000
   

spool /i1/&&env/hub/data/src_sbl_acct.dat ; 

SELECT  /*+  parallel (d, 4) */
      DW_ACCT_KEY
       || '~|~'
       || ACCT_NM
       || '~|~'
       || ACCT_SHORT_NM
       || '~|~'
       || ACCT_TYPE
       || '~|~'
       || ACCT_STS_RSN
       || '~|~'
       || ACCT_SIC_CD
       || '~|~'
       || ACCT_ROW_ID
       || '~|~'
       || ACCT_STS
       || '~|~'
       || ACCT_ALIAS
       || '~|~'
       || PGM_ROW_ID
       || '~|~'
       || SPNR_ACCT_NBR
       || '~|~'
       || WEX_ACCT_NBR
       || '~|~'
       || L1_HIER_LEVEL_LABEL
       || '~|~'
       || L2_HIER_LEVEL_LABEL
       || '~|~'
       || L3_HIER_LEVEL_LABEL
       || '~|~'
       || L4_HIER_LEVEL_LABEL
       || '~|~'
       || L5_HIER_LEVEL_LABEL
       || '~|~'
       || L6_HIER_LEVEL_LABEL
       || '~|~'
       || L7_HIER_LEVEL_LABEL
       || '~|~'
       || L8_HIER_LEVEL_LABEL
       || '~|~'
       || L9_HIER_LEVEL_LABEL
       || '~|~'
       || ALTERNATE_ID_2
       || '~|~'
       || CUSTOM_ACCT_NBR
       || '~|~'
       || BANK_ID_NBR
       || '~|~'
       || BUSINESS_HOURS
       || '~|~'
       || TO_CHAR (ACCT_FORMED_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (ACCT_INCORPORATED_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || FISCAL_START_DAY
       || '~|~'
       || FISCAL_START_MONTH
       || '~|~'
       || FRAUD_FLG
       || '~|~'
       || CRDT_LMT_AMT
       || '~|~'
       || ULT_PAR_ROW_ID
       || '~|~'
       || l1_wex_acct_nbr
       || '~|~'
       || HOME_PAGE
       || '~|~'
       || L1_acct_nm
       || '~|~'
       || FLA_PAR_ACCT_NBR
       || '~|~'
       || FLA_PAR_ACCT_NM
       || '~|~'
       || BILLING_PAR_ACCT_NBR
       || '~|~'
       || BILLING_PAR_ACCT_NM
       || '~|~'
       || REMIT_FROM_ACCT_NBR
       || '~|~'
       || MULTI_PROGRAM_FLG
       || '~|~'
       || REMIT_FROM_ACCT_NM
       || '~|~'
       || CREDIT_NA_FLG
       || '~|~'
       || GRP_PAR_ACCT_NBR
       || '~|~'
       || GRP_PAR_ACCT_NM
       || '~|~'
       || PRI_SERV_AGRMNT_NM
       || '~|~'
       || DUNS_NBR
       || '~|~'
       || INDUSTRY_NM
       || '~|~'
       || PGM_SHORT_NM
       || '~|~'
       || ''
       || '~|~'
       || FLA_PAR_ROW_ID
       || '~|~'
       || FLA_PAR_SPNR_ACCT_NBR
       || '~|~'
       || PRIMARY_ADDR_ROW_ID
       || '~|~'
       || BILL_ADDR_ROW_ID
       || '~|~'
       || PHYS_ADDR_ROW_ID
       || '~|~'
       || ADMIN_ADDR_ROW_ID
       || '~|~'
       || TAX_ADDR_ROW_ID
       || '~|~'
       || SALES_REP_CD
       || '~|~'
       || BILLING_PAR_ROW_ID
       || '~|~'
       || BILLING_PAR_SPNR_ACCT_NBR
       || '~|~'
       || SUSPENSE_CLS
       || '~|~'
       || RECOURSE_CD
       || '~|~'
       || SPEEDPASS
       || '~|~'
       || REMIT_FROM_ROW_ID
       || '~|~'
       || PYMT_METH
       || '~|~'
       || REMIT_FROM_SPNR_ACCT_NBR
       || '~|~'
       || SITE_DENIAL_WAIVER_FLG
       || '~|~'
       || FUNDER_CD
       || '~|~'
       || GRP_PAR_ROW_ID
       || '~|~'
       || GRP_PAR_SPNR_ACCT_NBR
       || '~|~'
       || PRIMARY_CONTACT_ROW_ID
       || '~|~'
       || PRIMARY_CONTACT_DEPT_CD
       || '~|~'
       || PRIMARY_CONTACT_DEPT_NM
       || '~|~'
       || PHYS_CONTACT_ROW_ID
       || '~|~'
       || BILL_CONTACT_ROW_ID
       || '~|~'
       || ADMIN_CONTACT_ROW_ID
       || '~|~'
       || TAX_CONTACT_ROW_ID
       || '~|~'
       || UNDERCOVER_FLG
       || '~|~'
       || VIP_FLG
       || '~|~'
       || ''
       || '~|~'
       || ''
       || '~|~'
       || HIER_LEVEL_NBR
       || '~|~'
       || HIER_PAR_ROW_ID
       || '~|~'
       || ACTV_DISC_FLG
       || '~|~'
       || FED_TAX_ID
       || '~|~'
       || TAX_EXEMPTION_STS
       || '~|~'
       || TAX_QUAL_FLG
       || '~|~'
       || TAX_ACCT_TYPE
       || '~|~'
       || FTP_SERVER
       || '~|~'
       || FTP_ACCOUNT
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || ''
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || CUSTOMER_TYPE
       || '~|~'
       || PFS_REP_CD
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (EFPS_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (ACCT_ORIG_ACTIV_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (RENEWAL_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || SERVICE_OPTION
       || '~|~'
       || TO_CHAR (SERVICE_OPTION_START_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || ACCT_ACCEPT_LVL
       || '~|~'
       || FULFIL_CARRIER
       || '~|~'
       || FULFIL_CARRIER_ACCT_NBR
       || '~|~'
       || SALES_REGION
       || '~|~'
       || REPORTING_PAR_FLG
       || '~|~'
       || BILLING_PAR_FLG
       || '~|~'
       || CROSS_ACCEPT_ACCT_NBR
       || '~|~'
       || CREDIT_NA_NBR
       || '~|~'
       || AUTO_PRMPT_CORR_FLG
       || '~|~'
       || RPT_SUBTOTAL_TYPE
       || '~|~'
       || CARD_TYPE_RESTRICTION
       || '~|~'
       || UDF1
       || '~|~'
       || UDF2
       || '~|~'
       || UDF3
       || '~|~'
       || UDF4
       || '~|~'
       || UDF5
       || '~|~'
       || UDF6
       || '~|~'
       || UDF7
       || '~|~'
       || UDF8
       || '~|~'
       || UDF9
       || '~|~'
       || UDF10
       || '~|~'
       || UDF11
       || '~|~'
       || UDF12
       || '~|~'
       || UDF13
       || '~|~'
       || UDF14
       || '~|~'
       || UDF15
       || '~|~'
       || UDF16
       || '~|~'
       || UDF17
       || '~|~'
       || UDF18
       || '~|~'
       || UDF19
       || '~|~'
       || UDF20
       || '~|~'
       || ACCTNG_FIELD_DEF_LVL
       || '~|~'
       || DIV_TO_ACCT_NBR
       || '~|~'
       || DIV_TO_ACCT_NM
       || '~|~'
       || DIV_TO_ROW_ID
       || '~|~'
       || DIV_TO_SPNR_ACCT_NBR
       || '~|~'
       || DIVERT_BY
       || '~|~'
       || CARD_SHIPPING_COMPANY_NAME
       || '~|~'
       || CARD_SHIPPING_ADDR_ROW_ID
       || '~|~'
       || CARD_SHIPPING_CONTACT_ROW_ID
       || '~|~'
       || SHARED_MODE
       || '~|~'
       || ''
       || '~|~'
       || ''
       || '~|~'
       || ''
       || '~|~'
       || ' '
       || '~|~'
       || TELEMATICS_PARTICIPATING_FLG
       || '~|~'
       || RTA_ENROLLMENT_ROOT_FLG
  FROM DW_ACCT D
 WHERE D.DW_CURRENT_FLG = 'Y' AND D.DW_ACCT_KEY <> 99 ; 


spool off ;     
  
