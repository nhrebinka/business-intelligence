

SET ECHO     ON 
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON 
SET HEADING   ON 
SET TRIMSPOOL ON
SET TAB       OFF
set LINESIZE  2000
SET TIMING ON 

spool /i1/&&env/hub/logs/dba_Data_load_monthly.log 


Insert /*+ Append  */  INTO F_MONTHLY_ACCOUNT_SNAPSHOT
( select *  from F_MONTHLY_ACCOUNT_SNAPSHOT@QTEDW.QAT ) ;

 COMMIT ;


spool off ; 