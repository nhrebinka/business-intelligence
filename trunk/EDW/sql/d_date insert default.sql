insert into wb_owner.d_date (
DATE_KEY,
VALID_REAL_DATE_FLG,
CALENDAR_DATE_DT,
DATE_TEXT,
DAY_OF_WEEK_NAME,
DAY_OF_WEEK_ABBR,
DAY_OF_WEEK_NUMBER,
SEQUENTIAL_DAY_NUMBER,
SEQUENTIAL_WEEK_NUMBER,
SEQUENTIAL_MONTH_NUMBER,
DAY_IN_MONTH_NUMBER,
WEEK_IN_MONTH_NUMBER,
DAY_IN_CALENDAR_YEAR_NUMBER,
WEEK_IN_CALENDAR_YEAR_NUMBER,
MONTH_IN_CALENDAR_YEAR_NUMBER,
LAST_DAY_IN_MONTH_FLG,
LAST_BUSINESS_DAY_IN_MONTH_FLG,
FIRST_DAY_IN_MONTH_DATE_DT,
LAST_DAY_IN_MONTH_DATE_DT,
LAST_BUSINESS_DAY_IN_MONTH_DT,
MONTH_NAME,
MONTH_ABBR,
MONTH_YEAR_ABBR,
MONTH_YEAR_NAME,
MONTH_NAME_PLUS_6,
MONTH_ABBR_PLUS_6,
MONTH_YEAR_NAME_PLUS_6,
MONTH_YEAR_ABBR_PLUS_6,
QUARTER_NAME,
QUARTER_ABBR,
QUARTER_NUMBER,
QUARTER_YEAR_NAME,
QUARTER_YEAR_ABBR,
YEAR_NAME,
YEAR_NAME_ABBR,
YEAR_NUMBER,
US_HOLIDAY_FLG,
CANADA_HOLIDAY_FLG,
WEEKDAY_FLG,
WEEKEND_FLG,
EQUIVALENT_FUEL_DAY_FACTOR,
ROW_CREATE_DTTM,
ROW_LAST_MOD_DTTM,
ROW_LAST_MOD_PROC_NM,
ROW_LAST_MOD_PROC_SEQ_NBR
)
values (
0,                                      -- DATE_KEY,
'N',                                    -- VALID_REAL_DATE_FLG,
TO_DATE('01/01/1900', 'MM/DD/YYYY'),    -- CALENDAR_DATE_DT,
'Unknown',                              -- DATE_TEXT,
'Unknown',                              -- DAY_OF_WEEK_NAME,
'Unknown',                              -- DAY_OF_WEEK_ABBR,
0,                                      -- DAY_OF_WEEK_NUMBER,
0,                                      -- SEQUENTIAL_DAY_NUMBER,
0,                                      -- SEQUENTIAL_WEEK_NUMBER,
0,                                      -- SEQUENTIAL_MONTH_NUMBER,
0,                                      -- DAY_IN_MONTH_NUMBER,
0,                                      -- WEEK_IN_MONTH_NUMBER,
0,                                      -- DAY_IN_CALENDAR_YEAR_NUMBER,
0,                                      -- WEEK_IN_CALENDAR_YEAR_NUMBER,
0,                                      -- MONTH_IN_CALENDAR_YEAR_NUMBER,
' ',                                    -- LAST_DAY_IN_MONTH_FLG,
' ',                                    -- LAST_BUSINESS_DAY_IN_MONTH_FLG,
TO_DATE('01/01/1900', 'MM/DD/YYYY'),    -- FIRST_DAY_IN_MONTH_DATE_DT,
TO_DATE('01/01/1900', 'MM/DD/YYYY'),    -- LAST_DAY_IN_MONTH_DATE_DT,
TO_DATE('01/01/1900', 'MM/DD/YYYY'),    -- LAST_BUSINESS_DAY_IN_MONTH_DT,
'Unknown',                              -- MONTH_NAME,
'Unknown',                              -- MONTH_ABBR,
'Unknown',                              -- MONTH_YEAR_ABBR,
'Unknown',                              -- MONTH_YEAR_NAME,
'Unknown',                              -- MONTH_NAME_PLUS_6,
'Unknown',                              -- MONTH_ABBR_PLUS_6,
'Unknown',                              -- MONTH_YEAR_NAME_PLUS_6,
'Unknown',                              -- MONTH_YEAR_ABBR_PLUS_6,
'Unknown',                              -- QUARTER_NAME,
'Unknown',                              -- QUARTER_ABBR,
0,                                      -- QUARTER_NUMBER,
'Unknown',                              -- QUARTER_YEAR_NAME,
'Unknown',                              -- QUARTER_YEAR_ABBR,
'Unknown',                              -- YEAR_NAME,
'00',                                   -- YEAR_NAME_ABBR,
1900,                                   -- YEAR_NUMBER,
' ',                                    -- US_HOLIDAY_FLG,
' ',                                    -- CANADA_HOLIDAY_FLG,
' ',                                    -- WEEKDAY_FLG,
' ',                                    -- WEEKEND_FLG,
0,                                      -- EQUIVALENT_FUEL_DAY_FACTOR,
sysdate,                                -- ROW_CREATE_DTTM,
sysdate,                                -- ROW_LAST_MOD_DTTM,
'Eric',                                 -- ROW_LAST_MOD_PROC_NM,
0                                       -- ROW_LAST_MOD_PROC_SEQ_NBR
);
