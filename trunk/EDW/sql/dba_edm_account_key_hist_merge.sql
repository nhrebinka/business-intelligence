SET ECHO    ON 
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON 
SET HEADING      ON 
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000
DEFINE &&env = &1

spool /i1/&&env/hub/logs/dba_edm_account_hist_merge.log ; 

MERGE INTO D_ACCOUNT D
     USING (  SELECT SOURCE_ACCOUNT_ID SOURCE_ACCOUNT_ID,
                     MIN (ACCOUNT_KEY) ACCOUNT_HIST_KEY
                FROM D_ACCOUNT
            GROUP BY SOURCE_ACCOUNT_ID) D1
        ON (D.SOURCE_ACCOUNT_ID = D1.SOURCE_ACCOUNT_ID)
WHEN MATCHED
THEN
   UPDATE SET
                    D.ACCOUNT_HIST_KEY = D1.ACCOUNT_HIST_KEY,
                    D.ROW_LAST_MOD_DTTM = SYSDATE,
                    D.ROW_LAST_MOD_PROC_NM = 'ACCOUNT_HIST_KEY DATA FIX  UPDATE ';
    
    commit ;
    
         
    
 
MERGE INTO  D_ACCOUNT D 
USING STAGE_OWNER.SRC_CLM_CLUSTER S 
 ON (D.SOURCE_ACCOUNT_ID = s.fleet_id )  
 when matched
 THEN 
   UPDATE SET 
      D.CLM_CLUSTER_NAME = s.CLUSTER1  , 
      D.ROW_LAST_MOD_DTTM = SYSDATE,
      D.ROW_LAST_MOD_PROC_NM = 'CLUSTER NAME  UPDATE '
                ;   

COMMIT ; 

 UPDATE D_ACCOUNT D 
 SET  D.CLM_CLUSTER_NAME = 'Unknown' 
 Where D.CLM_CLUSTER_NAME = 'N/A' ;
 
 
 COMMIT ; 
 
  MERGE INTO D_ACCOUNT D 
     USING
      (select 
                 ACCOUNT_KEY ,  
                 MD5HASH (
                TRIM(FUNDER_NAME)|| ' ' ||
                TRIM(FUNDING_TYPE_NAME)|| ' ' ||
                TRIM(ACCOUNT_REGION_CODE)|| ' ' ||
                TRIM(ACCOUNT_REGION_NAME)|| ' ' ||
                TRIM(ACCOUNT_DISTRICT_NAME)|| ' ' ||
                TRIM(ACCOUNT_DISTRICT_CODE)|| ' ' ||
                TRIM(ACCOUNT_SUBDISTRICT_NAME)|| ' ' ||
                TRIM(ACCOUNT_SUBDISTRICT_CODE)|| ' ' ||
                TRIM(ACCOUNT_POSTAL_CODE)|| ' ' ||
                TRIM(ACCOUNT_COUNTRY_NAME)|| ' ' ||
                TRIM(PROGRAM_NAME)|| ' ' ||
                TRIM(ACCOUNT_NAME)|| ' ' ||
                TRIM(ACCOUNT_STATUS_DESC)|| ' ' ||
                TRIM(DIVERSION_ACCOUNT_FLG)|| ' '||
                TRIM(BILLING_ACCOUNT_FLG)|| ' ' ||
                TRIM(RELATED_BILLING_ACCOUNT_ID)|| ' '||
                TRIM(TO_CHAR(SIC_LOOKUP_KEY)) || ' ' ||
                TRIM(ACCOUNT_MANAGER_NAME)|| ' ' ||
                TRIM(PURCHASE_ACCOUNT_FLG) || ' ' ||
                TRIM(CUSTOMER_NAME)|| ' ' ||
                TRIM(ATTRITION_REASON_DESC)|| ' ' ||
                TRIM(TO_CHAR(RISK_GRADE))|| ' ' ||
                TRIM(TO_CHAR(ACCOUNT_CLOSED_DATE , 'MM/DD/YYYY HH24:mi:SS' ))|| ' ' ||
                TRIM(ATTRITION_TYPE_NAME)|| ' ' ||
                TRIM(ATTRITION_REASON_CODE) || ' ' ||
                TRIM(SOURCE_SYSTEM_CODE) || ' ' ||
                TRIM(TAX_EXEMPT_FLG) || ' ' ||
                TRIM(CLM_CLUSTER_NAME) ) HASH_1   
    FROm D_ACCOUNT )  D1   
ON (  D.ACCOUNT_KEY = D1.ACCOUNT_KEY )  
when matched
THEN 
  UPDATE SET 
        D.ROW_MD5_CHECKSUM_HEX_T2      = D1.HASH_1 , 
        D.ROW_LAST_MOD_DTTM            = SYSDATE,
        D.ROW_LAST_MOD_PROC_NM         = 'ROW_MD5_CHECKSUM_HEX_T2 UPDATE BY SQL ' ; 
        
        
COMMIT ; 
           
spool OFF  ;



 
