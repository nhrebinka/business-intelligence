SET ECHO	ON 
SET NEWPAGE 	0
SET SPACE 	0
SET PAGESIZE 	0
SET FEEDBACK 	ON 
SET HEADING  	ON 
SET TRIMSPOOL 	ON
SET TAB         OFF
set LINESIZE    2000



spool /i1/&&env/hub/logs/dba_national_id_merge.log ; 
  

merge  into M_acct M  using STAGE_OWNER.SRC_TDM_FLT0OPT S 
 on  ( M.WEX_ACCT_NBR = S.WEX_ACCT_NBR) 
 WHEN MATCHED THEN 
   UPDATE SET M.NATIONAL_ACCOUNT_ID =  S.NATIONAL_FLEET_ID  ,
                      M.EDM_LAST_UPDT_DT   = sysdate  ,
                      M.EDM_LAST_UPDT_SESSION_NM  = 'Data Fix for NATIONAL FLEET IF update from full Fleet options' ;  


  
spool off ; 
    

  


