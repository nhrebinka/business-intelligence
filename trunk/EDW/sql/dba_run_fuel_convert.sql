/* Formatted on 10/14/2010 1:40:05 PM (QP5 v5.163.1008.3004) */
SET ECHO ON
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB ON
SET LINESIZE 2000

SPOOL /i1/&&env/hub/logs/dba_run_fuel_convert.log;

/*  NOTE fOR  HISTORY LOADING CARD suffix SHOULD NOT BE PART OF THE wex_trans_id SINCE  cARD SUFFIX IS NOT AVAILABLE IN tax TABLE */

INSERT                                                           /*+ append */
      INTO                SRC_TDM_FUL1MST_HIST
   (SELECT S.COMPANY_ID,
           (   S.CLIENT_MERCHANT
            || LPAD (S.CLIENT_STATE, 2, 0)
            || LPAD (S.CLIENT_ZEROES, 2, 0)
            || LPAD (S.ACCOUNT_NUMBER, 6, 0)
            || S.CHECK_DIGIT
            || LPAD (S.VEHICLE_NUMBER, 4, 0)
            || S.TRANSACTION_DATE
            || S.TRANSACTION_TIME
            || S.TICKET_NUMBER)
              WEX_TRANSACTION_ID,
           SUBSTR(S.MULTI_TRAN_FLAG,1,1)  WEX_TRANSACTION_SEQ_NBR,
           S.CLIENT_MERCHANT,
           S.CLIENT_STATE,
           S.CLIENT_ZEROES,
           S.ACCOUNT_NUMBER,
           S.CHECK_DIGIT,
           S.VEHICLE_NUMBER,
           S.CARD_SUFFIX,
           S.TRANSACTION_DATE,
           S.TRANSACTION_TIME,
           S.TICKET_NUMBER,
           S.MERCHANT_PREFIX,
           S.MERCHANT_SETTLE_ENTITY,
           S.WEX_SITE_NUMBER,
              S.MERCHANT_PREFIX
           || LPAD (S.MERCHANT_SETTLE_ENTITY, 3, 0)
           || LPAD (S.WEX_SITE_NUMBER, 3, 0)
              LEGACY_WEX_SITE_ID,
           S.PLX_VEHICLE_CARD_TYPE,
           S.POSTING_DATE,
           '' BATCH_NUM,
           '' DRIVER_NUMBER,
           S.PIN_PLAIN,
           '' DRIVER_LAST_NAME,
           '' DRIVER_FIRST_NAME,
           '' DRIVER_MIDDLE_INIT,
           S.PRODUCT_CODE,
           '' AUTHORIZATION_CODE,
           S.ODOMETER,
           S.CONTRACT_PRICE_FLAG,
           S.GALLONS_UNITS,
           S.GROSS_DOLLARS,
           S.VOLUME_DISCOUNT_AMT,
           S.WEX_FEE_AMOUNT,
           '' FEDERAL_TAX,
           '' STATE_TAX,
           '' OTHER_TAX,
           '' FEDERAL_TAX_RATE,
           '' STATE_TAX_RATE,
           '' OTHER_TAX_RATE,
           S.EFPS_DISCOUNT_AMT,
           '' VOL_DISCOUNT_PCT,
           S.NET_TRAN_TYPE,
           S.WEX_TRANSACTION_CODE,
           S.CONTROL_CODE,
           '' GE_SALE_CODE,
           '' TKT_REQUEST_FLAG,
           S.MULTI_TRAN_FLAG,
           '' BANK_FIID,
           S.NETWORK_IDENTIFIER,
           '' TAX_EXEMPT,
           S.STATE_CODE,
           S.CYCLE_CODE,
           S.CYCLE_PROCESS_FLAG,
           '' DEPARTMENT_NUMBER,
           S.MICROFILM_NUMBER,
           '' ACH_TRANS,
           '' CONVERSION_TYPE,
           S.FUELING_TYPE,
           S.AR_CARRIED_FLAG,
           S.ADJUSTED_ODOMETER,
           S.ADJ_ODOM_FLAG,
           S.SETTLE_ENTITY,
           '' FEDERAL_TAX_FLAG,
           '' STATE_TAX_FLAG,
           ' ' OTHER_TAX_FLAG,
           0 NUM_FUL0MTX_RECS,
           '' TAX_BILLING_FLAG,
           '' TAX_PROD_CODE,
           S.PPG_DEFAULT_FLAG,
           S.CURRENCY_CODE,
           S.EXCHANGE_RATE,
           '' IMPERIAL_FUEL_TYPE,
           '' TAX_INCLUSIVE_FLAG,
           ' ' TAX_TYPE_FLAG,
           S.ADDITIONAL_PROMPT_1,
           S.ADDITIONAL_PROMPT_2,
           '' SERVICE_LEVEL
      FROM STAGE_OWNER.FUEL_MASTER_&partname S);

COMMIT ; 

SPOOL OFF;
