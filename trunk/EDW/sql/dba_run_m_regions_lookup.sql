SPOOL  /i1/&&env/edm/logs/m_regions_init.sql.log  ; 

insert into m_regions values (1,'AK','West','WE','West Coast','WCST','West Coast','WCST'                ,'ALASKA'      , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (2,'AL','Southeast','SE','Gulf Coast','GCST','Gulf Coast','GCST'           ,'ALABAMA'     , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (3,'AR','Southeast','SE','Gulf Coast','GCST','Gulf Coast','GCST'           ,'ARKANSAS'    , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (4,'AZ','SouthWest','SW','West Coast','WCST','West Coast','WCST'           ,'ARIZONA'     , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (5,'CA','West','WE','West Coast','WCST','West Coast','WCST'                ,'CALIFORNIA'  , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (6,'CO','West','WE','Rocky Mountain','RMTN','Rocky Mountain','RMTN'        ,'COLORADO'    , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (7,'CT','Northeast','NE','East Coast','ECST','New England','NENG'          ,'CONNECTICUT' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (8,'DC','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'     ,'DISTRICT OF COLUMBIA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (9,'DE','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'     ,'DELAWARE' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (10,'FL','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'FLORIDA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (11,'GA','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'GEORGIA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (12,'HI','West','WE','West Coast','WCST','West Coast','WCST'               ,'HAWAII' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (13,'IA','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'IOWA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (14,'ID','West','WE','Rocky Mountain','RMTN','Rocky Mountain','RMTN'       ,'IDAHO' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (15,'IL','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'ILLINOIS' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (16,'IN','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'INDIANA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (17,'KS','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'KANSAS' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (18,'KY','Southeast','SE','Midwest','MWST','Midwest','MWST'                ,'KENTUCKY' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (19,'LA','Southeast','SE','Gulf Coast','GCST','Gulf Coast','GCST'          ,'LOUISIANA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (20,'MA','Northeast','NE','East Coast','ECST','New England','NENG'         ,'MASSACHUSETTS' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (21,'MD','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'    ,'MARYLAND' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (22,'ME','Northeast','NE','East Coast','ECST','New England','NENG'         ,'MAINE' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (23,'MI','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'MICHIGAN' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (24,'MN','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'MINNESOTA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (25,'MO','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'MISSOURI' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (26,'MS','Southeast','SE','Gulf Coast','GCST','Gulf Coast','GCST'          ,'MISSISSIPPI' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (27,'MT','West','WE','Rocky Mountain','RMTN','Rocky Mountain','RMTN'       ,'MONTANA'     , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (28,'NC','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'NORTH CAROLINA', 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (29,'ND','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'NORTH DAKOTA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (30,'NE','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'NEBRASKA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (31,'NH','Northeast','NE','East Coast','ECST','New England','NENG'         ,'NEW HAMPSHIRE' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (32,'NJ','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'    ,'NEW JERSEY' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (33,'NM','SouthWest','SW','Gulf Coast','GCST','Gulf Coast','GCST'          ,'NEW MEXICO' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (34,'NV','West','WE','West Coast','WCST','West Coast','WCST'               ,'NEVADA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (35,'NY','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'    ,'NEW YORK' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (36,'OH','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'OHIO' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (37,'OK','SouthWest','SW','Midwest','MWST','Midwest','MWST'                ,'OKLAHOMA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (38,'OR','West','WE','West Coast','WCST','West Coast','WCST'               ,'OREGON' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (39,'PA','Northeast','NE','East Coast','ECST','Central Atlantic','CATL'    ,'PENNSYLVANIA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (40,'PR','Southeast','SE','Puerto Rico','PUER','Puerto Rico','PUER'        ,'PUERTO RICO' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (41,'RI','Northeast','NE','East Coast','ECST','New England','NENG'         ,'RHODE ISLAND' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (42,'SC','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'SOUTH CAROLINA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (43,'SD','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'SOUTH DAKOTA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (44,'TN','Southeast','SE','Midwest','MWST','Midwest','MWST'                ,'TENNESSEE' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (45,'TX','SouthWest','SW','Gulf Coast','GCST','Gulf Coast','GCST'          ,'TEXAS' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (46,'UT','West','WE','Rocky Mountain','RMTN','Rocky Mountain','RMTN'       ,'UTAH' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (47,'VA','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'VIRGINIA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (48,'VT','Northeast','NE','East Coast','ECST','New England','NENG'         ,'VERMONT' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (49,'WA','West','WE','West Coast','WCST','West Coast','WCST'               ,'WASHINGTON' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (50,'WI','Midwest','MW','Midwest','MWST','Midwest','MWST'                  ,'WISCONSIN' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (51,'WV','Southeast','SE','East Coast','ECST','Lower Atlantic','LATL'      ,'WEST VIRGINIA' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (52,'WY','West','WE','Rocky Mountain','RMTN','Rocky Mountain','RMTN'       ,'WYOMING' , 'UNITED STATES OF AMERICA' , 'US', sysdate , sysdate , 'ONE SHOT' );

insert into m_regions values (53,'BC','Western Canada','WC','Pacific Coast','PCST','British Columbia','BCOL'                  ,'BRITISH COLUMBIA' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (54,'AB','Western Canada','WC','Prairies','PRAR','Alberta','ALBA'                                ,'ALBERTA' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (55,'SK','Western Canada','WC','Prairies','PRAR','Saskatchewan','SASK'                           ,'SASKATCHEWAN' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (56,'MB','Western Canada','WC','Prairies','PRAR','Manitoba','MANI'                               ,'MANITOBA' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (57,'ON','Eastern Canada','EC','Central Canada','CCAN','Ontario','ONTA'                          ,'ONTARIO' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (58,'QC','Eastern Canada','EC','Central Canada','CCAN','Quebec','QUEB'                           ,'QUEBEC' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (59,'NB','Eastern Canada','EC','Atlantic Canada','ACAN','New Brunswick','NBRN'                   ,'NEW BRUNSWICK' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (60,'PE','Eastern Canada','EC','Atlantic Canada','ACAN','Prince Edward Island','PEDI'            ,'PRINCE EDWARD ISLAND' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (61,'NS','Eastern Canada','EC','Atlantic Canada','ACAN','Nova Scotia','NSCO'                     ,'NOVA SCOTIA' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (62,'NL','Eastern Canada','EC','Atlantic Canada','ACAN','Newfoundland and Labrador','NFLB'       ,'NEWFOUNDLAND AND LABRADOR' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (63,'YT','Northern Canada','NC','Northern Canada','NCAN','Yukon','YUKN'                          ,'YUKON' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (64,'NT','Northern Canada','NC','Northern Canada','NCAN', 'Northwest Territories Nunavut','NWTN' ,'NORTHWEST TERRITORIES' ,'CANADA' , 'CA', sysdate , sysdate , 'ONE SHOT' );

insert into m_regions values (65,'GU','International','GU','International','INTL', 'International','INTL' ,'Guam'                      ,'Guam'                      , 'GU', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (66,'VI','International','VI','International','INTL', 'International','INTL' ,'Virgin Islands'            ,'Virgin Islands'            , 'VI', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (67,'AS','International','AS','International','INTL', 'International','INTL' ,'American Samoa'            ,'American Samoa'            , 'AS', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (68,'AP','International','AP','International','APO' , 'International','APO'  ,'Armed Service Post Office' ,'Armed Service Post Office' , 'AP', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (69,'MP','International','MP','International','INTL', 'International','INTL' ,'Mariana Islands'           ,'Mariana Islands'           , 'MP', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (70,'AE','International','AE','International','APO' , 'International','APO'  ,'Armed Service Post Office' ,'Armed Service Post Office' , 'AP', sysdate , sysdate , 'ONE SHOT' );
insert into m_regions values (71,'AA','International','AA','International','APO' , 'International','APO'  ,'Armed Service Post Office' ,'Armed Service Post Office' , 'AP', sysdate , sysdate , 'ONE SHOT' );




commit ; 

SPOOL OFF ;


  