SET ECHO     ON 
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON 
SET HEADING   ON 
SET TRIMSPOOL ON
SET TAB       OFF
set LINESIZE  2000

spool /i1/&&env/hub/logs/dba_truncate_f_daily_acct_2011.log ; 
   

EXEC F_DAILY_ACCOUNT_AGGREGATES_H ( '01-APR-2011' , '21-APR-2011' ) ; 

EXEC F_MONTHLY_ACCOUNT_AGGREGATES_H ( '01-APR-2011' , '30-APR-2011' ) ; 
      
spool off ;  