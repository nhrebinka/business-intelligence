/*
REM   Filename : cr_pub_synonym.sql		
REM 
REM
REM  Description: Generates script that which creates public synonyms for
REM    	          all tables in  wb_owner  schema for ?WB
REM 
REM  Log: Created:  10/29/2004   
REM  log        :  Changed by venu on 02/2/2005
REM                to write the dynamic scripts to i1/logs/wb and incluse the  prefix .
REM                changed by venu 0n 02/10/2005 
REM                to include owner prefix before the objects 
REM                modified for HUB on 09/24/2010 by Venu.
REM**********************************************************************/  



set heading off ;
set feedback off ; 
set linesize 180 ; 
set time off
set timing off
set echo off 


spool /i1/&&env/hub/logs/public_synonym.sql ;

select  'create or replace public synonym ' || uo.object_name || 
 ' ' || '  for ' ||uo.owner||'.'||uo.object_name || ';' 
from all_objects  uo , user_users uu 
 where uo.object_type in ('TABLE','FUNCTION','PROCEDURE', 'PACKAGE','SEQUENCE')
and uo.owner = uu.username 
/

spool off ; 

set linesize 80 ;
set feedback on ; 
set heading on  ; 
set echo on ;

spool /i1/&&env/hub/logs/public_synonym.log;

start /i1/&&env/hub/logs/public_synonym.sql;

spool off
