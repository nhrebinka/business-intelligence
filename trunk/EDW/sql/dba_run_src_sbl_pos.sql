SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_pos.dat;

SELECT                                             /*+parallel (dw_POS , 4) */
      Dw_POS_KEY
       || '~|~'
       || GRP_MRCH_ID
       || '~|~'
       || POS_ID
       || '~|~'
       || SITE_ROW_ID
       || '~|~'
       || POS_ROW_ID
       || '~|~'
       || GRP_MRCH_NM
       || '~|~'
       || MRCH_ID
       || '~|~'
       || MRCH_NM
       || '~|~'
       || POS_NAME
       || '~|~'
       || POS_STS
       || '~|~'
       || POS_STS_RSN
       || '~|~'
       || FUTURE_POS_STS
       || '~|~'
       || FUTURE_POS_STS_RSN
       || '~|~'
       || TO_CHAR (FUTURE_STS_RSN_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || AUTH_NETWORK
       || '~|~'
       || AUTH_VERSION
       || '~|~'
       || TO_CHAR (LAST_AUTH_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (LAST_POSTING_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || POST_NETWORK
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || REPORT_1
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || REPORT_2
       || '~|~'
       || REPORT_3
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || MRCH_LOC_NM
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || ''
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || POS_TYPE
       || '~|~'
       || LEGACY_WEX_SITE_ID      
  FROM DW_POS
 WHERE DW_CURRENT_FLG = 'Y';


spool off ; 

