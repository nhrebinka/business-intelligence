SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_site.dat;

SELECT                                           /*+parallel (dw_site  , 4) */
      Dw_SITE_KEY
       || '~|~'
       || SITE_ROW_ID
       || '~|~'
       || STKHLDR_ROW_ID
       || '~|~'
       || WEX_SITE_ID
       || '~|~'
       || MRCH_NM
       || '~|~'
       || MRCH_SITE_ID
       || '~|~'
       || MINORITY_OWNED_CD
       || '~|~'
       || PRI_AUTH_METH
       || '~|~'
       || LANGUAGE_CD
       || '~|~'
       || CANOPY_HEIGHT
       || '~|~'
       || SITE_NM
       || '~|~'
       || SITE_TYPE
       || '~|~'
       || SITE_FUEL_TYPE
       || '~|~'
       || TRADE_CLS
       || '~|~'
       || CURRENCY_CD
       || '~|~'
       || DAYLIGHT_SAVINGS_TM_FLG
       || '~|~'
       || EMAIL_ADDR
       || '~|~'
       || TAX_NONEXEMPT_FLG
       || '~|~'
       || GEO_CD_LAT
       || '~|~'
       || HOURS_OF_OPERATION
       || '~|~'
       || SIC_CD
       || '~|~'
       || INDUSTRY_TYPE
       || '~|~'
       || FED_SITE_TAX_ID
       || '~|~'
       || SITE_DIRECTORY_CD
       || '~|~'
       || INTL_UOM
       || '~|~'
       || FUTURE_SITE_STS
       || '~|~'
       || TO_CHAR (FUTURE_STS_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || FUTURE_STKHLDR_STS_RSN
       || '~|~'
       || SITE_STS
       || '~|~'
       || TURNING_RADIUS
       || '~|~'
       || SITE_STATUS_RSN
       || '~|~'
       || TIME_ZONE
       || '~|~'
       || GEO_CD_LONG
       || '~|~'
       || PRIMARY_ADDR_ROW_ID
       || '~|~'
       || PHYS_ADDR_ROW_ID
       || '~|~'
       || CONTACT_ROW_ID
       || '~|~'
       || FRAUD_FLG
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || ''
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || BRAND_OWNER_ROW_ID
       || '~|~'
       || LEGAL_OWNER_ROW_ID
       || '~|~'
       || REPORT_GROUPING
  FROM dw_site
 WHERE dw_current_flg = 'Y' AND DW_SITE_KEY <> '99';

spool off;

