SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/best_for_less_program_fix.log ; 


UPDATE M_SPNR_PGM_XREF
SET    EDM_SPNR_PGM_KEY = 1953
WHERE  SPNR_PGM_PK = 'AF26';
          
COMMIT;
        
        
UPDATE M_ACCT A
SET    A.EDM_SPNR_PGM_KEY = 1953
WHERE  A.WEX_ACCT_NBR IN ( SELECT DISTINCT WEX_ACCT_NBR FROM STAGE_OWNER.SRC_TDM_PLS0CTL
                            WHERE PLASTIC_TYPE_01 = 'AF26');
                                                     
COMMIT;


spool off;