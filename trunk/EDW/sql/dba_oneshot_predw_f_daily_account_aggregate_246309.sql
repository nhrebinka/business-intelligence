SET ECHO ON
SET SERVEROUTPUT ON 
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK ON
SET HEADING ON
SET TRIMSPOOL ON
SET TAB OFF
SET ESCAPE ON
SET LINESIZE 2000
SET SPOOL ON 

SPOOL /i1/&env/hub/logs/dba_oneshot_predw_f_daily_account_aggregate_246309.log ;


UPDATE  F_DAILY_ACCOUNT_SNAPSHOT 
                                 SET  PROGRAM_KEY 	      = 70   , 
                                      ROW_LAST_MOD_PROC_NM    = 'ONE SHOT DATA FIX HT 246309' , 
                                      ROW_LAST_MOD_DTTM       = SYSDATE  
                              where 
                                    account_key =  8374265
                               and PROGRAM_KEY = 701   ;  
                              
  COMMIT ; 
  
  -- will update  16 records                              
                               
UPDATE F_PURCHASE_DEVICE_EVENT F 
     SET F.PROGRAM_KEY 		   = 70 , 
         F.ROW_LAST_MOD_PROC_NM    = 'ONE SHOT DATA FIX HT 246309' , 
         F.ROW_LAST_MOD_DTTM       = SYSDATE  
  where 
         F.account_key    = 8374265
     AND F.PROGRAM_KEY    = 701   
   ;
   
   -- will update 1 record ... 
   
   COMMIT ; 
                                 
                               
 
SPOOL OFF;
