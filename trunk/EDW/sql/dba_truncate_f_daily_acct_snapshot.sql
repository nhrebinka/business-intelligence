SET ECHO    ON 
SET NEWPAGE     0
SET SPACE     0
SET PAGESIZE     0
SET FEEDBACK     ON 
SET HEADING      ON 
SET TRIMSPOOL     ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/dba_truncate_f_daily_acct_snapshot.log ; 
   

TRUNCATE TABLE  F_DAILY_ACCOUNT_SNAPSHOT  ; 

spool off ;  