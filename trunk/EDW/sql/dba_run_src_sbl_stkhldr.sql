

SET ECHO OFF
SET NEWPAGE 0
SET SPACE 0
SET PAGESIZE 0
SET FEEDBACK OFF
SET HEADING OFF
SET TRIMSPOOL ON
SET TAB OFF
set LINESIZE 2000

spool /i1/&&env/hub/data/src_sbl_stkhldr.dat;

SELECT                                        /*+parallel (dw_stkhldr  , 4) */
      Dw_STKHLDR_KEY
       || '~|~'
       || STKHLDR_ROW_ID
       || '~|~'
       || STKHLDR_ID
       || '~|~'
       || STKHLDR_TYPE
       || '~|~'
       || STKHLDR_STS
       || '~|~'
       || STKHLDR_STATUS_RSN
       || '~|~'
       || DBA_SYNONYM
       || '~|~'
       || STKHLDR_PARENT_ROW_ID
       || '~|~'
       || LANGUAGE_CD
       || '~|~'
       || TO_CHAR (FUTURE_STS_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || FED_TAX_ID
       || '~|~'
       || PRIMARY_ADDR_ROW_ID
       || '~|~'
       || BILL_ADDR_ROW_ID
       || '~|~'
       || PHYS_ADDR_ROW_ID
       || '~|~'
       || PRIMARY_CONTACT_ROW_ID
       || '~|~'
       || BILL_CONTACT_ROW_ID
       || '~|~'
       || PHYS_CONTACT_ROW_ID
       || '~|~'
       || STKHLDR_NM
       || '~|~'
       || TO_CHAR (DW_CREATE_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || TO_CHAR (DW_LAST_UPDT_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || DW_LAST_UPDT_SESSION_NM
       || '~|~'
       || DW_SOURCE_SYS
       || '~|~'
       || ''
       || '~|~'
       || TO_CHAR (DW_EFF_DT, 'MM/DD/YYYY hh24:mi:ss')
       || '~|~'
       || FRAUD_FLG
       || '~|~'
       || STKHLDR_LVL_NM
       || '~|~'
       || FUTURE_STKHLDR_STS
       || '~|~'
       || FUTURE_STKHLDR_STATUS_RSN
       || '~|~'
       || BRAND_NM
       || '~|~'
       || CUSTOM_STKHLDR_NBR
       || '~|~'
       || MRCH_PGM_ROW_ID
  FROM DW_STKHLDR
 WHERE DW_CURRENT_FLG = 'Y' AND DW_STKHLDR_KEY <> 99;

spool off;


