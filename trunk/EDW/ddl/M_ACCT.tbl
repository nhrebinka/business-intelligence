SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON
DEFINE &&env = &1

SPOOL /i1/&&env/hub/logs/M_ACCT.tbl.log ;

ALTER TABLE M_ACCT
 DROP PRIMARY KEY CASCADE;

DROP TABLE M_ACCT CASCADE CONSTRAINTS;

CREATE TABLE M_ACCT
(
  EDM_ACCT_KEY                   NUMBER(38)     NOT NULL,
  ACCT_NM                        VARCHAR2(100 BYTE) NOT NULL,
  ACCT_SHORT_NM                  VARCHAR2(30 BYTE),
  ACCT_TYPE                      VARCHAR2(50 BYTE),
  ACCT_STS_RSN                   VARCHAR2(50 BYTE),
  ACCT_SIC_CD                    VARCHAR2(30 BYTE),
  ACCT_STS                       VARCHAR2(24 BYTE) NOT NULL,
  ACCT_ALIAS                     VARCHAR2(50 BYTE),
  EDM_SPNR_PGM_KEY               NUMBER(38),
  SPNR_ACCT_NBR                  VARCHAR2(30 BYTE),
  WEX_ACCT_NBR                   VARCHAR2(15 BYTE) NOT NULL,
  ALTERNATE_ID_2                 VARCHAR2(30 BYTE),
  CUSTOM_ACCT_NBR                VARCHAR2(30 BYTE),
  BANK_ID_NBR                    VARCHAR2(50 BYTE),
  BUSINESS_HOURS                 VARCHAR2(30 BYTE),
  ACCT_FORMED_DT                 DATE,
  ACCT_INCORPORATED_DT           DATE,
  FISCAL_START_DAY               VARCHAR2(30 BYTE),
  FISCAL_START_MONTH             VARCHAR2(10 BYTE),
  FRAUD_FLG                      VARCHAR2(1 BYTE),
  CRDT_LMT_AMT                   NUMBER(12),
  ULT_PAR_EDM_ACCT_KEY           NUMBER(38),
  ULT_PAR_ACCT_NBR               VARCHAR2(15 BYTE),
  HOME_PAGE                      VARCHAR2(100 BYTE),
  ULT_PAR_ACCT_NM                VARCHAR2(100 BYTE),
  FLA_PAR_ACCT_NBR               VARCHAR2(15 BYTE) NOT NULL,
  FLA_PAR_ACCT_NM                VARCHAR2(100 BYTE),
  BILLING_PAR_ACCT_NBR           VARCHAR2(15 BYTE),
  BILLING_PAR_ACCT_NM            VARCHAR2(100 BYTE),
  REMIT_FROM_ACCT_NBR            VARCHAR2(15 BYTE),
  MULTI_PROGRAM_FLG              CHAR(1 BYTE),
  REMIT_FROM_ACCT_NM             VARCHAR2(100 BYTE),
  CREDIT_NA_FLG                  CHAR(1 BYTE),
  GRP_PAR_ACCT_NBR               VARCHAR2(15 BYTE),
  GRP_PAR_ACCT_NM                VARCHAR2(100 BYTE),
  PRI_SERV_AGRMNT_NM             VARCHAR2(50 BYTE),
  DUNS_NBR                       VARCHAR2(15 BYTE),
  INDUSTRY_NM                    VARCHAR2(50 BYTE),
  PGM_SHORT_NM                   VARCHAR2(50 BYTE),
  ULT_PAR_SPNR_ACCT_NBR          VARCHAR2(30 BYTE),
  FLA_PAR_EDM_ACCT_KEY           NUMBER(38)     NOT NULL,
  FLA_PAR_SPNR_ACCT_NBR          VARCHAR2(30 BYTE),
  PRIMARY_EDM_ADDR_KEY           NUMBER(38),
  BILL_EDM_ADDR_KEY              NUMBER(38),
  PHYS_EDM_ADDR_KEY              NUMBER(38),
  ADMIN_EDM_ADDR_KEY             NUMBER(38),
  TAX_EDM_ADDR_KEY               NUMBER(38),
  SALES_REP_CD                   VARCHAR2(50 BYTE),
  BILLING_PAR_EDM_ACCT_KEY       NUMBER(38),
  BILLING_PAR_SPNR_ACCT_NBR      VARCHAR2(30 BYTE),
  SUSPENSE_CLS                   VARCHAR2(20 BYTE),
  RECOURSE_CD                    VARCHAR2(30 BYTE),
  SPEEDPASS                      VARCHAR2(1 BYTE),
  REMIT_FROM_EDM_ACCT_KEY        NUMBER(38),
  PYMT_METH                      VARCHAR2(20 BYTE),
  REMIT_FROM_SPNR_ACCT_NBR       VARCHAR2(30 BYTE),
  SITE_DENIAL_WAIVER_FLG         CHAR(1 BYTE),
  FUNDER_CD                      VARCHAR2(50 BYTE) NOT NULL,
  GRP_PAR_EDM_ACCT_KEY           NUMBER(38),
  GRP_PAR_SPNR_ACCT_NBR          VARCHAR2(30 BYTE),
  PRIM_CONTACT_EDM_CONTACT_KEY   NUMBER(38),
  PRIMARY_CONTACT_DEPT_CD        VARCHAR2(20 BYTE),
  PRIMARY_CONTACT_DEPT_NM        VARCHAR2(50 BYTE),
  PHYS_EDM_CONTACT_KEY           NUMBER(38),
  BILL_EDM_CONTACT_KEY           NUMBER(38),
  ADMIN_EDM_CONTACT_KEY          NUMBER(38),
  TAX_EDM_CONTACT_KEY            NUMBER(38),
  UNDERCOVER_FLG                 CHAR(1 BYTE),
  VIP_FLG                        CHAR(1 BYTE),
  SALES_REP_FIRST_NM             VARCHAR2(50 BYTE),
  SALES_REP_LAST_NAME            VARCHAR2(50 BYTE),
  HIER_LEVEL_NBR                 NUMBER(4),
  HIER_PAR_KEY                   NUMBER(38),
  ACTV_DISC_FLG                  VARCHAR2(1 BYTE),
  FED_TAX_ID                     VARCHAR2(30 BYTE),
  TAX_EXEMPTION_STS              VARCHAR2(50 BYTE),
  TAX_QUAL_FLG                   VARCHAR2(1 BYTE),
  TAX_ACCT_TYPE                  VARCHAR2(50 BYTE),
  FTP_SERVER                     VARCHAR2(50 BYTE),
  FTP_ACCOUNT                    VARCHAR2(50 BYTE),
  CUSTOMER_TYPE                  VARCHAR2(50 BYTE),
  PFS_REP_CD                     VARCHAR2(3 BYTE),
  STATUS_CHANGE_DT               DATE,
  EFPS_EFF_DT                    DATE,
  ACCT_ORIG_ACTIV_DT             DATE,
  RENEWAL_DT                     DATE,
  SERVICE_OPTION                 VARCHAR2(30 BYTE),
  SERVICE_OPTION_START_DT        DATE,
  ACCT_ACCEPT_LVL                VARCHAR2(30 BYTE),
  FULFIL_CARRIER                 VARCHAR2(100 BYTE),
  FULFIL_CARRIER_ACCT_NBR        VARCHAR2(15 BYTE),
  SALES_REGION                   VARCHAR2(30 BYTE),
  REPORTING_PAR_FLG              CHAR(1 BYTE),
  BILLING_PAR_FLG                CHAR(1 BYTE),
  CROSS_ACCEPT_ACCT_NBR          VARCHAR2(15 BYTE),
  CREDIT_NA_NBR                  VARCHAR2(15 BYTE),
  AUTO_PRMPT_CORR_FLG            CHAR(1 BYTE),
  RPT_SUBTOTAL_TYPE              VARCHAR2(30 BYTE),
  CARD_TYPE_RESTRICTION          VARCHAR2(50 BYTE),
  UDF1                           VARCHAR2(50 BYTE),
  UDF2                           VARCHAR2(50 BYTE),
  UDF3                           VARCHAR2(50 BYTE),
  UDF4                           VARCHAR2(50 BYTE),
  UDF5                           VARCHAR2(50 BYTE),
  UDF6                           VARCHAR2(50 BYTE),
  UDF7                           VARCHAR2(50 BYTE),
  UDF8                           VARCHAR2(50 BYTE),
  UDF9                           VARCHAR2(50 BYTE),
  UDF10                          VARCHAR2(50 BYTE),
  UDF11                          VARCHAR2(50 BYTE),
  UDF12                          VARCHAR2(50 BYTE),
  UDF13                          VARCHAR2(50 BYTE),
  UDF14                          VARCHAR2(50 BYTE),
  UDF15                          VARCHAR2(50 BYTE),
  UDF16                          VARCHAR2(50 BYTE),
  UDF17                          VARCHAR2(50 BYTE),
  UDF18                          VARCHAR2(50 BYTE),
  UDF19                          VARCHAR2(50 BYTE),
  UDF20                          VARCHAR2(50 BYTE),
  ACCTNG_FIELD_DEF_LVL           VARCHAR2(10 BYTE),
  DIV_TO_ACCT_NBR                VARCHAR2(15 BYTE),
  DIV_TO_ACCT_NM                 VARCHAR2(100 BYTE),
  DIV_TO_EDM_ACCT_KEY            NUMBER(38),
  DIV_TO_SPNR_ACCT_NBR           VARCHAR2(30 BYTE),
  DIVERT_BY                      VARCHAR2(3 BYTE),
  CARD_SHIPPING_COMPANY_NAME     VARCHAR2(100 BYTE),
  CARD_SHIPPING_EDM_ADDR_KEY     NUMBER(38),
  CARD_SHIPPING_EDM_CONTACT_KEY  NUMBER(38),
  SHARED_MODE                    VARCHAR2(20 BYTE),
  BANK_ACCT_NBR                  VARCHAR2(50 BYTE),
  BANK_NAME                      VARCHAR2(50 BYTE),
  BANK_ROUTING_NBR               VARCHAR2(50 BYTE),
  TELEMATICS_PARTICIPATING_FLG   CHAR(1 BYTE),
  NATIONAL_ACCOUNT_ID            VARCHAR2(30 BYTE),
  EDM_CREATE_DT                  DATE           NOT NULL,
  EDM_LAST_UPDT_DT               DATE           NOT NULL,
  EDM_LAST_UPDT_SESSION_NM       VARCHAR2(100 BYTE) NOT NULL,
  SRC_SYS_LAST_UPDT_DT           DATE           NOT NULL,
  RTA_ENROLLMENT_ROOT_FLG        CHAR(1 BYTE),
  ASSOC_ROOT_ACCT_FLG            CHAR(1 BYTE)
)
TABLESPACE D_BI
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX M_ACCT_PK ON M_ACCT
(EDM_ACCT_KEY)
LOGGING
TABLESPACE I_BI
NOPARALLEL;


ALTER TABLE M_ACCT ADD (
  CONSTRAINT M_ACCT_PK
  PRIMARY KEY
  (EDM_ACCT_KEY)
  USING INDEX M_ACCT_PK);

GRANT DELETE, INSERT, SELECT, UPDATE ON M_ACCT TO DWLOADER;

GRANT SELECT ON M_ACCT TO EDM_OWNER_SELECT;

SPOOL OFF ; 	