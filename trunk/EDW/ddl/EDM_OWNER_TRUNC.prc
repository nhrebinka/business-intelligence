CREATE OR REPLACE PROCEDURE EDM_OWNER."EDM_OWNER_TRUNC" (tbl_nm in char) as

-- This is exclusivekly used to trunc and load T tables in EDM OWNER for T table's only ..
-- warning, there must be an explicit grant select on dba_tables
-- to the owner of this procedure, grant based on role causes
-- PLS-00201 identifier 'string' must be declared
--

sql_text      varchar2(100);
done          boolean;

cursor c3 is select table_name from all_tables where owner = 'EDM_OWNER' and table_name like 'T%';

begin
DBMS_OUTPUT.ENABLE(1000000);
DBMS_OUTPUT.PUT_LINE('START OF PROGRAM');
done := false;

for c3_rec in c3
loop
   dbms_output.put_line(c3_rec.table_name);
end loop;

for c3_rec in c3
  loop
  if tbl_nm = c3_rec.table_name then
     done := true;
     sql_text := 'truncate table '|| tbl_nm ;
     dbms_output.put_line(sql_text);
     execute immediate sql_text;
  end if;
  end loop;

if not done then
--  dbms_output.put_line ('raise an error');
    raise_application_error (-20101, 'not a permitted processing table, cannot truncate');
end if;

end;
/

grant execute on EDM_OWNER_TRUNC to dwloader ;

create public synonym EDM_OWNER_TRUNC for EDM_OWNER.EDM_OWNER_TRUNC ;   
