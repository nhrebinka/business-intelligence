SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON
DEFINE &&env = &1


SPOOL  /i1/&&env/hub/logs/T_ANCIL_DTL.tbl.log  ;

ALTER TABLE T_ANCIL_DTL
 DROP PRIMARY KEY CASCADE;

DROP TABLE T_ANCIL_DTL CASCADE CONSTRAINTS;

CREATE TABLE T_ANCIL_DTL
(
  EDM_ANCIL_DTL_KEY         NUMBER              NOT NULL,
  EDM_BILL_CD_KEY           NUMBER(38)          NOT NULL,
  EDM_RETAIL_DISC_KEY       NUMBER,
  EDM_ACCT_KEY              NUMBER(38)          NOT NULL,
  EDM_ACCT_AGRMNT_KEY       NUMBER,
  EDM_ENTLMT_KEY            NUMBER,
  EDM_DRIVER_KEY            NUMBER,
  EDM_SPNR_KEY              NUMBER,
  EDM_SPNR_PGM_KEY          NUMBER(38)          NOT NULL,
  EDM_MRCH_KEY              NUMBER,
  EDM_MRCH_AGRMNT_KEY       NUMBER,
  EDM_POS_KEY               NUMBER,
  EDM_SITE_KEY              NUMBER,
  EDM_POST_DT_KEY           NUMBER,
  EDM_CYCLE_CALENDAR_KEY    NUMBER,
  EDM_FEE_KEY               NUMBER,
  EDM_PURCH_DEVICE_KEY      NUMBER,
  EDM_FUNDER_KEY            NUMBER,
  EDM_DEPT_KEY              NUMBER,
  SRC_SYS_ID                VARCHAR2(15 BYTE),
  EDM_ANCIL_KEY             VARCHAR2(15 BYTE),
  ANCIL_DT                  DATE,
  POSTING_DT                DATE,
  REBATE_QTY                NUMBER(20,8),
  UNIT_OF_MEAS              VARCHAR2(10 BYTE),
  US_ANCIL_PER_UNT_AMT      NUMBER(20,8),
  US_BILLED_ANCIL_AMT       NUMBER(20,8),
  US_WAIVED_ANCIL_AMT       NUMBER(20,8),
  ACCT_ANCIL_PER_UNT_AMT    NUMBER(20,8),
  ACCT_BILLED_ANCIL_AMT     NUMBER(20,8),
  ACCT_WAIVED_ANCIL_AMT     NUMBER(20,8),
  ACCT_SPNR_BILL_AMT        NUMBER(20,8),
  ANCIL_PCT                 NUMBER(14,8),
  US_SPNR_BILL_AMT          NUMBER(20,8),
  EDM_ANCIL_TYPE            VARCHAR2(10 BYTE),
  EDM_CREATE_DT             DATE                NOT NULL,
  EDM_LAST_UPDT_DT          DATE                NOT NULL,
  EDM_LAST_UPDT_SESSION_NM  VARCHAR2(100 BYTE)  NOT NULL,
  EDM_SOURCE_SYS            VARCHAR2(30 BYTE)   NOT NULL,
  EDM_REBATE_TIER_KEY       VARCHAR2(15 BYTE),
  NBR_OF_UNTS               NUMBER(20,8),
  WEX_ACCT_NBR              VARCHAR2(15 BYTE)   NOT NULL,
  BILL_CD                   VARCHAR2(10 BYTE)   ,
  PROGRAM_PACKAGE_NAME      VARCHAR2(200 BYTE)  NOT NULL
)
TABLESPACE D_BI
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX FW_ANCIL_DTL_PK ON T_ANCIL_DTL
(EDM_ANCIL_DTL_KEY)
LOGGING
TABLESPACE I_BI
NOPARALLEL;


ALTER TABLE T_ANCIL_DTL ADD (
  CONSTRAINT FW_ANCIL_DTL_PK
  PRIMARY KEY
  (EDM_ANCIL_DTL_KEY)
  USING INDEX FW_ANCIL_DTL_PK);

ALTER TABLE T_ANCIL_DTL ADD (
  CONSTRAINT T_ANCIL_DTL_M_ACCT_FK 
  FOREIGN KEY (EDM_ACCT_KEY) 
  REFERENCES M_ACCT (EDM_ACCT_KEY),
  CONSTRAINT T_ANCIL_DTL_M_SPNR_PGM_FK 
  FOREIGN KEY (EDM_SPNR_PGM_KEY) 
  REFERENCES M_SPNR_PGM (EDM_SPNR_PGM_KEY));


GRANT DELETE, INSERT, SELECT, UPDATE ON T_ANCIL_DTL TO DWLOADER;

GRANT SELECT ON T_ANCIL_DTL TO EDM_OWNER_SELECT;


SPOOL OFF ; 	