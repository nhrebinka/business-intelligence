
SET SERVEROUTPUT ON
SET ECHO ON
SET TIMING ON
DEFINE &&env = &1

SPOOL /i1/&&env/hub/logs/SRC_PS_CUST_OPTION.tbl.log ; 

DROP TABLE SRC_PS_CUST_OPTION CASCADE CONSTRAINTS;

CREATE TABLE SRC_PS_CUST_OPTION
( SETID               VARCHAR2(5 )          ,
  CUST_ID             VARCHAR2(15 )         ,
  EFFDT               DATE                  ,
  EFF_STATUS          VARCHAR2(1 )          ,
  PYMNT_TERMS_CD      VARCHAR2(5 )          ,
  PYMNT_TERMS_AMT     VARCHAR2(1 )          ,
  GRACE_DUE_DAYS      INTEGER               ,
  GRACE_DISC_DAYS     INTEGER               ,
  RETENTION_DAYS      INTEGER               ,
  PP_HOLD             VARCHAR2(1 )          ,
  PP_METHOD           VARCHAR2(15 )         ,
  REF_QUALIFIER_CODE  VARCHAR2(2 )          ,
  PARTIAL_PY_SW       VARCHAR2(1 )          ,
  PO_REQUIRED         VARCHAR2(1 )          ,
  BILL_BY_ID          VARCHAR2(10 )         ,
  FREIGHT_BILL_TYPE   VARCHAR2(2 )          ,
  BILL_CYCLE_ID       VARCHAR2(10 )         ,
  BILL_INQUIRY_PHONE  VARCHAR2(24 )         ,
  BILL_TYPE_ID        VARCHAR2(3 )          ,
  BILLING_SPECIALIST  VARCHAR2(8 )          ,
  BILLING_AUTHORITY   VARCHAR2(8 )          ,
  COLLECTOR           VARCHAR2(8 )          ,
  CR_ANALYST          VARCHAR2(8 )          ,
  INVOICE_FORM_ID     VARCHAR2(10 )         ,
  DST_ID_AR           VARCHAR2(10 )         ,
  DRAFT_APPROVAL      VARCHAR2(1 )          ,
  DRAFT_DOC           VARCHAR2(1 )          ,
  DRAFT_TYPE          VARCHAR2(10 )         ,
  DD_PROFILE_ID       VARCHAR2(5 )          ,
  DD_GROUP            VARCHAR2(2 )          ,
  PAYMENT_METHOD      VARCHAR2(3 )          ,
  DIRECT_INVOICING    VARCHAR2(1 )          ,
  CUSTOMER_PO         VARCHAR2(25 )         ,
  START_DATE          DATE	            ,
  END_DATE            DATE                  ,
  MICR_ID             VARCHAR2(30 )         ,
  CONSOL_SETID        VARCHAR2(5 )          ,
  CONSOL_CUST_ID      VARCHAR2(15 )         ,
  CONSOL_KEY          VARCHAR2(22 )         ,
  INTERUNIT_FLG       VARCHAR2(1 )          ,
  BUSINESS_UNIT_GL    VARCHAR2(5 )          ,
  AR_SPECIALIST       VARCHAR2(8 )          ,
  MAX_WRITE_OFF_AMT   NUMBER(26,3)          ,
  MAX_WRITE_OFF_DAYS  INTEGER               ,
  MAX_WRITE_OFF_PCT   INTEGER               ,
  HOL_PROC_OPT        VARCHAR2(1 )          ,
  HOL_PROC_DAYS       INTEGER               ,
  HOL_PROC_OVERFL     VARCHAR2(1 )          ,
  REBATE_METHOD       VARCHAR2(1 )          ,
  BI_PROMPT_CURR      VARCHAR2(1 )          ,
  LAST_MAINT_OPRID    VARCHAR2(30 )         ,
  DATE_LAST_MAINT     DATE
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDM_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
    LOGFILE         EDM_LOG_DIR:'src_ps_cust_option.log'
    BADFILE         EDM_LOG_DIR:'src_ps_cust_option.bad'
    DISCARDFILE     EDM_LOG_DIR:'src_ps_cust_option.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS
( SETID               ,   
  CUST_ID             ,   
  EFFDT               CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  EFFDT   = BLANKS ,
  EFF_STATUS          ,   
  PYMNT_TERMS_CD      ,   
  PYMNT_TERMS_AMT     ,   
  GRACE_DUE_DAYS      ,   
  GRACE_DISC_DAYS     ,   
  RETENTION_DAYS      ,   
  PP_HOLD             ,   
  PP_METHOD           ,   
  REF_QUALIFIER_CODE  ,   
  PARTIAL_PY_SW       ,   
  PO_REQUIRED         ,   
  BILL_BY_ID          ,   
  FREIGHT_BILL_TYPE   ,   
  BILL_CYCLE_ID       ,   
  BILL_INQUIRY_PHONE  ,   
  BILL_TYPE_ID        ,   
  BILLING_SPECIALIST  ,   
  BILLING_AUTHORITY   ,   
  COLLECTOR           ,   
  CR_ANALYST          ,   
  INVOICE_FORM_ID     ,   
  DST_ID_AR           ,   
  DRAFT_APPROVAL      ,   
  DRAFT_DOC           ,   
  DRAFT_TYPE          ,   
  DD_PROFILE_ID       ,   
  DD_GROUP            ,   
  PAYMENT_METHOD      ,   
  DIRECT_INVOICING    ,   
  CUSTOMER_PO         ,   
  START_DATE          CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  START_DATE  = BLANKS ,
  END_DATE            CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  END_DATE  = BLANKS ,
  MICR_ID             ,   
  CONSOL_SETID        ,   
  CONSOL_CUST_ID      ,   
  CONSOL_KEY          ,   
  INTERUNIT_FLG       ,   
  BUSINESS_UNIT_GL    ,   
  AR_SPECIALIST       ,   
  MAX_WRITE_OFF_AMT   ,   
  MAX_WRITE_OFF_DAYS  ,   
  MAX_WRITE_OFF_PCT   ,   
  HOL_PROC_OPT        ,   
  HOL_PROC_DAYS       ,   
  HOL_PROC_OVERFL     ,   
  REBATE_METHOD       ,   
  BI_PROMPT_CURR      ,   
  LAST_MAINT_OPRID    ,   
  DATE_LAST_MAINT     CHAR  date_format DATE mask "MM/DD/YYYY hh24:mi:ss" NULLIF  DATE_LAST_MAINT    = BLANKS 
)
    )
     LOCATION (EDM_DATA_DIR:'src_ps_cust_option.dat')
  )
REJECT LIMIT 0
NOPARALLEL
NOMONITORING;

GRANT ALTER, SELECT ON SRC_PS_CUST_OPTION  TO DWLOADER     	;
GRANT ALTER, SELECT ON SRC_PS_CUST_OPTION  TO EDM_OWNER    	;
GRANT ALTER, SELECT ON SRC_PS_CUST_OPTION  TO STAGE_OWNER_SELECT;


SPOOL OFF; 
