CREATE OR REPLACE PROCEDURE EDW_OWNER.DRS_TRANSACTION_COUNT_FIX1 (
   p_calendar_dt_start   IN DATE := TRUNC (SYSDATE),
   p_calendar_dt_end     IN DATE := TRUNC (SYSDATE))
AS
   /*
   NAME                    :-    DRS_TRANSACTION_LINE_ITEM_COUNT
   Original AUTHOUR  :-    Izzy Reinish
   DATE                     :-    01/24/2011
   DECRIPTION :-   PROCEURE TO change (historically) the TRANSACTION_LINE_ITEM_COUNT1 represent the number of swipe line items only.
                        :- tUNED bY VK  ON 02/01/2011  :-) 
*/


   v_calendar_dt_start   DATE;
   v_calendar_dt_end     DATE;
BEGIN
   v_calendar_dt_start := p_calendar_dt_start;
   v_calendar_dt_end := p_calendar_dt_end;

   FOR rec
      IN (SELECT D.CALENDAR_DATE_DT - D.DAY_IN_MONTH_NUMBER + 1
                    AS FIRST_OF_MONTH,
                 D.CALENDAR_DATE_DT AS LAST_OF_MONTH,
                 D.DATE_KEY AS DATE_KEY
            FROM D_DATE D
           WHERE D.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start
                                        AND v_calendar_dt_end
                 AND D.LAST_DAY_IN_MONTH_FLG = 'Yes')
   LOOP
      MERGE INTO F_DAILY_REVENUE_SNAPSHOT DRS
           USING (  SELECT                              /*+ parallel (TLI,8)*/
                          TLI.PURCHASE_ACCOUNT_KEY  AS ACCOUNT_KEY,
                           TLI.PROGRAM_KEY                  AS PROGRAM_KEY,
                           TLI.POSTING_DATE_KEY          AS POSTING_DATE_KEY,
                           TLI.REVENUE_DATE_KEY          AS REVENUE_DATE_KEY,
                         --  TLI.ACCOUNT_REGION_KEY      AS ACCOUNT_REGION_KEY,
                           SUM (
                              CASE
                                 WHEN TLI.WEX_TRANSACTION_ITEM_SEQ_NBR = 1
                                      AND TLI.GROSS_SPEND_AMOUNT <> 0
                                 THEN
                                    TLI.TRANSACTION_LINE_ITEM_COUNT1
                                 ELSE
                                    0
                              END)
                              AS Transaction_Count
                      --        1 as Transaction_Count
                      FROM    F_TRANSACTION_LINE_ITEM TLI
                           JOIN
                              D_DATE D
                           ON D.DATE_KEY = TLI.REVENUE_DATE_KEY
                              --AND        D.DATE_KEY = REC.DATE_KEY
                              AND D.CALENDAR_DATE_DT BETWEEN REC.FIRST_OF_MONTH
                                                         AND REC.LAST_OF_MONTH
                  -- and     TLI.GROSS_SPEND_AMOUNT <> 0
                  GROUP BY 
                           TLI.PURCHASE_ACCOUNT_KEY,
                           TLI.PROGRAM_KEY,
                           --    TLI.ACCOUNT_REGION_KEY,
                           TLI.REVENUE_DATE_KEY,
                           TLI.POSTING_DATE_KEY--  TLI.WEX_TRANSACTION_ID
                 ) B
              ON (    B.ACCOUNT_KEY         = DRS.ACCOUNT_KEY
                  AND B.PROGRAM_KEY         = DRS.PROGRAM_KEY
                  AND B.REVENUE_DATE_KEY = DRS.REVENUE_DATE_KEY
                  AND B.POSTING_DATE_KEY = DRS.POSTING_DATE_KEY)
      WHEN MATCHED
      THEN
         UPDATE SET
            DRS.TRANSACTION_LINE_ITEM_COUNT1 = B.Transaction_Count,
            DRS.ROW_LAST_MOD_DTTM                  = SYSDATE,
            DRS.ROW_LAST_MOD_PROC_NM            = 'DRS_TRANSACTION_COUNT_FIX1';

      COMMIT;
   END LOOP;
   
   
  
UPDATE F_DAILY_TOTALS_SNAPSHOT DTS
   SET DTS.TRANSACTION_LINE_ITEM_COUNT1 = 0
 WHERE DTS.REVENUE_DATE_KEY = DTS.POSTING_DATE_KEY;
 
 commit ; 

UPDATE F_DAILY_REVENUE_SNAPSHOT DRS
   SET DRS.TRANSACTION_LINE_ITEM_COUNT1 = 0
 WHERE DRS.REVENUE_DATE_KEY = DRS.POSTING_DATE_KEY;
 
 commit ;

UPDATE F_MONTHLY_REVENUE_SNAPSHOT MRS
   SET MRS.TRANSACTION_LINE_ITEM_COUNT1 = 0
 WHERE MRS.REVENUE_DATE_KEY = MRS.POSTING_DATE_KEY;
 
 commit ;
   
     
END DRS_TRANSACTION_COUNT_FIX1;
/