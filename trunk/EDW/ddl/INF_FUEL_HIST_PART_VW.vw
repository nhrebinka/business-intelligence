
SPOOL  /i1/&&env/hub/logs/D_PURCHASE_DATE_VW.vw.log ;

DROP VIEW INF_FUEL_HIST_PART_VW;

/* Formatted on 10/18/2010 3:17:21 PM (QP5 v5.163.1008.3004) */
CREATE OR REPLACE FORCE VIEW INF_FUEL_HIST_PART_VW
(
   PART_NAME,
   P_START_DT,
   P_END_DATE
)
AS
   (SELECT 'P1' part_name,
           MIN (posting_date) P_start_dt,
           MIN (posting_date) + 14 p_end_date
      FROM SRC_TDM_FUL1MST_HIST
    UNION
    SELECT 'P2' part_name,
           MIN (posting_date) + 15 P_start_dt,
           LAST_DAY (MIN (posting_date)) p_end_date
      FROM SRC_TDM_FUL1MST_HIST
    UNION
    SELECT 'P3' part_name,
           ADD_MONTHS (MIN (posting_date), 1) P_start_dt,
           (ADD_MONTHS (MIN (posting_date), 1) + 14) p_end_date
      FROM SRC_TDM_FUL1MST_HIST
    UNION
    SELECT 'P4' part_name,
           (ADD_MONTHS (MIN (posting_date), 1) + 15) P_start_dt,
           LAST_DAY (ADD_MONTHS (MIN (posting_date), 1)) p_end_date
      FROM SRC_TDM_FUL1MST_HIST
    UNION
    SELECT 'P5' part_name,
           ADD_MONTHS (MIN (posting_date), 2) P_start_dt,
           (ADD_MONTHS (MIN (posting_date), 2) + 14) p_end_date
      FROM SRC_TDM_FUL1MST_HIST
    UNION
    SELECT 'P6' part_name,
           (ADD_MONTHS (MIN (posting_date), 2) + 15) P_start_dt,
           MAX (posting_date) p_end_date
      FROM SRC_TDM_FUL1MST_HIST);


GRANT SELECT ON INF_FUEL_HIST_PART_VW TO DWLOADER;

GRANT SELECT ON INF_FUEL_HIST_PART_VW TO STAGE_OWNER_SELECT;

SPOOL  OFF ; 