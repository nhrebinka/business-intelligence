SPOOL  /i1/&&env/hub/logs/D_PURCHASE_ACCOUNT_VW.vw.log ;

CREATE OR REPLACE FORCE VIEW D_PURCHASE_ACCOUNT_VW
(  PUR_SOURCE_ACCOUNT_ID,
   PUR_ACCOUNT_ADDRESS_LINE_1,
   PUR_ACCOUNT_ADDRESS_LINE_2,
   PUR_ACCOUNT_ADDRESS_LINE_3,
   PUR_ACCOUNT_CITY,
   PUR_ACCOUNT_STATE_PROV_CODE,
   PUR_ACCOUNT_POSTAL_CODE,
   PUR_ACCOUNT_MANAGER_NAME,
   PUR_ACCOUNT_NAME,
   PUR_BANK_IDENTIFICATION_NUMBER,
   PUR_ACCOUNT_OPEN_DATE,
   PUR_ACCOUNT_REGION_CODE,
   PUR_ACCOUNT_REGION_NAME,
   PUR_ACCOUNT_COUNTRY_NAME,
   PUR_ACCOUNT_STATUS_DESC,
   PUR_ACCOUNT_CLOSED_DATE,
   PUR_FUNDING_TYPE_NAME,
   PUR_CURRENT_RECORD_FLG,
   PUR_FUNDER_NAME,
   PUR_REBATE_FLG,
   PUR_SIC_INDUSTRY_CODE,
   PUR_NAICS_INDUSTRY_CODE,
   PUR_ACCOUNT_DISTRICT_NAME,
   PUR_ACCOUNT_DISTRICT_CODE,
   PUR_ACCOUNT_SUBDISTRICT_NAME,
   PUR_ACCOUNT_SUBDISTRICT_CODE,
   PUR_RELATED_BILLING_ACCOUNT_ID,
   PUR_CREDIT_NATIONAL_ACCOUNT_ID,
   PUR_NATIONAL_ACCOUNT_OPEN_DATE,
   PUR_CUSTOMER_NAME,
   PUR_RISK_GRADE,
   PUR_ATTRITION_TYPE_NAME,
   PUR_ATTRITION_REASON_CODE,
   PUR_ATTRITION_REASON_DESC,
   PUR_ACCOUNT_KEY
)
AS
   SELECT D_ACCOUNT.SOURCE_ACCOUNT_ID,
          D_ACCOUNT.ACCOUNT_ADDRESS_LINE_1,
          D_ACCOUNT.ACCOUNT_ADDRESS_LINE_2,
          D_ACCOUNT.ACCOUNT_ADDRESS_LINE_3,
          D_ACCOUNT.ACCOUNT_CITY,
          D_ACCOUNT.ACCOUNT_STATE_PROV_CODE,
          D_ACCOUNT.ACCOUNT_POSTAL_CODE,
          D_ACCOUNT.ACCOUNT_MANAGER_NAME,
          D_ACCOUNT.ACCOUNT_NAME,
          D_ACCOUNT.BANK_IDENTIFICATION_NUMBER,
          D_ACCOUNT.ACCOUNT_OPEN_DATE,
          D_ACCOUNT.ACCOUNT_REGION_CODE,
          D_ACCOUNT.ACCOUNT_REGION_NAME,
          D_ACCOUNT.ACCOUNT_COUNTRY_NAME,
          D_ACCOUNT.ACCOUNT_STATUS_DESC,
          D_ACCOUNT.ACCOUNT_CLOSED_DATE,
          D_ACCOUNT.FUNDING_TYPE_NAME,
          D_ACCOUNT.CURRENT_RECORD_FLG,
          D_ACCOUNT.FUNDER_NAME,
          D_ACCOUNT.REBATE_FLG,
          D_ACCOUNT.SIC_INDUSTRY_CODE,
          D_ACCOUNT.NAICS_INDUSTRY_CODE,
          D_ACCOUNT.ACCOUNT_DISTRICT_NAME,
          D_ACCOUNT.ACCOUNT_DISTRICT_CODE,
          D_ACCOUNT.ACCOUNT_SUBDISTRICT_NAME,
          D_ACCOUNT.ACCOUNT_SUBDISTRICT_CODE,
          D_ACCOUNT.RELATED_BILLING_ACCOUNT_ID,
          D_ACCOUNT.CREDIT_NATIONAL_ACCOUNT_ID,
          D_ACCOUNT.NATIONAL_ACCOUNT_OPEN_DATE,
          D_ACCOUNT.CUSTOMER_NAME,
          D_ACCOUNT.RISK_GRADE,
          D_ACCOUNT.ATTRITION_TYPE_NAME,
          D_ACCOUNT.ATTRITION_REASON_CODE,
          D_ACCOUNT.ATTRITION_REASON_DESC,
          D_ACCOUNT.ACCOUNT_KEY
     FROM D_ACCOUNT
    WHERE D_ACCOUNT.PURCHASE_ACCOUNT_FLG = 'Yes';



SPOOL  OFF ; 