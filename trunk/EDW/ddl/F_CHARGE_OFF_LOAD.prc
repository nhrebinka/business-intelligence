/* Formatted on 6/21/2011 10:02:23 AM (QP5 v5.139.911.3011) */
CREATE OR REPLACE PROCEDURE F_CHARGE_OFF_LOAD IS

/******************************************************************************
   NAME:       F_CHARGE_OFF_LOAD
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        6/21/2011   SSirigiri       1. Created this procedure.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     F_CHARGE_OFF_LOAD
      Sysdate:         6/21/2011
      Date and Time:   6/21/2011, 9:53:33 AM, and 6/21/2011 9:53:33 AM
      Username:        SSirigiri (set in TOAD Options, Procedure Editor)
      Table Name:       (set in the "New PL/SQL Object" dialog)

******************************************************************************/
BEGIN

INSERT                   /*+ append */

   INTO F_CHARGE_OFF(
     CHARGE_OFF_KEY,
  CHARGE_OFF_TYPE_KEY,
  ACCOUNT_KEY,
  PROGRAM_KEY,
  DATE_KEY,
  CHARGE_OFF_AMOUNT,
  RECOVERY_AMOUNT,
  CHARGE_OFF_COUNT1,
  LINE_ITEM_TYPE_KEY,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR)

  (SELECT                                       /*+ parallel (FTLI, 4 ) */
  CHARGE_OFF_KEY,
  CHARGE_OFF_TYPE_KEY,
  ACCOUNT_KEY,
  PROGRAM_KEY,
  DATE_KEY,
  CHARGE_OFF_AMOUNT,
  RECOVERY_AMOUNT,
  CHARGE_OFF_COUNT1,
  LINE_ITEM_TYPE_KEY,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR
  FROM EDW_STAGE_OWNER.F_CHARGE_OFF
  );

COMMIT;

END F_CHARGE_OFF_LOAD;
/


CREATE OR REPLACE PUBLIC SYNONYM F_CHARGE_OFF_LOAD FOR EDW_OWNER.F_CHARGE_OFF_LOAD ; 

GRANT EXECUTE ON  F_CHARGE_OFF_LOAD TO DWLOADER ; 