Note: See ~/release/bi4 for release bundlers

.
+--1_analysis/                   
|  +--dim/                                   # dimension
|  |  +--acct/
|  |  |  acct_attrition.sql
|  |  |  acct_attrition.txt
|  |  |  acct_attrition_full.sql
|  |  |  f_daily_account_aggregates.spp
|  |  |  f_daily_account_aggregates.spp.new
|  |  |  f_daily_account_aggregates.spp.orig
|  |  |  f_daily_account_aggregates.spp~
|  |  |  f_daily_account_aggs_h_dfix.spp
|  |  |  f_max_card_count_daily.sql
|  |  |  f_max_card_count_monthly_fix.sql
|  |  |  f_monthly_account_agg_h_dfix.spp
|  |  |  f_monthly_account_aggregates.spp
|  |  |  f_monthly_account_attrition_size.spp
|  |  |  +--src/
|  |  |     F_DAILY_ACCOUNT_AGGREGATES.prc
|  |  |     f_daily_account_aggregates_h.spp
|  |  +--user/
|  |     d_date_vw.sql
|  |     d_date_vw.sql.orig
|  |     d_pur_date_vw.sql
|  |     d_rev_date_vw.sql
|  |     d_user_hist_vw.sql
|  |     d_user_vw.sql
|  |     dept.sql
|  |     dup.sql
|  |     user_hist_key.sql
|  +--efd/                                  # effective dates
|  |  efd_2015.csv
|  |  efd_update_2015.sql
|  +--fact/                                 # fact tables
|  |  DTS_TRANSACTION_LINE_ITEM_CNT.prc
|  |  EDW_COMPRESS_PARTITIONS.prc
|  |  EDW_COMPRESS_PARTITIONS_ORIG.prc
|  |  F_ACCOUNTS_RECEIVABLE_LOAD.prc
|  |  F_CREDIT_APP_EVENT_LOAD.prc
|  |  F_TRANSACTION_LINE_ITEM_LOAD.prc
|  +--hier/                                 # hirarchies
|  |  hub_hier.sql
|  |  hub_hier.txt
|  |  m_acct.sql
|  |  m_spnr_pgm.sql
|  |  m_stkhldr.sql
|  |  x_hier.sql
|  +--risk_score/                           # account risk
|  |  fmtlst.rb
|  |  +--file_layout/
|  |     WEX CSAD file layout Sept 2010.xls
|  |     Wright Express P003734 Layout_WithClientDetail.xls
|  +--site/                                 # site 
|     add_session.sql
|     inc_fields.sql
|     naics.sql
|     xd_site.sql
|     xd_site2.sql
|     xd_site2_b.sql
|     xd_site3.sql
|     xd_site4.sql
|     xd_site_b.sql
|     xd_site_b_idx.sql
|     xm_site2.sql
|     +--tune/                              # captured BO queries
|        big_chad1.sql
|        big_chad2.sql
|        big_chad3.sql
|        big_chad4.sql
|        fix1.sql
|        fix1.txt
|        fix1b.txt
|        fix1c.txt
|        fix2.sql
|        fix2.txt
|        post.sql
|        post.txt
|        rev.sql
|        rev.txt
|        rev_post.sql
|        rev_post.txt
|        test1.sql
|        test2.sql
+--2_design/                                # d_* EDW dimensions
|  bi4_dir_config.txt             
|  d_date_vw.sql
|  d_idx.sql
|  d_ifa.sql
|  d_key.sql
|  d_seq.sql
|  d_tbl.sql
|  d_val.sql
|  d_val0.sql
|  edw_grant.sql
|  f_adt.sql                                # f_* EDW facts
|  f_ifa.sql
|  f_key.sql
|  f_seq.sql
|  f_tbl.sql
|  f_tbl_partner_billing.sql
|  hub_grant.sql
|  m_ifa.sql                                # m_* HUB master tables
|  m_key.sql
|  m_seq.sql
|  m_tbl.sql
|  m_tbl_partner_billing.sql
|  m_val.sql
|  m_xifa.sql
|  m_xtbl.sql
|  release_note.sql
|  src_acct_since_date_lookup.sql           # src_* HUB Stage tables
|  src_excel_tbl.sql
|  src_grant.sql
|  src_ps_tbl.sql
|  src_stg_init.sql
|  src_tbl.sql
|  t_ifa.sql                                # t_* HUB temp fact tables
|  t_tbl.sql
|  t_tbl2.sql
|  x_gnt.sql                                # x_* EDW Stage tables
|  x_tbl.sql
|  x_vw.sql
|  +--0_src_dat.0512_orig/                  # saved DDL
|  |  bi4edw15_idx.sql
|  |  bi4edw15_tbl.sql
|  |  bi4edw16_tbl.sql
|  |  bi4hub10_idx.sql
|  |  bi4hub10_tbl.sql
|  |  bi4hub9.sql
|  |  d_business_sector.sql
|  |  d_business_segment.sql
|  |  d_fr_category.sql
|  |  d_gl_account.sql
|  |  dwloader_synonym.sql
|  |  grant.sql
|  |  m_business_sector.sql
|  |  m_business_segment.sql
|  |  m_cost_center.sql
|  |  m_fr_category.sql
|  |  m_gl_account.sql
|  |  m_mkt_sub_channel.sql
|  |  stage.sql
|  |  t_aging.sql
|  |  t_ledger.sql
|  |  t_manual_rebate.sql
|  |  t_part_sub_chnl_events.sql
|  +--0_src_dat.0521/                       # captured data for analysis
|  |  src_ps_dept_tbl.dat.gz
|  |  src_ps_gl_account_tbl.dat.gz
|  |  src_ps_ledger.dat.gz
|  +--0_src_dat.0922/                       # captured data
|     20140925_qt_fix_edm.xlsx
|     20140925_qt_fix_edw.xlsx
|     edm_owner.xlsx
|     edm_owner_qt_vs_pd.xlsx
|     edw_qt_vs_pd.xlsx
|     ps_dept_tbl.txt.gz
|     ps_gl_account_tbl.txt.gz
|     ps_ledger.txt.gz
|     sqlldr20140922105733.ctl
|     todo_cleanup.xlsx
+--3_devel/                                 
|  f_gl_acct_daily_snapshot_load.spp        # Stored Procs for GL Account
|  f_gl_acct_mon_by_category_load.spp
|  f_gl_acct_mon_snapshot_load.spp
|  f_revenue_aggregates.spp                 # Revenue Aggregates
|  f_transaction_line_item_load.spp 
|  +--1_src_sp_1002/                        # Venu's Original
|  |  vk_edw_compress_partitions.spp
|  |  vk_f_daily_account_aggregates.spp
|  |  vk_f_daily_account_aggregates_h.spp
|  |  vk_f_daily_account_aggs_h_dfix.spp
|  |  vk_f_daily_affinity_program_check.spp
|  |  vk_f_daily_aging_snapshot_load.spp
|  |  vk_f_monthly_account_agg_h_dfix.spp
|  |  vk_f_monthly_account_aggregates.spp
|  |  vk_f_monthly_account_aggregates_h.spp
|  |  vk_f_monthly_revenue_aggs_fix.spp
|  |  vk_f_new_daily_totals_hist.spp
|  |  vk_f_revenue_aggregates.spp
|  |  vk_f_revenue_aggregates_fix.spp
|  |  vk_f_revenue_aggregates_h.spp
|  |  vk_f_revenue_aggregates_t.spp
|  |  vk_f_transaction_line_item_load.spp
|  +--test/                                 # test data
|     0_bad20150116111829.txt
|     bi4_txn_item_cnt_test1.sql
|     bi4_txn_item_cnt_test2.sql
|     d_naics.txt
|     d_seq.sql
|     d_sic.txt
|     d_tbl.sql
|     m_naics_sic_lookup.txt
|     t_null.sql
+--EDW_Models/
|  +--BI_PHASE_3/
|  |  BI_PHASE_3.pdf
|  |  EDW_OWNER_BI_PHASE_3.0_AGING.pdf
|  |  EDW_OWNER_BI_PHASE_3.0_AGING_02292012.pdf
|  |  EDW_OWNER_BI_PHASE_3.0_AGING_FINAL.pdf
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_1.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_2.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_3.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_4_20120319.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_4_20120327.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_5_20120329.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_5_20120410.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_5_20120410_ERIC.erwin
|  |  EDW_OWNER_BI_PHASE_3.0_F_AGING_5_20120701.erwin
|  |  EDW_OWNER_BI_PHASE_3.1_F_AGING_6_20120730.erwin
|  |  EDW_OWNER_BI_PHASE_3.2.1_F_PROGRAM_ACCOUNT_EVENT.erwin
|  |  +--BI_PHASE_3.2.2/
|  |     3 2 2_Task_and_TimeLine from Linda.xlsx
|  |     3.2.2_Task_and_TimeLine.xlsx
|  |     EDW_OWNER_BI_PHASE_3.2.11_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.12_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.13_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.14_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.15_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.16_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.17_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.18_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.19_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.1_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.20_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.21_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.22_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.23_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.24_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.25_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.26_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.27_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.28_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.29_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.30_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.31_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.32_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.33_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.34_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.35_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.36_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.37_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.38_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.39_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.40_OPTY.erwin
|  |     EDW_OWNER_BI_PHASE_3.2.41_OPTY.erwin
|  |     PRHUB_Model_20131021.DM1
|  |     Resource_Allocation.xlsx
|  |     ~$3 2 2_Task_and_TimeLine from Linda.xlsx
|  |     ~$Resource_Allocation.xlsx
|  |     +--SALESFORCE/
|  |        ApexDataLoader.exe
|  |        QuestDataHub-1.6.1.180.msi
|  |        SalesforceSystemAccessForm[1].pdf
|  |        ToadForCloudDatabases.zip
|  |        ToadForCloudDatabases_1.6.0.80.msi
|  |        User.csv
|  |        rrt0app.dat
|  |        src_sbl_s_src.dat
|  +--BI_Phase_4.0/
|  |  BI_Phase_4.0_HUB_ver_1.DM1
|  |  BI_Phase_4.0_HUB_ver_1.pdf
|  |  BI_Phase_4.0_HUB_ver_2.DM1
|  |  BI_Phase_4.0_HUB_ver_3.DM1
|  |  BI_Phase_4.0_HUB_ver_4.DM1
|  |  BI_Phase_4.0_HUB_ver_4.pdf
|  |  BI_Phase_4.0_HUB_ver_5.DM1
|  |  BI_Phase_4.0_HUB_ver_5.pdf
|  |  BI_Phase_4.0_HUB_ver_6.DM1
|  |  BI_Phase_4.0_HUB_ver_7.DM1
|  |  BI_Phase_4.0_HUB_ver_8.DM1
|  |  BI_Phase_4.0_ver_1.DM1
|  |  BI_Phase_4.0_ver_10.DM1
|  |  BI_Phase_4.0_ver_10.pdf
|  |  BI_Phase_4.0_ver_11.DM1
|  |  BI_Phase_4.0_ver_12.DM1
|  |  BI_Phase_4.0_ver_13.DM1
|  |  BI_Phase_4.0_ver_14.DM1
|  |  BI_Phase_4.0_ver_2.DM1
|  |  BI_Phase_4.0_ver_3.DM1
|  |  BI_Phase_4.0_ver_4.DM1
|  |  BI_Phase_4.0_ver_5.DM1
|  |  BI_Phase_4.0_ver_6.DM1
|  |  BI_Phase_4.0_ver_6.pdf
|  |  BI_Phase_4.0_ver_7.DM1
|  |  BI_Phase_4.0_ver_8.DM1
|  |  BI_Phase_4.0_ver_8.pdf
|  |  BI_Phase_4.0_ver_9.DM1
|  |  BI_Phase_4.0_ver_9.pdf
|  |  IMG_1438.JPG
|  |  IMG_1439.JPG
|  |  IMG_1440.JPG
|  |  IMG_1441.JPG
|  |  IMG_1442.JPG
|  |  IMG_1443.JPG
|  |  IMG_1444.JPG
|  |  IMG_1445.JPG
|  |  IMG_1582.JPG
|  +--Bi_phase2/
|     EDW_OWNER v2.0 Development.erwin
|     EDW_OWNER_CLM_1.5.erwin
|     EDW_OWNER_CLM_1.5_v2.erwin
|     EDW_OWNER_CLM_1.5_v3.erwin
|     EDW_OWNER_CLM_1.5_v4.erwin
|     EDW_OWNER_CLM_2.0_Site_Aggs.erwin
|     EDW_OWNER_CLM_2.0_Site_Aggs_11172011.erwin
|     PREDW_03282011.erwin
+--data/
|  Ancillary_date.dat
|  Ancillary_date.txt
|  Cluster.csv
|  DEFAULTS POS_SITE.txt
|  DEFAULTS.txt
|  DLIINE.dat
|  PARAM.txt
|  ps_us_holidays.dat
|  src_acct_sic_cd.dat
|  src_charge_off_type.csv
|  src_efd.dat
|  src_efd_2010.dat
|  src_regions.dat
|  src_sbl_spnr_pgm.dat
|  src_tdm_mp_pgm_xref.dat
|  src_wb_wex_product_to_nacs.dat
+--ddl/
|  CHARGE_OFF_TYPE.tbl
|  DRS_TRANSACTION_COUNT_FIX1.prc
|  DTS_TRANSACTION_LINE_ITEM_CNT.prc
|  DW_PURCH_DEVICE.tbl
|  D_ACCOUNT.tbl
|  D_ACCOUNT_CURRENT_VIEW.vw
|  D_ACCOUNT_CURRENT_VW.vw
|  D_ACCOUNT_HIST_VW.vw
|  D_ACCOUNT_NEW.tbl
|  D_ACCOUNT_RECEIVABLE_TYPE.tbl
|  D_ACCT_REC_TYPE_SEQ_T1.trg
|  D_BILLING_ACCOUNT_VW.vw
|  D_BILLING_ACCT_REGION_VW.vw
|  D_CHARGE_OFF_TYPE.tbl
|  D_CHARGE_OFF_TYPE_SEQ_T1.trg
|  D_DATE.tbl
|  D_DIVERSION_ACCOUNT_VW.vw
|  D_LINE_ITEM_TYPE.tbl
|  D_POSTING_DATE_VW.vw
|  D_POS_AND_SITE.tbl
|  D_PROGRAM.tbl
|  D_PURCHASE_ACCOUNT_VW.vw
|  D_PURCHASE_DATE_VW.vw
|  D_PURCHASE_DEVICE.tbl
|  D_REGION.tbl
|  D_REVENUE_DATE_VW.vw
|  D_SEED_DATES.prc
|  D_SITE_REGION_VW.vw
|  EDM_OWNER_SEQUENCES.seq
|  EDM_OWNER_TRIGGERS.trg
|  EDM_OWNER_TRUNC.prc
|  EDW_COMPRESS_PARTITIONS.prc
|  EDW_OWNER_INDEXES.sql
|  EDW_OWNER_SEQUENCES.seq
|  EDW_STAGE_OWNER_F_TRANSACTION_LINE_ITEM.tbl
|  EDW_STAGE_OWNER_TRUNC.prc
|  F_ACCOUNT_RECEIVABLE_ENTRY.tbl
|  F_ACCOUNT_RECEIVABLE_ENTRY_STAGE.tbl
|  F_CHARGE_OFF.tbl
|  F_CHARGE_OFF_LOAD.prc
|  F_CHARGE_OFF_TYPE_SEQ.seq
|  F_CHARGE_OF_STAGE.tbl
|  F_DAILY_ACCOUNT_AGGREGATES.prc
|  F_DAILY_ACCOUNT_AGGREGATES_H.prc
|  F_DAILY_ACCOUNT_SNAPSHOT.tbl
|  F_DAILY_ACCOUNT_SNAPSHOT_UI4.sql
|  F_DAILY_REVENUE_SNAPSHOT.tbl
|  F_DAILY_TOTALS_SNAPSHOT.tbl
|  F_MONTHLY_ACCOUNT_AGGREGATES.prc
|  F_MONTHLY_ACCOUNT_AGGREGATES_H.prc
|  F_MONTHLY_ACCOUNT_SNAPSHOT.tbl
|  F_MONTHLY_REVENUE_AGGS_FIX.prc
|  F_MONTHLY_REVENUE_SNAPSHOT.tbl
|  F_NEW_DAILY_TOTALS_HIST.prc
|  F_PURCHASE_DEVICE_EVENT.tbl
|  F_REVENUE_AGGREGATES.prc
|  F_REVENUE_AGGREGATES_H.prc
|  F_SEED_DATES.prc
|  F_TRANSACTION_LINE_ITEM.tbl
|  F_TRANSACTION_LINE_ITEM_LOAD.prc
|  F_WAIVED_FEE_EVENT.tbl
|  F_WAIVED_FEE_EVENT_KEY_SEQ.seq
|  F_WAIVED_FEE_EVENT_STAGE.tbl
|  INF_FUEL_HIST_PART_VW.vw
|  IZZY_SRC_SBL_PURCH_DEVICE.tbl
|  MD5HASH.fnc
|  MRS_TRANSACTION_COUNT_FIX.prc
|  M_ACCT.tbl
|  M_ACCT_FEE.tbl
|  M_ACCT_FEE_XREF.tbl
|  M_ACCT_XREF.tbl
|  M_ADDR.tbl
|  M_ADDR_XREF.tbl
|  M_BILL_CD.tbl
|  M_BILL_CD_XREF.tbl
|  M_CAMPAIGN.tbl
|  M_CAMPAIGN_XREF.tbl
|  M_CODE.tbl
|  M_CODE_XREF.tbl
|  M_CONTACT.tbl
|  M_CONTACT_XREF.tbl
|  M_CUSTOMER.tbl
|  M_CUSTOMER_XREF.tbl
|  M_CUST_CREDIT.tbl
|  M_FEE.tbl
|  M_FEE_XREF.tbl
|  M_FUNDER.tbl
|  M_FUNDER_XREF.tbl
|  M_PD_PROD.tbl
|  M_PD_PROD_XREF.tbl
|  M_POS.tbl
|  M_POS_XREF.tbl
|  M_PRODUCT.tbl
|  M_PRODUCT_XREF.tbl
|  M_PS_BI_CHARGE.tbl
|  M_PS_DST_CODE_TBL.tbl
|  M_PURCH_DEVICE.tbl
|  M_PURCH_DEVICE_XREF.tbl
|  M_REGIONS.tbl
|  M_RI_CONSTRAINTS.sql
|  M_SIC_LOOKUP.tbl
|  M_SITE.tbl
|  M_SITE_XREF.tbl
|  M_SPNR_PGM.tbl
|  M_SPNR_PGM_XREF.tbl
|  M_STKHLDR.tbl
|  M_STKHLDR_XREF.tbl
|  M_TTA_AGRMNT.tbl
|  M_TTPA_AGRMNT.tbl
|  M_WX_CUSTOMER_WEX.tbl
|  PROJECT_MASTER_LIST.tbl
|  PS_US_HOLIDAYS.tbl
|  SRC_ACCT_SIC_CD.tbl
|  SRC_CHARGE_OFF_TYPE.tbl
|  SRC_CLM_CLUSTER.tbl
|  SRC_CRD_CM_ACCOUNT.tbl
|  SRC_DW_STG_ANCIL_DTL.tbl
|  SRC_DW_STG_PURCH_TRANS.tbl
|  SRC_DW_STG_PURCH_TRANS_HIST.tbl
|  SRC_EFD.tbl
|  SRC_MC_F_TRANS_LINE_ITEM.tbl
|  SRC_PS_CUSTOMER.tbl
|  SRC_PS_CUST_CREDIT.tbl
|  SRC_PS_CUST_OPTION.tbl
|  SRC_PS_ITEM_ACTIVITY.tbl
|  SRC_PS_PSXLATITEM.tbl
|  SRC_PS_SET_CNTRL_REC.tbl
|  SRC_PS_WX_BILL_CD_ENT.tbl
|  SRC_PS_WX_CUSTOMER_WEX.tbl
|  SRC_REGIONS.tbl
|  SRC_SBL_ACCT.tbl
|  SRC_SBL_ACCT_RESTRICTION.tbl
|  SRC_SBL_ACCT_TERMED.tbl
|  SRC_SBL_ADDR.tbl
|  SRC_SBL_BILL_CD.tbl
|  SRC_SBL_CONTACT.tbl
|  SRC_SBL_FUNDER.tbl
|  SRC_SBL_PD_PROD.tbl
|  SRC_SBL_POS.tbl
|  SRC_SBL_PRODUCT.tbl
|  SRC_SBL_PURCH_DEVICE.tbl
|  SRC_SBL_PURCH_DEVICE_FULL.tbl
|  SRC_SBL_PURCH_DEVICE_HIST.tbl
|  SRC_SBL_SERVICE_OPTION.tbl
|  SRC_SBL_SITE.tbl
|  SRC_SBL_SPNR_PGM.tbl
|  SRC_SBL_STKHLDR.tbl
|  SRC_SBL_S_ENTLMNT.tbl
|  SRC_SBL_S_LST_OF_VAL.tbl
|  SRC_SBL_S_OPTY.tbl
|  SRC_SBL_S_SRC.tbl
|  SRC_TDM_ANC1MST.tbl
|  SRC_TDM_APP0TRK.tbl
|  SRC_TDM_CST0PRF.tbl
|  SRC_TDM_EXC1FIL.tbl
|  SRC_TDM_EXC1RAT.tbl
|  SRC_TDM_FLT0DRV.tbl
|  SRC_TDM_FLT0DRV_CDC.tbl
|  SRC_TDM_FLT0MST.tbl
|  SRC_TDM_FLT0MST_CDC.tbl
|  SRC_TDM_FLT0MST_HIST.tbl
|  SRC_TDM_FLT0OPT.tbl
|  SRC_TDM_FLT0SIT.tbl
|  SRC_TDM_FLT0VEH.tbl
|  SRC_TDM_FLT0VEH_CDC.tbl
|  SRC_TDM_FLT0VEH_HIST.tbl
|  SRC_TDM_FUL1MST.tbl
|  SRC_TDM_FUL1MST_HIST.tbl
|  SRC_TDM_HPD0TAX.tbl
|  SRC_TDM_HPD0TAX_HIST.tbl
|  SRC_TDM_MP_PGM_XREF.tbl
|  SRC_TDM_PAR0NER.tbl
|  SRC_TDM_PLS0CTL.tbl
|  SRC_TDM_PRE_TRANS_LOAD.prc
|  SRC_TDM_SIT1MST.tbl
|  SRC_TDM_WB_PLASTIC_TYPE_CODES.tbl
|  SRC_WB_AFFINITY_PGM.tbl
|  SRC_WB_POB_CONF_WAIVER_LIST.tbl
|  SRC_WB_WEX_PRODUCT_TO_NACS.tbl
|  STAGE_OWNER_DATA_DIR.dir
|  STG_ANCILLARY_GROUPING.tbl
|  STG_NACS_GROUPING.tbl
|  STG_PROGRAM_TEMPLATE.tbl
|  S_SIC_LOOKUP.tbl
|  T_ANCIL_DTL.tbl
|  T_ITEM_ACTIVITY.tbl
|  T_PURCH_TRANS.tbl
|  T_SALES_OPPORTUNITY.tbl
|  craptiv.tbl
|  dba_daily_account_snapshot_seq.seq
|  dba_monthly_account_snapshot_seq.seq
|  dba_run_src_dw_stg_purch_trans.sql
|  dba_run_src_ps_item_activity.sql
|  dba_run_src_ps_item_activity_arch.sql
|  dw_time_day.tbl
|  grant_all.sql
|  grant_exec.sql
|  grant_select.sql
+--scripts/
|  bi_runpcwfl.sh
|  edw_compress_partitions.sh
|  hub_copy_data_files.sh
|  hub_run_ad_extract.sh
|  hub_runpcwfl.sh
|  hub_set_env.sh
|  pc_env_var
|  powercenter_protdev.env
+--sql/
   D_DATE_PARTITION_MAINT.sql
   D_DEFAULT_INIT_LOAD.sql
   D_PROGRAM_FIX_CLM_1.4.sql
   Disable_constraints_qt.txt
   EDW_PARTITION_MAINTENANCE_2012.sql
   EDW_PARTITION_MAINTENANCE_2013.sql
   F_MONTHLY_LOAD.sql
   HT_218537_F_TRANS.sql
   MERGE_F_TRANS.sql
   M_SIC_LOOKUP_INSERT.sql
   M_SIC_LOOKUP_INSERT.tbl
   M_SPNR_PGM_FIX_CLM_1.4.sql
   QTEDW_FIX_1.sql
   STG_ANCILLARY_GROUPING.sql
   backup_purch_device.sql
   backup_purch_device_1.sql
   best_for_less_program_edw_fix.sql
   best_for_less_program_edw_fix_2.sql
   best_for_less_program_fix.sql
   clm_cluster_update.sql
   cr_priv_synonyms.sql
   cr_pub_synonyms.sql
   d_account insert default.sql
   d_date insert default.sql
   d_pos_and_site insert default.sql
   d_program insert default.sql
   d_program_update.sql
   d_purchase_device insert default.sql
   d_purchase_item insert default.sql
   d_region insert default.sql
   dba_d_account_merge.sql
   dba_d_national_id_merge.sql
   dba_d_reasoncode_merge.sql
   dba_d_reasoncode_merge_2.sql
   dba_edm_account_key_hist_merge.sql
   dba_edm_d_account_clm_1.3_hash_update.sql
   dba_edm_owner_cleanup.sql
   dba_edm_owner_drop_tables.sql
   dba_edw_account_key_hist_merge.sql
   dba_edw_owner_cleanup.sql
   dba_edw_owner_drop_tables.sql
   dba_exxon_pd_data_fix.sql
   dba_f_daily_acct_2011_04.sql
   dba_f_daily_acct_2011_04_1.sql
   dba_f_daily_acct_snapshot_2008.sql
   dba_f_daily_acct_snapshot_2009.sql
   dba_f_daily_acct_snapshot_2010.sql
   dba_national_id_merge.sql
   dba_national_id_merge_1.sql
   dba_oneshot_predw_f_daily_account_aggregate_246309.sql
   dba_reason_code_merge.sql
   dba_reason_code_merge_1.sql
   dba_run_d_date_efd_fix.sql
   dba_run_d_date_efd_fix_1.sql
   dba_run_d_date_efd_fix_2.sql
   dba_run_d_date_efd_fix_3.sql
   dba_run_date_fix.sql
   dba_run_drop_backup_tables_edm.sql
   dba_run_edw.sql
   dba_run_edw_owner_indexes.sql
   dba_run_fuel_convert.sql
   dba_run_ht_211125.sql
   dba_run_ht_218751.sql
   dba_run_indexes.sql
   dba_run_instructions_for_ps_item_arch.sql
   dba_run_m_acct_update.sql
   dba_run_m_pos_fix.sql
   dba_run_m_purch_device.sql
   dba_run_m_regions_lookup.sql
   dba_run_m_sic_lookup.sql
   dba_run_m_spnsr_pgm_ht_211125.sql
   dba_run_m_spnsr_pgm_ht_211125_2.sql
   dba_run_prep_src_sbl_purch_device_hist.tbl
   dba_run_prog_ht_211125.sql
   dba_run_src_dw_stg_ancil_dtl.sql
   dba_run_src_dw_stg_ancil_dtl_20101019.sql
   dba_run_src_dw_stg_ancil_dtl_lookup.sql
   dba_run_src_dw_stg_purch_trans.sql
   dba_run_src_ps_item_activity.sql
   dba_run_src_ps_item_activity_20101019.sql
   dba_run_src_ps_item_activity_arch.sql
   dba_run_src_ps_item_activity_cleanup.sql
   dba_run_src_sbl_acct.sql
   dba_run_src_sbl_acct_term.sql
   dba_run_src_sbl_addr.sql
   dba_run_src_sbl_bill_cd.sql
   dba_run_src_sbl_funder.sql
   dba_run_src_sbl_pd_prod.sql
   dba_run_src_sbl_pos.sql
   dba_run_src_sbl_product.sql
   dba_run_src_sbl_purch_device.sql
   dba_run_src_sbl_purch_device_full.sql
   dba_run_src_sbl_purch_device_run1.sql
   dba_run_src_sbl_s_opty.sql
   dba_run_src_sbl_site.sql
   dba_run_src_sbl_spnr_pgm.sql
   dba_run_src_sbl_stkhldr.sql
   dba_run_tax_convert.sql
   dba_run_update_credit_national_account_id_EDM.sql
   dba_run_update_credit_national_account_id_EDW.sql
   dba_run_update_credit_national_account_id_M_ACCT.sql
   dba_run_update_d_pos_and_site.sql
   dba_run_update_m_bill_cd.sql
   dba_run_update_national_id.sql
   dba_run_update_national_id_edw.sql
   dba_sbl_1980_pd_data_fix_update.sql
   dba_sbl_1980_terminated_pd_fix_last.sql
   dba_sbl_1980_terminated_pd_fix_last_2.sql
   dba_sbl_1980_terminated_pd_fix_last_3.sql
   dba_sbl_1980_terminated_pd_fix_last_4.sql
   dba_stage_owner_cleanup.sql
   dba_stage_owner_drop_tables.sql
   dba_truncate_f_daily_acct_1.2.sql
   dba_truncate_f_daily_acct_2008.sql
   dba_truncate_f_daily_acct_2009.sql
   dba_truncate_f_daily_acct_2010.sql
   dba_truncate_f_daily_acct_2011.sql
   dba_truncate_f_daily_acct_2011_2.sql
   dba_truncate_f_daily_acct_snapshot.sql
   drop_pub_synonyms.sql
   edm_dwloader_synonym.sql
   grant_all.sql
   grant_exec.sql
   grant_select.sql
   ht_00212204_datafix.sql
   m_spnr_pgm_update.sql
   program_bi_287_fix.sql
   program_distributor_fix_287.sql
   src_sbl_spnr_pgm.sql
