SET ECHO        ON 
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON 
SET HEADING     ON 
SET TRIMSPOOL   ON
SET TAB         OFF
set LINESIZE    2000

spool /i1/&&env/hub/logs/T_PURCH_TRANS_INTL_ALTER.log ; 

ALTER TABLE EDM_OWNER.T_PURCH_TRANS_INTL 
 MODIFY  (  SITE_STLMT_NBR_TRANS_UNTS           NUMBER( 20 , 8 ) ,  
            ACCT_STLMT_NBR_TRANS_UNTS           NUMBER( 20 , 8 ) , 
            US_STLMT_NBR_TRANS_UNTS             NUMBER( 20 , 8 ) ,
            SITE_PRE_STLMT_NBR_TRANS_UNTS       NUMBER( 20 , 8 ) , 
            ACCT_PRE_STLMT_NBR_TRANS_UNTS       NUMBER( 20 , 8 ) ,  
            US_PRE_STLMT_NBR_TRANS_UNTS         NUMBER( 20 , 8 ) ) ; 
            

COMMIT;

spool off;