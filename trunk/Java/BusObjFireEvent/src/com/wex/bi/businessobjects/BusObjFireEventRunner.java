/**
 *
 */
package com.wex.bi.businessobjects;

/**
 * @author Eric Maxham
 * @version 1.0
 */
public class BusObjFireEventRunner {

	/**
	 * @param args
	 * 
	 * args[0] = environment, i.e. dev, dev30, dit, dit2, prod, etc.
	 * args[1] = debug level, i.e. 1, 2, 3, 4, y, n
	 * 		     y = all, n = none, all others are levels.
	 * args[2] = the event to fire.
	 */
	public static void main(String[] args) {
        //Create the report object instance to begin processing
		BusObjFireEventInstance event = new BusObjFireEventInstance();
		event.fire( args[2] );
		
	}
}
