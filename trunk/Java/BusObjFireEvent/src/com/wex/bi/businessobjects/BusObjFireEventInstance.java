/**
 *
 */
package com.wex.bi.businessobjects;

import com.crystaldecisions.sdk.exception.SDKException;
import com.crystaldecisions.sdk.framework.IEnterpriseSession;
import com.crystaldecisions.sdk.occa.infostore.IInfoObjects;
import com.crystaldecisions.sdk.occa.infostore.IInfoStore;
import com.crystaldecisions.sdk.plugin.desktop.event.IEvent;
import com.crystaldecisions.sdk.plugin.desktop.event.IUserEvent;
import com.wex.bi.common.BOSession;

/**
 * @author Eric Maxham
 * @version 1.0
 */
public class BusObjFireEventInstance {

	private BOSession session = new BOSession();
	private IEnterpriseSession enterpriseSession = null;
	private IEvent boEvent = null;

	/**
	 * Default Constructor.
	 */
	public BusObjFireEventInstance( ) {
	}

	/**
	 * Fires the specified event on the Business Objects server.
	 * 
	 * @param eventName to fire.
	 */
	public void fire( String eventName ) {
		try {
			System.out.println("Get Business Objects Session.");
			enterpriseSession = session.StartSession( );

			System.out.println("Find Event");
			IInfoStore iStore = (IInfoStore) enterpriseSession.getService("InfoStore");
			IInfoObjects infoObjects = iStore.query("SELECT SI_ID, SI_NAME, SI_USERFULLNAME FROM CI_SYSTEMOBJECTS WHERE SI_NAME='" + eventName + "' And SI_KIND ='Event'");
			

			if (infoObjects.isEmpty()) {
				throw new Exception("Failed to find event [" + eventName + "].");
			}

			boEvent = (IEvent)infoObjects.get(0);
			System.out.println("Event [" + boEvent.getEventName() + "] found.");
			
		    boEvent.setEventType(0);
		    
			IUserEvent userEvent = (IUserEvent) boEvent.getEventInterface();
			
			userEvent.trigger();
			System.out.println("Event [" + boEvent.getEventName() + "] triggered.");

			iStore.commit(infoObjects);
		}
		catch(SDKException e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			
			throw new RuntimeException(" ERROR: Failed to find event." );
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
			throw new RuntimeException(" ERROR: Failed to find event." );
		}
		finally {
			session.EndSession();
			System.out.println("Business Objects Session Closed.");
		}
	}

	

}
