#!/usr/bin/sh
#
# SDLC15.1 - Release Package Builder script R1.0
#
# 20150301: CC - first version
#
#-------------------------------------------------------------------------
#
# environment variables
#
NM="sdlc151"                 # package name
VN=r1                        # version name
SD=~/wex/sdlc/15.1           # source directory
DD=$1                        # export directory
RN=${NM}_${VN}               # full release name
XO=~/wex/xo                  # business/operation object
NOTE=${SD}/release_note.sql  # BI4.0 release note header
#
# check input parameters
#
if [ "$DD" == "" ]; then
  echo "Usage:> build_${RN}.sh working_dir"
  exit 1
fi

if [ -d $DD ]; then
  echo "directory $DD already exist!"
  exit 1
else
  mkdir $DD
fi
#
# To facilitate Informatica housekeeping field post-modeling DDL export processing
#
function infa() {
    src=$1
    tmp=$2
    dst=$3
    for n in `grep 'CREATE TABLE' $SD/$src | cut -d' ' -f3`; do
        cat $SD/$tmp | sed -e "s/#{N}/${n}/g" >> $dst
    done
}

echo "start building $NM release package..."
#
# create packages directories
#
SRC=$DD/0_src
HUB=$DD/1_hub
STG=$DD/2_stg
EDW=$DD/3_edw

for d in $SRC $HUB $STG $EDW; do
  mkdir $d   
done

#-------------------------------------------------------------------------
# build for STAGE_OWNER
#-------------------------------------------------------------------------
SA=src

O_INIT="$SRC/${RN}a_init.sql"

S_DRP=${SA}_drp.ddl
S_TBL=${SA}_tbl.ddl
S_GNT=${SA}_gnt.ddl
S_SYN=${SA}_syn.ddl

cat $SD/${SA}_ps_tbl.sql    >  $SRC/$S_TBL
cat $SD/${SA}_sbl_tbl.sql   >> $SRC/$S_TBL

cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- SDLC14.2 HUB.STAGE_OWNER one time setup
@$S_DRP
@$S_TBL
@$S_GNT

SPOOL OFF
EOF

echo "-- drop  all tables and the constraints"     > $SRC/$S_DRP
echo "-- grant all tables and the constraints"     > $SRC/$S_GNT
echo "-- create STAGE_OWNER synonym for DWLOADER"  > $SRC/$S_SYN
for n in `grep 'CREATE TABLE' $SRC/$S_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"     >> $SRC/$S_DRP
  cat $SD/src_grant.sql | sed -e "s/#{N}/${n}/g"  >> $SRC/$S_GNT
  echo "CREATE OR REPLACE SYNONYM ${n} FOR STAGE_OWNER.${n};" >> $SRC/$S_SYN
done

echo "  $SRC> DDLs created."

#-------------------------------------------------------------------------
# build for EDM_OWNER
#-------------------------------------------------------------------------
#
# these are the scripts that needs to be run in sequence 
#
SA=hub

O_INIT="$HUB/${RN}a_init.sql"
O_LOAD="$HUB/${RN}b_load.sql"

H_DRP=${SA}_drp.ddl
H_TBL=${SA}_tbl.ddl
H_KEY=${SA}_key.ddl
H_SEQ=${SA}_seq.ddl
H_GNT=${SA}_gnt.ddl
H_SYN=${SA}_syn.ddl
#
# build store procedures packages
#
mv  $SRC/$S_SYN           $HUB/$S_SYN

cat $SD/m_tbl.sql     >   $HUB/$H_TBL
cat $SD/m_xtbl.sql    >>  $HUB/$H_TBL

cat $SD/m_key.sql     >   $HUB/$H_KEY
#
# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- BI40 HUB.EDM_OWNER one time setup
@$H_DRP
@$H_TBL
@$H_KEY
@$H_SEQ
@$H_GNT

SPOOL OFF
EOF

cat $NOTE > $O_LOAD
echo "-- create synonyms for DWLOADER" >> $O_LOAD
echo "@$S_SYN"                         >> $O_LOAD
echo "@$H_SYN"                         >> $O_LOAD
echo "SPOOL OFF"                       >> $O_LOAD
#
# build drop and grant scripts
#
echo "-- drop all tables and the constraints" > $HUB/$H_DRP
echo "-- grant access to tables "             > $HUB/$H_GNT

echo "-- add Informatica housekeeping fields" >> $HUB/$H_TBL
infa m_tbl.sql  m_ifa.sql  "$HUB/$H_TBL"
infa m_xtbl.sql m_xifa.sql "$HUB/$H_TBL"

echo "-- create sequences for dimension tables " > $HUB/$H_SEQ
# SEQ for M_* tables
for n in `grep 'CREATE TABLE' $SD/m_tbl.sql | cut -d' ' -f3 | sed -e "s/M_//"`; do
  cat $SD/m_seq.sql | sed -e "s/#{N}/${n}/g" >> $HUB/$H_SEQ
done

echo "-- add EDM_OWNER synonym for DWLOADER"  > $HUB/$H_SYN
for n in `grep 'CREATE TABLE' $HUB/$H_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"     >> $HUB/$H_DRP
  cat $SD/hub_grant.sql | sed -e "s/#{N}/${n}/g"  >> $HUB/$H_GNT
  echo "CREATE OR REPLACE SYNONYM ${n} FOR EDM_OWNER.${n};"  >> $HUB/$H_SYN
done
for n in `grep 'CREATE SEQUENCE' $HUB/$H_SEQ | cut -d' ' -f3`; do
  echo "CREATE OR REPLACE SYNONYM ${n} FOR EDM_OWNER.${n};"  >> $HUB/$H_SYN
done

echo "  $HUB> DDLs created."

#-------------------------------------------------------------------------
# build for EDW_STAGE_OWNER
#-------------------------------------------------------------------------
SA=stg
#
echo "  $STG> DDLs created."

#-------------------------------------------------------------------------
# build EDW_OWNER
#-------------------------------------------------------------------------
SA=edw

O_INIT="$EDW/${RN}a_init.sql"
O_ACT="$EDW/${RN}b_act.sql"
O_PRC="$EDW/${RN}c_prc.sql"

W_DRP=${SA}_drp.ddl
W_TBL=${SA}_tbl.ddl
W_GNT=${SA}_gnt.ddl
W_SYN=${SA}_syn.ddl

cat $SD/f_alt.sql >  $EDW/$W_TBL
cat $SD/f_vw.sql  >> $EDW/$W_TBL

# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- EDW.EDW_OWNER one time setup
@$W_DRP
@$W_TBL
@$W_GNT

SPOOL OFF
EOF

cat $NOTE > $O_ACT
echo "-- EDW_OWNER.SYNONYMS for DWLOADER, BORPTRNR, and USER_LDAP" >> $O_ACT
echo "@${W_SYN}" >> $O_ACT
#
# build workspace creation script
#
echo "-- drop all tables and the constraints" > $EDW/$W_DRP
echo "-- grant access to tables "             > $EDW/$W_GNT
echo "-- synonym for user access "            > $EDW/$W_SYN
for n in `grep 'CREATE TABLE' $EDW/$W_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"    >> $EDW/$W_DRP
  cat $SD/edw_grant.sql | sed -e "s/#{N}/${n}/g" >> $EDW/$W_GNT
  echo "CREATE OR REPLACE PUBLIC SYNONYM ${n} FOR EDW_OWNER.${n};" >> $EDW/$W_SYN
done
echo "SPOOL OFF" >> $O_ACT
echo "  $EDW> DDLs created."
#
# build store procedures packages
#
cat $NOTE > $O_PRC
echo "-- EDW_OWNER business/operation objects" >> $O_PRC
for n in log4me xos_tokenizer xos_session_mgr xos_workspace_mgr xbs_site_agg; do
  cp $XO/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
for n in f_revenue_aggregates f_gl_acct_daily_snapshot_load f_gl_acct_mon_snapshot_load f_gl_acct_mon_by_category_load; do
  cp $SD/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
echo "SPOOL OFF" >> $O_PRC
echo "  $EDW> Store Procedures created."
#
#-------------------------------------------------------------------------
# Done!
#-------------------------------------------------------------------------
echo "done building $NM release package"
