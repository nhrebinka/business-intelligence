CREATE OR REPLACE PROCEDURE F_GL_ACCT_MON_BY_CATEGORY_LOAD (
  p_min_dt date     := NULL,
  p_max_dt date     := NULL,
  p_ses_id varchar2 := to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4')
)
/*
  ============================================================================
  Name : F_GL_ACCT_MON_BY_CATEGORY_LOAD
  Spec.: Store procedure for monthly GL_ACCT snapshot denormalized from 
         F_GL_ACCT_MONTHLY_SNAPSHOT with FR_CATEGORY as columns.

  Dependency:
    log4me        package
    xos_tokenizer package

  Desc.:
    By utilizing Oracle PIVOT SQL support, for each account, we transpose 
    rows for each FR_CATEGORY into columns following these naming convention
      *_AMT     for amount
      *_DPC_AMT for depreciation amount

    The control flow as the following:
    1. get date range from stage seed table or input parameters
    2. find referenced partitions based on the date range
    3. for each partition do the following
       3.1 truncate partition that will be touched by the data
       3.2 populate the table with most recent data from monthly partition

  Note:
    1. Maintenance dimensional keys of D_FR_CATEOGY, i.e.
       Invalid:-2, Unknown:-1, and N/A:0 are filtered out in SQL by "> 0".
       Care should be taken when adding any maintenance key that might break 
       this condition.
    2. Cost to acquire marketing channel and cost of adjudication do not have
       a concept of depreciation. We decided to take them out of transposed
       column list. Care should be taken should we decided to add new categories
       into the list.
 
  Revision History:
  1.0 20140918 CC - first version

  ============================================================================
*/
AS
  FMT_DT   constant varchar2(20) := 'YYYY-MM-DD';
  SES_TYPE constant varchar2(40) := 'F_GL_ACCT_MON_BY_CATEGORY';

  SRC_TB   constant varchar2(80) := 'F_GL_ACCT_MONTHLY_SNAPSHOT';
  TGT_TB   constant varchar2(80) := 'F_GL_ACCT_MON_BY_CATEGORY';

  v_blist  TokList;       -- token collection of business date
  v_bdate  CLOB;          -- business date list
  v_ymlist TokList;       -- token collection of year_month

  v_proc   varchar2(80);
  v_min_dt date;
  v_max_dt date;

  v_sql    CLOB;
  v_part   varchar2(20);
  
  PROCEDURE get_date_range AS
  BEGIN
    CASE 
    WHEN p_min_dt IS NULL AND p_max_dt IS NULL THEN
      SELECT /*+ parallel */ min(d.CALENDAR_DATE_DT), max(d.CALENDAR_DATE_DT)
      INTO v_min_dt, v_max_dt
      FROM edw_stage_owner.f_gl_acct_daily_snapshot x, d_date d
      WHERE d.DATE_KEY = x.REVENUE_DATE_KEY;
    WHEN p_min_dt IS NULL THEN
      SELECT first_day_in_month_date_dt, last_day_in_month_date_dt 
      INTO   v_min_dt, v_max_dt
      FROM   d_date
      WHERE  calendar_date_dt = p_min_dt;
    ELSE
      v_min_dt := p_min_dt;
      v_max_dt := p_max_dt;
    END CASE;
  END;

  FUNCTION get_insert_sql(p_part varchar2) RETURN varchar2 
  AS
    v_sql CLOB := '
      INSERT /*+ append */ INTO ' || TGT_TB || '
      SELECT /*+ parallel */
        revenue_date_key,
        business_sector_key,
        business_segment_key,
        cost_center_key,
        program_key,
        account_key,
        account_hist_key,
        ''' || p_ses_id || ''' session_id,
        #{OLIST}
      FROM (
        SELECT /*+ parallel */
          revenue_date_key,
          business_sector_key,
          business_segment_key,
          fr_category_key,
          cost_center_key,
          program_key,
          account_key,
          account_hist_key,
          sum(amount)              amt,
          sum(depreciation_amount) dpc_amt
        FROM ' || SRC_TB || ' PARTITION (#{PARTI}) x
        GROUP BY
          revenue_date_key,
          business_sector_key,
          business_segment_key,
          fr_category_key,
          cost_center_key,
          program_key,
          account_key,
          account_hist_key
      )
      PIVOT (
        SUM(amt)     as amt,
        SUM(dpc_amt) as dpc_amt
        FOR fr_category_key IN (#{ILIST})
      )
    ';
    TYPE vc_t IS varray(100) OF varchar2(100);
    v_k vc_t;   -- fr_category_key list
    v_n vc_t;   -- fr_category_id  list

    v_eol  char(1);
    v_name varchar2(40);
    v_col  varchar2(40);

    v_ilist CLOB := '';
    v_olist CLOB := '';  
  BEGIN
    --
    -- get active FR_CATEGORIES
    --
    SELECT 
      to_char(fr_category_key),
      upper(replace(fr_category_id,' ','_')) 
    BULK COLLECT INTO v_k, v_n
    FROM   d_fr_category
    WHERE  fr_category_key > 0
    ORDER BY fr_category_key;
    --
    -- build substitution lists #{ILIST}, #{OLIST} for SQL query.
    --
    FOR i IN 1..v_k.COUNT
    LOOP
      v_eol  := CASE WHEN i=v_k.COUNT THEN '' ELSE ',' END;
      v_name := replace(v_n(i), 'MARKETING_CHANNEL', 'MKT');
      v_col  := 'C' || i; 
      v_ilist:= v_ilist || v_k(i) || ' AS ' || v_col || v_eol;
      v_olist:= v_olist || 
        'NVL(' || v_col || '_AMT,0) AS '     || v_name || '_AMT' ||
        -- 
        -- take out the fields that doesn't make sense
        --
        CASE WHEN (
          instr(v_name, 'ADJUDICATE' )>0 OR 
          instr(v_name, 'ACQUIRE_MKT')>0
        )
        THEN ''
        ELSE ', NVL(' || v_col || '_DPC_AMT,0) AS ' || v_name || '_DPC_AMT'
        END || v_eol;
    END LOOP;

    v_sql := replace(v_sql, '#{ILIST}', v_ilist);
    v_sql := replace(v_sql, '#{OLIST}', v_olist);
    v_sql := replace(v_sql, '#{PARTI}', p_part);

    RETURN v_sql;
  END get_insert_sql;

  PROCEDURE process_one_partition(p_part varchar2) AS
    v_sql CLOB;
  BEGIN
    --
    -- delete existing data if any
    --
    v_sql := '
      ALTER TABLE ' || TGT_TB || ' TRUNCATE PARTITION ' || p_part || '
      UPDATE INDEXES
    '
    ;
    log4me.debug(v_proc || ' clearing ' || v_part, v_sql, p_ses_id);
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
    --
    -- populate BY CATEGORY table from monthly
    --
    v_sql := get_insert_sql(p_part);
    log4me.debug(v_proc || ' populating ' || v_part, v_sql, p_ses_id);
    EXECUTE IMMEDIATE v_sql;
    dbms_output.put_line(v_sql);
    COMMIT;
  END process_one_partition;

BEGIN
  --
  -- get date range from seed table
  --
  get_date_range;

  v_proc := SES_TYPE || '[' ||
    to_char(v_min_dt, FMT_DT) || ',' ||
    to_char(v_max_dt, FMT_DT) ||
    ']';

  v_sql := '
    SELECT UNIQUE month_year_abbr FROM d_date 
    WHERE calendar_date_dt BETWEEN :d1 and :d2
  ';
  log4me.debug(v_proc, v_sql, p_ses_id);
  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_ymlist USING v_min_dt, v_max_dt;

  IF v_ymlist IS NULL OR v_ymlist.COUNT = 0 THEN
    log4me.warn(v_proc || ' no data found', p_ses_id);
    RETURN;  
  END IF;

  log4me.info('*** INIT ' || v_proc, p_ses_id);
  FOR i IN 1..v_ymlist.COUNT LOOP
    --
    -- walk through every snapshot_date 
    --
    v_part := 'YM_' || replace(v_ymlist(i), '-', '_');
    process_one_partition(v_part);
  END LOOP;
  log4me.info('*** DONE ' || v_proc, p_ses_id);

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('*** FAIL ' || v_proc, p_ses_id);
    RAISE; 
END F_GL_ACCT_MON_BY_CATEGORY_LOAD;
/
show errors


                
