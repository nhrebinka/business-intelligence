#!/usr/bin/sh
#
# BI4.0.2 - Release Package Builder script R1.0
#
# 20141022: CC - first version to add MANUAL_REBATE and TRUCK_STOP_FEE
#
#-------------------------------------------------------------------------
#
# environment variables
#

NM=bi4                       # package name
VN=r2                        # version name
SD=~/wex/bi4                 # bi4 related source directory
XO=~/wex/xo                  # business/operation object
RV=~/wex/sdlc/14.2           # SDLC 14.2 modification
DD=$1                        # export directory
RN=${NM}_${VN}               # full release name
NOTE=${SD}/release_note.sql  # BI4.0 release note header
#
# check input parameters
#
if [ "$DD" == "" ]; then
  echo "Usage:> build_${RN}.sh working_dir"
  exit 1
fi

if [ -d $DD ]; then
  echo "directory $DD already exist!"
  exit 1
else
  mkdir $DD
fi
#
# To facilitate Informatica housekeeping field post-modeling DDL export processing
#
function infa() {
    src=$1
    tmp=$2
    dst=$3
    for n in `grep 'CREATE TABLE' $src | cut -d' ' -f3`; do
        cat $tmp | sed -e "s/#{N}/${n}/g" >> $dst
    done
}

echo "start building $NM release package..."
#
# create packages directories
#
SRC=$DD/0_src
HUB=$DD/1_hub
STG=$DD/2_stg
EDW=$DD/3_edw

for d in $SRC $HUB $STG $EDW; do
  mkdir $d   
done

#-------------------------------------------------------------------------
# build for STAGE_OWNER
#-------------------------------------------------------------------------
SA=src

O_INIT="$SRC/${RN}a_init.sql"

S_DRP=${SA}_drp.ddl
S_TBL=${SA}_tbl.ddl
S_GNT=${SA}_gnt.ddl
S_SYN=${SA}_syn.ddl

cat $RV/${SA}_sbl_tbl.sql                >  $SRC/$S_TBL
cat $SD/${SA}_acct_since_date_lookup.sql >>  $SRC/$S_TBL

cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- BI40 HUB.STAGE_OWNER one time setup
@$S_DRP
@$S_TBL
@$S_GNT
EOF

echo "-- drop  all tables and the constraints"     > $SRC/$S_DRP
echo "-- grant all tables and the constraints"     > $SRC/$S_GNT
echo "-- create STAGE_OWNER synonym for DWLOADER"  > $SRC/$S_SYN
for n in `grep 'CREATE TABLE' $SRC/$S_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"     >> $SRC/$S_DRP
  cat $SD/src_grant.sql | sed -e "s/#{N}/${n}/g"  >> $SRC/$S_GNT
  echo "CREATE OR REPLACE SYNONYM ${n} FOR STAGE_OWNER.${n};" >> $SRC/$S_SYN
done

echo "  $SRC> DDLs created."

#-------------------------------------------------------------------------
# build for EDM_OWNER
#-------------------------------------------------------------------------
#
# these are the scripts that needs to be run in sequence 
#
SA=hub

O_PRC="$HUB/${RN}c_prc.sql"
O_RUN="$HUB/${RN}d_run.sql"

cat $NOTE > $O_PRC
echo "-- EDM_OWNER business/operation objects" >> $O_PRC
for n in xbs_acct_src_xform; do
  cp $XO/${n}.spp   $HUB/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
cat $NOTE > $O_RUN
echo "-- EDM_OWNER SINCE_DATE historical data prep" >> $O_RUN
cat $RV/${SA}_run.sql >> $O_RUN

echo "  $HUB> DDLs created."

#-------------------------------------------------------------------------
# build for EDW_STAGE_OWNER
#-------------------------------------------------------------------------
SA=stg

O_INIT="$STG/${RN}a_init.sql"

X_DRP=${SA}_drp.ddl
X_TBL=${SA}_tbl.ddl
X_GNT=${SA}_gnt.ddl

cp  $RV/x_tbl.sql  $STG/$X_TBL

# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- EDW.EDW_STAGE_OWNER one time setup
@$X_DRP
@$X_TBL
@$X_GNT
EOF
#
# build workspace creation script
#
echo "-- drop all tables and the constraints" > $STG/$X_DRP
echo "-- grant access to tables "             > $STG/$X_GNT
for n in `grep 'CREATE TABLE' $STG/$X_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"  >> $STG/$X_DRP
  cat $SD/x_gnt.sql | sed -e "s/#{N}/${n}/g"   >> $STG/$X_GNT
done
#
echo "  $STG> DDLs created."

#-------------------------------------------------------------------------
# build EDW_OWNER
#-------------------------------------------------------------------------
SA=edw

O_INIT="$EDW/${RN}a_init.sql"
O_ACT="$EDW/${RN}b_act.sql"
O_PRC="$EDW/${RN}c_prc.sql"
O_RUN="$EDW/${RN}d_run.sql"

W_DRP=${SA}_drp.ddl
W_TBL=${SA}_tbl.ddl
W_GNT=${SA}_gnt.ddl
W_SEQ=${SA}_seq.ddl
W_SYN=${SA}_syn.ddl

cat $RV/f_tbl.sql > $EDW/$W_TBL

# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- EDW.EDW_OWNER one time setup
@$W_DRP
@$W_TBL
@$W_SEQ
@$W_GNT
EOF

cat $NOTE > $O_ACT
echo "-- EDW_OWNER.SYNONYMS for DWLOADER, BORPTRNR, and USER_LDAP" >> $O_ACT
echo "@${W_SYN}" >> $O_ACT

echo "-- add Informatica housekeeping fields" >> $EDW/$W_TBL
infa $RV/f_tbl.sql $SD/f_ifa.sql "$EDW/$W_TBL"
#
# build workspace creation script
#
echo "-- drop all tables and the constraints" > $EDW/$W_DRP
echo "-- create sequence for fact tables"     > $EDW/$W_SEQ
echo "-- grant access to tables "             > $EDW/$W_GNT
echo "-- synonym for user access "            > $EDW/$W_SYN
for n in `grep 'CREATE TABLE' $EDW/$W_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"    >> $EDW/$W_DRP
  cat $SD/edw_grant.sql | sed -e "s/#{N}/${n}/g" >> $EDW/$W_GNT
  echo "CREATE OR REPLACE PUBLIC SYNONYM ${n} FOR EDW_OWNER.${n};" >> $EDW/$W_SYN
done
echo "  $EDW> DDLs created."
#
# updated procedures with MANUAL_REBATE_AMOUNT, TRUCK_STOP_FEE_AMOUNT
#
cat $NOTE > $O_PRC
echo "-- EDW_OWNER business/operation objects" >> $O_PRC
for n in f_transaction_line_item_load f_revenue_aggregates f_gl_acct_daily_snapshot_load; do
  cp $SD/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
for n in xbs_site_agg; do
  cp $XO/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
#
echo "  $EDW> Store Procedures created."
#
#-------------------------------------------------------------------------
# Done!
#-------------------------------------------------------------------------
echo "done building $NM release package"
