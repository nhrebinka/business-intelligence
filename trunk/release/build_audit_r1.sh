#!/usr/bin/sh
#
# Care Reactivation Audition v1.0 - Release Package Builder script
#
# 20140418: CC - first version
#
#-------------------------------------------------------------------------
#
# environment variables
#
NM=pd_audit_v1          # package name
DD=$1                   # export directory
XO=~/wex/xo             # xo packages
SD=~/wex/audit          # source directory
TB=D_PURCHASE_DEVICE    # target table
#
# check input parameters
#
if [ "$DD" == "" ]; then
  echo "Usage:> build_${NM}.sh working_dir"
  exit 1
fi

if [ -d $DD ]; then
  echo "directory $DD already exist!"
  exit 1
else
  mkdir $DD
fi

#
# these are the scripts that needs to be run in sequence 
#
O_INIT="$DD/${NM}a_init.sql"
O_LOAD="$DD/${NM}b_load.sql"
O_RUN="$DD/${NM}c_run.sql"
O_TRG="/tmp/d_purchase_device.prc"
#
# build store procedures packages
#
echo "$NM> start building release package..."
cp $XO/log4me.spp            $DD/log4me.prc
cp $XO/xos_audit_mgr.spp     $DD/xos_audit_mgr.prc
#
# build package and table creation scripts
#
echo "-- site aggregation one time load" > $O_INIT
echo "@log4me.prc"        >> $O_INIT
echo "@xos_audit_mgr.prc" >> $O_INIT
#
# build trigger creation script
#
cat <<EOF_SQL > $O_LOAD
-- create trigger script for $TB
set echo         off
set heading      off
set pagesize     0
set trimspool    on
set serveroutput on
set linesize     1000
spool $O_TRG
exec xos_audit_mgr.export_trigger('$TB');
spool off
EOF_SQL
#
# build trigger compilation script
#
echo "-- create trigger for $TB" > $O_RUN
echo "@$O_TRG" >> $O_RUN
#
# done
#
echo "$NM> done."



