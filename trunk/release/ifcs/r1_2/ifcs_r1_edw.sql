--
-- P4 IFCS R1 migration script
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ifcs_r1.log;

alter session enable parallel dml;

@xos_audit_mgr.prc
@d_upd_r1_edw.sql

SPOOL OFF


