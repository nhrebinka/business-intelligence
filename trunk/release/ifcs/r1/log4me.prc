/*
  Name : log4me Package
  Spec.: Logging utilities modeled against Log4j

  Revision History:
  v1.0 20140320 CC - first version
  v1.1 20140410 CC - add comments into the package spec
  v1.2 20140815 CC - comment out log table creation section for reuse
  v1.3 20141001 CC - touch up for readability
*/
-- ============================================================================
/*
DROP   TABLE xo_process_log PURGE;
CREATE TABLE xo_process_log
(
  event_at   timestamp      NOT NULL,
  event_type varchar2(10)   NOT NULL,
  event_id   varchar2(40),
  event_name varchar2(40),
  event_loc  varchar2(20),
  event_msg  varchar2(400),
  trace      CLOB
)
tablespace d_bi
;
-- ============================================================================
-- views
--
CREATE OR REPLACE VIEW xo_info AS
SELECT event_at, event_id, event_name, event_msg 
FROM  xo_process_log
WHERE event_type IN ('INFO')
ORDER BY event_at DESC
;
/
GRANT SELECT ON xo_info TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM xo_info FOR xo_info;

CREATE OR REPLACE VIEW xo_debug AS
SELECT 
  x.*, 
  regexp_replace(
    trim(replace(dbms_lob.substr(trace,4000), '|', '')),
    '[[:space:]]+', chr(32)
  ) trc
FROM xo_process_log x
ORDER BY event_at desc
;
/
GRANT SELECT ON xo_debug TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM xo_debug FOR xo_debug;
*/
-- ============================================================================
-- package spec
--
CREATE OR REPLACE PACKAGE log4me AS
/*
  The package is modeled against Log4J

  User can choose different level of logging, namely
    INFO:  for information, used mostly as process log
    WARN:  for warning, used in non-critical error
    ERR:   for critical (fatal) process error
    DEBUG: the most detailed logging, used in development mostly
           It can be disabled when released into production
           by simply calling enable() or disable().
  
  misc. functions
    split()  - splits a string on a given speparator
    otype()  - returns Oracle object type by name (on the callstack)
    caller() - returns the name of the caller (on the callstack)
  
  Note: 
    The package log into xo_process_log table for now
    we can enhance it to log into external file later if needed
*/
  FMT_DT constant varchar2(30) := 'YYYY-MM-DD HH24:MI:SS';

  g_debug  boolean := true;
  PROCEDURE enable;
  PROCEDURE disable;

  PROCEDURE info( p_msg varchar2, p_id varchar2 DEFAULT NULL);
  PROCEDURE warn(p_msg varchar2, p_id varchar2 DEFAULT NULL);
  PROCEDURE err( p_msg varchar2, p_id varchar2 DEFAULT NULL);
  PROCEDURE debug(
    p_msg  varchar2,
    p_more CLOB     DEFAULT NULL, 
    p_id   varchar2 DEFAULT NULL
  );
  PROCEDURE trace(
    p_type varchar2,
    p_msg  varchar2, 
    p_id   varchar2,
    p_name varchar2, 
    p_more CLOB
  );
  PROCEDURE split(
    p_orig IN  varchar2,
    p_find IN  varchar2,
    p_head OUT varchar2,
    p_tail OUT varchar2
  );
  FUNCTION otype(p_str varchar2) RETURN varchar2;
  FUNCTION caller RETURN varchar2;
END log4me;
/
show errors
    
-- ============================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY log4me AS
  LOG_TB   constant varchar2(40) := 'xo_process_log';
  NO_ID    constant varchar2(10) := '0';

  NL_CHAR  constant char(1) := CHR(10);
  TAB_CHAR constant char(1) := CHR(9);
  SP_CHAR  constant char(1) := '@';
  BAR_CHAR constant char(1) := '|';

-- ----------------------------------------------------------------------------
PROCEDURE enable  AS BEGIN g_debug := true;  END enable;
PROCEDURE disable AS BEGIN g_debug := false; END disable;

-- ----------------------------------------------------------------------------
PROCEDURE info(p_msg varchar2, p_id  varchar2 DEFAULT NULL) AS
BEGIN
  trace('INFO', p_msg, p_id, caller, NULL);
END info;

-- ----------------------------------------------------------------------------
PROCEDURE warn(p_msg varchar2, p_id  varchar2 DEFAULT NULL) AS
BEGIN
  trace('WARN', p_msg, p_id, caller, NULL);
END warn;

-- ----------------------------------------------------------------------------
PROCEDURE err(p_msg varchar2, p_id  varchar2 DEFAULT NULL) AS
BEGIN
  trace('ERROR', p_msg || ': ' || SQLERRM, p_id, 
        caller, dbms_utility.format_error_backtrace);
END err;

-- ----------------------------------------------------------------------------
PROCEDURE debug(
  p_msg  varchar2, 
  p_more CLOB     DEFAULT NULL,
  p_id   varchar2 DEFAULT NULL
) 
AS
  CMD   constant varchar2(10):= 'SQLCMD';
  HDR   constant varchar2(10):= 'DOSQL:';
  INJ   constant varchar2(4) := '--';
  idx   constant integer := instr(p_more, INJ)-length(INJ);
  X_CMD constant CLOB    := CASE WHEN idx > 0
    THEN substr(p_more, 1, length(p_more)-idx)
    ELSE p_more
  END;
BEGIN
  trace('DEBUG', p_msg, p_id, caller, X_CMD);
  IF p_msg = CMD AND instr(X_CMD, HDR)>0 THEN
    -- SQL: log4me.debug('SQLCMD',
    --   'DOSQL:insert x_jk (c1, i2) values (''ABC'', 123)'
    -- );
    -- PROC: log4me.debug('SQLCMD', 
    --   'DOSQL:begin xos_workspace_mgr.free(''X_D_SITE_A01''); end;'
    -- );
    EXECUTE IMMEDIATE replace(X_CMD, HDR, '');
    -- validate, commit or rollback
  END IF;

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  trace('DEBUG', p_msg || ' FAILED', p_id, caller, X_CMD);
END debug;

-- ----------------------------------------------------------------------------
PROCEDURE trace(
  p_type       varchar2,
  p_msg        varchar2,
  p_id         varchar2,
  p_name       varchar2, 
  p_more       CLOB
)
AS
  PRAGMA autonomous_transaction;

  X_NOW constant timestamp      := SYSTIMESTAMP;
  X_ID  constant varchar2(40)   := NVL(p_id, NO_ID);
  X_MSG constant varchar2(2000) := 
    '[' || to_char(X_NOW, FMT_DT) || ']' || 
    '<' ||  X_ID || '> ' || p_name || ': ' || p_type || '> ' || p_msg;
  X_ON  constant boolean := (p_type<>'DEBUG') or (p_type='DEBUG' and g_debug);
  X_SQL constant CLOB := '
    INSERT INTO ' || LOG_TB || '(
    event_at,
    event_type,
    event_id,
    event_name,
    event_loc,
    event_msg,
    trace
  )
  VALUES (:v, :t, :id, :n, :loc, :m, :x)
  ';
  X_EXTRA constant CLOB := NVL(translate(p_more, NL_CHAR, BAR_CHAR),'');
  v_name varchar2(40);
  v_loc  varchar2(20);
BEGIN
  --
  -- optional dbms_output logging
  --
  if X_ON then
    dbms_output.put_line(X_MSG);
    if (p_more is not NULL) then dbms_output.put_line(p_more);  end if;
  end if;
  --
  -- split name@loc
  --
  split(p_name, SP_CHAR, v_name, v_loc);

  EXECUTE IMMEDIATE X_SQL USING 
    X_NOW,
    p_type,
    X_ID,
    v_name,
    v_loc,
    p_msg,
    X_EXTRA;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  dbms_output.put_line(
    'ERROR: bad SQL=' || X_SQL || 'USING sysdate ' ||
     p_type || ', ' || X_ID || ', ' || v_name || ', ' || v_loc  || ', ' ||
     p_msg || p_more
  );
  RAISE;
END trace;

-- ----------------------------------------------------------------------------
-- split a string into two at an anchor
--
PROCEDURE split(
  p_orig  IN  varchar2,
  p_find  IN  varchar2,
  p_head  OUT varchar2,
  p_tail  OUT varchar2
)
AS
  i number;
BEGIN
  i := instr(p_orig, p_find);
  if (i > 0) then
    p_head := substr(p_orig, 1, i-1);
    p_tail := ltrim(substr(p_orig, i+length(p_find)));
  else  -- not found
    p_head := p_orig;
    p_tail := NULL;
  end if;
END split;

-- ----------------------------------------------------------------------------
-- identify Oracle object type in call stack
--
FUNCTION otype(p_str varchar2) RETURN varchar2 
AS
BEGIN
  if (   p_str like 'pr%')           then return 'procedure ';
  elsif (p_str like 'fun%')          then return 'function ';
  elsif (p_str like 'package body%') then return 'package body ';
  elsif (p_str like 'pack%')         then return 'package ';
  else return 'anonymous block ';
  end if;
END otype;

-- ----------------------------------------------------------------------------
-- well, we will have to do it ourselvs since no dbms_utility package
-- use the following call stack format 11r2
--
FUNCTION caller RETURN varchar2
AS
  v_stk    varchar2(2000) := dbms_utility.format_call_stack;

  v_lineno number         := 0;
  v_caller varchar2(60)   := '';

  h1 varchar2(255);
  ln varchar2(255);
  ok boolean := FALSE;
  r  number  := 0;
BEGIN
  -- --- PL/SQL Call Stack -----
  --   object      line  object
  --   handle    number  name
  -- 0x19b1f8868         1  anonymous block
  -- 0x177f3fe50        23  package body EDW_OWNER.LOG4ME
  -- 0x19c5aadd8        30  package body EDW_OWNER.SITE_AGG
  --
  LOOP
    split(v_stk, NL_CHAR, ln, v_stk);
    EXIT when (r = 3 or v_stk is NULL);     -- not found?

    if (NOT ok) then -- keep looping
       if (ln like '%handle%number%name%') then ok := TRUE; end if;
    else
      r := r + 1;    -- r = 1 is ME, 2 is MY Caller, 3 is Their Caller
      if (r = 3) then
        split(ln, ' ', h1, ln);
        if (ln is NOT NULL) then
          split(ln, ' ', h1, ln); -- take out the first piece
          v_lineno := to_number(h1);     -- 2nd piece i.e. line number
        end if;
        --
        -- get the last piece info of owner.object
        -- and pray that Oracle doesn't change the interface
        --
        split(ln, otype(ln), h1, v_caller);
      end if;
    end if;
  end LOOP;

  return v_caller || SP_CHAR || v_lineno;
end;

END log4me;
/
show errors

-- ============================================================================
GRANT EXECUTE ON log4me TO DWLOADER;
GRANT EXECUTE ON log4me TO EDW_OWNER_SELECT;
