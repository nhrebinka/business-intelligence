CREATE OR REPLACE PROCEDURE D_DATE_PARTITION_SPLIT(p_new_year integer) AS
/*
  Name : D_DATE_PARTITION_SPLIT
  Spec.: procedure for annual process to split range partitioning of
         D_DATE dimension.

  Usage: exec D_DATE_PARTITION_SPLIT(yyyy);
    1. loop through 12 months of the given year
       1a. split the partition
    2. TODO: optionally add the date data for given year

  Dependency:
    log4me package

  Revision History: 
  1.0 201112?? VK - original SQL version
  2.0 20141111 CC - parameterized PL/SQL version
                  
  Note:
    add_data() - DO NOT USE. It is not yet completed. D_DATE is currently 
    populated to 2040/12/31. It needs to be worked on when
    1. a need to separate between Warehouse and Mart key sets. Or,
    2. a new by-hour key set is needed.
*/
-- ==========================================================================
  FMT_DT constant varchar2(10) := 'YYYYMMDD';
  X_PROC varchar2(80) := 'D_DATE_PARTITION' || p_new_year;

  PROCEDURE split AS
    v_prc  varchar2(80) := X_PROC || '#split';
    v_dt   date;
    v_dmax varchar2(20);
    v_part varchar2(20);
    v_sql  varchar2(400);
    v_stat varchar2(400);
  BEGIN
    FOR i IN 1..12 LOOP
      v_dt   := to_date(to_char(p_new_year*10000+i*100+1, '09999999'), FMT_DT);
      v_part := 'YM_' || to_char(v_dt, 'YYYY_MM');

      v_dmax := to_char(trunc(add_months(v_dt, 1),'MM'), FMT_DT); -- next month
      v_sql  := '
        ALTER TABLE d_date SPLIT PARTITION MAX_VALUE AT (
          to_date(''' || v_dmax || ''',''' || FMT_DT || ''')
        )
        INTO (PARTITION ' || v_part || ' TABLESPACE d_bi, PARTITION MAX_VALUE)
        UPDATE GLOBAL INDEXES PARALLEL 8
      ';
      log4me.debug(v_prc || to_char(i,'09'), v_sql);
      EXECUTE IMMEDIATE v_sql;
    END LOOP;
    dbms_stats.gather_table_stats(USER, 'd_date');
  END split;

  PROCEDURE add_data AS
    X_SQL constant CLOB := '
      INSERT INTO xd_date (
        date_key,
        valid_real_date_flg,
        calendar_date_dt,
        date_text,
        day_of_week_name,
        day_of_week_abbr,
        day_of_week_number,

        sequential_day_number,
        sequential_week_number,
        sequential_month_number,

        day_in_month_number,
        day_in_calendar_year_number,
        last_day_in_month_flg,
        first_day_in_month_date_dt,
        last_day_in_month_date_dt,

        week_in_calendar_year_number,
        month_in_calendar_year_number,
        month_name,
        month_abbr,
        month_year_abbr,
        month_year_name,

        quarter_name,
        quarter_abbr,
        quarter_number,
        year_name,
        year_number,

        us_holiday_flg,
        canada_holidy_flg,

        weekday_flg,
        weekend_flg,
        equivalent_fuel_day_factor,
        last_business_day_in_month_flg,
        last_business_day_in_month_dt,

        week_in_month_number,
        year_name_abbr,
        quarter_year_name,
        quarter_year_abbr,
        sequenctial_quarter_number,

        spend_days_factor,
        holiday_name,
        row_create_dttm,
        row_last_mod_dttm,
        row_last_mod_proc_nm,
        row_last_mod_proc_seq_nbr,
        efd_month_year_abbr
      )
      VALUES (
        :k,
        ''Yes'',
        :d,
        to_char(:d, ''MM/DD/YYYY''),
        to_char(:d, ''DAY''),
        to_char(:d, ''DY''),
        to_number(to_char(:d, ''D'')),

        :sd,   -- seq_d#
        :sw + to_number(to_char(:d, ''WW'')),
        :sm + to_number(to_char(:d, ''MM'')),

        to_number(to_char(:d, ''DD'')),
        to_number(to_char(:d, ''DDD'')),

        DECODE(last_day(:d)=:d, ''Yes'', ''No''),
        trunc(:d, ''month''),
        last_day(:d),

        to_number(to_char(:d, ''WW'')),
        to_number(to_char(:d, ''MM'')),
        to_char(:d, ''MONTH''),
        to_char(:d, ''MON''),
        to_char(:d, ''YYYY-MM''),
        to_char(:d, ''YYYY-MONTH''),
        
        DECODE(to_char(:d, ''Q''), 
          ''1'', ''FIRST'', 
          ''2'', ''SECOND'', 
          ''3'', ''THIRD'',
          ''4'', ''FOURTH''
        ) || '' QUARTER'',
        ''Q'' || to_char(:d, ''Q''),
        to_number(to_char(:d, ''Q'')),
        to_char(:d, ''YYYY''),
        to_number(to_char(:d, ''YYYY'')),

        ''No''   as us_holiday_flg,
        ''Unkn'' as canada_holiday_flg,

        CASE WHEN to_char(:d, ''D'') NOT IN (''1'', ''7'') THEN ''Yes'' ELSE ''No'' END as weekday_flg,
        CASE WHEN to_char(:d, ''D'') IN (''1'', ''7'')     THEN ''Yes'' ELSE ''No'' END as weekend_flg,
        0            as equivalent_fuel_day_factor,
        ''No''       as last_business_day_in_month_flg,
        last_day(:d) as last_business_day_in_month_dt,

        to_number(to_char(last_day(:d),''W'')),
        to_char(:d, ''YY''),
        to_char(:d, ''YY'') || ''Q'' || to_char(:d, ''Q''),
        :sq + to_number(to_char(:d, ''Q'')) as sequenctial_quarter_number,

        0 as spend_days_factor,
        ''Unknown'',   

        systimestamp,
        systimestamp,
        ''' || X_PROC || ''',
        :rownum,
        ''Unknown''
      )
    ';
    v_prc   varchar2(80) := X_PROC || '#add_date';
    v_dt    date;
    v_dcnt  integer;
    v_key   integer;
    v_dn    integer;
    v_wn    integer;
    v_mn    integer;
    v_qn    integer;
  BEGIN
    log4me.debug(v_prc, X_SQL);
    SELECT
      to_number(to_char(to_date(p_new_year || '1231', 'YYYYMMDD'), 'DDD')) nd,
      date_key, 
      sequential_day_number, 
      sequential_week_number,
      sequential_month_number,
      sequential_quarter_number
    INTO v_dcnt, v_key, v_dn, v_wn, v_mn, v_qn
    FROM d_date WHERE date_key = (
      SELECT max(date_key) FROM d_date
      WHERE year_number = p_new_year
    );
    --
    -- walk through the year
    --
/*
    FORALL i IN 1..v_dcnt
      EXECUTE IMMEDIATE v_sql USING v_key+i, v_dt+i;
    COMMIT;
 */
    log4me.debug(v_prc, v_dcnt || ' days added');
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      log4me.err(v_prc || ' failed to insert');
      RAISE;
  END add_data;

BEGIN
  log4me.info('INIT ' || X_PROC);
  split;
--
-- optionally add date data for the given year 
--  
--  add_data();
--
  log4me.info('DONE ' || X_PROC);

EXCEPTION
  WHEN OTHERS THEN
    log4me.err('FAIL ' || X_PROC || SQLERRM);
    RAISE;
END D_DATE_PARTITION_SPLIT;
/
show errors
-- ==========================================================================
