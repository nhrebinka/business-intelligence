--
-- IFCS - CiC - alter DDL for Dimension tables 
--
-- ------------------------------------------------------------------------
-- 
ALTER TABLE D_PURCHASE_DEVICE ADD (SOURCE_SYSTEM_CODE VARCHAR2(30));
ALTER TABLE D_POS_AND_SITE    ADD (SOURCE_SYSTEM_CODE VARCHAR2(30));
ALTER TABLE D_LINE_ITEM_TYPE  ADD (SOURCE_SYSTEM_CODE VARCHAR2(30));
