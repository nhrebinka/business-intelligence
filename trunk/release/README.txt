.
README.txt             
build_audit_r1.sh      # bundler for D_PURCHASE_DEVICE auditor
build_bi4_r1.sh        #         for BI4.0 R1
build_bi4_r2.sh        #         for BI4.0 R2
build_sdlc142_r1.sh    #         for SDLC 14.2         
build_sdlc151_r1.sh    #         for SDLC 15.1
build_site_agg_r1.sh   #         for Site Agg
+--acct1/                                # max card count fix1
|  HT288233_max_card_count_fix.sql
|  f_daily_account_aggregates.prc
|  f_daily_account_aggregates_h.prc
|  f_daily_account_aggs_h_dfix.prc
+--acct2/                                # max card count fix2
|  HT288233_max_card_count_fix2.sql
|  f_monthly_account_agg_h_dfix.prc
+--audit/                                # D_PURCHASE_DEVICE auditor
|  d_audit_test.sql
|  d_purchase_device_trigger.prc
|  ht278605.sql
|  log4me.prc
|  poc.sql
|  xos_audit_mgr.prc
+--bi4/                                  # BI4.0 R1 release
|  +--0_src/
|  |  bi4_r1a_init.sql
|  |  src_drp.ddl
|  |  src_gnt.ddl
|  |  src_tbl.ddl
|  +--1_hub/
|  |  bi4_r1a_init.sql
|  |  bi4_r1b_load.sql
|  |  hub_drp.ddl
|  |  hub_gnt.ddl
|  |  hub_key.ddl
|  |  hub_seq.ddl
|  |  hub_syn.ddl
|  |  hub_tbl.ddl
|  |  src_syn.ddl
|  +--2_stg/
|  |  bi4_r1a_init.sql
|  |  stg_drp.ddl
|  |  stg_gnt.ddl
|  |  stg_tbl.ddl
|  +--3_edw/
|     bi4_r1a_init.sql
|     bi4_r1b_act.sql
|     bi4_r1c_prc.sql
|     edw_drp.ddl
|     edw_gnt.ddl
|     edw_key.ddl
|     edw_seq.ddl
|     edw_syn.ddl
|     edw_tbl.ddl
|     f_gl_acct_daily_snapshot_load.prc
|     f_gl_acct_mon_by_category_load.prc
|     f_gl_acct_mon_snapshot_load.prc
|     f_transaction_line_item_load.prc
|     log4me.prc
|     xbs_site_agg.prc
|     xos_session_mgr.prc
|     xos_tokenizer.prc
|     xos_workspace_mgr.prc
+--bi4r2/                                 # BI4.0 R2 release
|  +--0_src/
|  |  bi4_r2a_init.sql
|  |  src_drp.ddl
|  |  src_gnt.ddl
|  |  src_syn.ddl
|  |  src_tbl.ddl
|  +--1_hub/
|  |  bi4_r2c_prc.sql
|  |  bi4_r2d_run.sql
|  |  xbs_acct_src_xform.prc
|  +--2_stg/
|  |  bi4_r2a_init.sql
|  |  stg_drp.ddl
|  |  stg_gnt.ddl
|  |  stg_tbl.ddl
|  +--3_edw/
|     bi4_r2a_init.sql
|     bi4_r2b_act.sql
|     bi4_r2c_prc.sql
|     edw_drp.ddl
|     edw_gnt.ddl
|     edw_seq.ddl
|     edw_syn.ddl
|     edw_tbl.ddl
|     f_gl_acct_daily_snapshot_load.prc
|     f_revenue_aggregates.prc
|     f_transaction_line_item_load.prc
|     xbs_site_agg.prc
+--heat/                                   # Heat release packages
|  +--HT288233_daily/
|  |  HT288233_max_card_count_fix.sql
|  |  f_daily_account_aggregates.prc
|  |  f_daily_account_aggregates_h.prc
|  |  f_daily_account_aggs_h_dfix.prc
|  +--HT288233_monthly/
|  |  HT288233_max_card_count_fix2.sql
|  |  f_monthly_account_agg_h_dfix.prc
|  +--HT288233_monthly_2/
|  |  HT288233_max_card_count_fix2.sql
|  |  f_monthly_account_agg_h_dfix.prc
|  +--HT290224B_Site_Agg/
|  |  ht290224_fix.sql
|  +--HT290224C_Site_Agg/
|  |  log4me.prc
|  |  site_agg_r1.sql
|  |  xbs_site_agg.prc
|  |  xos_session_mgr.prc
|  |  xos_workspace_mgr.prc
|  +--HT292418_Site_Agg/
|  |  ht292418_fix.sql
|  +--bi40rtm/                             # BI4.0 RTM
|  |  +--0_src/
|  |  |  bi4_r1a_init.sql
|  |  |  src_drp.ddl
|  |  |  src_gnt.ddl
|  |  |  src_tbl.ddl
|  |  +--1_hub/
|  |  |  bi4_r1a_init.sql
|  |  |  bi4_r1b_load.sql
|  |  |  hub_drp.ddl
|  |  |  hub_gnt.ddl
|  |  |  hub_key.ddl
|  |  |  hub_seq.ddl
|  |  |  hub_syn.ddl
|  |  |  hub_tbl.ddl
|  |  |  src_syn.ddl
|  |  +--2_stg/
|  |  |  bi4_r1a_init.sql
|  |  |  stg_drp.ddl
|  |  |  stg_gnt.ddl
|  |  |  stg_tbl.ddl
|  |  +--3_edw/
|  |     bi4_r1a_init.sql
|  |     bi4_r1b_act.sql
|  |     bi4_r1c_prc.sql
|  |     edw_drp.ddl
|  |     edw_gnt.ddl
|  |     edw_key.ddl
|  |     edw_seq.ddl
|  |     edw_syn.ddl
|  |     edw_tbl.ddl
|  |     f_gl_acct_daily_snapshot_load.prc
|  |     f_gl_acct_mon_by_category_load.prc
|  |     f_gl_acct_mon_snapshot_load.prc
|  |     f_transaction_line_item_load.prc
|  |     log4me.prc
|  |     xbs_site_agg.prc
|  |     xos_session_mgr.prc
|  |     xos_tokenizer.prc
|  |     xos_workspace_mgr.prc
|  +--date_view/
|  |  d_date_vw.sql
|  +--ht255358/
|  |  ht255358_patch.sql
|  |  src_drp.ddl
|  |  src_gnt.ddl
|  |  src_tbl.ddl
|  +--ht290224/
|  |  ht290224_fix.sql
|  +--ht290450_sdlc_151/
|  |  f_alt.sql
|  |  f_revenue_aggregates.prc
|  |  f_transaction_line_item_load.prc
|  |  ht290450_edw.sql
|  |  ht290450_hub.sql
|  |  ht290450_stg.sql
|  |  m_alt.sql
|  |  release_note.sql
|  |  x_alt.sql
|  |  xbs_site_agg.prc
|  |  xm_site_vw.ddl
|  +--ht292418/
|  |  ht292418_fix.sql
|  +--ht295309/
|  |  ht295309_fix.sql
|  |  log4me.prc
|  +--ht295843a/
|  |  f_revenue_agreegates_h.prc
|  +--ht297508/
|  |  ht297508_fix_1.sql
|  +--ht297508_affi_pkg/
|  |  f_daily_affinity_program_check_orig.prc
|  |  ht297508_edw.sql
|  |  xos_acct_affinity_fix.prc
|  +--ht297623/
|  |  ht297623_fix_2.sql
|  |  monthly_revenue_snapshot_fix.sql
|  +--ht297623a/
|  |  edw_run.ddl
|  |  ht297623_patch.sql
|  |  ht297623_setup.sql
|  |  hub_drp.ddl
|  |  hub_gnt.ddl
|  |  hub_tbl.ddl
|  |  log4me.prc
|  +--ht297623b/
|  |  ht297623_edm.sql
|  |  ht297623_ldr.sql
|  +--ht297830/
|  |  ht297830_fix.sql
|  |  xos_account_affinity_fix.prc
|  +--ht297830b/
|  |  ht297830_edm.sql
|  |  ht297830_ldr.sql
|  |  ht297830_src.sql
|  |  hub_tbl.ddl
|  |  src_drp.ddl
|  |  src_gnt.ddl
|  |  src_tbl.ddl
|  +--ht298251/
|  |  edw_drp.ddl
|  |  edw_gnt.ddl
|  |  edw_syn.ddl
|  |  edw_tbl.ddl
|  |  ht298251a_init.sql
|  |  ht298251d_run.sql
|  +--ht298691/
|  |  ht298691_edw.sql
|  |  ht298691_stg.sql
|  +--ht299031/
|  |  ht299031_edw_fix.sql
|  |  ht299031_hub_fix.sql
|  +--ht299723/
|  |  d_syn_ht299723.ddl
|  +--ht300819_site_agg_fix/
|  |  ht300819_site_agg_fix.sql
|  |  xbs_site_agg.prc
|  |  xos_session_mgr.prc
|  |  xos_workspace_mgr.prc
|  +--ht301103_rev_agg_1202/
|  |  ht301103_rev_agg_1202.sql
|  +--ht301154_mrbt_sign_fix/
|  |  ht301154_edw.sql
|  +--ht302974_affi/
|  |  f_daily_affinity_program_check.prc
|  |  ht302974_edw.sql
|  +--ht303282_affi/
|  |  f_daily_affinity_program_check.prc
|  |  ht303282_edw.sql
|  |  xos_acct_affinity_fix.prc
|  +--ht303657_mrbt_his_agg/
|  |  f_revenue_aggregates.prc
|  |  ht303657_edw.sql
|  +--ht303657_mrbt_site_agg/
|  |  ht303657_edw.sql
|  |  ht303657_edw2.sql
|  |  not_null.sql
|  |  xbs_site_agg.prc
|  |  xos_session_mgr.prc
|  |  xos_workspace_mgr.prc
|  +--ht306669_speedway/
|  |  f_alt.sql
|  |  f_revenue_aggregates.prc
|  |  f_transaction_line_item_load.prc
|  |  ht306669_edw.sql
|  |  ht306669_stg.sql
|  |  release_note.sql
|  |  x_alt.sql
|  |  xbs_site_agg.prc
|  |  xm_site_vw.ddl
|  +--ht308458_anci_fix/
|  |  ht308458_edw.sql
|  +--ht309365_bi_mon_by_cat/
|  |  f_gl_acct_mon_by_category_load.prc
|  |  ht309365_edw.sql
|  +--ht309594_bi_gl_daily_load/
|  |  f_gl_acct_mon_by_category_load.prc
|  |  ht309594_edw.sql
|  +--ht309690_affi_0454_upd/
|  |  f_daily_affinity_program_check.prc
|  |  ht309690_edw.sql
|  +--ht310762_large_fleet_rbt/
|  |  ht310762_edw_a.sql
|  |  ht310762_edw_b.sql
|  +--ht312340_large_fleet_rbt/
|  |  f_daily_affinity_program_check.prc
|  |  ht312340_edw.sql
|  |  ht312340_edw_a1.sql
|  |  ht312340_edw_a2.sql
|  +--ht312340_large_fleet_rbt_2/
|  |  f_daily_affinity_program_check.prc
|  |  ht312340_edw_a1.sql
|  |  ht312340_edw_a2.sql
|  +--ht312340_large_fleet_rbt_3/
|  |  ht312340_edw_a1.sql
|  |  ht312340_edw_a2.sql
|  +--ht312340_large_fleet_rbt_4/
|  |  ht312340_edw_a1.sql
|  |  ht312340_edw_a2.sql
|  +--ht312340_large_fleet_rbt_5/
|  |  ht312340_edw_a1.sql
|  |  ht312340_edw_a2.sql
|  +--site_agg_r1_prod/
|  |  d_user_hist_vw.sql
|  |  d_user_vw.sql
|  |  site_agg_r1a_init.sql
|  |  site_agg_r1b_load.sql
|  |  site_agg_r1c_run.sql
|  |  site_agg_r1d_idx.sql
|  |  xbs_site_agg.prc
|  |  xd_site_a01.ddl
|  |  xd_site_a02.ddl
|  |  xd_site_a03.ddl
|  |  xd_site_a04.ddl
|  |  xd_site_a05.ddl
|  |  xd_site_a06.ddl
|  |  xd_site_b.ddl
|  |  xm_site_vw.ddl
|  |  xos_session_mgr.prc
|  |  xos_workspace_mgr.prc
|  +--site_agg_r1_prod_batch2_v2/
|  |  site_agg_r1.sql
|  |  site_agg_r1a_init.sql
|  |  site_agg_r1b_load.sql
|  |  site_agg_r1c_run.sql
|  |  site_agg_r1d_idx.sql
|  +--site_agg_r1_prod_batch3/
|     d_date_vw.sql
|     site_agg_r1.sql
|     site_agg_r1a_init.sql
|     site_agg_r1b_load.sql
|     site_agg_r1c_run.sql
|     site_agg_r1d_idx.sql
+--ifcs/                                   # EDW backup before IFCS migration
|  +--r1/
|  |  d_backup.sql
|  |  d_date_partition_split.prc
|  |  d_tbl.ddl
|  |  d_upd.sql
|  |  ifcs_r1_edw.sql
|  |  ifcs_r1_hub.sql
|  |  log4me.prc
|  |  m_backup.sql
|  |  xos_tokenizer.prc
|  +--r1_1/
|  |  d_upd_r1_1.sql
|  |  ifcs_r1_hub.sql
|  +--r1_2/
|     d_upd_r1_edw.sql
|     ifcs_r1_edw.sql
|     xos_audit_mgr.prc
+--sdlc/                                   # SDLC releases
|  +--142r1/
|  |  +--0_src/
|  |  |  sdlc142_r1a_init.sql
|  |  |  src_drp.ddl
|  |  |  src_gnt.ddl
|  |  |  src_tbl.ddl
|  |  +--1_hub/
|  |  |  hub_drp.ddl
|  |  |  hub_gnt.ddl
|  |  |  hub_key.ddl
|  |  |  hub_seq.ddl
|  |  |  hub_syn.ddl
|  |  |  hub_tbl.ddl
|  |  |  sdlc142_r1a_init.sql
|  |  |  sdlc142_r1b_load.sql
|  |  |  src_syn.ddl
|  |  +--3_edw/
|  |     edw_drp.ddl
|  |     edw_gnt.ddl
|  |     edw_syn.ddl
|  |     edw_tbl.ddl
|  |     f_gl_acct_daily_snapshot_load.prc
|  |     f_gl_acct_mon_by_category_load.prc
|  |     f_gl_acct_mon_snapshot_load.prc
|  |     f_revenue_aggregates.prc
|  |     log4me.prc
|  |     sdlc142_r1a_init.sql
|  |     sdlc142_r1b_act.sql
|  |     sdlc142_r1c_prc.sql
|  |     xbs_site_agg.prc
|  |     xos_session_mgr.prc
|  |     xos_tokenizer.prc
|  |     xos_workspace_mgr.prc
|  +--142r1_ht299781/
|     +--0_src/
|     |  sdlc142_r2a_init.sql
|     |  src_drp.ddl
|     |  src_gnt.ddl
|     |  src_tbl.ddl
|     +--1_hub/
|     |  hub_tbl.ddl
|     |  hub_upd.ddl
|     |  sdlc142_r2a_init.sql
|     +--3_edw/
|        hub_tbl.ddl
|        sdlc142_r2a_init.sql
+--site/                                   # Site Agg release
   +--sa1/                                 # R1 - DDL and Stored Proc
   |  d_user_hist_vw.sql
   |  d_user_vw.sql
   |  site_agg_r1a_init.sql
   |  site_agg_r1b_load.sql
   |  site_agg_r1c_run.sql
   |  site_agg_r1d_idx.sql
   |  xbs_site_agg.prc
   |  xd_site_a01.ddl
   |  xd_site_a02.ddl
   |  xd_site_a03.ddl
   |  xd_site_a04.ddl
   |  xd_site_a05.ddl
   |  xd_site_a06.ddl
   |  xd_site_b.ddl
   |  xm_site_vw.ddl
   |  xos_session_mgr.prc
   |  xos_workspace_mgr.prc
   +--sa2/                                 # R2 - Historical loads
   |  site_agg_r1.sql
   |  site_agg_r1a_init.sql
   |  site_agg_r1b_load.sql
   |  site_agg_r1c_run.sql
   |  site_agg_r1d_idx.sql
   +--sa3/                                 # R3 - Date view
   |  d_date_vw.sql
   |  site_agg_r1.sql
   |  site_agg_r1a_init.sql
   |  site_agg_r1b_load.sql
   |  site_agg_r1c_run.sql
   |  site_agg_r1d_idx.sql
   +--sa4/                                 # R4 - Date View, Grant
   |  d_date_vw.sql
   +--sa5/                                 # R5 - Historical Re-run
   |  site_agg_r1.sql
   +--sa6/                                 # R6 - session manager update
      log4me.prc
      site_agg_r1.sql
      xbs_site_agg.prc
      xos_session_mgr.prc
      xos_workspace_mgr.prc
