-- add EDM_OWNER synonym for DWLOADER
CREATE OR REPLACE SYNONYM M_GL_PRODUCT FOR EDM_OWNER.M_GL_PRODUCT;
CREATE OR REPLACE SYNONYM M_GL_PRODUCT_XREF FOR EDM_OWNER.M_GL_PRODUCT_XREF;
CREATE OR REPLACE SYNONYM EDM_GL_PRODUCT_KEY_SEQ FOR EDM_OWNER.EDM_GL_PRODUCT_KEY_SEQ;
