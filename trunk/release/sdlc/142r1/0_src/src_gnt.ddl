-- grant all tables and the constraints
GRANT ALL    ON SRC_PS_GL_PRODUCT_TBL TO DWLOADER;
GRANT ALL    ON SRC_PS_GL_PRODUCT_TBL TO EDM_OWNER;
GRANT SELECT ON SRC_PS_GL_PRODUCT_TBL TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_SBL_S_ORG_EXT_X TO DWLOADER;
GRANT ALL    ON SRC_SBL_S_ORG_EXT_X TO EDM_OWNER;
GRANT SELECT ON SRC_SBL_S_ORG_EXT_X TO STAGE_OWNER_SELECT;
