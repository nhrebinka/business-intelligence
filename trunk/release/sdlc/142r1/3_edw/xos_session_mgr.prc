/*
  Name : xos_session Package - Process Management Objects and Procedures
  Spec.: For Session-based Process Management

  Dependency:
    log4me package

  Revision History: 
  1.0 20140320 CC - first try
  1.1 20140324 CC - add source_name, move constants to PACKAGE spec
  1.2 20140430 CC - use FF4 timestamp for session_id

  2.0 20140516 CC - add session-controlled workspace for auditing
  2.1 20140804 CC - change format of comments, and revision history
                    commented out table/sequence creation section (for reuse)
  2.2 20140814 CC - fix previous_section to find the most recent session
  2.3 20140829 CC - add_session_from_seed now returns ym_abbr (==this month)
  2.4 20140902 CC - fix previous_section(); rownum=1 doesn't work as expected;
                    enforce session_id logging
*/
-- ============================================================================
/*
 --
 -- make sure the following sequence, table, and index are created
 --
DROP   SEQUENCE xo_session_key_seq;
CREATE SEQUENCE xo_session_key_seq
  START WITH 10000001
  MINVALUE 10000000
  MAXVALUE 999999999999999999999999999
  NOCYCLE;

DROP   TABLE xo_session;
CREATE TABLE xo_session
(
  session_key   integer       NOT NULL,  -- SK
  session_id    varchar2(40)  NOT NULL,  -- NK (unique)
  session_type  varchar2(40),
  status        varchar2(20),
  source_name   varchar2(80),
  rev_ym_abbr   varchar2(20),
  rev_bgn_dt    date,
  rev_end_dt    date,
  run_id        varchar2(40),
  batch_id      varchar2(40),
  proc_name     varchar2(80),
  proc_seq_nbr  integer,
  proc_rowcount integer,
  proc_elapsed  integer,
  created_at    timestamp     NOT NULL,
  updated_at    timestamp     NOT NULL,
  CONSTRAINT xpk_session_key UNIQUE (session_key),
  CONSTRAINT xnk_session_id  UNIQUE (session_id)
)
;
CREATE BITMAP INDEX xbi_xo_session 
ON xo_session(SESSION_TYPE, STATUS)
tablespace i_bi
;
*/
CREATE OR REPLACE PACKAGE xos_session_mgr AS
/*
  Session Manager for BSS/OSS session metadata management

    Session management is a concept of fine-grained data warehouse 
    management borrowed from modern business operations with a hierarchy of 
  
       Run -> Batch -> Session
  
    A Run typically means a logical concept of one business process
    which could have multiple flows of logical Batches either going in 
    parallel or serial paths in a workflow environment. A Batch then
    can have one or multiple Sessions which works on physical level and
    can run mostly in parallel. 
  
    User can define different types of session based on requirement of
    business process, say daily_revenue_agg, daily_site_agg... Aside from
    system resource constraints, different types of sessions should be
    independent of each other and should not interfere with each other's
    operation. That said, for global optimization, we should always 
    consider adding system resource management package later when needed.
   
    A Session, the lowest level of a business process, is designed to run
    independently to each other so that it can scale onto the MPP or Cloud 
    platform if needed. It goes through the following states
  
    INIT - a new session is introduced as a place holder, this can be
           created by scheduled process or manual. The logical/conceptual
           separation of a process from a session is subtle but mainly 
           for production traceability. i.e. no session is created without
           a reason (design by contract).
    OPEN - signify the session is been worked on (similar a ticket item)
    DONE - the session is been worked on and is CLOSED
    FAIL - the session has been worked on and the result is a failure.
           the status does not change until further process usually by
           operator's intervention to reset it back to INIT. We can later
           introduce RESET or RESTART type of deal if needed.  
  
  Spec:
    add_session() - add new session to the session list with INIT status
  
    open()  - open a specific session of certain session type just like
              a ticketing system
    close() - close a session similar to a ticketing system
  
  
  Note:
    1. Currently, Run, Batch, and Session IDs are associated with timestamp
       i.e. YYYYMMDD.HHMISS.nnnn for readability. Should there be need of 
       higher precision, other mechanism should be considered.
       Because we tie the session_id to the clock, so we have to 
       make sure we don't create two session within 0.1 millisecond
*/
  FMT_DT      constant varchar2(20) := 'YYYY-MM-DD';
  FMT_BATCH   constant varchar2(20) := 'YYYYMMDD';
  FMT_RUN     constant varchar2(40) := FMT_BATCH || '.HH24MISS';
  FMT_SES     constant varchar2(40) := FMT_RUN   || '.FF4';

  STATUS_INIT constant varchar2(10) := 'INIT';
  STATUS_OPEN constant varchar2(10) := 'OPEN';
  STATUS_DONE constant varchar2(10) := 'DONE';
  STATUS_FAIL constant varchar2(10) := 'FAIL';

  PROCEDURE get_date_range(
    p_bgn_dt     date,
    p_end_dt     date,
    p_seed_table varchar2,
    o_min_dt OUT date,
    o_max_dt OUT date
  );
  --
  -- adding a new session entry into xo_session table
  -- note: 
  --   1. The procedure is usually called within a loop,
  --      in order to have a transactional control,
  --      this procedure does not COMMIT after each insert.
  --      The caller need to COMMIT after calling this procedure
  --
  PROCEDURE add_session (
    p_ses_type     varchar2,
    p_source_table varchar2,
    p_ym_abbr      varchar2,
    p_ses_id       varchar2 := to_char(systimestamp, FMT_SES),
    p_run_id       varchar2 := to_char(sysdate,      FMT_RUN),
    p_batch_id     varchar2 := to_char(sysdate,      FMT_BATCH)
  );
  --
  -- Procedure SEED_DATES populate process management entries
  -- 
  -- 1. fetch seeding date range from control table
  -- 2. for each month (month_year_abbr) does the following
  --    2.1 create a session entry in xo_session table with status 'OPEN'
  --
  FUNCTION add_session_from_seed (
    p_ses_type     varchar2,
    p_seed_table   varchar2,
    p_source_table varchar2
  ) RETURN varchar2;
  --
  -- open a session to work on (set status to BUSY)
  --
  PROCEDURE open(
    p_ses_type     varchar2, 
    p_proc_name    varchar2,
    o_ses_id       OUT varchar2,
    o_ym           OUT varchar2,
    o_bgn_dt       OUT varchar2,
    o_end_dt       OUT varchar2
  );

  --
  -- close a session when completed (status to DONE, or FAIL) and row count if any
  --
  PROCEDURE close(
    p_ses_id     varchar2, 
    p_status     varchar2,
    p_count      integer := -1,
    p_elapsed    integer := NULL
  );

  PROCEDURE previous_session(
    p_ses_type     varchar2,
    p_ym           varchar2,
    o_ses_id   OUT varchar2
  );
END xos_session_mgr;
/

CREATE OR REPLACE PACKAGE BODY xos_session_mgr AS
  X_SES_TB constant varchar2(40) := 'xo_session';
  X_SES_SQ constant varchar2(40) := 'xo_session_key_seq';

PROCEDURE get_date_range(
  p_bgn_dt     date,
  p_end_dt     date,
  p_seed_table varchar2,
  o_min_dt OUT date,
  o_max_dt OUT date
) AS
BEGIN
  CASE 
  WHEN p_bgn_dt IS NULL AND p_end_dt IS NULL THEN
    EXECUTE IMMEDIATE '
      SELECT /*+ parallel */ 
        min(d.CALENDAR_DATE_DT), max(d.CALENDAR_DATE_DT)
      FROM ' || p_seed_table || ' x, d_date d
      WHERE d.DATE_KEY = x.REVENUE_DATE_KEY'
    INTO o_min_dt, o_max_dt;
  WHEN p_bgn_dt IS NULL THEN
    SELECT first_day_in_month_date_dt, last_day_in_month_date_dt 
    INTO   o_min_dt, o_max_dt
    FROM   d_date
    WHERE  calendar_date_dt = p_bgn_dt;
  ELSE
    o_min_dt := p_bgn_dt;
    o_max_dt := p_end_dt;
  END CASE;
END get_date_range;

PROCEDURE add_session (
  p_ses_type     varchar2,
  p_source_table varchar2,
  p_ym_abbr      varchar2,
  p_ses_id       varchar2 := to_char(systimestamp, FMT_SES),
  p_run_id       varchar2 := to_char(sysdate,      FMT_RUN),
  p_batch_id     varchar2 := to_char(sysdate,      FMT_BATCH)
)
AS
  X_PROC constant varchar2(80) := 'add session for ' || p_ses_id;
  X_SQL  constant CLOB := '
    INSERT INTO ' || X_SES_TB || '(
      SESSION_KEY,
      SESSION_ID,
      SESSION_TYPE,
      STATUS,
      SOURCE_NAME,
      REV_YM_ABBR,
      REV_BGN_DT,
      REV_END_DT,
      RUN_ID,
      BATCH_ID,
      CREATED_AT,
      UPDATED_AT
    ) 
    VALUES (' || 
      X_SES_SQ || '.NEXTVAL,
      :id, :t, :s, :n, :x,
      (SELECT min(calendar_date_dt) FROM d_date WHERE month_year_abbr=:x),
      (SELECT max(calendar_date_dt) FROM d_date WHERE month_year_abbr=:x),
      :r, :b,
      SYSDATE,
      SYSDATE
    )
    ';
    X_USING constant CLOB := 'USING ' || 
      p_ses_id   || ', ' || 
      NVL(p_ses_type,'')     || ', ' || 
      STATUS_INIT    || ', ' || 
      NVL(p_source_table,'') || ', ' || 
      p_ym_abbr      || ', ' ||
      p_run_id       || ', ' || 
      p_batch_id;
BEGIN
    log4me.debug(X_PROC, X_SQL || X_USING);
    EXECUTE IMMEDIATE X_SQL USING 
      p_ses_id,
      p_ses_type,
      STATUS_INIT,
      p_source_table,
      p_ym_abbr,
      p_ym_abbr,
      p_ym_abbr,
      p_run_id,
      p_batch_id;
    -- NO COMMIT here, the caller needs to do it
END add_session;

FUNCTION add_session_from_seed (
  p_ses_type     varchar2,
  p_seed_table   varchar2,
  p_source_table varchar2
) RETURN varchar2
AS
  X_SQL_CUR constant CLOB := '
    SELECT /*+ parallel(x, 12) */
    DISTINCT d.MONTH_YEAR_ABBR
    FROM ' || p_seed_table || ' x, D_DATE d
    WHERE x.revenue_date_key = d.date_key
  ';

  X_BATCH_ID constant varchar2(20) := to_char(sysdate, FMT_BATCH);
  X_RUN_ID   constant varchar2(20) := to_char(sysdate, FMT_RUN);

  TYPE VCUR IS REF CURSOR;
  c_ym_abbr    VCUR;

  v_ym_abbr    varchar2(20);        -- revenue year_month_abbr
  v_msg        varchar2(200);
  v_cnt        integer := 1;
BEGIN
  log4me.debug('fetch date range from: ' || p_seed_table);
  OPEN c_ym_abbr FOR X_SQL_CUR;
  LOOP
    FETCH c_ym_abbr INTO v_ym_abbr;
    EXIT WHEN c_ym_abbr%NOTFOUND;

    v_msg := 'create ' || p_ses_type || ' session for: ' || v_ym_abbr;
    log4me.debug(v_msg);

    add_session(
      p_ses_type,
      p_source_table,
      v_ym_abbr,
      X_RUN_ID || '.' || to_char(v_cnt,'0009'),  -- ses_id
      X_RUN_ID,
      X_BATCH_ID
    );

    v_cnt := v_cnt + 1;
  END LOOP;
  CLOSE c_ym_abbr;
  --
  -- commit all sessions inserted from the loop above as one transaction
  --
  COMMIT;
  RETURN v_ym_abbr;

EXCEPTION
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(v_msg);
  if c_ym_abbr%ISOPEN then CLOSE c_ym_abbr; end if;
  RAISE;
END add_session_from_seed;
--
-- open a session to work on
--
PROCEDURE open(
  p_ses_type     varchar2,
  p_proc_name    varchar2,
  o_ses_id       OUT varchar2,
  o_ym           OUT varchar2,
  o_bgn_dt       OUT varchar2,
  o_end_dt       OUT varchar2
) 
AS
 -- 
 -- pop session (LIFO, most recent row first)
 --
  X_SQL_FIND constant CLOB := '
    SELECT session_id, rev_ym_abbr, bgn_dt, end_dt FROM (
      SELECT 
        session_id,
        rev_ym_abbr,
        to_char(REV_BGN_DT, ''' || FMT_DT || ''') bgn_dt,
        to_char(REV_END_DT, ''' || FMT_DT || ''') end_dt
      FROM  ' || X_SES_TB || '
      WHERE 1=1
        AND session_type=''' || p_ses_type || '''
        AND STATUS=''' || STATUS_INIT || '''
      ORDER BY UPDATED_AT DESC
    )
    WHERE rownum=1
  ';
  X_SQL_UPD constant CLOB := '
    UPDATE ' || X_SES_TB || ' SET
      STATUS      =''' || STATUS_OPEN || ''',
      UPDATED_AT  =sysdate,
      PROC_NAME   =:n,
      PROC_SEQ_NBR=1
    WHERE SESSION_ID=:id
  ';
BEGIN
  --
  -- get date range from the session (%NOTFOUND exception if no session)
  --
  log4me.debug('find session with type: ' || p_ses_type, X_SQL_FIND);
  EXECUTE IMMEDIATE X_SQL_FIND INTO o_ses_id, o_ym, o_bgn_dt, o_end_dt;

  if (o_ses_id is NULL) then return; end if;  -- extra precaution here
  --
  -- change session status to 'OPEN', i.e. case openned
  --
  log4me.debug(
    p_proc_name || ' open session ' || o_ses_id, 
    X_SQL_UPD || ' USING ' || p_proc_name || ', ' || o_ses_id,
    o_ses_id
  );
  EXECUTE IMMEDIATE X_SQL_UPD USING p_proc_name, o_ses_id;
  COMMIT;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.debug('no session found for: ' || p_ses_type);
  return;
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err('set session ' || o_ses_id || ' to ' || STATUS_OPEN, o_ses_id);
  RAISE;
END open;

PROCEDURE close(
  p_ses_id   varchar2, 
  p_status   varchar2,
  p_count    integer := -1,
  p_elapsed  integer := NULL
) 
AS
  X_PROC     constant varchar2(80) := 'close session ' || p_ses_id;
  X_SQL_FIND constant CLOB := '
    SELECT UPDATED_AT FROM ' || X_SES_TB || '
    WHERE SESSION_ID = :id
  ';
  X_SQL_UPD  constant CLOB := '
    UPDATE ' || X_SES_TB || ' SET
      STATUS=:s,
      PROC_ROWCOUNT=:c,
      PROC_ELAPSED=:d,
      UPDATED_AT=sysdate
    WHERE SESSION_ID=:id
  ';
  v_msg     CLOB;
  v_updated date;
  v_delta   integer;
BEGIN
  IF p_elapsed IS NULL THEN
    EXECUTE IMMEDIATE X_SQL_FIND INTO v_updated USING p_ses_id;
    v_delta := NVL(86400 * (sysdate - v_updated), -1);
  ELSE
    v_delta := p_elapsed;
  END IF;

  v_msg := 
     X_SQL_UPD || ' USING ' || 
     p_status  || ',' || p_count || ',' || v_delta || ', ' || p_ses_id;
  log4me.debug(X_PROC, v_msg, p_ses_id);

  EXECUTE IMMEDIATE X_SQL_UPD USING p_status, p_count, v_delta, p_ses_id;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(X_PROC, p_ses_id);
  RAISE;
END close;

PROCEDURE previous_session(
  p_ses_type     varchar2,
  p_ym           varchar2,
  o_ses_id   OUT varchar2
) 
AS
  X_PROC constant varchar2(80) := 
    'previous session of ' || p_ym || ' for ' || p_ses_type;
  X_SQL  constant CLOB := '
    SELECT session_id
    FROM (
      SELECT session_id
      FROM  ' || X_SES_TB || '
      WHERE rev_ym_abbr =''' || p_ym || '''
        AND session_type=''' || p_ses_type || '''
        AND STATUS      =''' || STATUS_DONE    || '''
      ORDER BY updated_at DESC
    )
    WHERE rownum = 1
  ';
BEGIN
  log4me.debug('find ' || X_PROC, X_SQL);
  EXECUTE IMMEDIATE X_SQL INTO o_ses_id;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.debug('no ' || X_PROC || ' found', '', o_ses_id);
  o_ses_id := NULL;
  RETURN;
WHEN OTHERS THEN
  log4me.err(X_PROC || X_SQL, o_ses_id);
  RAISE;
END previous_session;

END xos_session_mgr;
/
show errors
-- ============================================================================
GRANT EXECUTE ON xos_session_mgr TO DWLOADER;
GRANT EXECUTE ON xos_session_mgr TO EDW_OWNER_SELECT;


