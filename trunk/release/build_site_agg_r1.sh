#!/usr/bin/sh
#
# Site Aggregation - Release Package Builder script - R1.0
#
# 20140501: CC - first version
# 20140505: CC - add monthly view
#
#-------------------------------------------------------------------------
#
# environment variables
#
NM=site_agg_r1          # package name
DD=$1                   # export directory
XO=~/wex/xo             # xo packages
SD=~/wex/site           # site agg related source directory
NUM_WS=6                # max idx of workspaces (total 6)
#
# check input parameters
#
if [ "$DD" == "" ]; then
  echo "Usage:> build_${NM}.sh working_dir"
  exit 1
fi

if [ -d $DD ]; then
  echo "directory $DD already exist!"
  exit 1
else
  mkdir $DD
fi

#
# these are the scripts that needs to be run in sequence 
#
O_INIT="$DD/${NM}a_init.sql"
O_LOAD="$DD/${NM}b_load.sql"
O_RUN="$DD/${NM}c_run.sql"
O_IDX="$DD/${NM}d_idx.sql"
#
# build store procedures packages
#
echo "$NM> start building release package..."
cp $XO/xos_session_mgr.spp   $DD/xos_session_mgr.prc
cp $XO/xos_workspace_mgr.spp $DD/xos_workspace_mgr.prc
cp $XO/xbs_site_agg.spp      $DD/xbs_site_agg.prc
#
# create the tables
#
cp $SD/xd_site_b.sql         $DD/xd_site_b.ddl
cp $SD/xd_site_b_idx.sql     $O_IDX
cp $SD/xm_site2.sql          $DD/xm_site_vw.ddl
#
# build package and table creation scripts
#
echo "-- site aggregation one time load" > $O_INIT
echo "@xos_session_mgr.prc"   >> $O_INIT
echo "@xos_workspace_mgr.prc" >> $O_INIT
echo "@xbs_site_agg.prc"      >> $O_INIT
echo "@xd_site_b.ddl"         >> $O_INIT
echo "@xm_site_vw.ddl"        >> $O_INIT
#
# build workspace creation script
#
for n in `seq 1 $NUM_WS`; do
  fn="xd_site_a0${n}.ddl"
  cat $SD/xd_site2.sql | sed -e "s/#{N}/A0${n}/" > $DD/$fn
  echo "@${fn}" >> $O_INIT
done
echo "$NM>   $NUM_WS workspaces created."
#
# build session creation script
#
echo "-- add historical site agg sessions" > $O_LOAD
cnt=0
function ses() {
  y=$1
  m=`printf "%02d" $2`
  echo "exec xbs_site_agg.add_session_for_ym('${y}-${m}');" >> $O_LOAD
  echo "alter index x_d_site_b_ui1 rebuild partition ym_${y}_${m};" >> $O_IDX
  cnt=$((cnt+1))
}

function yses() {
  y=$1
  for m in {1..12}; do 
    m1=$((13-$m))
    ses $y $m1
  done
}

# for 2014-01 to -04 only, leave 2014-05 for nightly job
for m in {1..4}; do 
  ses "2014" $m
done
# backfill to 2010
for y in {3,2,1,0}; do 
  yses "201${y}"
done
#
# build the run script (with safty)
#
max=$(($NUM_WS-2))
div=$(($cnt/$max))
rmd=$(($cnt-$div*$max))
echo "-- one-time historical site aggregation" > $O_RUN
echo "alter session enable parallel dml;"  >> $O_RUN
echo "begin"                               >> $O_RUN
echo "  for n in 1..$div loop"             >> $O_RUN
  for i in `seq 1 $max`; do
    echo "    xbs_site_agg.run_one_session;" >> $O_RUN
  done
  echo "    xos_workspace_mgr.push_all('D_SITE_AGG');" >> $O_RUN
echo "  end loop;" >> $O_RUN
for i in `seq 1 $rmd`; do
  echo "  xbs_site_agg.run_one_session;"   >> $O_RUN
done
echo "  UPDATE xo_session SET status='HOLD' WHERE status='INIT';" >> $O_RUN
echo "  commit;"                                >> $O_RUN
echo "  F_DAILY_SITE_AGGREGATES;"               >> $O_RUN
echo "  commit;"                                >> $O_RUN
echo "end;"        >> $O_RUN
echo "/"           >> $O_RUN
#
# done
#
echo "$NM>   $cnt sessions created."



