SET ECHO         ON
SET SERVEROUTPUT ON
SET NEWPAGE      0
SET SPACE        0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL    ON
SET TAB          OFF
SET ESCAPE       ON
SET LINESIZE     2000

SPOOL /i1/&env/hub/logs/dba_oneshot_predw_HT274371a.log;

CREATE OR REPLACE FORCE VIEW EDW_OWNER.D_USER_CREDIT_OWNER_HIST_VW
AS
SELECT 
   USER_KEY,
   USER_HIST_KEY,
   USER_ID,
   USER_ROLE_NAME,
   USER_TITLE_NAME,
   USER_DEPT_NAME,
   USER_EMAIL_NAME,
   USER_FIRST_NAME,
   USER_LAST_NAME,
   USER_ROLE_START_DATE,
   USER_ROLE_END_DATE,
   USER_REP_CODE,
   CURRENT_RECORD_FLG
FROM EDW_OWNER.D_USER
;
/

GRANT SELECT ON EDW_OWNER.D_USER_CREDIT_OWNER_HIST_VW TO BORPTRNR;
GRANT SELECT ON EDW_OWNER.D_USER_CREDIT_OWNER_HIST_VW TO USER_LDAP;
GRANT SELECT ON EDW_OWNER.D_USER_CREDIT_OWNER_HIST_VW TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM D_USER_CREDIT_OWNER_HIST_VW FOR EDW_OWNER.D_USER_CREDIT_OWNER_HIST_VW;

/* Formatted on 4/9/2014 10:15:12 AM (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW EDW_OWNER.D_USER_SALES_CREATOR_HIST_VW
AS 
SELECT 
   USER_KEY,
   USER_HIST_KEY,
   USER_ID,
   USER_ROLE_NAME,
   USER_TITLE_NAME,
   USER_DEPT_NAME,
   USER_EMAIL_NAME,
   USER_FIRST_NAME,
   USER_LAST_NAME,
   USER_ROLE_START_DATE,
   USER_ROLE_END_DATE,
   USER_REP_CODE,
   CURRENT_RECORD_FLG
FROM EDW_OWNER.D_USER
WHERE LOWER(USER_ROLE_NAME) LIKE '%sale%' OR USER_KEY IN (0, -1, -2)
;
/

GRANT SELECT ON EDW_OWNER.D_USER_SALES_CREATOR_HIST_VW TO BORPTRNR;
GRANT SELECT ON EDW_OWNER.D_USER_SALES_CREATOR_HIST_VW TO USER_LDAP;
GRANT SELECT ON EDW_OWNER.D_USER_SALES_CREATOR_HIST_VW TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM D_USER_SALES_CREATOR_HIST_VW FOR EDW_OWNER.D_USER_SALES_CREATOR_HIST_VW;

/* Formatted on 4/9/2014 10:17:33 AM (QP5 v5.215.12089.38647) */
CREATE OR REPLACE FORCE VIEW EDW_OWNER.D_USER_SALES_OWNER_HIST_VW
AS
SELECT 
   USER_KEY,
   USER_HIST_KEY,
   USER_ID,
   USER_ROLE_NAME,
   USER_TITLE_NAME,
   USER_DEPT_NAME,
   USER_EMAIL_NAME,
   USER_FIRST_NAME,
   USER_LAST_NAME,
   USER_ROLE_START_DATE,
   USER_ROLE_END_DATE,
   USER_REP_CODE,
   CURRENT_RECORD_FLG
FROM EDW_OWNER.D_USER
WHERE LOWER (USER_ROLE_NAME) LIKE '%sale%' OR USER_KEY IN (0, -1, -2)
;
/

GRANT SELECT ON EDW_OWNER.D_USER_SALES_OWNER_HIST_VW TO BORPTRNR;
GRANT SELECT ON EDW_OWNER.D_USER_SALES_OWNER_HIST_VW TO USER_LDAP;
GRANT SELECT ON EDW_OWNER.D_USER_SALES_OWNER_HIST_VW TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM D_USER_SALES_OWNER_HIST_VW FOR EDW_OWNER.D_USER_SALES_OWNER_HIST_VW;

/* Formatted on 4/24/2014 3:55:43 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW EDW_OWNER.D_CREDIT_ANALYST_HIST_VW
AS
SELECT 
  USER_KEY,
  USER_HIST_KEY,
  USER_ID,
  USER_ROLE_NAME,
  USER_TITLE_NAME,
  USER_DEPT_NAME,
  USER_EMAIL_NAME,
  USER_FIRST_NAME,
  USER_LAST_NAME,
  USER_ROLE_START_DATE,
  USER_ROLE_END_DATE,
  USER_REP_CODE
FROM  EDW_OWNER.D_USER
WHERE USER_ROLE_NAME='Credit Analyst' OR USER_KEY in (0, -1, -2)
;
/

GRANT SELECT ON EDW_OWNER.D_CREDIT_ANALYST_HIST_VW TO BORPTRNR;
GRANT SELECT ON EDW_OWNER.D_CREDIT_ANALYST_HIST_VW TO USER_LDAP;
GRANT SELECT ON EDW_OWNER.D_CREDIT_ANALYST_HIST_VW TO EDW_OWNER_SELECT;
CREATE OR REPLACE PUBLIC SYNONYM D_CREDIT_ANALYST_HIST_VW FOR EDW_OWNER.D_CREDIT_ANALYST_HIST_VW;
-- CREATE OR REPLACE SYNONYM BORPTRNR.D_CREDIT_ANALYST_HIST_VW FOR EDW_OWNER.D_CREDIT_ANALYST_HIST_VW;
-- CREATE OR REPLACE SYNONYM USER_LDAP.D_CREDIT_ANALYST_HIST_VW FOR EDW_OWNER.D_CREDIT_ANALYST_HIST_VW;

COMMIT;
SPOOL OFF;
/



