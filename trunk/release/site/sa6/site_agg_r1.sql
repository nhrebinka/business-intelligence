--
-- HT287759 - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht290224_site_agg_fix_c.log;

@log4me.prc
@xos_workspace_mgr.prc
@xos_session_mgr.prc
@xbs_site_agg.prc

SPOOL OFF


