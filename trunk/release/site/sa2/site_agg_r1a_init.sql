-- site aggregation one time load
-- 
GRANT SELECT ON EDW_OWNER.XO_SESSION   TO EDW_OWNER_SELECT;
GRANT SELECT ON EDW_OWNER.XO_WORKSPACE TO EDW_OWNER_SELECT;

begin
  for n in 1..6 loop
    xos_workspace_mgr.free('X_D_SITE_A0' || n);
  end loop;
end;
/
