CREATE OR REPLACE PROCEDURE EDW_OWNER.F_GL_ACCT_DAILY_SNAPSHOT_LOAD
AS
/*
  ============================================================================
  Name : F_GL_ACCT_DAILY_SNAPSHOT_LOAD
  Spec.: Store procedure for daily GL_ACCT snapshot loading from 
         EDW_STAGE_OWNER to EDW_OWNER. This process supports re-run, i.e.
         It will clear the data produced by previous run identified by
         the same business date (ODATE or SNAPSHOT_DATE_KEY).

  Dependency:
    log4me package
    xos_tokenizer.concat_rpad

  Desc.:
    1. get date range from seed table (EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT)
    2. loop through every business date (SNAPSHOT_DATE_KEY)
       2.1 find every partitions that will be touched for the business date
       2.2 loop through each partition from the list 
           2.2.1 delete existing date from target table from the partition
           2.2.2 insert new data into target table

  Revision History:
  1.0 20140626 SSirigiri - created this procedure
  1.1 20140825 CC        - add re-run logic and logging info
  1.2 20141029 CC        - call monthly and monthly_by_category at the end

  ToDo:
    1. Enable partition compressiong
    2. Replace delete with read-modify-write
  ============================================================================
*/
  FMT_DT     constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';
  v_ses_id   constant varchar2(80) := to_char(systimestamp, FMT_DT);
  v_ses_type constant varchar2(80) := 'F_GL_ACCT_DAILY';
  v_src      constant varchar2(80) := 'EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  v_tgt      constant varchar2(80) := 'EDW_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  --
  -- date range control variables
  --
  v_blist    TokList;       -- token collection of business date
  v_bdate    CLOB;          -- business date list
  v_ymlist   TokList;       -- token collection of year_month
  --
  -- process variables
  --
  v_proc     varchar2(80);
  v_sql      varchar2(400);

  PROCEDURE process_one_partition(p_bdate varchar2, p_ym varchar2) AS
    v_part   varchar2(20);
    v_sql    CLOB;
  BEGIN
    --
    -- delete existing data if any
    --
    v_part := 'YM_' || replace(p_ym, '-', '_');
    v_sql  := '
    DELETE FROM ' || v_tgt || ' PARTITION(' || v_part || ')
    WHERE SNAPSHOT_DATE_KEY = ' || p_bdate
    ;
    log4me.debug(v_proc || ' delete existing data for ' || v_part, v_sql, v_ses_id);
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
    --
    -- adding new data
    --
    v_sql  := '
    INSERT /*+ append */ 
    INTO ' || v_tgt || '(
      GL_ACCT_DAILY_SNAPSHOT_KEY,
      REVENUE_DATE_KEY,
      BUSINESS_SECTOR_KEY,
      BUSINESS_SEGMENT_KEY,
      FR_CATEGORY_KEY,
      COST_CENTER_KEY,
      PROGRAM_KEY,
      ACCOUNT_KEY,
      ACCOUNT_HIST_KEY,
      AMOUNT,
      DEPRECIATION_AMOUNT,
      SNAPSHOT_DATE_KEY,
      SESSION_ID,
      ROW_CREATE_DTTM,
      ROW_LAST_MOD_DTTM,
      ROW_LAST_MOD_PROC_NM,
      ROW_LAST_MOD_PROC_SEQ_NBR
    )
    SELECT /*+ parallel(x,16) full(x) use_hash(d, x) */
      GL_ACCT_DAILY_SNAPSHOT_KEY,
      REVENUE_DATE_KEY,
      BUSINESS_SECTOR_KEY,
      BUSINESS_SEGMENT_KEY,
      FR_CATEGORY_KEY,
      COST_CENTER_KEY,
      PROGRAM_KEY,
      ACCOUNT_KEY,
      ACCOUNT_HIST_KEY,
      AMOUNT,
      DEPRECIATION_AMOUNT,
      SNAPSHOT_DATE_KEY,
      ''' || v_ses_id || ''',
      sysdate,
      sysdate,
      ''' || v_proc || ''',
      1
    FROM ' || v_src || ' x
    JOIN (
      SELECT DATE_KEY FROM d_date
      WHERE month_year_abbr = ''' || p_ym || '''
    ) d ON (x.REVENUE_DATE_KEY = d.DATE_KEY)
    WHERE SNAPSHOT_DATE_KEY = ' || p_bdate
    ;
    log4me.debug(v_proc || ' insert new data for ' || v_part, v_sql, v_ses_id);
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
  END;

BEGIN
  --
  -- get date range from seed table
  --
  v_sql := '
    SELECT /*+ parallel */ UNIQUE to_char(snapshot_date_key)
    FROM ' || v_src || ' ORDER BY 1
  ';
  log4me.debug(v_ses_type, v_sql, v_ses_id);
  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_blist;

  IF v_blist IS NULL OR v_blist.COUNT = 0 THEN
    log4me.info('DONE ' || v_ses_type || ' no data found', v_ses_id);
    RETURN;  
  END IF;
  --
  -- walk through every snapshot_date 
  --
  v_bdate := concat_rpad(v_blist);
  v_proc  := v_ses_type || '.' || v_bdate;
  log4me.info('INIT ' || v_proc, v_ses_id);

  FOR i IN 1..v_blist.COUNT LOOP
    --
    -- for a given business_date:
    --   get the list of months involved based on accounting date
    --
    v_sql := '
      SELECT /*+ parallel(x,16) full(x) use_hash(d,x) */
        UNIQUE d.month_year_abbr
      FROM ' || v_src || ' x
      JOIN d_date d ON (x.REVENUE_DATE_KEY = d.DATE_KEY)
      WHERE snapshot_date_key = ' || v_blist(i) || '
        AND revenue_date_key != 0
      ORDER BY 1
    ';
    log4me.debug(v_ses_type, v_sql, v_ses_id);
    EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_ymlist;
    --
    -- walk through one partition (on revenue_date) at a time
    --
    FOR j IN 1..v_ymlist.COUNT LOOP
      process_one_partition(v_blist(i), v_ymlist(j));
    END LOOP;
  END LOOP;

  log4me.info('DONE ' || v_proc, v_ses_id);
  --
  -- to save some ETL work, we combine these into one call
  --
  f_gl_acct_mon_snapshot_load;
  f_gl_acct_mon_by_category_load;

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('FAIL ' || v_proc, v_ses_id);
    RAISE; 
END F_GL_ACCT_DAILY_SNAPSHOT_LOAD;
/
show errors

GRANT EXECUTE ON F_GL_ACCT_DAILY_SNAPSHOT_LOAD TO DWLOADER;
