--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- EDM_OWNER SINCE_DATE historical data prep
--
-- create the historical set of SINCE_DATE data
--
begin
  src_acct_since_date_conv;
end;
/
show errors
