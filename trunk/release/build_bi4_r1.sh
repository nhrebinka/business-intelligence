#!/usr/bin/sh
#
# BI4.0 - Release Package Builder script R1.0
#
# 20140512: CC - first version
# 20140520: CC - combine STG, HUB, EDW into one
# 20140611: CC - add function infa(); externalize infa housekeeping fields
#
#-------------------------------------------------------------------------
#
# environment variables
#

NM=bi4                       # package name
VN=r1                        # version name
SD=~/wex/bi4                 # bi4 related source directory
DD=$1                        # export directory
RN=${NM}_${VN}               # full release name
XO=~/wex/xo                  # business/operation object
NOTE=${SD}/release_note.sql  # BI4.0 release note header
#
# check input parameters
#
if [ "$DD" == "" ]; then
  echo "Usage:> build_${NM}_${VN}.sh working_dir"
  exit 1
fi

if [ -d $DD ]; then
  echo "directory $DD already exist!"
  exit 1
else
  mkdir $DD
fi
#
# To facilitate Informatica housekeeping field post-modeling DDL export processing
#
function infa() {
    src=$1
    tmp=$2
    dst=$3
    for n in `grep 'CREATE TABLE' $SD/$src | cut -d' ' -f3`; do
        cat $SD/$tmp | sed -e "s/#{N}/${n}/g" >> $dst
    done
}

echo "start building $NM release package..."
#
# create packages directories
#
SRC=$DD/0_src
HUB=$DD/1_hub
STG=$DD/2_stg
EDW=$DD/3_edw

for d in $SRC $HUB $STG $EDW; do
  mkdir $d   
done

#-------------------------------------------------------------------------
# build for STAGE_OWNER
#-------------------------------------------------------------------------
SA=src

O_INIT="$SRC/${RN}a_init.sql"

S_DRP=${SA}_drp.ddl
S_TBL=${SA}_tbl.ddl
S_GNT=${SA}_gnt.ddl
S_SYN=${SA}_syn.ddl

cp  $SD/${SA}_ps_tbl.sql       $SRC/$S_TBL
cat $SD/${SA}_excel_tbl.sql >> $SRC/$S_TBL
cat $SD/${SA}_tbl.sql       >> $SRC/$S_TBL

cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- BI40 HUB.STAGE_OWNER one time setup
-- Also, ensure DBA copy the PS_* tables from PRPS88
@$S_DRP
@$S_TBL
@$S_GNT
EOF

echo "-- drop  all tables and the constraints"     > $SRC/$S_DRP
echo "-- grant all tables and the constraints"     > $SRC/$S_GNT
echo "-- create STAGE_OWNER synonym for DWLOADER"  > $SRC/$S_SYN
for n in `grep 'CREATE TABLE' $SRC/$S_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"     >> $SRC/$S_DRP
  cat $SD/src_grant.sql | sed -e "s/#{N}/${n}/g"  >> $SRC/$S_GNT
  echo "CREATE OR REPLACE SYNONYM ${n} FOR STAGE_OWNER.${n};" >> $SRC/$S_SYN
done

echo "  $SRC> DDLs created."

#-------------------------------------------------------------------------
# build for EDM_OWNER
#-------------------------------------------------------------------------
#
# these are the scripts that needs to be run in sequence 
#
SA=hub

O_INIT="$HUB/${RN}a_init.sql"
O_LOAD="$HUB/${RN}b_load.sql"

H_DRP=${SA}_drp.ddl
H_TBL=${SA}_tbl.ddl
H_KEY=${SA}_key.ddl
H_SEQ=${SA}_seq.ddl
H_GNT=${SA}_gnt.ddl
H_SYN=${SA}_syn.ddl
#
# build store procedures packages
#
mv  $SRC/$S_SYN           $HUB/$S_SYN
cp  $SD/m_tbl.sql         $HUB/$H_TBL
cat $SD/m_xtbl.sql    >>  $HUB/$H_TBL
cat $SD/t_tbl.sql     >>  $HUB/$H_TBL
cat $SD/d_tbl.sql     >>  $HUB/$H_TBL
cp  $SD/m_key.sql         $HUB/$H_KEY
cat $SD/d_key.sql     >>  $HUB/$H_KEY
#
# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- BI40 HUB.EDM_OWNER one time setup
@$H_DRP
@$H_TBL
@$H_KEY
@$H_SEQ
@$H_GNT
EOF

cat $NOTE > $O_LOAD
echo "-- create synonyms for DWLOADER" >> $O_LOAD
echo "@$S_SYN"                         >> $O_LOAD
echo "@$H_SYN"                         >> $O_LOAD
#
# build drop and grant scripts
#
echo "-- drop all tables and the constraints" > $HUB/$H_DRP
echo "-- grant access to tables "             > $HUB/$H_GNT

echo "-- add Informatica housekeeping fields" >> $HUB/$H_TBL
infa m_tbl.sql  m_ifa.sql  "$HUB/$H_TBL"
infa m_xtbl.sql m_xifa.sql "$HUB/$H_TBL"
infa t_tbl.sql  t_ifa.sql  "$HUB/$H_TBL"
infa d_tbl.sql  d_ifa.sql  "$HUB/$H_TBL"
#
# add initialization/hand-coded data 
#
cat  $SD/m_val.sql  >> $HUB/$H_TBL
cat  $SD/d_val0.sql >> $HUB/$H_TBL 

echo "-- create sequences for dimension tables " > $HUB/$H_SEQ
# SEQ for M_* tables
for n in `grep 'CREATE TABLE' $SD/m_tbl.sql | cut -d' ' -f3 | sed -e "s/M_//"`; do
  cat $SD/m_seq.sql | sed -e "s/#{N}/${n}/g" >> $HUB/$H_SEQ
done
# SEQ for D_* tables
for n in `grep 'CREATE TABLE' $SD/d_tbl.sql | cut -d' ' -f3 | sed -e "s/D_//"`; do
  cat $SD/d_seq.sql | sed -e "s/#{N}/${n}/g" >> $HUB/$H_SEQ
done
#
# add the DAILY_GL on HUB side (awkward but part of life)
#
for n in DAILY_GL_ACCT_SNAPSHOT SEGMENT_ACCT_EVENT MON_GL_ACCT_SNAPSHOT; do
  cat $SD/f_seq.sql | sed -e "s/#{N}/${n}/g" >> $HUB/$H_SEQ
done

echo "-- add EDM_OWNER synonym for DWLOADER"  > $HUB/$H_SYN
for n in M_SPNR_PGM M_SPNR_PGM_XREF D_PROGRAM; do
  echo "CREATE TABLE ${n}_201410_BAK TABLESPACE D_BI AS SELECT * FROM ${n};" >> $HUB/$H_DRP
  echo "GRANT SELECT ON ${n}_201410_BAK TO EDM_OWNER_SELECT;" >> $HUB/$H_DRP  
done
for n in `grep 'CREATE TABLE' $HUB/$H_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"     >> $HUB/$H_DRP
  cat $SD/hub_grant.sql | sed -e "s/#{N}/${n}/g"  >> $HUB/$H_GNT
  echo "CREATE OR REPLACE SYNONYM ${n} FOR EDM_OWNER.${n};"  >> $HUB/$H_SYN
done
for n in `grep 'CREATE SEQUENCE' $HUB/$H_SEQ | cut -d' ' -f3`; do
  echo "CREATE OR REPLACE SYNONYM ${n} FOR EDM_OWNER.${n};"  >> $HUB/$H_SYN
done

echo "  $HUB> DDLs created."

#-------------------------------------------------------------------------
# build for EDW_STAGE_OWNER
#-------------------------------------------------------------------------
SA=stg

O_INIT="$STG/${RN}a_init.sql"

X_DRP=${SA}_drp.ddl
X_TBL=${SA}_tbl.ddl
X_GNT=${SA}_gnt.ddl

cp  $SD/x_tbl.sql          $STG/$X_TBL
cat $SD/src_tbl.sql     >> $STG/$X_TBL

# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- EDW.EDW_STAGE_OWNER one time setup
@$X_DRP
@$X_TBL
@$X_GNT
EOF
#
# build workspace creation script
#
echo "-- drop all tables and the constraints" > $STG/$X_DRP
echo "-- grant access to tables "             > $STG/$X_GNT
for n in `grep 'CREATE TABLE' $STG/$X_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"  >> $STG/$X_DRP
  cat $SD/x_gnt.sql | sed -e "s/#{N}/${n}/g"   >> $STG/$X_GNT
done
#
echo "  $STG> DDLs created."

#-------------------------------------------------------------------------
# build EDW_OWNER
#-------------------------------------------------------------------------
SA=edw

O_INIT="$EDW/${RN}a_init.sql"
O_ACT="$EDW/${RN}b_act.sql"
O_PRC="$EDW/${RN}c_prc.sql"

W_DRP=${SA}_drp.ddl
W_TBL=${SA}_tbl.ddl
W_KEY=${SA}_key.ddl
W_GNT=${SA}_gnt.ddl
W_SEQ=${SA}_seq.ddl
W_SYN=${SA}_syn.ddl

cp  $SD/d_tbl.sql          $EDW/$W_TBL
cat $SD/f_tbl.sql       >> $EDW/$W_TBL
cp  $SD/d_key.sql          $EDW/$W_KEY

# build package and table creation scripts
#
cat $NOTE >  $O_INIT
cat <<EOF >> $O_INIT
-- EDW.EDW_OWNER one time setup
@$W_DRP
@$W_TBL
@$W_KEY
@$W_SEQ
@$W_GNT
EOF

cat $NOTE > $O_ACT
echo "-- EDW_OWNER.SYNONYMS for DWLOADER, BORPTRNR, and USER_LDAP" >> $O_ACT
echo "@${W_SYN}" >> $O_ACT

echo "-- add Informatica housekeeping fields" >> $EDW/$W_TBL
infa d_tbl.sql d_ifa.sql "$EDW/$W_TBL"
infa f_tbl.sql f_ifa.sql "$EDW/$W_TBL"
#
# add initialization/hand-coded data 
#
cat  $SD/d_val.sql >> $EDW/$W_TBL
#
# build workspace creation script
#
echo "-- drop all tables and the constraints" > $EDW/$W_DRP
echo "-- create sequence for fact tables"     > $EDW/$W_SEQ
echo "-- grant access to tables "             > $EDW/$W_GNT
echo "-- synonym for user access "            > $EDW/$W_SYN
for n in `grep 'CREATE TABLE' $EDW/$W_TBL | cut -d' ' -f3`; do
  echo "DROP TABLE ${n} CASCADE CONSTRAINTS;"    >> $EDW/$W_DRP
  cat $SD/edw_grant.sql | sed -e "s/#{N}/${n}/g" >> $EDW/$W_GNT
  echo "CREATE OR REPLACE PUBLIC SYNONYM ${n} FOR EDW_OWNER.${n};" >> $EDW/$W_SYN
done
for n in SEGMENT_ACCT_EVENT; do
  cat $SD/f_seq.sql | sed -e "s/#{N}/${n}/g" >> $EDW/$W_SEQ
done
echo "  $EDW> DDLs created."
#
# build store procedures packages
#
cat $NOTE > $O_PRC
echo "-- EDW_OWNER business/operation objects" >> $O_PRC
# V4.0.2=>
#for n in log4me xos_tokenizer xos_session_mgr xos_workspace_mgr xbs_site_agg xbs_revenue_agg; do
#<=
for n in log4me xos_tokenizer xos_session_mgr xos_workspace_mgr xbs_site_agg; do
  cp $XO/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
#
# add new existing procedures with MANUAL_REBATE_AMOUNT added
# V4.0.2=>
#for n in f_transaction_line_item_load f_gl_acct_daily_snapshot_load f_gl_acct_mon_snapshot_load f_gl_acct_mon_by_category_load f_revenue_aggregates; do
#<=
for n in f_transaction_line_item_load f_gl_acct_daily_snapshot_load f_gl_acct_mon_snapshot_load f_gl_acct_mon_by_category_load; do
  cp $SD/${n}.spp   $EDW/${n}.prc
  echo "@${n}.prc"  >> $O_PRC
done
#
# drop store proc that is no more needed
#
for n in f_revenue_aggregates_fix f_revenue_aggregates_h f_revenue_aggregates_t; do
  echo "DROP PROCEDURE ${n};"  >> $O_PRC
done
echo "  $EDW> Store Procedures created."
#
#-------------------------------------------------------------------------
# Done!
#-------------------------------------------------------------------------
echo "done building $NM release package"
