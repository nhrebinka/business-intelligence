CREATE OR REPLACE PROCEDURE XOS_ACCOUNT_AFFINITY_FIX (
  p_proc    varchar2,
  p_acct    number,
  p_pgm_in  number,
  p_pgm_out number,
  p_part    varchar2 := 'ym_' || to_char(sysdate, 'YYYY_MM')
)
AS
  v_part constant varchar2(40) := 'partition (' || p_part || ')';
  v_tbl  constant TokList      := TokList(
    'f_daily_aging_snapshot   ' || v_part,
    'f_daily_revenue_snapshot ' || v_part,
    'f_daily_account_snapshot ' || v_part,
    'f_monthly_account_snapshot',
    'f_accounts_receivable_entry',
    'f_purchase_device_event',
    'f_account_naics_sic_event'
  );
  v_cond constant varchar2(400) := '
    set program_key          = ' || p_pgm_out || ',
        row_last_mod_dttm    = sysdate,
        row_last_mod_proc_nm = ''' || p_proc || '''
    where account_key = ' || p_acct   || ' 
      and program_key = ' || p_pgm_in || '
  ';  
  v_sql varchar2(400);
 
BEGIN
  log4me.info('INIT ' || p_proc);
  --
  -- data fix for affinity program, account association
  --
  v_sql := '
    update f_transaction_line_item ' || v_part || '
    set program_key        = ' || p_pgm_out || ',
      row_last_mod_dttm    = sysdate,
      row_last_mod_proc_nm = ''' || p_proc || '''
    where program_key = ' || p_pgm_in || ' and (
      purchase_account_key = ' || p_acct || ' or
      billing_account_key  = ' || p_acct || '
    )
  ';
  log4me.debug(p_proc, v_sql);
  EXECUTE IMMEDIATE v_sql;
  
  FOR i IN v_tbl.FIRST..v_tbl.LAST LOOP
    v_sql := 'update ' || v_tbl(i) || v_cond;
    log4me.debug(p_proc, v_sql);
    EXECUTE IMMEDIATE 'update ' || v_tbl(i) || v_cond;
  END LOOP;

  COMMIT;
  log4me.info('DONE ' || p_proc);

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('FAIL ' || p_proc);
    RAISE;
END XOS_ACCOUNT_AFFINITY_FIX;
/
show errors

