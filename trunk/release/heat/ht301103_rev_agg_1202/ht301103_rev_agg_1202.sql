--
-- Revenue Agg 12/02 Rerun - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht301103_rev_agg_1202.log;

alter session enable parallel dml;

begin
  f_revenue_aggregates(
    to_date('20141202', 'YYYYMMDD'),    
    to_date('20141202', 'YYYYMMDD')
  );
end;
/

SPOOL OFF

