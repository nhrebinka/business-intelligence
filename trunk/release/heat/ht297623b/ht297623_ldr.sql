--
-- BI4.0 patch - script to create synonym for T_LEDGER_TEMP
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht255358_297623b.log;

CREATE SYNONYM t_ledger_temp FOR edm_owner.t_ledger_temp;

SPOOL OFF


