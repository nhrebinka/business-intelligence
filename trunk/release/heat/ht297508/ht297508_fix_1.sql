--
-- P1 HT297508 migration script
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht297508_fix.log;
--
-- data fix for affinity program, account association
--
update f_purchase_device_event 
set program_key = 637,
    row_last_mod_dttm = sysdate,
    row_last_mod_proc_nm = 'One-Shot Affinity53 Program Fix'
where account_key = 10868529 and program_key = 701;

update f_account_naics_sic_event
set program_key = 637,
    row_last_mod_dttm = sysdate,
    row_last_mod_proc_nm = 'One-Shot Affinity53 Program Fix'
where account_key = 10868529 and program_key = 701;

update f_daily_account_snapshot partition (ym_2014_10)
set program_key = 637,
    row_last_mod_dttm = sysdate,
    row_last_mod_proc_nm = 'One-Shot Affinity53 Program Fix'
where account_key = 10868529 and program_key = 701;

update f_monthly_account_snapshot
set program_key = 637,
    row_last_mod_dttm = sysdate,
    row_last_mod_proc_nm = 'One-Shot Affinity53 Program Fix'
where account_key = 10868529 and program_key = 701;

commit;

SPOOL OFF


