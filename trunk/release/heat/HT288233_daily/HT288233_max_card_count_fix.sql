--
-- Account Max Outstanding Card Count fix - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht288233_max_card_count.log;

drop procedure f_daily_account_aggregates_f;
drop procedure f_daily_account_aggregates_t;

@f_daily_account_aggregates.prc
@f_daily_account_aggregates_h.prc
@f_daily_account_aggs_h_dfix.prc

alter table F_ACCOUNT_NAICS_SIC_EVENT parallel 16;

exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140701', 'yyyymmdd'), to_date('20140731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140601', 'yyyymmdd'), to_date('20140630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140501', 'yyyymmdd'), to_date('20140531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140401', 'yyyymmdd'), to_date('20140430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140301', 'yyyymmdd'), to_date('20140331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140201', 'yyyymmdd'), to_date('20140228', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20140101', 'yyyymmdd'), to_date('20140131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20131201', 'yyyymmdd'), to_date('20131231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20131101', 'yyyymmdd'), to_date('20131130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20131001', 'yyyymmdd'), to_date('20131031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130901', 'yyyymmdd'), to_date('20130930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130801', 'yyyymmdd'), to_date('20130831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130701', 'yyyymmdd'), to_date('20130731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130601', 'yyyymmdd'), to_date('20130630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130501', 'yyyymmdd'), to_date('20130531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130401', 'yyyymmdd'), to_date('20130430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130301', 'yyyymmdd'), to_date('20130331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130201', 'yyyymmdd'), to_date('20130228', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20130101', 'yyyymmdd'), to_date('20130131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20121201', 'yyyymmdd'), to_date('20121231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20121101', 'yyyymmdd'), to_date('20121130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20121001', 'yyyymmdd'), to_date('20121031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120901', 'yyyymmdd'), to_date('20120930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120801', 'yyyymmdd'), to_date('20120831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120701', 'yyyymmdd'), to_date('20120731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120601', 'yyyymmdd'), to_date('20120630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120501', 'yyyymmdd'), to_date('20120531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120401', 'yyyymmdd'), to_date('20120430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120301', 'yyyymmdd'), to_date('20120331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120201', 'yyyymmdd'), to_date('20120229', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20120101', 'yyyymmdd'), to_date('20120131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20111201', 'yyyymmdd'), to_date('20111231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20111101', 'yyyymmdd'), to_date('20111130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20111001', 'yyyymmdd'), to_date('20111031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110901', 'yyyymmdd'), to_date('20110930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110801', 'yyyymmdd'), to_date('20110831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110701', 'yyyymmdd'), to_date('20110731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110601', 'yyyymmdd'), to_date('20110630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110501', 'yyyymmdd'), to_date('20110531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110401', 'yyyymmdd'), to_date('20110430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110301', 'yyyymmdd'), to_date('20110331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110201', 'yyyymmdd'), to_date('20110228', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20110101', 'yyyymmdd'), to_date('20110131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20101201', 'yyyymmdd'), to_date('20101231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20101101', 'yyyymmdd'), to_date('20101130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20101001', 'yyyymmdd'), to_date('20101031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100901', 'yyyymmdd'), to_date('20100930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100801', 'yyyymmdd'), to_date('20100831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100701', 'yyyymmdd'), to_date('20100731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100601', 'yyyymmdd'), to_date('20100630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100501', 'yyyymmdd'), to_date('20100531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100401', 'yyyymmdd'), to_date('20100430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100301', 'yyyymmdd'), to_date('20100331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100201', 'yyyymmdd'), to_date('20100228', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20100101', 'yyyymmdd'), to_date('20100131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20091201', 'yyyymmdd'), to_date('20091231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20091101', 'yyyymmdd'), to_date('20091130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20091001', 'yyyymmdd'), to_date('20091031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090901', 'yyyymmdd'), to_date('20090930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090801', 'yyyymmdd'), to_date('20090831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090701', 'yyyymmdd'), to_date('20090731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090601', 'yyyymmdd'), to_date('20090630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090501', 'yyyymmdd'), to_date('20090531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090401', 'yyyymmdd'), to_date('20090430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090301', 'yyyymmdd'), to_date('20090331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090201', 'yyyymmdd'), to_date('20090228', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20090101', 'yyyymmdd'), to_date('20090131', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20081201', 'yyyymmdd'), to_date('20081231', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20081101', 'yyyymmdd'), to_date('20081130', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20081001', 'yyyymmdd'), to_date('20081031', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080901', 'yyyymmdd'), to_date('20080930', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080801', 'yyyymmdd'), to_date('20080831', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080701', 'yyyymmdd'), to_date('20080731', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080601', 'yyyymmdd'), to_date('20080630', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080501', 'yyyymmdd'), to_date('20080531', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080401', 'yyyymmdd'), to_date('20080430', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080301', 'yyyymmdd'), to_date('20080331', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080201', 'yyyymmdd'), to_date('20080229', 'yyyymmdd'));
exec F_DAILY_ACCOUNT_AGGS_H_DFIX(to_date('20080101', 'yyyymmdd'), to_date('20080131', 'yyyymmdd'));

SPOOL OFF

