CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_ACCOUNT_AGGS_H_DFIX (
  p_calendar_dt_start   IN DATE := TRUNC (SYSDATE),
  p_calendar_dt_end     IN DATE := TRUNC (SYSDATE)
)
AS
/*
  NAME      : F_DAILY_ACCOUNT_AGGREGATES
  DECRIPTION: load daily Account Aggregates into F_DAILY_ACCOUNT_AGGREGATES
  REVISION  :
  1.0 20100615 by Izzy Reinish - Init version
  2.0 20101011 by Venugopal Kancharla
      Added the F_SEED_DATES procedure to autoamte the history  laoding
      and added the  indexes Drop and Recreate
  2.1 20101212 by Venu KAncharla
      Adding the Filter in the where Clause
  2.2 20110117 by VK
      DW_Current_FLG = '1', commented since this is not needed
  2.3 20110331 by VK
      Added the new Seed Dates to the PROD D_SEED_DATES    
  2.4 201100524 by VK
      Elimate the Card counts for Future Dates for re-issues durinng historic  Data processing
  2.5 20130725 by VK
      Code to Calculate MAX_NUMBER_OF_OUTRSTANDING CARDS . .(Fleet Size ) 
      This is being added to imporve performance of BO queries
  2.6 20130906 by VK
      Changes for the  Fleet Size Logic based on the Business Inputs
      Adding the Account_hist_key
  2.7 20130926 by VK
      Added Moved and Converted Filters apart from ther existing Terminated Filters
      To move these Card status from the outstanding Card Count
  2.8 20140515 by VK
      Added extra routine to max count to account for any account where aCCOUNT_KEY <> aCCOUNT_HIST_KEY
  2.8.1 20140709 Lee - reformat for readability
*/
  v_calendar_dt_start DATE;
  v_calendar_dt_end   DATE;

  v_first_date_key    INTEGER ;
  v_part              VARCHAR2(10);
  v_sql               VARCHAR2(4000);


BEGIN
  -- Seed Procedure and Indexes management  Added By venu on 11/09/2010
  --  D_SEED_DATES (v_calendar_dt_start, v_calendar_dt_end);

  v_calendar_dt_start := p_calendar_dt_start;
  v_calendar_dt_end   := p_calendar_dt_end;

  DBMS_OUTPUT.PUT_LINE(v_calendar_dt_start);
  DBMS_OUTPUT.PUT_LINE(v_calendar_dt_end);

  FOR rec IN (
    SELECT 
      d.CALENDAR_DATE_DT, 
      d.DATE_KEY, 
      d.FIRST_DAY_IN_MONTH_DATE_DT, 
      d.LAST_DAY_IN_MONTH_DATE_DT
    FROM D_DATE d
    WHERE d.CALENDAR_DATE_DT BETWEEN v_calendar_dt_start AND v_calendar_dt_end
  )
  LOOP
    --  Added By VK on 07/25/2013 For MAX Number Of outstnding CARD COUNTS
    v_part := 'YM_' || TO_CHAR (rec.LAST_DAY_IN_MONTH_DATE_DT, 'YYYY_MM');

    -- Code to get the First Day of the month
    SELECT d.DATE_KEY
    INTO   v_first_date_key
    FROM   D_DATE d
    WHERE  d.CALENDAR_DATE_DT = rec.FIRST_DAY_IN_MONTH_DATE_DT;

    -- Actual Code to find the MAx Number of outstanding CARDCOUNT,
    -- From the starting of the month until that day ....
    -- Note Max number outstnading CArd Count is updated where 
    -- Account_key = Account_hist_key

    v_sql := '
      MERGE INTO /*+ parallel */ 
        EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT PARTITION (' || v_part || ') FDAS
      USING (
        SELECT F1.ACCOUNT_HIST_KEY ACCOUNT_HIST_KEY,
          MIN(F2.MIN_ACCOUNT_KEY)         MIN_ACCOUNT_KEY, 
          MAX(MAX_OUTSTANDING_CARD_COUNT) MAX_OUTSTANDING_CARD_COUNT
        FROM (
          SELECT 
            F.ACCOUNT_HIST_KEY             ACCOUNT_HIST_KEY,
            F.DATE_KEY                     DATE_KEY,
            SUM(F.OUTSTANDING_CARDS_COUNT) MAX_OUTSTANDING_CARD_COUNT
          FROM EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT PARTITION ('|| v_part ||') F
          WHERE F.DATE_KEY BETWEEN ' || v_first_date_key || ' AND ' || rec.DATE_KEY || '
          GROUP BY F.ACCOUNT_HIST_KEY, F.DATE_KEY
        ) F1, (
          SELECT   
            MIN(ACCOUNT_KEY) MIN_ACCOUNT_KEY,
            ACCOUNT_HIST_KEY   
          FROM EDW_OWNER.F_DAILY_ACCOUNT_SNAPSHOT PARTITION ('|| v_part ||') F
          WHERE F.DATE_KEY = ' || rec.DATE_KEY || '
          GROUP BY F.ACCOUNT_HIST_KEY, F.DATE_KEY
        ) F2
        WHERE F2.ACCOUNT_HIST_KEY = F1.ACCOUNT_HIST_KEY         
        GROUP BY F1.ACCOUNT_HIST_KEY
      ) A ON (FDAS.ACCOUNT_KEY = A.MIN_ACCOUNT_KEY)
      WHEN MATCHED THEN
        UPDATE SET
          FDAS.MAX_OUTSTANDING_CARDS_COUNT = A.MAX_OUTSTANDING_CARD_COUNT
        WHERE FDAS.DATE_KEY = '|| rec.DATE_KEY
    ;
    DBMS_OUTPUT.PUT_LINE('V_SQL ' || v_sql);

    EXECUTE IMMEDIATE v_sql;
    COMMIT;

  END LOOP;
END F_DAILY_ACCOUNT_AGGS_H_DFIX;
/

GRANT EXECUTE ON F_DAILY_ACCOUNT_AGGS_H_DFIX TO DWLOADER;
