alter session set plsql_ccflags = 'ADD_COLUMN:true';

--
-- SDLC 15.1 - DDL for EDW Fact tables
--
-- ------------------------------------------------------------------------
set serveroutput on
set pagesize 400
set linesize 2000

declare
  x_prc varchar2(80) := 'SDLC 15.1 DDL ';
  x_lst TokList := TokList(
$IF $$ADD_COLUMN $THEN
    'F_TRANSACTION_LINE_ITEM'
$ELSE
    'F_DAILY_REVENUE_SNAPSHOT',
    'F_MONTHLY_REVENUE_SNAPSHOT',
    'F_DAILY_TOTALS_SNAPSHOT',
    'X_D_SITE_B',
    'X_D_SITE_A01',
    'X_D_SITE_A02',
    'X_D_SITE_A03',
    'X_D_SITE_A04',
    'X_D_SITE_A05',
    'X_D_SITE_A06'
$END
  );
$IF $$ADD_COLUMN $THEN
  x_col varchar2(800) := '(
    RETAIL_PPU_AMOUNT         NUMBER(15,5) DEFAULT 0 NOT NULL,
    ADJUSTED_PPU_AMOUNT       NUMBER(15,5) DEFAULT 0 NOT NULL,
    BETTER_PRICE_PPU_AMOUNT   NUMBER(15,5) DEFAULT 0 NOT NULL
  )';
$ELSE
  x_col varchar2(800) := '(
    RETAIL_PPU_AMOUNT, ADJUSTED_PPU_AMOUNT, BETTER_PRICE_PPU_AMOUNT
  )';
$END
  v_tb  varchar2(120);
  v_sql varchar2(2000);

begin
  log4me.info(x_prc || ' INIT');
  for i in 1..x_lst.count loop
    v_tb  := 'ALTER TABLE ' || x_lst(i);
$IF $$ADD_COLUMN $THEN
    v_sql := v_tb || ' ADD ' || 
$ELSE
    v_sql := v_tb || ' SET UNUSED ' || 
$END
      CASE WHEN instr(v_tb, 'X_D')>0 
      THEN replace(x_col,'AMOUNT', 'AMT')
      ELSE x_col
      END;
 
    begin
      execute immediate v_sql;
      log4me.debug(x_prc || ' => ' || v_tb, v_sql);
    exception when others then
      log4me.debug(x_prc || ' => ' || v_tb || ' failed', v_sql);
      log4me.err(x_prc || ' => ' || v_tb || ' FAILED');
    end;
  end loop;
  log4me.info(x_prc || ' DONE');
end;
/
show errors
