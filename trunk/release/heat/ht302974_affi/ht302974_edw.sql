--
-- Account Affinity Program Check Store Procedure
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht302974_affi.log;

@f_daily_affinity_program_check.prc

SPOOL OFF

