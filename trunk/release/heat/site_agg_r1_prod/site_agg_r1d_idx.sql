--
-- nonprefixed partitioned index
--
CREATE INDEX x_d_site_b_ui1 
ON x_d_site_b (
  POS_AND_SITE_KEY
)
TABLESPACE I_BI
PCTFREE    0
INITRANS   2
MAXTRANS   255
STORAGE    (
  BUFFER_POOL      DEFAULT
  FLASH_CACHE      DEFAULT
  CELL_FLASH_CACHE DEFAULT
)
LOCAL
PARALLEL 12
NOLOGGING
COMPRESS
UNUSABLE  -- create only, rebuild later
;
--
-- to rebuild
-- ALTER INDEX x_d_site_b_ui1 REBUILD PARTITION YM_2013_01 PARALLEL 12;
--
alter index x_d_site_b_ui1 rebuild partition ym_2014_01;
alter index x_d_site_b_ui1 rebuild partition ym_2014_02;
alter index x_d_site_b_ui1 rebuild partition ym_2014_03;
alter index x_d_site_b_ui1 rebuild partition ym_2014_04;
alter index x_d_site_b_ui1 rebuild partition ym_2013_12;
alter index x_d_site_b_ui1 rebuild partition ym_2013_11;
alter index x_d_site_b_ui1 rebuild partition ym_2013_10;
alter index x_d_site_b_ui1 rebuild partition ym_2013_09;
alter index x_d_site_b_ui1 rebuild partition ym_2013_08;
alter index x_d_site_b_ui1 rebuild partition ym_2013_07;
alter index x_d_site_b_ui1 rebuild partition ym_2013_06;
alter index x_d_site_b_ui1 rebuild partition ym_2013_05;
alter index x_d_site_b_ui1 rebuild partition ym_2013_04;
alter index x_d_site_b_ui1 rebuild partition ym_2013_03;
alter index x_d_site_b_ui1 rebuild partition ym_2013_02;
alter index x_d_site_b_ui1 rebuild partition ym_2013_01;
alter index x_d_site_b_ui1 rebuild partition ym_2012_12;
alter index x_d_site_b_ui1 rebuild partition ym_2012_11;
alter index x_d_site_b_ui1 rebuild partition ym_2012_10;
alter index x_d_site_b_ui1 rebuild partition ym_2012_09;
alter index x_d_site_b_ui1 rebuild partition ym_2012_08;
alter index x_d_site_b_ui1 rebuild partition ym_2012_07;
alter index x_d_site_b_ui1 rebuild partition ym_2012_06;
alter index x_d_site_b_ui1 rebuild partition ym_2012_05;
alter index x_d_site_b_ui1 rebuild partition ym_2012_04;
alter index x_d_site_b_ui1 rebuild partition ym_2012_03;
alter index x_d_site_b_ui1 rebuild partition ym_2012_02;
alter index x_d_site_b_ui1 rebuild partition ym_2012_01;
alter index x_d_site_b_ui1 rebuild partition ym_2011_12;
alter index x_d_site_b_ui1 rebuild partition ym_2011_11;
alter index x_d_site_b_ui1 rebuild partition ym_2011_10;
alter index x_d_site_b_ui1 rebuild partition ym_2011_09;
alter index x_d_site_b_ui1 rebuild partition ym_2011_08;
alter index x_d_site_b_ui1 rebuild partition ym_2011_07;
alter index x_d_site_b_ui1 rebuild partition ym_2011_06;
alter index x_d_site_b_ui1 rebuild partition ym_2011_05;
alter index x_d_site_b_ui1 rebuild partition ym_2011_04;
alter index x_d_site_b_ui1 rebuild partition ym_2011_03;
alter index x_d_site_b_ui1 rebuild partition ym_2011_02;
alter index x_d_site_b_ui1 rebuild partition ym_2011_01;
alter index x_d_site_b_ui1 rebuild partition ym_2010_12;
alter index x_d_site_b_ui1 rebuild partition ym_2010_11;
alter index x_d_site_b_ui1 rebuild partition ym_2010_10;
alter index x_d_site_b_ui1 rebuild partition ym_2010_09;
alter index x_d_site_b_ui1 rebuild partition ym_2010_08;
alter index x_d_site_b_ui1 rebuild partition ym_2010_07;
alter index x_d_site_b_ui1 rebuild partition ym_2010_06;
alter index x_d_site_b_ui1 rebuild partition ym_2010_05;
alter index x_d_site_b_ui1 rebuild partition ym_2010_04;
alter index x_d_site_b_ui1 rebuild partition ym_2010_03;
alter index x_d_site_b_ui1 rebuild partition ym_2010_02;
alter index x_d_site_b_ui1 rebuild partition ym_2010_01;
