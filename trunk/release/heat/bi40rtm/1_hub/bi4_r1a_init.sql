--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- BI40 HUB.EDM_OWNER one time setup
@hub_drp.ddl
@hub_tbl.ddl
@hub_key.ddl
@hub_seq.ddl
@hub_gnt.ddl
