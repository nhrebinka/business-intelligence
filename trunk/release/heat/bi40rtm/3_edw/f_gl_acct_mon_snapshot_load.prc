CREATE OR REPLACE PROCEDURE EDW_OWNER.F_GL_ACCT_MON_SNAPSHOT_LOAD
AS
/*
  ============================================================================
  Name : F_GL_ACCT_MONTHLY_SNAPSHOT
  Spec.: Store procedure for populating monthly GL_ACCT_MONTHLY snapshot by
         copying from EDW_STAGE_OWNER to EDW_OWNER. The process support re-run
         by truncating existing monthly data before repopulating them.

  Dependency:
    log4me package
    xos_tokenizer.concat_rpad

  Desc.:
    1. get date range from seed table (EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT)
    2. delete existing data from all partitions that will be touched
       2.a remove only 4 "repeating" categories for previous month
       2.b truncate every
    3. copy from stage into target table 

  Revision History:
  1.0 20140825 CC - first version (modeled from f_gl_acct_daily_snapshot_load
  ============================================================================
*/
  FMT_DT     constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';
  v_ses_id   constant varchar2(80) := to_char(systimestamp, FMT_DT);
  v_ses_type constant varchar2(80) := 'F_GL_ACCT_MONTHLY';
  v_src      constant varchar2(80) := 'EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  v_tgt      constant varchar2(80) := 'F_GL_ACCT_MONTHLY_SNAPSHOT';
  --
  -- SQLs
  --
  v_gl_cat   constant CLOB := '
    SELECT FR_CATEGORY_KEY FROM D_FR_CATEGORY
    WHERE  FR_CATEGORY_ID IN (
      ''Cost of IT'', 
      ''Cost of Sales'', 
      ''Cost to Acquire Sales'', 
      ''Cost to Serve''
    )
  ';
  v_dk_list  constant CLOB := '
    SELECT /*+ parallel */ UNIQUE to_char(revenue_date_key)
    FROM ' || v_src || ' ORDER BY 1
  ';

  v_alist    TokList;        -- scratch token list
  v_rlist    TokList;        -- list of date key to process
  v_rdate    CLOB;           -- revenue_date_key list
  v_catkey   CLOB;           -- category_key list

  v_proc     varchar2(80) := v_ses_type;
  v_dt       varchar2(20);
  v_sql      CLOB;

  FUNCTION run(p_msg varchar2, p_sql varchar2) RETURN TokList AS
    v_rst TokList;
  BEGIN
    log4me.debug(p_msg, p_sql, v_ses_id);
    IF instr(p_sql, 'ALTER TABLE')=0 THEN
      EXECUTE IMMEDIATE p_sql BULK COLLECT INTO v_rst;
    ELSE
      EXECUTE IMMEDIATE p_sql;
    END IF;
    RETURN v_rst;
  END run;

  FUNCTION wipe_sql(
    p_curr boolean, p_dt varchar2, p_catkey varchar2) RETURN varchar2
  AS
    v_ym    varchar2(10);
    v_part  varchar2(80);
  BEGIN
    SELECT month_year_abbr INTO v_ym FROM d_date
    WHERE date_key = cast(p_dt as number)
      AND month_year_abbr NOT IN ('N/A');

    v_part := 'YM_' || replace(v_ym, '-', '_');

    IF p_curr THEN
      --
      -- current momth: truncate everything from the partition
      --
      RETURN 'ALTER TABLE ' || v_tgt || ' TRUNCATE PARTITION ' || v_part;
    ELSE
      --
      -- previous month: delete only the "repeating" categories
      --
      RETURN '
      DELETE FROM ' || v_tgt || ' PARTITION (' || v_part || ') x
      WEHRE x.revenue_date_key = ' || p_dt || '
        AND EXISTS (
          SELECT 1 FROM d_date d
          WHERE (1=1
            AND d.date_key = x.snapshot_date_key 
            AND d.month_year_abbr > ''' || v_ym || '''
            AND x.fr_category_key in (' || p_catkey || ')
          )
        )
      ';
    END IF;
  END wipe_sql;

BEGIN
  v_rlist  := run('*** ' || v_proc, v_dk_list);
  --
  -- check if any data to be processed. Bail if none.
  --
  IF v_rlist IS NULL OR v_rlist.COUNT=0 THEN
    log4me.info('*** ' || v_proc || ' no data found', v_ses_id);
    RETURN;  
  END IF;

  v_rdate := concat_rpad(v_rlist);
  v_proc  := v_ses_type || '.' || v_rdate;

  log4me.info('INIT ' || v_proc, v_ses_id);
  --
  -- build GL category filter for "repeating" categories
  --
  v_alist  := run(v_proc, v_gl_cat);
  v_catkey := concat_rpad(v_alist);
  --
  -- get every month/partition involved, clean them one by one
  --
  FOR i IN 1..v_rlist.COUNT LOOP
    --
    -- clean existing data month-by-month
    --
    v_dt    := v_rlist(i);
    v_sql   := wipe_sql(i=v_rlist.COUNT, v_dt, v_catkey);
    v_alist := run(v_proc || ' clearing ' || v_dt, v_sql);
  END LOOP;
  --
  --
  v_sql := '
  INSERT /*+ parallel append */ 
  INTO  ' || v_tgt || '(
    GL_ACCT_MONTHLY_SNAPSHOT_KEY,
    REVENUE_DATE_KEY,
    BUSINESS_SECTOR_KEY,
    BUSINESS_SEGMENT_KEY,
    FR_CATEGORY_KEY,
    COST_CENTER_KEY,
    PROGRAM_KEY,
    ACCOUNT_KEY,
    ACCOUNT_HIST_KEY,
    AMOUNT,
    DEPRECIATION_AMOUNT,
    SNAPSHOT_DATE_KEY,
    SESSION_ID,
    ROW_CREATE_DTTM,
    ROW_LAST_MOD_DTTM,
    ROW_LAST_MOD_PROC_NM,
    ROW_LAST_MOD_PROC_SEQ_NBR
  )
  SELECT /*+ parallel full(x) */
    GL_ACCT_DAILY_SNAPSHOT_KEY,
    REVENUE_DATE_KEY,
    BUSINESS_SECTOR_KEY,
    BUSINESS_SEGMENT_KEY,
    FR_CATEGORY_KEY,
    COST_CENTER_KEY,
    PROGRAM_KEY,
    ACCOUNT_KEY,
    ACCOUNT_HIST_KEY,
    AMOUNT,
    DEPRECIATION_AMOUNT,
    SNAPSHOT_DATE_KEY,
    ''' || v_ses_id || ''',
    sysdate,
    sysdate,
    ''' || v_proc || ''',
    rownum
  FROM ' || v_src || ' x
  ';
  log4me.debug(v_proc || ' insert new data into ' || v_tgt, v_sql, v_ses_id);
  EXECUTE IMMEDIATE v_sql;
--  v_alist := run(v_proc || ' insert new data into ' || v_tgt, v_sql);

  COMMIT;
  log4me.info('DONE ' || v_proc, v_ses_id);

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('FAIL ' || v_proc, v_ses_id);
    RAISE; 
END F_GL_ACCT_MON_SNAPSHOT_LOAD;
/
show errors
GRANT EXECUTE ON F_GL_ACCT_MON_SNAPSHOT_LOAD TO DWLOADER;
