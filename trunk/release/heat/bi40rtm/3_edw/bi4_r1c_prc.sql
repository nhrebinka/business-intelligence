--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- EDW_OWNER business/operation objects
@log4me.prc
@xos_tokenizer.prc
@xos_session_mgr.prc
@xos_workspace_mgr.prc
@xbs_site_agg.prc
@f_transaction_line_item_load.prc
@f_gl_acct_daily_snapshot_load.prc
@f_gl_acct_mon_snapshot_load.prc
@f_gl_acct_mon_by_category_load.prc
DROP PROCEDURE f_revenue_aggregates_fix;
DROP PROCEDURE f_revenue_aggregates_h;
DROP PROCEDURE f_revenue_aggregates_t;
