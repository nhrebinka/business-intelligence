--
-- 2013-2014 Revenue Aggregates - Manual Rebates Historical Reload
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht303657_mrbt_site_agg.log;

@xos_workspace_mgr.prc
@xos_session_mgr.prc
@xbs_site_agg.prc

ALTER SESSION ENABLE PARALLEL DML;

DECLARE
  x_prc constant varchar2(200) := 'HT303657: Manual Rebate Site Load';

  v_ym0  TokList := TokList(
    '2013-11', '2013-12', 
    '2014-01', '2014-02', '2014-03', '2014-04', '2014-05', '2014-06', 
    '2014-07', '2014-08', '2014-09', '2014-10'
  );
  v_ym1  TokList := TokList(
    '2014-11', '2014-12', '2015-01', '2015-02'
  );
  v_ses varchar2(40);

BEGIN
  log4me.info('INIT ' || x_prc);

  FOR i IN 1..6 LOOP
    xos_workspace_mgr.free('X_D_SITE_A0' || i);
  END LOOP;

  --
  -- load directly into x_d_site_b 
  --
  FOR i IN 1..v_ym0.COUNT LOOP
    xbs_site_agg.rerun(v_ym0(i), TRUE);
  END LOOP;
  --
  -- keep these in buffer ring
  --
  FOR i IN 1..v_ym1.COUNT LOOP
    xbs_site_agg.rerun(v_ym1(i));
    xos_session_mgr.previous_session('D_SITE_AGG', v_ym1(i), v_ses);
    xos_workspace_mgr.audit_by_session(v_ses);
  END LOOP;

  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

