--
-- Fact tables - enable not Null constraints
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht303657_fact_not_null.log;

ALTER SESSION ENABLE PARALLEL DML;
ALTER SESSION FORCE  PARALLEL DDL;

DECLARE
  x_year constant integer      := &year;

  x_ses constant varchar2(40)  := to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4');
  x_prc constant varchar2(200) := 'HT303657: enforce NOT NULL';
  x_tbl constant TokList := TokList(
    'F_TRANSACTION_LINE_ITEM',
    'F_DAILY_REVENUE_SNAPSHOT',
    'F_DAILY_TOTALS_SNAPSHOT',
    'F_MONTHLY_REVENUE_SNAPSHOT'
  );
  x_clst constant TokList := TokList(
    'PRIVATE_SITE_GALLONS_QTY',
    'PRIVATE_SITE_LITRES_QTY',
    'WAIVED_LATE_FEES_AMOUNT',
    'MANUAL_REBATE_AMOUNT',
    'TRUCK_STOP_FEE_AMOUNT'
  );
  x_nop constant varchar2(10)  := ' ';
  g_msg varchar2(400);

  PROCEDURE exec_sql(p_msg varchar2, p_sql varchar2) AS
  BEGIN
    log4me.debug(p_msg || ' starts ...', p_sql, x_ses);
    EXECUTE IMMEDIATE p_sql;
    IF instr(p_msg, '=>')<>0 THEN
      log4me.debug(p_msg || SQL%ROWCOUNT || ' rows', p_sql, x_ses);
    ELSE
      log4me.debug(p_msg, p_sql, x_ses);
    END IF;
    COMMIT;
  END exec_sql;

  PROCEDURE do_one_year(p_yr varchar2, p_tb varchar2) AS
    x_pt   constant boolean := 
      instr(p_tb, 'MONTHLY')=0 AND instr(p_tb, 'TOTALS')=0;
    x_sql1 constant varchar2(2000) := '
      UPDATE /*+ append */ ' || p_tb || ' #{PT}
      SET
        TRUCK_STOP_FEE_AMOUNT= nvl(TRUCK_STOP_FEE_AMOUNT, 0),
        MANUAL_REBATE_AMOUNT = nvl(MANUAL_REBATE_AMOUNT, 0),
        ROW_LAST_MOD_DTTM    = sysdate,
        ROW_LAST_MOD_PROC_NM = substr(ROW_LAST_MOD_PROC_NM,1,140) || ''#{PC}''
    ' || CASE WHEN x_pt
    THEN ' '
    ELSE '
      WHERE revenue_date_key IN (
        SELECT /*+ noparallel */ date_key FROM d_date
        WHERE YEAR_NUMBER = ' || p_yr || '
      )'
    END;

    x_sql2 constant varchar2(2000) := '
      ALTER TABLE    ' || p_tb || '
      MOVE PARTITION #{PT}
      COMPRESS FOR ALL OPERATIONS 
      UPDATE INDEXES PARALLEL 64
    ';

    v_mn  varchar2(20);
    v_pt  varchar2(80);
    v_sql varchar2(2000);

  BEGIN
    FOR m IN 1..(CASE WHEN x_pt THEN 12 ELSE 1 END) LOOP
      v_mn  := trim(to_char(m, '09'));
      v_pt  := CASE WHEN x_pt
        THEN 'PARTITION(YM_' || p_yr || '_' || v_mn || ')'
        ELSE x_nop
      END;
      v_sql := replace(replace(x_sql1, '#{PT}', v_pt), '#{PC}', x_prc);

      g_msg := x_prc || ' for ' || p_tb || ':' || v_pt;
      exec_sql(g_msg || ' updated => ', v_sql);

      IF x_pt THEN
        v_pt  := 'YM_' || p_yr || '_' || v_mn;
        v_sql := replace(x_sql2, '#{PT}', v_pt);
        exec_sql(g_msg || ' compressed', v_sql);
      END IF;
    END LOOP;
  END do_one_year;

  PROCEDURE enable_not_null(p_tbl varchar2) AS
    x_sql constant TokList := TokList(
      'ALTER TABLE ' || p_tbl || ' ADD CONSTRAINT #{CN} CHECK(#{CI} IS NOT NULL) DISABLE',
      'ALTER TABLE ' || p_tbl || ' ENABLE NOVALIDATE CONSTRAINT #{CN}',
      'ALTER TABLE ' || p_tbl || ' ENABLE VALIDATE CONSTRAINT #{CN}'
    );
    x_msg constant TokList := TokList(
      ' created',
      ' enabled',
      ' validated'
    );

    v_sql varchar2(2000);
    v_cst varchar2(80);
  BEGIN
    FOR c IN 1..x_clst.COUNT LOOP
      v_cst := 'CNN' || p_tbl || '_' || c;
      FOR s IN 1..x_sql.COUNT LOOP
        v_sql := replace(replace(x_sql(s), '#{CN}', v_cst), '#{CI}', x_clst(c));
        exec_sql(x_prc || x_msg(s), v_sql);
      END LOOP;
    END LOOP;
  END enable_not_null;

BEGIN
  log4me.info('INIT ' || x_prc, x_ses);

  FOR y IN x_year..x_year LOOP
    log4me.debug(x_prc || ' processing year ' || y, '', x_ses);
    FOR t IN 1..x_tbl.COUNT LOOP
      do_one_year(trim(to_char(y, '9999')), x_tbl(t));
    END LOOP;
  END LOOP;  

  FOR i IN 1..x_tbl.COUNT LOOP
    NULL;  -- taking the table offline too long, skip for now
--    enable_not_null(x_tbl(t));
  END LOOP;

  log4me.info('DONE ' || x_prc, x_ses);

EXCEPTION WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('FAIL ' || g_msg, x_ses);
  RAISE;
END;
/
show errors

SPOOL OFF

