set serveroutput on

alter session enable parallel ddl;
alter session enable parallel dml;

create table /*+ append */ tmp_tli 
tablespace d_bi 
as
select /*+ parallel(x, 64) */
  transaction_line_item_key, 
  truck_stop_fee_amount, 
  manual_rebate_amount
from   f_transaction_line_item 
where revenue_date_key >= 12359
and   (truck_stop_fee_amount <> 0 or manual_rebate_amount <> 0)
;

commit;

alter table f_transaction_line_item set unused column truck_stop_fee_amount;
alter table f_transaction_line_item set unused column manual_rebate_amount;
alter table f_transaction_line_item set unused column retail_ppu_amount;
alter table f_transaction_line_item set unused column adjusted_ppu_amount;
alter table f_transaction_line_item set unused column better_price_ppu_amount;

alter table f_transaction_line_item add (
  truck_stop_fee_amount   number(15,2) default 0 not null,
  manual_rebate_amount    number(15,2) default 0 not null,
  retail_ppu_amount       number(15,5) default 0 not null,
  adjusted_ppu_amount     number(15,5) default 0 not null,
  better_price_ppu_amount number(15,5) default 0 not null
)
;

merge into f_transaction_line_item x
using tmp_tli t on (t.transaction_line_item_key = x.transaction_line_item_key)
when matched then update set
  x.truck_stop_fee_amount = t.truck_stop_fee_amount,
  x.manual_rebate_amount  = t.manual_rebate_amount
;

commit;

drop table tmp_tli purge;

