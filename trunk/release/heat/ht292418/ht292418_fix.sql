--
-- HT292418 migration script
--
--   Site Agg fix to add 2014-08
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht292418_site_agg_fix.log;

alter session enable parallel dml;
begin
  --
  -- audit 2014-08
  --
  xos_workspace_mgr.audit('X_D_SITE_A03');
  --
  -- re-run 2014-09 and to create the view
  --
  F_DAILY_SITE_AGGREGATES;
  commit;
end;
/

SPOOL OFF


