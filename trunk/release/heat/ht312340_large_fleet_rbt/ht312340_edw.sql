SET ECHO        ON
SET NEWPAGE     0
SET SPACE       0
SET PAGESIZE    0
SET FEEDBACK    ON
SET HEADING     ON
SET TRIMSPOOL   ON
SET TAB         OFF
SET LINESIZE    2000

SPOOL /i1/&env/hub/logs/ht312340_large_fleet_rbt.log;

declare
  x_prc varchar2(80) := 'HT312340: Large Fleet Manual Rebate Update';
  v_ym  varchar2(20);
  v_sql varchar2(2000);
begin
 log4me.info(x_prc || ' INIT');
  for y in 2015..2015 loop
    for  m in 1..12 loop
      exit when y=2015 and m>5;

      v_ym  := 'YM_' || trim(to_char(y)) || '_' || trim(to_char(m, '00'));
      v_sql := '
        UPDATE /*+ parallel(x, 64) */
          F_TRANSACTION_LINE_ITEM PARTITION (' || v_ym || ') x
        SET 
          GROSS_REVENUE_AMOUNT = GROSS_REVENUE_AMOUNT * -1,
          MANUAL_REBATE_AMOUNT = MANUAL_REBATE_AMOUNT * -1,
          ROW_LAST_MOD_DTTM    = sysdate
        WHERE 1=1
        AND ROW_SOURCE_SYS_NM=''EXCEL AUTO REBATES''
        AND (GROSS_REVENUE_AMOUNT<>0 OR MANUAL_REBATE_AMOUNT<>0)
      ';
      execute immediate v_sql;
      log4me.debug(x_prc || ' => ' || sql%rowcount || ' row updated', v_sql);
      commit;
    end loop;
  end loop;
  log4me.info(x_prc || ' DONE');

exception when others then
  log4me.debug(x_prc || ' FAIL', v_sql);
  log4me.err(x_prc);
  raise;
end;
/
show errors

SPOOL OFF;
