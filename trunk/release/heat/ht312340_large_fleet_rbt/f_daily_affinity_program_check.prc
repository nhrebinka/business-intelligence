CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_AFFINITY_PROGRAM_CHECK(
  p_bgn_dt date     := NULL,
  p_end_dt date     := NULL
)
AS
  /*
  NAME               :-   F_DAILY_AFFINITY_PROGRAM_CHECK
  Original AUTHOUR   :-   VENU KANCHARLA
  DATE               :-   06/18/2013

  DECRIPTION         :-   
    PROCEURE TO CHECK IF A  PROGRAM HAS BEEN MODIFIED FROM 701 
    Affinity to Something else ..? This is happening on the classic due to 
    the plastic code changes for Affinity Programs. This Procedure would 
    identify suck propgrams and correct the data appropriately.

  Revision History
  1.0 Venu Kancharla
  1.1 Venu Kancharla - Removed the Filter For Program Name So that it can 
                       handle all duplicate programs

  2.0 Venu Kancharla - added another Routine to handle the Duplicate Cards 
                       for Puchase Device Event
  2.1 20140604: CC   - add full hints to avoid the need for pre-run Stat 
                       on F_TRANSACTION_LINE_ITEM

  3.0 20141106: CC   - update to PROGRAM_NAME change; refactor code structure
  3.1 20141231: CC   - check both fact tables for duplicates, i.e.
                       F_TRANSACTION_LINE_ITEM and F_PURCHASE_DEVICE_EVENT.
  3.2 20150318: CC   - add v_old_pgm_key instead of hardcoded 701
  3.3 20150327: CC   - analyze partition at new month begin
  3.4 20150421: CC   - add optional date range input
  */
  X_PRC    constant varchar2(120) := 'affinity_program_check';
  X_SES_ID constant varchar2(40)  := to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4');

  -- vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  --
  -- here are the default values that might need to be changed
  -- in the future depends on the slowly changing business rules
  --
  X_PGM_SPNR_NM  constant varchar2(40) := '(''Affin 53'',''Affinity 54'')';
  X_OLD_PGM_KEY0 constant integer      := 700; -- PROGRAM_KEY for 'Affinity 54'
  X_OLD_PGM_KEY1 constant integer      := 701; -- PROGRAM_KEY for 'Affin 53'
  --
  -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  X_SQL varchar2(32767) := '
  SELECT ACCOUNT_KEY, PROGRAM_KEY, 0, PROGRAM_SPONSOR_NAME FROM (
    SELECT /*+ full(f) parallel(f, 64) use_hash(f, p) */ 
      UNIQUE f.ACCOUNT_KEY, f.PROGRAM_KEY, p.PROGRAM_SPONSOR_NAME
    FROM F_PURCHASE_DEVICE_EVENT f
    JOIN D_PROGRAM p ON (p.PROGRAM_KEY = f.PROGRAM_KEY)
    WHERE f.ACCOUNT_KEY IN (
      SELECT ACCOUNT_KEY FROM (
        SELECT /*+ parallel(x, 64) full(x) */ 
          UNIQUE ACCOUNT_KEY, PROGRAM_KEY
        FROM   F_PURCHASE_DEVICE_EVENT x
      )
      GROUP BY ACCOUNT_KEY HAVING count(*) > 1
    )
    UNION 
    SELECT /*+ full(f) parallel(f, 64) use_hash(f, p) */
      UNIQUE f.PURCHASE_ACCOUNT_KEY, f.PROGRAM_KEY, p.PROGRAM_SPONSOR_NAME
    FROM F_TRANSACTION_LINE_ITEM PARTITION(#{PART}) f
    JOIN D_PROGRAM p ON (p.PROGRAM_KEY = f.PROGRAM_KEY)
    WHERE f.PURCHASE_ACCOUNT_KEY IN ( 
      SELECT PURCHASE_ACCOUNT_KEY FROM (
        SELECT /*+ parallel(x,64) full(x) */
           UNIQUE PURCHASE_ACCOUNT_KEY, PROGRAM_KEY
        FROM F_TRANSACTION_LINE_ITEM PARTITION(#{PART}) x
      )
      GROUP BY PURCHASE_ACCOUNT_KEY HAVING count(*) > 1
    )
  )
  ORDER BY ACCOUNT_KEY, PROGRAM_KEY, PROGRAM_SPONSOR_NAME
  ';

  TYPE REC IS RECORD(
    acct_key integer,
    pgm_key  integer,
    to_pkey  integer,
    spnr_nm  varchar2(200)
  );

  TYPE T_REC IS TABLE OF REC;
  v_lst T_REC;

  v_min_dt date;
  v_max_dt date;
  v_part   varchar2(20);
  v_sql    varchar2(32767);
  v_cnt    integer := 0;

  PROCEDURE get_stats(p_part varchar2) IS
    v_rows integer;
  BEGIN
    BEGIN
      SELECT num_rows INTO v_rows
      FROM   user_tab_partitions 
      WHERE  table_name = 'F_TRANSACTION_LINE_ITEM'
        AND  partition_name = v_part;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_rows := 0;
    END;

    IF v_rows = 0 THEN
      log4me.info(X_PRC || ' analyze partition ' || p_part, X_SES_ID);
      EXECUTE IMMEDIATE '
        ANALYZE TABLE f_transaction_line_item
        PARTITION(' || p_part || ') 
        COMPUTE STATISTICS';
    END IF;
  END get_stats;

BEGIN
  log4me.info('*** INIT ' || X_PRC, X_SES_ID);
  --
  -- find the processing data from the seed table
  --
  xos_session_mgr.get_date_range(
    p_bgn_dt,
    p_end_dt,
    'EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM',
    v_min_dt,
    v_max_dt
  );
  IF (v_min_dt IS NULL OR v_max_dt IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20400, 'no date range given');
  END IF;
  --
  -- Create cursor for Looping thru if we find multiple accounts for Affinity Changes ...
  --
  v_part := 'YM_' || to_char(v_min_dt, 'YYYY_MM');
  v_sql  := replace(X_SQL, '#{PART}', v_part);
  --
  -- The following Cursor is to identify the Duplicate programs occuriing on the Revenue Side
  --
  log4me.debug('starting ' || X_PRC, v_sql, X_SES_ID);

  get_stats(v_part);

  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_lst;
  log4me.info(X_PRC || ' ' || SQL%ROWCOUNT || ' dup accounts found', X_SES_ID);
  --
  -- pairing the old_progm_key with new_program_key
  -- might need to go into a collection should we have more than one matches
  --  
  FOR i IN 1..v_lst.COUNT LOOP
    FOR j IN 1..v_lst.COUNT LOOP
      IF v_lst(i).acct_key=v_lst(j).acct_key
        AND v_lst(i).spnr_nm IN ('Affin 53', 'Affinity 54')
        AND v_lst(j).spnr_nm NOT IN ('Affin 53', 'Affinity 54')
      THEN
        v_lst(i).to_pkey := v_lst(j).pgm_key;
      END IF;
    END LOOP;
  END LOOP;
  --
  -- the real fixing comes here
  --
  -- vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  FOR i IN 1..v_lst.COUNT LOOP
    CONTINUE WHEN v_lst(i).to_pkey=0;
    log4me.info(
      X_PRC || ' dup found in FACT table(s). Fixing ' ||
      'account_key=' || v_lst(i).acct_key || 
      ', old_pgm_key=' || v_lst(i).pgm_key || 
      ' => new_pgm_key=' || v_lst(i).to_pkey,
      X_SES_ID
    );
    --
    -- Correcting the account affinity program in fact tables with 
    -- the correct affinity program key
    --
    XOS_ACCOUNT_AFFINITY_FIX(
      'acct affinity[' || v_lst(i).pgm_key || ' => ' || v_lst(i).to_pkey || ']',
      v_lst(i).acct_key, -- the account that has two programs
      v_lst(i).pgm_key,  -- the original program key that we want to replace
      v_lst(i).to_pkey,  -- the new program key that we think it should be
      v_part             -- table partition if used in the updated tables
    );
    v_cnt := v_cnt + 1;
  END LOOP;
  -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  log4me.info(
    '*** GOOD ' || X_PRC || ' => ' || v_cnt || ' key(s) updated', 
    X_SES_ID
  );
  --
  -- double check the result, in case we have process run in parallel
  -- in other session that we didn't know about
  --
  log4me.debug('validating ' || X_PRC, v_sql, X_SES_ID);

  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_lst;
  FOR i IN 1..v_lst.COUNT LOOP
    log4me.info(
      X_PRC || 
      ' still dup found in FACT(s): ' ||
      'account_key=' || v_lst(i).acct_key || 
      ', pgm_key=' || v_lst(i).pgm_key ||
      '. Ctrl-M schedule review needed!',
      X_SES_ID
    );
  END LOOP;
  IF v_cnt=0 THEN
    log4me.debug(X_PRC || ' no more dup found in FACT(s).', X_SES_ID);
  END IF;
  log4me.info('*** DONE ' || X_PRC, X_SES_ID);

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('*** FAIL ' || X_PRC || SQLERRM, X_SES_ID);
END F_DAILY_AFFINITY_PROGRAM_CHECK;
/
show errors

-- ============================================================================
GRANT EXECUTE ON F_DAILY_AFFINITY_PROGRAM_CHECK TO DWLOADER;
