--
-- Manual Rebate change sign - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht301154.log;

alter session enable parallel dml;

begin
  update /*+ parallel(x, 64) append */ 
    f_transaction_line_item partition (ym_2014_11) x
  set gross_revenue_amount = -gross_revenue_amount,
      manual_rebate_amount = -manual_rebate_amount,
      purchase_account_key = billing_account_key,
      line_item_type_key   = 18002,
      row_last_mod_dttm    = sysdate,
      row_last_mod_proc_nm = row_last_mod_proc_nm || '; ht301154'
  where row_last_mod_proc_nm like '%MANUAL_REBATE%'
    and x.gross_revenue_amount > 0
  ;
  commit;
end;
/

SPOOL OFF

