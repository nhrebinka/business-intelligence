--
-- 2013-2014 Revenue Aggregates - Manual Rebates Historical Reload
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht303657_mrbt_his_agg.log;

@f_revenue_aggregates.prc

ALTER SESSION ENABLE PARALLEL DML;
ALTER SESSION SET SKIP_UNUSABLE_INDEXES = TRUE; 

DECLARE
  x_prc constant varchar2(200) := 'HT303657: Manual Rebate Hist Load';

  v_ym  TokList;
  v_dx  TokList;

  v_dt  date;
  v_pt0 varchar2(20) := 'N/A';
  v_pt  varchar2(20);
  v_cnt integer;

  PROCEDURE disable_idx AS
    x_bidx constant TokList := TokList(
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_UI1',
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_UI2',
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_UI3',
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_UI4',
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_UI5'
    );
  BEGIN
    log4me.info(x_prc || ' => disable (non-unique) indexes');
    FOR i IN 1..x_bidx.COUNT LOOP
      EXECUTE IMMEDIATE 'ALTER INDEX ' || x_bidx(i) || ' UNUSABLE';
    END LOOP;
  END disable_idx;

  PROCEDURE rebuild_idx AS
    x_uidx constant TokList := TokList(
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_NK'
    );
  BEGIN
    log4me.info(x_prc || ' => rebuild unique indexes');
    FOR i IN 1..x_uidx.COUNT LOOP
      EXECUTE IMMEDIATE '
        ALTER INDEX ' || x_uidx(i) || '
        REBUILD PARTITION ' || v_pt || ' PARALLEL 64
      ';
    END LOOP;
  END rebuild_idx;

  PROCEDURE compress_parti AS
  BEGIN
    log4me.debug(x_prc || ' => compressing ' || v_pt, '');
    EXECUTE IMMEDIATE '
      ALTER TABLE EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT
      MOVE PARTITION ' || v_pt || '
      COMPRESS UPDATE INDEXES PARALLEL 64
      NOLOGGING
    ';
  END compress_parti;

BEGIN
  log4me.info('INIT ' || x_prc);
  --
  -- get date list with manual rebates
  --
  SELECT /*+ parallel(x,64) use_hash(d, x) */
    unique d.month_year_abbr, d.date_text
    unique d.month_year_abbr, d.date_text
  BULK COLLECT INTO v_ym, v_dx
  FROM f_transaction_line_item x
  JOIN d_date d ON (d.date_key = x.revenue_date_key)
  WHERE x.revenue_date_key between 12055 and 12905
  AND   ROW_SOURCE_SYS_NM='EXCEL AUTO REBATES'
  ORDER BY d.month_year_abbr desc, d.date_text
  ;

--  disable_idx;

  v_cnt := v_ym.COUNT;
  FOR i IN 1..v_cnt LOOP
    v_pt := 'YM_' || replace(v_ym(i), '-', '_');
    v_dt := to_date(v_dx(i), 'MM/DD/YYYY');

    log4me.debug(x_prc || ' => agg: ' || v_dx(i), '');
    --
    -- make sure indices are usable
    --
    IF v_pt0 <> v_pt THEN 
      rebuild_idx;
      v_pt0 := v_pt;
    END IF;
    --
    -- rebuild daily aggregates, and monthly aggregate on last day of the month
    --
    IF (i=v_cnt) OR (i<v_cnt AND v_ym(i+1)<>v_ym(i)) THEN
      f_revenue_aggregates(v_dt, v_dt, TRUE);
      compress_parti;
    ELSE
      f_revenue_aggregates(v_dt, v_dt, FALSE);
    END IF;
  END LOOP;  

  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

