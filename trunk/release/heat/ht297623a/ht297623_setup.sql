--
-- BI4.0 patch - script to correct external table DDL
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht255358_297623a.log;

@hub_drp.ddl
@hub_tbl.ddl
@hub_gnt.ddl

@log4me.prc

SPOOL OFF


