--
-- P1 HT295309 migration script
--
--  Backup Program Tables
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht295309_fix.log;

alter session enable parallel dml;
--
-- backup program table
--
CREATE TABLE D_PROGRAM_201410_BAK TABLESPACE D_BI
AS SELECT * FROM D_PROGRAM;
GRANT SELECT ON D_PROGRAM_201410_BAK TO EDW_OWNER_SELECT;
--
-- turn audit on
--
begin
  xos_workspace_mgr.audit('X_D_SITE_A05');
  F_DAILY_SITE_AGGREGATES;
end;
/
@log4me.prc

SPOOL OFF


