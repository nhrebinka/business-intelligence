CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_AFFINITY_PROGRAM_CHECK
AS
  /*
   NAME               :-   F_DAILY_AFFINITY_PROGRAM_CHECK
   Original AUTHOUR   :-   VENU KANCHARLA
   DATE               :-   06/18/2013
   DECRIPTION         :-   PROCEURE TO CHECK IF A  PROGRAM HAS BEEN MODIFIED FROM 701 ( Affinity to Something else ..?)
                           This is happening on the classic due to the plastic code changes for Affinity Programs ..
                           This Procedure would identify suck propgrams and correct the data appropriately ...


   LOG                 -   VERSION 1.0 Venu Kancharla
                       -   Version 1.1 Venu Kancharla Removed the Filter For Program Name So that it can handle all duplicate programs .. .
                       -   Version 2.0  Venu Kancharla added another Routine to handle the Duplicate Cards for Puchase Device Event ...
                       -   Version 2.1 CC - mask out extra program check for a quick fix
*/


   v_calendar_dt_start           DATE;
   v_calendar_dt_end             DATE;

   v_first_day_in_month_date_dt  DATE ;
   v_last_day_in_month_date_dt   DATE ;

   V_SQL                         VARCHAR2 (4000);

   V_PDE_SQL                         VARCHAR2 (4000);

   V_SQL1                        VARCHAR2(2000) ;


   v_account_key                 INTEGER ;
   v_program_key                 INTEGER ;

   v_date_key_start              INTEGER ;
   V_date_key_end                INTEGER  ;

   V_PART                        VARCHAR2 (10);

   v_program_name               D_PROGRAM.PROGRAM_NAME%type :=  'Affinity' ;

   v_program_sponsor_name1      D_PROGRAM.PROGRAM_SPONSOR_NAME%TYPE   := 'Affin 53' ;

   v_program_sponsor_name2      D_PROGRAM.PROGRAM_SPONSOR_NAME%TYPE   := 'Affinity 54';

   TYPE   REF_CURSOR   IS  REF CURSOR ;

   C1                      REF_CURSOR ;

   C2                      REF_CURSOR ;


BEGIN


F_SEED_DATES (v_calendar_dt_start, v_calendar_dt_end); -- Seed Dates to find the processing Data

SELECT
       DD.FIRST_DAY_IN_MONTH_DATE_DT,
       DD.LAST_DAY_IN_MONTH_DATE_DT
  INTO
       v_first_day_in_month_date_dt ,
       v_last_day_in_month_date_dt
  FROM
        D_DATE  DD
 WHERE
        DD.CALENDAR_DATE_DT = v_calendar_dt_start;


SELECT
       DD.DATE_KEY
  INTO
       v_date_key_start
  FROM
        D_DATE  DD
 WHERE
        DD.CALENDAR_DATE_DT = v_first_day_in_month_date_dt ;


 -- Create cursor for Looping thru if we find multiple accounts for Affinity Changes ...


V_PART :=   'YM_' || TO_CHAR (v_last_day_in_month_date_dt, 'YYYY_MM');


 /* The following Cursor is to identify the Duplicate programs occuriing on the Revenue Side

 */


  V_SQL := '
  SELECT  F.PURCHASE_ACCOUNT_KEY AS ACCOUNT_KEY ,   F.PROGRAM_KEY AS PROGRAM_KEY
  FROM    F_TRANSACTION_LINE_ITEM PARTITION ('||V_PART||') F, D_PROGRAM DP
  WHERE   F.PROGRAM_KEY   = DP.PROGRAM_KEY
    AND DP.PROGRAM_SPONSOR_NAME NOT IN ( :V_PROGRAM_SPONSOR_NAME1 )
    AND DP.PROGRAM_SPONSOR_NAME NOT IN ( :V_PROGRAM_SPONSOR_NAME2 )
    AND F.PURCHASE_ACCOUNT_KEY IN  (  
    SELECT PURCHASE_ACCOUNT_KEY FROM (
      SELECT  FDR.PURCHASE_ACCOUNT_KEY,  FDR.PROGRAM_KEY
      FROM    F_TRANSACTION_LINE_ITEM PARTITION ('||V_PART||') FDR,  D_PROGRAM DP
      WHERE   DP.PROGRAM_KEY  = FDR.PROGRAM_KEY
      GROUP BY  FDR.PURCHASE_ACCOUNT_KEY,FDR.PROGRAM_KEY
    )
    GROUP BY  PURCHASE_ACCOUNT_KEY HAVING COUNT (*) > 1  
  )
  GROUP BY  F.PURCHASE_ACCOUNT_KEY,  F.PROGRAM_KEY' ;

  DBMS_OUTPUT.PUT_LINE( v_sql)  ;


OPEN C1 FOR  V_SQL USING   V_PROGRAM_SPONSOR_NAME1, V_PROGRAM_SPONSOR_NAME2;

LOOP

FETCH   c1    into V_account_key  , v_program_key  ;
EXIT WHEN C1%NOTFOUND ;

DBMS_OUTPUT.PUT_LINE( 'REC.ACCOUNT_KEY ' || v_account_key)  ;

DBMS_OUTPUT.PUT_LINE( 'REC.PROGRAM_KEY ' || v_Program_key)  ;

DBMS_OUTPUT.PUT_LINE( 'V_PART   ' || v_part ) ;


-- Correcting the affinity program transactions with the correct affinity program


 V_SQL1 :=  'UPDATE F_TRANSACTION_LINE_ITEM  partition ('||v_part||')  F
            SET F.PROGRAM_KEY             = '||v_program_key||' ,
                F.ROW_LAST_MOD_PROC_NM    = ''Auto correct to Afinity program data issue'' ,
                F.ROW_LAST_MOD_DTTM       = SYSDATE
           WHERE
                  F.PURCHASE_account_key    ='||v_account_key||'
              AND F.PROGRAM_KEY             =  701 '
            ;

 DBMS_OUTPUT.PUT_LINE ( ' V_SQL1 ' || V_SQL1 ) ;

 EXECUTE IMMEDIATE V_SQL1 ;


 COMMIT ;


-- Update the Revenue Snapshot with the correct Affinity program


 V_SQL1:=      'UPDATE F_DAILY_REVENUE_SNAPSHOT partition ('||v_part||') F
                SET F.PROGRAM_KEY             =  '||v_program_key||' ,
                   F.ROW_LAST_MOD_PROC_NM    = ''Auto correct to Afinity program data issue'' ,
                   F.ROW_LAST_MOD_DTTM       = SYSDATE
                WHERE
                     F.ACCOUNT_KEY    = '||  v_account_key||'
                 AND F.PROGRAM_KEY    =  701 '
               ;

DBMS_OUTPUT.PUT_LINE ( ' V_SQL1  ' || V_SQL1 ) ;

EXECUTE IMMEDIATE V_SQL1 ;

COMMIT ;

--SHOULD UPDATE 1  records only ..

 V_SQL1 :=  'UPDATE  F_DAILY_ACCOUNT_SNAPSHOT  partition ('||v_part||') F
             SET  PROGRAM_KEY             ='|| v_program_key||'  ,
                 ROW_LAST_MOD_PROC_NM    = ''Auto correct to Afinity program data issue''  ,
                 ROW_LAST_MOD_DTTM       = SYSDATE
            WHERE
                 F.ACCOUNT_KEY = '||v_account_key || '
             and PROGRAM_KEY   = 701 '
               ;
DBMS_OUTPUT.PUT_LINE ( ' V_SQL1  ' || V_SQL1 ) ;

EXECUTE IMMEDIATE V_SQL1 ;

COMMIT ;



  UPDATE F_PURCHASE_DEVICE_EVENT F
     SET    F.PROGRAM_KEY             =  v_program_key  ,
            F.ROW_LAST_MOD_PROC_NM    = 'Auto correct to Afinity program data issue' ,
            F.ROW_LAST_MOD_DTTM       = SYSDATE
  WHERE
         F.account_key    = v_account_key
     AND F.PROGRAM_KEY    = 701
   ;


   COMMIT ;

    UPDATE  F_MONTHLY_ACCOUNT_SNAPSHOT
    SET  PROGRAM_KEY             = v_program_key  ,
         ROW_LAST_MOD_PROC_NM    = 'Auto correct to Afinity program data issue' ,
         ROW_LAST_MOD_DTTM       = SYSDATE
    WHERE
         account_key  = v_account_key
     and PROGRAM_KEY  = 701   ;

  COMMIT ;

   UPDATE  F_MONTHLY_REVENUE_SNAPSHOT
    SET  PROGRAM_KEY             = v_program_key  ,
         ROW_LAST_MOD_PROC_NM    = 'Auto correct to Afinity program data issue' ,
         ROW_LAST_MOD_DTTM       = SYSDATE
    WHERE
         account_key  = v_account_key
     and PROGRAM_KEY  = 701   ;

  COMMIT ;


END LOOP ;

/*  The Following Cusrsor is to find the Duplicate programs related to Affinity for Account Aggregates ...

*/


  V_PDE_SQL := '(SELECT  /*+ parallel 16 */ F.ACCOUNT_KEY AS ACCOUNT_KEY , F.PROGRAM_KEY AS PROGRAM_KEY
                  FROM
                      F_PURCHASE_DEVICE_EVENT F, D_PROGRAM DP
                 WHERE
                      F.PROGRAM_KEY   = DP.PROGRAM_KEY
                 AND DP.PROGRAM_SPONSOR_NAME NOT IN ( :V_PROGRAM_SPONSOR_NAME1 )
                 AND DP.PROGRAM_SPONSOR_NAME NOT IN ( :V_PROGRAM_SPONSOR_NAME2 )
                 AND F.ACCOUNT_KEY IN   ( SELECT ACCOUNT_KEY FROM
                                                                (SELECT  FDR.ACCOUNT_KEY,  FDR.PROGRAM_KEY
                                                                   FROM
                                                                        F_PURCHASE_DEVICE_EVENT  FDR,  D_PROGRAM DP
                                                                   WHERE DP.PROGRAM_KEY  = FDR.PROGRAM_KEY
                                                               GROUP BY  FDR.ACCOUNT_KEY,FDR.PROGRAM_KEY
                                                               )
                                            GROUP BY  ACCOUNT_KEY HAVING COUNT (*) > 1
                                          )
                 GROUP BY  F.ACCOUNT_KEY,  F.PROGRAM_KEY )  ' ;

  DBMS_OUTPUT.PUT_LINE( v_pde_sql )  ;


OPEN C2  FOR  V_PDE_SQL USING   V_PROGRAM_SPONSOR_NAME1, V_PROGRAM_SPONSOR_NAME2;

LOOP

FETCH   C2    into V_account_key  , v_program_key  ;
EXIT WHEN C2%NOTFOUND ;

DBMS_OUTPUT.PUT_LINE( 'REC.ACCOUNT_KEY ' || v_account_key)  ;
DBMS_OUTPUT.PUT_LINE( 'REC.PROGRAM_KEY ' || v_Program_key)  ;
DBMS_OUTPUT.PUT_LINE( 'V_PART   ' || v_part ) ;


     UPDATE F_PURCHASE_DEVICE_EVENT F
         SET    F.PROGRAM_KEY             =  v_program_key  ,
                F.ROW_LAST_MOD_PROC_NM    = 'Auto correct to Afinity program data issue' ,
                F.ROW_LAST_MOD_DTTM       = SYSDATE
      WHERE
             F.account_key    = v_account_key
         AND F.PROGRAM_KEY    = 701
       ;

     COMMIT ;


     V_SQL1 :=  'UPDATE  F_DAILY_ACCOUNT_SNAPSHOT  partition ('||v_part||') F
                 SET  PROGRAM_KEY             ='|| v_program_key||'  ,
                     ROW_LAST_MOD_PROC_NM    = ''Auto correct to Afinity program data issue''  ,
                     ROW_LAST_MOD_DTTM       = SYSDATE
                WHERE
                     F.ACCOUNT_KEY = '||v_account_key || '
                 and PROGRAM_KEY   = 701 '
                   ;
    DBMS_OUTPUT.PUT_LINE ( ' V_SQL1  ' || V_SQL1 ) ;

    EXECUTE IMMEDIATE V_SQL1 ;

    COMMIT ;



    UPDATE  F_MONTHLY_ACCOUNT_SNAPSHOT
    SET  PROGRAM_KEY             = v_program_key  ,
         ROW_LAST_MOD_PROC_NM    = 'Auto correct to Afinity program data issue' ,
         ROW_LAST_MOD_DTTM       = SYSDATE
    WHERE
         account_key  = v_account_key
     and PROGRAM_KEY  = 701   ;

  COMMIT ;


END LOOP ;


END F_DAILY_AFFINITY_PROGRAM_CHECK;
/
