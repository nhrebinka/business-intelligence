--
-- 2013-2015 Aggregates Rerun: due to GL MON_BY_CATEGORY agg. error
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht309365_bi_mon_by_cat.log;

@f_gl_acct_mon_by_category_load.prc

ALTER SESSION ENABLE PARALLEL DML;

DECLARE
  x_prc varchar2(100) := 'BI4 MON_BY_CATEGORY Fix - Agg Rerun';
  v_dt  date;
BEGIN
  log4me.info('INIT ' || x_prc);
  --
  -- get date list with manual rebates
  --
  for y in 2013..2015 loop
    for m in 1..12 loop
      continue when y=2015 and m>3;
      v_dt := to_date(to_char(y) || to_char(m, '00') || '01', 'YYYYMMDD');
--        log4me.info('do=>' || to_char(v_dt,'YYYYMMDD'));
      f_gl_acct_mon_by_category_load(v_dt, v_dt);
    end loop;
  end loop;
  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

