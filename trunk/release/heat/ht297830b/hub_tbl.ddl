--
-- Wex BI4.0 - DDL for HUB Fact Temp tables
--
-- ------------------------------------------------------------------------
--
-- TABLE: T_MANUAL_REBATE 
--
ALTER TABLE T_MANUAL_REBATE MODIFY (
  REBATE_DT        TIMESTAMP, 
  EDM_CREATE_DT    TIMESTAMP,
  EDM_LAST_UPDT_DT TIMESTAMP
);
