--
-- BI4.0 patch - script to create synonym for DWLOADER
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht255358_297830b.log;

CREATE OR REPLACE SYNONYM edm_business_segment_seq   FOR edm_owner.edm_business_segment_seq;
CREATE OR REPLACE SYNONYM t_allocation_event         FOR edm_owner.t_allocation_event;

CREATE OR REPLACE SYNONYM src_ps_dept_tbl            FOR stage_owner.src_ps_dept_tbl;
CREATE OR REPLACE SYNONYM src_ps_ledger              FOR stage_owner.src_ps_ledger;
CREATE OR REPLACE SYNONYM src_acct_since_date_lookup FOR stage_owner.src_acct_since_date_lookup;

SPOOL OFF


