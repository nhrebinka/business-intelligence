--
-- BI4.0 patch - script to correct external table DDL
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht255358_patch.log;

@src_drp.ddl
@src_tbl.ddl
@src_gnt.ddl

SPOOL OFF


