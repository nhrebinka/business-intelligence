--
-- HT298691 fix for EDW_STAGE_OWNER
--
-- Issue:    newly SDLC14.2 release missing interchange in the ETL
-- Solution: remove the loaded TP/CP data off F_TRANSACTION_LINE_ITEM
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht298691.log;

alter session enable parallel dml;

declare
  v_proc varchar2(80) := 
    'HT298691 - SDLC14.2 interchange missing for EDW_STAGE_OWNER';
begin
  edw_owner.log4me.info('INIT ' || v_proc);

  delete /*+ parellel(x, 64) */ 
  from  edw_stage_owner.f_transaction_line_item
  where row_source_sys_nm = 'TP/CP'
  and   revenue_date_key = 12733;

  edw_owner.log4me.info('DONE ' || v_proc || ' => ' || SQL%ROWCOUNT || ' rows');
  commit;
end;
/
show errors

SPOOL OFF


