CREATE OR REPLACE PROCEDURE EDW_OWNER.XOS_ACCOUNT_AFFINITY_FIX (
  p_proc    varchar2,  -- process name for logging
  p_acct    number,    -- account key that has multiple program keys
  p_pgm_old number,    -- program ke that needs to be replaced
  p_pgm_new number,    -- the new program key that we want to be
  p_part    varchar2 := 'YM_' || to_char(sysdate, 'YYYY_MM')
)
AS
  v_ses  constant varchar2(40) := 
    to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4');
  v_part constant varchar2(40) := 'partition (' || p_part || ')';
  v_tbl  constant TokList      := TokList(
    'f_purchase_device_event',
    'f_accounts_receivable_entry',
    'f_account_naics_sic_event',
    'f_monthly_account_snapshot',
    'f_gl_acct_monthly_snapshot',
    'f_daily_aging_snapshot   ' || v_part,
    'f_daily_revenue_snapshot ' || v_part,
    'f_daily_account_snapshot ' || v_part,
    'f_gl_acct_daily_snapshot ' || v_part
  );
  v_cond constant varchar2(400) := '
    set program_key          = ' || p_pgm_new || ',
        row_last_mod_dttm    = sysdate,
        row_last_mod_proc_nm = ''' || p_proc || '''
    where account_key = ' || p_acct    || '
      and program_key = ' || p_pgm_old || '
  ';
  v_msg varchar2(400);
  v_sql varchar2(32767);

BEGIN
  log4me.info('INIT ' || p_proc);
  --
  -- data fix for affinity program, account association
  --
  v_msg := p_proc || ' f_transaction_line_item/' || p_acct;
  v_sql := '
    update f_transaction_line_item ' || v_part || '
    set program_key        = ' || p_pgm_new || ',
      row_last_mod_dttm    = sysdate,
      row_last_mod_proc_nm = ''' || p_proc || '''
    where program_key = ' || p_pgm_old || ' and (
      purchase_account_key = ' || p_acct || ' or
      billing_account_key  = ' || p_acct || '
    )
  ';
  EXECUTE IMMEDIATE v_sql;
  log4me.debug(v_msg || ' => ' || SQL%ROWCOUNT || ' rows updated', v_sql);
  --
  -- f_gl_acct_mon_by_category
  --
  v_msg := p_proc || ' f_gl_acct_mon_by_category/' || p_acct;
  v_sql := '
    update f_gl_acct_mon_by_category ' || v_part || '
    set program_key        = ' || p_pgm_new || ',
      session_id = ''' || v_ses || '''
    where program_key = ' || p_pgm_old || ' 
      and account_key = ' || p_acct || '
  ';
  EXECUTE IMMEDIATE v_sql;
  log4me.debug(v_msg || ' => ' || SQL%ROWCOUNT || ' rows updated', v_sql);

  FOR i IN v_tbl.FIRST..v_tbl.LAST LOOP
    v_msg := p_proc || ' ' || v_tbl(i) || '/' || p_acct;
    v_sql := 'update ' || v_tbl(i) || v_cond;
    EXECUTE IMMEDIATE v_sql;
    log4me.debug(v_msg || ' => ' || SQL%ROWCOUNT || ' rows updated', v_sql);
  END LOOP;

  COMMIT;
  log4me.info('DONE ' || p_proc);

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('FAIL ' || v_msg);
  RAISE;
END XOS_ACCOUNT_AFFINITY_FIX;
/
show errors
