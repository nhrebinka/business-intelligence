-- one-time historical site aggregation
alter session enable parallel dml;
begin
  for n in 1..13 loop
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xos_workspace_mgr.push_all('D_SITE_AGG');
  end loop;
  UPDATE xo_session SET status='HOLD' WHERE status='INIT';
  commit;
  F_DAILY_SITE_AGGREGATES;
  commit;
end;
/
