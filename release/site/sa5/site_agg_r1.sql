--
-- HT287759 - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht_287759.log;

alter session enable parallel dml;
begin
  xbs_site_agg.add_session_for_ym('2014-06');
  xbs_site_agg.run_one_session;
  commit;

  F_DAILY_SITE_AGGREGATES;
  commit;
end;
/
alter index x_d_site_b_ui1 rebuild partition ym_2014_06;

SPOOL OFF


