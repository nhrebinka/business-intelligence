/*
  Name : xbs_site_agg Package
  Spec.: Daily site/program/SIC measurement aggregation

  Package dependency
    log4me
    xos_workspace
    xos_session

  Revision History: 
  1.0 20140318 CC - first try
  1.1 20140320 CC - add seed_date process control using pc_session table
  1.2 20140410 CC - add comments
  1.3 20140422 CC - use F_ACCOUNT_NAICS_SIC_EVENT
  1.4 20140429 CC - commented out non-populated fields,
                    add source_system_name field,
                    add condition to F_DAILY_SITE_SNAPSHOT_VW
  2.0 20140516 CC - enable LRU cache-based daily site agg routines
                    and monthly view dependency recompilation
  2.1 20140804 CC - change format of comments, and revision history
  2.2 20140829 CC - use business_date as auditing definition of "this month"
  2.3 20140911 CC - for manual rebate, use BILLING_ACCOUNT_KEY when
                    PURCHASE_ACCOUNT_KEY=0
*/
-- ============================================================================
CREATE OR REPLACE PACKAGE xbs_site_agg AS
/*
  Daily site/program/SIC measurement aggregation 
   
  This is the aggregation code for daily site aggregation. It utilizes
  workspace_mgm package for storage management and by using the naming 
  convention it aggregate the data onto the "parent" table. The procedures
  comes with a dry-run concept which one can optionally dry run the process
  without actually going through the data set which typically takes a 
  long time. The concept can be expanded into sampling certain percentage
  of the full dataset for quick peek or data profiling.
  
  A typical use case is, per request or schedule, to add new session into
  session list via using session_mgr package and let the workflow control 
  takes it forward through the following states by issuing a 
  run_one_session() call.
  
    INIT - the process has been initialized which means it has picked up
           a valid SITE_AGG session to work on and is currently busy
           paddling through the mud.
    GOOD - the process has finished the main part of data creation, and
           is doing a dbms_stats on the given result set.
    DONE - the stats is done, a new view has been remapped. we should be
           able to use the content safely now
    FAIL - the process has failed for some reason that one needs to look
           into xo_process_log to figure out.
  
  Spec:
    add_session_from_seed() - add a session by peeking into the optional
           seed table default to EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM.
    add_session_for_ym() - add a session with specific year month abbr.
           i.e. '2013-06'.
  
    run_by_date() - a manual run with specific date range into specific
           destination table.
    run_one_session() - pick up a session from the session list to run.
    recompile_dependents() - recompile dependent views after remap
  
  Note:
    1. Column name abbreviation rule used: 
      amount      => amt
      fees        => fee
      maintenance => maint
      transaction => trans
      interchange => intchg

    2. To truncate a specific partion for reloading historic data, do it
       like this:
      alter table x_d_site_b truncate partition ym_2013_06 update indexes;

  Todo:
    1. use an updatable join view with parallel UPDATE to updated rows, 
    2. use an anti-hash join with parallel INSERT to refresh the new rows.
*/

  FMT_MN   constant varchar2(20) := 'YYYY-MM';
  FMT_DT   constant varchar2(20) := 'YYYY-MM-DD';
  --
  MSG_INIT constant varchar2(10) := 'INIT ';
  MSG_GOOD constant varchar2(10) := 'GOOD ';
  MSG_DONE constant varchar2(10) := 'DONE ';
  MSG_FAIL constant varchar2(10) := 'FAIL';
 
  EMPTY_SES constant varchar2(20):= '0.0.0';
  --
  FUNCTION  add_session_from_seed RETURN varchar2;  -- returns ym_abbr
  PROCEDURE add_session_for_ym(
    p_ym_abbr varchar2             -- i.e. '2013-06'
  );
  PROCEDURE run_by_dates (
    p_bgn_dt     varchar2,         -- starting date
    p_end_dt     varchar2,         -- end date (inclusive)
    p_table      varchar2,         -- table to insert into
    p_ses_id     varchar2,         -- pass for logging 
    p_runlevel   integer := 1      -- 0: dryrun, 1: full, 2: sampling, maybe...
  );
  PROCEDURE run_one_session (
    p_runlevel   integer := 1
  );
  PROCEDURE recompile_dependents(
    p_view       varchar2,
    p_ses_id     varchar2 := EMPTY_SES
  );
  PROCEDURE run_daily(p_runlevel integer := 1);
END xbs_site_agg;
/
show errors

-- ============================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY xbs_site_agg AS
  SCHEMA_OWNER constant varchar2(40) := 'EDW_OWNER';
  SESSION_TYPE constant varchar2(40) := 'D_SITE_AGG';
  SOURCE_TB    constant varchar2(80) := 'F_TRANSACTION_LINE_ITEM';
  SEED_TB      constant varchar2(80) := 'EDW_STAGE_OWNER.' || SOURCE_TB;

  TGT_TB       constant varchar2(40) := 'x_d_site_b';
  TGT_VW       constant varchar2(40) := 'f_daily_site_snapshot_vw';

  DRYRUN       constant number       := 0;

-- ---------------------------------------------------------------------------
FUNCTION add_session_from_seed RETURN varchar2 AS
BEGIN
  RETURN xos_session_mgr.add_session_from_seed(
    SESSION_TYPE, SEED_TB, SCHEMA_OWNER || '.' || SOURCE_TB
  );
END add_session_from_seed;

PROCEDURE add_session_for_ym(
  p_ym_abbr varchar2
)
AS
  v_msg varchar2(80) := 'create session ' || SESSION_TYPE || ' for ' || p_ym_abbr;
BEGIN
  log4me.info(v_msg);
  xos_session_mgr.add_session(
    SESSION_TYPE,
    SCHEMA_OWNER || '.' || SOURCE_TB,
    p_ym_abbr
  );
  COMMIT;
END add_session_for_ym;
--
-- core SITE_AGG routine mainly called by higher level drivers
--
-- Note: column name abbriviation rule used: 
--   1. amount => amt
--   2. fees   => fee
--   3. maintenance => maint
--   4. transaction => trans
--   5. interchange => intchg
--
PROCEDURE run_by_dates (
  p_bgn_dt     varchar2,          -- starting date
  p_end_dt     varchar2,          -- end date (inclusive)
  p_table      varchar2,          -- table to insert into
  p_ses_id     varchar2,          -- session_id for logging
  p_runlevel   integer := 1       -- 0: dryrun, 1: full, 2: sampling, maybe...
) 
AS
  v_now   DATE         := SYSDATE;
  v_table varchar2(40) := upper(p_table);  -- some checking should be done here
  v_proc  varchar2(80) := 
    to_char(v_now, FMT_DT) || ' site daily agg. using ' || v_table;

  v_range constant CLOB:= 'CALENDAR_DATE_DT BETWEEN to_date(''' ||
    p_bgn_dt || ''', ''' || FMT_DT || ''') AND to_date(''' ||
    p_end_dt || ''', ''' || FMT_DT || ''')';

  v_sql   constant CLOB := '
  INSERT /*+ append */ INTO ' || v_table || '
  SELECT /*+ star_transformation fact(tx) parallel(tx,64) */
  tx.REVENUE_DATE_KEY,
  tx.POSTING_DATE_KEY,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
  ns.NAICS_KEY,
  ns.SIC_KEY,
  tx.ROW_SOURCE_SYS_NM                   source_system_name,
-- measures
  CAST(COUNT(
    CASE
    WHEN (tx.GROSS_SPEND_AMOUNT<>0 AND tx.WEX_TRANSACTION_ITEM_SEQ_NBR=1)
    THEN tx.WEX_TRANSACTION_ID
    ELSE NULL
    END
  ) AS integer)                          trans_ticket_count,
  CAST(
   SUM(tx.NBR_OF_UNITS_PURCHASED_COUNT) 
   AS integer
  )                                      nbr_of_units_purchased_count,
  CAST(
   SUM(tx.TRANSACTION_LINE_ITEM_COUNT1)
   AS integer
  )                                      trans_line_item_count,
-- gross
  SUM(tx.NET_REVENUE_AMOUNT)             net_revenue_amt,	
  SUM(tx.GROSS_SPEND_AMOUNT)             gross_spend_amt,	
  SUM(tx.GROSS_SPEND_LOCAL_AMOUNT)       gross_spend_local_amt,	
  SUM(tx.GROSS_REVENUE_AMOUNT)           gross_revenue_amt,	
  SUM(tx.GROSS_NON_REVENUE_AMOUNT)       gross_non_revenue_amt,	
  SUM(tx.DISCOUNT_AMOUNT)                gross_discount_amt,
  SUM(tx.PURCHASE_ITEM_AMOUNT)           gross_purchase_item_amt,
  SUM(tx.PURCHASE_ITEM_LOCAL_AMOUNT)     gross_purchase_item_local_amt,
--
-- site does not have the following
--
--  SUM(tx.ALL_OTHER_ANCILLARY_AMOUNT)     gross_all_other_ancillary_amt,
--  SUM(tx.REBATE_AMOUNT)                  gross_rebate_amt,
--  SUM(tx.WAIVED_REVENUE_AMOUNT)          gross_waived_revenue_amt,	
--  SUM(tx.ALL_OTHER_ACILLARY_AMOUNT)      gross_all_other_ancillary_amt,
--  SUM(tx.CARD_FEES_AMOUNT)               fee_card_amt,
--  SUM(tx.FINANCE_FEE_AMOUNT)             fee_finance_amt,	
--  SUM(tx.LATE_FEES_AMOUNT)	             fee_late_amt,
--  SUM(tx.WAIVED_LATE_FEES_AMOUNT)        fee_late_waived_amt,	
--
  SUM(tx.CURRENCY_CONVERSION_FEE_AMOUNT) fee_currency_conversion_amt,	
-- spend fuel
  SUM(tx.SPEND_FUEL_ONLY_AMOUNT)         spend_fuel_only_amt,
  SUM(tx.SPEND_FUEL_DIESEL_AMOUNT)       spend_fuel_diesel_amt,
  SUM(tx.SPEND_FUEL_GAS_AMOUNT)          spend_fuel_gas_amt,
  SUM(tx.SPEND_FUEL_OTHER_AMOUNT)        spend_fuel_other_amt,
  SUM(tx.SPEND_MAINTENANCE_AMOUNT)       spend_maint_amt,
  SUM(tx.SPEND_OTHER_AMOUNT)             spend_other_amt,
-- spend fuel local
  SUM(tx.SPEND_FUEL_ONLY_LOCAL_AMOUNT)   spend_fuel_only_local_amt,
  SUM(tx.SPEND_FUEL_DIESEL_LOCAL_AMOUNT) spend_fuel_diesel_local_amt,
  SUM(tx.SPEND_FUEL_GAS_LOCAL_AMOUNT)    spend_fuel_gas_local_amt,
  SUM(tx.SPEND_FUEL_OTHER_LOCAL_AMOUNT)  spend_fuel_other_local_amt,
  SUM(tx.SPEND_MAINTENANCE_LOCAL_AMOUNT) spend_maint_local_amt,
  SUM(tx.SPEND_OTHER_LOCAL_AMOUNT)       spend_other_local_amt,
-- interchange
  SUM(tx.INTERCHANGE_TOTAL_AMOUNT)       intchg_total_amt,
  SUM(tx.FUEL_ONLY_INTERCHANGE_AMOUNT)   intchg_fuel_only_amt,	
  SUM(tx.INTERCHANGE_FUEL_DIESEL_AMT)    intchg_fuel_diesel_amt,
  SUM(tx.INTERCHANGE_FUEL_GAS_AMT)       intchg_fuel_gas_amt,
  SUM(tx.INTERCHANGE_FUEL_OTHER_AMT)     intchg_fuel_other_amt,
  SUM(tx.MAINTENANCE_ONLY_INTCHG_AMOUNT) intchg_maint_only_amt,
  SUM(tx.ALL_OTHER_INTERCHANGE_AMOUNT)   intchg_all_other_amt,
-- interchange fee
  SUM(tx.INTERCHANGE_RATE_ACTUAL_AMOUNT) intchg_rate_actual_amt,
  SUM(tx.INTCHG_LINE_ITEM_FLAT_FEE_AMT)  intchg_line_item_flat_fee_amt,
  SUM(tx.INTCHG_TRANS_FLAT_FEE_AMT)      intchg_trans_flat_fee_amt,
  SUM(tx.INTERCHANGE_PER_UNIT_FEE_AMT)   intchg_per_unit_fee_amt,
-- tax
  SUM(tx.TOTAL_TAX_AMOUNT)               tax_total_amt,
  SUM(tx.FEDERAL_TAX_AMOUNT)             tax_federal_amt,
  SUM(tx.NON_FEDERAL_TAX_AMOUNT)         tax_non_federal_amt,
  SUM(tx.STATE_TAX_AMOUNT)               tax_state_amt,
  SUM(tx.LOCAL_TAX_AMOUNT)               tax_local_amt,
  SUM(tx.TOTAL_TAX_LOCAL_AMOUNT)         tax_total_local_amt,
-- sales tax
  SUM(tx.STATE_SALES_TAX_AMOUNT)         tax_sales_state_amt,
  SUM(tx.COUNTY_SALES_TAX_AMOUNT)        tax_sales_county_amt,
  SUM(tx.CITY_SALES_TAX_AMOUNT)          tax_sales_city_amt,
-- excise tax
  SUM(tx.FEDERAL_EXCISE_TAX_AMOUNT)      tax_excise_federal_amt,
  SUM(tx.STATE_EXCISE_TAX_AMOUNT)        tax_excise_state_amt,
  SUM(tx.COUNTY_EXCISE_TAX_AMOUNT)       tax_excise_county_amt,
  SUM(tx.CITY_EXCISE_TAX_AMOUNT)         tax_excise_city_amt,
-- special tax
  SUM(tx.STATE_SPECIAL_TAX_AMOUNT)       tax_speical_state_amt,
  SUM(tx.COUNTY_SPECIAL_TAX_AMOUNT)      tax_special_county_amt,
  SUM(tx.CITY_SPECIAL_TAX_AMOUNT)        tax_special_city_amt,
-- exempt
  SUM(tx.TAX_EXEMPT_SPEND_AMOUNT)        tax_exempt_spend_amt,
  SUM(tx.TAX_EXEMPT_SPEND_LOCAL_AMOUNT)  tax_exempt_spend_local_amt,
-- qualtity gallons
  SUM(tx.PURCHASE_GALLONS_QTY)           purchase_gallons_qty,
  SUM(tx.FUEL_GAS_GALLONS_QTY)           fuel_gas_gallons_qty,
  SUM(tx.FUEL_DIESEL_GALLONS_QTY)        fuel_diesel_gallons_qty,
  SUM(tx.FUEL_OTHER_GALLONS_QTY)         fuel_other_gallons_qty,
  SUM(tx.PRIVATE_SITE_GALLONS_QTY)       private_site_gallons_qty,
-- qualtity liters
  SUM(tx.PURCHASE_LITRES_QTY)            purchase_litres_qty,
  SUM(tx.FUEL_GAS_LITRES_QTY)            fuel_gas_litres_qty,
  SUM(tx.FUEL_DIESEL_LITRES_QTY)         fuel_diesel_litres_qty,
  SUM(tx.FUEL_OTHER_LITRES_QTY)          fuel_other_litres_qty,
  SUM(tx.PRIVATE_SITE_LITRES_QTY)        private_site_litres_qty,
-- session_id YYYYMMDD.HH24MISS.PID
  ''' || p_ses_id || '''                 session_id,
  SUM(tx.MANUAL_REBATE_AMOUNT)           manual_rebate_amt
  FROM F_TRANSACTION_LINE_ITEM tx
  JOIN D_DATE    dt ON (tx.REVENUE_DATE_KEY = dt.DATE_KEY)
  JOIN D_ACCOUNT ac ON (ac.ACCOUNT_KEY = 
    CASE WHEN tx.PURCHASE_ACCOUNT_KEY = 0 
    THEN tx.BILLING_ACCOUNT_KEY ELSE tx.PURCHASE_ACCOUNT_KEY
    END
  )
-- get naics and sic keys
  JOIN (
    SELECT 
      ACCOUNT_HIST_KEY, 
      NAICS_KEY,
      SIC_KEY,
      ROW_LAST_MOD_DTTM,
      max(ROW_LAST_MOD_DTTM) OVER (PARTITION BY ACCOUNT_HIST_KEY) last_mod
    FROM F_ACCOUNT_NAICS_SIC_EVENT
  ) ns ON (1=1
    AND ns.ROW_LAST_MOD_DTTM = ns.last_mod
    AND ns.ACCOUNT_HIST_KEY  = ac.ACCOUNT_HIST_KEY
  )
  WHERE ' || v_range || '
    AND 1=' || p_runlevel || '  -- 0: can creates table schema without data
  GROUP BY
    tx.REVENUE_DATE_KEY,
    tx.POSTING_DATE_KEY,
    tx.PROGRAM_KEY,
    tx.POS_AND_SITE_KEY,
    ns.NAICS_KEY,
    ns.SIC_KEY,
    tx.ROW_SOURCE_SYS_NM
  ';

BEGIN
  log4me.info(MSG_INIT || v_proc, p_ses_id);

  if (p_runlevel = DRYRUN) then 
    dbms_output.put_line(v_sql);
  else 
    EXECUTE IMMEDIATE v_sql;
    --
    -- direct path: no query before commit (wrote to tmp first)
    --
    COMMIT;
  end if;
  log4me.info(MSG_GOOD || v_proc, p_ses_id);
  --
  -- stat it (takes about 20 seconds)
  --
  if (p_runlevel <> DRYRUN) then
    dbms_stats.gather_table_stats(SCHEMA_OWNER, v_table);
  end if;
  log4me.info(MSG_DONE || v_proc, p_ses_id);

EXCEPTION
  WHEN OTHERS THEN
    --
    -- because we use NOLOGGING, no rolling back is needed, always forward
    -- however, if exception is caused by connection lost (or timeout)
    -- any commit after that is causing another exception, i.e. no logging
    -- after that. We need to be able to register that in the log.
    -- 
    COMMIT;
    log4me.err(MSG_FAIL || v_proc, p_ses_id);
    RAISE;
END run_by_dates;
--
-- run by entry in xos_session table
--
PROCEDURE run_one_session (
  p_runlevel   integer := 1
)
AS
  PROC_NAME constant varchar2(40) := 'SITE_AGG';
  VIEW_NAME constant varchar2(40) := 'F_DAILY_SITE_SNAPSHOT_VW';
  VIEW_COND constant varchar2(200):= '
    WHERE SOURCE_SYSTEM_NAME in (''TANDEM'', ''TP/CP'')
  ';
  v_now        varchar2(30) := to_char(sysdate, FMT_DT);
  v_workspace  varchar2(40) := NULL;
  v_ses_id     varchar2(40) := NULL;
  v_ym         varchar2(30);
  v_bgn_dt     varchar2(30);
  v_end_dt     varchar2(30);
  v_msg        CLOB;
  v_sql        CLOB;
  v_count      integer;  
  v_start      date;     -- be very careful with time calculator in Oracle
  v_elapsed    integer;  -- in seconds
BEGIN
  -- -------------------------------------------------------
  -- Processes:
  --   1. find next work session from the xos_session table
  --   2. allocate a destination table from workspace
  --   3. gather the incoming data into 'this' staging
  --   4. remap the view as final+'this'
  --   4. remap the view as final+'this'
  -- -------------------------------------------------------
  --
  -- find next available session to work on
  --
  xos_session_mgr.open(
    SESSION_TYPE, 
    PROC_NAME, 
    v_ses_id, v_ym, v_bgn_dt, v_end_dt
  );
  if (v_ses_id is NULL) then RETURN; end if; -- no work, bail out
  --
  -- get the workspace to store the resultant data set, 
  --
  xos_workspace_mgr.alloc(SESSION_TYPE, v_ses_id, v_workspace);

  if (v_workspace is NULL) then
    -- no workspace left or wrong type and logged in xos_process_log
    v_msg := 'no workspace availabe for process';
    raise STORAGE_ERROR;
  end if;
  -- 
  -- conditional logging
  --
  v_msg := v_now || 
      ' t='   || SESSION_TYPE || 
      ' sid=' || v_ses_id || 
      '[' || v_bgn_dt || ', ' || v_end_dt || '] using ' ||
      v_workspace;
  log4me.info(v_msg, v_ses_id);
  --
  -- now comes the real work, with benchmark
  --
  v_start := sysdate;
  BEGIN
    run_by_dates(
      v_bgn_dt, 
      v_end_dt, 
      v_workspace, 
      v_ses_id,
      p_runlevel
    );
    xos_workspace_mgr.mark(
      v_workspace, 
      xos_workspace_mgr.SPACE_USED,
      xos_workspace_mgr.CLASS_WARM,
      v_ses_id
    );
  EXCEPTION
    WHEN OTHERS THEN
      --
      -- Because we use NOLOGGING and many logging in-between, rolling back
      -- is not possible. workspace manager could have inconsistant states
      -- external process many needed to be scheduled to validate the 
      -- workspace state consistancy
      --
      COMMIT;
      xos_workspace_mgr.free(v_workspace, v_ses_id);
      RAISE;
  END;
  SELECT 86400*(sysdate - v_start) INTO v_elapsed FROM dual;
  --
  -- collect process statistics, close the session with success
  --
  v_sql := 'SELECT /*+ parallel */ count(*) FROM ' || v_workspace;
  log4me.debug(v_msg, v_sql, v_ses_id);
  EXECUTE IMMEDIATE v_sql INTO v_count;
  --
  -- no exception! almost done, just remap view to include the new data
  --
  xos_workspace_mgr.remap(SESSION_TYPE, VIEW_NAME, VIEW_COND, v_ses_id);
  xbs_site_agg.recompile_dependents(VIEW_NAME, v_ses_id);
  xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_DONE, v_count, v_elapsed);
  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    --
    -- Stop propergating the exception so we can roll forward.
    --
    log4me.err(MSG_FAIL || v_msg, v_ses_id);
    -- 
    -- close the session with failure, need to be resurrected
    --
    if (v_ses_id is not NULL) then
      xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_FAIL, -1, -1);
    end if;
    --
    -- No RAISE again here!
    --
    -- To prevent store_procedure failure from propergating and stopping 
    -- any other consequent job from going forward, we use a soft error 
    -- handler here, i.e. session management, instead of breaking the whole 
    -- process steam. That also means we need to have an external monitoring 
    -- tool to validate the success of the process.
    --
END run_one_session;

PROCEDURE recompile_dependents(
  p_view   varchar2,
  p_ses_id varchar2 := EMPTY_SES
)
AS
  v_sql constant CLOB := '
    SELECT name FROM USER_DEPENDENCIES
    WHERE  1=1
    AND    TYPE = ''VIEW''
    AND    REFERENCED_TYPE = ''VIEW''
    AND    REFERENCED_NAME = ''' || p_view || '''
    AND    DEPENDENCY_TYPE = ''HARD''';
  v_msg  varchar2(200) := '';
  v_proc varchar2(200) := '';
  v_dept varchar2(80);
  TYPE VCUR  IS REF CURSOR;
  c_ws VCUR;
BEGIN
  -- --------------------------------------------------------
  -- Due to daily view been replaced every session run,
  -- the dependent views are invalidated. It is required
  -- to recompile the dependent views before the user can
  -- access to the newly updated content.
  -- It is better to use workspace_mgr to do the update
  -- but we do not have enough time to do that within the 
  -- release timeframe. So, backlog (aka 'TODO' list) entered.
  -- --------------------------------------------------------
  OPEN c_ws FOR v_sql;
  LOOP
    FETCH c_ws INTO v_dept;
    EXIT WHEN c_ws%NOTFOUND;
    v_msg  := 'dependent view ' || v_dept || ' recompiled';
    v_proc := 'ALTER VIEW ' || v_dept || ' COMPILE';
    EXECUTE IMMEDIATE v_proc;
    log4me.info(v_msg, p_ses_id);
  END LOOP;
  CLOSE c_ws;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc, p_ses_id);
    if c_ws%ISOPEN then CLOSE c_ws; end if;
    RAISE;
END recompile_dependents;

PROCEDURE run_daily(p_runlevel integer := 1) 
AS
  v_last_month constant varchar2(20) := to_char(sysdate-7, FMT_MN);

  v_this_month varchar2(20);
  v_ses_id     varchar2(20);
BEGIN
  --
  -- 1. take seed dates from EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM table,
  --    create a entry in xo_session table
  -- 2. mark a cached result from previous run stale
  -- 3. execute the session to create the daily result, including
  --    rebuild the daily view and monthly view.
  --
  v_this_month := xbs_site_agg.add_session_from_seed;
  if (v_last_month < v_this_month) then
    -- 
    -- get any session that has been run for this month
    --
    xos_session_mgr.previous_session(SESSION_TYPE, v_this_month, v_ses_id);
    --
    -- If this is the first time ever been run for the month,
    -- we put the run from last month to AUDIT state.
    --
    if (v_ses_id IS NULL) then
        xos_session_mgr.previous_session(SESSION_TYPE, v_last_month, v_ses_id);
        xos_workspace_mgr.audit_by_session(v_ses_id);
    else
      log4me.debug('session exists for ' || v_this_month || ': ' || v_ses_id);
    end if;
  end if;
  -- 
  -- We mark the output from previous session STALE first before we start
  -- processing. Should the process failed, the view is still pointing to
  -- the STALE but still valid data. The view will be recompiled upon next
  -- successful run.
  --
  xos_workspace_mgr.retire(SESSION_TYPE, v_ses_id);
  xbs_site_agg.run_one_session(p_runlevel);
END run_daily;

END xbs_site_agg;
/
show errors

-- ============================================================================
-- ETL store procedure interface
--
CREATE OR REPLACE PROCEDURE F_DAILY_SITE_AGGREGATES AS
BEGIN
  -- Daily site aggregate proc - calls underneath xbs_site_agg package
  -- and perform the following steps:
  --
  -- 1. take seed dates from EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM table,
  --    create a entry in xo_session table
  -- 2. mark a cached result from previous run stale
  -- 3. execute the session to create the daily result, including
  --    rebuild the daily view and monthly view.
  --
  xbs_site_agg.run_daily;
END F_DAILY_SITE_AGGREGATES;
/
show errors

GRANT EXECUTE ON F_DAILY_SITE_AGGREGATES TO DWLOADER;





