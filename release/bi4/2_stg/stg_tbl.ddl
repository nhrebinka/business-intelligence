-- 
-- TABLE: F_GL_ACCT_DAILY_SNAPSHOT for EDW_STAGE_OWNER
--
ALTER TABLE F_TRANSACTION_LINE_ITEM
ADD MANUAL_REBATE_AMOUNT NUMBER(20, 2)
;

CREATE TABLE F_GL_ACCT_DAILY_SNAPSHOT
(
    GL_ACCT_DAILY_SNAPSHOT_KEY   INTEGER          NOT NULL,
    REVENUE_DATE_KEY             INTEGER          NOT NULL,
    BUSINESS_SECTOR_KEY          INTEGER          NOT NULL,
    BUSINESS_SEGMENT_KEY         INTEGER          NOT NULL,
    FR_CATEGORY_KEY              INTEGER          NOT NULL,
    COST_CENTER_KEY              INTEGER          NOT NULL,
    PROGRAM_KEY                  INTEGER          NOT NULL,
    ACCOUNT_KEY                  INTEGER          NOT NULL,
    ACCOUNT_HIST_KEY             INTEGER          NOT NULL,
    AMOUNT                       NUMBER(20,2),
    DEPRECIATION_AMOUNT          NUMBER(20,2),
    SNAPSHOT_DATE_KEY            INTEGER            NOT NULL,
    ROW_CREATE_DTTM              TIMESTAMP(6)       NOT NULL,
    ROW_LAST_MOD_DTTM            TIMESTAMP(6)       NOT NULL,
    ROW_LAST_MOD_PROC_NM         VARCHAR2(200 BYTE) NOT NULL,
    ROW_LAST_MOD_PROC_SEQ_NBR    INTEGER            NOT NULL
)
TABLESPACE D_F_TRANS_LINE_ITEM
NOLOGGING
COMPRESS FOR ALL OPERATIONS
NOCACHE
PARALLEL (DEGREE 16 INSTANCES 1)
;



--
-- Wex BI4.0 - DDL for Excel-based source external tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_SOURCE_MKT_CHANNEL
(
    NAME                   varchar2(100)          NOT NULL,
    VERSION                varchar2(100)          NOT NULL,
    SOURCE_EXCEL_TYPE      varchar2(100)          NOT NULL,
    START_DATE             date                   NOT NULL,
    END_DATE               date                   NOT NULL,
    MKT_PARTNER_ID         varchar2(200)          NOT NULL,
    MKT_PARTNER_SUB_CHL_ID varchar2(200)          NOT NULL,
    MKT_SUB_CHL_ALLOCATION number(20,2)           NOT NULL
)
TABLESPACE D_BI
;

CREATE TABLE SRC_SOURCE_BUSINESS_SEGMENT
(
    NAME                        varchar2(100)     NOT NULL,
    VERSION                     varchar2(100)     NOT NULL,
    SOURCE_EXCEL_TYPE           varchar2(100)     NOT NULL,
    START_DATE                  date              NOT NULL,
    END_DATE                    date              NOT NULL,
    COST_CENTER_ID              varchar2(200)     NOT NULL,
    COST_CENTER_NAME            varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ID         varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ALLOCATION number(20,2)      NOT NULL
)
TABLESPACE D_BI
;

