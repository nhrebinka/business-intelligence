--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- BI40 HUB.STAGE_OWNER one time setup
-- Also, ensure DBA copy the PS_* tables from PRPS88
@src_drp.ddl
@src_tbl.ddl
@src_gnt.ddl
