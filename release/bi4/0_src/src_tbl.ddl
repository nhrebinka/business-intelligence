-- ------------------------------------------------------------------------
-- !!! For Historical Load: Ask DBA to populate the following from PRPS88
--
-- Wex BI4.0 - DDL for HUB PeopleSoft source tables for historical load
--
-- ------------------------------------------------------------------------
CREATE TABLE PS_DEPT_TBL
(
  SETID              VARCHAR2(5 BYTE)           NOT NULL,
  DEPTID             VARCHAR2(10 BYTE)          NOT NULL,
  EFFDT              DATE                       NOT NULL,
  MANAGER_NAME       VARCHAR2(30 BYTE)          NOT NULL,
  ACCOUNTING_OWNER   VARCHAR2(30 BYTE)          NOT NULL,
  COUNTRY_GRP        VARCHAR2(18 BYTE)          NOT NULL,
  BUDGETARY_ONLY     VARCHAR2(1 BYTE)           NOT NULL,
  SYNCID             INTEGER,
  SYNCDTTM           DATE,
  EFF_STATUS         VARCHAR2(1 BYTE)           NOT NULL,
  DESCR              VARCHAR2(30 BYTE)          NOT NULL,
  DESCRSHORT         VARCHAR2(10 BYTE)          NOT NULL,
  COMPANY            VARCHAR2(3 BYTE)           NOT NULL,
  SETID_LOCATION     VARCHAR2(5 BYTE)           NOT NULL,
  LOCATION           VARCHAR2(10 BYTE)          NOT NULL,
  TAX_LOCATION_CD    VARCHAR2(10 BYTE)          NOT NULL,
  MANAGER_ID         VARCHAR2(11 BYTE)          NOT NULL,
  MANAGER_POSN       VARCHAR2(8 BYTE)           NOT NULL,
  BUDGET_YR_END_DT   INTEGER                    NOT NULL,
  BUDGET_LVL         VARCHAR2(1 BYTE)           NOT NULL,
  GL_EXPENSE         VARCHAR2(35 BYTE)          NOT NULL,
  EEO4_FUNCTION      VARCHAR2(2 BYTE)           NOT NULL,
  CAN_IND_SECTOR     VARCHAR2(3 BYTE)           NOT NULL,
  ACCIDENT_INS       VARCHAR2(3 BYTE)           NOT NULL,
  SI_ACCIDENT_NUM    VARCHAR2(15 BYTE)          NOT NULL,
  HAZARD             VARCHAR2(4 BYTE)           NOT NULL,
  ESTABID            VARCHAR2(12 BYTE)          NOT NULL,
  RISKCD             VARCHAR2(6 BYTE)           NOT NULL,
  GVT_DESCR40        VARCHAR2(40 BYTE)          NOT NULL,
  GVT_SUB_AGENCY     VARCHAR2(2 BYTE)           NOT NULL,
  GVT_PAR_LINE2      VARCHAR2(10 BYTE)          NOT NULL,
  GVT_PAR_LINE3      VARCHAR2(10 BYTE)          NOT NULL,
  GVT_PAR_LINE4      VARCHAR2(10 BYTE)          NOT NULL,
  GVT_PAR_LINE5      VARCHAR2(10 BYTE)          NOT NULL,
  GVT_PAR_DESCR2     VARCHAR2(40 BYTE)          NOT NULL,
  GVT_PAR_DESCR3     VARCHAR2(40 BYTE)          NOT NULL,
  GVT_PAR_DESCR4     VARCHAR2(40 BYTE)          NOT NULL,
  GVT_PAR_DESCR5     VARCHAR2(40 BYTE)          NOT NULL,
  CLASS_UNIT_NZL     VARCHAR2(5 BYTE)           NOT NULL,
  ORG_UNIT_AUS       VARCHAR2(4 BYTE)           NOT NULL,
  WORK_SECTOR_AUS    VARCHAR2(1 BYTE)           NOT NULL,
  APS_AGENT_CD_AUS   INTEGER                    NOT NULL,
  IND_COMMITTEE_BEL  VARCHAR2(3 BYTE)           NOT NULL,
  NACE_CD_BEL        VARCHAR2(10 BYTE)          NOT NULL,
  FTE_EDIT_INDC      VARCHAR2(1 BYTE)           NOT NULL,
  DEPT_TENURE_FLG    VARCHAR2(1 BYTE)           NOT NULL,
  TL_DISTRIB_INFO    VARCHAR2(1 BYTE)           NOT NULL,
  USE_BUDGETS        VARCHAR2(1 BYTE)           NOT NULL,
  USE_ENCUMBRANCES   VARCHAR2(1 BYTE)           NOT NULL,
  USE_DISTRIBUTION   VARCHAR2(1 BYTE)           NOT NULL,
  BUDGET_DEPTID      VARCHAR2(10 BYTE)          NOT NULL,
  DIST_PRORATE_OPTN  VARCHAR2(1 BYTE)           NOT NULL,
  HP_STATS_DEPT_CD   VARCHAR2(3 BYTE)           NOT NULL,
  HP_STATS_FACULTY   VARCHAR2(5 BYTE)           NOT NULL
)
TABLESPACE D_BI
;

CREATE TABLE PS_GL_ACCOUNT_TBL
(
  SETID               VARCHAR2(5 BYTE)          NOT NULL,
  ACCOUNT             VARCHAR2(10 BYTE)         NOT NULL,
  EFFDT               DATE                      NOT NULL,
  DESCR               VARCHAR2(30 BYTE)         NOT NULL,
  ACCOUNT_TYPE        VARCHAR2(1 BYTE)          NOT NULL,
  EFF_STATUS          VARCHAR2(1 BYTE)          NOT NULL,
  DESCRSHORT          VARCHAR2(10 BYTE)         NOT NULL,
  BUDGETARY_ONLY      VARCHAR2(1 BYTE)          NOT NULL,
  SYNCID              INTEGER,
  SYNCDTTM            DATE,
  BUDG_OVERRIDE_ACCT  VARCHAR2(1 BYTE)          NOT NULL,
  ACCOUNTING_OWNER    VARCHAR2(30 BYTE)         NOT NULL,
  AB_ACCOUNT_SW       VARCHAR2(1 BYTE)          NOT NULL,
  GL_ACCOUNT_SW       VARCHAR2(1 BYTE)          NOT NULL,
  PF_ACCOUNT_SW       VARCHAR2(1 BYTE)          NOT NULL,
  UNIT_OF_MEASURE     VARCHAR2(3 BYTE)          NOT NULL,
  OPEN_ITEM           VARCHAR2(1 BYTE)          NOT NULL,
  OPEN_ITEM_DESCR     VARCHAR2(10 BYTE)         NOT NULL,
  OPEN_ITEM_EDIT_REC  VARCHAR2(15 BYTE)         NOT NULL,
  OPEN_ITEM_EDIT_FLD  VARCHAR2(18 BYTE)         NOT NULL,
  OPEN_ITEM_PROMPT    VARCHAR2(15 BYTE)         NOT NULL,
  OPEN_ITEM_TOL_AMT   NUMBER(26,3)              NOT NULL,
  CURRENCY_CD         VARCHAR2(3 BYTE)          NOT NULL,
  STATISTICS_ACCOUNT  VARCHAR2(1 BYTE)          NOT NULL,
  BALANCE_FWD_SW      VARCHAR2(1 BYTE)          NOT NULL,
  CONTROL_FLAG        VARCHAR2(1 BYTE)          NOT NULL,
  BOOK_CODE           VARCHAR2(4 BYTE)          NOT NULL,
  BOOK_CODE_OVERRIDE  VARCHAR2(1 BYTE)          NOT NULL,
  BAL_SHEET_IND       VARCHAR2(2 BYTE)          NOT NULL,
  VAT_ACCOUNT_FLG     VARCHAR2(1 BYTE)          NOT NULL,
  PHYSICAL_NATURE     VARCHAR2(1 BYTE)          NOT NULL
)
TABLESPACE D_BI
;

CREATE TABLE PS_LEDGER
(
  BUSINESS_UNIT      VARCHAR2(5 BYTE)           NOT NULL,
  LEDGER             VARCHAR2(10 BYTE)          NOT NULL,
  ACCOUNT            VARCHAR2(10 BYTE)          NOT NULL,
  ALTACCT            VARCHAR2(10 BYTE)          NOT NULL,
  DEPTID             VARCHAR2(10 BYTE)          NOT NULL,
  OPERATING_UNIT     VARCHAR2(8 BYTE)           NOT NULL,
  PRODUCT            VARCHAR2(6 BYTE)           NOT NULL,
  FUND_CODE          VARCHAR2(5 BYTE)           NOT NULL,
  CLASS_FLD          VARCHAR2(5 BYTE)           NOT NULL,
  PROGRAM_CODE       VARCHAR2(5 BYTE)           NOT NULL,
  BUDGET_REF         VARCHAR2(8 BYTE)           NOT NULL,
  AFFILIATE          VARCHAR2(5 BYTE)           NOT NULL,
  AFFILIATE_INTRA1   VARCHAR2(10 BYTE)          NOT NULL,
  AFFILIATE_INTRA2   VARCHAR2(10 BYTE)          NOT NULL,
  CHARTFIELD1        VARCHAR2(10 BYTE)          NOT NULL,
  CHARTFIELD2        VARCHAR2(10 BYTE)          NOT NULL,
  CHARTFIELD3        VARCHAR2(10 BYTE)          NOT NULL,
  PROJECT_ID         VARCHAR2(15 BYTE)          NOT NULL,
  BOOK_CODE          VARCHAR2(4 BYTE)           NOT NULL,
  GL_ADJUST_TYPE     VARCHAR2(4 BYTE)           NOT NULL,
  CURRENCY_CD        VARCHAR2(3 BYTE)           NOT NULL,
  STATISTICS_CODE    VARCHAR2(3 BYTE)           NOT NULL,
  FISCAL_YEAR        INTEGER                    NOT NULL,
  ACCOUNTING_PERIOD  INTEGER                    NOT NULL,
  POSTED_TOTAL_AMT   NUMBER(26,3)               NOT NULL,
  POSTED_BASE_AMT    NUMBER(26,3)               NOT NULL,
  POSTED_TRAN_AMT    NUMBER(26,3)               NOT NULL,
  BASE_CURRENCY      VARCHAR2(3 BYTE)           NOT NULL,
  DTTM_STAMP_SEC     DATE,
  PROCESS_INSTANCE   NUMBER(10)                 NOT NULL
)
TABLESPACE D_BI
;
-- ------------------------------------------------------------------------
--
-- Wex BI4.0 - Since Date Lookup table
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_ACCT_SINCE_DATE_LOOKUP
(
    CUR_WEX_ACCT_NBR       varchar2(200),
    CUR_ACTIV_DT           date,
    LEGACY_ACTIV_DT        date,
    LEGACY_WEX_ACCT_NBR    varchar2(200),
    SINCE_DATE             date,
    CONVERSION_RULE_CD     varchar2(200),
    CONFIDENCE_LEVEL       number,
    EDM_ACCT_KEY           number,
    SOURCE_SYS_NM          varchar2(200),
    SOURCE_TABLE_NM        varchar2(200),
    UPDATED_CD             varchar2(200),
    UPDATED_AT             timestamp
)
TABLESPACE D_BI
;
-- ------------------------------------------------------------------------
--
-- Wex BI4.0 - DDL for HUB PeopleSoft external source tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_PS_GL_DEPT_TBL
(
  SETID              VARCHAR2(5 BYTE)           NULL,
  DEPTID             VARCHAR2(10 BYTE)          NULL,
  EFFDT              DATE                       NULL,
  MANAGER_NAME       VARCHAR2(30 BYTE)          NULL,
  ACCOUNTING_OWNER   VARCHAR2(30 BYTE)          NULL,
  COUNTRY_GRP        VARCHAR2(18 BYTE)          NULL,
  BUDGETARY_ONLY     VARCHAR2(1 BYTE)           NULL,
  SYNCID             INTEGER                    NULL,
  SYNCDTTM           DATE                       NULL,
  EFF_STATUS         VARCHAR2(1 BYTE)           NULL,
  DESCR              VARCHAR2(30 BYTE)          NULL,
  DESCRSHORT         VARCHAR2(10 BYTE)          NULL,
  COMPANY            VARCHAR2(3 BYTE)           NULL,
  SETID_LOCATION     VARCHAR2(5 BYTE)           NULL,
  LOCATION           VARCHAR2(10 BYTE)          NULL,
  TAX_LOCATION_CD    VARCHAR2(10 BYTE)          NULL,
  MANAGER_ID         VARCHAR2(11 BYTE)          NULL,
  MANAGER_POSN       VARCHAR2(8 BYTE)           NULL,
  BUDGET_YR_END_DT   INTEGER                    NULL,
  BUDGET_LVL         VARCHAR2(1 BYTE)           NULL,
  GL_EXPENSE         VARCHAR2(35 BYTE)          NULL,
  EEO4_FUNCTION      VARCHAR2(2 BYTE)           NULL,
  CAN_IND_SECTOR     VARCHAR2(3 BYTE)           NULL,
  ACCIDENT_INS       VARCHAR2(3 BYTE)           NULL,
  SI_ACCIDENT_NUM    VARCHAR2(15 BYTE)          NULL,
  HAZARD             VARCHAR2(4 BYTE)           NULL,
  ESTABID            VARCHAR2(12 BYTE)          NULL,
  RISKCD             VARCHAR2(6 BYTE)           NULL,
  GVT_DESCR40        VARCHAR2(40 BYTE)          NULL,
  GVT_SUB_AGENCY     VARCHAR2(2 BYTE)           NULL,
  GVT_PAR_LINE2      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE3      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE4      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_LINE5      VARCHAR2(10 BYTE)          NULL,
  GVT_PAR_DESCR2     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR3     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR4     VARCHAR2(40 BYTE)          NULL,
  GVT_PAR_DESCR5     VARCHAR2(40 BYTE)          NULL,
  CLASS_UNIT_NZL     VARCHAR2(5 BYTE)           NULL,
  ORG_UNIT_AUS       VARCHAR2(4 BYTE)           NULL,
  WORK_SECTOR_AUS    VARCHAR2(1 BYTE)           NULL,
  APS_AGENT_CD_AUS   INTEGER                    NULL,
  IND_COMMITTEE_BEL  VARCHAR2(3 BYTE)           NULL,
  NACE_CD_BEL        VARCHAR2(10 BYTE)          NULL,
  FTE_EDIT_INDC      VARCHAR2(1 BYTE)           NULL,
  DEPT_TENURE_FLG    VARCHAR2(1 BYTE)           NULL,
  TL_DISTRIB_INFO    VARCHAR2(1 BYTE)           NULL,
  USE_BUDGETS        VARCHAR2(1 BYTE)           NULL,
  USE_ENCUMBRANCES   VARCHAR2(1 BYTE)           NULL,
  USE_DISTRIBUTION   VARCHAR2(1 BYTE)           NULL,
  BUDGET_DEPTID      VARCHAR2(10 BYTE)          NULL,
  DIST_PRORATE_OPTN  VARCHAR2(1 BYTE)           NULL,
  HP_STATS_DEPT_CD   VARCHAR2(3 BYTE)           NULL,
  HP_STATS_FACULTY   VARCHAR2(5 BYTE)           NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_dept_tbl.log'
    BADFILE      edm_log_dir:'src_ps_gl_dept_tbl.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_dept_tbl.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      SETID,
      DEPTID,
      EFFDT    CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      MANAGER_NAME,
      ACCOUNTING_OWNER,
      COUNTRY_GRP,
      BUDGETARY_ONLY,
      SYNCID,
      SYNCDTTM CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      EFF_STATUS,
      DESCR,
      DESCRSHORT,
      COMPANY,
      SETID_LOCATION,
      LOCATION,
      TAX_LOCATION_CD,
      MANAGER_ID,
      MANAGER_POSN,
      BUDGET_YR_END_DT,
      BUDGET_LVL,
      GL_EXPENSE,
      EEO4_FUNCTION,
      CAN_IND_SECTOR,
      ACCIDENT_INS,
      SI_ACCIDENT_NUM,
      HAZARD,
      ESTABID,
      RISKCD,
      GVT_DESCR40,
      GVT_SUB_AGENCY,
      GVT_PAR_LINE2,
      GVT_PAR_LINE3,
      GVT_PAR_LINE4,
      GVT_PAR_LINE5,
      GVT_PAR_DESCR2,
      GVT_PAR_DESCR3,
      GVT_PAR_DESCR4,
      GVT_PAR_DESCR5,
      CLASS_UNIT_NZL,
      ORG_UNIT_AUS,
      WORK_SECTOR_AUS,
      APS_AGENT_CD_AUS,
      IND_COMMITTEE_BEL,
      NACE_CD_BEL,
      FTE_EDIT_INDC,
      DEPT_TENURE_FLG,
      TL_DISTRIB_INFO,
      USE_BUDGETS,
      USE_ENCUMBRANCES,
      USE_DISTRIBUTION,
      BUDGET_DEPTID,
      DIST_PRORATE_OPTN,
      HP_STATS_DEPT_CD,
      HP_STATS_FACULTY
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_dept_tbl.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_PS_GL_ACCOUNT_TBL
(
  SETID               VARCHAR2(5 BYTE)          NULL,
  ACCOUNT             VARCHAR2(10 BYTE)         NULL,
  EFFDT               DATE                      NULL,
  DESCR               VARCHAR2(30 BYTE)         NULL,
  ACCOUNT_TYPE        VARCHAR2(1 BYTE)          NULL,
  EFF_STATUS          VARCHAR2(1 BYTE)          NULL,
  DESCRSHORT          VARCHAR2(10 BYTE)         NULL,
  BUDGETARY_ONLY      VARCHAR2(1 BYTE)          NULL,
  SYNCID              INTEGER                   NULL,
  SYNCDTTM            DATE                      NULL,
  BUDG_OVERRIDE_ACCT  VARCHAR2(1 BYTE)          NULL,
  ACCOUNTING_OWNER    VARCHAR2(30 BYTE)         NULL,
  AB_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  GL_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  PF_ACCOUNT_SW       VARCHAR2(1 BYTE)          NULL,
  UNIT_OF_MEASURE     VARCHAR2(3 BYTE)          NULL,
  OPEN_ITEM           VARCHAR2(1 BYTE)          NULL,
  OPEN_ITEM_DESCR     VARCHAR2(10 BYTE)         NULL,
  OPEN_ITEM_EDIT_REC  VARCHAR2(15 BYTE)         NULL,
  OPEN_ITEM_EDIT_FLD  VARCHAR2(18 BYTE)         NULL,
  OPEN_ITEM_PROMPT    VARCHAR2(15 BYTE)         NULL,
  OPEN_ITEM_TOL_AMT   NUMBER(26,3)              NULL,
  CURRENCY_CD         VARCHAR2(3 BYTE)          NULL,
  STATISTICS_ACCOUNT  VARCHAR2(1 BYTE)          NULL,
  BALANCE_FWD_SW      VARCHAR2(1 BYTE)          NULL,
  CONTROL_FLAG        VARCHAR2(1 BYTE)          NULL,
  BOOK_CODE           VARCHAR2(4 BYTE)          NULL,
  BOOK_CODE_OVERRIDE  VARCHAR2(1 BYTE)          NULL,
  BAL_SHEET_IND       VARCHAR2(2 BYTE)          NULL,
  VAT_ACCOUNT_FLG     VARCHAR2(1 BYTE)          NULL,
  PHYSICAL_NATURE     VARCHAR2(1 BYTE)          NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_account_tbl.log'
    BADFILE      edm_log_dir:'src_ps_gl_account_tbl.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_account_tbl.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      SETID,
      ACCOUNT,
      EFFDT    CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      DESCR,
      ACCOUNT_TYPE,
      EFF_STATUS,
      DESCRSHORT,
      BUDGETARY_ONLY,
      SYNCID,
      SYNCDTTM CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      BUDG_OVERRIDE_ACCT,
      ACCOUNTING_OWNER,
      AB_ACCOUNT_SW,
      GL_ACCOUNT_SW,
      PF_ACCOUNT_SW,
      UNIT_OF_MEASURE,
      OPEN_ITEM,
      OPEN_ITEM_DESCR,
      OPEN_ITEM_EDIT_REC,
      OPEN_ITEM_EDIT_FLD,
      OPEN_ITEM_PROMPT,
      OPEN_ITEM_TOL_AMT,
      CURRENCY_CD,
      STATISTICS_ACCOUNT,
      BALANCE_FWD_SW,
      CONTROL_FLAG,
      BOOK_CODE,
      BOOK_CODE_OVERRIDE,
      BAL_SHEET_IND,
      VAT_ACCOUNT_FLG,
      PHYSICAL_NATURE
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_account_tbl.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_PS_GL_LEDGER
(
  BUSINESS_UNIT      VARCHAR2(5 BYTE)           NULL,
  LEDGER             VARCHAR2(10 BYTE)          NULL,
  ACCOUNT            VARCHAR2(10 BYTE)          NULL,
  ALTACCT            VARCHAR2(10 BYTE)          NULL,
  DEPTID             VARCHAR2(10 BYTE)          NULL,
  OPERATING_UNIT     VARCHAR2(8 BYTE)           NULL,
  PRODUCT            VARCHAR2(6 BYTE)           NULL,
  FUND_CODE          VARCHAR2(5 BYTE)           NULL,
  CLASS_FLD          VARCHAR2(5 BYTE)           NULL,
  PROGRAM_CODE       VARCHAR2(5 BYTE)           NULL,
  BUDGET_REF         VARCHAR2(8 BYTE)           NULL,
  AFFILIATE          VARCHAR2(5 BYTE)           NULL,
  AFFILIATE_INTRA1   VARCHAR2(10 BYTE)          NULL,
  AFFILIATE_INTRA2   VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD1        VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD2        VARCHAR2(10 BYTE)          NULL,
  CHARTFIELD3        VARCHAR2(10 BYTE)          NULL,
  PROJECT_ID         VARCHAR2(15 BYTE)          NULL,
  BOOK_CODE          VARCHAR2(4 BYTE)           NULL,
  GL_ADJUST_TYPE     VARCHAR2(4 BYTE)           NULL,
  CURRENCY_CD        VARCHAR2(3 BYTE)           NULL,
  STATISTICS_CODE    VARCHAR2(3 BYTE)           NULL,
  FISCAL_YEAR        INTEGER                    NULL,
  ACCOUNTING_PERIOD  INTEGER                    NULL,
  POSTED_TOTAL_AMT   NUMBER(26,3)               NULL,
  POSTED_BASE_AMT    NUMBER(26,3)               NULL,
  POSTED_TRAN_AMT    NUMBER(26,3)               NULL,
  BASE_CURRENCY      VARCHAR2(3 BYTE)           NULL,
  DTTM_STAMP_SEC     DATE                       NULL,
  PROCESS_INSTANCE   NUMBER(10)                 NULL
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_ps_gl_ledger.log'
    BADFILE      edm_log_dir:'src_ps_gl_ledger.bad'
    DISCARDFILE  edm_log_dir:'src_ps_gl_ledger.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      BUSINESS_UNIT,
      LEDGER,
      ACCOUNT,
      ALTACCT,
      DEPTID,
      OPERATING_UNIT,
      PRODUCT,
      FUND_CODE,
      CLASS_FLD,
      PROGRAM_CODE,
      BUDGET_REF,
      AFFILIATE,
      AFFILIATE_INTRA1,
      AFFILIATE_INTRA2,
      CHARTFIELD1,
      CHARTFIELD2,
      CHARTFIELD3,
      PROJECT_ID,
      BOOK_CODE,
      GL_ADJUST_TYPE,
      CURRENCY_CD,
      STATISTICS_CODE,
      FISCAL_YEAR,
      ACCOUNTING_PERIOD,
      POSTED_TOTAL_AMT,
      POSTED_BASE_AMT,
      POSTED_TRAN_AMT,
      BASE_CURRENCY,
      DTTM_STAMP_SEC CHAR date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      PROCESS_INSTANCE
    )
  )
  LOCATION (EDM_DATA_DIR:'src_ps_gl_ledger.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_MANUAL_REBATES
(
  FIN_ID                  NUMBER(20,2),
  SYSTEM                  VARCHAR2(100 BYTE),
  PERIOD                  NUMBER(20,2),
  YEAR                    NUMBER(20,2),
  CYCLE                   NUMBER(20,2),
  WEX_ACCT_NBR            VARCHAR2(50 BYTE),
  WEX_ACCT_NM             VARCHAR2(200 BYTE),
  REBATE_GALLONS          NUMBER(20,3),
  REBATE_SPEND            NUMBER(20,2),
  EP_REBATE               NUMBER(20,2),
  VOLUME_REBATE           NUMBER(20,2),
  TOTAL_REBATE_INCENTIVE  NUMBER(20,2),
  DAYS_TO_PAY             VARCHAR2(50 BYTE),
  PAYOUT_TYPE             VARCHAR2(100 BYTE),
  CONTRACT_NBR            NUMBER(20,2),
  DATE_PROCESSED          DATE
)
ORGANIZATION EXTERNAL (
  TYPE ORACLE_LOADER
  DEFAULT DIRECTORY EDM_DATA_DIR
  ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_manual_rebates.log'
    BADFILE      edm_log_dir:'src_manual_rebates.bad'
    DISCARDFILE  edm_log_dir:'src_manual_rebates.dis'
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      FIN_ID,    
      "SYSTEM",    
      PERIOD,    
      "YEAR",  
      "CYCLE",
      WEX_ACCT_NBR,
      WEX_ACCT_NM,
      REBATE_GALLONS,
      REBATE_SPEND,
      EP_REBATE,
      VOLUME_REBATE,
      TOTAL_REBATE_INCENTIVE,
      DAYS_TO_PAY,
      PAYOUT_TYPE,
      CONTRACT_NBR,
      DATE_PROCESSED CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF DATE_PROCESSED = BLANKS
    )
  )
  LOCATION (EDM_DATA_DIR:'src_manual_rebates.dat')
)
REJECT LIMIT 0
NOPARALLEL
;


--
-- Wex BI4.0 - DDL for Excel-based source external tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_EXCEL_COST_TO_MKT_CHL
(
  NAME                   VARCHAR2(100 BYTE),
  START_DATE             DATE,
  END_DATE               DATE,
  MARKETING_SUB_CHANNEL  VARCHAR2(200 BYTE),
  "7-ELEVEN"             NUMBER(20,2),
  ALON                   NUMBER(20,2),
  CIRCLE_K               NUMBER(20,2),
  CITGO                  NUMBER(20,2),
  ENTERPRISE             NUMBER(20,2),
  EXXONMOBIL             NUMBER(20,2),
  FUELTRAC               NUMBER(20,2),
  GET_GO                 NUMBER(20,2),
  GULF                   NUMBER(20,2),
  HESS                   NUMBER(20,2),
  IMPERIAL               NUMBER(20,2),
  LUKOIL                 NUMBER(20,2),
  MAPCO                  NUMBER(20,2),
  MARATHON               NUMBER(20,2),
  MEIJER                 NUMBER(20,2),
  MURPHY_OIL             NUMBER(20,2),
  PEP_BOYS               NUMBER(20,2),
  PHILLIPS66             NUMBER(20,2),
  QUIKTRIP               NUMBER(20,2),
  RACETRAC               NUMBER(20,2),
  SEARS                  NUMBER(20,2),
  SHEETZ                 NUMBER(20,2),
  SUNOCO                 NUMBER(20,2),
  TESORO                 NUMBER(20,2),
  VALVOLINE              NUMBER(20,2),
  WAWA                   NUMBER(20,2),
  WEX                    NUMBER(20,2),
  ALLOTHER               NUMBER(20,2)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EDM_DATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_mkt_chl.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_mkt_chl.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF START_DATE = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF END_DATE = BLANKS,
      MARKETING_SUB_CHANNEL,
      "7-ELEVEN",
      ALON,
      CIRCLE_K,
      CITGO,
      ENTERPRISE,
      EXXONMOBIL,
      FUELTRAC,
      GET_GO,
      GULF,
      HESS,
      IMPERIAL,
      LUKOIL,
      MAPCO,
      MARATHON,
      MEIJER,
      MURPHY_OIL,
      PEP_BOYS,
      PHILLIPS66,
      QUIKTRIP,
      RACETRAC,
      SEARS,
      SHEETZ,
      SUNOCO,
      TESORO,
      VALVOLINE,
      WAWA,
      WEX,
      ALLOTHER
    )
     )
     LOCATION (EDM_DATA_DIR:'src_excel_cost_to_mkt_chl.dat')
  )
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_SERVE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_serve.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_serve.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_serve.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_serve.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ADJUDICATE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_adjudicate.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_adjudicate.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_adjudicate.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_adjudicate.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_TO_ACQUIRE
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_to_acquire.log'
    BADFILE      edm_log_dir:'src_excel_cost_to_acquire.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_to_acquire.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_to_acquire.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_SALES
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_sales.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_sales.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_sales.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_sales.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_COST_OF_IT
(
    NAME                   varchar2(100),
    VERSION                varchar2(100),
    START_DATE             date,
    END_DATE               date,
    COST_CENTER            varchar2(200),
    "WEX<=80"              number(20,2),
    "WEX81+"               number(20,2),
    BRANDED_UNIV           number(20,2),
    GOVT                   number(20,2),
    FLEET_BASED_COBRAND    number(20,2),
    PARTNER_BILLED_COBRAND number(20,2),
    PRIVATE_LABEL          number(20,2),
    PAC_PRIDE              number(20,2),
    REVOLVER               number(20,2),
    ALL_OTHER              number(20,2)
)
ORGANIZATION EXTERNAL (
    TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EDM_DATA_DIR
    ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_cost_of_it.log'
    BADFILE      edm_log_dir:'src_excel_cost_of_it.bad'
    DISCARDFILE  edm_log_dir:'src_excel_cost_of_it.dis'
    SKIP 1 -- skip the header row
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      NAME,
      VERSION,
      START_DATE CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      END_DATE   CHAR  date_format DATE mask "MM/DD/YYYY HH24:mi:ss" NULLIF EFFDT = BLANKS,
      COST_CENTER,
      "WEX<=80",
      "WEX81+",
      BRANDED_UNIV,
      GOVT,
      FLEET_BASED_COBRAND,
      PARTNER_BILLED_COBRAND,
      PRIVATE_LABEL,
      PAC_PRIDE,
      REVOLVER,
      ALL_OTHER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_cost_of_it.dat')
)
REJECT LIMIT 0
NOPARALLEL
;

CREATE TABLE SRC_EXCEL_PROGRAM
(
  PSRELATIONCODE             VARCHAR2(50 BYTE),
  PROGRAM                    VARCHAR2(200 BYTE),
  PARENT                     VARCHAR2(200 BYTE),
  DATEADDED                  DATE,
  SIEBELPROGRAMNAME          VARCHAR2(200 BYTE),
  SIEBELROWID                VARCHAR2(200 BYTE),
  PROFITABILITYTYPE          VARCHAR2(200 BYTE),
  PROFITABILITYRELATIONSHIP  VARCHAR2(200 BYTE),
  MARKETINGRELATIONSHIP      VARCHAR2(200 BYTE),
  MARKETINGPARTNER           VARCHAR2(200 BYTE)
)
ORGANIZATION EXTERNAL (
  TYPE ORACLE_LOADER
  DEFAULT DIRECTORY EDM_DATA_DIR
  ACCESS PARAMETERS (
    RECORDS DELIMITED BY NEWLINE
    LOGFILE      edm_log_dir:'src_excel_program.log'
    BADFILE      edm_log_dir:'src_excel_program.bad'
    DISCARDFILE  edm_log_dir:'src_excel_program.dis'
--  no skpping, in case no header is given
    FIELDS  TERMINATED BY '~|~'
    MISSING FIELD VALUES ARE NULL
    REJECT ROWS WITH ALL NULL FIELDS (
      PSRELATIONCODE,
      PROGRAM,
      PARENT,
      DATEADDED CHAR  DATE_FORMAT DATE MASK "MM/DD/YYYY HH24:mi:ss" NULLIF DATEADDED = BLANKS,
      SIEBELPROGRAMNAME,
      SIEBELROWID,
      PROFITABILITYTYPE,
      PROFITABILITYRELATIONSHIP,
      MARKETINGRELATIONSHIP,
      MARKETINGPARTNER
    )
  )
  LOCATION (EDM_DATA_DIR:'src_excel_program.dat')
)
REJECT LIMIT 2  -- to prevent bad headers
NOPARALLEL
;

--
-- Wex BI4.0 - DDL for Excel-based source external tables
--
-- ------------------------------------------------------------------------
CREATE TABLE SRC_SOURCE_MKT_CHANNEL
(
    NAME                   varchar2(100)          NOT NULL,
    VERSION                varchar2(100)          NOT NULL,
    SOURCE_EXCEL_TYPE      varchar2(100)          NOT NULL,
    START_DATE             date                   NOT NULL,
    END_DATE               date                   NOT NULL,
    MKT_PARTNER_ID         varchar2(200)          NOT NULL,
    MKT_PARTNER_SUB_CHL_ID varchar2(200)          NOT NULL,
    MKT_SUB_CHL_ALLOCATION number(20,2)           NOT NULL
)
TABLESPACE D_BI
;

CREATE TABLE SRC_SOURCE_BUSINESS_SEGMENT
(
    NAME                        varchar2(100)     NOT NULL,
    VERSION                     varchar2(100)     NOT NULL,
    SOURCE_EXCEL_TYPE           varchar2(100)     NOT NULL,
    START_DATE                  date              NOT NULL,
    END_DATE                    date              NOT NULL,
    COST_CENTER_ID              varchar2(200)     NOT NULL,
    COST_CENTER_NAME            varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ID         varchar2(200)     NOT NULL,
    BUSINESS_SEGMENT_ALLOCATION number(20,2)      NOT NULL
)
TABLESPACE D_BI
;

