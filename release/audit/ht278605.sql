SET ECHO         ON
SET SERVEROUTPUT ON
SET NEWPAGE      0
SET SPACE        0
SET PAGESIZE     0
SET FEEDBACK     ON
SET HEADING      ON
SET TRIMSPOOL    ON
SET TAB          OFF
SET ESCAPE       ON
SET LINESIZE     2000

SPOOL /i1/&env/hub/logs/dba_oneshot_qtedw_HT278605.log;

@log4me.prc
@xos_audit_mgr.prc
@d_purchase_device_trigger.prc

SPOOL OFF


