--
-- D_PURCHASE_DEVICE auditing 
--  card_status_code
--  card_status_desc
--  card_status_date
--  terminated_date
--  terminated_count1
--
CREATE OR REPLACE TRIGGER TRUA_D_AUDIT_TEST
BEFORE UPDATE ON D_AUDIT_TEST
REFERENCING OLD AS o NEW AS n
FOR EACH ROW
BEGIN
  xos_audit_mgr.set_context(
    'D_AUDIT_TEST',
    :o.AUDIT_TEST_KEY,
    :o.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_PROC_NM,
    :n.ROW_LAST_MOD_PROC_SEQ_NBR
  );

  xos_audit_mgr.audit('ACTIVATED_COUNT1', :o.ACTIVATED_COUNT1, :n.ACTIVATED_COUNT1);
  xos_audit_mgr.audit('ACTIVATED_DATE', :o.ACTIVATED_DATE, :n.ACTIVATED_DATE);
  xos_audit_mgr.audit('AUDIT_TEST_COUNT1', :o.AUDIT_TEST_COUNT1, :n.AUDIT_TEST_COUNT1);
  xos_audit_mgr.audit('AUDIT_TEST_KEY', :o.AUDIT_TEST_KEY, :n.AUDIT_TEST_KEY);
  xos_audit_mgr.audit('CARD_STATUS_CODE', :o.CARD_STATUS_CODE, :n.CARD_STATUS_CODE);
  xos_audit_mgr.audit('CARD_STATUS_DATE', :o.CARD_STATUS_DATE, :n.CARD_STATUS_DATE);
  xos_audit_mgr.audit('CARD_STATUS_DESC', :o.CARD_STATUS_DESC, :n.CARD_STATUS_DESC);
  xos_audit_mgr.audit('CURRENT_RECORD_FLG', :o.CURRENT_RECORD_FLG, :n.CURRENT_RECORD_FLG);
  xos_audit_mgr.audit('EMBOSSED_CARD_NUMBER_ID', :o.EMBOSSED_CARD_NUMBER_ID, :n.EMBOSSED_CARD_NUMBER_ID);
  xos_audit_mgr.audit('IN_TRANSITION_COUNT1', :o.IN_TRANSITION_COUNT1, :n.IN_TRANSITION_COUNT1);
  xos_audit_mgr.audit('IN_TRANSITION_DATE', :o.IN_TRANSITION_DATE, :n.IN_TRANSITION_DATE);
  xos_audit_mgr.audit('ISSUED_DATE', :o.ISSUED_DATE, :n.ISSUED_DATE);
  xos_audit_mgr.audit('MAGSTRIPE_DEVICE_COUNT1', :o.MAGSTRIPE_DEVICE_COUNT1, :n.MAGSTRIPE_DEVICE_COUNT1);
  xos_audit_mgr.audit('SUSPENDED_COUNT1', :o.SUSPENDED_COUNT1, :n.SUSPENDED_COUNT1);
  xos_audit_mgr.audit('SUSPENDED_DATE', :o.SUSPENDED_DATE, :n.SUSPENDED_DATE);
  xos_audit_mgr.audit('TERMINATED_COUNT1', :o.TERMINATED_COUNT1, :n.TERMINATED_COUNT1);
  xos_audit_mgr.audit('TERMINATED_DATE', :o.TERMINATED_DATE, :n.TERMINATED_DATE);
  xos_audit_mgr.audit('TIED_TO_AN_ASSET_FLG', :o.TIED_TO_AN_ASSET_FLG, :n.TIED_TO_AN_ASSET_FLG);
  xos_audit_mgr.audit('TIED_TO_A_PERSON_FLG', :o.TIED_TO_A_PERSON_FLG, :n.TIED_TO_A_PERSON_FLG);
  xos_audit_mgr.audit('TIED_TO_A_SITE_FLG', :o.TIED_TO_A_SITE_FLG, :n.TIED_TO_A_SITE_FLG);
  xos_audit_mgr.audit('VIRTUAL_DEVICE_COUNT1', :o.VIRTUAL_DEVICE_COUNT1, :n.VIRTUAL_DEVICE_COUNT1);

END;
/
