--
-- Wex SDLC 14.2 - DDL for HUB master tables
--
-- ------------------------------------------------------------------------
CREATE TABLE M_GL_PRODUCT (
  EDM_GL_PRODUCT_KEY  integer not null,
  SETID               varchar2(30),
  PRODUCT             varchar2(100),
  EFFDT               date,
  EFF_STATUS          varchar2(30),
  DESCR               varchar2(400),
  DESCRSHORT          varchar2(100),
  ACCOUNTING_OWNER    varchar2(100),
  BUDGETARY_ONLY      varchar2(30),
  SYNCID              integer,
  SYNCDTTM            date,
  CONSTRAINT PKM_GL_PRODUCT PRIMARY KEY (EDM_GL_PRODUCT_KEY)
)
TABLESPACE D_BI
;



--
-- Wex SDLC14.2 - DDL for HUB master cross-reference tables
--
-- ------------------------------------------------------------------------
CREATE TABLE M_GL_PRODUCT_XREF (
  DW_SOURCE_SYS                 varchar2(100)    NOT NULL,
  GL_PRODUCT_SOURCE_PK          varchar2(100)    NOT NULL,    
  EDM_GL_PRODUCT_KEY            integer          NOT NULL,
  CONSTRAINT PKM_GL_PRODUCT_XREF PRIMARY KEY (
    DW_SOURCE_SYS, GL_PRODUCT_SOURCE_PK
  )
)
TABLESPACE D_BI
;


-- add Informatica housekeeping fields
ALTER TABLE M_GL_PRODUCT
ADD (
    EDM_CREATE_DT                 TIMESTAMP        NOT NULL,
    EDM_LAST_UPDT_DT              TIMESTAMP        NOT NULL,
    EDM_LAST_UPDT_SESSION_NM      VARCHAR2(100)    NOT NULL,
    SRC_SYS_LAST_UPDT_DT          TIMESTAMP        NOT NULL
)
;
ALTER TABLE M_GL_PRODUCT_XREF
ADD (
    EDM_CREATE_DT                 TIMESTAMP        NOT NULL,
    EDM_LAST_UPDT_DT              TIMESTAMP        NOT NULL,
    EDM_LAST_UPDT_SESSION_NM      VARCHAR2(100)    NOT NULL,
    SRC_SYS_LAST_UPDT_DT          TIMESTAMP        NOT NULL
)
;
