--
-- SDLC14.2 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/sdlc142_migration.log;


-- SDLC14.2 HUB.STAGE_OWNER one time setup
@src_drp.ddl
@src_tbl.ddl
@src_gnt.ddl

SPOOL OFF
