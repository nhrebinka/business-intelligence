CREATE OR REPLACE PROCEDURE EDW_OWNER.F_GL_ACCT_MON_SNAPSHOT_LOAD
AS
/*
  ============================================================================
  Name : F_GL_ACCT_MONTHLY_SNAPSHOT
  Spec.: Store procedure for populating monthly GL_ACCT_MONTHLY snapshot by
         copying from EDW_STAGE_OWNER to EDW_OWNER. The process support re-run
         by truncating existing monthly data before repopulating them.

  Dependency:
    log4me package
    xos_tokenizer.concat_rpad

  Desc.:
    1. get date range from seed table (EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT)
    2. delete existing data from all partitions that will be touched
       2.a remove only 4 "repeating" categories for previous month
       2.b truncate every
    3. copy from stage into target table 

  Revision History:
  1.0 20140825 CC - first version (modeled from f_gl_acct_daily_snapshot_load
  ============================================================================
*/
  FMT_DT     constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';
  X_SES_ID   constant varchar2(80) := to_char(systimestamp, FMT_DT);
  X_SES_TYPE constant varchar2(80) := 'F_GL_ACCT_MONTHLY';
  X_SRC_TB   constant varchar2(80) := 'EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  X_TGT_TB   constant varchar2(80) := 'F_GL_ACCT_MONTHLY_SNAPSHOT';
  X_PROC     constant varchar2(80) := X_SES_TYPE;  
  --
  -- SQLs
  --
  X_GL_CAT   constant CLOB := '
    SELECT FR_CATEGORY_KEY FROM D_FR_CATEGORY
    WHERE  FR_CATEGORY_ID IN (
      ''Cost of IT'', 
      ''Cost of Sales'', 
      ''Cost to Acquire Sales'', 
      ''Cost to Serve''
    )
  ';
  X_DK_LIST  constant CLOB := '
    SELECT /*+ parallel */ UNIQUE to_char(revenue_date_key)
    FROM ' || X_SRC_TB || ' ORDER BY 1
  ';
  X_SQL      constant CLOB := '
  INSERT /*+ parallel append */ 
  INTO ' || X_TGT_TB || '(
    GL_ACCT_MONTHLY_SNAPSHOT_KEY,
    REVENUE_DATE_KEY,
    BUSINESS_SECTOR_KEY,
    BUSINESS_SEGMENT_KEY,
    FR_CATEGORY_KEY,
    COST_CENTER_KEY,
    PROGRAM_KEY,
    ACCOUNT_KEY,
    ACCOUNT_HIST_KEY,
    AMOUNT,
    DEPRECIATION_AMOUNT,
    SNAPSHOT_DATE_KEY,
    SESSION_ID,
    ROW_CREATE_DTTM,
    ROW_LAST_MOD_DTTM,
    ROW_LAST_MOD_PROC_NM,
    ROW_LAST_MOD_PROC_SEQ_NBR
  )
  SELECT /*+ parallel full(x) */
    GL_ACCT_DAILY_SNAPSHOT_KEY,
    REVENUE_DATE_KEY,
    BUSINESS_SECTOR_KEY,
    BUSINESS_SEGMENT_KEY,
    FR_CATEGORY_KEY,
    COST_CENTER_KEY,
    PROGRAM_KEY,
    ACCOUNT_KEY,
    ACCOUNT_HIST_KEY,
    AMOUNT,
    DEPRECIATION_AMOUNT,
    SNAPSHOT_DATE_KEY,
    ''' || X_SES_ID || ''',
    sysdate,
    sysdate,
    ''' || X_PROC || ''',
    rownum
  FROM ' || X_SRC_TB || ' x
  ';

  v_alist    TokList;        -- scratch token list
  v_rlist    TokList;        -- list of date key to process
  v_rdate    CLOB;           -- revenue_date_key list

  v_dt       varchar2(20);
  v_msg      varchar2(200);
  v_sql      CLOB;

  FUNCTION run(p_msg varchar2, p_sql varchar2, p_rtn boolean) RETURN TokList AS
    v_rst TokList;
    v_cnt varchar2(40);
  BEGIN
    IF p_rtn THEN
      EXECUTE IMMEDIATE p_sql BULK COLLECT INTO v_rst;
    ELSE
      EXECUTE IMMEDIATE p_sql;
    END IF;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    log4me.debug(p_msg || v_cnt, p_sql, X_SES_ID);
    RETURN v_rst;
  END run;

  FUNCTION wipe_sql(p_curr boolean, p_dt varchar2) RETURN varchar2
  AS
    v_ym     varchar2(10);
    v_part   varchar2(80);
    v_catkey CLOB;           -- category_key list
  BEGIN
    SELECT month_year_abbr INTO v_ym FROM d_date
    WHERE date_key = cast(p_dt as number)
      AND month_year_abbr NOT IN ('N/A');
    v_part := 'YM_' || replace(v_ym, '-', '_');

    IF p_curr THEN
      --
      -- current momth: truncate everything from the partition
      --
      RETURN 'ALTER TABLE ' || X_TGT_TB || ' TRUNCATE PARTITION ' || v_part;
    ELSE
      --
      -- build GL category filter for "repeating" categories
      --
      v_catkey := concat_rpad(
        run(v_msg || ' existing FR_CATEGORYs', X_GL_CAT, TRUE)
      );
      --
      -- previous month: delete only the "repeating" categories
      --
      RETURN '
      DELETE FROM ' || X_TGT_TB || ' PARTITION (' || v_part || ') x
      WEHRE x.revenue_date_key = ' || p_dt || '
        AND EXISTS (
          SELECT 1 FROM d_date d
          WHERE (1=1
            AND d.date_key = x.snapshot_date_key 
            AND d.month_year_abbr > ''' || v_ym || '''
            AND x.fr_category_key in (' || v_catkey || ')
          )
        )
      ';
    END IF;
  END wipe_sql;

BEGIN
  v_msg    := X_PROC;
  v_rlist  := run(v_msg, X_DK_LIST, TRUE);
  IF v_rlist IS NULL OR v_rlist.COUNT=0 THEN RAISE NO_DATA_FOUND; END IF;
  --
  -- gather date list to be processed
  --
  v_rdate := concat_rpad(v_rlist);
  v_msg   := X_PROC || ' daylist=[' || v_rdate || ']';
  log4me.info('INIT ' || v_msg, X_SES_ID);
  --
  -- get every month/partition involved, clean them one by one
  --
  FOR i IN 1..v_rlist.COUNT LOOP
    --
    -- clean existing data month-by-month
    --
    v_dt    := v_rlist(i);
    v_msg   := X_PROC || ' clearing ' || v_dt;
    v_alist := run(v_msg, wipe_sql(i=v_rlist.COUNT, v_dt), FALSE);
  END LOOP;
  --
  --
  v_msg   := X_PROC || ' insert new data into ' || X_TGT_TB;
  v_alist := run(v_msg, X_SQL, FALSE);

  -- ----------------------------------------------------------------------
  log4me.info('DONE ' || X_PROC, X_SES_ID);

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.warn('DONE ' || v_msg || ' no data found', X_SES_ID);
  RETURN;  
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('FAIL ' || v_msg, X_SES_ID);
  RAISE; 
END F_GL_ACCT_MON_SNAPSHOT_LOAD;
/
show errors
GRANT EXECUTE ON F_GL_ACCT_MON_SNAPSHOT_LOAD TO DWLOADER;
