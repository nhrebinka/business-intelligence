--
-- SDLC14.2 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/sdlc142_migration.log;


-- EDW.EDW_OWNER one time setup
@edw_drp.ddl
@edw_tbl.ddl
@edw_gnt.ddl

SPOOL OFF
