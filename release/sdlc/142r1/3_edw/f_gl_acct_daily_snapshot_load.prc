CREATE OR REPLACE PROCEDURE EDW_OWNER.F_GL_ACCT_DAILY_SNAPSHOT_LOAD
AS
/*
  ============================================================================
  Name : F_GL_ACCT_DAILY_SNAPSHOT_LOAD
  Spec.: Store procedure for daily GL_ACCT snapshot loading from 
         EDW_STAGE_OWNER to EDW_OWNER. This process supports re-run, i.e.
         It will clear the data produced by previous run identified by
         the same business date (ODATE or SNAPSHOT_DATE_KEY).

  Dependency:
    log4me package
    xos_tokenizer.concat_rpad

  Desc.:
    1. get date range from seed table (EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT)
    2. loop through every business date (SNAPSHOT_DATE_KEY)
       2.1 find every partitions that will be touched for the business date
       2.2 loop through each partition from the list 
           2.2.1 delete existing date from target table from the partition
           2.2.2 insert new data into target table

  Revision History:
  1.0 20140626 SSirigiri - created this procedure
  1.1 20140825 CC        - add re-run logic and logging info
  1.2 20141029 CC        - call monthly and monthly_by_category at the end

  ToDo:
    1. Enable partition compressiong
    2. Replace delete with read-modify-write
  ============================================================================
*/
  FMT_DT     constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';
  X_SES_ID   constant varchar2(80) := to_char(systimestamp, FMT_DT);
  X_SES_TYPE constant varchar2(80) := 'F_GL_ACCT_DAILY';
  X_SRC_TB   constant varchar2(80) := 'EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  X_TGT_TB   constant varchar2(80) := 'EDW_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';
  X_PROC     constant varchar2(80) := X_SES_TYPE;
  --
  -- date range control variables
  --
  v_blist    TokList;       -- token collection of business date
  v_bdate    CLOB;          -- business date list
  v_ymlist   TokList;       -- token collection of year_month
  --
  -- process variables
  --
  v_msg      varchar2(80);

  FUNCTION get_date_range RETURN TokList AS
    X_SQL constant varchar2(200) := '
      SELECT /*+ parallel */ UNIQUE to_char(snapshot_date_key)
      FROM ' || X_SRC_TB || ' ORDER BY 1
    ';
    v_list TokList;
  BEGIN
    log4me.debug(X_SES_TYPE || ' fetching date range', X_SQL, X_SES_ID);
    EXECUTE IMMEDIATE X_SQL BULK COLLECT INTO v_list;
    IF v_list IS NULL OR v_list.COUNT = 0 THEN
      RAISE NO_DATA_FOUND;
    END IF;
    RETURN v_list;
  END get_date_range;

  FUNCTION get_month_list(date_key varchar2) RETURN TokList AS
    X_SQL constant varchar2(400) := '
      SELECT /*+ parallel(x,16) full(x) use_hash(d,x) */
        UNIQUE d.month_year_abbr
      FROM ' || X_SRC_TB || ' x
      JOIN d_date d ON (x.REVENUE_DATE_KEY = d.DATE_KEY)
      WHERE snapshot_date_key = ' || date_key || '
        AND revenue_date_key != 0
      ORDER BY 1
    ';
    v_list TokList;
  BEGIN
    --
    -- for a given business_date:
    --   get the list of months involved based on accounting date
    --
    v_msg := X_PROC || ' daykey=' || date_key;
    EXECUTE IMMEDIATE X_SQL BULK COLLECT INTO v_list;
    log4me.debug(v_msg, X_SQL, X_SES_ID);

    RETURN v_list;
  END get_month_list;

  PROCEDURE process_one_partition(p_bdate varchar2, p_ym varchar2) AS
    X_PART constant varchar2(20) := 'YM_' || replace(p_ym, '-', '_');
    v_sql  CLOB;
    v_cnt  varchar2(40);
  BEGIN
    --
    -- delete existing data if any
    --
    v_sql  := '
    DELETE FROM ' || X_TGT_TB || ' PARTITION(' || X_PART || ')
    WHERE SNAPSHOT_DATE_KEY = ' || p_bdate
    ;
    v_msg  := X_PROC || 
      ' clear existing data of dt=' || p_bdate || ' within ' || X_PART;
    EXECUTE IMMEDIATE v_sql;
    v_cnt  := ' => ' || SQL%ROWCOUNT || ' rows';
    COMMIT;
    log4me.debug(v_msg || v_cnt, v_sql, X_SES_ID);
    --
    -- adding new data
    --
    v_sql  := '
    INSERT /*+ append */ 
    INTO ' || X_TGT_TB || '(
      GL_ACCT_DAILY_SNAPSHOT_KEY,
      REVENUE_DATE_KEY,
      BUSINESS_SECTOR_KEY,
      BUSINESS_SEGMENT_KEY,
      FR_CATEGORY_KEY,
      COST_CENTER_KEY,
      PROGRAM_KEY,
      ACCOUNT_KEY,
      ACCOUNT_HIST_KEY,
      AMOUNT,
      DEPRECIATION_AMOUNT,
      SNAPSHOT_DATE_KEY,
      SESSION_ID,
      ROW_CREATE_DTTM,
      ROW_LAST_MOD_DTTM,
      ROW_LAST_MOD_PROC_NM,
      ROW_LAST_MOD_PROC_SEQ_NBR
    )
    SELECT /*+ parallel(x,16) full(x) use_hash(d, x) */
      GL_ACCT_DAILY_SNAPSHOT_KEY,
      REVENUE_DATE_KEY,
      BUSINESS_SECTOR_KEY,
      BUSINESS_SEGMENT_KEY,
      FR_CATEGORY_KEY,
      COST_CENTER_KEY,
      PROGRAM_KEY,
      ACCOUNT_KEY,
      ACCOUNT_HIST_KEY,
      AMOUNT,
      DEPRECIATION_AMOUNT,
      SNAPSHOT_DATE_KEY,
      ''' || X_SES_ID || ''',
      sysdate,
      sysdate,
      ''' || X_PROC || ''',
      1
    FROM ' || X_SRC_TB || ' x
    JOIN (
      SELECT DATE_KEY FROM d_date
      WHERE month_year_abbr = ''' || p_ym || '''
    ) d ON (x.REVENUE_DATE_KEY = d.DATE_KEY)
    WHERE SNAPSHOT_DATE_KEY = ' || p_bdate
    ;
    v_msg := X_PROC || ' insert new data of dt=' || p_bdate;
    EXECUTE IMMEDIATE v_sql;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    COMMIT;
    log4me.debug(v_msg || v_cnt, v_sql, X_SES_ID);
  END;

BEGIN
  --
  -- get date range from seed table
  --
  v_blist := get_date_range;
  v_bdate := concat_rpad(v_blist);
  log4me.info('INIT ' || X_PROC || ' daylist=[' || v_bdate || ']', X_SES_ID);
  --
  -- walk through every snapshot_date 
  --
  FOR i IN 1..v_blist.COUNT LOOP
    --
    -- get every partition for the given revenue_date
    --
    v_ymlist := get_month_list(v_blist(i));
    FOR j IN 1..v_ymlist.COUNT LOOP
      process_one_partition(v_blist(i), v_ymlist(j));
    END LOOP;
  END LOOP;

  log4me.info('DONE ' || X_PROC, X_SES_ID);
  --
  -- to save some ETL work, we combine these into one call
  --
  f_gl_acct_mon_snapshot_load;
  f_gl_acct_mon_by_category_load;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.warn('DONE ' || X_SES_TYPE || ' no data found', X_SES_ID);
  RETURN;
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('FAIL ' || v_msg, X_SES_ID);
  RAISE; 
END F_GL_ACCT_DAILY_SNAPSHOT_LOAD;
/
show errors

GRANT EXECUTE ON F_GL_ACCT_DAILY_SNAPSHOT_LOAD TO DWLOADER;
