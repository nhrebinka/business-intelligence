--
-- SDLC 14.2 - DDL for EDW Fact tables
--
-- ------------------------------------------------------------------------
ALTER TABLE F_GL_ACCT_MON_BY_CATEGORY
MODIFY (SESSION_ID varchar2(40))
;
ALTER TABLE X_D_SITE_B
MODIFY (SESSION_ID varchar2(40))
;
BEGIN
  FOR i IN 1..6
  LOOP
    EXECUTE IMMEDIATE '
      ALTER TABLE X_D_SITE_A0' || to_char(i) || '
      MODIFY (SESSION_ID varchar2(40))
    ';
  END LOOP;
END;
/
CREATE OR REPLACE FORCE VIEW D_ACCOUNT_CURRENT_VW (
  ACCOUNT_KEY,
  SIC_LOOKUP_KEY,
  SOURCE_ACCOUNT_ID,
  ACCOUNT_ADDRESS_LINE_1,
  ACCOUNT_ADDRESS_LINE_2,
  ACCOUNT_ADDRESS_LINE_3,
  ACCOUNT_CITY,
  ACCOUNT_STATE_PROV_CODE,
  ACCOUNT_POSTAL_CODE,
  ACCOUNT_MANAGER_NAME,
  ACCOUNT_NAME,
  BANK_IDENTIFICATION_NUMBER,
  ACCOUNT_OPEN_DATE,
  ACCOUNT_REGION_CODE,
  ACCOUNT_REGION_NAME,
  ACCOUNT_COUNTRY_NAME,
  ACCOUNT_STATUS_DESC,
  ACCOUNT_CLOSED_DATE,
  FUNDING_TYPE_NAME,
  CURRENT_RECORD_FLG,
  FUNDER_NAME,
  REBATE_FLG,
  PURCHASE_ACCOUNT_FLG,
  BILLING_ACCOUNT_FLG,
  SIC_INDUSTRY_CODE,
  NAICS_INDUSTRY_CODE,
  ACCOUNT_DISTRICT_NAME,
  ACCOUNT_DISTRICT_CODE,
  ACCOUNT_SUBDISTRICT_NAME,
  ACCOUNT_SUBDISTRICT_CODE,
  RELATED_BILLING_ACCOUNT_ID,
  REPORTING_NATIONAL_ACCOUNT_ID,
  NATIONAL_ACCOUNT_OPEN_DATE,
  CUSTOMER_NAME,
  RISK_GRADE,
  ATTRITION_TYPE_NAME,
  ATTRITION_REASON_CODE,
  ATTRITION_REASON_DESC,
  ROW_EFF_BEGIN_DTTM,
  ROW_EFF_END_DTTM,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR,
  ROW_MD5_CHECKSUM_HEX_T1,
  ROW_MD5_CHECKSUM_HEX_T2,
  TAX_EXEMPT_FLG,
  SOURCE_SYSTEM_CODE,
  CLM_CLUSTER_NAME,
  ACCOUNT_HIST_KEY,
  LAST_CLUSTERED_DATE,
  EFPS_TIME_INTERVAL,
  EFPS_REQUIRED_FLG,
  EFPS_FLG,
  ACQUISITION_CHANNEL,
  COUPON_CODE,
  OPPORTUNITY_NUMBER,
  SALES_REP_ID,
  PAYMENT_METHOD,
  PRODUCT_PURCHASE_RESTRICTION,
  CREDIT_NATIONAL_ACCOUNT_ID,
  REWARDS_FLG,
  RECOURSE_CODE,
  RECOURSE_NAME,
  BUSINESS_UNIT,
  BANKRUPTCY_FLG,
  DEAD_LOSS_FLG,
  BILLING_CYCLE_CODE,
  SETID,
  BANKRUPTCY_FILING_TYPE_NAME,
  BANKRUPTCY_PROOF_OF_CLAIM_FLG,
  BANKRUPTCY_DISMISSAL_DATE,
  ATTORNEY_RETAIN_DATE,
  BANKRUPTCY_FILE_DATE,
  RECEIVABLES_ANALYST_NAME,
  GOVERNMENT_FLG,
  ACCOUNT_SINCE_DATE
) 
AS 
SELECT
  ACCOUNT_KEY,
  SIC_LOOKUP_KEY,
  SOURCE_ACCOUNT_ID,
  ACCOUNT_ADDRESS_LINE_1,
  ACCOUNT_ADDRESS_LINE_2,
  ACCOUNT_ADDRESS_LINE_3,
  ACCOUNT_CITY,
  ACCOUNT_STATE_PROV_CODE,
  ACCOUNT_POSTAL_CODE,
  ACCOUNT_MANAGER_NAME,
  ACCOUNT_NAME,
  BANK_IDENTIFICATION_NUMBER,
  ACCOUNT_OPEN_DATE,
  ACCOUNT_REGION_CODE,
  ACCOUNT_REGION_NAME,
  ACCOUNT_COUNTRY_NAME,
  ACCOUNT_STATUS_DESC,
  ACCOUNT_CLOSED_DATE,
  FUNDING_TYPE_NAME,
  CURRENT_RECORD_FLG,
  FUNDER_NAME,
  REBATE_FLG,
  PURCHASE_ACCOUNT_FLG,
  BILLING_ACCOUNT_FLG,
  SIC_INDUSTRY_CODE,
  NAICS_INDUSTRY_CODE,
  ACCOUNT_DISTRICT_NAME,
  ACCOUNT_DISTRICT_CODE,
  ACCOUNT_SUBDISTRICT_NAME,
  ACCOUNT_SUBDISTRICT_CODE,
  RELATED_BILLING_ACCOUNT_ID,
  REPORTING_NATIONAL_ACCOUNT_ID,
  NATIONAL_ACCOUNT_OPEN_DATE,
  CUSTOMER_NAME,
  RISK_GRADE,
  ATTRITION_TYPE_NAME,
  ATTRITION_REASON_CODE,
  ATTRITION_REASON_DESC,
  ROW_EFF_BEGIN_DTTM,
  ROW_EFF_END_DTTM,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR,
  ROW_MD5_CHECKSUM_HEX_T1,
  ROW_MD5_CHECKSUM_HEX_T2,
  TAX_EXEMPT_FLG,
  SOURCE_SYSTEM_CODE,
  CLM_CLUSTER_NAME,
  ACCOUNT_HIST_KEY,
  LAST_CLUSTERED_DATE,
  EFPS_TIME_INTERVAL,
  EFPS_REQUIRED_FLG,
  EFPS_FLG,
  ACQUISITION_CHANNEL,
  COUPON_CODE,
  OPPORTUNITY_NUMBER,
  SALES_REP_ID,
  PAYMENT_METHOD,
  PRODUCT_PURCHASE_RESTRICTION,
  CREDIT_NATIONAL_ACCOUNT_ID,
  REWARDS_FLG,
  RECOURSE_CODE,
  RECOURSE_NAME,
  BUSINESS_UNIT,
  BANKRUPTCY_FLG,
  DEAD_LOSS_FLG,
  BILLING_CYCLE_CODE,
  SETID,
  BANKRUPTCY_FILING_TYPE_NAME,
  BANKRUPTCY_PROOF_OF_CLAIM_FLG,
  BANKRUPTCY_DISMISSAL_DATE,
  ATTORNEY_RETAIN_DATE,
  BANKRUPTCY_FILE_DATE,
  RECEIVABLES_ANALYST_NAME,
  GOVERNMENT_FLG,
  NVL(ACCOUNT_SINCE_DATE, ACCOUNT_OPEN_DATE)
FROM D_ACCOUNT PARTITION(D_ACCOUNT_CURRENT);

CREATE OR REPLACE FORCE VIEW D_ACCOUNT_HIST_VW (
  ACCOUNT_KEY,
  ACCOUNT_HIST_KEY,
  RECOURSE_CODE,
  RECOURSE_NAME,
  FUNDER_NAME,
  BILLING_CYCLE_CODE,
  SIC_LOOKUP_KEY,
  SOURCE_ACCOUNT_ID,
  ACCOUNT_ADDRESS_LINE_1,
  ACCOUNT_ADDRESS_LINE_2,
  ACCOUNT_ADDRESS_LINE_3,
  ACCOUNT_CITY,
  ACCOUNT_STATE_PROV_CODE,
  ACCOUNT_POSTAL_CODE,
  ACCOUNT_MANAGER_NAME,
  ACCOUNT_NAME,
  BANK_IDENTIFICATION_NUMBER,
  ACCOUNT_OPEN_DATE,
  ACCOUNT_REGION_CODE,
  ACCOUNT_REGION_NAME,
  ACCOUNT_COUNTRY_NAME,
  ACCOUNT_STATUS_DESC,
  ACCOUNT_CLOSED_DATE,
  FUNDING_TYPE_NAME,
  CURRENT_RECORD_FLG,
  REBATE_FLG,
  PURCHASE_ACCOUNT_FLG,
  BILLING_ACCOUNT_FLG,
  SIC_INDUSTRY_CODE,
  NAICS_INDUSTRY_CODE,
  ACCOUNT_DISTRICT_NAME,
  ACCOUNT_DISTRICT_CODE,
  ACCOUNT_SUBDISTRICT_NAME,
  ACCOUNT_SUBDISTRICT_CODE,
  RELATED_BILLING_ACCOUNT_ID,
  REPORTING_NATIONAL_ACCOUNT_ID,
  NATIONAL_ACCOUNT_OPEN_DATE,
  CUSTOMER_NAME,
  RISK_GRADE,
  ATTRITION_TYPE_NAME,
  ATTRITION_REASON_CODE,
  ATTRITION_REASON_DESC,
  ROW_EFF_BEGIN_DTTM,
  ROW_EFF_END_DTTM,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR,
  ROW_MD5_CHECKSUM_HEX_T1,
  ROW_MD5_CHECKSUM_HEX_T2,
  TAX_EXEMPT_FLG,
  SOURCE_SYSTEM_CODE,
  CLM_CLUSTER_NAME,
  LAST_CLUSTERED_DATE,
  EFPS_TIME_INTERVAL,
  EFPS_REQUIRED_FLG,
  EFPS_FLG,
  ACQUISITION_CHANNEL,
  COUPON_CODE,
  OPPORTUNITY_NUMBER,
  SALES_REP_ID,
  PAYMENT_METHOD,
  PRODUCT_PURCHASE_RESTRICTION,
  CREDIT_NATIONAL_ACCOUNT_ID,
  REWARDS_FLG,
  BUSINESS_UNIT,
  BANKRUPTCY_FLG,
  DEAD_LOSS_FLG,
  SETID,
  BANKRUPTCY_FILING_TYPE_NAME,
  BANKRUPTCY_PROOF_OF_CLAIM_FLG,
  BANKRUPTCY_DISMISSAL_DATE,
  ATTORNEY_RETAIN_DATE,
  BANKRUPTCY_FILE_DATE,
  RECEIVABLES_ANALYST_NAME,
  GOVERNMENT_FLG,
  ACCOUNT_SINCE_DATE
) 
AS
SELECT
  ACCOUNT_KEY AS ACCOUNT_KEY,
  ACCOUNT_HIST_KEY AS ACCOUNT_HIST_KEY,
  RECOURSE_CODE AS RECOURSE_CODE,
  RECOURSE_NAME AS RECOURSE_NAME,
  FUNDER_NAME AS FUNDER_NAME,
  BILLING_CYCLE_CODE,
  SIC_LOOKUP_KEY,
  SOURCE_ACCOUNT_ID,
  ACCOUNT_ADDRESS_LINE_1,
  ACCOUNT_ADDRESS_LINE_2,
  ACCOUNT_ADDRESS_LINE_3,
  ACCOUNT_CITY,
  ACCOUNT_STATE_PROV_CODE,
  ACCOUNT_POSTAL_CODE,
  ACCOUNT_MANAGER_NAME,
  ACCOUNT_NAME,
  BANK_IDENTIFICATION_NUMBER,
  ACCOUNT_OPEN_DATE,
  ACCOUNT_REGION_CODE,
  ACCOUNT_REGION_NAME,
  ACCOUNT_COUNTRY_NAME,
  ACCOUNT_STATUS_DESC,
  ACCOUNT_CLOSED_DATE,
  FUNDING_TYPE_NAME,
  CURRENT_RECORD_FLG,
  REBATE_FLG,
  PURCHASE_ACCOUNT_FLG,
  BILLING_ACCOUNT_FLG,
  SIC_INDUSTRY_CODE,
  NAICS_INDUSTRY_CODE,
  ACCOUNT_DISTRICT_NAME,
  ACCOUNT_DISTRICT_CODE,
  ACCOUNT_SUBDISTRICT_NAME,
  ACCOUNT_SUBDISTRICT_CODE,
  RELATED_BILLING_ACCOUNT_ID,
  REPORTING_NATIONAL_ACCOUNT_ID,
  NATIONAL_ACCOUNT_OPEN_DATE,
  CUSTOMER_NAME,
  RISK_GRADE,
  ATTRITION_TYPE_NAME,
  ATTRITION_REASON_CODE,
  ATTRITION_REASON_DESC,
  ROW_EFF_BEGIN_DTTM,
  ROW_EFF_END_DTTM,
  ROW_CREATE_DTTM,
  ROW_LAST_MOD_DTTM,
  ROW_LAST_MOD_PROC_NM,
  ROW_LAST_MOD_PROC_SEQ_NBR,
  ROW_MD5_CHECKSUM_HEX_T1,
  ROW_MD5_CHECKSUM_HEX_T2,
  TAX_EXEMPT_FLG,
  SOURCE_SYSTEM_CODE,
  CLM_CLUSTER_NAME,
  LAST_CLUSTERED_DATE,
  EFPS_TIME_INTERVAL,
  EFPS_REQUIRED_FLG,
  EFPS_FLG,
  ACQUISITION_CHANNEL,
  COUPON_CODE,
  OPPORTUNITY_NUMBER,
  SALES_REP_ID,
  PAYMENT_METHOD,
  PRODUCT_PURCHASE_RESTRICTION,
  CREDIT_NATIONAL_ACCOUNT_ID,
  REWARDS_FLG,
  BUSINESS_UNIT,
  BANKRUPTCY_FLG,
  DEAD_LOSS_FLG,
  SETID,
  BANKRUPTCY_FILING_TYPE_NAME,
  BANKRUPTCY_PROOF_OF_CLAIM_FLG,
  BANKRUPTCY_DISMISSAL_DATE,
  ATTORNEY_RETAIN_DATE,
  BANKRUPTCY_FILE_DATE,
  RECEIVABLES_ANALYST_NAME,
  GOVERNMENT_FLG,
  NVL(ACCOUNT_SINCE_DATE, ACCOUNT_OPEN_DATE)
FROM D_ACCOUNT;



