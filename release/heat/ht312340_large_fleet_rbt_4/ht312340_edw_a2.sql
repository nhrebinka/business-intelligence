--
-- 2013-2015 Site Agg - Large Fleet Rebates Historical Rerun
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht310762_large_fleet_rbt.log;

ALTER SESSION ENABLE PARALLEL DML;

DECLARE
  x_prc constant varchar2(100) := 'HT312340: Large Fleet Rebate - RevAgg Rerun';
  v_ym  varchar2(40);
  v_ses varchar2(40);

BEGIN
  log4me.info('INIT ' || x_prc);

  FOR i IN 1..6 LOOP
    xos_workspace_mgr.free('X_D_SITE_A0' || i);
  END LOOP;
  --
  -- load directly into x_d_site_b 
  --
  FOR y IN 2013..2014 LOOP
    FOR m IN 1..12 LOOP
      v_ym := trim(to_char(y)) || '-' || trim(to_char(m, '00'));
      xbs_site_agg.rerun(v_ym, TRUE);
    END LOOP;
  END LOOP;
  --
  -- keep these in buffer ring
  --
  FOR m IN 1..4 LOOP
    v_ym := '2015' || '-' || trim(to_char(m, '00'));
    xbs_site_agg.rerun(v_ym);
    IF m<4 THEN
      xos_session_mgr.previous_session('D_SITE_AGG', v_ym, v_ses);
      xos_workspace_mgr.audit_by_session(v_ses);
    END IF;
  END LOOP;

  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

