CREATE OR REPLACE PROCEDURE F_GL_ACCT_MON_BY_CATEGORY_LOAD (
  p_bgn_dt date     := NULL,
  p_end_dt date     := NULL,
  p_ses_id varchar2 := to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4')
)
/*
  ============================================================================
  Name : F_GL_ACCT_MON_BY_CATEGORY_LOAD
  Spec.: Store procedure for monthly GL_ACCT snapshot denormalized from 
         F_GL_ACCT_MONTHLY_SNAPSHOT with FR_CATEGORY as columns.

  Dependency:
    log4me        package
    xos_tokenizer package

  Desc.:
    By utilizing Oracle PIVOT SQL support, for each account, we transpose 
    rows for each FR_CATEGORY into columns following these naming convention
      *_AMT     for amount
      *_DPC_AMT for depreciation amount

    The control flow as the following:
    1. get date range from stage seed table or input parameters
    2. find referenced partitions based on the date range
    3. for each partition do the following
       3.1 truncate partition that will be touched by the data
       3.2 populate the table with most recent data from monthly partition

  Note:
    1. Maintenance dimensional keys of D_FR_CATEOGY, i.e.
       Invalid:-2, Unknown:-1, and N/A:0 are filtered out in SQL by "> 0".
       Care should be taken when adding any maintenance key that might break 
       this condition.
    2. Cost to acquire marketing channel and cost of adjudication do not have
       a concept of depreciation. We decided to take them out of transposed
       column list. Care should be taken should we decided to add new categories
       into the list.
 
  Revision History:
  1.0 20140918 CC - first version
  1.1 20150312 CC - fix over-stating bug,
                    by replace source from daily_snapshot to monthly_snapshot
                    

  ============================================================================
*/
AS
  FMT_DT     constant varchar2(20) := 'YYYY-MM-DD';

  X_SES_TYPE constant varchar2(40) := 'F_GL_ACCT_MON_BY_CATEGORY';
  X_PROC     constant varchar2(40) := X_SES_TYPE || '_LOAD';
  X_SRC_TB   constant varchar2(80) := 'F_GL_ACCT_MONTHLY_SNAPSHOT';
  X_TGT_TB   constant varchar2(80) := 'F_GL_ACCT_MON_BY_CATEGORY';
  X_SEED_TB  constant varchar2(80) := 'EDW_STAGE_OWNER.F_GL_ACCT_DAILY_SNAPSHOT';

  v_blist  TokList;       -- token collection of business date
  v_bdate  CLOB;          -- business date list
  v_ymlist TokList;       -- token collection of year_month

  v_min_dt date;
  v_max_dt date;

  v_sql    CLOB;
  v_part   varchar2(20);
  
  FUNCTION get_insert_sql(p_part varchar2) RETURN varchar2 
  AS
    v_sql CLOB := '
      INSERT /*+ append */ INTO ' || X_TGT_TB || ' (
        revenue_date_key,
        business_sector_key,
        business_segment_key,
        cost_center_key,
        program_key,
        account_key,
        account_hist_key,
        session_id,
        cost_of_it_amt,
        cost_of_it_dpc_amt,
        cost_of_sales_amt,
        cost_of_sales_dpc_amt,
        cost_to_acquire_mkt_amt,
        cost_to_acquire_sales_amt,
        cost_to_acquire_sales_dpc_amt,
        cost_to_adjudicate_amt,
        cost_to_serve_amt,
        cost_to_serve_dpc_amt
      )
      SELECT /*+ parallel */
        revenue_date_key,
        business_sector_key,
        business_segment_key,
        cost_center_key,
        program_key,
        account_key,
        account_hist_key,
        ''' || p_ses_id || ''' session_id,
        #{OLIST}
      FROM (
        SELECT /*+ parallel */
          revenue_date_key,
          business_sector_key,
          business_segment_key,
          fr_category_key,
          cost_center_key,
          program_key,
          account_key,
          account_hist_key,
          sum(amount)              amt,
          sum(depreciation_amount) dpc_amt
        FROM ' || X_SRC_TB || ' PARTITION (#{PARTI}) x
        GROUP BY
          revenue_date_key,
          business_sector_key,
          business_segment_key,
          fr_category_key,
          cost_center_key,
          program_key,
          account_key,
          account_hist_key
      )
      PIVOT (
        SUM(amt)     as amt,
        SUM(dpc_amt) as dpc_amt
        FOR fr_category_key IN (#{ILIST})
      )
    ';
    TYPE vc_t IS varray(100) OF varchar2(100);
    v_k vc_t;   -- fr_category_key list
    v_n vc_t;   -- fr_category_id  list

    v_eol  char(1);
    v_name varchar2(40);
    v_col  varchar2(40);

    v_ilist CLOB := '';
    v_olist CLOB := '';  
  BEGIN
    --
    -- get active FR_CATEGORIES
    --
    SELECT 
      to_char(fr_category_key),
      upper(replace(fr_category_id,' ','_')) 
    BULK COLLECT INTO v_k, v_n
    FROM   d_fr_category
    WHERE  fr_category_key > 0
    ORDER BY fr_category_key;
    --
    -- build substitution lists #{ILIST}, #{OLIST} for SQL query.
    --
    FOR i IN 1..v_k.COUNT
    LOOP
      v_eol  := CASE WHEN i=v_k.COUNT THEN '' ELSE ',' END;
      v_name := replace(v_n(i), 'MARKETING_CHANNEL', 'MKT');
      v_col  := 'C' || i; 
      v_ilist:= v_ilist || v_k(i) || ' AS ' || v_col || v_eol;
      v_olist:= v_olist || 
        'NVL(' || v_col || '_AMT,0) AS '     || v_name || '_AMT' ||
        -- 
        -- take out the fields that doesn't make sense
        --
        CASE WHEN (
          instr(v_name, 'ADJUDICATE' )>0 OR 
          instr(v_name, 'ACQUIRE_MKT')>0
        )
        THEN ''
        ELSE ', NVL(' || v_col || '_DPC_AMT,0) AS ' || v_name || '_DPC_AMT'
        END || v_eol;
    END LOOP;

    v_sql := replace(v_sql, '#{ILIST}', v_ilist);
    v_sql := replace(v_sql, '#{OLIST}', v_olist);
    v_sql := replace(v_sql, '#{PARTI}', p_part);

    RETURN v_sql;
  END get_insert_sql;

  PROCEDURE process_one_partition AS
    v_ses_id  varchar2(40) := NULL;
    v_ym      varchar2(30);
    v_bgn_dt  varchar2(30);
    v_end_dt  varchar2(30);
   
    v_proc    varchar2(100);
    v_part    varchar2(20);
    v_sql     CLOB;
    v_cnt     integer := 0;  
  BEGIN
    xos_session_mgr.open(
      X_SES_TYPE, X_PROC, 
      v_ses_id, v_ym, v_bgn_dt, v_end_dt
    );
    if (v_ses_id is NULL) then RETURN; end if; -- no work, bail out

    v_proc := X_SES_TYPE || '[' || v_bgn_dt || ',' || v_end_dt || ']';

    log4me.debug(v_proc || ' ses_id=' || v_ses_id || ' found','', p_ses_id);
    v_part  := 'YM_' || replace(v_ym, '-', '_');
    --
    -- delete existing data if any
    --
    v_sql := '
      ALTER TABLE ' || X_TGT_TB || ' 
      TRUNCATE PARTITION ' || v_part || '
      UPDATE INDEXES
    ';
    EXECUTE IMMEDIATE v_sql;
    v_cnt := SQL%ROWCOUNT;
    log4me.debug(v_proc || ' clear => ' || v_cnt || ' rows', v_sql, p_ses_id);
    COMMIT;
    --
    -- populate BY CATEGORY table from monthly
    --
    v_sql := get_insert_sql(v_part);
    EXECUTE IMMEDIATE v_sql;
    v_cnt := SQL%ROWCOUNT;
    COMMIT;
    log4me.debug(v_proc || ' populate => ' || v_cnt || ' rows', v_sql, p_ses_id);
    --
    -- sucess! close the session 
    --
    xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_DONE, v_cnt);

  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('FAIL ' || v_proc || SQLERRM, v_ses_id);
    -- 
    -- close the session with failure, need to be resurrected
    --
    if (v_ses_id is not NULL) then
      xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_FAIL, -1);
    end if;
  END process_one_partition;

BEGIN
  --
  -- get date range from seed table
  --
  xos_session_mgr.get_date_range(
    p_bgn_dt,
    p_end_dt,
    X_SEED_TB,
    v_min_dt,
    v_max_dt    
  );
  v_sql := '
    SELECT UNIQUE month_year_abbr FROM d_date 
    WHERE calendar_date_dt BETWEEN :d1 and :d2
  ';
  log4me.debug(X_PROC, v_sql, p_ses_id);
  EXECUTE IMMEDIATE v_sql BULK COLLECT INTO v_ymlist USING v_min_dt, v_max_dt;

  IF v_ymlist IS NULL OR v_ymlist.COUNT = 0 THEN
    log4me.warn(X_PROC || ' no data found', p_ses_id);
    RETURN;  
  END IF;

  log4me.info('INIT ' || X_PROC, p_ses_id);
  FOR i IN 1..v_ymlist.COUNT LOOP
    xos_session_mgr.add_session(X_SES_TYPE, X_SRC_TB, v_ymlist(i));
    process_one_partition;
  END LOOP;

  log4me.info('DONE ' || X_PROC, p_ses_id);

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err('FAIL ' || X_PROC, p_ses_id);
  RAISE; 
END F_GL_ACCT_MON_BY_CATEGORY_LOAD;
/
show errors


                
