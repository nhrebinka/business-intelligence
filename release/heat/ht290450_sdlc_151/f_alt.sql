--
-- SDLC 15.1 - DDL for EDW Fact tables
--
-- ------------------------------------------------------------------------
set serveroutput on
set pagesize 400
set linesize 2000

declare
  x_prc varchar2(80) := 'SDLC 15.1 DDL ';
  x_lst TokList := TokList(
    'F_TRANSACTION_LINE_ITEM',
    'F_DAILY_REVENUE_SNAPSHOT',
    'F_MONTHLY_REVENUE_SNAPSHOT',
    'F_DAILY_TOTALS_SNAPSHOT',
    'X_D_SITE_B',
    'X_D_SITE_A01',
    'X_D_SITE_A02',
    'X_D_SITE_A03',
    'X_D_SITE_A04',
    'X_D_SITE_A05',
    'X_D_SITE_A06'
  );
  x_col varchar2(2000) := '(
    EV_TRANSACTION_FEE_AMOUNT NUMBER(15,2) DEFAULT 0 NOT NULL,
    SPEND_FUEL_KWH_AMOUNT     NUMBER(15,2) DEFAULT 0 NOT NULL,
    INTERCHANGE_FUEL_KWH_AMT  NUMBER(15,2) DEFAULT 0 NOT NULL,
    FUEL_KWH_QTY              NUMBER(15,5) DEFAULT 0 NOT NULL
  )';
  v_tb  varchar2(100);
  v_sql varchar2(2000);

begin
  log4me.info(x_prc || ' INIT');
  for i in 1..x_lst.count loop
    v_tb  := 'ALTER TABLE ' || x_lst(i);
    v_sql := v_tb || ' ADD ' || 
      CASE WHEN instr(v_tb, 'X_D')>0 
      THEN replace(replace(x_col,'AMOUNT', 'AMT'), 'INTERCHANGE', 'INTCHG')
      ELSE x_col
      END;

    begin
      log4me.debug(x_prc || ' => ' || v_tb, v_sql);
      execute immediate v_sql;
    exception when others then
      log4me.err(x_prc || ' ERROR');
    end;
  end loop;
  log4me.info(x_prc || ' DONE');
end;
/
show errors
