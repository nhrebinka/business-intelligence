--
-- SDLC 15.1 Release - EDW_OWNER
--
@release_note

@f_alt

@f_transaction_line_item_load.prc
@f_revenue_aggregates.prc
@xbs_site_agg.prc

--@xm_site_vw.ddl  -- only after SiteAgg is run at least once

SPOOL OFF

