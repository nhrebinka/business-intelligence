--
-- Site Agg Batch 2 - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht_247371_site_agg_batch2.log;

@site_agg_r1a_init.sql;
@site_agg_r1b_load.sql;
@site_agg_r1c_run.sql;
@site_agg_r1d_idx.sql;

SPOOL OFF


