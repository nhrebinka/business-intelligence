--
-- historical site aggregation - batch 2
--
alter session enable parallel dml;
begin
  --
  -- 2014-05 recovery
  --
  xbs_site_agg.run_one_session;

  UPDATE xo_session SET status='INIT' WHERE status='HOLD';
  commit;

  for n in 1..9 loop
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xbs_site_agg.run_one_session;
    xos_workspace_mgr.push_all('D_SITE_AGG');
  end loop;
  commit;

  UPDATE xo_session SET status='HOLD' WHERE status='INIT';
  commit;

  F_DAILY_SITE_AGGREGATES;
  commit;
end;
/
