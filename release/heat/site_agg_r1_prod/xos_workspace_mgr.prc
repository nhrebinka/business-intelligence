-
-- Name : xos_workspace_mgr Package
-- Spec.: Storage Allocation/Management for working tables sets
--
-- Dependency:
--   log4me
--
-- Log: 
--    20140320 CC - v1.0 first version
--    20140410 CC - v1.1 tune queries, add comments
--    20140430 CC - v1.2 migrate workspace entries to table creation script
--
--    20140516 CC - v2.0 add LRU-based cache management with state machine
--                  and enable audit() capability
--
-- ============================================================================
DROP   TABLE xo_workspace;
CREATE TABLE xo_workspace
(
  ws_class   varchar2(10) NOT NULL,
  ws_type    varchar2(40) NOT NULL,
  ws_name    varchar2(40) NOT NULL,
  session_id varchar2(40) NOT NULL,
  status     varchar2(20) NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL
)
;
--
-- make sure X_D_SITE_Ann creation script does this
--
-- INSERT INTO xo_workspace VALUES (
--   'HOT', 'D_SITE_AGG', 'X_D_SITE_A00', 'FREE', sysdate, sysdate
-- );
--
--
CREATE OR REPLACE PACKAGE xos_workspace_mgr AS
  --
  -- Storage Allocation/Management for working tables sets
  --
  --   Similar to how OS manages its memory, an ODS/DW system can manage its
  --   busylist and freelist of working block sets. By uitlizing different 
  --   storage technology, these working sets can be keep on SSD, Stripped
  --   RAID1/0, RAID5, or even optical silo for performance/cost effectiveness.
  --   80/20 rule is a good rule of thumb for distribution. 90/10 will work 
  --   well when we get really big i.e. like Facebook.
  --   
  --   The main driver behind is to use naming convention for space allocation
  --   scheme to reduce the complexity for managemnet in the long run. The
  --   cascading workspace (or tables in RDBMS) with various hotness for their
  --   different storage/retrieval requirements and are named as follows
  --
  --     COOL   WARM   HOT
  --       C <-  B0 <- A00   i.e. FM_SITE_C, FM_SITE_B0, FM_SITE_A00,...
  --         <-  B1 <- A01
  --                <- A02
  --                <- ...
  --                <- Ann
  --
  --   A given workspace goes through a life cycle (state machine) of the 
  --   followings:
  --     FREE - available for taking, a session will pick from a pool of these
  --     BUSY - is currently been worked on, process failure will return this 
  --            back to FREE or transition to USED when success
  --     USED - a workspace that is occupied with valid data
  --
  --   The workspace state machine is depicted as following
  --
  --          +-----free---- (HOT,STALE) --retire-+
  --          |                                   |
  --          v                                   |
  --     (HOT,FREE) -alloc-> (HOT,BUSY) -agg-> (WARM,USED)
  --          ^                                  |   |
  --          |                                  |   |
  --          +--push--- (WARM,BUSY) <--push-----+   |
  --                         ^                      audit
  --                         |                       |
  --                         +--push-- (COOL,USED) --+
  --   
  -- Spec:
  --   add()   - add new workspace to the xo_workspace table which is managed by
  --             this package
  --   mark()  - mark the workspace with a specific status
  --   alloc() - allocate a FREE workspace for process (mark BUSY)
  --   free()  - release the workspace back to FREE pool
  --
  --   copy()  - copy content from one workspace to other table
  --
  --   push()  - push the content of a workspace to its parent
  --             using the naming convension
  --   push_all() - push all the content of typed workspaces upto the parent
  --            
  --   remap() - is a short-hand for push()+free() following by remaping a view
  --             on top of resulting tables
  --
  --   retire()- mark the oldest workspace as stale for later usage
  --   audit() - mark workspace (safe copy) for auditing
  --
  -- Note:
  --   1. For implementation, individual table is used as a block currently
  --   2. The naming of blocks are designed using "convention over configuration",
  --      i.e. FD_SITE_An, FD_SITE_Bn, FD_SITE_C as HOT, WARM, and COLD sections
  --      for site aggregation tables. Other family of aggregation tables work
  --      the same way
  --   3. session_id from previous usage will be left after workspace freed
  --      for tracking until next alloc() request.
  --
  MSG_INIT   constant varchar2(10) := 'INIT ';
  MSG_DONE   constant varchar2(10) := 'DONE ';
  MSG_FAIL   constant varchar2(10) := 'FAIL ';

  SPACE_FREE constant varchar2(10) := 'FREE';
  SPACE_BUSY constant varchar2(10) := 'BUSY';
  SPACE_USED constant varchar2(10) := 'USED';
  SPACE_STALE constant varchar2(10) := 'STALE';

  CLASS_HOT  constant varchar2(10) := 'HOT';
  CLASS_WARM constant varchar2(10) := 'WARM';
  CLASS_COOL constant varchar2(10) := 'COOL';
  CLASS_COLD constant varchar2(10) := 'COLD';

  EMPTY_SES  constant varchar2(10) := '0.0.0';
  --
  -- 'soft' storage management
  --
  PROCEDURE add(
    p_class varchar2,
    p_type  varchar2,
    p_table varchar2
  );
  FUNCTION  upname(p_this varchar2) RETURN varchar2;
  PROCEDURE mark(
    p_ws_name varchar2,
    p_status  varchar2,
    p_class   varchar2,
    p_session varchar2 DEFAULT NULL
  );
  PROCEDURE alloc(
    p_type    varchar2, 
    p_session varchar2,
    p_ws OUT  varchar2
  );
  --
  -- 'hard' storage management
  --
  PROCEDURE copy(p_this varchar2, p_that varchar2);
  PROCEDURE free(p_this varchar2);
  PROCEDURE push(p_this varchar2);
  PROCEDURE push_all(p_type varchar2);
  --
  -- view management
  --
  PROCEDURE remap(
    p_type varchar2, 
    p_view varchar2,
    p_cond varchar2 DEFAULT ''
  );
  --
  -- LRU-based cache management
  --
  PROCEDURE retire(p_type varchar2);
  PROCEDURE audit(p_this varchar2);
  PROCEDURE audit_by_session(p_session_id varchar2);
END xos_workspace_mgr;
/

CREATE OR REPLACE PACKAGE BODY xos_workspace_mgr AS
  WSPACE_TB constant varchar2(40) := 'xo_workspace';
  VIEW_PFIX constant varchar2(3)  := '_VW';

PROCEDURE add(
  p_class varchar2,
  p_type  varchar2,
  p_table varchar2
)
AS
  v_proc constant varchar2(80) := 'add workspace ' || p_table;
  v_sql  constant CLOB := '
    INSERT INTO ' || WSPACE_TB || '
    VALUES(:c, :t, :w, :s, ''FREE'', sysdate, sysdate)
  ';
BEGIN
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql USING p_class, p_type, p_table, EMPTY_SES;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_sql);
    ROLLBACK;
    RAISE;
END add;
--
-- use naming convension to get the rollup table name
--
FUNCTION upname(p_this varchar2) RETURN varchar2
AS
  v_type varchar2(40) := substr(p_this, 1, length(p_this)-3); -- table type
  v_up   char(1) := chr(ascii(substr(p_this, -3, 1))+1);   -- next char
  v_nn   integer := to_number(substr(p_this, -2));         -- not used now
  v_that constant varchar2(40) := v_type || v_up;
BEGIN
  RETURN v_that;
END;
--
-- allocate a working table from freelist
--
PROCEDURE mark(
  p_ws_name varchar2, 
  p_status  varchar2,
  p_class   varchar2,
  p_session varchar2 DEFAULT NULL
) AS
  v_proc constant varchar2(80) := 
    'mark ' || p_ws_name || ' as [' || p_class || ',' || p_status || ']';
  v_sql0 constant CLOB := '
    UPDATE ' || WSPACE_TB || ' SET
      WS_CLASS   = ''' || p_class  || ''',
      STATUS     = ''' || p_status || ''', 
      UPDATED_AT = sysdate
    WHERE ws_name = ''' || p_ws_name || '''
  ';
  v_sql1 constant CLOB := '
    UPDATE ' || WSPACE_TB || ' SET
      WS_CLASS   = ''' || p_class   || ''',
      SESSION_ID = ''' || p_session || ''',
      STATUS     = ''' || p_status || ''', 
      UPDATED_AT = sysdate
    WHERE ws_name = ''' || p_ws_name || '''
  ';
  v_sql CLOB;
BEGIN
  if p_session IS NULL then
    v_sql := v_sql0;
  else
    v_sql := v_sql1;
  end if;
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    RAISE;
END mark;
--
-- retire the oldest workspace and mark it stale (LRU)
--
PROCEDURE retire(p_type varchar2)
AS
  v_proc   constant varchar2(80) := 'mark oldest ' || p_type || ' stale';
  v_sql    constant CLOB := '
    SELECT ws_name
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_class = ''' || CLASS_WARM || '''
      AND ws_type  = ''' || p_type     || '''
      AND status   = ''' || SPACE_USED || '''
      AND ROWNUM   = 1
    ORDER BY updated_at asc
  ';
  v_this  varchar2(40) := NULL;
BEGIN
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql INTO v_this;
  --
  -- keep the last entry in cache, mark the others STALE
  --
  if (v_this is NOT NULL) then
    mark(v_this, SPACE_STALE, CLASS_HOT);
  end if;
  COMMIT;    
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    log4me.info(v_proc ||  ' => no workspace found');
    NULL;
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    RAISE;
END retire;

PROCEDURE alloc(
  p_type    varchar2, 
  p_session varchar2,
  p_ws OUT  varchar2
) AS
  v_proc   constant varchar2(80) := 'alloc type: ' || p_type;
  v_sql    constant CLOB := '
    SELECT ws_name, ws_class
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_class = ''' || CLASS_HOT || '''
      AND ws_type  = ''' || p_type || '''
      AND status   in (''' || SPACE_FREE || ''',''' || SPACE_STALE || ''')
    ORDER BY updated_at desc
  ';
  v_this  varchar2(40);
  v_class varchar2(40);
  v_list CLOB := '';
  TYPE VCUR  IS REF CURSOR;
  c_ws VCUR;
BEGIN
  OPEN c_ws FOR v_sql;
  LOOP
    FETCH c_ws INTO v_this, v_class;
    EXIT WHEN c_ws%NOTFOUND;
    p_ws   := v_this;
    v_list := v_list || ', ' || p_ws;
  END LOOP;
  CLOSE c_ws;

  if p_ws is NULL then
    log4me.warn(
      'No free workspace left ' || 'or ws_type unsupported ' || v_proc
    );
  else
    log4me.debug('found workspaces ' || v_list);
    if (v_class <> SPACE_FREE) then 
      free(p_ws);
    end if;
    mark(p_ws, SPACE_BUSY, CLASS_HOT, p_session);
  end if;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    if c_ws%ISOPEN then CLOSE c_ws; end if;
    RAISE;
END alloc;
--
-- free a working table back to freelist
--
PROCEDURE  free(p_this varchar2) AS
  v_proc constant varchar2(80) := 'free ' || p_this;
  v_sql  constant varchar2(80) := 'TRUNCATE TABLE ' || p_this;
BEGIN
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql;
  --
  -- session_id from previous usage will be left after workspace freed
  -- for tracking until next alloc() request.
  --
  mark(p_this, SPACE_FREE, CLASS_HOT);
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    RAISE;
END free;
--
-- mark a working table for audit
--
PROCEDURE audit(p_this varchar2) AS
  v_proc constant varchar2(80) := 'audit ' || p_this;
BEGIN
  log4me.debug(v_proc);
  mark(p_this, SPACE_USED, CLASS_COOL);
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    RAISE;
END audit;
--
-- mark a working table for audit
--
PROCEDURE audit_by_session(p_session_id varchar2) 
AS
  v_proc   constant varchar2(80) := 'audit by session: ' || p_session_id;
  v_sql    constant CLOB := '
    SELECT ws_name
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND session_id = ''' || p_session_id || '''
      AND ws_class   = ''' || CLASS_WARM || '''
      AND status     = ''' || SPACE_USED || '''
      AND rownum     = 1
  ';
  v_this   varchar2(40) := NULL;
BEGIN
  log4me.debug(v_proc);
  EXECUTE IMMEDIATE v_sql INTO v_this;
  if (v_this IS NOT NULL) then
    audit(v_this);
  end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    log4me.info(v_proc || ' => no data found');
    NULL;
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    RAISE;
END audit_by_session;
--
-- copy content from p_this table to p_that table
--
PROCEDURE copy(p_this varchar2, p_that varchar2)
AS
  v_proc constant varchar2(200) := 'copy workspace ' || p_this || ' to ' || p_that;
  v_sql  constant CLOB  := '
    INSERT /*+ append */ INTO ' || p_that || '
    SELECT /*+ full(x) parallel */ *
    FROM ' || p_this || ' x
  ';
BEGIN
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    RAISE;
END copy;
--
-- copy content from p_this table to a parent table
-- using naming convension Ann -> Bn -> C
--
PROCEDURE push(p_this varchar2) AS
  v_rollup constant varchar2(40)  := upname(p_this);
  v_proc   constant varchar2(200) := 
    'push and free workspace ' || p_this || ' into ' || v_rollup;
BEGIN
  log4me.info(MSG_INIT || v_proc);
  mark(p_this, SPACE_BUSY, CLASS_WARM);
  copy(p_this, v_rollup);
  free(p_this);
  log4me.info(MSG_DONE || v_proc);
  --
  -- dbms_stats should be run by the caller to ensure query performance
  -- 
END push;

PROCEDURE push_all(p_type varchar2)
AS
  v_proc  constant varchar2(40) := 'push_all(' || p_type || ')';
  v_sql   constant CLOB := '
    SELECT ws_name
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_type = ''' || p_type || '''
      AND status in (''' || SPACE_USED || ''')
    ORDER BY updated_at
  ';
  v_this varchar2(40);
  TYPE VCUR  IS REF CURSOR;
  c_ws VCUR;
BEGIN
  OPEN c_ws FOR v_sql;
  LOOP
    FETCH c_ws INTO v_this;
    EXIT WHEN c_ws%NOTFOUND;
    push(v_this);
    COMMIT;
  END LOOP;
  CLOSE c_ws;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_proc);
    ROLLBACK;
    if c_ws%ISOPEN then CLOSE c_ws; end if;
    RAISE;
END push_all;

PROCEDURE remap(
  p_type varchar2,
  p_view varchar2,
  p_cond varchar2 DEFAULT ''
) AS
  v_proc constant varchar2(80) := 'create view ' || p_view || ' union all ' || p_type;
  v_sql_find constant CLOB := '
    SELECT ws_name
    FROM ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_class in (''' || CLASS_WARM || ''',''' || CLASS_COOL || ''')
      AND ws_type  = ''' || p_type     || '''
      AND status   = ''' || SPACE_USED || '''
    ORDER BY updated_at
  ';
  v_sql CLOB := '
    CREATE OR REPLACE VIEW ' || p_view || ' AS
    SELECT /*+ parallel */ * FROM ';

  TYPE VCUR  IS REF CURSOR;
  c_ws VCUR;

  v_ws    varchar2(40);
  v_list  CLOB    := '';
  v_base  boolean := true;
BEGIN
  --
  -- Note:
  --   This procedure deduces the base workspace (table) name by calling upname(..)
  --   Do double check the algorithm, it is based on a naming convention which is 
  --   subject to change over time. Without proper understanding, this could break 
  --   everything.
  --
  OPEN c_ws FOR v_sql_find;
  LOOP
    FETCH c_ws INTO v_ws;
    EXIT WHEN c_ws%NOTFOUND;

    v_list := v_list || ',' || v_ws;
    if v_base then 
      v_sql  := v_sql || upname(v_ws);
      v_base := false;
    end if;
    v_sql := v_sql || '
      UNION ALL
      SELECT /*+ parallel */ * FROM ' || v_ws || p_cond;
  END LOOP;
  CLOSE c_ws;

  if (v_list = '') then raise NO_DATA_FOUND; end if;

  log4me.debug('found ' || p_type || ' workspaces: ' || v_list);
  --
  -- execute the long UNION ALL view creation SQL
  --
  log4me.debug(v_proc, v_sql);
  EXECUTE IMMEDIATE v_sql;
  COMMIT;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    log4me.debug('no workspace found for: ' || p_type);
    return;
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err(MSG_FAIL || 'remaping view ' || p_view);
    if c_ws%ISOPEN then CLOSE c_ws; end if;
    RAISE;
END remap;

END xos_workspace_mgr;
/

