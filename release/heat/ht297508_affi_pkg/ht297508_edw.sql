--
-- Account Affinity Program Check Store Procedure
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht297508.log;

@xos_acct_affinity_fix.prc
@f_daily_affinity_program_check_orig.prc

SPOOL OFF

