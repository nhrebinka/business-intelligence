SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ht317533_0454_reagg.log;

declare
  prc   varchar2(40) := 'ht317533_0454_reagg ';
  odate date := to_date('20150615','YYYYMMDD');
begin
  log4me.info(prc || 'starting...');
  log4me.info('INIT account_agg ' || to_char(odate));
  f_daily_account_aggs_h_dfix(odate, odate);
  log4me.info('DONE account_agg ' || to_char(odate));
  f_revenue_aggregates(odate, odate);
  log4me.info(prc || 'done');
end;
/

SPOOL OFF
