--
-- HT290224 migration script
--
--   Site Agg fix to add 2014-07
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht290224_site_agg_fix.log;

alter session enable parallel dml;
begin
  -- 
  -- clean up the workspace
  --
  xos_workspace_mgr.free('X_D_SITE_A05');
  --
  -- add site agg 2014-07
  --
  xbs_site_agg.add_session_for_ym('2014-07');
  xbs_site_agg.run_one_session;

  xos_workspace_mgr.audit('X_D_SITE_A01');
  --
  -- run 2014-08 and to create the view
  --
  F_DAILY_SITE_AGGREGATES;
  commit;
end;
/

SPOOL OFF


