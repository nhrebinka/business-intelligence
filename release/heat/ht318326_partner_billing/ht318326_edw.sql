--
-- BI4.2 Partner Billing - EDW_OWNER
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ht318326_partner_billing.log APPEND;

@f_tbl_partner_billing

SPOOL OFF
