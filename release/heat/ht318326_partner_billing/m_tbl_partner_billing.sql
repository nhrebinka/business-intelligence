--
-- Wex BI4.0 - DDL for HUB partner billing tables
--
-- ------------------------------------------------------------------------
-- 
-- TABLE: M_SPNR_PGM
--
--ALTER TABLE M_SPNR_PGM ADD (PARTNER_BILLING_CUST_ID        VARCHAR2(200));
--ALTER TABLE M_SPNR_PGM ADD (PARTNER_BILLING_PRODUCT_NAME   VARCHAR2(200));

CREATE SEQUENCE PGM_BILLING_SNAPSHOT_KEY_SEQ
  START WITH 10020001
  MAXVALUE 999999999999999999999999999
  MINVALUE 10000001
  NOCYCLE
  CACHE 1000
  NOORDER;

GRANT SELECT, ALTER ON PGM_BILLING_SNAPSHOT_KEY_SEQ TO DWLOADER;




