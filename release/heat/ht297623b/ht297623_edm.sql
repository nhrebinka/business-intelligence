--
-- BI4.0 patch - script to grant select on EDM_OWNER_SELECT
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht255358_297623b.log;

GRANT SELECT ON t_ledger_temp TO edm_owner_select;

SPOOL OFF


