CREATE OR REPLACE PROCEDURE EDW_OWNER.F_DAILY_AFFINITY_PROGRAM_CHECK
AS
  /*
  NAME               :-   F_DAILY_AFFINITY_PROGRAM_CHECK
  Original AUTHOUR   :-   VENU KANCHARLA
  DATE               :-   06/18/2013

  DECRIPTION         :-   
    PROCEURE TO CHECK IF A  PROGRAM HAS BEEN MODIFIED FROM 701 
    Affinity to Something else ..? This is happening on the classic due to 
    the plastic code changes for Affinity Programs. This Procedure would 
    identify suck propgrams and correct the data appropriately.

  Revision History
  1.0 Venu Kancharla
  1.1 Venu Kancharla - Removed the Filter For Program Name So that it can 
                       handle all duplicate programs

  2.0 Venu Kancharla - added another Routine to handle the Duplicate Cards 
                       for Puchase Device Event
  2.1 20140604: CC   - add full hints to avoid the need for pre-run Stat 
                       on F_TRANSACTION_LINE_ITEM

  3.0 20141106: CC   - update to PROGRAM_NAME change; refactor code structure
  3.1 20141231: CC   - check both fact tables for duplicates, i.e.
                       F_TRANSACTION_LINE_ITEM and F_PURCHASE_DEVICE_EVENT.
  */
  X_PROC   constant varchar2(120) := 'affinity_program_check';
  X_SES_ID constant varchar2(40)  := to_char(systimestamp, 'YYYYMMDD.HH24MISS.FF4');
  -- vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
  --
  -- here are the default values that might need to be changed
  -- in the future depends on the slowly changing business rules
  --
  X_PGM_SPNR_NM constant varchar2(100) := '(''Affin 53'',''Affinity 54'')';
  X_OLD_PGM_KEY constant integer       := 701; -- PROGRAM_KEY for 'Affin 53'
  --
  -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  v_min_dt      date;
  v_max_dt      date;

  v_acct_key    integer;
  v_new_pgm_key integer;

  v_part        varchar2(20);
  v_sql         varchar2(32767);
  v_cnt         integer := 0;

  c_dup         SYS_REFCURSOR;

BEGIN
  log4me.info('*** INIT ' || X_PROC, X_SES_ID);
  --
  -- find the processing data from the seed table EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM
  --
  F_SEED_DATES(v_min_dt, v_max_dt);
  --
  -- Create cursor for Looping thru if we find multiple accounts for Affinity Changes ...
  --
  v_part := 'YM_' || to_char(v_min_dt, 'YYYY_MM');
  --
  -- The following Cursor is to identify the Duplicate programs occuriing on the Revenue Side
  --
  v_sql := '
    SELECT /*+ parallel(f, 64) use_hash(f, d) */ 
      UNIQUE f.ACCOUNT_KEY, f.PROGRAM_KEY
    FROM F_PURCHASE_DEVICE_EVENT f
    JOIN D_PROGRAM d ON (f.PROGRAM_KEY = d.PROGRAM_KEY)
    AND  d.PROGRAM_SPONSOR_NAME NOT IN ' || X_PGM_SPNR_NM || '
    AND  f.ACCOUNT_KEY IN (
      SELECT /*+ parallel(x, 64) */ ACCOUNT_KEY FROM (
        SELECT unique ACCOUNT_KEY, PROGRAM_KEY
        FROM   F_PURCHASE_DEVICE_EVENT x
      )
      GROUP BY ACCOUNT_KEY HAVING count(*) > 1
    )
    UNION 
    SELECT /*+ parallel(f, 64) */ 
      UNIQUE f.PURCHASE_ACCOUNT_KEY, f.PROGRAM_KEY
    FROM  F_TRANSACTION_LINE_ITEM PARTITION(' || v_part || ') f
    JOIN  D_PROGRAM d ON (f.PROGRAM_KEY = d.PROGRAM_KEY)
    WHERE d.PROGRAM_SPONSOR_NAME NOT IN ' || X_PGM_SPNR_NM || '
    AND   f.PURCHASE_ACCOUNT_KEY IN ( 
      SELECT PURCHASE_ACCOUNT_KEY FROM (
        SELECT /*+ parallel(x,64) */
           UNIQUE PURCHASE_ACCOUNT_KEY, PROGRAM_KEY
        FROM F_TRANSACTION_LINE_ITEM PARTITION(' || v_part || ') x
      )
      GROUP BY PURCHASE_ACCOUNT_KEY HAVING count(*) > 1
    )
  ';
  log4me.debug('starting ' || X_PROC, v_sql, X_SES_ID);
  OPEN c_dup FOR v_sql;
  LOOP
    FETCH c_dup INTO v_acct_key, v_new_pgm_key;
    EXIT WHEN c_dup%NOTFOUND;

    log4me.debug(
      X_PROC || ' dup found in FACT table(s). Fixing...', 
      'account_key=' || v_acct_key || ', program_key=' || v_new_pgm_key,
      X_SES_ID
    );
    --
    -- Correcting the account affinity program in fact tables with 
    -- the correct affinity program key
    --
    XOS_ACCOUNT_AFFINITY_FIX(
      'acct affinity[' || X_OLD_PGM_KEY || ' => ' || v_new_pgm_key || ']',
      v_acct_key,  -- the account that has two programs
      X_OLD_PGM_KEY,  -- the original program key that we want to replace
      v_new_pgm_key,  -- the new program key that we think it should be
      v_part          -- table partition if used in the updated tables
    );
    v_cnt := v_cnt + 1;
  END LOOP;
  CLOSE c_dup;
  log4me.info(
    '*** GOOD ' || X_PROC || ' => ' || v_cnt || ' key(s) updated', 
    X_SES_ID
  );
  --
  -- double check the result, in case we have process run in parallel
  -- in other session that we didn't know about
  --
  log4me.debug('validating ' || X_PROC, v_sql, X_SES_ID);
  v_cnt := 0;
  OPEN c_dup FOR v_sql;
  LOOP
    FETCH c_dup INTO v_acct_key, v_new_pgm_key;
    EXIT WHEN c_dup%NOTFOUND;
  
    log4me.info(
      X_PROC || 
      ' still dup found in FACT(s): ' ||
      'account_key=' || v_acct_key || ', program_key=' || v_new_pgm_key || 
      '. Ctrl-M schedule review needed!',
      X_SES_ID
    );
    v_cnt := v_cnt + 1;
  END LOOP;
  CLOSE c_dup;
  IF v_cnt=0 THEN
    log4me.debug(X_PROC || ' no more dup found in FACT(s).', X_SES_ID);
  END IF;
  log4me.info('*** DONE ' || X_PROC, X_SES_ID);

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  IF c_dup%ISOPEN THEN CLOSE c_dup; END IF;
  log4me.err('*** FAIL ' || X_PROC || SQLERRM, X_SES_ID);
END F_DAILY_AFFINITY_PROGRAM_CHECK;
/
show errors

-- ============================================================================
GRANT EXECUTE ON F_DAILY_AFFINITY_PROGRAM_CHECK TO DWLOADER;
