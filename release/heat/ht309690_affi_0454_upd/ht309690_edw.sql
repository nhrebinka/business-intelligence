--
-- Affinity Program Check Update for 0454
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht309690_affi_0454_upd.log;

@f_daily_affinity_program_check.prc

SPOOL OFF

