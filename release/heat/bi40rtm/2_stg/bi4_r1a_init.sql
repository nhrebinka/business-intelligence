--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- EDW.EDW_STAGE_OWNER one time setup
@stg_drp.ddl
@stg_tbl.ddl
@stg_gnt.ddl
