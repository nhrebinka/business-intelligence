/*
  Name : split function - String tokenization utilities
  Spec.: For splitting string into tokens by a given demiter

  Revision History: 
  1.0 20140813 CC - first edition
  1.1 20140814 CC - use reversed logic

  Note:
    1. This package does not validate correct use of delimiter.
       It assumes valid construction of lists.
*/
CREATE OR REPLACE TYPE TokList AS TABLE OF varchar2(2000);
/

CREATE OR REPLACE FUNCTION split(p_str varchar2, p_x varchar2 := '|')
RETURN TokList
AS
/*
  split() - Parse simple String into array of varchar2 (aka TokList)
            with a single delimiter (default to '|').
  Usage:
    
    select split('1|2|3|4') from dual;
    select split('1,2,3,4',',') from dual;
    
*/
  INC  constant integer := length(p_x);

  i1     integer := 1;
  i2     integer := instr(p_str, p_x, i1);
  v_list TokList := TokList();

  PROCEDURE add_tok(p_tok varchar2) AS 
  BEGIN
    v_list.extend(1);
    IF p_tok = p_x THEN
      v_list(v_list.COUNT) := NULL;
    ELSE
      v_list(v_list.COUNT) := p_tok;
    END IF;
  END;

BEGIN
  LOOP
    i2 := instr(p_str, p_x, i1);
    EXIT WHEN i2 IS NULL OR i2 = 0;

    add_tok(substr(p_str, i1, i2 - i1));

    i1 := i2 + INC;
  END LOOP;
  IF p_str IS NOT NULL THEN 
    add_tok(substr(p_str, i1, length(p_str)-i1+1));
  END IF;
  RETURN v_list;
END split;
/
show errors

CREATE OR REPLACE FUNCTION split2(p_str varchar2, p_x varchar2 := '|')
RETURN TokList
AS
  v_list TokList;
  v_pat  varchar2(40) := 
    '[^' || (CASE WHEN p_x = '|' THEN '\|' ELSE p_x  END) || ']+'; 
BEGIN
  WITH t AS (
    SELECT p_str str FROM dual
  )
  SELECT regexp_substr(str, v_pat, 1, ROWNUM) sp
  BULK COLLECT INTO v_list
  FROM t
  CONNECT BY LEVEL <= length(regexp_replace(str, v_pat))+1;
  
  RETURN v_list;
END split2;
/
show errors

CREATE OR REPLACE FUNCTION concat_rpad(
  p_toklist TokList, 
  p_x       char    := ',',
  p_rpad    integer := 0
)
RETURN varchar2
AS
/*
  concat_rpad() - concatenate TokList into a long varchar2
                  with optional right padding to each element
  Usage:
    select concat_rpad(split('1|2|3|4'),'~',10) from dual;
    
*/
  v_str varchar2(4000) := '';
BEGIN
  FOR i IN p_toklist.FIRST..p_toklist.LAST
  LOOP
    IF p_rpad > 0 THEN
      v_str := v_str || rpad(p_toklist(i), p_rpad, p_x);
    ELSE
      v_str := v_str || p_toklist(i);
      IF i < p_toklist.COUNT THEN
        v_str := v_str || p_x;
      END IF;
    END IF;
  END LOOP;

  return v_str;
END concat_rpad;
/
show errors;




