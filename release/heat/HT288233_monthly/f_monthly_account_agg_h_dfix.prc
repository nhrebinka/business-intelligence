CREATE OR REPLACE PROCEDURE F_MONTHLY_ACCOUNT_AGG_H_DFIX (
  p_year_month_abbr VARCHAR2
)
AS
/*
  NAME      : F_MONTHLY_ACCOUNT_AGG_H_DFIX
  DECRIPTION: One-time fix for monthly Max Outstanding Card Count

  REVISION  :
  1.0 20140716 Lee
      Deduced from F_MONTHLY_ACCOUNT_AGGREGATES and F_DAILY_ACCOUNT_AGG_H_DFIX.
*/
  SESSION_TYPE constant VARCHAR2(40) := 'F_MONTHLY_ACCOUNT_AGG_H_DFIX';
  SESSION_FMT  constant VARCHAR2(40) := 'YYYYMMDD.HH24MISS';
  FMT_DT       constant varchar2(20) := 'YYYY-MM-DD';

  v_min_dt DATE;
  v_max_dt DATE;

  v_ses_id VARCHAR2(40) := to_char(SYSTIMESTAMP, SESSION_FMT) || '.1';
  v_proc   VARCHAR2(80);
BEGIN
  --
  -- get date range from the input month
  --
  SELECT min(calendar_date_dt), max(calendar_date_dt)
  INTO   v_min_dt, v_max_dt
  FROM   D_DATE
  WHERE  month_year_abbr = p_year_month_abbr;

  v_proc := 
    SESSION_TYPE || ' ' || 
    to_char(v_min_dt, FMT_DT) || ' to ' ||
    to_char(v_max_dt, FMT_DT);
  log4me.info('INIT ' || v_proc, v_ses_id);

  MERGE INTO F_MONTHLY_ACCOUNT_SNAPSHOT mas USING (
    SELECT
      das.ACCOUNT_KEY,
      ldom.DATE_KEY,
      das.MAX_OUTSTANDING_CARDS_COUNT
    FROM F_DAILY_ACCOUNT_SNAPSHOT das
    JOIN (  
      --
      -- collect the latest date for each accounts
      --
      SELECT /*+ parallel(das) parallel(d) use_hash(d, das) */
        das.ACCOUNT_KEY                  AS ACCOUNT_KEY,
        MAX(d.DATE_KEY)                  AS MAX_DATE_KEY,
        MAX(d.LAST_DAY_IN_MONTH_DATE_DT) AS LAST_DT
      FROM F_DAILY_ACCOUNT_SNAPSHOT das, D_DATE d
      WHERE 1=1
        AND d.CALENDAR_DATE_DT BETWEEN v_min_dt AND v_max_dt
        AND das.DATE_KEY  = d.DATE_KEY
      GROUP BY das.ACCOUNT_KEY
    ) a ON (a.MAX_DATE_KEY = das.DATE_KEY AND a.ACCOUNT_KEY = das.ACCOUNT_KEY)
    JOIN D_DATE ldom ON (ldom.CALENDAR_DATE_DT = a.LAST_DT)
  ) ab ON (1=1
    --
    -- match by date and account
    --
    AND mas.ACCOUNT_KEY = ab.ACCOUNT_KEY
    AND mas.DATE_KEY    = ab.DATE_KEY
  )
  WHEN MATCHED THEN UPDATE
  SET  
    mas.MAX_OUTSTANDING_CARDS_COUNT = ab.MAX_OUTSTANDING_CARDS_COUNT
  ;
  COMMIT;

  log4me.info('DONE ' || v_proc, v_ses_id);
EXCEPTION
  WHEN OTHERS THEN
    log4me.err('FAIL ' || SQLCODE || ' ' || v_proc, v_ses_id);
END F_MONTHLY_ACCOUNT_AGG_H_DFIX;
/

