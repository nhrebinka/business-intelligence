--
-- Wex BI4.0 - Site 2014_08 redo due to missing Ancillary data
--
-- ------------------------------------------------------------------------
--
-- Upgrade audited 2014_05 into X_D_SITE_B
--
DECLARE
  v_type   varchar2(20) := 'SITE_AGG';
  v_ym     varchar2(10) := '2014-08';
  v_proc   varchar2(80) := 
    'Rerun ' || v_type || ' ' || v_ym || ' withn ancilliary data correction';
  v_ses_id varchar2(40);
BEGIN
  xos_workspace_mgr.push('X_D_SITE_A06');   -- 2014_06 data
  xos_workspace_mgr.free('X_D_SITE_A03');   -- 2014_08 data
 
  xbs_site_agg.add_session_for_ym(v_ym);
  xbs_site_agg.run_one_session;

  log4me.info('INIT ' || v_proc);

  xos_session_mgr.previous_session(v_type, v_ym, v_ses_id);
  IF v_ses_id IS NULL OR v_ses_id = '' THEN
    log4me.info('FAIL ' || v_proc || '=> no session found');
  ELSE
    xos_workspace_mgr.audit_by_session(v_ses_id);
    log4me.info('DONE ' || v_proc);
  END IF;
END;
/
