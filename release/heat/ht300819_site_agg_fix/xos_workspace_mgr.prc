/*
  Name : xos_workspace_mgr Package
  Spec.: Storage Allocation/Management for working tables sets

  Dependency:
    log4me package

  Revision Histroy:
  1.0 20140320 CC - first version
  1.1 20140410 CC - tune queries, add comments
  1.2 20140430 CC - migrate workspace entries to table creation script
  2.0 20140516 CC - add LRU-based cache management with state machine;
                    and enable audit() capability
  2.1 20140804 CC - change format of comments, and revision history
                    commented out table creation section (for reuse)
  2.2 20140904 CC - enforce session_id (or EMPTY_SES) logging;
                    ensure excption err logging with SQLERRM
*/
-- ===========================================================================
/*
DROP   TABLE xo_workspace;
CREATE TABLE xo_workspace
(
  ws_class   varchar2(10) NOT NULL,
  ws_type    varchar2(40) NOT NULL,
  ws_name    varchar2(40) NOT NULL,
  session_id varchar2(40) NOT NULL,
  status     varchar2(20) NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL
)
;
--
-- make sure X_D_SITE_Ann creation script does this
--
INSERT INTO xo_workspace VALUES (
  'HOT', 'D_SITE_AGG', 'X_D_SITE_A00', 'FREE', sysdate, sysdate
);
*/
-- ===========================================================================
CREATE OR REPLACE PACKAGE xos_workspace_mgr AS
/*
  Storage Allocation/Management for working tables sets
  
    Similar to how OS manages its memory, an ODS/DW system can manage its
    busy_list and free_list of working block sets. By utilizing different 
    storage technology, these working sets can be keep on SSD, Striped
    RAID1/0, RAID5, or even optical silos for performance/cost effectiveness.
    80/20 rule is a good rule of thumb for distribution. 90/10 will work 
    well when we get really big i.e. like Facebook.
     
    The main driver behind is to use naming convention for space allocation
    scheme to reduce the complexity for management in the long run. The
    cascading workspace (or tables in RDBMS) with various hotness for their
    different storage/retrieval requirements and are named as follows

      COOL   WARM   HOT
        C <-  B0 <- A00   i.e. FM_SITE_C, FM_SITE_B0, FM_SITE_A00,...
          <-  B1 <- A01
                 <- A02
                 <- ...
                 <- Ann
  
    A given workspace goes through a life cycle (state machine) of the 
    followings:
      FREE - available for taking, a session will pick from a pool of these
      BUSY - is currently been worked on, process failure will return this 
             back to FREE or transition to USED when success
      USED - a workspace that is occupied with valid data
  
    The workspace state machine is depicted as following
  
           +-----free---- (HOT,STALE) --retire-+
           |                                   |
           v                                   |
      (HOT,FREE) -alloc-> (HOT,BUSY) -agg-> (WARM,USED)
           ^                                  |   |
           |                                  |   |
           +--push--- (WARM,BUSY) <--push-----+   |
                          ^                      audit
                          |                       |
                          +--push-- (COOL,USED) --+
     
  Spec:
    add()   - add new workspace to the xo_workspace table which is managed by
              this package
    mark()  - mark the workspace with a specific status
    alloc() - allocate a FREE workspace for process (mark BUSY)
    free()  - release the workspace back to FREE pool
  
    copy()  - copy content from one workspace to other table
  
    push()  - push the content of a workspace to its parent
              using the naming convention
    push_all() - push all the content of typed workspaces upto the parent
              
    remap() - is a short-hand for push()+free() following by remapping a view
              on top of resulting tables
    recompile_dependents() - recompile dependent objects of a view
  
    retire()- mark the oldest workspace as stale for later usage
    audit() - mark workspace (safe copy) for auditing
  
  Note:
    1. For implementation, individual table is used as a block currently
    2. The naming of blocks are designed using "convention over configuration",
       i.e. FD_SITE_An, FD_SITE_Bn, FD_SITE_C as HOT, WARM, and COLD sections
       for site aggregation tables. Other families of aggregation tables work
       the same way
    3. session_id from previous usage will be left after workspace freed
       for tracking until next alloc() request.
*/

  MSG_INIT   constant varchar2(10) := 'INIT ';
  MSG_DONE   constant varchar2(10) := 'DONE ';
  MSG_FAIL   constant varchar2(10) := 'FAIL ';

  SPACE_FREE constant varchar2(10) := 'FREE';
  SPACE_BUSY constant varchar2(10) := 'BUSY';
  SPACE_USED constant varchar2(10) := 'USED';
  SPACE_STALE constant varchar2(10) := 'STALE';

  CLASS_HOT  constant varchar2(10) := 'HOT';
  CLASS_WARM constant varchar2(10) := 'WARM';
  CLASS_COOL constant varchar2(10) := 'COOL';
  CLASS_COLD constant varchar2(10) := 'COLD';

  EMPTY_SES  constant varchar2(10) := '0.0.0';
  --
  -- 'soft' storage management
  --
  PROCEDURE add(
    p_class varchar2,
    p_type  varchar2,
    p_table varchar2
  );
  FUNCTION  upname(p_this varchar2) RETURN varchar2;
  PROCEDURE mark(
    p_ws_name varchar2,
    p_status  varchar2,
    p_class   varchar2,
    p_ses_id  varchar2 := EMPTY_SES
  );
  PROCEDURE alloc(
    p_type    varchar2, 
    p_ses_id  varchar2,
    p_ws OUT  varchar2
  );
  --
  -- 'hard' storage management
  --
  PROCEDURE copy(p_this varchar2, p_that   varchar2);
  PROCEDURE free(p_this varchar2, p_ses_id varchar2 := EMPTY_SES);
  PROCEDURE push(p_this varchar2);
  PROCEDURE push_all(p_type varchar2);
  --
  -- view management
  --
  PROCEDURE remap(
    p_type   varchar2, 
    p_view   varchar2,
    p_cond   varchar2 := '',
    p_ses_id varchar2 := EMPTY_SES
  );

  PROCEDURE recompile_dependents(
    p_view   varchar2,
    p_ses_id varchar2 := EMPTY_SES
  );
  --
  -- LRU-based cache management
  --
  PROCEDURE retire(p_type varchar2, p_ses_id varchar2 := EMPTY_SES);
  PROCEDURE audit (p_this varchar2, p_ses_id varchar2 := EMPTY_SES);
  PROCEDURE audit_by_session(p_ses_id varchar2);
END xos_workspace_mgr;
/
show errors

-- ===========================================================================
-- Package Body
-- ===========================================================================
CREATE OR REPLACE PACKAGE BODY xos_workspace_mgr AS
  WSPACE_TB constant varchar2(40) := 'xo_workspace';
  VIEW_PFIX constant varchar2(3)  := '_VW';

PROCEDURE add(
  p_class varchar2,
  p_type  varchar2,
  p_table varchar2
)
AS
  X_PRC constant varchar2(80) := 'add workspace ' || p_table;
  X_SQL constant CLOB := '
    INSERT INTO ' || WSPACE_TB || '
    VALUES(:c, :t, :w, :s, ''FREE'', sysdate, sysdate)
  ';
BEGIN
  log4me.debug(X_PRC, X_SQL);
  EXECUTE IMMEDIATE X_SQL USING p_class, p_type, p_table, EMPTY_SES;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(MSG_FAIL || X_SQL, EMPTY_SES);
  RAISE;
END add;
--
-- use naming convension to get the rollup table name
--
FUNCTION upname(p_this varchar2) RETURN varchar2
AS
  v_type varchar2(40) := substr(p_this, 1, length(p_this)-3); -- table type
  v_up   char(1)      := chr(ascii(substr(p_this, -3, 1))+1); -- next char
  v_nn   integer      := to_number(substr(p_this, -2));       -- not used now
  v_that varchar2(40) := v_type || v_up;
BEGIN
  RETURN v_that;
END;
--
-- allocate a working table from freelist
--
PROCEDURE mark(
  p_ws_name varchar2, 
  p_status  varchar2,
  p_class   varchar2,
  p_ses_id  varchar2 := EMPTY_SES
) AS
  X_PRC constant varchar2(80) := 
    'mark ' || p_ws_name || ' as [' || p_class || ',' || p_status || ']';

  X_SQL constant CLOB := '
    UPDATE ' || WSPACE_TB || ' SET
      WS_CLASS   = ''' || NVL(p_class, '') || ''',
      SESSION_ID = ''' || NVL(p_ses_id, EMPTY_SES) || ''',
      STATUS     = ''' || NVL(p_status,'') || ''', 
      UPDATED_AT = sysdate
    WHERE ws_name = ''' || NVL(p_ws_name,'') || '''
  ';
BEGIN
  log4me.debug(X_PRC, X_SQL, p_ses_id);
  EXECUTE IMMEDIATE X_SQL;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  RAISE;
END mark;
--
-- find and retire the oldest USED workspace by marking it STALE (LRU)
--
PROCEDURE retire(p_type varchar2, p_ses_id varchar2 := EMPTY_SES)
AS
  X_PRC constant varchar2(80) := 'mark oldest ' || p_type || ' stale';
  X_SQL constant CLOB := '
    SELECT ws_name FROM (
      SELECT ws_name
      FROM   ' || WSPACE_TB || '
      WHERE 1=1 
        AND ws_class = ''' || CLASS_WARM     || '''
        AND ws_type  = ''' || NVL(p_type,'') || '''
        AND status   = ''' || SPACE_USED     || '''
      ORDER BY updated_at ASC  -- the least recent used
    )
    WHERE ROWNUM = 1
  ';
  v_this  varchar2(40) := NULL;
BEGIN
  log4me.debug(X_PRC, X_SQL, p_ses_id);
  EXECUTE IMMEDIATE X_SQL INTO v_this;
  --
  -- keep the last entry in cache, mark the others STALE
  --
  if (v_this is NOT NULL) then
    mark(v_this, SPACE_STALE, CLASS_HOT, p_ses_id);
  end if;
  COMMIT;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.warn(X_PRC ||  ' => no workspace found', p_ses_id);
  NULL;
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  RAISE;
END retire;

PROCEDURE alloc(
  p_type    varchar2, 
  p_ses_id  varchar2,
  p_ws OUT  varchar2
) AS
  X_PRC constant varchar2(80) := 'alloc type: ' || p_type;
  X_SQL constant CLOB := '
    SELECT ws_name, ws_class
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_class = ''' || CLASS_HOT || '''
      AND ws_type  = ''' || p_type || '''
      AND status in (''' || SPACE_FREE || ''',''' || SPACE_STALE || ''')
    ORDER BY updated_at desc
  ';
  v_this  varchar2(40);
  v_class varchar2(40);
  v_list CLOB := '';

  c_ws SYS_REFCURSOR;
BEGIN
  OPEN c_ws FOR X_SQL;
  --
  -- get all the workspace ordered reversely based on updated_at
  --
  LOOP
    FETCH c_ws INTO v_this, v_class;
    EXIT WHEN c_ws%NOTFOUND;
    p_ws   := v_this;
    v_list := v_list || ', ' || p_ws;
  END LOOP;
  CLOSE c_ws;
  --
  -- pointing  p_ws at the 'oldest' availabe workspace
  --
  if p_ws is NULL then
    log4me.warn(
      'No free workspace left ' || 'or ws_type unsupported ' || X_PRC, 
      p_ses_id
    );
  else
    log4me.debug('found workspaces ' || v_list, '', p_ses_id);
    if (v_class <> SPACE_FREE) then 
      free(p_ws, p_ses_id);
    end if;
    --
    -- take the "oldest" available workspace
    --
    mark(p_ws, SPACE_BUSY, CLASS_HOT, p_ses_id);
  end if;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  if c_ws%ISOPEN then CLOSE c_ws; end if;
  RAISE;
END alloc;
--
-- free a working table back to freelist
--
PROCEDURE  free(
  p_this   varchar2,
  p_ses_id varchar2 := EMPTY_SES
) AS
  X_PRC constant varchar2(80) := 'free ' || p_this;
  X_SQL constant varchar2(80) := 'TRUNCATE TABLE ' || p_this;
BEGIN
  log4me.debug(X_PRC, X_SQL, p_ses_id);
  EXECUTE IMMEDIATE X_SQL;
  --
  -- session_id from previous usage will be left after workspace freed
  -- for tracking until next alloc() request.
  --
  mark(p_this, SPACE_FREE, CLASS_HOT, p_ses_id);
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  --
  -- no rolling back but we could have transaction inconsistancy here
  -- the table is truncated but the table is not marked FREE
  --
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  RAISE;
END free;
--
-- mark a working table for audit
--
PROCEDURE audit(p_this varchar2, p_ses_id varchar2 := EMPTY_SES) AS
  X_PRC constant varchar2(80) := 'audit ' || p_this;
BEGIN
  log4me.debug(X_PRC, '', p_ses_id);
  mark(p_this, SPACE_USED, CLASS_COOL, p_ses_id);
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  -- no rollback needed, always forward
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  RAISE;
END audit;
--
-- mark a working table for audit
--
PROCEDURE audit_by_session(p_ses_id varchar2) 
AS
  X_PRC constant varchar2(80) := 'audit by session: ' || p_ses_id;
  X_SQL constant CLOB := '
    SELECT ws_name
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND session_id = ''' || p_ses_id   || '''
      AND ws_class   = ''' || CLASS_WARM || '''
      AND status     = ''' || SPACE_USED || '''
      AND rownum     = 1
  ';
  v_this   varchar2(40) := NULL;
BEGIN
  log4me.debug(X_PRC, X_SQL, p_ses_id);
  EXECUTE IMMEDIATE X_SQL INTO v_this;
  if (v_this IS NOT NULL) then
    audit(v_this, p_ses_id);
  end if;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.warn(X_PRC || ' => no data found', p_ses_id);
  NULL;
WHEN OTHERS THEN
  --
  -- no rolling back but we could have partial committed case
  -- i.e. some workspaces are entered into AUDIT state but some are not
  -- 
  log4me.err(MSG_FAIL || X_PRC, p_ses_id);
  RAISE;
END audit_by_session;
--
-- copy content from p_this table to p_that table
--
PROCEDURE copy(p_this varchar2, p_that varchar2)
AS
  X_PRC constant varchar2(200) := 'copy workspace ' || p_this || ' to ' || p_that;
  X_SQL  constant CLOB  := '
    INSERT /*+ append */ INTO ' || p_that || '
    SELECT /*+ full(x) parallel */ *
    FROM ' || p_this || ' x
  ';
BEGIN
  log4me.debug(X_PRC, X_SQL);
  EXECUTE IMMEDIATE X_SQL;
  COMMIT;

EXCEPTION
WHEN OTHERS THEN
  -- we have to rollback for partial insert should anything goes wrong
  ROLLBACK;
  log4me.err(MSG_FAIL || X_PRC);
  RAISE;
END copy;
--
-- copy content from p_this table to a parent table
-- using naming convension Ann -> Bn -> C
--
PROCEDURE push(p_this varchar2) AS
  X_ROLLUP constant varchar2(40)  := upname(p_this);
  X_PRC    constant varchar2(200) := 
    'push and free workspace ' || p_this || ' into ' || X_ROLLUP;
BEGIN
  --
  -- The following should be grouped in a transaction
  -- but since we have so many logging in-between, it is not possible
  -- to behave like one. i.e. we could have the procedure partially done
  -- and left the stat of workspace inconsistant. This requires a 
  -- separate process to do scheduled check against the workspaces.
  --
  log4me.info(MSG_INIT || X_PRC);
  mark(p_this, SPACE_BUSY, CLASS_WARM);
  copy(p_this, X_ROLLUP);
  free(p_this);
  log4me.info(MSG_DONE || X_PRC);
  --
  -- dbms_stats should be run by the caller to ensure query performance
  -- 
END push;

PROCEDURE push_all(p_type varchar2)
AS
  X_PRC constant varchar2(40) := 'push_all(' || p_type || ')';
  X_SQL constant CLOB := '
    SELECT ws_name
    FROM   ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_type = ''' || p_type || '''
      AND status in (''' || SPACE_USED || ''')
    ORDER BY updated_at
  ';
  v_this varchar2(40);

  c_ws SYS_REFCURSOR;
BEGIN
  OPEN c_ws FOR X_SQL;
  LOOP
    FETCH c_ws INTO v_this;
    EXIT WHEN c_ws%NOTFOUND;
    push(v_this);
    COMMIT;
  END LOOP;
  CLOSE c_ws;

EXCEPTION
WHEN OTHERS THEN
  --
  -- no rolling back here but we could have some workspaces push
  -- but some are not
  --
  log4me.err(MSG_FAIL || X_PRC);
  if c_ws%ISOPEN then CLOSE c_ws; end if;
  RAISE;
END push_all;

PROCEDURE remap(
  p_type   varchar2,
  p_view   varchar2,
  p_cond   varchar2 := '',
  p_ses_id varchar2 := EMPTY_SES
) AS
  X_PRC     constant varchar2(80) := 'create view ' || p_view || ' union all ' || p_type;
  X_SQL_FIND constant CLOB := '
    SELECT ws_name
    FROM ' || WSPACE_TB || '
    WHERE 1=1 
      AND ws_class in (''' || CLASS_WARM || ''',''' || CLASS_COOL || ''')
      AND ws_type  = ''' || p_type     || '''
      AND status   = ''' || SPACE_USED || '''
    ORDER BY updated_at
  ';
  v_sql CLOB := '
    CREATE OR REPLACE VIEW ' || p_view || ' AS
    SELECT /*+ parallel */ * FROM ';

  c_ws SYS_REFCURSOR;

  v_ws    varchar2(40);
  v_list  CLOB    := '';
  v_base  boolean := true;
BEGIN
  --
  -- Note:
  --   This procedure deduces the base workspace (table) name by calling upname(..)
  --   Do double check the algorithm, it is based on a naming convention which is 
  --   subject to change over time. Without proper understanding, this could break 
  --   everything.
  --
  OPEN c_ws FOR X_SQL_FIND;
  LOOP
    FETCH c_ws INTO v_ws;
    EXIT WHEN c_ws%NOTFOUND;

    v_list := v_list || ',' || v_ws;
    if v_base then 
      v_sql  := v_sql || upname(v_ws) || ' ' || p_cond;
      v_base := false;
    end if;
    v_sql := v_sql || '
      UNION ALL
      SELECT /*+ parallel */ * FROM ' || v_ws || ' ' || p_cond;
  END LOOP;
  CLOSE c_ws;

  if (v_list = '') then raise NO_DATA_FOUND; end if;

  log4me.debug('found ' || p_type || ' workspaces: ' || v_list, '', p_ses_id);
  --
  -- execute the long UNION ALL view creation SQL
  --
  log4me.debug(X_PRC, v_sql, p_ses_id);
  EXECUTE IMMEDIATE v_sql;
  COMMIT;

EXCEPTION
WHEN NO_DATA_FOUND THEN
  log4me.warn('no workspace found for: ' || p_type, p_ses_id);
  return;
WHEN OTHERS THEN
  -- no rollback, going forward only
  log4me.err(MSG_FAIL || 'remaping view ' || p_view, p_ses_id);
  if c_ws%ISOPEN then CLOSE c_ws; end if;
  RAISE;
END remap;

PROCEDURE recompile_dependents(
  p_view   varchar2,
  p_ses_id varchar2 := EMPTY_SES
)
AS
  X_SQL constant CLOB := '
    SELECT name FROM USER_DEPENDENCIES
    WHERE  1=1
    AND    TYPE = ''VIEW''
    AND    REFERENCED_TYPE = ''VIEW''
    AND    REFERENCED_NAME = ''' || p_view || '''
    AND    DEPENDENCY_TYPE = ''HARD''';
  v_msg  varchar2(200) := '';
  v_proc varchar2(200) := '';
  v_dept varchar2(80);

  c_ws SYS_REFCURSOR;
BEGIN
  -- --------------------------------------------------------
  -- Due to daily view been replaced every session run,
  -- the dependent views are invalidated. It is required
  -- to recompile the dependent views before the user can
  -- access to the newly updated content.
  -- It is better to use workspace_mgr to do the update
  -- but we do not have enough time to do that within the 
  -- release timeframe. So, backlog (aka 'TODO' list) entered.
  -- --------------------------------------------------------
  OPEN c_ws FOR X_SQL;
  LOOP
    FETCH c_ws INTO v_dept;
    EXIT WHEN c_ws%NOTFOUND;
    v_msg  := 'dependent view ' || v_dept || ' recompiled';
    v_proc := 'ALTER VIEW ' || v_dept || ' COMPILE';
    EXECUTE IMMEDIATE v_proc;
    log4me.info(v_msg, p_ses_id);
  END LOOP;
  CLOSE c_ws;

EXCEPTION
WHEN OTHERS THEN
  log4me.err(MSG_FAIL || v_proc, p_ses_id);
  if c_ws%ISOPEN then CLOSE c_ws; end if;
  RAISE;
END recompile_dependents;

END xos_workspace_mgr;
/
show errors

-- ============================================================================
GRANT EXECUTE ON xos_workspace_mgr TO DWLOADER;
GRANT EXECUTE ON xos_workspace_mgr TO EDW_OWNER_SELECT;
