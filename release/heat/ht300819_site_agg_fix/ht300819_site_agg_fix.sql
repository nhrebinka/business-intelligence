--
-- Account Max Outstanding Card Count fix - migration script 
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht300819_site_agg_fix.log;

@xos_workspace_mgr.prc
@xos_session_mgr.prc
@xbs_site_agg.prc

alter session enable parallel dml;

begin
  update edw_owner.xo_session set status='FAIL' where status='INIT';
  commit;

  f_daily_site_aggregates;
end;
/

SPOOL OFF

