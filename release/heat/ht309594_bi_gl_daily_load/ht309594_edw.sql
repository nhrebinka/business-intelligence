--
-- 2013-2015 Aggregates Rerun: due to GL MON_BY_CATEGORY agg. error
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht3095945_bi_gl_daily_load.log;

@f_gl_acct_mon_by_category_load.prc

SPOOL OFF

