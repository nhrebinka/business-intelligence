--
-- P1 HT297803 migration script
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht297803_fix.log;

@xos_account_affinity_fix.prc

DECLARE
  v_proc    varchar2(80) :=  'One-Shot Affinity Program Fix 70=>701';
  v_acct    number       := 11599747;
  v_pgm_in  number       := 70;
  v_pgm_out number       := 701;
BEGIN
  xos_account_affinity_fix(v_proc, v_acct, v_pgm_in, v_pgm_out);
END;
/
show errors

SPOOL OFF


