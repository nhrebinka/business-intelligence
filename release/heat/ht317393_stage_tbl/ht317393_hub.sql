SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ht317393_stage_tbl.log;

update edm_owner.d_pos_and_site 
set source_system_code = 'SIEBEL',
    row_last_mod_dttm = sysdate,
    row_last_mod_proc_nm = 'update SRC NULL'
where source_system_code is null;

commit;

SPOOL OFF
