SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ht317393_stage_tbl.log;

insert into edw_owner.d_pos_and_site
select * from link_owner.d_pos_and_site
where row_last_mod_proc_nm = 'update SRC NULL';

commit;

SPOOL OFF
