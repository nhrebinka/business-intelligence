SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/ht317393_stage_tbl.log;

alter table stg_nacs_grouping modify spend_fuel_kwh_amount       null;
alter table stg_nacs_grouping modify fuel_kwh_interchange_amount null;

SPOOL OFF

