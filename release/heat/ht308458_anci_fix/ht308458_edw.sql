--
-- 2015-02 Aggregates Rerun due to Ancillary fix
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht308458_anci_fix.log;

ALTER SESSION ENABLE PARALLEL DML;

DECLARE
  x_prc varchar2(100) := '2015-02 Ancillary Fix - Agg Rerun';
  x_dt1 date := to_date('20150216', 'YYYYMMDD');
  x_dt2 date := to_date('20150228', 'YYYYMMDD');
BEGIN
  log4me.info('INIT ' || x_prc);
  --
  -- get date list with manual rebates
  --
  f_revenue_aggregates(x_dt1, x_dt1, false);
  f_revenue_aggregates(x_dt2, x_dt2);

  xbs_site_agg.rerun('2015-02');

  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

