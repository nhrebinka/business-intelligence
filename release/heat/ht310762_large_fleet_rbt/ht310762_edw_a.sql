--
-- 2013-2015 Revenue Agg Rerun: due to Large Fleet Rebate
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht310762_large_fleet_rbt.log;

ALTER SESSION ENABLE PARALLEL DML;

DECLARE
  x_prc constant varchar2(100) := 'HT310762: Large Fleet Rebate - RevAgg Rerun';
  v_dt  date;
BEGIN
  log4me.info('INIT ' || x_prc);

  for y in 2013..2015 loop
    for m in 1..12 loop
      continue when y=2015 and m>4;
      v_dt := to_date(to_char(y) || to_char(m, '00') || '01', 'YYYYMMDD');
      log4me.info('rev_agg=>' || to_char(v_dt,'YYYY-MM'));
      f_revenue_aggregates(v_dt, v_dt);  -- rebuild a whole month
    end loop;
  end loop;
  log4me.info('DONE ' || x_prc);

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

