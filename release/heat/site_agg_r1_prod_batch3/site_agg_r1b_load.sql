-- add historical site agg sessions
exec xbs_site_agg.add_session_for_ym('2009-12');
exec xbs_site_agg.add_session_for_ym('2009-11');
exec xbs_site_agg.add_session_for_ym('2009-10');
exec xbs_site_agg.add_session_for_ym('2009-09');
exec xbs_site_agg.add_session_for_ym('2009-08');
exec xbs_site_agg.add_session_for_ym('2009-07');
exec xbs_site_agg.add_session_for_ym('2009-06');
exec xbs_site_agg.add_session_for_ym('2009-05');
exec xbs_site_agg.add_session_for_ym('2009-04');
exec xbs_site_agg.add_session_for_ym('2009-03');
exec xbs_site_agg.add_session_for_ym('2009-02');
exec xbs_site_agg.add_session_for_ym('2009-01');
exec xbs_site_agg.add_session_for_ym('2008-12');
exec xbs_site_agg.add_session_for_ym('2008-11');
exec xbs_site_agg.add_session_for_ym('2008-10');
exec xbs_site_agg.add_session_for_ym('2008-09');
exec xbs_site_agg.add_session_for_ym('2008-08');
exec xbs_site_agg.add_session_for_ym('2008-07');
exec xbs_site_agg.add_session_for_ym('2008-06');
exec xbs_site_agg.add_session_for_ym('2008-05');
exec xbs_site_agg.add_session_for_ym('2008-04');
exec xbs_site_agg.add_session_for_ym('2008-03');
exec xbs_site_agg.add_session_for_ym('2008-02');
exec xbs_site_agg.add_session_for_ym('2008-01');
