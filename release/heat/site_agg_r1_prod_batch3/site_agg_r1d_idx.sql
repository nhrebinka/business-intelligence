--
-- to rebuild
-- ALTER INDEX x_d_site_b_ui1 REBUILD PARTITION YM_2013_01 PARALLEL 12;
--
alter index x_d_site_b_ui1 rebuild partition ym_2009_12;
alter index x_d_site_b_ui1 rebuild partition ym_2009_11;
alter index x_d_site_b_ui1 rebuild partition ym_2009_10;
alter index x_d_site_b_ui1 rebuild partition ym_2009_09;
alter index x_d_site_b_ui1 rebuild partition ym_2009_08;
alter index x_d_site_b_ui1 rebuild partition ym_2009_07;
alter index x_d_site_b_ui1 rebuild partition ym_2009_06;
alter index x_d_site_b_ui1 rebuild partition ym_2009_05;
alter index x_d_site_b_ui1 rebuild partition ym_2009_04;
alter index x_d_site_b_ui1 rebuild partition ym_2009_03;
alter index x_d_site_b_ui1 rebuild partition ym_2009_02;
alter index x_d_site_b_ui1 rebuild partition ym_2009_01;
alter index x_d_site_b_ui1 rebuild partition ym_2008_12;
alter index x_d_site_b_ui1 rebuild partition ym_2008_11;
alter index x_d_site_b_ui1 rebuild partition ym_2008_10;
alter index x_d_site_b_ui1 rebuild partition ym_2008_09;
alter index x_d_site_b_ui1 rebuild partition ym_2008_08;
alter index x_d_site_b_ui1 rebuild partition ym_2008_07;
alter index x_d_site_b_ui1 rebuild partition ym_2008_06;
alter index x_d_site_b_ui1 rebuild partition ym_2008_05;
alter index x_d_site_b_ui1 rebuild partition ym_2008_04;
alter index x_d_site_b_ui1 rebuild partition ym_2008_03;
alter index x_d_site_b_ui1 rebuild partition ym_2008_02;
alter index x_d_site_b_ui1 rebuild partition ym_2008_01;
