-- 
-- SDLC 15.1 TABLE DDL for EDW_STAGE_OWNER
--
set serveroutput on
set pagesize 0
set linesize 2000

declare 
  x_prc varchar2(200) := 'SDLC 15.1 EDW_STAGE_OWNER DDL';
  g_cnt integer       := 1;

  procedure exec_sql(p_sql varchar2) is
  begin
    edw_owner.log4me.debug(x_prc, p_sql);
    execute immediate p_sql;

  exception when others then
    edw_owner.log4me.err(x_prc);
    null;
  end exec_sql;

  procedure enable_not_null(p_tb varchar2) is
    v_tb  varchar2(200)  := 'alter table ' || p_tb;
    v_sql varchar2(2000);
  begin
    for r in (
      select column_name, data_type from user_tab_cols
      where table_name = p_tb
      and nullable = 'Y'
      and column_name not like 'ROW_%'
      order by 2,1
    )
    loop
      v_sql :=
      case
      when r.data_type = 'DATE' then
        v_tb || ' modify (' || r.column_name || ' not null)'
      when r.data_type = 'NUMBER' and r.column_name like '%KEY' then
        v_tb || ' modify (' || r.column_name || ' not null)'
      when r.data_type = 'NUMBER' then
        v_tb || ' modify (' || r.column_name || ' default 0 not null)'
      else
        ''
      end;
      if v_sql is not null then
        exec_sql(v_sql);
      else
        edw_owner.log4me.debug(x_prc || ' ' || r.column_name || ' no mod');
      end if;
    end loop;
  end enable_not_null;

  procedure add_column(p_tb varchar2, p_cols varchar2) is
    v_tmp varchar2(40);
  begin
    exec_sql('alter table ' || p_tb || ' add (' || p_cols || ')');
    return;

$if $$theoldway $then
    v_tmp := 'stg_bk' || g_cnt;
    g_cnt := g_cnt + 1;
    --
    --  the following is the 'hard' way we used before
    --
    exec_sql('create table /*+ append */ ' || v_tmp || ' as /*+ parallel */ select * from ' || p_tb);
    commit;

    exec_sql('truncate table ' || p_tb);
    enable_not_null(p_tb);

    exec_sql('insert into /*+ append */ ' || p_tb || ' select * from ' || v_tmp);
    commit;
--    exec_sql('drop table ' || v_tmp);
$end
  end add_column;

begin
  edw_owner.log4me.info(x_prc || ' INIT');
  add_column(
    'F_TRANSACTION_LINE_ITEM', '
      RETAIL_PPU_AMOUNT           NUMBER(15,5) DEFAULT 0 NOT NULL,
      ADJUSTED_PPU_AMOUNT         NUMBER(15,5) DEFAULT 0 NOT NULL,
      BETTER_PRICE_PPU_AMOUNT     NUMBER(15,5) DEFAULT 0 NOT NULL
/*
      ,
      EV_TRANSACTION_FEE_AMOUNT   NUMBER(15,2) DEFAULT 0 NOT NULL,
      SPEND_FUEL_KWH_AMOUNT       NUMBER(15,2) DEFAULT 0 NOT NULL,
      INTERCHANGE_FUEL_KWH_AMT    NUMBER(15,2) DEFAULT 0 NOT NULL,
      FUEL_KWH_QTY                NUMBER(15,5) DEFAULT 0 NOT NULL
*/
    '
  );
  add_column(
    'STG_NACS_GROUPING', '
      SPEND_FUEL_KWH_AMOUNT       VARCHAR2(4) DEFAULT 0 NOT NULL,
      FUEL_KWH_INTERCHANGE_AMOUNT VARCHAR2(4) DEFAULT 0 NOT NULL
    '
  );
  edw_owner.log4me.info(x_prc || ' DONE');
end;
/
show errors


