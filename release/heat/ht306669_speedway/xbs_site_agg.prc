-- ============================================================================
CREATE OR REPLACE PACKAGE xbs_site_agg AS
/*
  Daily site/program/SIC measurement aggregation 
   
  This is the aggregation code for daily site aggregation. It utilizes
  workspace_mgm package for storage management and by using the naming 
  convention it aggregate the data onto the "parent" table. The procedures
  comes with a dry-run concept which one can optionally dry run the process
  without actually going through the data set which typically takes a 
  long time. The concept can be expanded into sampling certain percentage
  of the full dataset for quick peek or data profiling.
  
  A typical use case is, per request or schedule, to add new session into
  session list via using session_mgr package and let the workflow control 
  takes it forward through the following states by issuing a 
  run_one_session() call.
  
    INIT - the process has been initialized which means it has picked up
           a valid SITE_AGG session to work on and is currently busy
           paddling through the mud.
    GOOD - the process has finished the main part of data creation, and
           is doing a dbms_stats on the given result set.
    DONE - the stats is done, a new view has been remapped. we should be
           able to use the content safely now
    FAIL - the process has failed for some reason that one needs to look
           into xo_process_log to figure out.
  
  Package dependency
    log4me
    xos_workspace
    xos_session

  Spec:
    add_session_from_seed() - add a session by peeking into the optional
           seed table default to EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM.
    add_session_for_ym() - add a session with specific year month abbr.
           i.e. '2013-06'.
  
    run_by_date() - a manual run with specific date range into specific
           destination table.
    run_one_session() - pick up a session from the session list to run.
    recompile_dependents() - recompile dependent views after remap
  
  Note:
    1. Column name abbreviation rule used: 
      amount      => amt
      fees        => fee
      maintenance => maint
      transaction => trans
      interchange => intchg

    2. To truncate a specific partion for reloading historic data, do it
       like this:
      alter table x_d_site_b truncate partition ym_2013_06 update indexes;

  Todo:
    1. use an updatable join view with parallel UPDATE to updated rows, 
    2. use an anti-hash join with parallel INSERT to refresh the new rows.

*/

  FMT_MN   constant varchar2(20) := 'YYYY-MM';
  FMT_DT   constant varchar2(20) := 'YYYY-MM-DD';
  --
  MSG_INIT constant varchar2(10) := 'INIT ';
  MSG_GOOD constant varchar2(10) := 'GOOD ';
  MSG_DONE constant varchar2(10) := 'DONE ';
  MSG_FAIL constant varchar2(10) := 'FAIL';
 
  EMPTY_SES constant varchar2(20):= '0.0.0';
  --
  FUNCTION  add_session_from_seed RETURN varchar2;  -- returns ym_abbr
  PROCEDURE add_session_for_ym(
    p_ym_abbr varchar2             -- i.e. '2013-06'
  );
  PROCEDURE run_by_dates (
    p_bgn_dt     varchar2,         -- starting date
    p_end_dt     varchar2,         -- end date (inclusive)
    p_table      varchar2,         -- table to insert into
    p_ses_id     varchar2,         -- pass for logging 
    p_runlevel   integer := 1      -- 0: dryrun, 1: full, 2: sampling, maybe...
  );
  PROCEDURE run_one_session (
    p_push       boolean := FALSE,
    p_runlevel   integer := 1
  );
  PROCEDURE recompile_dependents(
    p_view       varchar2,
    p_ses_id     varchar2 := EMPTY_SES
  );
  PROCEDURE run_daily(p_runlevel integer := 1);
  --
  -- for rerun historical
  --
  FUNCTION  find_workspace(p_ym_abbr varchar2) RETURN varchar2;
  PROCEDURE clean(p_ym_abbr varchar2);
  PROCEDURE rerun(p_ym_abbr varchar2, p_push boolean := FALSE);
END xbs_site_agg;
/
show errors

-- ============================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY xbs_site_agg AS
  G_OWNER     constant varchar2(40) := 'EDW_OWNER';
  G_SES_TYPE  constant varchar2(40) := 'D_SITE_AGG';
  G_SRC_TB    constant varchar2(80) := 'F_TRANSACTION_LINE_ITEM';
  G_SEED_TB   constant varchar2(80) := 'EDW_STAGE_OWNER.' || G_SRC_TB;

  G_TGT_TB    constant varchar2(40) := 'x_d_site_b';
  G_TGT_VW    constant varchar2(40) := 'f_daily_site_snapshot_vw';

  G_DRYRUN    constant number       := 0;

-- ---------------------------------------------------------------------------
FUNCTION add_session_from_seed RETURN varchar2 AS
BEGIN
  RETURN xos_session_mgr.add_session_from_seed(
    G_SES_TYPE, G_SEED_TB, G_OWNER || '.' || G_SRC_TB
  );
END add_session_from_seed;

PROCEDURE add_session_for_ym(
  p_ym_abbr varchar2
)
AS
  v_msg varchar2(80) := 'create session ' || G_SES_TYPE || ' for ' || p_ym_abbr;
BEGIN
  log4me.info(v_msg);
  xos_session_mgr.add_session(
    G_SES_TYPE,
    G_OWNER || '.' || G_SRC_TB,
    p_ym_abbr
  );
  COMMIT;
END add_session_for_ym;
-- ----------------------------------------------------------------------------
--
-- core SITE_AGG routine mainly called by higher level drivers
--
-- Note: column name abbriviation rule used: 
--   1. amount => amt
--   2. fees   => fee
--   3. maintenance => maint
--   4. transaction => trans
--   5. interchange => intchg
--
PROCEDURE run_by_dates (
  p_bgn_dt     varchar2,          -- starting date
  p_end_dt     varchar2,          -- end date (inclusive)
  p_table      varchar2,          -- table to insert into
  p_ses_id     varchar2,          -- session_id for logging
  p_runlevel   integer := 1       -- 0: dryrun, 1: full, 2: sampling, maybe...
) 
AS
  X_NOW   constant DATE         := sysdate;
  X_PRC   constant varchar2(80) := 'SITE_AGG using ' || upper(p_table);
  X_RANGE constant varchar2(4000) := 'CALENDAR_DATE_DT BETWEEN
    to_date(''' || p_bgn_dt || ''', ''' || FMT_DT || ''') AND 
    to_date(''' || p_end_dt || ''', ''' || FMT_DT || ''')
  ';
  X_PART  constant varchar2(40)    := 
    'YM_' || to_char(to_date(p_end_dt, FMT_DT), 'YYYY_MM');
  X_SQL   constant varchar2(32767) := '
  INSERT /*+ append */ INTO ' || p_table || '
  SELECT /*+ parallel(tx, 64) use_hash(dt, tx) */
  tx.REVENUE_DATE_KEY,
  tx.POSTING_DATE_KEY,
  tx.PROGRAM_KEY,
  tx.POS_AND_SITE_KEY,
  ns.NAICS_KEY,
  ns.SIC_KEY,
  tx.ROW_SOURCE_SYS_NM                   source_system_name,
  /* measures */
  CAST(COUNT(
    CASE
    WHEN (tx.GROSS_SPEND_AMOUNT<>0 AND tx.WEX_TRANSACTION_ITEM_SEQ_NBR=1)
    THEN tx.WEX_TRANSACTION_ID
    ELSE NULL
    END
  ) AS integer)                          trans_ticket_count,
  CAST(
   SUM(tx.NBR_OF_UNITS_PURCHASED_COUNT) 
   AS integer
  )                                      nbr_of_units_purchased_count,
  CAST(
   SUM(tx.TRANSACTION_LINE_ITEM_COUNT1)
   AS integer
  )                                      trans_line_item_count,
  /* gross */
  SUM(tx.NET_REVENUE_AMOUNT)             net_revenue_amt,	
  SUM(tx.GROSS_SPEND_AMOUNT)             gross_spend_amt,	
  SUM(tx.GROSS_SPEND_LOCAL_AMOUNT)       gross_spend_local_amt,	
  SUM(tx.GROSS_REVENUE_AMOUNT)           gross_revenue_amt,	
  SUM(tx.GROSS_NON_REVENUE_AMOUNT)       gross_non_revenue_amt,	
  SUM(tx.DISCOUNT_AMOUNT)                gross_discount_amt,
  SUM(tx.PURCHASE_ITEM_AMOUNT)           gross_purchase_item_amt,
  SUM(tx.PURCHASE_ITEM_LOCAL_AMOUNT)     gross_purchase_item_local_amt,
  /*
   site does not have the following

   SUM(tx.ALL_OTHER_ANCILLARY_AMOUNT)     gross_all_other_ancillary_amt,
   SUM(tx.REBATE_AMOUNT)                  gross_rebate_amt,
   SUM(tx.WAIVED_REVENUE_AMOUNT)          gross_waived_revenue_amt,	
   SUM(tx.ALL_OTHER_ACILLARY_AMOUNT)      gross_all_other_ancillary_amt,
   SUM(tx.CARD_FEES_AMOUNT)               fee_card_amt,
   SUM(tx.FINANCE_FEE_AMOUNT)             fee_finance_amt,	
   SUM(tx.LATE_FEES_AMOUNT)	             fee_late_amt,
   SUM(tx.WAIVED_LATE_FEES_AMOUNT)        fee_late_waived_amt,	
  */
  SUM(tx.CURRENCY_CONVERSION_FEE_AMOUNT) fee_currency_conversion_amt,	
  /* spend fuel */
  SUM(tx.SPEND_FUEL_ONLY_AMOUNT)         spend_fuel_only_amt,
  SUM(tx.SPEND_FUEL_DIESEL_AMOUNT)       spend_fuel_diesel_amt,
  SUM(tx.SPEND_FUEL_GAS_AMOUNT)          spend_fuel_gas_amt,
  SUM(tx.SPEND_FUEL_OTHER_AMOUNT)        spend_fuel_other_amt,
  SUM(tx.SPEND_MAINTENANCE_AMOUNT)       spend_maint_amt,
  SUM(tx.SPEND_OTHER_AMOUNT)             spend_other_amt,
  /* spend fuel local */
  SUM(tx.SPEND_FUEL_ONLY_LOCAL_AMOUNT)   spend_fuel_only_local_amt,
  SUM(tx.SPEND_FUEL_DIESEL_LOCAL_AMOUNT) spend_fuel_diesel_local_amt,
  SUM(tx.SPEND_FUEL_GAS_LOCAL_AMOUNT)    spend_fuel_gas_local_amt,
  SUM(tx.SPEND_FUEL_OTHER_LOCAL_AMOUNT)  spend_fuel_other_local_amt,
  SUM(tx.SPEND_MAINTENANCE_LOCAL_AMOUNT) spend_maint_local_amt,
  SUM(tx.SPEND_OTHER_LOCAL_AMOUNT)       spend_other_local_amt,
  /* interchange */
  SUM(tx.INTERCHANGE_TOTAL_AMOUNT)       intchg_total_amt,
  SUM(tx.FUEL_ONLY_INTERCHANGE_AMOUNT)   intchg_fuel_only_amt,	
  SUM(tx.INTERCHANGE_FUEL_DIESEL_AMT)    intchg_fuel_diesel_amt,
  SUM(tx.INTERCHANGE_FUEL_GAS_AMT)       intchg_fuel_gas_amt,
  SUM(tx.INTERCHANGE_FUEL_OTHER_AMT)     intchg_fuel_other_amt,
  SUM(tx.MAINTENANCE_ONLY_INTCHG_AMOUNT) intchg_maint_only_amt,
  SUM(tx.ALL_OTHER_INTERCHANGE_AMOUNT)   intchg_all_other_amt,
  /* interchange fee */
  SUM(tx.INTERCHANGE_RATE_ACTUAL_AMOUNT) intchg_rate_actual_amt,
  SUM(tx.INTCHG_LINE_ITEM_FLAT_FEE_AMT)  intchg_line_item_flat_fee_amt,
  SUM(tx.INTCHG_TRANS_FLAT_FEE_AMT)      intchg_trans_flat_fee_amt,
  SUM(tx.INTERCHANGE_PER_UNIT_FEE_AMT)   intchg_per_unit_fee_amt,
  /* tax */
  SUM(tx.TOTAL_TAX_AMOUNT)               tax_total_amt,
  SUM(tx.FEDERAL_TAX_AMOUNT)             tax_federal_amt,
  SUM(tx.NON_FEDERAL_TAX_AMOUNT)         tax_non_federal_amt,
  SUM(tx.STATE_TAX_AMOUNT)               tax_state_amt,
  SUM(tx.LOCAL_TAX_AMOUNT)               tax_local_amt,
  SUM(tx.TOTAL_TAX_LOCAL_AMOUNT)         tax_total_local_amt,
  /* sales tax */
  SUM(tx.STATE_SALES_TAX_AMOUNT)         tax_sales_state_amt,
  SUM(tx.COUNTY_SALES_TAX_AMOUNT)        tax_sales_county_amt,
  SUM(tx.CITY_SALES_TAX_AMOUNT)          tax_sales_city_amt,
  /* excise tax */
  SUM(tx.FEDERAL_EXCISE_TAX_AMOUNT)      tax_excise_federal_amt,
  SUM(tx.STATE_EXCISE_TAX_AMOUNT)        tax_excise_state_amt,
  SUM(tx.COUNTY_EXCISE_TAX_AMOUNT)       tax_excise_county_amt,
  SUM(tx.CITY_EXCISE_TAX_AMOUNT)         tax_excise_city_amt,
  /* special tax */
  SUM(tx.STATE_SPECIAL_TAX_AMOUNT)       tax_speical_state_amt,
  SUM(tx.COUNTY_SPECIAL_TAX_AMOUNT)      tax_special_county_amt,
  SUM(tx.CITY_SPECIAL_TAX_AMOUNT)        tax_special_city_amt,
  /* exempt */
  SUM(tx.TAX_EXEMPT_SPEND_AMOUNT)        tax_exempt_spend_amt,
  SUM(tx.TAX_EXEMPT_SPEND_LOCAL_AMOUNT)  tax_exempt_spend_local_amt,
  /* qualtity gallons */
  SUM(tx.PURCHASE_GALLONS_QTY)           purchase_gallons_qty,
  SUM(tx.FUEL_GAS_GALLONS_QTY)           fuel_gas_gallons_qty,
  SUM(tx.FUEL_DIESEL_GALLONS_QTY)        fuel_diesel_gallons_qty,
  SUM(tx.FUEL_OTHER_GALLONS_QTY)         fuel_other_gallons_qty,
  SUM(tx.PRIVATE_SITE_GALLONS_QTY)       private_site_gallons_qty,
  /* qualtity liters */
  SUM(tx.PURCHASE_LITRES_QTY)            purchase_litres_qty,
  SUM(tx.FUEL_GAS_LITRES_QTY)            fuel_gas_litres_qty,
  SUM(tx.FUEL_DIESEL_LITRES_QTY)         fuel_diesel_litres_qty,
  SUM(tx.FUEL_OTHER_LITRES_QTY)          fuel_other_litres_qty,
  SUM(tx.PRIVATE_SITE_LITRES_QTY)        private_site_litres_qty,
  /* session_id YYYYMMDD.HH24MISS.PID */
  ''' || p_ses_id || '''                 session_id,
  nvl(SUM(tx.MANUAL_REBATE_AMOUNT),0)    manual_rebate_amt,
  nvl(SUM(tx.TRUCK_STOP_FEE_AMOUNT),0)   truck_stop_fee_amt
/*  ,
  nvl(SUM(tx.RETAIL_PPU_AMOUNT),0)       RETAIL_PPU_AMOUNT,   
  nvl(SUM(tx.ADJUSTED_PPU_AMOUNT),0)     ADJUSTED_PPU_AMOUNT,
  nvl(SUM(tx.BETTER_PRICE_PPU_AMOUNT),0) BETTER_PRICE_PPU_AMOUNT
  SUM(tx.EV_TRANSACTION_FEE_AMOUNT)      EV_TRANSACTION_FEE_AMOUNT,
  SUM(tx.SPEND_FUEL_KWH_AMOUNT)          SPEND_FUEL_KWH_AMOUNT,
  SUM(tx.INTERCHANGE_FUEL_KWH_AMT)       INTERCHANGE_FUEL_KWH_AMT,
  SUM(tx.FUEL_KWH_QTY)                   FUEL_KWH_QTY
*/
  FROM F_TRANSACTION_LINE_ITEM PARTITION (' || X_PART || ') tx
  JOIN D_DATE    dt ON (tx.REVENUE_DATE_KEY = dt.DATE_KEY)
  JOIN D_ACCOUNT ac ON (ac.ACCOUNT_KEY = 
    CASE WHEN tx.PURCHASE_ACCOUNT_KEY = 0 
    THEN tx.BILLING_ACCOUNT_KEY ELSE tx.PURCHASE_ACCOUNT_KEY
    END
  )
  /* get naics and sic keys */
  JOIN (
    SELECT 
      ACCOUNT_HIST_KEY, 
      NAICS_KEY,
      SIC_KEY,
      ROW_LAST_MOD_DTTM,
      max(ROW_LAST_MOD_DTTM) OVER (PARTITION BY ACCOUNT_HIST_KEY) last_mod
    FROM F_ACCOUNT_NAICS_SIC_EVENT
  ) ns ON (1=1
    AND ns.ROW_LAST_MOD_DTTM = ns.last_mod
    AND ns.ACCOUNT_HIST_KEY  = ac.ACCOUNT_HIST_KEY
  )
  WHERE ' || X_RANGE || '
    AND 1=' || p_runlevel || ' /* 0: can creates table schema without data */
  GROUP BY
    tx.REVENUE_DATE_KEY,
    tx.POSTING_DATE_KEY,
    tx.PROGRAM_KEY,
    tx.POS_AND_SITE_KEY,
    ns.NAICS_KEY,
    ns.SIC_KEY,
    tx.ROW_SOURCE_SYS_NM
  ';
  v_count integer := 0;

BEGIN
  log4me.debug(MSG_INIT || X_PRC, X_SQL, p_ses_id);

  EXECUTE IMMEDIATE 'alter session force parallel dml';

  IF (p_runlevel = G_DRYRUN) THEN
    dbms_output.put_line(X_SQL);
  ELSE 
    BEGIN
      EXECUTE IMMEDIATE X_SQL;
      v_count := SQL%ROWCOUNT;
      --
      -- direct path: no query before commit (written to tmp first)
      --
      COMMIT;
    EXCEPTION WHEN OTHERS THEN
      log4me.debug(MSG_FAIL || X_PRC, X_SQL, p_ses_id);
      RAISE;
    END;
  END IF;
  log4me.debug(MSG_GOOD || X_PRC || ' => ' || v_count || ' rows', '', p_ses_id);
  --
  -- stat it (takes about 20 seconds)
  --
  IF (p_runlevel<>G_DRYRUN AND p_table<>G_TGT_TB) THEN
    dbms_stats.gather_table_stats(G_OWNER, p_table);
  END IF;
  log4me.debug(MSG_DONE || X_PRC, '', p_ses_id);

EXCEPTION
  WHEN OTHERS THEN
    --
    -- because we use NOLOGGING, no rolling back is needed, always forward
    -- however, if exception is caused by connection lost (or timeout)
    -- any commit after that is causing another exception, i.e. no logging
    -- after that. We need to be able to register that in the log.
    -- 
    COMMIT;
    log4me.err(MSG_FAIL || X_PRC, p_ses_id);
    RAISE;
END run_by_dates;
-- ----------------------------------------------------------------------------
--
-- run by entry in xos_session table
--
PROCEDURE run_one_session (
  p_push       boolean := FALSE,
  p_runlevel   integer := 1
)
AS
  X_PRC_NAME  constant varchar2(40) := 'SITE_AGG';
  X_NOW       constant varchar2(30) := to_char(sysdate, FMT_DT);
  X_VIEW_NAME constant varchar2(40) := 'F_DAILY_SITE_SNAPSHOT_VW';
  X_VIEW_COND constant varchar2(200):= '
    WHERE SOURCE_SYSTEM_NAME in (''TANDEM'', ''TP/CP'')
  ';
  v_wspace varchar2(40) := NULL;
  v_ses_id varchar2(40) := NULL;
  v_ym     varchar2(30);
  v_min_dt varchar2(30);
  v_max_dt varchar2(30);

  v_msg    varchar2(4000);
  v_sql    varchar2(4000);
  v_count  integer := 0;  

BEGIN
  -- -------------------------------------------------------
  -- Processes:
  --   1. find next work session from the xos_session table
  --   2. allocate a destination table from workspace
  --   3. gather the incoming data into 'this' staging
  --   4. remap the view as final+'this'
  --   4. remap the view as final+'this'
  -- -------------------------------------------------------
  --
  -- find next available session to work on
  --
  xos_session_mgr.open(
    G_SES_TYPE, 
    X_PRC_NAME, 
    v_ses_id, v_ym, v_min_dt, v_max_dt
  );
  IF (v_ses_id IS NULL) THEN RETURN; END IF; -- no work, bail out
  --
  -- get the workspace to store the resultant data set, 
  --
  IF p_push THEN
    v_wspace := G_TGT_TB;
  ELSE
    xos_workspace_mgr.alloc(G_SES_TYPE, v_ses_id, v_wspace);
  END IF;

  IF (v_wspace IS NULL) THEN
    -- no workspace left or wrong type and logged in xos_process_log
    v_msg := 'no workspace availabe for process';
    raise STORAGE_ERROR;
  END IF;
  -- 
  -- conditional logging
  --
  v_msg := X_NOW ||
      ' t=' || G_SES_TYPE ||
      ' sid=' || v_ses_id || 
      '[' || v_min_dt || ', ' || v_max_dt || '] using ' ||
      v_wspace;
  log4me.debug(G_SES_TYPE || ' run on ' || X_NOW, v_msg, v_ses_id);
  --
  -- now comes the real work, with benchmark
  --
  BEGIN
    run_by_dates(v_min_dt, v_max_dt, v_wspace, v_ses_id, p_runlevel);
    IF NOT p_push THEN
      xos_workspace_mgr.mark(
        v_wspace, 
        xos_workspace_mgr.SPACE_USED,
        xos_workspace_mgr.CLASS_WARM,
        v_ses_id
      );
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      --
      -- Because we use NOLOGGING and many logging in-between, rolling back
      -- is not possible. workspace manager could have inconsistant states
      -- external process many needed to be scheduled to validate the 
      -- workspace state consistancy
      --
      COMMIT;
      IF NOT p_push THEN 
        xos_workspace_mgr.free(v_wspace, v_ses_id); 
      END IF;
      RAISE;
  END;
  --
  -- validate result statistics, close the session with success
  --
  v_sql := 'SELECT /*+ parallel(x,64) */ count(*) FROM ' || v_wspace || 
    CASE WHEN p_push 
      THEN ' PARTITION (YM_'|| replace(v_ym,'-','_') || ') x' ELSE ' x'
    END;
  log4me.debug(v_msg, v_sql, v_ses_id);
  EXECUTE IMMEDIATE v_sql INTO v_count;
  --
  -- no exception! almost done, just remap view to include the new data
  --
  xos_workspace_mgr.remap(G_SES_TYPE, X_VIEW_NAME, X_VIEW_COND, v_ses_id);
  xbs_site_agg.recompile_dependents(X_VIEW_NAME, v_ses_id);
  xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_DONE, v_count);
  COMMIT;

EXCEPTION
  WHEN OTHERS THEN
    --
    -- Stop propergating the exception so we can roll forward.
    --
    log4me.err(MSG_FAIL || v_msg, v_ses_id);
    -- 
    -- close the session with failure, need to be resurrected
    --
    IF (v_ses_id IS NOT NULL) THEN
      xos_session_mgr.close(v_ses_id, xos_session_mgr.STATUS_FAIL, -1);
    END IF;
    --
    -- No RAISE again here!
    --
    -- To prevent store_procedure failure from propergating and stopping 
    -- any other consequent job from going forward, we use a soft error 
    -- handler here, i.e. session management, instead of breaking the whole 
    -- process steam. That also means we need to have an external monitoring 
    -- tool to validate the success of the process.
    --
END run_one_session;

-- ----------------------------------------------------------------------------
PROCEDURE recompile_dependents(
  p_view   varchar2,
  p_ses_id varchar2 := EMPTY_SES
)
AS
  X_SQL constant varchar2(4000) := '
    SELECT name FROM USER_DEPENDENCIES
    WHERE  1=1
    AND    TYPE = ''VIEW''
    AND    REFERENCED_TYPE = ''VIEW''
    AND    REFERENCED_NAME = ''' || p_view || '''
    AND    DEPENDENCY_TYPE = ''HARD''';
  v_msg  varchar2(200) := '';
  v_sql  varchar2(200) := '';
  v_dept varchar2(80);
  TYPE VCUR  IS REF CURSOR;
  c_ws VCUR;
BEGIN
  -- --------------------------------------------------------
  -- Due to daily view been replaced every session run,
  -- the dependent views are invalidated. It is required
  -- to recompile the dependent views before the user can
  -- access to the newly updated content.
  -- It is better to use workspace_mgr to do the update
  -- but we do not have enough time to do that within the 
  -- release timeframe. So, backlog (aka 'TODO' list) entered.
  -- --------------------------------------------------------
  OPEN c_ws FOR X_SQL;
  LOOP
    FETCH c_ws INTO v_dept;
    EXIT WHEN c_ws%NOTFOUND;
    v_msg  := 'dependent view ' || v_dept || ' recompiled';
    v_sql  := 'ALTER VIEW ' || v_dept || ' COMPILE';
    EXECUTE IMMEDIATE v_sql;
    log4me.debug(v_msg, v_sql, p_ses_id);
  END LOOP;
  CLOSE c_ws;
EXCEPTION
  WHEN OTHERS THEN
    log4me.err(MSG_FAIL || v_sql, p_ses_id);
    IF c_ws%ISOPEN THEN CLOSE c_ws; END IF;
    RAISE;
END recompile_dependents;

-- ----------------------------------------------------------------------------
PROCEDURE run_daily(p_runlevel integer := 1) 
AS
  X_PRC        constant varchar2(40) := 'site_agg';
  X_LAST_MONTH constant varchar2(20) := to_char(sysdate-7, FMT_MN);

  v_this_month varchar2(20);
  v_ses_id     varchar2(40);
BEGIN
  log4me.info('INIT ' || X_PRC, v_ses_id);
  --
  -- 1. take seed dates from EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM table,
  --    create a entry in xo_session table
  -- 2. mark a cached result from previous run stale
  -- 3. execute the session to create the daily result, including
  --    rebuild the daily view and monthly view.
  --
  v_this_month := xbs_site_agg.add_session_from_seed;
  IF (X_LAST_MONTH < v_this_month) THEN
    -- 
    -- get any session that has been run for this month
    --
    xos_session_mgr.previous_session(G_SES_TYPE, v_this_month, v_ses_id);
    --
    -- If this is the first time ever been run for the month,
    -- we put the run from last month to AUDIT state.
    --
    IF (v_ses_id IS NULL) THEN
        xos_session_mgr.previous_session(G_SES_TYPE, X_LAST_MONTH, v_ses_id);
        xos_workspace_mgr.audit_by_session(v_ses_id);
    ELSE
      log4me.debug('session exists for ' || v_this_month || ': ' || v_ses_id);
    END IF;
  END IF;
  -- 
  -- We mark the output from previous session STALE first before we start
  -- processing. Should the process failed, the view is still pointing to
  -- the STALE but still valid data. The view will be recompiled upon next
  -- successful run.
  --
  xos_workspace_mgr.retire(G_SES_TYPE, v_ses_id);
  xbs_site_agg.run_one_session(FALSE, p_runlevel);

  log4me.info('DONE ' || X_PRC, v_ses_id);
END run_daily;

-- ----------------------------------------------------------------------------
-- find previous workspace that has been occupied by the given month
--
FUNCTION find_workspace(p_ym_abbr varchar2) RETURN varchar2 AS
  v_ses_id varchar2(40);
BEGIN
  --
  -- find session info from previously runlog
  --
  xos_session_mgr.previous_session(G_SES_TYPE, p_ym_abbr, v_ses_id);
  IF v_ses_id IS NULL THEN
    log4me.warn(G_SES_TYPE || ' for ' || p_ym_abbr || '=> no session found!');
    RETURN NULL;
  END IF;
  --
  -- see if the data still sits in the A buffer ring
  --
  RETURN xos_workspace_mgr.find_by_session(v_ses_id);
END find_workspace;

-- ----------------------------------------------------------------------------
-- clean up historical data for reload
--
PROCEDURE clean(p_ym_abbr varchar2) AS
  X_PRC   varchar2(200) := 'clean ' || G_SES_TYPE || ' for ' || p_ym_abbr;
  X_PART  varchar2(40)  := 'YM_' || replace(p_ym_abbr, '-', '_');
  X_SQL   varchar2(400) := '
    ALTER TABLE ' || G_TGT_TB || '
    TRUNCATE PARTITION ' || X_PART || ' REUSE STORAGE
  ';
  v_wspace varchar2(80);
BEGIN
  --
  -- clear if the data still sits in the A buffer ring
  --
  v_wspace := find_workspace(p_ym_abbr);
  IF v_wspace IS NOT NULL THEN
    xos_workspace_mgr.free(v_wspace);
  END IF;
  --
  -- to be safe, also truncate the partition from B layer (takes only 1 second)
  --
  log4me.debug(X_PRC || '=> truncate partition ' || X_PART, X_SQL);
  EXECUTE IMMEDIATE X_SQL;
END clean;
-- ----------------------------------------------------------------------------
-- rerun historical data from transaction (after loaded)
--
-- Note:
--   This method will clean and overwrite the content of historical data, in 
--   the upper layer buffer. By design, they are kept as audited data and
--   considered read-only.
--
PROCEDURE rerun(p_ym_abbr varchar2, p_push boolean := FALSE) AS
  X_PRC    constant varchar2(200) := 'SITE_AGG_RERUN for ' || p_ym_abbr;
BEGIN
  log4me.info('*** INIT ' || X_PRC);
  --
  -- clean up pre-existed historical data, the reload from the source
  --
  clean(p_ym_abbr);
  --
  -- now the real business
  --
  add_session_for_ym(p_ym_abbr);
  run_one_session(p_push);

  log4me.info('*** DONE ' || X_PRC);
END rerun;
-- ----------------------------------------------------------------------------

END xbs_site_agg;
/
show errors

-- ============================================================================
-- ETL store procedure interface
--
CREATE OR REPLACE PROCEDURE F_DAILY_SITE_AGGREGATES AS
BEGIN
/*
  Daily site aggregate proc - calls underneath xbs_site_agg package
  and perform the following steps:
  
  1. take seed dates from EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM table,
     create a entry in xo_session table
  2. mark a cached result from previous run stale
  3. execute the session to create the daily result, including
     rebuild the daily view and monthly view.
  
  See xbs_site_agg Package - Daily site/program/SIC measurement aggregation

  Revision History: 
  1.0 20140318 CC - first try
  1.1 20140320 CC - add seed_date process control using pc_session table
  1.2 20140410 CC - add comments
  1.3 20140422 CC - use F_ACCOUNT_NAICS_SIC_EVENT
  1.4 20140429 CC - commented out non-populated fields,
                    add source_system_name field,
                    add condition to F_DAILY_SITE_SNAPSHOT_VW
  2.0 20140516 CC - enable LRU cache-based daily site agg routines
                    and monthly view dependency recompilation
  2.1 20140804 CC - change format of comments, and revision history
  2.2 20140829 CC - use business_date as auditing definition of "this month"
  2.3 20140911 CC - for manual rebate, use BILLING_ACCOUNT_KEY when
                    PURCHASE_ACCOUNT_KEY=0
  2.4 20141022 CC - add TRUCK_STOP_FEE_AMT (SDLC14.2)
  2.5 20150308 CC - add DEF, EV (SDLC15.1); PPUs (SpeedWay/Pepsi)
  2.6 20150312 CC - mask EDF and EV for speedway
  2.7 20150412 CC - remove SpeedWay PPUs, BI doesn't need them
*/
  xbs_site_agg.run_daily;
END F_DAILY_SITE_AGGREGATES;
/
show errors

CREATE OR REPLACE PUBLIC SYNONYM F_DAILY_SITE_AGGREGATES FOR F_DAILY_SITE_AGGREGATES;
GRANT EXECUTE ON F_DAILY_SITE_AGGREGATES TO DWLOADER;



