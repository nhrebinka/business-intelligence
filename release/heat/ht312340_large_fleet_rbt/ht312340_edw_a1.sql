--
-- 2013_01-2015_03 Revenue Aggregates - Large Fleet Rebates Historical Reload
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ht312340_large_fleet_rbt.log;

@f_daily_affinity_program_check.prc

ALTER SESSION ENABLE PARALLEL DML;
ALTER SESSION SET SKIP_UNUSABLE_INDEXES = TRUE; 

DECLARE
  x_prc constant varchar2(200) := 'HT312340: Large Fleet Rebate Agg-Run';

  m_plst TokList;
  m_dlst TokList;

  PROCEDURE rebuild_idx(p_part varchar2) AS
    x_uidx constant TokList := TokList(
      'EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT_NK'
    );
  BEGIN
    log4me.info(x_prc || ' => rebuild unique indexes');
    FOR i IN 1..x_uidx.COUNT LOOP
      EXECUTE IMMEDIATE '
        ALTER INDEX ' || x_uidx(i) || '
        REBUILD PARTITION ' || p_part || ' PARALLEL 64
      ';
    END LOOP;
  END rebuild_idx;

  PROCEDURE compress_parti(p_part varchar2) AS
  BEGIN
    log4me.debug(x_prc || ' => compressing ' || p_part, '');
    EXECUTE IMMEDIATE '
      ALTER TABLE EDW_OWNER.F_DAILY_REVENUE_SNAPSHOT
      MOVE PARTITION ' || p_part || '
      COMPRESS UPDATE INDEXES PARALLEL 64
      NOLOGGING
    ';
  END compress_parti;

  PROCEDURE rerun(
    p_dlst TokList -- list of date string in 'YYYYMMDD'
  ) IS
    X_CNT constant integer := p_dlst.COUNT;

    v_dt  date;
    v_pt  varchar2(20);             -- current  partition name

    FUNCTION is_new_partition(i integer) RETURN boolean IS
    BEGIN RETURN i<=1 OR substr(p_dlst(i),1,6)<>substr(p_dlst(i-1),1,6); END;

    FUNCTION is_end_of_month(i integer)  RETURN boolean IS
    BEGIN RETURN i>=X_CNT OR substr(p_dlst(i+1),1,6)<>substr(p_dlst(i),1,6); END;
   
  BEGIN
    FOR i IN 1..X_CNT LOOP
      v_dt := to_date(p_dlst(i), 'YYYYMMDD');          -- date to rerun
      v_pt := 'YM_' || trim(to_char(v_dt, 'YYYY_MM')); -- partition name

      IF is_new_partition(i) THEN
        --
        -- make sure indices are usable, rebuild if needed
        --
        rebuild_idx(v_pt);
      END IF;

      log4me.debug(x_prc || ' => agg: ' || v_dt, '');
      IF is_end_of_month(i) THEN
        -- last day of the month, 
        -- 1. run daily
        -- 2. run monthly
        -- 3. compress partition with index rebuild
        f_revenue_aggregates(v_dt, v_dt, TRUE);
        compress_parti(v_pt);
      ELSE
        -- rebuild only daily aggregates
        f_revenue_aggregates(v_dt, v_dt, FALSE);
      END IF;
    END LOOP;  
  END rerun;

BEGIN
  log4me.info('INIT ' || x_prc);
  --
  -- get partition, date list with rebates from 2013-01 to 2015-04
  -- running from most recent partition backward to the past
  --
  SELECT /*+ parallel(x,64) */
    unique 
      d.month_year_abbr,
      to_char(d.calendar_date_dt, 'YYYYMMDD')
  BULK COLLECT INTO m_plst, m_dlst
  FROM f_transaction_line_item x
  JOIN d_date d ON (d.date_key = x.revenue_date_key)
  WHERE x.revenue_date_key between 12055 and 12904
  AND   x.ROW_SOURCE_SYS_NM='EXCEL AUTO REBATES'
  ORDER BY 1 desc, 2
  ;
  --
  -- run through date list
  --
  rerun(m_dlst);
  log4me.info('DONE ' || x_prc);
  RETURN;

EXCEPTION WHEN OTHERS THEN
  log4me.err('FAIL ' || x_prc);
  RAISE;
END;
/
show errors

SPOOL OFF

