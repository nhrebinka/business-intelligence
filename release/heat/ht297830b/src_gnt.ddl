-- grant all tables and the constraints
GRANT ALL    ON PS_DEPT_TBL TO DWLOADER;
GRANT ALL    ON PS_DEPT_TBL TO EDM_OWNER;
GRANT SELECT ON PS_DEPT_TBL TO STAGE_OWNER_SELECT;
GRANT ALL    ON PS_GL_ACCOUNT_TBL TO DWLOADER;
GRANT ALL    ON PS_GL_ACCOUNT_TBL TO EDM_OWNER;
GRANT SELECT ON PS_GL_ACCOUNT_TBL TO STAGE_OWNER_SELECT;
GRANT ALL    ON PS_LEDGER TO DWLOADER;
GRANT ALL    ON PS_LEDGER TO EDM_OWNER;
GRANT SELECT ON PS_LEDGER TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_ACCT_SINCE_DATE_LOOKUP TO DWLOADER;
GRANT ALL    ON SRC_ACCT_SINCE_DATE_LOOKUP TO EDM_OWNER;
GRANT SELECT ON SRC_ACCT_SINCE_DATE_LOOKUP TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_PS_GL_DEPT_TBL TO DWLOADER;
GRANT ALL    ON SRC_PS_GL_DEPT_TBL TO EDM_OWNER;
GRANT SELECT ON SRC_PS_GL_DEPT_TBL TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_PS_GL_ACCOUNT_TBL TO DWLOADER;
GRANT ALL    ON SRC_PS_GL_ACCOUNT_TBL TO EDM_OWNER;
GRANT SELECT ON SRC_PS_GL_ACCOUNT_TBL TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_PS_GL_LEDGER TO DWLOADER;
GRANT ALL    ON SRC_PS_GL_LEDGER TO EDM_OWNER;
GRANT SELECT ON SRC_PS_GL_LEDGER TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_MANUAL_REBATES TO DWLOADER;
GRANT ALL    ON SRC_MANUAL_REBATES TO EDM_OWNER;
GRANT SELECT ON SRC_MANUAL_REBATES TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_TO_MKT_CHL TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_TO_MKT_CHL TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_TO_MKT_CHL TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_TO_SERVE TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_TO_SERVE TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_TO_SERVE TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_TO_ADJUDICATE TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_TO_ADJUDICATE TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_TO_ADJUDICATE TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_TO_ACQUIRE TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_TO_ACQUIRE TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_TO_ACQUIRE TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_OF_SALES TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_OF_SALES TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_OF_SALES TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_COST_OF_IT TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_COST_OF_IT TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_COST_OF_IT TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_EXCEL_PROGRAM TO DWLOADER;
GRANT ALL    ON SRC_EXCEL_PROGRAM TO EDM_OWNER;
GRANT SELECT ON SRC_EXCEL_PROGRAM TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_SOURCE_MKT_CHANNEL TO DWLOADER;
GRANT ALL    ON SRC_SOURCE_MKT_CHANNEL TO EDM_OWNER;
GRANT SELECT ON SRC_SOURCE_MKT_CHANNEL TO STAGE_OWNER_SELECT;
GRANT ALL    ON SRC_SOURCE_BUSINESS_SEGMENT TO DWLOADER;
GRANT ALL    ON SRC_SOURCE_BUSINESS_SEGMENT TO EDM_OWNER;
GRANT SELECT ON SRC_SOURCE_BUSINESS_SEGMENT TO STAGE_OWNER_SELECT;
