/*
DROP TABLE SRC_ACCT_SINCE_DATE_LOOKUP CASCADE CONSTRAINTS;

CREATE TABLE SRC_ACCT_SINCE_DATE_LOOKUP
(
  ACCT_TYPE             varchar2(50),
  ACCT_STS              varchar2(30),
  EDM_ACCT_KEY          number,
  WEX_ACCT_NBR          VARCHAR2(100),
  CUR_ACTIV_DT          DATE,
  LEGACY_SOURCE_SYS_NM  VARCHAR2(200),
  LEGACY_XREF_NM_OR_NBR VARCHAR2(100),
  LEGACY_ACTIV_DT       DATE,
  SINCE_DATE            DATE,
  CONVERSION_RULE_CD    VARCHAR2(200),
  CONFIDENCE_LEVEL      NUMBER,
  UPDATED_CD            VARCHAR2(200),
  UPDATED_AT            TIMESTAMP,
  SESSION_ID            varchar2(80)
)
TABLESPACE D_BI
;
GRANT SELECT ON SRC_ACCT_SINCE_DATE_LOOKUP TO STAGE_OWNER_SELECT;
GRANT SELECT ON SRC_ACCT_SINCE_DATE_LOOKUP TO EDM_OWNER_SELECT;
GRANT ALL    ON SRC_ACCT_SINCE_DATE_LOOKUP TO DWLOADER;
*/
CREATE OR REPLACE PACKAGE EDM_OWNER.xbs_acct_src_xform AS
/*
-- ---------------------------------------------------------------------------
  Name : xbs_acct_src_xform
  Name : SRC_ACCT_SINCE_DATE_CONV
  Spec.: procedure to create the account since date map to actually 
         address the setup date. Due to some unknown reason, the date
         was not captured by the legacy process during the conversion of
         TANDEM to SIEBEL.

  Usage: exec SRC_ACCT_SINCE_DATE_CONV([odate1, [odate2]]);
  Spec.: package to manage acctount source transformation from the following
         STAGE_OWNER tables
         1. SRC_SBL_S_ORG_EXT
         2. SRC_SBL_S_ORG_EXT_X
         3. SRC_SBL_S_EVT_ACT

         and the following EDM_OWNER tables
         1. M_ACCT
         2. M_ACCT_XREF
         3. M_USER
         4. M_USER_XREF
           
         into the following STAGE_OWNER table
         1. SRC_ACCT_SINCE_DATE_LOOKUP

  Note:
    To find most recent update for since_date of a wex_acct_nbr, 
    use the following SQL:

    with 
    lk as (
      select 
        wex_acct_nbr, 
        since_date,
        updated_at,
        row_number() over (partition by wex_acct_nbr order by updated_at desc) rn
      from stage_owner.src_acct_since_date_lookup
    )
    select wex_acct_nbr, since_date from lk
    where rn = 1;
         
-- ---------------------------------------------------------------------------
  Dependency:
    log4me package

  Process Desc.:
    add_acct_since_date_conv_case1(p_ses_id)
    :- Get original SINCE_DATE from original TANDEM source row

    add_acct_since_date_conv_case2(p_ses_id)
    :- Get SINCE_DATE from user manual input

-- ---------------------------------------------------------------------------
  Revision History: 
  1.0 20141009 CC - first version
  1.1 20141015 CC - alter CASE2 logic to mask out existing CASE1 accounts
  1.2 20141022 CC - relax CASE2 rules
                    * remove ATTRIB_04 != ATTRIB_06
                    * Onboarding Sales from '..Impl%' to '..Imp%'

  Note:
-- ============================================================================
*/
  FMT_DT  constant varchar2(20) := 'YYYY-MM-DD';

  g_proc_nm varchar2(120);
  g_min_dt  date;
  g_max_dt  date;

  PROCEDURE init(p_min_dt date, p_max_dt date);
  PROCEDURE add_sbl_since_date_conv_case1(p_ses_id varchar2);
  PROCEDURE add_sbl_since_date_conv_case2(p_ses_id varchar2);
END xbs_acct_src_xform;
/
show errors

-- ============================================================================
-- package body
--
CREATE OR REPLACE PACKAGE BODY EDM_OWNER.xbs_acct_src_xform AS
--
-- constants
--
DEFAULT_CONFIDENCE_LEVEL constant number := 6;
LEVEL_DELTA              constant number := 2;

-- ---------------------------------------------------------------------------
PROCEDURE init(p_min_dt date, p_max_dt date) AS
BEGIN
  g_min_dt  := p_min_dt;
  g_max_dt  := p_max_dt;
  g_proc_nm := 'xbs_acct_src_xform[' || 
    to_char(p_min_dt, FMT_DT) || ',' ||
    to_char(p_max_dt, FMT_DT) || 
  ']';
END init;

-- ---------------------------------------------------------------------------
PROCEDURE add_sbl_since_date_conv_case1(p_ses_id varchar2) AS
  v_proc constant varchar2(120) := 
    g_proc_nm || '.add_sbl_since_date_conv_case1';
BEGIN
  log4me.debug(v_proc, '', p_ses_id);

  INSERT INTO stage_owner.src_acct_since_date_lookup
  WITH
  sbl AS ( -- account from SIBEL source
    SELECT /*+ parallel use_hash(m, x) */ 
      m.acct_type, m.acct_sts, 
      m.edm_acct_key, m.wex_acct_nbr, m.acct_orig_activ_dt, 
      x.acct_source_pk
    FROM edm_owner.m_acct m
    JOIN edm_owner.m_acct_xref x ON (m.edm_acct_key = x.edm_acct_key)
    WHERE dw_source_sys = 'SIEBEL'
      AND m.acct_type NOT IN('Customer', 'National ID')
  ),
  tdm AS ( -- account from TANDEM source
    SELECT /*+ parallel use_hash(m, x) */ 
      m.wex_acct_nbr, m.acct_orig_activ_dt, 
      x.dw_source_sys, x.acct_source_pk
    FROM edm_owner.m_acct m
    JOIN edm_owner.m_acct_xref x ON (m.edm_acct_key = x.edm_acct_key)
    WHERE dw_source_sys = 'TANDEM'
      AND m.acct_type NOT IN('Customer', 'National ID')
  ),
  ox AS (  -- TANDEM converted account
    SELECT /*+ noparallel nocache */ 
      par_row_id, attrib_06, last_upd, 
      row_number() over (
        partition by par_row_id, attrib_06 order by last_upd desc
      ) rn
    FROM stage_owner.src_sbl_s_org_ext_x
    WHERE attrib_06 IS NOT NULL
      AND last_upd between g_min_dt and g_max_dt
  )
  SELECT 
    sbl.acct_type,
    sbl.acct_sts,
    sbl.edm_acct_key,
    sbl.wex_acct_nbr,
    sbl.acct_orig_activ_dt,
    tdm.dw_source_sys,
    tdm.wex_acct_nbr,
    tdm.acct_orig_activ_dt,
    tdm.acct_orig_activ_dt,
    'CASE1',
    DEFAULT_CONFIDENCE_LEVEL + LEVEL_DELTA,
    g_proc_nm,
    SYSTIMESTAMP,
    p_ses_id
  FROM tdm
  JOIN ox  ON (tdm.acct_source_pk = ox.attrib_06)
  JOIN sbl ON (sbl.acct_source_pk = ox.par_row_id)
  WHERE ox.rn = 1
  ;
  COMMIT;
END add_sbl_since_date_conv_case1;

-- ---------------------------------------------------------------------------
PROCEDURE add_sbl_since_date_conv_case2(p_ses_id varchar2) AS
  v_proc constant varchar2(120) := 
    g_proc_nm || '.add_sbl_since_date_conv_case2';

BEGIN
  log4me.debug(v_proc, '', p_ses_id);

  MERGE INTO stage_owner.src_acct_since_date_lookup lk USING (
    WITH
    ox AS ( -- accounts that has manual entered Setup Date
            -- which is not covered by CASE 1 either
      SELECT /*+ parallel(a, 4) nocache */ 
        a.target_ou_id, a.name, a.last_upd,
        row_number() over (
          partition by a.target_ou_id, a.name order by a.last_upd desc
        ) rn
      FROM stage_owner.src_sbl_s_evt_act a 
      WHERE a.todo_cd = 'Legacy Setup Date'
        AND a.last_upd between g_min_dt and g_max_dt
    ),
    obs AS ( -- Onboading Sales
      SELECT /*+ use_hash(u, x) */ 
        unique x.user_source_pk
      FROM edm_owner.m_user u
      JOIN edm_owner.m_user_xref x ON (u.edm_user_key = x.edm_user_key)
      WHERE x.user_source_pk IN (
        --
        -- needs to be externalize if we keep growing this list
        --
        'WEXLINK', 
        'TBOYDEN', 'KCYR', 'LDOWNEY',
        'LLEWIS', 'JPAQUETTE', 'DCANTY'
      ) 
      OR (1=1
        AND u.user_department = 'US - MP Boarding'
        AND u.user_title LIKE '%Strategic Imp%'
      )
    ),
    mu AS ( -- find referenced M_ACCT that is managed only by Onboarding Sales
      SELECT /*+ parallel(m,64) parallel(x,64) use_hash(ox, x) */
        m.ult_par_acct_nbr, 
        m.wex_acct_nbr,
        m.acct_orig_activ_dt,
        to_date(ox.name,'MM/DD/YYYY') since_dt,
        ox.target_ou_id
      FROM ox
      JOIN edm_owner.m_acct_xref x ON (x.acct_source_pk = ox.target_ou_id)
      JOIN edm_owner.m_acct      m ON (1=1
        AND m.edm_acct_key = x.edm_acct_key
        AND m.acct_type NOT IN ('Customer', 'National ID')
        AND m.sales_rep_cd IN (           -- only by onboarding sales
          SELECT user_source_pk FROM obs
        )
      )
      WHERE ox.rn = 1
    )
    SELECT /*+ parallel(m, 64) parallel(x, 64) use_hash(m, x) */
      m.acct_type,
      m.acct_sts,
      m.edm_acct_key,
      m.wex_acct_nbr,
      m.acct_orig_activ_dt  cur_activ_dt,
      mu.ult_par_acct_nbr   legacy_acct_nbr,
      mu.acct_orig_activ_dt legacy_activ_dt,
      mu.since_dt
    FROM mu   -- get every accounts under L1
    JOIN edm_owner.m_acct m ON (1=1
      AND mu.ult_par_acct_nbr = m.ult_par_acct_nbr
      AND m.sales_rep_cd IN (                   -- only by onboarding sales
        SELECT user_source_pk FROM obs
      )
    )
    WHERE mu.ult_par_acct_nbr = mu.wex_acct_nbr -- take only L1
  ) u ON (lk.session_id = p_ses_id AND lk.edm_acct_key = u.edm_acct_key)
  WHEN MATCHED THEN
    UPDATE SET confidence_level = confidence_level - LEVEL_DELTA
  WHEN NOT MATCHED THEN
    INSERT VALUES (
      u.acct_type,
      u.acct_sts,
      u.edm_acct_key,
      u.wex_acct_nbr,
      u.cur_activ_dt,
      'SIEBEL MANUAL',
      u.legacy_acct_nbr, 
      u.legacy_activ_dt,
      u.since_dt,
      'CASE2',
      DEFAULT_CONFIDENCE_LEVEL,
      g_proc_nm,
      SYSTIMESTAMP,
      p_ses_id
    )
  ;
  COMMIT;
END add_sbl_since_date_conv_case2;
-- ---------------------------------------------------------------------------
-- end package body
--
END xbs_acct_src_xform;
/
show errors
-- ============================================================================

CREATE OR REPLACE PROCEDURE EDM_OWNER.SRC_ACCT_SINCE_DATE_CONV0(
  p_min_business_dt date := NULL,
  p_max_business_dt date := NULL
)
AS
/*
  Name : SRC_ACCT_SINCE_DATE_CONV
  Spec.: procedure to create the account since date map to actually 
         address the setup date. Due to some unknown reason, the date
         was not captured by the legacy process during the conversion of
         TANDEM to SIEBEL.

  Usage: exec SRC_ACCT_SINCE_DATE_CONV([odate1, [odate2]]);
    1. process all data if both odates are NULL
    2. process one full month if odate1 is given, and odate2 is NULL
    3. process only given date range if both odate1 and odate2 are given

  Dependency:
    log4me package

  Revision History: 
  1.0 20141008 CC - first version
*/
-- ==========================================================================
  FMT_SES constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';
  FMT_DT  constant varchar2(20) := 'YYYY-MM-DD';

  v_ses_id varchar2(40) := to_char(systimestamp, FMT_SES);
  v_proc   varchar2(120);
  v_min_dt date;
  v_max_dt date;
  -- 
  -- create process date range
  --
  PROCEDURE adjust_date_range AS
  BEGIN
    CASE
    WHEN (p_min_business_dt IS NULL AND p_max_business_dt IS NULL) THEN
      v_min_dt := to_date('19000101', 'YYYYMMDD');
      v_max_dt := to_date('21000101', 'YYYYMMDD');
    WHEN (p_max_business_dt IS NULL) THEN
      SELECT /*+ noparallel */
        first_day_in_month_date_dt, last_day_in_month_date_dt
      INTO   v_min_dt, v_max_dt
      FROM   d_date
      WHERE  calendar_date_dt = p_min_business_dt;
    ELSE
      v_min_dt := p_min_business_dt;
      v_max_dt := p_max_business_dt;
    END CASE;
  END adjust_date_range;

BEGIN
  adjust_date_range;

  v_proc := 'SRC_ACCT_SINCE_DATE_CONV[' ||
    to_char(v_min_dt, FMT_DT) || ',' ||
    to_char(v_max_dt, FMT_DT) || 
  ']';
  log4me.info('INIT ' || v_proc, v_ses_id);

  xbs_acct_src_xform.init(v_min_dt, v_max_dt);
  xbs_acct_src_xform.add_sbl_since_date_conv_case1(v_ses_id);
  xbs_acct_src_xform.add_sbl_since_date_conv_case2(v_ses_id);

  log4me.info('DONE ' || v_proc, v_ses_id);

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    log4me.err('FAIL ' || v_proc, v_ses_id);
    RAISE;
END SRC_ACCT_SINCE_DATE_CONV0;
/
show errors

CREATE OR REPLACE PROCEDURE EDM_OWNER.SRC_ACCT_SINCE_DATE_CONV(
  p_min_business_dt varchar2 := '01/01/1900',
  p_max_business_dt varchar2 := '12/31/2100'
)
AS
  v_min_dt date := to_date(p_min_business_dt, 'MM/DD/YYYY');
  v_max_dt date := to_date(p_max_business_dt, 'MM/DD/YYYY');
BEGIN
  SRC_ACCT_SINCE_DATE_CONV0(v_min_dt, v_max_dt);
END SRC_ACCT_SINCE_DATE_CONV;
/
show errors

-- ==========================================================================

GRANT EXECUTE ON SRC_ACCT_SINCE_DATE_CONV0 TO DWLOADER;
GRANT EXECUTE ON SRC_ACCT_SINCE_DATE_CONV  TO DWLOADER;
