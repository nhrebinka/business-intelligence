--
-- BI4.0 migration script
--
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000
SET ECHO      ON

SPOOL /i1/&env/hub/logs/bi40_migration.log;


-- EDW.EDW_OWNER one time setup
@edw_drp.ddl
@edw_tbl.ddl
@edw_seq.ddl
@edw_gnt.ddl
