CREATE OR REPLACE PROCEDURE EDW_OWNER.F_TRANSACTION_LINE_ITEM_LOAD(
  p_min_business_dt date := NULL,
  p_max_business_dt date := NULL
)
AS
/*
  Name : F_TRANSACTION_LINE_ITEM_LOAD
  Spec.: procedure to load daily transaction data from 
         EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM to 
         EDW_OWNER.F_TRANSACTION_LINE_ITEM

  Usage: exec F_TRANSACTION_LINE_ITEM_LOAD([odate1, [odate2]]);
    1. process all data if both odates are NULL
    2. process one full month if odate1 is given and odate2 is NULL
    3. process only given date range if both odate1 and odate2 are given

  Dependency:
    log4me package

  Revision History: 
  1.0 ?        VK
  2.0 20100601 VK - Adding Currency conversion fee amount for 13.1 Canada Ph 2
  2.1 20140808 CC - remove bitmap index rebuild,
                    reformat for readability
  2.2 20140911 CC - add date range for reprocessing historical data
  2.3 20141020 CC - add MANUAL_REBATE_AMOUNT (BI4.0)
                    add TRUCK_STOP_FEE_AMOUNT (SDLC14.2)
                    add double-load prevention logic
*/
-- ==========================================================================
  CHECK_DUP constant boolean      := false;
  FMT_DT    constant varchar2(20) := 'YYYY-MM-DD';

  v_proc    varchar2(80) := 'F_TRANSACTION_LINE_ITEM_LOAD';
  v_min_dt  date;
  v_max_dt  date;
 
  PROCEDURE set_date_range AS
  BEGIN
    CASE
    WHEN (p_min_business_dt IS NULL AND p_max_business_dt IS NULL) THEN
      SELECT /*+ noparallel */ 
        min(calendar_date_dt), max(calendar_date_dt)
      INTO   v_min_dt, v_max_dt
      FROM   d_date;
    WHEN (p_max_business_dt IS NULL) THEN
      SELECT /*+ noparallel */
        first_day_in_month_date_dt, last_day_in_month_date_dt
      INTO   v_min_dt, v_max_dt
      FROM   d_date
      WHERE  calendar_date_dt = p_min_business_dt;
    ELSE
      v_min_dt := p_min_business_dt;
      v_max_dt := p_max_business_dt;
    END CASE;
  END set_date_range;

  FUNCTION if_posted RETURN boolean AS
    ERROR_RANGE constant number := 0.1;

    v_rev_dt  integer;
    v_txn_id  varchar2(200);
    v_total   integer;

    v_cnt1    integer;
    v_cnt2    integer;

    v_part    varchar2(40);
    v_sql     CLOB;
  BEGIN
    --
    -- check whether we have run today's process already
    --
    SELECT /*+ parallel(x, 64) */ 
      max(revenue_date_key), max(wex_transaction_id), count(*)
    INTO v_rev_dt, v_txn_id, v_total
    FROM EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM;

    SELECT 'partition (ym_' || to_char(calendar_date_dt, 'YYYY_MM') || ')'
    INTO v_part
    FROM d_date WHERE date_key = v_rev_dt;

    EXECUTE IMMEDIATE '
    SELECT /*+ parallel(x, 64) */ 
      SUM(
        CASE WHEN wex_transaction_id = ''' || v_txn_id || ''' THEN 1 ELSE 0 END
      ) t_cnt,
      SUM(
        CASE WHEN revenue_date_key = ' || v_rev_dt || ' THEN 1 ELSE 0 END
      ) d_cnt
    FROM EDW_OWNER.F_TRANSACTION_LINE_ITEM ' || v_part || ' x
    '
    INTO v_cnt1, v_cnt2;
    --
    -- compare processed data to stage
    --
    log4me.debug(v_proc,
      v_cnt1 || ', ' || v_cnt2 || ' of ' || v_total || 
      ' rows with the same wex_txn_id, revenue_date exist in EDW already'
    );
    RETURN (v_cnt1 > 0 OR v_cnt2/v_total > ERROR_RANGE);
  END if_posted;

BEGIN
  set_date_range;
  -- 
  -- create process date range
  --
  log4me.info('INIT ' || v_proc || 
    '[' ||
    to_char(v_min_dt, FMT_DT) || ',' ||
    to_char(v_max_dt, FMT_DT) || 
    ']'
  );
  --
  -- check whether we have run this batch already
  --
  IF CHECK_DUP AND p_min_business_dt IS NULL AND if_posted THEN
    RAISE_APPLICATION_ERROR(
      -20205, 
      v_proc || ' ODATE exists in EDW already, check no duplicated run. Give p_min_business_dt parameter to force execution if you must.'
    );
  END IF;
  -- 
  -- start the process
  --
  INSERT /*+ append */ INTO  EDW_OWNER.F_TRANSACTION_LINE_ITEM
  SELECT /*+ parallel(x,64) full(x) use_hash(d, x) */
    SITE_REGION_KEY,
    ACCOUNT_REGION_KEY,
    REVENUE_DATE_KEY,
    POSTING_DATE_KEY,
    PURCHASE_DATE_KEY,
    PURCHASE_ACCOUNT_KEY,
    BILLING_ACCOUNT_KEY,
    DIVERSION_ACCOUNT_KEY,
    PURCHASE_DEVICE_KEY,
    PROGRAM_KEY,
    PURCHASE_ITEM_AMOUNT,
    DISCOUNT_AMOUNT,
    REBATE_AMOUNT,
    INTERCHANGE_RATE_ACTUAL_AMOUNT,
    INTCHG_TRANS_FLAT_FEE_AMT,
    INTCHG_LINE_ITEM_FLAT_FEE_AMT,
    INTERCHANGE_FUEL_GAS_AMT,
    INTERCHANGE_FUEL_DIESEL_AMT,
    INTERCHANGE_FUEL_OTHER_AMT,
    INTERCHANGE_PER_UNIT_FEE_AMT,
    GROSS_REVENUE_AMOUNT,
    TOTAL_TAX_AMOUNT,
    GROSS_SPEND_AMOUNT,
    GROSS_SPEND_LOCAL_AMOUNT,
    SPEND_FUEL_ONLY_AMOUNT,
    SPEND_FUEL_GAS_AMOUNT,
    SPEND_FUEL_DIESEL_AMOUNT,
    SPEND_FUEL_OTHER_AMOUNT,
    SPEND_MAINTENANCE_AMOUNT,
    SPEND_OTHER_AMOUNT,
    SPEND_FUEL_ONLY_LOCAL_AMOUNT,
    SPEND_FUEL_GAS_LOCAL_AMOUNT,
    SPEND_FUEL_DIESEL_LOCAL_AMOUNT,
    SPEND_FUEL_OTHER_LOCAL_AMOUNT,
    SPEND_MAINTENANCE_LOCAL_AMOUNT,
    SPEND_OTHER_LOCAL_AMOUNT,
    TAX_EXEMPT_SPEND_AMOUNT,
    TAX_EXEMPT_SPEND_LOCAL_AMOUNT,
    PURCHASE_GALLONS_QTY,
    PURCHASE_LITRES_QTY,
    FUEL_GAS_GALLONS_QTY,
    FUEL_GAS_LITRES_QTY,
    FUEL_DIESEL_GALLONS_QTY,
    FUEL_DIESEL_LITRES_QTY,
    FUEL_OTHER_GALLONS_QTY,
    FUEL_OTHER_LITRES_QTY,
    PURCHASE_CURRENCY_CODE,
    PURCHASE_UNITS_NAME,
    MERCHANT_TRANSACTION_ID,
    POS_AND_SITE_KEY,
    WEX_TRANSACTION_ITEM_SEQ_NBR,
    INTERCHANGE_RATE,
    TOTAL_TAX_LOCAL_AMOUNT,
    FEDERAL_TAX_AMOUNT,
    STATE_TAX_AMOUNT,
    LOCAL_TAX_AMOUNT,
    COUNTY_SALES_TAX_AMOUNT,
    CITY_SALES_TAX_AMOUNT,
    COUNTY_SPECIAL_TAX_AMOUNT,
    STATE_EXCISE_TAX_AMOUNT,
    COUNTY_EXCISE_TAX_AMOUNT,
    FEDERAL_EXCISE_TAX_AMOUNT,
    STATE_SPECIAL_TAX_AMOUNT,
    STATE_SALES_TAX_AMOUNT,
    CITY_EXCISE_TAX_AMOUNT,
    CITY_SPECIAL_TAX_AMOUNT,
    NBR_OF_UNITS_PURCHASED_COUNT,
    TRANSACTION_LINE_ITEM_KEY,
    INTERCHANGE_TOTAL_AMOUNT,
    NET_REVENUE_AMOUNT,
    TRANSACTION_LINE_ITEM_COUNT1,
    WEX_TRANSACTION_ID,
    LATE_FEES_AMOUNT,
    CARD_FEES_AMOUNT,
    ALL_OTHER_ANCILLARY_AMOUNT,
    WAIVED_REVENUE_AMOUNT,
    LINE_ITEM_TYPE_KEY,
    FINANCE_FEE_AMOUNT,
    PURCHASE_ITEM_LOCAL_AMOUNT,
    NON_FEDERAL_TAX_AMOUNT,
    FUEL_ONLY_INTERCHANGE_AMOUNT,
    MAINTENANCE_ONLY_INTCHG_AMOUNT,
    ALL_OTHER_INTERCHANGE_AMOUNT,
    GROSS_NON_REVENUE_AMOUNT,
    x.ROW_CREATE_DTTM,
    x.ROW_LAST_MOD_DTTM,
    x.ROW_SOURCE_SYS_NM,
    x.ROW_LAST_MOD_PROC_NM,
    x.ROW_LAST_MOD_PROC_SEQ_NBR,
    PRIVATE_SITE_GALLONS_QTY,
    PRIVATE_SITE_LITRES_QTY,
    WAIVED_LATE_FEES_AMOUNT,
    CURRENCY_CONVERSION_FEE_AMOUNT,
    MANUAL_REBATE_AMOUNT,
    TRUCK_STOP_FEE_AMOUNT
  FROM EDW_STAGE_OWNER.F_TRANSACTION_LINE_ITEM x
  JOIN D_DATE d ON (d.DATE_KEY = x.REVENUE_DATE_KEY)
  WHERE d.CALENDAR_DATE_DT BETWEEN v_min_dt AND v_max_dt
  ;
  COMMIT;
  log4me.info('DONE ' || v_proc);

EXCEPTION
  WHEN OTHERS THEN
    log4me.err('FAIL ' || v_proc || ' ' || SQLERRM);
    RAISE;
END F_TRANSACTION_LINE_ITEM_LOAD;
/
show errors
