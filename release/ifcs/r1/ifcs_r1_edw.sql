--
-- P4 IFCS R1 migration script
--
SET ECHO      OFF
SET NEWPAGE   0
SET SPACE     0
SET PAGESIZE  0
SET FEEDBACK  ON
SET HEADING   OFF
SET TRIMSPOOL ON
SET TAB       OFF
SET LINESIZE  2000

SPOOL /i1/&env/hub/logs/ifcs_r1.log;

alter session enable parallel dml;
alter session enable parallel ddl;

@d_tbl.ddl
@d_date_partition_split.prc

begin
  d_date_partition_split(2015);
end;
/

@d_backup.sql

SPOOL OFF


