/*
  To copy SOURCE_SYSTEM_CODE from HUB to EDW via LINK_OWNER views.

  The procedure update the following tables:

    1. D_PROGRAM
    2. D_LINE_ITEM_TYPE
    3. D_PURCHASE_DEVICE
    4. D_POS_AND_SITE

  ===========================================================================
*/
alter session enable parallel dml;

declare
  FMT_SES  constant varchar2(40) := 'YYYYMMDD.HH24MISS.FF4';

  X_PROC   constant varchar2(80) := 'one-shot source_system_code copy from HUB';
  X_SES_ID constant varchar2(40) := to_char(systimestamp, FMT_SES);
  X_LST    constant toklist      := toklist(
    'PROGRAM',
    'LINE_ITEM_TYPE',
    'POS_AND_SITE',
    'PURCHASE_DEVICE'
  );

  TYPE KV_T   IS RECORD(key integer, val varchar2(200));
  TYPE LIST_T IS TABLE OF KV_T;

  v_msg varchar2(200);
  v_cnt varchar2(100);

  -- ------------------------------------------------------------------------
  -- fill null source_system_code with SIEBEL
  --
  procedure copy(p_tbl varchar2) as
    v_src varchar2(80) := 'link_owner.D_' || p_tbl;
    v_dst varchar2(80) := 'edw_owner.D_'  || p_tbl;
    v_key varchar2(40) := p_tbl || '_KEY';

    v_cnd varchar2(200):= 'd.' || v_key || '=' || 's.' || v_key;
    v_sql CLOB := '
      merge /*+ parallel(d, 64) use_hash(d, s) append */ 
      into ' || v_dst || ' d
      using (
        select /*+ parallel(x, 64) */ ' || v_key || ',
           source_system_code, row_last_mod_proc_nm
        from  ' || v_src || ' x
        where source_system_code is not null
          and row_last_mod_proc_nm is not null
      ) s
      on (' || v_cnd || ') 
      when matched then update 
        set d.source_system_code   = s.source_system_code, 
            d.row_last_mod_proc_nm = s.row_last_mod_proc_nm, 
            d.row_last_mod_dttm    = sysdate
    ';
  begin
    v_msg := 'copying from ' || v_src || ' to ' || v_dst;
    execute immediate v_sql;
    v_cnt := ' => ' || SQL%ROWCOUNT || ' rows';
    commit;
    log4me.debug(v_msg || v_cnt, v_sql, X_SES_ID);
  end copy;

begin
  log4me.info('INIT ' || X_PROC, X_SES_ID);
  for i in 1..X_LST.count loop
    copy(X_LST(i));
  end loop;
  log4me.info('DONE ' || X_PROC, X_SES_ID);

exception
when others then
  rollback;
  log4me.err('FAIL ' || X_PROC || '#' || v_msg, X_SES_ID);
  raise;
end;
/
show errors

