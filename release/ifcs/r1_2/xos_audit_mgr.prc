/*
  Name : xos_audit_mgr Package
  Spec.: Dimension table audit facility

  Revision History: 
  1.0 20140328 CC - first version
  1.1 20140814 CC - change comment format, 
                    commented out audit table creation section (for reuse)
*/
-- ============================================================================
/*
 --
 -- make sure the following table and grant 
 -- 
DROP   TABLE ua_dim_audit PURGE;
CREATE TABLE ua_dim_audit(
  dim_key      integer        NOT NULL, -- but not unique
  table_name   varchar2(40),    -- table name
  column_name  varchar2(40),    -- column name
  before_value varchar2(1000),
  after_value  varchar2(1000),
  before_at    date,            -- before updated_at
  after_at     date,            -- after  updated_at
  created_at   timestamp,       -- this audit record is created
  schema_name  varchar2(40),    
  proc_nm      varchar2(200),
  proc_seq_nbr integer,
  session_id   varchar2(40)
)
tablespace D_BI
pctfree  10
initrans 1
maxtrans 255
storage(
  initial 64K
  minextents 1
  maxextents unlimited
);
GRANT SELECT ON ua_dim_audit TO EDW_OWNER_SELECT;
*/
-- ============================================================================
CREATE OR REPLACE PACKAGE xos_audit_mgr AS
/*
  Dimension table audit facility
  
    Using trigger to register table changes (update only). 
  
    In the heat of Data Warehousing development, time (or temporal) comes 
    into the picture. This new dimension created out of point-in-time 
    transactional system has gradually becomes an issue. 
    The solution to handle the "slowly changing" dimension has been proposed 
    by our friend Kimball. Type I, II, III, IV and even hybrid or sorts. 
    They solve the problem on the technical side. However, unfortunately, it 
    more often than not, has introduce new challenges from the business 
    point of  view. After a typically 2-3 years stabilization of a newly 
    constructed warehousing system, the changing dimension handling can
    start to have any visibility to the business communities.
  
    Without proper education and advertisement, push-backs are often seen 
    as the result. Frequently, reverting every thing back to Type I view 
    is common. Keeping the fancy back-end, expensive op supporting
    system with no accountable benefit, it often run gradually out-of-sync 
    with the business community. A rebuild with a different consultancy firm
    is typically.
  
    So, here we are. Auditing system with trigger is a simple yet effective
    solution to changing dimensions. Yes, there are cases, Type II is
    truly warranted. However, this approach will take care of 90% of the 
    required functionalities for the need. 
   
    By keeping the Key-Value pair approach, it is Type I but can be seen as
    Type II, II, even Type IV. Especially, with the advance of Cloud 
    computing on the horizon, this can be a quick and "clean" solution to 
    all the dimension management madness.
  
  What it does is to simply add trigger on update. Compare the different
  fields of interest. Register to an audit table when there is a difference
  before an update is made to the record. The audit record itself can be 
  treated as a skinny and tall fact table. When it grows to many records,
  same technique for fact partitioning can be used. Actually, Cloud is
  perfect for them.
  
  Spec:
    export_trigger() - the only exposed interface to "outside" world
    create a trigger procedure to be compiled. 
  
  Note:
    Too bad Oracle PL/SQL is not capable of doing this specific 
    dynamic SQL. So, we have to use this to create the procedure for 
    compilation. One day, maybe Oracle will fix that.
*/
  FMT_DT constant varchar2(30) := 'YYYY-MM-DD HH24:MI:SS';

  PROCEDURE set_context(
    p_tname     varchar2,
    p_key       integer,
    p_before_at date,
    p_after_at  date,
    p_proc_name varchar2,
    p_proc_seq  integer
  );
  PROCEDURE audit(p_cname varchar2, p_before number,   p_after number);
  PROCEDURE audit(p_cname varchar2, p_before date,     p_after date);
  PROCEDURE audit(p_cname varchar2, p_before varchar2, p_after varchar2);
  PROCEDURE save(
    p_cname     varchar2,
    p_before    varchar2,
    p_after     varchar2
  );
  PROCEDURE debug(
    p_cname     varchar2,
    p_before    varchar2,
    p_after     varchar2
  );
  PROCEDURE export_trigger(
    p_table  varchar2,
    p_schema varchar2 DEFAULT NULL
  );
END xos_audit_mgr;
/

CREATE OR REPLACE PACKAGE BODY xos_audit_mgr AS
  AUDIT_TB constant varchar2(40) := 'ua_dim_audit';

  g_now        date;
  g_schema     varchar2(60);
  g_session_id varchar2(40);

  g_tname      varchar2(60);
  g_key        integer;
  g_before_at  date;
  g_after_at   date;
  g_proc_name  varchar2(200);
  g_proc_seq   integer;

PROCEDURE set_context(
  p_tname     varchar2,
  p_key       integer,
  p_before_at date,
  p_after_at  date,
  p_proc_name varchar2,
  p_proc_seq  integer
)
AS
BEGIN
  select 
    sysdate, 
    sys_context('USERENV', 'SESSION_USER'),
    sys_context('USERENV', 'SESSIONID')
  into g_now, g_schema, g_session_id
  from dual;

  select p_tname, p_key, p_before_at, p_after_at, p_proc_name, p_proc_seq
  into   g_tname, g_key, g_before_at, g_after_at, g_proc_name, g_proc_seq
  from dual;
END set_context;
--
-- polymophic audit() interfaces for number, date, and varchar
--
PROCEDURE audit(p_cname varchar2, p_before number, p_after number) AS
BEGIN
  if p_before <> p_after then 
    save(p_cname, 
      NVL(to_char(p_before),''), 
      NVL(to_char(p_after), '')
    );
  end if;
END audit;

PROCEDURE audit(p_cname varchar2, p_before date, p_after date) AS
BEGIN
  if p_before <> p_after then 
    save(p_cname, 
      NVL(to_char(p_before, FMT_DT), ''), 
      NVL(to_char(p_after, FMT_DT), '')
    );
  end if;
END audit;

PROCEDURE audit(p_cname varchar2, p_before varchar2, p_after varchar2) AS
BEGIN
  if NVL(p_before,'X') <> NVL(p_after,'X') then 
    save(p_cname, 
      NVL(p_before, ''), 
      NVL(p_after, '')
    );
  end if;
END audit;

PROCEDURE save(
  p_cname     varchar2,
  p_before    varchar2,
  p_after     varchar2
)
AS
  v_sql constant CLOB := '
  INSERT INTO ' || AUDIT_TB || '(
    TABLE_NAME,
    COLUMN_NAME,
    DIM_KEY,
    BEFORE_VALUE,
    AFTER_VALUE,
    BEFORE_AT,
    AFTER_AT,
    CREATED_AT,
    SCHEMA_NAME,
    PROC_NM,
    PROC_SEQ_NBR,
    SESSION_ID
  )
  VALUES (
   :t, :c, :k, :b, :a, :bt, :at, :ct, :s, :p, :s, :i
  )'
  ;
BEGIN
  --
  -- the following debugging dump is only enabled in development
  -- debug(pcname, p_before, p_after);
  --
  EXECUTE IMMEDIATE v_sql USING
    g_tname,
    p_cname,
    g_key,
    p_before,
    p_after,
    g_before_at,
    g_after_at,
    systimestamp,
    g_schema,
    g_proc_name,
    g_proc_seq,
    g_session_id;
EXCEPTION
  WHEN OTHERS THEN
    debug(p_cname, p_before, p_after);
    RAISE_APPLICATION_ERROR(-20099, 'xos_audit_mgr.save');
END save;

PROCEDURE debug(
  p_cname     varchar2,
  p_before    varchar2,
  p_after     varchar2
)
AS
  v_using constant CLOB := 
    g_tname                     || ', ' ||
    p_cname                     || ', ' ||
    NVL(to_char(g_key),'')      || ', ' ||
    p_before                    || ', ' ||
    p_after                     || ', ' ||
    NVL(to_char(g_before_at,FMT_DT),'') || ', ' ||
    NVL(to_char(g_after_at,FMT_DT),'')  || ', ' ||
    to_char(g_now,FMT_DT)       || ', ' ||
    g_schema                    || ', ' ||
    NVL(g_proc_name,'')         || ', ' ||
    to_char(NVL(g_proc_seq,'')) || ', ' ||
    g_session_id;
  v_proc constant varchar2(400) := 
    'audit on ' || g_tname || '.' || p_cname || 
    '[' || NVL(to_char(g_key),'') || '] ' ||
    '{' || p_before || '=>' || p_after || '}';
BEGIN
  log4me.debug(v_proc, v_using);
END debug;

PROCEDURE export_trigger(
  p_table  varchar2,
  p_schema varchar2 DEFAULT NULL
)
AS
  v_key    varchar2(40) := substr(p_table, instr(p_table,'_')+1) || '_KEY';
  v_schema varchar2(40) := p_schema;
  CURSOR c_clist IS
    SELECT /*+ cache(t) */ DISTINCT
      t.column_name
    FROM all_cons_columns t 
    WHERE 1=1
      AND t.table_name = p_table
      AND t.owner      = v_schema
      AND t.column_name not like 'ROW_LAST%'
      AND t.column_name not like 'ROW_CREATE%'
      AND t.column_name not like 'ROW_MD5%'
    ORDER BY t.column_name
  ;

  v_trigger_head constant CLOB := '
CREATE OR REPLACE TRIGGER TRUA_' || p_table || '
BEFORE UPDATE ON ' || upper(p_table) || '
REFERENCING OLD AS o NEW AS n
FOR EACH ROW
BEGIN
  xos_audit_mgr.set_context(
    ''' || p_table || ''',
    :o.' || v_key   || ',
    :o.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_DTTM,
    :n.ROW_LAST_MOD_PROC_NM,
    :n.ROW_LAST_MOD_PROC_SEQ_NBR
  );
';
  v_trigger_tail constant CLOB := '
END;
/';
  
  v_cname  varchar2(40);
  v_text   CLOB;
BEGIN
  if v_schema is NULL then
    v_schema := sys_context('USERENV', 'SESSION_USER');
  end if;
  dbms_output.put_line(v_trigger_head);
  FOR rec IN c_clist
  LOOP
    v_cname := rec.column_name;
    v_text := '  xos_audit_mgr.audit(''' || v_cname || ''', ' ||
      ':o.' || v_cname || ', ' ||
      ':n.' || v_cname || ');';

    dbms_output.put_line(v_text);
  END LOOP;
  dbms_output.put_line(v_trigger_tail);
END export_trigger;

END xos_audit_mgr;
/
