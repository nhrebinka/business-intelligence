<?xml version="1.0" encoding="UTF-8" ?>
<?oracle-xsl-mapper
  <!-- SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY. -->
  <mapSources>
    <source type="XSD">
      <schema location="../../ProjTemplateXSD/Exxon/ExxonPeriodBalancesAndAgedAmounts.xsd"/>
      <rootElement name="ExxonATOSPeriodBalancesandAgedAmountsDetail" namespace=""/>
    </source>
  </mapSources>
  <mapTargets>
    <target type="XSD">
      <schema location="../../xsd/PeriodBalancesAndAgedAmounts.xsd"/>
      <rootElement name="periodBalanceAndAgedAmount" namespace="http://www.wrightexpress.com/cmm/2.0/"/>
    </target>
  </mapTargets>
  <!-- GENERATED BY ORACLE XSL MAPPER 11.1.1.7.0(build 140401.1420.0097) AT [WED JUL 09 15:01:30 NZST 2014]. -->
?>
<xsl:stylesheet version="1.0"
	xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20"
	xmlns:bpws="http://schemas.xmlsoap.org/ws/2003/03/business-process/"
	xmlns:aia="http://www.oracle.com/XSL/Transform/java/oracle.apps.aia.core.xpath.AIAFunctions"
	xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction"
	xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
	xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue"
	xmlns:hwf="http://xmlns.oracle.com/bpel/workflow/xpath" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:med="http://schemas.oracle.com/mediator/xpath" xmlns:ids="http://xmlns.oracle.com/bpel/services/IdentityService/xpath"
	xmlns:bpm="http://xmlns.oracle.com/bpmn20/extensions" xmlns:xdk="http://schemas.oracle.com/bpel/extension/xpath/function/xdk"
	xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:bpmn="http://schemas.oracle.com/bpm/xpath"
	xmlns:ora="http://schemas.oracle.com/xpath/extension" xmlns:wex="http://www.wrightexpress.com/cmm/2.0/"
	xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator"
	xmlns:ldap="http://schemas.oracle.com/xpath/extension/ldap"
	exclude-result-prefixes="xsi xsl xsd wex xp20 bpws aia mhdr bpel oraext dvm hwf med ids bpm xdk xref bpmn ora socket ldap">
	<xsl:template match="/">
		<wex:ExxonATOSPeriodBalancesandAgedAmountsDetail>
			<xsl:for-each
				select="/ExxonATOSPeriodBalancesandAgedAmountsDetail/PeriodBalancesAndAgedAmountsDetail">
				<xsl:variable name="vCreatedOn"
						select="concat(substring(CreatedOn,7,4),'-',substring(CreatedOn,4,2),'-',substring(CreatedOn,1,2))" />
				<wex:periodBalanceAndAgedAmount>
					<wex:uid>
						<xsl:value-of select="Uid" />
					</wex:uid>
      <!-- The CDM / CMM should change still -->
						<wex:clientMid>
							<xsl:choose>
								<xsl:when test='ISOCode = "800316"'>
										<xsl:value-of select='"31"' />
								</xsl:when>
								<xsl:when test='ISOCode = "800580"'>
										<xsl:value-of select='"30"' />
								</xsl:when>
								<xsl:when test='ISOCode = "800702"'>
										<xsl:value-of select='"33"' />
								</xsl:when>																
						    </xsl:choose>
						</wex:clientMid>					
					<wex:customerNo>
						<xsl:value-of select="CustomerNumber" />
					</wex:customerNo>
					<wex:accountNo>
						<xsl:value-of select="AccountNumber" />
					</wex:accountNo>					
					<wex:createdOn>
						<xsl:value-of select="$vCreatedOn" />
					</wex:createdOn>
					<wex:openingBalance>
						<xsl:value-of select="OpeningBalance" />
					</wex:openingBalance>
					<wex:closingBalance>
						<xsl:value-of select="ClosingBalance" />
					</wex:closingBalance>
					<wex:overdueAmount>
						<xsl:value-of select="OverdueAmount" />
					</wex:overdueAmount>
					<wex:disputesTotal>
						<xsl:value-of select="DisputesTotal" />
					</wex:disputesTotal>
					<wex:dunningCode>
						<xsl:value-of select="DunningCode" />
					</wex:dunningCode>
					<wex:dunningControl1Description>
						<xsl:value-of select="DunningControl1Description" />
					</wex:dunningControl1Description>
					<wex:dunningControl2Description>
						<xsl:value-of select="DunningControl2Description" />
					</wex:dunningControl2Description>
					<wex:dunningControl3Description>
						<xsl:value-of select="DunningControl3Description" />
					</wex:dunningControl3Description>
					<wex:dunningControl4Description>
						<xsl:value-of select="DunningControl4Description" />
					</wex:dunningControl4Description>
					<wex:description>
						<xsl:value-of select="Description" />
					</wex:description>
					<wex:totalVolume>
						<xsl:value-of select="TotalVolume" />
					</wex:totalVolume>
					<wex:totalBalance>
						<xsl:value-of select="TotalBalance" />
					</wex:totalBalance>	
					<xsl:choose>
							<xsl:when test='PeriodBalanceType = "30 Day Buckets"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("buckets30days")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "Billing"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "Calendar Month"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("calendarMonth")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "30 days from billing"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing30days")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:otherwise>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing")' />
								</wex:periodBalanceType>
							</xsl:otherwise>
						</xsl:choose>					
					<wex:agedAmount>
						<wex:agingBucketDescription>
							<xsl:value-of select="AgingBucketDescription" />
						</wex:agingBucketDescription>						
						<wex:amount>
							<xsl:value-of select="Amount" />
						</wex:amount>
						<xsl:choose>
							<xsl:when test='PeriodBalanceType = "30 Day Buckets"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("buckets30days")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "Billing"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "Calendar Month"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("calendarMonth")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:when test='PeriodBalanceType = "30 days from billing"'>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing30days")' />
								</wex:periodBalanceType>
							</xsl:when>
							<xsl:otherwise>
								<wex:periodBalanceType>
									<xsl:value-of select='string("billing")' />
								</wex:periodBalanceType>
							</xsl:otherwise>
						</xsl:choose>								
					</wex:agedAmount>
				</wex:periodBalanceAndAgedAmount>
			</xsl:for-each>
		</wex:ExxonATOSPeriodBalancesandAgedAmountsDetail>
	</xsl:template>
</xsl:stylesheet>
