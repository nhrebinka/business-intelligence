<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="utf-8"
		indent="yes" />
	<xsl:param name="RefDataXml" />
	<xsl:variable name="LocationData" select="document($RefDataXml)//LocationDetail" />
	<xsl:template match="/">
		<xsl:call-template name="LocationData" />
	</xsl:template>

	<xsl:template name="LocationData">
		<ExxonATOSMerchantAgreementDetail>
			<xsl:for-each select="//MerchantAgreementDetail">
				<xsl:variable name="site_ref" select="SiteReference" />
				<MerchantAgreementDetail>
					<xsl:copy-of select="./*"/>
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SiteName1" />
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SiteName2" />
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SiteAddress1" />
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SiteAddress2" />
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SiteTown" />
					<xsl:copy-of select="$LocationData[SiteReference=$site_ref]/SitePostCode" />
				</MerchantAgreementDetail>
			</xsl:for-each>
		</ExxonATOSMerchantAgreementDetail>
	</xsl:template>
</xsl:stylesheet>