<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="utf-8"
		indent="yes" />
	<xsl:param name="RefDataXml" />
	<xsl:variable name="MerchantRef" select="document($RefDataXml)//MerchantDetail" />
	<xsl:template match="/">
		<xsl:call-template name="MerchantData" />
	</xsl:template>

	<xsl:template name="MerchantData">
		<ExxonATOSMerchantAgreementDetail>
			<xsl:for-each select="//MerchantAgreementDetail">
				<xsl:variable name="mchnt_ref" select="MerchantReference" />
				<MerchantAgreementDetail>
					<xsl:copy-of select="./*" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantName1" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantName2" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/AddressCode" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantLanguage" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantCurrency" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantType" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/MerchantTaxNumber" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/ReimbursementFrequency" />
					<xsl:copy-of select="$MerchantRef[MerchantReference=$mchnt_ref]/SBTIFrequency" />
				</MerchantAgreementDetail>
			</xsl:for-each>
		</ExxonATOSMerchantAgreementDetail>
	</xsl:template>
</xsl:stylesheet>