<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="utf-8"
		indent="yes" />
	<xsl:param name="RefDataXml" />
	<xsl:variable name="AddressDataXml" select="document($RefDataXml)//AddressDetail" />
	<xsl:template match="/">
		<xsl:call-template name="MerchantAddressData" />
	</xsl:template>

	<xsl:template name="MerchantAddressData">
		<ExxonATOSMerchantAgreementDetail>
			<xsl:for-each select="//MerchantAgreementDetail">
				<xsl:variable name="add_code" select="AddressCode" />
				<MerchantAgreementDetail>
					<xsl:copy-of select="./*" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/Address1" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/Address2" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/Address3" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/Address4" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/Address5" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/City" />
					<xsl:copy-of select="$AddressDataXml[AddressCode=$add_code]/PostalCode" />
				</MerchantAgreementDetail>
			</xsl:for-each>
		</ExxonATOSMerchantAgreementDetail>
	</xsl:template>
</xsl:stylesheet>