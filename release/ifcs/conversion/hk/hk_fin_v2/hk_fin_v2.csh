#/bin/tcsh

set VER = hk_fin_v2
set RST = ./${VER}_release_note.md
set SRC = /download/ifcs
set MNS = '-'
set TS  = `date +%Y-%m-%dT%H:%M:%SZ`

set xsd = ( \
  $SRC/AccountStatisticFile_v2.xsd \
  $SRC/PaymentInvoiceFile_v1.xsd \
  $SRC/PeriodBalanceAndAgedAmountFile_v2.xsd \
)

set xml = ( \
  HKG_accountstatistic_v2_20150818.xml  \
  HKG_paymentinvoice_v1_20150812.xml \
  HKG_periodbalandagedamt_v2_20150819.xml \
)

set elm = (  \
  accountStatistic \
  paymentInvoice \
  periodBalanceAndAgedAmount \
)

echo "### IFCS HK Financials v2 test dataset release note $TS\n" > $RST
echo '# XSD Validation' >> $RST

foreach i (1 2 3)
  echo "- validating $xsd[$i]" >> $RST
  set x=`xmlstarlet val -e -s $xsd[$i] $xml[$i]`
  echo "> $x\n" >> $RST
end

echo '# XML Element count' >> $RST
foreach i (1 2 3)
  echo "- $elm[$i] count in $xml[$i]" >> $RST
  set x=`xmlstarlet sel -t -v "count(.//wex:$elm[$i])" $xml[$i]`
  echo "> $x\n" >> $RST
end

echo '# Done' >> $RST





