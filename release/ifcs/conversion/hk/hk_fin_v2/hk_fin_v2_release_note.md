### IFCS HK Financials v2 test dataset release note 2015-08-25T15:42:13Z

# XSD Validation
- validating /download/ifcs/AccountStatisticFile_v2.xsd
> HKG_accountstatistic_v2_20150818.xml - valid

- validating /download/ifcs/PaymentInvoiceFile_v1.xsd
> HKG_paymentinvoice_v1_20150812.xml - valid

- validating /download/ifcs/PeriodBalanceAndAgedAmountFile_v2.xsd
> HKG_periodbalandagedamt_v2_20150819.xml - valid

# XML Element count
- accountStatistic count in HKG_accountstatistic_v2_20150818.xml
> 8

- paymentInvoice count in HKG_paymentinvoice_v1_20150812.xml
> 7

- periodBalanceAndAgedAmount count in HKG_periodbalandagedamt_v2_20150819.xml
> 6

# Done
