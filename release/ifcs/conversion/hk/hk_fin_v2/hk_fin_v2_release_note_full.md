### IFCS HK Financials v2 full dataset release note 2015-08-27T14:21:31Z

# XSD Validation

- validating against /download/ifcs/AccountStatisticFile_v2.xsd
> HKG_accountstatistic_v2_20150827_full.xml - valid

- validating against /download/ifcs/PaymentInvoiceFile_v1.xsd
> HKG_paymentinvoice_v1_20150827_full.xml - valid

- validating against /download/ifcs/PeriodBalanceAndAgedAmountFile_v2.xsd
> HKG_periodbalandagedamt_v2_20150827_full.xml - valid

# XML Element count

- accountStatistic element count for HKG_accountstatistic_v2_20150827_full.xml
> 5378

- paymentInvoice element count for HKG_paymentinvoice_v1_20150827_full.xml
> 3742

- periodBalanceAndAgedAmount element count for HKG_periodbalandagedamt_v2_20150827_full.xml
> 5378

# Done
